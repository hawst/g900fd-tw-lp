.class public final Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;
.super Ljava/lang/Object;
.source "TabModelSelectorUma.java"

# interfaces
.implements Lorg/chromium/base/ApplicationStatus$ActivityStateListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final USER_ACTION_DURING_RESTORE_MAX:I = 0x3


# instance fields
.field private mRestoreStartedAtMsec:J

.field private mRestoredTabId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    .line 26
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoreStartedAtMsec:J

    .line 29
    invoke-static {p0, p1}, Lorg/chromium/base/ApplicationStatus;->registerStateListenerForActivity(Lorg/chromium/base/ApplicationStatus$ActivityStateListener;Landroid/app/Activity;)V

    .line 30
    return-void
.end method

.method private static nowMsec()J
    .locals 2

    .prologue
    .line 103
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final destroy()V
    .locals 0

    .prologue
    .line 36
    invoke-static {p0}, Lorg/chromium/base/ApplicationStatus;->unregisterActivityStateListener(Lorg/chromium/base/ApplicationStatus$ActivityStateListener;)V

    .line 37
    return-void
.end method

.method public final onActivityStateChange(Landroid/app/Activity;I)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 41
    const/4 v0, 0x5

    if-eq p2, v0, :cond_1

    .line 46
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    if-eq v0, v1, :cond_0

    .line 43
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->userActionDuringTabRestore(I)V

    .line 44
    iput v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    goto :goto_0
.end method

.method final onPageLoadFailed(I)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 84
    iget v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    if-eq v0, v4, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    if-ne p1, v0, :cond_2

    .line 85
    sget-boolean v0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoreStartedAtMsec:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 88
    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->nowMsec()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoreStartedAtMsec:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1388

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 89
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->userActionDuringTabRestore(I)V

    .line 91
    :cond_1
    iput v4, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    .line 93
    :cond_2
    return-void
.end method

.method final onPageLoadFinished(I)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 77
    iget v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    if-ne p1, v0, :cond_0

    .line 78
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->userActionDuringTabRestore(I)V

    .line 79
    iput v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    .line 81
    :cond_0
    return-void
.end method

.method final onShowTab(IZ)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 53
    iget v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    if-eq p1, v0, :cond_0

    .line 54
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->userActionDuringTabRestore(I)V

    .line 55
    iput v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    .line 57
    :cond_0
    if-eqz p2, :cond_1

    .line 58
    iput p1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    .line 59
    invoke-static {}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->nowMsec()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoreStartedAtMsec:J

    .line 61
    :cond_1
    return-void
.end method

.method final onTabClosing(I)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 64
    iget v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    if-ne p1, v0, :cond_0

    .line 65
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->userActionDuringTabRestore(I)V

    .line 66
    iput v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    .line 68
    :cond_0
    return-void
.end method

.method final onTabCrashed(I)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 71
    iget v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    if-ne p1, v0, :cond_0

    .line 72
    iput v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    .line 74
    :cond_0
    return-void
.end method

.method final onTabsViewShown()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 96
    iget v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    if-eq v0, v1, :cond_0

    .line 97
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->userActionDuringTabRestore(I)V

    .line 98
    iput v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->mRestoredTabId:I

    .line 100
    :cond_0
    return-void
.end method

.method final userSwitchedToTab()V
    .locals 0

    .prologue
    .line 49
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabSwitched()V

    .line 50
    return-void
.end method
