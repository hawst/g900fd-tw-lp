.class public Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;
.super Ljava/lang/Object;
.source "UpdateInfoBarHelper.java"


# instance fields
.field private mAlreadyCheckedForUpdates:Z

.field private mMustShowInfoBar:Z

.field private mUpdateURL:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->mUpdateURL:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;Z)Z
    .locals 0

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->mMustShowInfoBar:Z

    return p1
.end method

.method private showUpdateInfoBar(Lcom/google/android/apps/chrome/ChromeActivity;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 64
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 67
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v4

    .line 68
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/Tab;->isNativePage()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    .line 69
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ChromeActivity;->mayShowUpdateInfoBar()Z

    move-result v5

    .line 70
    iget-object v3, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->mUpdateURL:Ljava/lang/String;

    if-nez v3, :cond_3

    move v3, v2

    .line 71
    :goto_1
    if-nez v0, :cond_1

    if-eqz v5, :cond_1

    if-eqz v3, :cond_4

    .line 72
    :cond_1
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->mMustShowInfoBar:Z

    .line 84
    :goto_2
    return-void

    :cond_2
    move v0, v1

    .line 68
    goto :goto_0

    :cond_3
    move v3, v1

    .line 70
    goto :goto_1

    .line 77
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ChromeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 78
    sget v2, Lcom/google/android/apps/chrome/R$string;->update_available_infobar:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 79
    sget v3, Lcom/google/android/apps/chrome/R$string;->update_available_infobar_button:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 80
    new-instance v3, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;

    iget-object v5, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->mUpdateURL:Ljava/lang/String;

    invoke-direct {v3, p1, v2, v0, v5}, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-virtual {v4}, Lorg/chromium/chrome/browser/Tab;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v0

    invoke-virtual {v0, v3}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->addInfoBar(Lorg/chromium/chrome/browser/infobar/InfoBar;)V

    .line 83
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->mMustShowInfoBar:Z

    goto :goto_2
.end method


# virtual methods
.method public checkForUpdateOnBackgroundThread(Lcom/google/android/apps/chrome/ChromeActivity;)V
    .locals 2

    .prologue
    .line 32
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 34
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->mAlreadyCheckedForUpdates:Z

    if-eqz v0, :cond_0

    .line 55
    :goto_0
    return-void

    .line 35
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->mAlreadyCheckedForUpdates:Z

    .line 37
    new-instance v0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper$1;-><init>(Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;Lcom/google/android/apps/chrome/ChromeActivity;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public showUpdateInfobarIfNecessary(Lcom/google/android/apps/chrome/ChromeActivity;)V
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->mMustShowInfoBar:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->showUpdateInfoBar(Lcom/google/android/apps/chrome/ChromeActivity;)V

    .line 60
    :cond_0
    return-void
.end method
