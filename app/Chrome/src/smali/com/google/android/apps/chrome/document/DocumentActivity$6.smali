.class Lcom/google/android/apps/chrome/document/DocumentActivity$6;
.super Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabObserver;
.source "DocumentActivity.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 659
    const-class v0, Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentActivity;)V
    .locals 0

    .prologue
    .line 659
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onDidChangeThemeColor(I)V
    .locals 2

    .prologue
    .line 717
    const/high16 v0, -0x1000000

    or-int/2addr v0, p1

    .line 719
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    # setter for: Lcom/google/android/apps/chrome/document/DocumentActivity;->mThemeColor:Ljava/lang/Integer;
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$602(Lcom/google/android/apps/chrome/document/DocumentActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 720
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # invokes: Lcom/google/android/apps/chrome/document/DocumentActivity;->updateTaskDescription()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$500(Lcom/google/android/apps/chrome/document/DocumentActivity;)V

    .line 721
    return-void
.end method

.method public onDidNavigateMainFrame(Lorg/chromium/chrome/browser/Tab;Ljava/lang/String;Ljava/lang/String;ZZI)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 686
    if-eqz p4, :cond_0

    .line 687
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # setter for: Lcom/google/android/apps/chrome/document/DocumentActivity;->mIcon:Landroid/graphics/Bitmap;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$402(Lcom/google/android/apps/chrome/document/DocumentActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 688
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # setter for: Lcom/google/android/apps/chrome/document/DocumentActivity;->mThemeColor:Ljava/lang/Integer;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$602(Lcom/google/android/apps/chrome/document/DocumentActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 690
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # invokes: Lcom/google/android/apps/chrome/document/DocumentActivity;->updateTaskDescription()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$500(Lcom/google/android/apps/chrome/document/DocumentActivity;)V

    .line 691
    return-void
.end method

.method protected onFaviconReceived(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 662
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabObserver;->onFaviconReceived(Landroid/graphics/Bitmap;)V

    .line 663
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # setter for: Lcom/google/android/apps/chrome/document/DocumentActivity;->mIcon:Landroid/graphics/Bitmap;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$402(Lcom/google/android/apps/chrome/document/DocumentActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 664
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # invokes: Lcom/google/android/apps/chrome/document/DocumentActivity;->updateTaskDescription()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$500(Lcom/google/android/apps/chrome/document/DocumentActivity;)V

    .line 665
    return-void
.end method

.method public onLoadStarted(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 695
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # invokes: Lcom/google/android/apps/chrome/document/DocumentActivity;->updateTaskDescription()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$500(Lcom/google/android/apps/chrome/document/DocumentActivity;)V

    .line 697
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentActivity;->mChromeAppMenuPropertiesDelegate:Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$300(Lcom/google/android/apps/chrome/document/DocumentActivity;)Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 698
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentActivity;->mChromeAppMenuPropertiesDelegate:Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$300(Lcom/google/android/apps/chrome/document/DocumentActivity;)Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->loadingStateChanged(Z)V

    .line 700
    :cond_0
    return-void
.end method

.method public onLoadStopped(Lorg/chromium/chrome/browser/Tab;)V
    .locals 3

    .prologue
    .line 704
    sget-boolean v0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    if-eq v0, p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 706
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # invokes: Lcom/google/android/apps/chrome/document/DocumentActivity;->updateTaskDescription()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$500(Lcom/google/android/apps/chrome/document/DocumentActivity;)V

    .line 707
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentActivity;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$100(Lcom/google/android/apps/chrome/document/DocumentActivity;)Lcom/google/android/apps/chrome/document/DocumentTabList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    iget-object v2, v2, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/document/DocumentTabList;->updateEntry(Landroid/content/Intent;Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 709
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentActivity;->mChromeAppMenuPropertiesDelegate:Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$300(Lcom/google/android/apps/chrome/document/DocumentActivity;)Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 710
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentActivity;->mChromeAppMenuPropertiesDelegate:Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$300(Lcom/google/android/apps/chrome/document/DocumentActivity;)Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->loadingStateChanged(Z)V

    .line 712
    :cond_1
    return-void
.end method

.method public onTabCrashed()V
    .locals 3

    .prologue
    .line 725
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-static {v0}, Lorg/chromium/base/ApplicationStatus;->getStateForActivity(Landroid/app/Activity;)I

    move-result v0

    .line 726
    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    .line 734
    :cond_0
    :goto_0
    return-void

    .line 728
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isTaskRoot()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "com.android.chrome.append_task"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 733
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->finish()V

    goto :goto_0
.end method

.method public onTitleUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 678
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabObserver;->onTitleUpdated(Lorg/chromium/chrome/browser/Tab;)V

    .line 679
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # invokes: Lcom/google/android/apps/chrome/document/DocumentActivity;->updateTaskDescription()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$500(Lcom/google/android/apps/chrome/document/DocumentActivity;)V

    .line 680
    return-void
.end method

.method public onUrlUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 3

    .prologue
    .line 669
    sget-boolean v0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    if-eq v0, p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 671
    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentManager;->getInstance()Lcom/google/android/apps/chrome/document/DocumentManager;

    move-result-object v0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentManager;->setCurrentUrl(Ljava/lang/String;)V

    .line 672
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # invokes: Lcom/google/android/apps/chrome/document/DocumentActivity;->updateTaskDescription()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$500(Lcom/google/android/apps/chrome/document/DocumentActivity;)V

    .line 673
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentActivity;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$100(Lcom/google/android/apps/chrome/document/DocumentActivity;)Lcom/google/android/apps/chrome/document/DocumentTabList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    iget-object v2, v2, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/document/DocumentTabList;->updateEntry(Landroid/content/Intent;Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 674
    return-void
.end method
