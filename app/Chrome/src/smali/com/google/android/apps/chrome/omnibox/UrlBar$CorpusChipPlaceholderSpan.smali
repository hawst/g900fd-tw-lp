.class public Lcom/google/android/apps/chrome/omnibox/UrlBar$CorpusChipPlaceholderSpan;
.super Landroid/text/style/AbsoluteSizeSpan;
.source "UrlBar.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1077
    const-class v0, Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/omnibox/UrlBar$CorpusChipPlaceholderSpan;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1081
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    .line 1082
    return-void
.end method

.method public static doesSpanExist(Landroid/text/Editable;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1100
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1102
    :cond_0
    :goto_0
    return v0

    .line 1101
    :cond_1
    const-class v2, Lcom/google/android/apps/chrome/omnibox/UrlBar$CorpusChipPlaceholderSpan;

    invoke-interface {p0, v0, v1, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    .line 1102
    array-length v2, v2

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static getSpan(Landroid/text/Editable;)Lcom/google/android/apps/chrome/omnibox/UrlBar$CorpusChipPlaceholderSpan;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1110
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1114
    :goto_0
    return-object v0

    .line 1111
    :cond_0
    const-class v0, Lcom/google/android/apps/chrome/omnibox/UrlBar$CorpusChipPlaceholderSpan;

    invoke-interface {p0, v3, v4, v0}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/omnibox/UrlBar$CorpusChipPlaceholderSpan;

    .line 1113
    sget-boolean v2, Lcom/google/android/apps/chrome/omnibox/UrlBar$CorpusChipPlaceholderSpan;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    array-length v2, v0

    if-le v2, v4, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1114
    :cond_1
    array-length v2, v0

    if-lez v2, :cond_2

    aget-object v0, v0, v3

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public addSpan(Landroid/text/Editable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1089
    invoke-static {p1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$CorpusChipPlaceholderSpan;->doesSpanExist(Landroid/text/Editable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1093
    :goto_0
    return-void

    .line 1090
    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    const-string/jumbo v1, " "

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1091
    const/4 v1, 0x1

    const/16 v2, 0x21

    invoke-virtual {v0, p0, v3, v1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1092
    invoke-interface {p1, v3, v0}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_0
.end method
