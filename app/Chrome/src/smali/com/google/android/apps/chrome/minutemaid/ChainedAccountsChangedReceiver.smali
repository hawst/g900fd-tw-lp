.class public Lcom/google/android/apps/chrome/minutemaid/ChainedAccountsChangedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ChainedAccountsChangedReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 60
    const-string/jumbo v0, "acleung-debug"

    const-string/jumbo v1, "MinuteMaid: onReceive"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/minutemaid/ChainedAccountsChangedReceiver$SystemAccountChangeEventChecker;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/minutemaid/ChainedAccountsChangedReceiver$SystemAccountChangeEventChecker;-><init>()V

    new-instance v2, Lcom/google/android/apps/chrome/services/AccountsChangedReceiver;

    invoke-direct {v2}, Lcom/google/android/apps/chrome/services/AccountsChangedReceiver;-><init>()V

    invoke-static {v0, v1, v2, p2}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->updateAccountRenameDataAsync(Landroid/content/Context;Lcom/google/android/apps/chrome/services/GoogleServicesManager$AccountChangeEventChecker;Landroid/content/BroadcastReceiver;Landroid/content/Intent;)V

    .line 71
    return-void
.end method
