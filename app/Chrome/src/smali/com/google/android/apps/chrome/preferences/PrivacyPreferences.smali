.class public Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "PrivacyPreferences.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field public static final SHOW_CLEAR_BROWSING_DATA_EXTRA:Ljava/lang/String; = "ShowClearBrowsingData"


# instance fields
.field private mClearBrowsingDataDialogFragment:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method

.method private showClearBrowsingDialog()V
    .locals 3

    .prologue
    .line 142
    new-instance v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->mClearBrowsingDataDialogFragment:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->mClearBrowsingDataDialogFragment:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "ClearBrowsingDataDialogFragment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 145
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 39
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->migrateNetworkPredictionPreferences()V

    .line 41
    sget v0, Lcom/google/android/apps/chrome/R$xml;->privacy_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->addPreferencesFromResource(I)V

    .line 43
    const-string/jumbo v0, "network_predictions"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionPreference;

    .line 46
    const-string/jumbo v1, "network_predictions_no_cellular"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isMobileNetworkCapable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 52
    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 58
    :goto_0
    const-string/jumbo v0, "crash_dump_upload"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;

    .line 61
    const-string/jumbo v1, "crash_dump_upload_no_cellular"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isMobileNetworkCapable()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 67
    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 73
    :goto_1
    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const-string/jumbo v0, "navigation_error"

    aput-object v0, v1, v2

    const-string/jumbo v0, "search_suggestions"

    aput-object v0, v1, v5

    .line 75
    array-length v3, v1

    move v0, v2

    :goto_2
    if-ge v0, v3, :cond_2

    aget-object v4, v1, v0

    .line 76
    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 77
    invoke-virtual {v4, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 55
    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_0

    .line 69
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 70
    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_1

    .line 80
    :cond_2
    invoke-virtual {p0, v5}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->setHasOptionsMenu(Z)V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchFieldTrial;->isEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "contextual_search"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 86
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "ShowClearBrowsingData"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 89
    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->showClearBrowsingDialog()V

    .line 91
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->updateSummaries()V

    .line 92
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 149
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onDestroy()V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->mClearBrowsingDataDialogFragment:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->mClearBrowsingDataDialogFragment:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->dismissProgressDialog()V

    .line 155
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->mClearBrowsingDataDialogFragment:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;

    .line 156
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 160
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 161
    sget v2, Lcom/google/android/apps/chrome/R$id;->menu_id_clear_browsing_data:I

    if-ne v1, v2, :cond_0

    .line 162
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->showClearBrowsingDialog()V

    .line 168
    :goto_0
    return v0

    .line 164
    :cond_0
    sget v2, Lcom/google/android/apps/chrome/R$id;->menu_id_help_privacy:I

    if-ne v1, v2, :cond_1

    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string/jumbo v2, "mobile_privacy"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/help/GoogleHelpUtils;->show(Landroid/app/Activity;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 168
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 99
    instance-of v0, p1, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 100
    check-cast v0, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z

    .line 106
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/chrome/preferences/NetworkPredictionPreference;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 107
    check-cast v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionPreference;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionPreference;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z

    .line 110
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setPreferenceChanged(Landroid/preference/Preference;Ljava/lang/Object;)V

    .line 112
    const/4 v0, 0x1

    return v0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 117
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onResume()V

    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->updateSummaries()V

    .line 119
    return-void
.end method

.method public updateSummaries()V
    .locals 3

    .prologue
    .line 125
    const-string/jumbo v0, "do_not_track"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 126
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isDoNotTrackEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->text_on:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 131
    :goto_0
    const-string/jumbo v0, "contextual_search"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 132
    if-eqz v0, :cond_0

    .line 133
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isContextualSearchDisabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->text_off:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 139
    :cond_0
    :goto_1
    return-void

    .line 129
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->text_off:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 136
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->text_on:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
