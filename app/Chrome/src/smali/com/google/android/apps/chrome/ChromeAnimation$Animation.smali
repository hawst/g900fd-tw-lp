.class public abstract Lcom/google/android/apps/chrome/ChromeAnimation$Animation;
.super Ljava/lang/Object;
.source "ChromeAnimation.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected mAnimatedObject:Ljava/lang/Object;

.field private mCurrentTime:J

.field private mDelayStartValue:Z

.field private mDuration:J

.field private mEnd:F

.field private mInterpolator:Landroid/view/animation/Interpolator;

.field private mStart:F

.field private mStartDelay:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 206
    const-class v0, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/Object;FFJJ)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getDecelerateInterpolator()Landroid/view/animation/DecelerateInterpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 247
    sget-boolean v0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    cmp-long v0, p4, v2

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 248
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mAnimatedObject:Ljava/lang/Object;

    .line 249
    iput p2, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mStart:F

    .line 250
    iput p3, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mEnd:F

    .line 251
    iput-wide p4, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mDuration:J

    .line 252
    iput-wide p6, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mStartDelay:J

    .line 253
    iput-wide v2, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mCurrentTime:J

    .line 254
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;FFJJLandroid/view/animation/Interpolator;)V
    .locals 0

    .prologue
    .line 231
    invoke-direct/range {p0 .. p7}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;-><init>(Ljava/lang/Object;FFJJ)V

    .line 232
    iput-object p8, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 233
    return-void
.end method


# virtual methods
.method public checkProperty(Ljava/lang/Enum;)Z
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x1

    return v0
.end method

.method public finished()Z
    .locals 6

    .prologue
    .line 317
    iget-wide v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mCurrentTime:J

    iget-wide v2, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mDuration:J

    iget-wide v4, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mStartDelay:J

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 318
    const/4 v0, 0x1

    .line 320
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getAnimatedObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mAnimatedObject:Ljava/lang/Object;

    return-object v0
.end method

.method public abstract setProperty(F)V
.end method

.method public setStartValueAfterStartDelay(Z)V
    .locals 0

    .prologue
    .line 268
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mDelayStartValue:Z

    .line 269
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 309
    iput-wide v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mCurrentTime:J

    .line 310
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->update(J)V

    .line 311
    return-void
.end method

.method public update(J)V
    .locals 9

    .prologue
    .line 288
    iget-wide v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mCurrentTime:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mCurrentTime:J

    .line 291
    iget-wide v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mCurrentTime:J

    iget-wide v2, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mDuration:J

    iget-wide v4, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mStartDelay:J

    add-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mCurrentTime:J

    .line 293
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mDelayStartValue:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mCurrentTime:J

    iget-wide v2, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mStartDelay:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 303
    :goto_0
    return-void

    .line 298
    :cond_0
    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mCurrentTime:J

    iget-wide v4, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mStartDelay:J

    sub-long/2addr v2, v4

    iget-wide v4, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mDuration:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 301
    iget v2, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mStart:F

    iget v3, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mEnd:F

    iget v4, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mStart:F

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mInterpolator:Landroid/view/animation/Interpolator;

    long-to-float v0, v0

    iget-wide v6, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mDuration:J

    long-to-float v1, v6

    div-float/2addr v0, v1

    invoke-interface {v4, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->setProperty(F)V

    goto :goto_0
.end method

.method public updateAndFinish()V
    .locals 4

    .prologue
    .line 276
    iget-wide v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mDuration:J

    iget-wide v2, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mStartDelay:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mCurrentTime:J

    .line 277
    iget v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->mEnd:F

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->setProperty(F)V

    .line 278
    return-void
.end method
