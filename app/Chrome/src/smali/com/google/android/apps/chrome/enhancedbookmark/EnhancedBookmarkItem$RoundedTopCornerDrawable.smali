.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "EnhancedBookmarkItem.java"


# instance fields
.field private final mBitmapHeight:I

.field private final mBitmapWidth:I

.field private final mCornerRadius:F

.field private final mPaint:Landroid/graphics/Paint;

.field private final mRect:Landroid/graphics/RectF;

.field final synthetic this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 80
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->enhanced_bookmark_item_corner_radius:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mCornerRadius:F

    .line 66
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mRect:Landroid/graphics/RectF;

    .line 67
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mPaint:Landroid/graphics/Paint;

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 83
    iput v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mBitmapWidth:I

    .line 84
    iput v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mBitmapHeight:I

    .line 85
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->enhanced_bookmark_item_corner_radius:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mCornerRadius:F

    .line 66
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mRect:Landroid/graphics/RectF;

    .line 67
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mPaint:Landroid/graphics/Paint;

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/BitmapShader;

    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v3, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v1, p2, v2, v3}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 76
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mBitmapWidth:I

    .line 77
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mBitmapHeight:I

    .line 78
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mRect:Landroid/graphics/RectF;

    iget v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mCornerRadius:F

    iget v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mCornerRadius:F

    iget-object v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 112
    return-void
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 116
    const/4 v0, -0x3

    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    .line 89
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mRect:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mCornerRadius:F

    add-float/2addr v2, v3

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 94
    iget v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mBitmapWidth:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mBitmapHeight:I

    if-lez v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getShader()Landroid/graphics/Shader;

    move-result-object v0

    check-cast v0, Landroid/graphics/BitmapShader;

    .line 97
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mBitmapWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mBitmapHeight:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 99
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mBitmapWidth:I

    int-to-float v3, v3

    mul-float/2addr v3, v1

    sub-float/2addr v2, v3

    mul-float/2addr v2, v5

    iget v3, p1, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    .line 100
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mBitmapHeight:I

    int-to-float v4, v4

    mul-float/2addr v4, v1

    sub-float/2addr v3, v4

    mul-float/2addr v3, v5

    iget v4, p1, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    .line 102
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 103
    invoke-virtual {v4, v1, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 104
    invoke-virtual {v4, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 105
    invoke-virtual {v0, v4}, Landroid/graphics/BitmapShader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 107
    :cond_0
    return-void
.end method

.method public setAlpha(I)V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 122
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 127
    return-void
.end method
