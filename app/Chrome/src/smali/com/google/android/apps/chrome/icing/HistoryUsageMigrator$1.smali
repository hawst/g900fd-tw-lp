.class final Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator$1;
.super Landroid/os/AsyncTask;
.source "HistoryUsageMigrator.java"


# instance fields
.field final synthetic val$controller:Lcom/google/android/apps/chrome/icing/IcingController;

.field final synthetic val$prefs:Landroid/content/SharedPreferences;

.field final synthetic val$searchJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/icing/SearchJniBridge;Lcom/google/android/apps/chrome/icing/IcingController;Landroid/content/SharedPreferences;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator$1;->val$searchJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;

    iput-object p2, p0, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator$1;->val$controller:Lcom/google/android/apps/chrome/icing/IcingController;

    iput-object p3, p0, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator$1;->val$prefs:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator$1;->val$searchJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/icing/SearchJniBridge;->addHistoricVisitsToUsageReportsBuffer()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    .line 50
    # getter for: Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator;->sLock:Ljava/lang/Object;
    invoke-static {}, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator;->access$000()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 51
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator$1;->val$controller:Lcom/google/android/apps/chrome/icing/IcingController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/icing/IcingController;->enableUsageReporting()V

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator$1;->val$prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "com.google.android.apps.chrome.icing.HistoryUsageMigrator.HISTORY_MIGRATION_REQUESTED"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 60
    :goto_0
    const/4 v0, 0x0

    # setter for: Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator;->sInProgress:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator;->access$102(Z)Z

    .line 61
    monitor-exit v1

    return-void

    .line 58
    :cond_0
    const-string/jumbo v0, "HistoryUsageMigrator"

    const-string/jumbo v2, "Unable to migrate history to icing."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 42
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator$1;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
