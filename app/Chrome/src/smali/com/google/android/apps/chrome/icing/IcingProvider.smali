.class public Lcom/google/android/apps/chrome/icing/IcingProvider;
.super Landroid/content/ContentProvider;
.source "IcingProvider.java"


# static fields
.field static final ACTION_COL_NAME:Ljava/lang/String; = "action"

.field private static final CURSOR_COLUMNS:[Ljava/lang/String;

.field static final SCORE_COL_NAME:Ljava/lang/String; = "doc_score"

.field static final SECTION_TITLE_COL_NAME:Ljava/lang/String; = "section_title"

.field static final SECTION_URL_COL_NAME:Ljava/lang/String; = "section_url"

.field static final SEQ_NO_COL_NAME:Ljava/lang/String; = "seqno"

.field static final URI_COL_NAME:Ljava/lang/String; = "uri"

.field private static volatile sInstance:Lcom/google/android/apps/chrome/icing/IcingProvider;


# instance fields
.field private volatile mContext:Landroid/content/Context;

.field private volatile mController:Lcom/google/android/apps/chrome/icing/IcingController;

.field private final mJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;

.field private final mNativeSideInitialized:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mProcessingRequest:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private volatile mUsageReportingEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "seqno"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "action"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "uri"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "doc_score"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "section_url"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "section_title"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "section_indexed_url"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/icing/IcingProvider;->CURSOR_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/icing/IcingJniBridge;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/icing/IcingProvider;-><init>(Lcom/google/android/apps/chrome/icing/SearchJniBridge;)V

    .line 127
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/chrome/icing/SearchJniBridge;)V
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 72
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mProcessingRequest:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 76
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mNativeSideInitialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 121
    sput-object p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->sInstance:Lcom/google/android/apps/chrome/icing/IcingProvider;

    .line 122
    iput-object p1, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;

    .line 123
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/icing/SearchJniBridge;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/icing/IcingProvider;-><init>(Lcom/google/android/apps/chrome/icing/SearchJniBridge;)V

    .line 117
    iput-object p2, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mContext:Landroid/content/Context;

    .line 118
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/icing/IcingProvider;Z)Z
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mUsageReportingEnabled:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/icing/IcingProvider;)Lcom/google/android/apps/chrome/icing/IcingController;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mController:Lcom/google/android/apps/chrome/icing/IcingController;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/icing/IcingProvider;)Lcom/google/android/apps/chrome/icing/SearchJniBridge;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/icing/IcingProvider;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mNativeSideInitialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method public static getContextReporter(Lcom/google/android/apps/chrome/ChromeActivity;)Lcom/google/android/apps/chrome/icing/ContextReporter;
    .locals 2

    .prologue
    .line 162
    sget-object v0, Lcom/google/android/apps/chrome/icing/IcingProvider;->sInstance:Lcom/google/android/apps/chrome/icing/IcingProvider;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/icing/IcingProvider;->sInstance:Lcom/google/android/apps/chrome/icing/IcingProvider;

    iget-object v0, v0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mController:Lcom/google/android/apps/chrome/icing/IcingController;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 165
    :goto_0
    return-object v0

    .line 163
    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/icing/ContextReporter;

    sget-object v1, Lcom/google/android/apps/chrome/icing/IcingProvider;->sInstance:Lcom/google/android/apps/chrome/icing/IcingProvider;

    iget-object v1, v1, Lcom/google/android/apps/chrome/icing/IcingProvider;->mController:Lcom/google/android/apps/chrome/icing/IcingController;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/icing/ContextReporter;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;Lcom/google/android/apps/chrome/icing/IcingController;)V

    .line 164
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/icing/ContextReporter;->enable()V

    goto :goto_0
.end method

.method static getInstanceForTest()Lcom/google/android/apps/chrome/icing/IcingProvider;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/google/android/apps/chrome/icing/IcingProvider;->sInstance:Lcom/google/android/apps/chrome/icing/IcingProvider;

    return-object v0
.end method

.method private init()V
    .locals 4

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mController:Lcom/google/android/apps/chrome/icing/IcingController;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$integer;->google_play_services_version:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 152
    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mController:Lcom/google/android/apps/chrome/icing/IcingController;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/icing/IcingController;->init(I)V

    .line 153
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mUsageReportingEnabled:Z

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mController:Lcom/google/android/apps/chrome/icing/IcingController;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator;->startMigrationAndEnableReporting(Landroid/content/SharedPreferences;Lcom/google/android/apps/chrome/icing/SearchJniBridge;Lcom/google/android/apps/chrome/icing/IcingController;Z)V

    .line 159
    :cond_0
    return-void
.end method

.method private isEnabled(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 130
    invoke-static {p1}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/chrome/icing/IcingProvider;->isSvelteDevice(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isSvelteDevice(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 135
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 136
    invoke-static {p0}, Lcom/google/android/apps/chrome/icing/IcingProvider;->isSvelteDeviceV19(Landroid/content/Context;)Z

    move-result v0

    .line 138
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isSvelteDeviceV19(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 143
    const-string/jumbo v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 145
    invoke-virtual {v0}, Landroid/app/ActivityManager;->isLowRamDevice()Z

    move-result v0

    return v0
.end method

.method private static isValid(Lcom/google/android/gms/appdatasearch/j;)Z
    .locals 4

    .prologue
    .line 202
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/j;->d()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/j;->d()J

    move-result-wide v0

    const-wide/32 v2, 0x7fffffff

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/j;->d()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static start()V
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/google/android/apps/chrome/icing/IcingProvider;->sInstance:Lcom/google/android/apps/chrome/icing/IcingProvider;

    if-nez v0, :cond_0

    .line 95
    :goto_0
    return-void

    .line 94
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/icing/IcingProvider;->sInstance:Lcom/google/android/apps/chrome/icing/IcingProvider;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/icing/IcingProvider;->init()V

    goto :goto_0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 269
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 275
    invoke-super {p0, p1, p2, p3}, Landroid/content/ContentProvider;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/icing/IcingProvider;->isEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 277
    const-string/jumbo v0, "\nIcingProvider [disabled]"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 301
    :goto_0
    return-void

    .line 279
    :cond_0
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    .line 280
    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    .line 281
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    .line 284
    new-instance v2, Lcom/google/android/apps/chrome/icing/IcingProvider$2;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/apps/chrome/icing/IcingProvider$2;-><init>(Lcom/google/android/apps/chrome/icing/IcingProvider;[Ljava/lang/String;Landroid/os/ConditionVariable;)V

    sget-object v3, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v4, v5, [Ljava/lang/Void;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/chrome/icing/IcingProvider$2;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 295
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\nIcingProvider [ jni initialized: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mNativeSideInitialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", processing request: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mProcessingRequest:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 297
    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mController:Lcom/google/android/apps/chrome/icing/IcingController;

    invoke-virtual {v2, p2}, Lcom/google/android/apps/chrome/icing/IcingController;->dump(Ljava/io/PrintWriter;)V

    .line 298
    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 299
    aget-object v0, v1, v5

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    goto :goto_0
.end method

.method getJniBridgeForTest()Lcom/google/android/apps/chrome/icing/SearchJniBridge;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;

    return-object v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->verifyContentProviderClient(Landroid/content/Context;)V

    .line 195
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.google.chrome.delta"

    return-object v0
.end method

.method hasPendingTasksForTest()Z
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mController:Lcom/google/android/apps/chrome/icing/IcingController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/icing/IcingController;->hasPendingTasksForTest()Z

    move-result v0

    return v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 259
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate()Z
    .locals 5

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/icing/IcingProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mContext:Landroid/content/Context;

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/icing/IcingProvider;->isEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 173
    new-instance v1, Lcom/google/android/apps/chrome/icing/IcingController;

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;

    iget-object v4, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/android/apps/chrome/icing/IcingController;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Lcom/google/android/apps/chrome/icing/SearchJniBridge;Landroid/content/SharedPreferences;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mController:Lcom/google/android/apps/chrome/icing/IcingController;

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/content/browser/BrowserStartupController;->get(Landroid/content/Context;)Lorg/chromium/content/browser/BrowserStartupController;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/icing/IcingProvider$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/icing/IcingProvider$1;-><init>(Lcom/google/android/apps/chrome/icing/IcingProvider;)V

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/BrowserStartupController;->addStartupCompletedObserver(Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;)V

    .line 189
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->verifyContentProviderClient(Landroid/content/Context;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mNativeSideInitialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mProcessingRequest:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mController:Lcom/google/android/apps/chrome/icing/IcingController;

    if-nez v0, :cond_1

    .line 216
    :cond_0
    const/4 v0, 0x0

    .line 238
    :goto_0
    return-object v0

    .line 219
    :cond_1
    :try_start_0
    invoke-static {p4}, Lcom/google/android/gms/appdatasearch/j;->a([Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/j;

    move-result-object v2

    .line 221
    invoke-static {v2}, Lcom/google/android/apps/chrome/icing/IcingProvider;->isValid(Lcom/google/android/gms/appdatasearch/j;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mProcessingRequest:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const/4 v0, 0x0

    goto :goto_0

    .line 225
    :cond_2
    :try_start_1
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/google/android/apps/chrome/icing/IcingProvider;->CURSOR_COLUMNS:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 226
    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/j;->d()J

    move-result-wide v4

    long-to-int v1, v4

    .line 227
    iget-object v3, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;

    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/j;->b()J

    move-result-wide v4

    invoke-interface {v3, v4, v5, v1}, Lcom/google/android/apps/chrome/icing/SearchJniBridge;->query(JI)[Lcom/google/android/apps/chrome/icing/DeltaFileEntry;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 228
    const/4 v6, 0x7

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-wide v8, v5, Lcom/google/android/apps/chrome/icing/DeltaFileEntry;->seqNo:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, v5, Lcom/google/android/apps/chrome/icing/DeltaFileEntry;->type:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget-object v8, v5, Lcom/google/android/apps/chrome/icing/DeltaFileEntry;->id:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    iget v8, v5, Lcom/google/android/apps/chrome/icing/DeltaFileEntry;->score:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    iget-object v8, v5, Lcom/google/android/apps/chrome/icing/DeltaFileEntry;->url:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x5

    iget-object v8, v5, Lcom/google/android/apps/chrome/icing/DeltaFileEntry;->title:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x6

    iget-object v5, v5, Lcom/google/android/apps/chrome/icing/DeltaFileEntry;->indexedUrl:Ljava/lang/String;

    aput-object v5, v6, v7

    invoke-virtual {v0, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 227
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 231
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mUsageReportingEnabled:Z

    if-eqz v1, :cond_4

    .line 232
    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;

    iget-object v4, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mController:Lcom/google/android/apps/chrome/icing/IcingController;

    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/j;->c()Z

    move-result v2

    invoke-static {v1, v3, v4, v2}, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator;->startMigrationAndEnableReporting(Landroid/content/SharedPreferences;Lcom/google/android/apps/chrome/icing/SearchJniBridge;Lcom/google/android/apps/chrome/icing/IcingController;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 238
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mProcessingRequest:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mProcessingRequest:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    throw v0
.end method

.method setIcingClientsForTest(Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;)V
    .locals 7

    .prologue
    .line 104
    new-instance v1, Lcom/google/android/apps/chrome/icing/IcingController;

    iget-object v3, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;

    const-wide/16 v4, 0x1f4

    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/icing/IcingController;-><init>(Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Lcom/google/android/apps/chrome/icing/SearchJniBridge;JLandroid/content/SharedPreferences;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mController:Lcom/google/android/apps/chrome/icing/IcingController;

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mController:Lcom/google/android/apps/chrome/icing/IcingController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/icing/IcingController;->enableUsageReporting()V

    .line 107
    return-void
.end method

.method setNativeSideInitializedForTest(Z)V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mNativeSideInitialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 250
    return-void
.end method

.method setUsageReportingEnabled(Z)V
    .locals 0

    .prologue
    .line 111
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/icing/IcingProvider;->mUsageReportingEnabled:Z

    .line 112
    return-void
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 264
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
