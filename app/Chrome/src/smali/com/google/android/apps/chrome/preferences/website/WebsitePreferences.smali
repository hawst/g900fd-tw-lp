.class public Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "WebsitePreferences.java"


# instance fields
.field private mEmptyView:Landroid/widget/TextView;

.field private final mSitesByHost:Ljava/util/Map;

.field private final mSitesByOrigin:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    .line 31
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByOrigin:Ljava/util/Map;

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByHost:Ljava/util/Map;

    .line 201
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->findOrCreateSitesByHost(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->findOrCreateSitesByOrigin(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByOrigin:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByHost:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->displayEmptyScreenMessage()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Lcom/google/android/apps/chrome/preferences/website/Website;
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->createSiteByOrigin(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Lcom/google/android/apps/chrome/preferences/website/Website;

    move-result-object v0

    return-object v0
.end method

.method private createSiteByOrigin(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Lcom/google/android/apps/chrome/preferences/website/Website;
    .locals 5

    .prologue
    .line 244
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->getOrigin()Ljava/lang/String;

    move-result-object v0

    .line 245
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 246
    new-instance v2, Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-direct {v2, p1}, Lcom/google/android/apps/chrome/preferences/website/Website;-><init>(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)V

    .line 247
    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByOrigin:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 248
    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByOrigin:Ljava/util/Map;

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByOrigin:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByHost:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByHost:Ljava/util/Map;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByHost:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 253
    return-object v2
.end method

.method private displayEmptyScreenMessage()V
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mEmptyView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mEmptyView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/chrome/R$string;->no_saved_website_settings:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 199
    :cond_0
    return-void
.end method

.method private findOrCreateSitesByHost(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Ljava/util/Set;
    .locals 3

    .prologue
    .line 264
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByHost:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByHost:Ljava/util/Map;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByHost:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    new-instance v2, Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-direct {v2, p1}, Lcom/google/android/apps/chrome/preferences/website/Website;-><init>(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)V

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByHost:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method private findOrCreateSitesByOrigin(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Ljava/util/Set;
    .locals 2

    .prologue
    .line 257
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->getOrigin()Ljava/lang/String;

    move-result-object v0

    .line 258
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByOrigin:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 259
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->createSiteByOrigin(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Lcom/google/android/apps/chrome/preferences/website/Website;

    .line 260
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByOrigin:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method private getInfoForOrigins()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByOrigin:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByHost:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 62
    new-instance v0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$GeolocationInfoFetcher;

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$GeolocationInfoFetcher;-><init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$1;)V

    .line 64
    new-instance v1, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$MidiInfoFetcher;

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$MidiInfoFetcher;-><init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;->chain(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;)Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$LocalStorageInfoFetcher;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$LocalStorageInfoFetcher;-><init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$1;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;->chain(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;)Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$WebStorageInfoFetcher;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$WebStorageInfoFetcher;-><init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$1;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;->chain(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;)Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$PopupExceptionInfoFetcher;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$PopupExceptionInfoFetcher;-><init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$1;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;->chain(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;)Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$ProtectedMediaIdentifierInfoFetcher;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$ProtectedMediaIdentifierInfoFetcher;-><init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$1;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;->chain(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;)Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$VoiceAndVideoCaptureInfoFetcher;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$VoiceAndVideoCaptureInfoFetcher;-><init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$1;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;->chain(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;)Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$ResultsPopulator;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$ResultsPopulator;-><init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$1;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;->chain(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;)Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;

    .line 78
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;->run()V

    .line 79
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 274
    sget v0, Lcom/google/android/apps/chrome/R$xml;->website_settings_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->addPreferencesFromResource(I)V

    .line 275
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 276
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mEmptyView:Landroid/widget/TextView;

    .line 277
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mEmptyView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 278
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 279
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 2

    .prologue
    .line 283
    instance-of v0, p2, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 284
    check-cast v0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;

    .line 285
    const-class v1, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->setFragment(Ljava/lang/String;)V

    .line 286
    const-string/jumbo v1, "com.google.android.apps.chrome.preferences.site"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->putSiteIntoExtras(Ljava/lang/String;)V

    .line 287
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    .line 289
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 294
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onResume()V

    .line 295
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->getInfoForOrigins()V

    .line 296
    return-void
.end method
