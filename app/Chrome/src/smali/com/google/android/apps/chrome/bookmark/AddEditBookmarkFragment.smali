.class public Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;
.super Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;
.source "AddEditBookmarkFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mActionListener:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;

.field private mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

.field private mBookmarkId:Ljava/lang/Long;

.field private mBookmarkNodeLoaded:Z

.field private mCancelButton:Landroid/widget/Button;

.field private mDefaultFolderLoaded:Z

.field private mFolderInput:Landroid/widget/Button;

.field private mInitialName:Ljava/lang/String;

.field private mInitialUrl:Ljava/lang/String;

.field private final mLoadedLock:Ljava/lang/Object;

.field private mOkButton:Landroid/widget/Button;

.field private mParentFolderId:J

.field private mParentFolderName:Ljava/lang/String;

.field private mRemoveButton:Landroid/widget/Button;

.field private mTitleInput:Landroid/widget/EditText;

.field private mUrlInput:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;-><init>()V

    .line 99
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mParentFolderId:J

    .line 105
    sget-object v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ADD_BOOKMARK:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    .line 117
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mLoadedLock:Ljava/lang/Object;

    .line 118
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mBookmarkNodeLoaded:Z

    .line 119
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mDefaultFolderLoaded:Z

    .line 671
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->handleDefaultBookmarkNode(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->handleGetBookmarkNode(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mOkButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mRemoveButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mCancelButton:Landroid/widget/Button;

    return-object v0
.end method

.method private checkValidInputs(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 424
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 425
    const/4 v0, 0x1

    .line 426
    iget-object v3, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->isFolder()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;

    sget v3, Lcom/google/android/apps/chrome/R$string;->bookmark_missing_url:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 428
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    move v0, v1

    .line 431
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 432
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;

    sget v3, Lcom/google/android/apps/chrome/R$string;->bookmark_missing_name:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 433
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 436
    :goto_0
    return v1

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method private clearErrorWhenNonEmpty(Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 405
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$1;-><init>(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;Landroid/widget/TextView;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 421
    return-void
.end method

.method private handleDefaultBookmarkNode(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V
    .locals 3

    .prologue
    .line 505
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 515
    :cond_0
    :goto_0
    return-void

    .line 507
    :cond_1
    if-eqz p1, :cond_2

    .line 508
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->setParentFolderInfo(JLjava/lang/String;)V

    .line 513
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mLoadedLock:Ljava/lang/Object;

    monitor-enter v1

    .line 514
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mDefaultFolderLoaded:Z

    .line 515
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 510
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->default_folder_error:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setError(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private handleGetBookmarkNode(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V
    .locals 3

    .prologue
    .line 472
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 501
    :cond_0
    :goto_0
    return-void

    .line 474
    :cond_1
    if-nez p1, :cond_2

    .line 475
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->invalid_bookmark:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->ok:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$3;-><init>(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$2;-><init>(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 489
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 493
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 494
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->isUrl()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->url()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 495
    :cond_3
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->parent()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 496
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->parent()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->parent()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->setParentFolderInfo(JLjava/lang/String;)V

    .line 499
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mLoadedLock:Ljava/lang/Object;

    monitor-enter v1

    .line 500
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mBookmarkNodeLoaded:Z

    .line 501
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private hideUrlInputRow()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 297
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->bookmark_url_label:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 298
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->bookmark_url_input:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 299
    return-void
.end method

.method private initializeFolderInputContent(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 316
    if-eqz p1, :cond_1

    .line 317
    const-string/jumbo v0, "parentId"

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 319
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 320
    iput-wide v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mParentFolderId:J

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    const-string/jumbo v1, "parentName"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 324
    :cond_1
    iget-wide v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mParentFolderId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mParentFolderName:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mParentFolderName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 330
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->isFragmentAsyncTaskRunning()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->isDefaultFolderLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->isBookmarkNodeLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 331
    new-instance v1, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    sget-object v2, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ADD_BOOKMARK:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    if-eq v0, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    sget-object v2, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ADD_FOLDER:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    if-ne v0, v2, :cond_4

    :cond_3
    const/4 v0, 0x0

    :goto_1
    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;-><init>(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;Ljava/lang/Long;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$string;->loading_bookmark:I

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->runFragmentAsyncTask(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;Ljava/lang/String;)Z

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mBookmarkId:Ljava/lang/Long;

    goto :goto_1
.end method

.method private initializeStateFromIntent(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 304
    if-eqz p1, :cond_1

    .line 313
    :cond_0
    :goto_0
    return-void

    .line 308
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mInitialName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mInitialName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 309
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 312
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mInitialUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mInitialUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static newAddNewFolderInstance(JLjava/lang/String;)Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;
    .locals 4

    .prologue
    .line 130
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;-><init>()V

    .line 131
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 132
    const-string/jumbo v2, "mode"

    sget-object v3, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ADD_FOLDER:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 133
    const-string/jumbo v2, "parentId"

    invoke-virtual {v1, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 134
    const-string/jumbo v2, "parentName"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->setArguments(Landroid/os/Bundle;)V

    .line 136
    return-object v0
.end method

.method public static newEditInstance(ZJ)Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;
    .locals 5

    .prologue
    .line 148
    new-instance v1, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;-><init>()V

    .line 149
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 150
    const-string/jumbo v3, "mode"

    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->EDIT_FOLDER:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 151
    const-string/jumbo v0, "id"

    invoke-virtual {v2, v0, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 152
    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->setArguments(Landroid/os/Bundle;)V

    .line 153
    return-object v1

    .line 150
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->EDIT_BOOKMARK:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    goto :goto_0
.end method

.method public static newInstance(ZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;
    .locals 8

    .prologue
    .line 170
    new-instance v1, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;-><init>()V

    .line 171
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 172
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_4

    .line 173
    :cond_0
    const-string/jumbo v3, "mode"

    if-eqz p0, :cond_3

    sget-object v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ADD_FOLDER:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 180
    :goto_1
    if-eqz p2, :cond_1

    const-string/jumbo v0, "name"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :cond_1
    if-eqz p3, :cond_2

    const-string/jumbo v0, "url"

    invoke-virtual {v2, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_2
    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->setArguments(Landroid/os/Bundle;)V

    .line 183
    return-object v1

    .line 173
    :cond_3
    sget-object v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ADD_BOOKMARK:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    goto :goto_0

    .line 176
    :cond_4
    const-string/jumbo v3, "mode"

    if-eqz p0, :cond_5

    sget-object v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->EDIT_FOLDER:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    :goto_2
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 178
    const-string/jumbo v0, "id"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_1

    .line 176
    :cond_5
    sget-object v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->EDIT_BOOKMARK:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    goto :goto_2
.end method


# virtual methods
.method protected getOnActionListenerForTest()Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;

    return-object v0
.end method

.method protected getParentFolderId()J
    .locals 2

    .prologue
    .line 461
    iget-wide v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mParentFolderId:J

    return-wide v0
.end method

.method protected isBookmarkNodeLoaded()Z
    .locals 2

    .prologue
    .line 216
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mLoadedLock:Ljava/lang/Object;

    monitor-enter v1

    .line 217
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mBookmarkNodeLoaded:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 218
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected isDefaultFolderLoaded()Z
    .locals 2

    .prologue
    .line 207
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mLoadedLock:Ljava/lang/Object;

    monitor-enter v1

    .line 208
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mDefaultFolderLoaded:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected isFolder()Z
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->isFolder()Z

    move-result v0

    return v0
.end method

.method public isLoadingBookmarks()Z
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mCurrentTask:Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;

    instance-of v0, v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 266
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 268
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->bookmark_action_title:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 269
    sget-object v1, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$4;->$SwitchMap$com$google$android$apps$chrome$bookmark$AddEditBookmarkFragment$Mode:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 283
    sget-boolean v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 274
    :pswitch_0
    sget v1, Lcom/google/android/apps/chrome/R$string;->edit_bookmark:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 285
    :cond_0
    :goto_0
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    sget-object v1, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ADD_BOOKMARK:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    sget-object v1, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ADD_FOLDER:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    if-ne v0, v1, :cond_2

    .line 286
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mRemoveButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 288
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->isFolder()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 289
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->hideUrlInputRow()V

    .line 292
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->initializeFolderInputContent(Landroid/os/Bundle;)V

    .line 293
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->initializeStateFromIntent(Landroid/os/Bundle;)V

    .line 294
    return-void

    .line 277
    :pswitch_2
    sget v1, Lcom/google/android/apps/chrome/R$string;->add_folder:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 280
    :pswitch_3
    sget v1, Lcom/google/android/apps/chrome/R$string;->edit_folder:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 269
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;

    if-nez v0, :cond_1

    .line 351
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "No OnResultListener specified -- onClick == NoOp"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    :cond_0
    :goto_0
    return-void

    .line 354
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 355
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 356
    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mOkButton:Landroid/widget/Button;

    if-ne p1, v2, :cond_6

    .line 357
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->checkValidInputs(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 358
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->isFragmentAsyncTaskRunning()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 359
    const-string/jumbo v0, "AddEditBookmarkFragment"

    const-string/jumbo v1, "Pending asynchronous task when trying to add/update bookmarks."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 363
    :cond_2
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 364
    iget-object v3, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->isFolder()Z

    move-result v3

    if-nez v3, :cond_3

    .line 365
    const-string/jumbo v3, "url"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    :cond_3
    const-string/jumbo v1, "title"

    invoke-virtual {v2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    const-string/jumbo v0, "parentId"

    iget-wide v4, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mParentFolderId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 370
    const-string/jumbo v0, "isFolder"

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->isFolder()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 373
    new-instance v1, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    sget-object v3, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ADD_BOOKMARK:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    if-eq v0, v3, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    sget-object v3, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ADD_FOLDER:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    if-ne v0, v3, :cond_5

    :cond_4
    const/4 v0, 0x0

    :goto_1
    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;-><init>(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;Landroid/content/ContentValues;Ljava/lang/Long;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$string;->saving_bookmark:I

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->runFragmentAsyncTask(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;Ljava/lang/String;)Z

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mBookmarkId:Ljava/lang/Long;

    goto :goto_1

    .line 379
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mRemoveButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_9

    .line 380
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    sget-object v1, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ADD_BOOKMARK:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    if-ne v0, v1, :cond_7

    .line 381
    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "The remove functionality should be disabled for new bookmarks."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 385
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->isFragmentAsyncTaskRunning()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 386
    const-string/jumbo v0, "AddEditBookmarkFragment"

    const-string/jumbo v1, "Pending asynchronous task when trying to delete bookmarks."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 390
    :cond_8
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$DeleteBookmarkTask;

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mBookmarkId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, p0, v2, v3}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$DeleteBookmarkTask;-><init>(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;J)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->removing_bookmark:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->runFragmentAsyncTask(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 393
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mCancelButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_a

    .line 394
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;->onCancel()V

    goto/16 :goto_0

    .line 395
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;->triggerFolderSelection()V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 223
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->onCreate(Landroid/os/Bundle;)V

    .line 225
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 226
    const-string/jumbo v0, "mode"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    if-nez v0, :cond_0

    .line 228
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Created new AddEditBookmarkFragment without a mode defined.  Make sure arguments are correctly populated."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    sget-object v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ADD_BOOKMARK:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    .line 238
    :goto_0
    return-void

    .line 233
    :cond_0
    const-string/jumbo v0, "id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mBookmarkId:Ljava/lang/Long;

    .line 234
    :cond_1
    const-string/jumbo v0, "parentId"

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mParentFolderId:J

    .line 235
    const-string/jumbo v0, "parentName"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mParentFolderName:Ljava/lang/String;

    .line 236
    const-string/jumbo v0, "name"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mInitialName:Ljava/lang/String;

    .line 237
    const-string/jumbo v0, "url"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mInitialUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 243
    sget v0, Lcom/google/android/apps/chrome/R$layout;->add_bookmark:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 340
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 342
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 346
    :goto_0
    return-void

    .line 344
    :cond_0
    const-string/jumbo v0, "parentName"

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    const-string/jumbo v0, "parentId"

    iget-wide v2, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mParentFolderId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 248
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 250
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->remove:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mRemoveButton:Landroid/widget/Button;

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mRemoveButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 252
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->ok:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mOkButton:Landroid/widget/Button;

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 254
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->cancel:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mCancelButton:Landroid/widget/Button;

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 256
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->bookmark_title_input:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->clearErrorWhenNonEmpty(Landroid/widget/TextView;)V

    .line 258
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->bookmark_url_input:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->clearErrorWhenNonEmpty(Landroid/widget/TextView;)V

    .line 260
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->bookmark_folder_select:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 262
    return-void
.end method

.method public setOnActionListener(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;

    .line 192
    return-void
.end method

.method protected setParentFolderInfo(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 450
    iput-object p3, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mParentFolderName:Ljava/lang/String;

    .line 451
    iput-wide p1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mParentFolderId:J

    .line 452
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    invoke-virtual {v0, p3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 455
    :cond_0
    return-void
.end method
