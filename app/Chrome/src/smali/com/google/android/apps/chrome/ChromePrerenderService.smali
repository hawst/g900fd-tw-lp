.class public Lcom/google/android/apps/chrome/ChromePrerenderService;
.super Landroid/app/Service;
.source "ChromePrerenderService.java"


# static fields
.field public static final KEY_PREPRENDERED_URL:Ljava/lang/String; = "url_to_preprender"

.field public static final KEY_PRERENDER_HEIGHT:Ljava/lang/String; = "prerender_height"

.field public static final KEY_PRERENDER_WIDTH:Ljava/lang/String; = "prerender_width"

.field public static final KEY_REFERER:Ljava/lang/String; = "Referer"

.field public static final MSG_CANCEL_PRERENDER:I = 0x2

.field public static final MSG_PRERENDER_URL:I = 0x1


# instance fields
.field private mMessenger:Landroid/os/Messenger;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 48
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ChromePrerenderService;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/ChromePrerenderService;->prerenderUrl(Ljava/lang/String;Ljava/lang/String;II)V

    return-void
.end method

.method private prerenderUrl(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 128
    invoke-static {}, Lcom/google/android/apps/chrome/WarmupManager;->getInstance()Lcom/google/android/apps/chrome/WarmupManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/WarmupManager;->prerenderUrl(Ljava/lang/String;Ljava/lang/String;II)V

    .line 129
    return-void
.end method


# virtual methods
.method protected handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 99
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 101
    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "url_to_preprender"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 102
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "Referer"

    const-string/jumbo v3, ""

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 103
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "prerender_width"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 104
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "prerender_height"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 105
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    new-instance v0, Lcom/google/android/apps/chrome/ChromePrerenderService$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/ChromePrerenderService$1;-><init>(Lcom/google/android/apps/chrome/ChromePrerenderService;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 115
    :pswitch_1
    new-instance v0, Lcom/google/android/apps/chrome/ChromePrerenderService$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromePrerenderService$2;-><init>(Lcom/google/android/apps/chrome/ChromePrerenderService;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4

    .prologue
    .line 71
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/google/android/apps/chrome/ChromePrerenderService$IncomingHandler;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromePrerenderService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/chrome/ChromePrerenderService$IncomingHandler;-><init>(Lcom/google/android/apps/chrome/ChromePrerenderService;Landroid/content/Context;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromePrerenderService;->mMessenger:Landroid/os/Messenger;

    .line 74
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromePrerenderService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->init(Landroid/content/Context;)V

    .line 75
    new-instance v0, Lcom/google/android/apps/chrome/ChromePrerenderService$LauncherWarmUpTask;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/ChromePrerenderService$LauncherWarmUpTask;-><init>(Lcom/google/android/apps/chrome/ChromePrerenderService$1;)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromePrerenderService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromePrerenderService$LauncherWarmUpTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromePrerenderService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromePrerenderService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startBrowserProcessesAndLoadLibrariesSync(Landroid/content/Context;Z)V

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromePrerenderService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 80
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->control_container_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromePrerenderService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromePrerenderService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ApplicationInitialization;->enableFullscreenFlags(Landroid/content/res/Resources;Landroid/content/Context;F)V
    :try_end_0
    .catch Lorg/chromium/base/library_loader/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromePrerenderService;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ProcessInitException while starting the browser process"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    goto :goto_0
.end method
