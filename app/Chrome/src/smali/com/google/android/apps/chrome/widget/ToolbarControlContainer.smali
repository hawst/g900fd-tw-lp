.class public Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;
.super Lcom/google/android/apps/chrome/widget/ControlContainer;
.source "ToolbarControlContainer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final NOTIFICATIONS:[I


# instance fields
.field private mFindInPageShowing:Z

.field private mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private mMenuBtn:Landroid/view/View;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private final mPxToDp:F

.field private mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

.field private final mTabStripHeight:F

.field private final mTempPosition:[I

.field private mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->$assertionsDisabled:Z

    .line 38
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->NOTIFICATIONS:[I

    return-void

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 38
    :array_0
    .array-data 4
        0x3b
        0x2d
        0xc
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/ControlContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mTempPosition:[I

    .line 55
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    .line 57
    new-instance v0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$1;-><init>(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 86
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mPxToDp:F

    .line 87
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->tab_strip_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mTabStripHeight:F

    .line 88
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;-><init>(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$1;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mGestureDetector:Landroid/view/GestureDetector;

    .line 89
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mFindInPageShowing:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;Z)Z
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mFindInPageShowing:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/toolbar/Toolbar;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)[I
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mTempPosition:[I

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->isOnTabStrip(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)F
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mPxToDp:F

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;F)Z
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->isOverMenuButton(F)Z

    move-result v0

    return v0
.end method

.method private isOnTabStrip(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 287
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mTabStripHeight:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isOverMenuButton(F)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 122
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mMenuBtn:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mTempPosition:[I

    invoke-virtual {v1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 124
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mTempPosition:[I

    aget v1, v1, v0

    int-to-float v1, v1

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mTempPosition:[I

    aget v1, v1, v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mMenuBtn:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method protected createResourceAdapter()Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;
    .locals 2

    .prologue
    .line 93
    new-instance v0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$ViewResourceAdapterImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$ViewResourceAdapterImpl;-><init>(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$1;)V

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 274
    invoke-super {p0}, Lcom/google/android/apps/chrome/widget/ControlContainer;->onAttachedToWindow()V

    .line 275
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 277
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    .line 281
    invoke-super {p0}, Lcom/google/android/apps/chrome/widget/ControlContainer;->onDetachedFromWindow()V

    .line 282
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 284
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0}, Lcom/google/android/apps/chrome/widget/ControlContainer;->onFinishInflate()V

    .line 107
    sget v0, Lcom/google/android/apps/chrome/R$id;->toolbar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/toolbar/Toolbar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    .line 108
    sget v0, Lcom/google/android/apps/chrome/R$id;->menu_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mMenuBtn:Landroid/view/View;

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->toolbar_background:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->setBackgroundResource(I)V

    .line 117
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 118
    :cond_1
    sget-boolean v0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mMenuBtn:Landroid/view/View;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 119
    :cond_2
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 269
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    if-nez v0, :cond_1

    const/4 v1, 0x0

    .line 262
    :cond_0
    :goto_0
    return v1

    .line 249
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->isOnTabStrip(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 255
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 256
    if-eq v2, v1, :cond_3

    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    sget-object v3, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v2, v3, :cond_4

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->swipeFinished()V

    .line 259
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    move v0, v1

    :cond_4
    move v1, v0

    .line 262
    goto :goto_0
.end method

.method public setSwipeHandler(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    .line 101
    return-void
.end method
