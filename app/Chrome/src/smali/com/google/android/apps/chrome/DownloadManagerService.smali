.class public Lcom/google/android/apps/chrome/DownloadManagerService;
.super Ljava/lang/Object;
.source "DownloadManagerService.java"

# interfaces
.implements Lorg/chromium/content/browser/DownloadController$DownloadNotificationService;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final UPDATE_DELAY_MILLIS:J = 0x3e8L

.field private static sDownloadManagerService:Lcom/google/android/apps/chrome/DownloadManagerService;


# instance fields
.field private final mDownloadNotifier:Lcom/google/android/apps/chrome/DownloadNotifier;

.field private final mDownloadProgressMap:Ljava/util/concurrent/ConcurrentHashMap;

.field private final mHandler:Landroid/os/Handler;

.field private final mIsUIUpdateScheduled:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mSharedPrefs:Landroid/content/SharedPreferences;

.field private final mUpdateDelayInMillis:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/apps/chrome/DownloadManagerService;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/DownloadManagerService;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/DownloadNotifier;Landroid/os/Handler;J)V
    .locals 4

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/4 v1, 0x4

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mDownloadProgressMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 108
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mSharedPrefs:Landroid/content/SharedPreferences;

    .line 110
    iput-object p2, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mDownloadNotifier:Lcom/google/android/apps/chrome/DownloadNotifier;

    .line 111
    iput-wide p4, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mUpdateDelayInMillis:J

    .line 112
    iput-object p3, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mHandler:Landroid/os/Handler;

    .line 113
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mIsUIUpdateScheduled:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 114
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/DownloadManagerService;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/chrome/DownloadManagerService;->updateAllNotifications()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/DownloadManagerService;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mIsUIUpdateScheduled:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private addDownloadIdToSharedPrefs(I)V
    .locals 2

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/google/android/apps/chrome/DownloadManagerService;->getStoredDownloadIds()Ljava/util/Set;

    move-result-object v0

    .line 182
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 183
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/DownloadManagerService;->storeDownloadIds(Ljava/util/Set;)V

    .line 184
    return-void
.end method

.method public static getDownloadManagerService(Landroid/content/Context;)Lcom/google/android/apps/chrome/DownloadManagerService;
    .locals 6

    .prologue
    .line 82
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 83
    sget-object v0, Lcom/google/android/apps/chrome/DownloadManagerService;->sDownloadManagerService:Lcom/google/android/apps/chrome/DownloadManagerService;

    if-nez v0, :cond_0

    .line 84
    new-instance v0, Lcom/google/android/apps/chrome/DownloadManagerService;

    new-instance v2, Lcom/google/android/apps/chrome/SystemDownloadNotifier;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/SystemDownloadNotifier;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/DownloadManagerService;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/DownloadNotifier;Landroid/os/Handler;J)V

    sput-object v0, Lcom/google/android/apps/chrome/DownloadManagerService;->sDownloadManagerService:Lcom/google/android/apps/chrome/DownloadManagerService;

    .line 87
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/DownloadManagerService;->sDownloadManagerService:Lcom/google/android/apps/chrome/DownloadManagerService;

    return-object v0
.end method

.method private getStoredDownloadIds()Ljava/util/Set;
    .locals 4

    .prologue
    .line 167
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "DownloadNotificationIds"

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method private static parseNotificationId(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 153
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 156
    :goto_0
    return v0

    .line 155
    :catch_0
    move-exception v0

    const-string/jumbo v0, "DownloadNotificationService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Exception while parsing download id:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private removeDownloadIdFromSharedPrefs(I)V
    .locals 3

    .prologue
    .line 172
    invoke-direct {p0}, Lcom/google/android/apps/chrome/DownloadManagerService;->getStoredDownloadIds()Ljava/util/Set;

    move-result-object v0

    .line 173
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 174
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 175
    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 176
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/DownloadManagerService;->storeDownloadIds(Ljava/util/Set;)V

    .line 178
    :cond_0
    return-void
.end method

.method private removeProgressNotificationForDownload(I)V
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mDownloadProgressMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mDownloadNotifier:Lcom/google/android/apps/chrome/DownloadNotifier;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/DownloadNotifier;->cancelNotification(I)V

    .line 253
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/DownloadManagerService;->removeDownloadIdFromSharedPrefs(I)V

    .line 254
    return-void
.end method

.method private scheduleUpdateIfNeeded()V
    .locals 4

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mIsUIUpdateScheduled:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    new-instance v0, Lcom/google/android/apps/chrome/DownloadManagerService$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/DownloadManagerService$1;-><init>(Lcom/google/android/apps/chrome/DownloadManagerService;)V

    .line 242
    iget-object v1, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mHandler:Landroid/os/Handler;

    iget-wide v2, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mUpdateDelayInMillis:J

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 244
    :cond_0
    return-void
.end method

.method public static setDownloadManagerService(Lcom/google/android/apps/chrome/DownloadManagerService;)Lcom/google/android/apps/chrome/DownloadManagerService;
    .locals 1

    .prologue
    .line 97
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 98
    sget-object v0, Lcom/google/android/apps/chrome/DownloadManagerService;->sDownloadManagerService:Lcom/google/android/apps/chrome/DownloadManagerService;

    .line 99
    sput-object p0, Lcom/google/android/apps/chrome/DownloadManagerService;->sDownloadManagerService:Lcom/google/android/apps/chrome/DownloadManagerService;

    .line 100
    return-object v0
.end method

.method private storeDownloadIds(Ljava/util/Set;)V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 192
    const-string/jumbo v1, "DownloadNotificationIds"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 193
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 194
    return-void
.end method

.method private updateAllNotifications()V
    .locals 6

    .prologue
    .line 200
    sget-boolean v0, Lcom/google/android/apps/chrome/DownloadManagerService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mDownloadProgressMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;

    .line 202
    if-eqz v0, :cond_1

    .line 203
    sget-object v2, Lcom/google/android/apps/chrome/DownloadManagerService$2;->$SwitchMap$com$google$android$apps$chrome$DownloadManagerService$DownloadStatus:[I

    iget-object v3, v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;->mDownloadStatus:Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 205
    :pswitch_0
    iget-object v2, v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;->mDownloadInfo:Lorg/chromium/content/browser/DownloadInfo;

    invoke-virtual {v2}, Lorg/chromium/content/browser/DownloadInfo;->getDownloadId()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/DownloadManagerService;->removeProgressNotificationForDownload(I)V

    .line 207
    iget-object v2, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mDownloadNotifier:Lcom/google/android/apps/chrome/DownloadNotifier;

    iget-object v3, v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;->mDownloadInfo:Lorg/chromium/content/browser/DownloadInfo;

    invoke-interface {v2, v3}, Lcom/google/android/apps/chrome/DownloadNotifier;->notifyDownloadSuccessful(Lorg/chromium/content/browser/DownloadInfo;)V

    .line 208
    iget-object v0, v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;->mDownloadInfo:Lorg/chromium/content/browser/DownloadInfo;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/DownloadManagerService;->broadcastDownloadSuccessful(Lorg/chromium/content/browser/DownloadInfo;)V

    goto :goto_0

    .line 211
    :pswitch_1
    iget-object v2, v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;->mDownloadInfo:Lorg/chromium/content/browser/DownloadInfo;

    invoke-virtual {v2}, Lorg/chromium/content/browser/DownloadInfo;->getDownloadId()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/DownloadManagerService;->removeProgressNotificationForDownload(I)V

    .line 213
    iget-object v2, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mDownloadNotifier:Lcom/google/android/apps/chrome/DownloadNotifier;

    iget-object v3, v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;->mDownloadInfo:Lorg/chromium/content/browser/DownloadInfo;

    invoke-interface {v2, v3}, Lcom/google/android/apps/chrome/DownloadNotifier;->notifyDownloadFailed(Lorg/chromium/content/browser/DownloadInfo;)V

    .line 214
    const-string/jumbo v2, "DownloadNotificationService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Download failed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;->mDownloadInfo:Lorg/chromium/content/browser/DownloadInfo;

    invoke-virtual {v0}, Lorg/chromium/content/browser/DownloadInfo;->getFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 217
    :pswitch_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mDownloadNotifier:Lcom/google/android/apps/chrome/DownloadNotifier;

    iget-object v3, v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;->mDownloadInfo:Lorg/chromium/content/browser/DownloadInfo;

    iget-wide v4, v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;->mStartTimeInMillis:J

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/apps/chrome/DownloadNotifier;->notifyDownloadProgress(Lorg/chromium/content/browser/DownloadInfo;J)V

    goto :goto_0

    .line 222
    :cond_2
    return-void

    .line 203
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updateDownloadProgress(Lorg/chromium/content/browser/DownloadInfo;Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;)V
    .locals 4

    .prologue
    .line 257
    sget-boolean v0, Lcom/google/android/apps/chrome/DownloadManagerService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->hasDownloadId()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 258
    :cond_0
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getDownloadId()I

    move-result v1

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mDownloadProgressMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;

    .line 260
    if-nez v0, :cond_2

    .line 261
    new-instance v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3, p1, p2}, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;-><init>(JLorg/chromium/content/browser/DownloadInfo;Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;)V

    .line 263
    sget-object v2, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;->IN_PROGRESS:Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    if-ne p2, v2, :cond_1

    .line 266
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/DownloadManagerService;->addDownloadIdToSharedPrefs(I)V

    .line 268
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mDownloadProgressMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    :goto_0
    return-void

    .line 270
    :cond_2
    iput-object p2, v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;->mDownloadStatus:Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    .line 271
    iput-object p1, v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;->mDownloadInfo:Lorg/chromium/content/browser/DownloadInfo;

    goto :goto_0
.end method


# virtual methods
.method protected broadcastDownloadSuccessful(Lorg/chromium/content/browser/DownloadInfo;)V
    .locals 0

    .prologue
    .line 164
    return-void
.end method

.method public clearPendingDownloadNotifications()V
    .locals 5

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "DownloadNotificationIds"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 138
    invoke-direct {p0}, Lcom/google/android/apps/chrome/DownloadManagerService;->getStoredDownloadIds()Ljava/util/Set;

    move-result-object v0

    .line 139
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 140
    invoke-static {v0}, Lcom/google/android/apps/chrome/DownloadManagerService;->parseNotificationId(Ljava/lang/String;)I

    move-result v2

    .line 141
    if-lez v2, :cond_0

    .line 142
    iget-object v3, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mDownloadNotifier:Lcom/google/android/apps/chrome/DownloadNotifier;

    invoke-interface {v3, v2}, Lcom/google/android/apps/chrome/DownloadNotifier;->cancelNotification(I)V

    .line 143
    const-string/jumbo v2, "DownloadNotificationService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Download failed: Cleared download id:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 146
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "DownloadNotificationIds"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/chrome/DownloadManagerService;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 149
    :cond_2
    return-void
.end method

.method public onDownloadCompleted(Lorg/chromium/content/browser/DownloadInfo;)V
    .locals 6

    .prologue
    .line 118
    sget-object v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;->COMPLETE:Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    .line 119
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->isSuccessful()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getContentLength()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 120
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;->FAILED:Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    .line 122
    :cond_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/DownloadManagerService;->updateDownloadProgress(Lorg/chromium/content/browser/DownloadInfo;Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;)V

    .line 123
    invoke-direct {p0}, Lcom/google/android/apps/chrome/DownloadManagerService;->scheduleUpdateIfNeeded()V

    .line 124
    return-void
.end method

.method public onDownloadUpdated(Lorg/chromium/content/browser/DownloadInfo;)V
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;->IN_PROGRESS:Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/DownloadManagerService;->updateDownloadProgress(Lorg/chromium/content/browser/DownloadInfo;Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;)V

    .line 129
    invoke-direct {p0}, Lcom/google/android/apps/chrome/DownloadManagerService;->scheduleUpdateIfNeeded()V

    .line 130
    return-void
.end method
