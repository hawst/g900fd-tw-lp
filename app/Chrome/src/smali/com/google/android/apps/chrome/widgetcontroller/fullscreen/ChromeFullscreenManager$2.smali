.class Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$2;
.super Landroid/os/Handler;
.source "ChromeFullscreenManager.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 169
    const-class v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$2;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$2;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 172
    if-nez p1, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 181
    sget-boolean v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$2;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unexpected message for ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 175
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$2;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    # getter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->access$500(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)Lcom/google/android/apps/chrome/widget/ControlContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/ControlContainer;->requestLayout()V

    goto :goto_0

    .line 178
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$2;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->update(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->access$600(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;Z)V

    goto :goto_0

    .line 173
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
