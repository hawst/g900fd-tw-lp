.class public abstract Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;
.super Landroid/app/IntentService;
.source "IntentServiceWithWakeLock.java"


# static fields
.field static final EXTRA_HANDLE_WAKELOCK:Ljava/lang/String; = "wakelock"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method private releaseWakeLock(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 78
    const-string/jumbo v0, "wakelock"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/android2/WakeLockManager;->getInstance(Landroid/content/Context;)Lcom/google/ipc/invalidation/ticl/android2/WakeLockManager;

    move-result-object v0

    .line 80
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/android2/WakeLockManager;->isHeld(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/android2/WakeLockManager;->release(Ljava/lang/Object;)V

    .line 84
    :cond_0
    return-void
.end method

.method public static startServiceWithWakeLock(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 94
    const-string/jumbo v0, "wakelock"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 95
    invoke-static {p0}, Lcom/google/ipc/invalidation/ticl/android2/WakeLockManager;->getInstance(Landroid/content/Context;)Lcom/google/ipc/invalidation/ticl/android2/WakeLockManager;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x7530

    invoke-virtual {v0, v1, v2}, Lcom/google/ipc/invalidation/ticl/android2/WakeLockManager;->acquire(Ljava/lang/Object;I)V

    .line 97
    invoke-virtual {p0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 98
    return-void
.end method


# virtual methods
.method protected final onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 51
    if-nez p1, :cond_0

    .line 52
    const-string/jumbo v0, "IntentServiceWithWakeLock"

    const-string/jumbo v1, "Received null intent.  Ignoring intent."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    :goto_0
    return-void

    .line 57
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;->onHandleIntentWithWakeLock(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;->releaseWakeLock(Landroid/content/Intent;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;->releaseWakeLock(Landroid/content/Intent;)V

    throw v0
.end method

.method protected abstract onHandleIntentWithWakeLock(Landroid/content/Intent;)V
.end method
