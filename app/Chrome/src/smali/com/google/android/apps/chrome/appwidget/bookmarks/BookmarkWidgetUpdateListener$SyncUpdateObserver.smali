.class Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$SyncUpdateObserver;
.super Ljava/lang/Object;
.source "BookmarkWidgetUpdateListener.java"

# interfaces
.implements Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;


# instance fields
.field private mIsSyncEnabled:Z

.field private final mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

.field final synthetic this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;Lorg/chromium/sync/notifier/SyncStatusHelper;)V
    .locals 1

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$SyncUpdateObserver;->this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p2, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$SyncUpdateObserver;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$SyncUpdateObserver;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0, p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->registerSyncSettingsChangedObserver(Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;)V

    .line 72
    invoke-virtual {p2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$SyncUpdateObserver;->mIsSyncEnabled:Z

    .line 73
    return-void
.end method


# virtual methods
.method public syncSettingsChanged()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$SyncUpdateObserver;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    .line 78
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$SyncUpdateObserver;->mIsSyncEnabled:Z

    if-eq v1, v0, :cond_0

    .line 79
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$SyncUpdateObserver;->mIsSyncEnabled:Z

    .line 80
    iget-object v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$SyncUpdateObserver;->this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;

    # getter for: Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mListener:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;
    invoke-static {v1}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->access$000(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;)Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$SyncUpdateObserver;->this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;

    # getter for: Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mListener:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;
    invoke-static {v1}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->access$000(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;)Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;->onSyncEnabledStatusUpdated(Z)V

    .line 82
    :cond_0
    return-void
.end method
