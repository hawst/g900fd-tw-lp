.class Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;
.super Ljava/lang/Object;
.source "SyncStartupHelper.java"

# interfaces
.implements Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;Landroid/app/Activity;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->val$activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public tokenAvailable(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->access$000(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    :goto_0
    return-void

    .line 58
    :cond_0
    if-nez p1, :cond_1

    .line 61
    const-string/jumbo v0, "SyncStartupHelper"

    const-string/jumbo v1, "Could not create auth token"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDelegate:Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->access$100(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;)Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;->startupFailed()V

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->val$activity:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->val$activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 66
    :cond_1
    const-string/jumbo v0, "SyncStartupHelper"

    const-string/jumbo v1, "Received auth token - starting sync setup"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->access$200(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->val$account:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->syncSignInWithAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->access$200(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->addSyncStateChangedListener(Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->syncStateChanged()V

    goto :goto_0
.end method
