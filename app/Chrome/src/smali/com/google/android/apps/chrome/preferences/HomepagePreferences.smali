.class public Lcom/google/android/apps/chrome/preferences/HomepagePreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;
.source "HomepagePreferences.java"


# instance fields
.field private final mChromePreferenceManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

.field private mCustomUriCache:Ljava/lang/String;

.field private mCustomUriEditText:Landroid/widget/EditText;

.field private mHomepageEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

.field private mHomepageSwitch:Landroid/widget/Switch;

.field private mPartnerDefaultCheckbox:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;-><init>()V

    .line 30
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mChromePreferenceManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/preferences/HomepagePreferences;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->updateUIState()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/preferences/HomepagePreferences;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mCustomUriEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/preferences/HomepagePreferences;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mCustomUriCache:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/preferences/HomepagePreferences;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mChromePreferenceManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    return-object v0
.end method

.method private updateUIState()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mChromePreferenceManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefHomepagePreference()Z

    move-result v2

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mChromePreferenceManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefHomepagePartnerEnabledPreference()Z

    move-result v3

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mCustomUriEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 105
    iget-object v4, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mCustomUriEditText:Landroid/widget/EditText;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->getHomePageUrl()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v4, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mCustomUriEditText:Landroid/widget/EditText;

    if-eqz v2, :cond_1

    if-nez v3, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mPartnerDefaultCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mPartnerDefaultCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 111
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mCustomUriCache:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move v0, v1

    .line 107
    goto :goto_1
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 42
    new-instance v0, Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mHomepageSwitch:Landroid/widget/Switch;

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mHomepageSwitch:Landroid/widget/Switch;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->addSwitchToActionBar(Landroid/widget/Switch;)V

    .line 44
    new-instance v0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences$1;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mHomepageSwitch:Landroid/widget/Switch;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/chrome/preferences/HomepagePreferences$1;-><init>(Lcom/google/android/apps/chrome/preferences/HomepagePreferences;Landroid/content/Context;Landroid/widget/Switch;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mHomepageEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mHomepageEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/WidgetEnabler;->attach()V

    .line 52
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 57
    sget v0, Lcom/google/android/apps/chrome/R$layout;->homepage_preferences:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mChromePreferenceManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefHomepageCustomUriPreference()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mCustomUriCache:Ljava/lang/String;

    .line 61
    sget v0, Lcom/google/android/apps/chrome/R$id;->custom_uri:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mCustomUriEditText:Landroid/widget/EditText;

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mCustomUriEditText:Landroid/widget/EditText;

    new-instance v2, Lcom/google/android/apps/chrome/preferences/HomepagePreferences$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/preferences/HomepagePreferences$2;-><init>(Lcom/google/android/apps/chrome/preferences/HomepagePreferences;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mCustomUriEditText:Landroid/widget/EditText;

    new-instance v2, Lcom/google/android/apps/chrome/preferences/HomepagePreferences$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/preferences/HomepagePreferences$3;-><init>(Lcom/google/android/apps/chrome/preferences/HomepagePreferences;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 85
    sget v0, Lcom/google/android/apps/chrome/R$id;->default_checkbox:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mPartnerDefaultCheckbox:Landroid/widget/CheckBox;

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mPartnerDefaultCheckbox:Landroid/widget/CheckBox;

    new-instance v2, Lcom/google/android/apps/chrome/preferences/HomepagePreferences$4;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/preferences/HomepagePreferences$4;-><init>(Lcom/google/android/apps/chrome/preferences/HomepagePreferences;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 94
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->updateUIState()V

    .line 96
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 122
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->onDestroy()V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mHomepageEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/WidgetEnabler;->destroy()V

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mHomepageSwitch:Landroid/widget/Switch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setVisibility(I)V

    .line 125
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 115
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->onStop()V

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mChromePreferenceManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mCustomUriCache:Ljava/lang/String;

    invoke-static {v1}, Lorg/chromium/chrome/browser/UrlUtilities;->fixupUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/chrome/browser/UrlUtilities;->fixUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setPrefHomepageCustomUriPreference(Ljava/lang/String;)V

    .line 118
    return-void
.end method
