.class public Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;
.super Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;
.source "LayoutManagerChromeTablet.java"


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private mDefaultTitle:Ljava/lang/String;

.field private final mEnableCompositorTabStrip:Z

.field private final mStripFilterArea:Landroid/graphics/RectF;

.field private mTabStripFilter:Lcom/google/android/apps/chrome/eventfilter/AreaGestureEventFilter;

.field private mTabStripLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x14
        0xa
        0x2
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;ZZ)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 57
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Z)V

    .line 43
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mStripFilterArea:Landroid/graphics/RectF;

    .line 58
    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 61
    if-eqz p3, :cond_0

    .line 62
    new-instance v0, Lcom/google/android/apps/chrome/eventfilter/AreaGestureEventFilter;

    new-instance v3, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet$TabStripEventHandler;

    invoke-direct {v3, p0, v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet$TabStripEventHandler;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet$1;)V

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mStripFilterArea:Landroid/graphics/RectF;

    move-object v2, p2

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/eventfilter/AreaGestureEventFilter;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/GestureHandler;Landroid/graphics/RectF;ZZ)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripFilter:Lcom/google/android/apps/chrome/eventfilter/AreaGestureEventFilter;

    .line 64
    new-instance v0, Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripFilter:Lcom/google/android/apps/chrome/eventfilter/AreaGestureEventFilter;

    aput-object v3, v2, v5

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->getEventFilter()Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {v0, p2, v2}, Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;-><init>(Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;[Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    .line 66
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setEventFilter(Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    .line 70
    :cond_0
    iput-boolean p3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mEnableCompositorTabStrip:Z

    .line 71
    sget v0, Lcom/google/android/apps/chrome/R$string;->tab_loading_default_title:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mDefaultTitle:Ljava/lang/String;

    .line 72
    if-eqz p3, :cond_1

    .line 73
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getLayoutRenderHost()Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->setStripLayoutHelperManager(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;)V

    .line 79
    :cond_1
    invoke-virtual {p0, v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->setNextLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;)V

    .line 80
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    return-object v0
.end method


# virtual methods
.method public createLayoutTab(IZZZFF)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;
    .locals 7

    .prologue
    .line 176
    iget-boolean v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mEnableCompositorTabStrip:Z

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v5, p5

    move v6, p6

    invoke-super/range {v0 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->createLayoutTab(IZZZFF)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    return-object v0
.end method

.method protected getNotificationsToRegisterFor()[I
    .locals 1

    .prologue
    .line 194
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->NOTIFICATIONS:[I

    return-object v0
.end method

.method public getStripLayoutHelperManager()Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    return-object v0
.end method

.method protected getTitleForTab(Lcom/google/android/apps/chrome/tab/ChromeTab;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 182
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTitleForTab(Lcom/google/android/apps/chrome/tab/ChromeTab;)Ljava/lang/String;

    move-result-object v0

    .line 183
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mDefaultTitle:Ljava/lang/String;

    .line 184
    :cond_0
    return-object v0
.end method

.method protected handleNotificationMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    .line 205
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 212
    :sswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 213
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->getTabById(I)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v5

    .line 214
    if-eqz v5, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

    invoke-virtual {p0, v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->getTitleBitmap(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {p0, v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->getFaviconBitmap(Lorg/chromium/chrome/browser/Tab;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v4

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isTitleDirectionRtl()Z

    move-result v5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/TitleCache;->put(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;ZZ)V

    goto :goto_0

    .line 205
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0xa -> :sswitch_0
        0x14 -> :sswitch_0
    .end sparse-switch
.end method

.method public init(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;Lcom/google/android/apps/chrome/compositor/TabContentManager;Landroid/view/ViewGroup;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;)V
    .locals 10

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 137
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->init(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;Lcom/google/android/apps/chrome/compositor/TabContentManager;Landroid/view/ViewGroup;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;)V

    .line 139
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mEnableCompositorTabStrip:Z

    if-eqz v0, :cond_3

    .line 141
    invoke-interface {p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModels()Ljava/util/List;

    move-result-object v9

    .line 142
    const/4 v0, 0x0

    move v8, v0

    :goto_0
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-ge v8, v0, :cond_3

    .line 143
    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lorg/chromium/chrome/browser/tabmodel/TabModel;

    .line 144
    const/4 v0, 0x0

    move v7, v0

    :goto_1
    invoke-interface {v6}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    if-ge v7, v0, :cond_2

    .line 145
    invoke-interface {v6, v7}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v5

    .line 146
    if-eqz v5, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v1

    invoke-virtual {p0, v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->getTitleBitmap(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {p0, v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->getFaviconBitmap(Lorg/chromium/chrome/browser/Tab;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v4

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isTitleDirectionRtl()Z

    move-result v5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/TitleCache;->put(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;ZZ)V

    .line 144
    :cond_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    .line 142
    :cond_2
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    .line 155
    :cond_3
    return-void
.end method

.method protected onViewportChanged(Landroid/graphics/RectF;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 159
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->onViewportChanged(Landroid/graphics/RectF;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mLastVisibleViewportDp:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    .line 162
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mStripFilterArea:Landroid/graphics/RectF;

    iput v3, v1, Landroid/graphics/RectF;->left:F

    .line 163
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mStripFilterArea:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v2

    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 164
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mStripFilterArea:Landroid/graphics/RectF;

    iput v3, v1, Landroid/graphics/RectF;->top:F

    .line 165
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mStripFilterArea:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getHeight()F

    move-result v2

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripFilter:Lcom/google/android/apps/chrome/eventfilter/AreaGestureEventFilter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mStripFilterArea:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/eventfilter/AreaGestureEventFilter;->setEventArea(Landroid/graphics/RectF;)V

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->onSizeChanged(F)V

    .line 169
    :cond_0
    return-void
.end method

.method protected tabClosureCommitted(IZ)V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabClosureCommitted(IZ)V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/tabs/TitleCache;->remove(I)V

    .line 116
    :cond_0
    return-void
.end method

.method protected tabCreated(IILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;ZZFF)V
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->showControlsTransient()V

    .line 109
    :cond_0
    invoke-super/range {p0 .. p7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabCreated(IILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;ZZFF)V

    .line 110
    return-void
.end method

.method protected tabModelSwitched(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    .line 122
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->useAccessibilityLayout()Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mAccessibilityLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    .line 127
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 120
    goto :goto_0

    .line 125
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabModelSwitched(Z)V

    goto :goto_1
.end method

.method public tabSelected(IIZ)V
    .locals 7

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mAccessibilityLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    if-ne v0, v1, :cond_2

    .line 91
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabSelected(IIZ)V

    .line 103
    :cond_1
    :goto_0
    return-void

    .line 93
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->time()J

    move-result-wide v2

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabSelected(JIIZ)V

    .line 101
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->time()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabSelecting(JI)V

    goto :goto_0
.end method
