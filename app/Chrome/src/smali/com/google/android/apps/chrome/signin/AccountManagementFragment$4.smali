.class Lcom/google/android/apps/chrome/signin/AccountManagementFragment$4;
.super Ljava/lang/Object;
.source "AccountManagementFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/signin/AccountManagementFragment;)V
    .locals 0

    .prologue
    .line 369
    iput-object p1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$4;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 372
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$4;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    # getter for: Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGaiaServiceType:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->access$000(Lcom/google/android/apps/chrome/signin/AccountManagementFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper;->logEvent(II)V

    .line 376
    new-instance v0, Lcom/google/android/apps/chrome/services/AccountAdder;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/services/AccountAdder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$4;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x66

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/services/AccountAdder;->addAccount(Landroid/app/Activity;I)V

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$4;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    # getter for: Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGaiaServiceType:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->access$000(Lcom/google/android/apps/chrome/signin/AccountManagementFragment;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$4;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$4;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->closeEditor()V

    .line 384
    :cond_0
    return-void
.end method
