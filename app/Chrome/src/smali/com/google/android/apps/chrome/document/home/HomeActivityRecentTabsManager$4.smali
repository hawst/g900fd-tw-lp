.class Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$4;
.super Ljava/lang/Object;
.source "HomeActivityRecentTabsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

.field final synthetic val$tabId:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;I)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$4;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    iput p2, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$4;->val$tabId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 181
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 182
    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 183
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$4;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    # getter for: Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$100(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 184
    sget-object v1, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->BRING_TAB_TO_FRONT:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->name()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$4;->val$tabId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 186
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 187
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$4;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    # getter for: Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$100(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 188
    return-void
.end method
