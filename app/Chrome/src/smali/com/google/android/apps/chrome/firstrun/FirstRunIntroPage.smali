.class public abstract Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;
.super Lcom/google/android/apps/chrome/firstrun/FirstRunPage;
.source "FirstRunIntroPage.java"


# static fields
.field public static final INTRO_ALL_NECESSARY_DOCUMENT:J = 0x2L

.field public static final INTRO_ALL_NECESSARY_TABBED:J = 0x0L

.field public static final INTRO_RECENTS:J = 0x2L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;-><init>()V

    return-void
.end method

.method public static getAllNecessaryPages(Landroid/content/Context;)J
    .locals 2

    .prologue
    .line 108
    invoke-static {p0}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentModeEligible(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x2

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static getAllPresentablePages(Landroid/content/Context;)J
    .locals 2

    .prologue
    .line 117
    invoke-static {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;->getAllNecessaryPages(Landroid/content/Context;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static markAsShown(Landroid/content/Context;J)V
    .locals 5

    .prologue
    .line 90
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunIntroPageVisitedFlags()J

    move-result-wide v2

    .line 92
    or-long/2addr v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setFirstRunIntroPageVisitedFlags(J)V

    .line 93
    return-void
.end method

.method public static wasShown(Landroid/content/Context;J)Z
    .locals 3

    .prologue
    .line 79
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunIntroPageVisitedFlags()J

    move-result-wide v0

    .line 81
    and-long/2addr v0, p1

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static wereAllNecessaryPagesShown(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 100
    invoke-static {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;->getAllNecessaryPages(Landroid/content/Context;)J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;->wasShown(Landroid/content/Context;J)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected abstract getIntroDrawableId()I
.end method

.method protected abstract getIntroTextId()I
.end method

.method protected abstract getIntroTitleId()I
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 45
    sget v0, Lcom/google/android/apps/chrome/R$layout;->fre_page:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 47
    sget v1, Lcom/google/android/apps/chrome/R$id;->positive_button:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;->isLastPage()Z

    move-result v2

    if-eqz v2, :cond_0

    sget v2, Lcom/google/android/apps/chrome/R$string;->fre_done:I

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 49
    :cond_0
    new-instance v2, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage$1;-><init>(Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    sget v1, Lcom/google/android/apps/chrome/R$id;->negative_button:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v2, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage$2;-><init>(Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    sget v1, Lcom/google/android/apps/chrome/R$id;->image_view:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;->getIntroDrawableId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 68
    sget v1, Lcom/google/android/apps/chrome/R$id;->title:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;->getIntroTitleId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 69
    sget v1, Lcom/google/android/apps/chrome/R$id;->text:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;->getIntroTextId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 70
    return-object v0
.end method
