.class final Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;
.super Ljava/lang/Object;
.source "DocumentMigrationHelper.java"

# interfaces
.implements Lorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$contentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

.field final synthetic val$finalizeMode:I

.field final synthetic val$finalizeWhenDone:Z

.field final synthetic val$tabId:I

.field final synthetic val$tabList:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;

.field final synthetic val$title:Ljava/lang/String;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Landroid/app/Activity;ILcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;Ljava/lang/String;ZILcom/google/android/apps/chrome/compositor/TabContentManager;)V
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$url:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$activity:Landroid/app/Activity;

    iput p3, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$tabId:I

    iput-object p4, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$tabList:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;

    iput-object p5, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$title:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$finalizeWhenDone:Z

    iput p7, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$finalizeMode:I

    iput-object p8, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$contentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFaviconAvailable(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 362
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5$1;-><init>(Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;Landroid/graphics/Bitmap;)V

    .line 379
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$contentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    iget v2, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$tabId:I

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->getThumbnailForId(ILcom/google/android/apps/chrome/compositor/TabContentManager$DecompressThumbnailCallback;)V

    .line 380
    return-void
.end method
