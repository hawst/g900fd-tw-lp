.class public Lcom/google/android/apps/chrome/services/GoogleServicesManager;
.super Ljava/lang/Object;
.source "GoogleServicesManager.java"

# interfaces
.implements Lorg/chromium/base/ApplicationStatus$ApplicationStateListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final SESSION_TAG_PREFIX:Ljava/lang/String; = "session_sync"

.field private static sGoogleServicesManager:Lcom/google/android/apps/chrome/services/GoogleServicesManager;


# instance fields
.field private final mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

.field protected final mContext:Landroid/content/Context;

.field private mFirstActivityStarted:Z

.field private final mOAuth2TokenService:Lorg/chromium/chrome/browser/signin/OAuth2TokenService;

.field private final mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

.field private final mSigninManager:Lorg/chromium/chrome/browser/signin/SigninManager;

.field private final mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mFirstActivityStarted:Z

    .line 135
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 136
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 138
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSigninManager:Lorg/chromium/chrome/browser/signin/SigninManager;

    .line 142
    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->getForProfile(Lorg/chromium/chrome/browser/profiles/Profile;)Lorg/chromium/chrome/browser/signin/OAuth2TokenService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mOAuth2TokenService:Lorg/chromium/chrome/browser/signin/OAuth2TokenService;

    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->setupSessionSyncId()V

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    .line 150
    invoke-static {p1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    new-instance v1, Lcom/google/android/apps/chrome/services/GoogleServicesManager$SyncSettingsObserver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/chrome/services/GoogleServicesManager$SyncSettingsObserver;-><init>(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Lcom/google/android/apps/chrome/services/GoogleServicesManager$1;)V

    invoke-virtual {v0, v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->registerSyncSettingsChangedObserver(Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->createNewInstance(Landroid/content/Context;Lorg/chromium/chrome/browser/sync/ProfileSyncService;Lorg/chromium/sync/notifier/SyncStatusHelper;)Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    move-result-object v0

    .line 157
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->addSyncStateChangedListener(Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;)V

    .line 158
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    invoke-virtual {v1, v0}, Lorg/chromium/sync/signin/ChromeSigninController;->addListener(Lorg/chromium/sync/signin/ChromeSigninController$Listener;)V

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    new-instance v1, Lcom/google/android/apps/chrome/sync/SyncDisabler;

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    iget-object v3, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    iget-object v4, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/apps/chrome/sync/SyncDisabler;-><init>(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Lorg/chromium/sync/signin/ChromeSigninController;Lorg/chromium/sync/notifier/SyncStatusHelper;Lorg/chromium/chrome/browser/sync/ProfileSyncService;)V

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->addSyncStateChangedListener(Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;)V

    .line 167
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->applyAndroidSyncStateOnUiThread()V

    .line 169
    invoke-static {p0}, Lorg/chromium/base/ApplicationStatus;->registerApplicationStateListener(Lorg/chromium/base/ApplicationStatus$ApplicationStateListener;)V

    .line 170
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 171
    return-void
.end method

.method static synthetic access$100(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 61
    invoke-static {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->clearNewSignedInAccountName(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/services/GoogleServicesManager;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->applyAndroidSyncStateOnUiThread()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Ljava/lang/String;Lcom/google/android/apps/chrome/services/GoogleServicesStates;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->performResignin(Ljava/lang/String;Lcom/google/android/apps/chrome/services/GoogleServicesStates;Ljava/util/Set;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Landroid/accounts/Account;Lcom/google/android/apps/chrome/services/GoogleServicesStates;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->setSyncState(Landroid/accounts/Account;Lcom/google/android/apps/chrome/services/GoogleServicesStates;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Z)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->setChromeToMobileState(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Z)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->setAutologinState(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/services/GoogleServicesManager;)Lorg/chromium/sync/signin/ChromeSigninController;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/services/GoogleServicesManager;)Lorg/chromium/sync/notifier/SyncStatusHelper;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/services/GoogleServicesManager;)Z
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->getPreviousMasterSyncState()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Landroid/accounts/Account;)Z
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->getWantedSyncState(Landroid/accounts/Account;)Z

    move-result v0

    return v0
.end method

.method private accountExists(Landroid/accounts/Account;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 365
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v2

    .line 366
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 367
    invoke-virtual {v4, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 368
    const/4 v0, 0x1

    .line 371
    :cond_0
    return v0

    .line 366
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private applyAndroidSyncStateOnUiThread()V
    .locals 1

    .prologue
    .line 456
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 457
    new-instance v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager$5;-><init>(Lcom/google/android/apps/chrome/services/GoogleServicesManager;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 475
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 476
    return-void
.end method

.method static checkAndClearAccountsChangedPref(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 696
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "prefs_sync_accounts_changed"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 699
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "prefs_sync_accounts_changed"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 701
    const/4 v0, 0x1

    .line 703
    :cond_0
    return v0
.end method

.method private static clearNewSignedInAccountName(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 604
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "prefs_sync_account_renamed"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 606
    return-void
.end method

.method public static get(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/GoogleServicesManager;
    .locals 1

    .prologue
    .line 127
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 128
    sget-object v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->sGoogleServicesManager:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    if-nez v0, :cond_0

    .line 129
    new-instance v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->sGoogleServicesManager:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    .line 131
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->sGoogleServicesManager:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    return-object v0
.end method

.method private getCurrentStates()Lcom/google/android/apps/chrome/services/GoogleServicesStates;
    .locals 2

    .prologue
    .line 448
    invoke-static {}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->create()Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isMasterSyncAutomaticallyEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->sync(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isAutologinEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->autoLogin(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/chrome/snapshot/ChromeToMobileFeature;->isEnabled(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->chromeToMobile(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->build()Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    move-result-object v0

    return-object v0
.end method

.method private static getLastKnownAccountName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 637
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "prefs_sync_account_renamed"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 641
    if-nez v0, :cond_0

    invoke-static {p0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInAccountName()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public static getNewSignedInAccountName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 599
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "prefs_sync_account_renamed"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getPreviousMasterSyncState()Z
    .locals 3

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "prefs_sync_master_previous_state"

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isMasterSyncAutomaticallyEnabled()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private getWantedSyncState(Landroid/accounts/Account;)Z
    .locals 3

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "prefs_sync_wanted_state"

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v2, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabledForChrome(Landroid/accounts/Account;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private handleAccountRename(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 310
    const-string/jumbo v0, "GoogleServicesManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "handleAccountRename from: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->getCurrentStates()Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    move-result-object v0

    .line 317
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getPreferredDataTypes()Ljava/util/Set;

    move-result-object v1

    .line 322
    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/apps/chrome/services/GoogleServicesManager$2;

    invoke-direct {v3, p0, p2, v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager$2;-><init>(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Ljava/lang/String;Lcom/google/android/apps/chrome/services/GoogleServicesStates;Ljava/util/Set;)V

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->signOut(Landroid/app/Activity;Ljava/lang/Runnable;)V

    .line 332
    return-void
.end method

.method public static isAutoLoginEnabled(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 555
    invoke-static {p0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 556
    const/4 v0, 0x0

    .line 560
    :goto_0
    return v0

    .line 558
    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    .line 559
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->update()V

    .line 560
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isAutologinEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method private isStateChangeValid(Landroid/accounts/Account;Lcom/google/android/apps/chrome/services/GoogleServicesStates;)Z
    .locals 4

    .prologue
    .line 394
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 395
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->hasSync()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 396
    const-string/jumbo v1, "sync = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->isSyncEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 398
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->hasWantedSyncState()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 399
    const-string/jumbo v1, ", wantedSyncState = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->isWantedSyncStateEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 401
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->hasChromeToMobile()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 402
    const-string/jumbo v1, ", chromeToMobile = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->isChromeToMobileEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 404
    :cond_2
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->hasAutoLoginSet()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 405
    const-string/jumbo v1, ", autoLogin = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->isAutoLoginEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 407
    :cond_3
    const-string/jumbo v1, "GoogleServicesManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "setState: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    if-nez p1, :cond_4

    .line 409
    const/4 v0, 0x0

    .line 411
    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static markAccountsChangedPref(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 591
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "prefs_sync_accounts_changed"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 593
    return-void
.end method

.method private onFirstStart()V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    .line 200
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/chrome/snapshot/ChromeToMobileFeature;->isEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 203
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    invoke-virtual {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->ensureGcmIsInitialized()V

    .line 205
    :cond_1
    if-eqz v0, :cond_2

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/invalidation/InvalidationController;

    move-result-object v0

    .line 209
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getPreferredDataTypes()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->refreshRegisteredTypes(Ljava/util/Set;)V

    .line 211
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mFirstActivityStarted:Z

    .line 212
    return-void
.end method

.method private performResignin(Ljava/lang/String;Lcom/google/android/apps/chrome/services/GoogleServicesStates;Ljava/util/Set;)V
    .locals 5

    .prologue
    .line 337
    invoke-static {p1}, Lorg/chromium/sync/signin/AccountManagerHelper;->createAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 339
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSigninManager:Lorg/chromium/chrome/browser/signin/SigninManager;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-instance v4, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;

    invoke-direct {v4, p0, p2, v0, p3}, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;-><init>(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Lcom/google/android/apps/chrome/services/GoogleServicesStates;Landroid/accounts/Account;Ljava/util/Set;)V

    invoke-virtual {v1, v2, v0, v3, v4}, Lorg/chromium/chrome/browser/signin/SigninManager;->startSignIn(Landroid/app/Activity;Landroid/accounts/Account;ZLorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;)V

    .line 362
    return-void
.end method

.method private setAutologinState(Z)V
    .locals 1

    .prologue
    .line 479
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setAutologinEnabled(Z)V

    .line 480
    if-nez p1, :cond_0

    .line 481
    const/16 v0, 0x3a

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    .line 484
    :cond_0
    return-void
.end method

.method private setChromeToMobileState(Z)V
    .locals 2

    .prologue
    .line 527
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/ChromeToMobileFeature;->isEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eq v0, p1, :cond_1

    .line 528
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->ensureGcmIsInitialized()V

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createSetEnabledIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    .line 531
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;->startServiceWithWakeLock(Landroid/content/Context;Landroid/content/Intent;)V

    .line 533
    :cond_1
    return-void
.end method

.method private setSyncState(Landroid/accounts/Account;Lcom/google/android/apps/chrome/services/GoogleServicesStates;)V
    .locals 3

    .prologue
    .line 487
    sget-boolean v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->hasSync()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 488
    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->storeSyncStatePreferences(Lcom/google/android/apps/chrome/services/GoogleServicesStates;)V

    .line 489
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/invalidation/InvalidationController;

    move-result-object v0

    .line 490
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->isSyncEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 491
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    invoke-virtual {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->ensureGcmIsInitialized()V

    .line 492
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isMasterSyncAutomaticallyEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 493
    const-string/jumbo v1, "GoogleServicesManager"

    const-string/jumbo v2, "Enabling sync"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->start()V

    .line 495
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->enableSync()V

    .line 496
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->enableAndroidSync(Landroid/accounts/Account;)V

    .line 509
    :goto_0
    return-void

    .line 498
    :cond_1
    const-string/jumbo v0, "GoogleServicesManager"

    const-string/jumbo v1, "Unable to enable sync, since master sync is disabled. Displaying error notification."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    invoke-static {}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->getInstance()Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->displayAndroidMasterSyncDisabledNotification()V

    goto :goto_0

    .line 504
    :cond_2
    const-string/jumbo v1, "GoogleServicesManager"

    const-string/jumbo v2, "Disabling sync"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->stop()V

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->disableSync()V

    .line 507
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->disableAndroidSync(Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method private setupSessionSyncId()V
    .locals 3

    .prologue
    .line 538
    const-string/jumbo v0, "GSERVICES_ANDROID_ID"

    new-instance v1, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lorg/chromium/chrome/browser/identity/UniqueIdentificationGeneratorFactory;->registerGenerator(Ljava/lang/String;Lorg/chromium/chrome/browser/identity/UniqueIdentificationGenerator;Z)V

    .line 543
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    const-string/jumbo v1, "GSERVICES_ANDROID_ID"

    invoke-static {v1}, Lorg/chromium/chrome/browser/identity/UniqueIdentificationGeneratorFactory;->getInstance(Ljava/lang/String;)Lorg/chromium/chrome/browser/identity/UniqueIdentificationGenerator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->setSessionsId(Lorg/chromium/chrome/browser/identity/UniqueIdentificationGenerator;)V

    .line 545
    return-void
.end method

.method private storeSyncStatePreferences(Lcom/google/android/apps/chrome/services/GoogleServicesStates;)V
    .locals 3

    .prologue
    .line 512
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->hasWantedSyncState()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->isWantedSyncStateEnabled()Z

    move-result v0

    .line 515
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 517
    const-string/jumbo v2, "prefs_sync_wanted_state"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 519
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->hasMasterSyncState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    const-string/jumbo v0, "prefs_sync_master_previous_state"

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->isMasterSyncStateEnabled()Z

    move-result v2

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 523
    :cond_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 524
    return-void

    .line 512
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->isSyncEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public static updateAccountRenameData(Landroid/content/Context;Lcom/google/android/apps/chrome/services/GoogleServicesManager$AccountChangeEventChecker;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 646
    invoke-static {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->getLastKnownAccountName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 649
    if-nez v5, :cond_1

    .line 692
    :cond_0
    :goto_0
    return-void

    .line 654
    :cond_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "prefs_sync_account_rename_event_index"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    move v1, v2

    move-object v4, v5

    .line 661
    :goto_1
    :try_start_0
    invoke-interface {p1, p0, v1, v4}, Lcom/google/android/apps/chrome/services/GoogleServicesManager$AccountChangeEventChecker;->getAccountChangeEvents(Landroid/content/Context;ILjava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 664
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 665
    if-eqz v0, :cond_2

    move v1, v3

    move-object v4, v0

    .line 670
    goto :goto_1

    .line 676
    :cond_3
    invoke-interface {v6}, Ljava/util/List;->size()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 683
    :goto_2
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 684
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v3, "prefs_sync_account_renamed"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 688
    :cond_4
    if-eq v0, v2, :cond_0

    .line 689
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "prefs_sync_account_rename_event_index"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 679
    :catch_0
    move-exception v0

    .line 680
    const-string/jumbo v3, "GoogleServicesManager"

    const-string/jumbo v6, "Error while looking for rename events."

    invoke-static {v3, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    goto :goto_2
.end method

.method public static updateAccountRenameDataAsync(Landroid/content/Context;Lcom/google/android/apps/chrome/services/GoogleServicesManager$AccountChangeEventChecker;Landroid/content/BroadcastReceiver;Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 615
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 616
    new-instance v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$6;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/services/GoogleServicesManager$6;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/services/GoogleServicesManager$AccountChangeEventChecker;Landroid/content/BroadcastReceiver;Landroid/content/Context;Landroid/content/Intent;)V

    .line 628
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 629
    return-void
.end method


# virtual methods
.method public onApplicationStateChange(I)V
    .locals 1

    .prologue
    .line 709
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 710
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->onMainActivityStart()V

    .line 712
    :cond_0
    return-void
.end method

.method public onMainActivityStart()V
    .locals 2

    .prologue
    .line 179
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 180
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mFirstActivityStarted:Z

    if-nez v0, :cond_0

    .line 181
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->onFirstStart()V

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->checkAndClearAccountsChangedPref(Landroid/content/Context;)Z

    move-result v0

    .line 184
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->validateAccountSettings(Z)V

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isFirstSetupInProgress()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->setSyncSetupCompleted()V

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getDelaySync()Z

    move-result v0

    if-nez v0, :cond_1

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->setSetupInProgress(Z)V

    .line 191
    :cond_1
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 192
    return-void
.end method

.method public setStates(Lcom/google/android/apps/chrome/services/GoogleServicesStates;)V
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->setStates(Lcom/google/android/apps/chrome/services/GoogleServicesStates;Landroid/accounts/Account;)V

    .line 419
    return-void
.end method

.method public setStates(Lcom/google/android/apps/chrome/services/GoogleServicesStates;Landroid/accounts/Account;)V
    .locals 1

    .prologue
    .line 422
    invoke-direct {p0, p2, p1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->isStateChangeValid(Landroid/accounts/Account;Lcom/google/android/apps/chrome/services/GoogleServicesStates;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 439
    :goto_0
    return-void

    .line 425
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$4;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/chrome/services/GoogleServicesManager$4;-><init>(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Lcom/google/android/apps/chrome/services/GoogleServicesStates;Landroid/accounts/Account;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public signOut(Landroid/app/Activity;Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    .line 377
    const-string/jumbo v1, "GoogleServicesManager"

    const-string/jumbo v2, "Signing user out of Chrome"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    invoke-static {}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->create()Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v1

    .line 379
    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v2, v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled(Landroid/accounts/Account;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 380
    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->sync(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    .line 382
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/chrome/snapshot/ChromeToMobileFeature;->isEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 383
    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->chromeToMobile(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    .line 385
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->isAutoLoginEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 386
    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->autoLogin(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    .line 388
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->build()Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->setStates(Lcom/google/android/apps/chrome/services/GoogleServicesStates;Landroid/accounts/Account;)V

    .line 390
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSigninManager:Lorg/chromium/chrome/browser/signin/SigninManager;

    invoke-virtual {v0, p1, p2}, Lorg/chromium/chrome/browser/signin/SigninManager;->signOut(Landroid/app/Activity;Ljava/lang/Runnable;)V

    .line 391
    return-void
.end method

.method public validateAccountSettings(Z)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    .line 217
    if-nez v0, :cond_5

    .line 218
    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->getAndroidSigninPromoExperimentGroup()I

    move-result v0

    if-gez v0, :cond_1

    .line 302
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getSyncLastAccountName()Ljava/lang/String;

    move-result-object v0

    .line 223
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 226
    const-string/jumbo v1, "prefs_sync_android_accounts"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    .line 228
    if-eqz v1, :cond_3

    if-eqz p1, :cond_0

    .line 230
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccountNames()Ljava/util/List;

    move-result-object v2

    .line 232
    if-eqz v1, :cond_4

    .line 233
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v1

    .line 235
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getSigninPromoShown()Z

    move-result v3

    if-nez v3, :cond_4

    .line 236
    const-string/jumbo v3, "prefs_sync_android_accounts"

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v3

    .line 238
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 239
    invoke-interface {v4, v3}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 240
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 241
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setShowSigninPromo(Z)V

    .line 246
    :cond_4
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "prefs_sync_android_accounts"

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 251
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->getNewSignedInAccountName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 252
    if-eqz p1, :cond_6

    if-eqz v1, :cond_6

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->handleAccountRename(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 259
    :cond_6
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->accountExists(Landroid/accounts/Account;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 263
    new-instance v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager$1;-><init>(Lcom/google/android/apps/chrome/services/GoogleServicesManager;)V

    .line 280
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 284
    :cond_7
    if-eqz p1, :cond_8

    .line 287
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mOAuth2TokenService:Lorg/chromium/chrome/browser/signin/OAuth2TokenService;

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v3}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->validateAccounts(Landroid/content/Context;Z)V

    .line 290
    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v1, v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled(Landroid/accounts/Account;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 291
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->hasSyncSetupCompleted()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 292
    if-eqz p1, :cond_0

    .line 294
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->requestSyncFromNativeChromeForAllTypes()V

    goto/16 :goto_0

    .line 299
    :cond_9
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->syncSignIn(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
