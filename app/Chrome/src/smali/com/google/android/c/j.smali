.class public final Lcom/google/android/c/j;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "SsbStubProto.java"


# static fields
.field private static volatile f:[Lcom/google/android/c/j;


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field private g:Ljava/lang/Long;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 708
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 709
    iput-object v0, p0, Lcom/google/android/c/j;->g:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/c/j;->a:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/c/j;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/c/j;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/c/j;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/c/j;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/c/j;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/c/j;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/c/j;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/j;->cachedSize:I

    .line 710
    return-void
.end method

.method public static a()[Lcom/google/android/c/j;
    .locals 2

    .prologue
    .line 673
    sget-object v0, Lcom/google/android/c/j;->f:[Lcom/google/android/c/j;

    if-nez v0, :cond_1

    .line 674
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 676
    :try_start_0
    sget-object v0, Lcom/google/android/c/j;->f:[Lcom/google/android/c/j;

    if-nez v0, :cond_0

    .line 677
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/j;

    sput-object v0, Lcom/google/android/c/j;->f:[Lcom/google/android/c/j;

    .line 679
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 681
    :cond_1
    sget-object v0, Lcom/google/android/c/j;->f:[Lcom/google/android/c/j;

    return-object v0

    .line 679
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 758
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 759
    iget-object v1, p0, Lcom/google/android/c/j;->g:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 760
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/j;->g:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 763
    :cond_0
    iget-object v1, p0, Lcom/google/android/c/j;->a:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 764
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/c/j;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 767
    :cond_1
    iget-object v1, p0, Lcom/google/android/c/j;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 768
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/c/j;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 771
    :cond_2
    iget-object v1, p0, Lcom/google/android/c/j;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 772
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/c/j;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 775
    :cond_3
    iget-object v1, p0, Lcom/google/android/c/j;->d:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 776
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/c/j;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 779
    :cond_4
    iget-object v1, p0, Lcom/google/android/c/j;->h:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 780
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/c/j;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 783
    :cond_5
    iget-object v1, p0, Lcom/google/android/c/j;->i:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 784
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/c/j;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 787
    :cond_6
    iget-object v1, p0, Lcom/google/android/c/j;->e:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 788
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/c/j;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 791
    :cond_7
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 2

    .prologue
    .line 667
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/c/j;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/j;->g:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/j;->a:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/j;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/j;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/j;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/j;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/j;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/j;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4

    .prologue
    .line 729
    iget-object v0, p0, Lcom/google/android/c/j;->g:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 730
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/j;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IJ)V

    .line 732
    :cond_0
    iget-object v0, p0, Lcom/google/android/c/j;->a:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 733
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/c/j;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IJ)V

    .line 735
    :cond_1
    iget-object v0, p0, Lcom/google/android/c/j;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 736
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/c/j;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    .line 738
    :cond_2
    iget-object v0, p0, Lcom/google/android/c/j;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 739
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/c/j;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    .line 741
    :cond_3
    iget-object v0, p0, Lcom/google/android/c/j;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 742
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/c/j;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    .line 744
    :cond_4
    iget-object v0, p0, Lcom/google/android/c/j;->h:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 745
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/c/j;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    .line 747
    :cond_5
    iget-object v0, p0, Lcom/google/android/c/j;->i:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 748
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/c/j;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    .line 750
    :cond_6
    iget-object v0, p0, Lcom/google/android/c/j;->e:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 751
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/c/j;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    .line 753
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 754
    return-void
.end method
