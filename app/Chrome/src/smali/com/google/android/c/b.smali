.class public final Lcom/google/android/c/b;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "SsbStubProto.java"


# instance fields
.field private a:Lcom/google/android/c/e;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1031
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 1032
    iput-object v0, p0, Lcom/google/android/c/b;->a:Lcom/google/android/c/e;

    iput-object v0, p0, Lcom/google/android/c/b;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b;->cachedSize:I

    .line 1033
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1053
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 1054
    iget-object v1, p0, Lcom/google/android/c/b;->a:Lcom/google/android/c/e;

    if-eqz v1, :cond_0

    .line 1055
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/b;->a:Lcom/google/android/c/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1058
    :cond_0
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1

    .prologue
    .line 1011
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/c/b;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/c/b;->a:Lcom/google/android/c/e;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/c/e;

    invoke-direct {v0}, Lcom/google/android/c/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b;->a:Lcom/google/android/c/e;

    :cond_1
    iget-object v0, p0, Lcom/google/android/c/b;->a:Lcom/google/android/c/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    .prologue
    .line 1045
    iget-object v0, p0, Lcom/google/android/c/b;->a:Lcom/google/android/c/e;

    if-eqz v0, :cond_0

    .line 1046
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/b;->a:Lcom/google/android/c/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1048
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1049
    return-void
.end method
