.class public final Lcom/google/android/c/k;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "SsbStubProto.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:[Lcom/google/android/c/i;

.field private d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 414
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 415
    iput-object v1, p0, Lcom/google/android/c/k;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/c/k;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/c/k;->d:Ljava/lang/Boolean;

    invoke-static {}, Lcom/google/android/c/i;->a()[Lcom/google/android/c/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/k;->c:[Lcom/google/android/c/i;

    iput-object v1, p0, Lcom/google/android/c/k;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/k;->cachedSize:I

    .line 416
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 453
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 454
    iget-object v1, p0, Lcom/google/android/c/k;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 455
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/k;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 458
    :cond_0
    iget-object v1, p0, Lcom/google/android/c/k;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 459
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/c/k;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 462
    :cond_1
    iget-object v1, p0, Lcom/google/android/c/k;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 463
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/c/k;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 466
    :cond_2
    iget-object v1, p0, Lcom/google/android/c/k;->c:[Lcom/google/android/c/i;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/c/k;->c:[Lcom/google/android/c/i;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 467
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/android/c/k;->c:[Lcom/google/android/c/i;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 468
    iget-object v2, p0, Lcom/google/android/c/k;->c:[Lcom/google/android/c/i;

    aget-object v2, v2, v0

    .line 469
    if-eqz v2, :cond_3

    .line 470
    const/4 v3, 0x4

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 467
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 475
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 385
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/c/k;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/k;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/k;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/k;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->a(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/k;->c:[Lcom/google/android/c/i;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/c/i;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/c/k;->c:[Lcom/google/android/c/i;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/c/i;

    invoke-direct {v3}, Lcom/google/android/c/i;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/k;->c:[Lcom/google/android/c/i;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/c/i;

    invoke-direct {v3}, Lcom/google/android/c/i;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    iput-object v2, p0, Lcom/google/android/c/k;->c:[Lcom/google/android/c/i;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3

    .prologue
    .line 431
    iget-object v0, p0, Lcom/google/android/c/k;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 432
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/k;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    .line 434
    :cond_0
    iget-object v0, p0, Lcom/google/android/c/k;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 435
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/c/k;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    .line 437
    :cond_1
    iget-object v0, p0, Lcom/google/android/c/k;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 438
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/c/k;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    .line 440
    :cond_2
    iget-object v0, p0, Lcom/google/android/c/k;->c:[Lcom/google/android/c/i;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/c/k;->c:[Lcom/google/android/c/i;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 441
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/c/k;->c:[Lcom/google/android/c/i;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 442
    iget-object v1, p0, Lcom/google/android/c/k;->c:[Lcom/google/android/c/i;

    aget-object v1, v1, v0

    .line 443
    if-eqz v1, :cond_3

    .line 444
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 441
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 448
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 449
    return-void
.end method
