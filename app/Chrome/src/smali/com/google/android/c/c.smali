.class public final Lcom/google/android/c/c;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "SsbStubProto.java"


# instance fields
.field public a:Lcom/google/android/c/a;

.field public b:Ljava/lang/String;

.field private c:Ljava/lang/Long;

.field private d:Lcom/google/android/c/b;

.field private e:Lcom/google/android/c/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 889
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 890
    iput-object v0, p0, Lcom/google/android/c/c;->c:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/c/c;->d:Lcom/google/android/c/b;

    iput-object v0, p0, Lcom/google/android/c/c;->a:Lcom/google/android/c/a;

    iput-object v0, p0, Lcom/google/android/c/c;->e:Lcom/google/android/c/d;

    iput-object v0, p0, Lcom/google/android/c/c;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/c/c;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/c;->cachedSize:I

    .line 891
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 927
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 928
    iget-object v1, p0, Lcom/google/android/c/c;->d:Lcom/google/android/c/b;

    if-eqz v1, :cond_0

    .line 929
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/c/c;->d:Lcom/google/android/c/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 932
    :cond_0
    iget-object v1, p0, Lcom/google/android/c/c;->a:Lcom/google/android/c/a;

    if-eqz v1, :cond_1

    .line 933
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/c/c;->a:Lcom/google/android/c/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 936
    :cond_1
    iget-object v1, p0, Lcom/google/android/c/c;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 937
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/c/c;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 940
    :cond_2
    iget-object v1, p0, Lcom/google/android/c/c;->e:Lcom/google/android/c/d;

    if-eqz v1, :cond_3

    .line 941
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/c/c;->e:Lcom/google/android/c/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 944
    :cond_3
    iget-object v1, p0, Lcom/google/android/c/c;->b:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 945
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/c/c;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 948
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 2

    .prologue
    .line 857
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/c/c;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/c/c;->d:Lcom/google/android/c/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/c/b;

    invoke-direct {v0}, Lcom/google/android/c/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/c;->d:Lcom/google/android/c/b;

    :cond_1
    iget-object v0, p0, Lcom/google/android/c/c;->d:Lcom/google/android/c/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/c/c;->a:Lcom/google/android/c/a;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/c/a;

    invoke-direct {v0}, Lcom/google/android/c/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/c;->a:Lcom/google/android/c/a;

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/c;->a:Lcom/google/android/c/a;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/c;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/c/c;->e:Lcom/google/android/c/d;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/c/d;

    invoke-direct {v0}, Lcom/google/android/c/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/c;->e:Lcom/google/android/c/d;

    :cond_3
    iget-object v0, p0, Lcom/google/android/c/c;->e:Lcom/google/android/c/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/c;->b:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2a -> :sswitch_1
        0x32 -> :sswitch_2
        0x38 -> :sswitch_3
        0x4a -> :sswitch_4
        0x52 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4

    .prologue
    .line 907
    iget-object v0, p0, Lcom/google/android/c/c;->d:Lcom/google/android/c/b;

    if-eqz v0, :cond_0

    .line 908
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/c/c;->d:Lcom/google/android/c/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 910
    :cond_0
    iget-object v0, p0, Lcom/google/android/c/c;->a:Lcom/google/android/c/a;

    if-eqz v0, :cond_1

    .line 911
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/c/c;->a:Lcom/google/android/c/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 913
    :cond_1
    iget-object v0, p0, Lcom/google/android/c/c;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 914
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/c/c;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IJ)V

    .line 916
    :cond_2
    iget-object v0, p0, Lcom/google/android/c/c;->e:Lcom/google/android/c/d;

    if-eqz v0, :cond_3

    .line 917
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/c/c;->e:Lcom/google/android/c/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 919
    :cond_3
    iget-object v0, p0, Lcom/google/android/c/c;->b:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 920
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/c/c;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    .line 922
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 923
    return-void
.end method
