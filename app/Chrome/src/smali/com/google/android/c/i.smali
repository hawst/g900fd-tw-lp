.class public final Lcom/google/android/c/i;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "SsbStubProto.java"


# static fields
.field private static volatile c:[Lcom/google/android/c/i;


# instance fields
.field public a:Ljava/lang/Long;

.field public b:[Lcom/google/android/c/j;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 564
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 565
    iput-object v1, p0, Lcom/google/android/c/i;->a:Ljava/lang/Long;

    invoke-static {}, Lcom/google/android/c/j;->a()[Lcom/google/android/c/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/i;->b:[Lcom/google/android/c/j;

    iput-object v1, p0, Lcom/google/android/c/i;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/i;->cachedSize:I

    .line 566
    return-void
.end method

.method public static a()[Lcom/google/android/c/i;
    .locals 2

    .prologue
    .line 547
    sget-object v0, Lcom/google/android/c/i;->c:[Lcom/google/android/c/i;

    if-nez v0, :cond_1

    .line 548
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 550
    :try_start_0
    sget-object v0, Lcom/google/android/c/i;->c:[Lcom/google/android/c/i;

    if-nez v0, :cond_0

    .line 551
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/i;

    sput-object v0, Lcom/google/android/c/i;->c:[Lcom/google/android/c/i;

    .line 553
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 555
    :cond_1
    sget-object v0, Lcom/google/android/c/i;->c:[Lcom/google/android/c/i;

    return-object v0

    .line 553
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 595
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 596
    iget-object v1, p0, Lcom/google/android/c/i;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 597
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/i;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 600
    :cond_0
    iget-object v1, p0, Lcom/google/android/c/i;->b:[Lcom/google/android/c/j;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/c/i;->b:[Lcom/google/android/c/j;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 601
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/android/c/i;->b:[Lcom/google/android/c/j;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 602
    iget-object v2, p0, Lcom/google/android/c/i;->b:[Lcom/google/android/c/j;

    aget-object v2, v2, v0

    .line 603
    if-eqz v2, :cond_1

    .line 604
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 601
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 609
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 541
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/c/i;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/i;->a:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->a(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/i;->b:[Lcom/google/android/c/j;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/c/j;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/c/i;->b:[Lcom/google/android/c/j;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/c/j;

    invoke-direct {v3}, Lcom/google/android/c/j;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/i;->b:[Lcom/google/android/c/j;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/c/j;

    invoke-direct {v3}, Lcom/google/android/c/j;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    iput-object v2, p0, Lcom/google/android/c/i;->b:[Lcom/google/android/c/j;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4

    .prologue
    .line 579
    iget-object v0, p0, Lcom/google/android/c/i;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 580
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/i;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IJ)V

    .line 582
    :cond_0
    iget-object v0, p0, Lcom/google/android/c/i;->b:[Lcom/google/android/c/j;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/c/i;->b:[Lcom/google/android/c/j;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 583
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/c/i;->b:[Lcom/google/android/c/j;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 584
    iget-object v1, p0, Lcom/google/android/c/i;->b:[Lcom/google/android/c/j;

    aget-object v1, v1, v0

    .line 585
    if-eqz v1, :cond_1

    .line 586
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 583
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 590
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 591
    return-void
.end method
