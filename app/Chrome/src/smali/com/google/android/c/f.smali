.class public final Lcom/google/android/c/f;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "SsbStubProto.java"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Lcom/google/android/c/c;

.field public c:Lcom/google/android/c/k;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 278
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 279
    iput-object v0, p0, Lcom/google/android/c/f;->a:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/c/f;->b:Lcom/google/android/c/c;

    iput-object v0, p0, Lcom/google/android/c/f;->c:Lcom/google/android/c/k;

    iput-object v0, p0, Lcom/google/android/c/f;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/c/f;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/f;->cachedSize:I

    .line 280
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 312
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 313
    iget-object v1, p0, Lcom/google/android/c/f;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 314
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/f;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 317
    :cond_0
    iget-object v1, p0, Lcom/google/android/c/f;->b:Lcom/google/android/c/c;

    if-eqz v1, :cond_1

    .line 318
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/c/f;->b:Lcom/google/android/c/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 321
    :cond_1
    iget-object v1, p0, Lcom/google/android/c/f;->c:Lcom/google/android/c/k;

    if-eqz v1, :cond_2

    .line 322
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/c/f;->c:Lcom/google/android/c/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 325
    :cond_2
    iget-object v1, p0, Lcom/google/android/c/f;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 326
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/c/f;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 329
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 2

    .prologue
    .line 249
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/c/f;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/f;->a:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/c/f;->b:Lcom/google/android/c/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/c/c;

    invoke-direct {v0}, Lcom/google/android/c/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/f;->b:Lcom/google/android/c/c;

    :cond_1
    iget-object v0, p0, Lcom/google/android/c/f;->b:Lcom/google/android/c/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/c/f;->c:Lcom/google/android/c/k;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/c/k;

    invoke-direct {v0}, Lcom/google/android/c/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/f;->c:Lcom/google/android/c/k;

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/f;->c:Lcom/google/android/c/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/f;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/c/f;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 296
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/f;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IJ)V

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/google/android/c/f;->b:Lcom/google/android/c/c;

    if-eqz v0, :cond_1

    .line 299
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/c/f;->b:Lcom/google/android/c/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 301
    :cond_1
    iget-object v0, p0, Lcom/google/android/c/f;->c:Lcom/google/android/c/k;

    if-eqz v0, :cond_2

    .line 302
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/c/f;->c:Lcom/google/android/c/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 304
    :cond_2
    iget-object v0, p0, Lcom/google/android/c/f;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 305
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/c/f;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    .line 307
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 308
    return-void
.end method
