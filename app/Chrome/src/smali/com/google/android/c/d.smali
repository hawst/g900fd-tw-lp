.class public final Lcom/google/android/c/d;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "SsbStubProto.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1339
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 1340
    iput-object v0, p0, Lcom/google/android/c/d;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/c/d;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/c/d;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/c/d;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/d;->cachedSize:I

    .line 1341
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1369
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 1370
    iget-object v1, p0, Lcom/google/android/c/d;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1371
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/c/d;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1374
    :cond_0
    iget-object v1, p0, Lcom/google/android/c/d;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1375
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/c/d;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1378
    :cond_1
    iget-object v1, p0, Lcom/google/android/c/d;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1379
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/c/d;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1382
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1

    .prologue
    .line 1313
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/c/d;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/d;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/d;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/d;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    .prologue
    .line 1355
    iget-object v0, p0, Lcom/google/android/c/d;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1356
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/c/d;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    .line 1358
    :cond_0
    iget-object v0, p0, Lcom/google/android/c/d;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1359
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/c/d;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    .line 1361
    :cond_1
    iget-object v0, p0, Lcom/google/android/c/d;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1362
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/c/d;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    .line 1364
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1365
    return-void
.end method
