.class public final Lcom/google/ipc/invalidation/ticl/a/ak;
.super Lcom/google/ipc/invalidation/b/n;
.source "ClientProtocol.java"


# static fields
.field public static final a:Lcom/google/ipc/invalidation/ticl/a/ak;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2285
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/ak;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/ticl/a/ak;-><init>()V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/ak;->a:Lcom/google/ipc/invalidation/ticl/a/ak;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2288
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 2289
    return-void
.end method

.method static a(Lcom/google/b/a/a/af;)Lcom/google/ipc/invalidation/ticl/a/ak;
    .locals 1

    .prologue
    .line 2320
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 2321
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/ak;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/ticl/a/ak;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 1

    .prologue
    .line 2305
    const-string/jumbo v0, "<RegistrationSyncRequestMessage:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 2306
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 2307
    return-void
.end method

.method protected final b()I
    .locals 1

    .prologue
    .line 2300
    const/4 v0, 0x1

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2293
    if-ne p0, p1, :cond_1

    .line 2295
    :cond_0
    :goto_0
    return v0

    .line 2294
    :cond_1
    instance-of v1, p1, Lcom/google/ipc/invalidation/ticl/a/ak;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method
