.class public Lcom/google/ipc/invalidation/ticl/android2/a;
.super Ljava/lang/Object;
.source "ResourcesFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(ILcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/ticl/a/L;Z)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 149
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/ipc/invalidation/ticl/a/r;->a(ILcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/ticl/a/L;Z)Lcom/google/ipc/invalidation/ticl/a/r;

    move-result-object v0

    .line 151
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 152
    const-string/jumbo v2, "ipcinv-internal-downcall"

    sget-object v3, Lcom/google/ipc/invalidation/ticl/android2/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    invoke-static {v3, v0}, Lcom/google/ipc/invalidation/ticl/a/q;->a(Lcom/google/ipc/invalidation/ticl/a/ap;Lcom/google/ipc/invalidation/ticl/a/r;)Lcom/google/ipc/invalidation/ticl/a/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/q;->f()[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 154
    return-object v1
.end method

.method public static a(Lcom/google/ipc/invalidation/b/c;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 126
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 127
    const-string/jumbo v1, "ipcinv-internal-downcall"

    sget-object v2, Lcom/google/ipc/invalidation/ticl/android2/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    invoke-static {p0}, Lcom/google/ipc/invalidation/ticl/a/t;->a(Lcom/google/ipc/invalidation/b/c;)Lcom/google/ipc/invalidation/ticl/a/t;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/q;->a(Lcom/google/ipc/invalidation/ticl/a/ap;Lcom/google/ipc/invalidation/ticl/a/t;)Lcom/google/ipc/invalidation/ticl/a/q;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/q;->f()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 130
    return-object v0
.end method

.method public static a(Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 226
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 227
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;->getErrorReason()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;->getErrorMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;->isTransient()Z

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/google/ipc/invalidation/ticl/a/v;->a(ILjava/lang/String;Z)Lcom/google/ipc/invalidation/ticl/a/v;

    move-result-object v1

    .line 229
    const-string/jumbo v2, "ipcinv-upcall"

    sget-object v3, Lcom/google/ipc/invalidation/ticl/android2/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    invoke-static {v3, v1}, Lcom/google/ipc/invalidation/ticl/a/u;->a(Lcom/google/ipc/invalidation/ticl/a/ap;Lcom/google/ipc/invalidation/ticl/a/v;)Lcom/google/ipc/invalidation/ticl/a/u;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/u;->h()[B

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 231
    return-object v0
.end method

.method public static a(Ljava/util/Collection;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 101
    invoke-static {p0}, Lcom/google/ipc/invalidation/ticl/a/n;->a(Ljava/util/Collection;)Lcom/google/ipc/invalidation/ticl/a/n;

    move-result-object v0

    .line 103
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 104
    const-string/jumbo v2, "ipcinv-downcall"

    sget-object v3, Lcom/google/ipc/invalidation/ticl/android2/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    invoke-static {v3, v0}, Lcom/google/ipc/invalidation/ticl/a/l;->a(Lcom/google/ipc/invalidation/ticl/a/ap;Lcom/google/ipc/invalidation/ticl/a/n;)Lcom/google/ipc/invalidation/ticl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->f()[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 106
    return-object v1
.end method

.method public static b()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 141
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 142
    const-string/jumbo v1, "ipcinv-internal-downcall"

    sget-object v2, Lcom/google/ipc/invalidation/ticl/android2/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/q;->a(Lcom/google/ipc/invalidation/ticl/a/ap;Z)Lcom/google/ipc/invalidation/ticl/a/q;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/q;->f()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 144
    return-object v0
.end method

.method public static b(Ljava/util/Collection;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 110
    invoke-static {p0}, Lcom/google/ipc/invalidation/ticl/a/n;->b(Ljava/util/Collection;)Lcom/google/ipc/invalidation/ticl/a/n;

    move-result-object v0

    .line 112
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 113
    const-string/jumbo v2, "ipcinv-downcall"

    sget-object v3, Lcom/google/ipc/invalidation/ticl/android2/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    invoke-static {v3, v0}, Lcom/google/ipc/invalidation/ticl/a/l;->a(Lcom/google/ipc/invalidation/ticl/a/ap;Lcom/google/ipc/invalidation/ticl/a/n;)Lcom/google/ipc/invalidation/ticl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->f()[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 115
    return-object v1
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 30
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method
