.class public final Lcom/google/ipc/invalidation/ticl/a/X;
.super Ljava/lang/Object;
.source "ClientProtocol.java"


# instance fields
.field public a:Lcom/google/ipc/invalidation/b/c;

.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/Boolean;

.field private d:Lcom/google/ipc/invalidation/ticl/a/Y;

.field private e:Z

.field private f:J


# direct methods
.method public constructor <init>(Lcom/google/ipc/invalidation/ticl/a/Y;ZJ)V
    .locals 1

    .prologue
    .line 517
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 518
    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/X;->d:Lcom/google/ipc/invalidation/ticl/a/Y;

    iput-boolean p2, p0, Lcom/google/ipc/invalidation/ticl/a/X;->e:Z

    iput-wide p3, p0, Lcom/google/ipc/invalidation/ticl/a/X;->f:J

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/ipc/invalidation/ticl/a/W;
    .locals 8

    .prologue
    .line 521
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/W;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/X;->d:Lcom/google/ipc/invalidation/ticl/a/Y;

    iget-boolean v2, p0, Lcom/google/ipc/invalidation/ticl/a/X;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/ipc/invalidation/ticl/a/X;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/a/X;->a:Lcom/google/ipc/invalidation/b/c;

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/a/X;->b:Ljava/lang/Long;

    iget-object v6, p0, Lcom/google/ipc/invalidation/ticl/a/X;->c:Ljava/lang/Boolean;

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/ipc/invalidation/ticl/a/W;-><init>(Lcom/google/ipc/invalidation/ticl/a/Y;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Long;Ljava/lang/Boolean;B)V

    return-object v0
.end method
