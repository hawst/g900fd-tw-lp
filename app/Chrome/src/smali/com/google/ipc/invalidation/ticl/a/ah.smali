.class public final Lcom/google/ipc/invalidation/ticl/a/ah;
.super Lcom/google/ipc/invalidation/b/n;
.source "ClientProtocol.java"


# instance fields
.field private final a:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1391
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/ah;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/ah;-><init>(Ljava/util/Collection;)V

    return-void
.end method

.method private constructor <init>(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 1395
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 1396
    const-string/jumbo v0, "registered_object"

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/a/ah;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/ah;->a:Ljava/util/List;

    .line 1397
    return-void
.end method

.method static a(Lcom/google/b/a/a/ac;)Lcom/google/ipc/invalidation/ticl/a/ah;
    .locals 3

    .prologue
    .line 1431
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 1436
    :goto_0
    return-object v0

    .line 1432
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    array-length v0, v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1433
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1434
    iget-object v2, p0, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    aget-object v2, v2, v0

    invoke-static {v2}, Lcom/google/ipc/invalidation/ticl/a/Y;->a(Lcom/google/b/a/a/T;)Lcom/google/ipc/invalidation/ticl/a/Y;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1433
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1436
    :cond_1
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/ah;

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/ah;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;)Lcom/google/ipc/invalidation/ticl/a/ah;
    .locals 1

    .prologue
    .line 1388
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/ah;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/ticl/a/ah;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method


# virtual methods
.method final a()Lcom/google/b/a/a/ac;
    .locals 4

    .prologue
    .line 1444
    new-instance v2, Lcom/google/b/a/a/ac;

    invoke-direct {v2}, Lcom/google/b/a/a/ac;-><init>()V

    .line 1445
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/ah;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/b/a/a/T;

    iput-object v0, v2, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    .line 1446
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, v2, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 1447
    iget-object v3, v2, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/ah;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/Y;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/Y;->d()Lcom/google/b/a/a/T;

    move-result-object v0

    aput-object v0, v3, v1

    .line 1446
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1449
    :cond_0
    return-object v2
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 2

    .prologue
    .line 1415
    const-string/jumbo v0, "<RegistrationSubtree:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 1416
    const-string/jumbo v0, " registered_object=["

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/ah;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/Iterable;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 1417
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 1418
    return-void
.end method

.method protected final b()I
    .locals 1

    .prologue
    .line 1409
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/ah;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 1411
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1402
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    .line 1405
    :goto_0
    return v0

    .line 1403
    :cond_0
    instance-of v0, p1, Lcom/google/ipc/invalidation/ticl/a/ah;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1404
    :cond_1
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/ah;

    .line 1405
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/ah;->a:Ljava/util/List;

    iget-object v1, p1, Lcom/google/ipc/invalidation/ticl/a/ah;->a:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/ah;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
