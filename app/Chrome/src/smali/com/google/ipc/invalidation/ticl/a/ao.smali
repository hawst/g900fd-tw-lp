.class public final Lcom/google/ipc/invalidation/ticl/a/ao;
.super Lcom/google/ipc/invalidation/b/n;
.source "ClientProtocol.java"


# static fields
.field public static final a:Lcom/google/ipc/invalidation/ticl/a/ao;


# instance fields
.field private final b:J

.field private final c:Lcom/google/ipc/invalidation/b/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2075
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/ao;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/ao;-><init>(Lcom/google/ipc/invalidation/b/c;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/ao;->a:Lcom/google/ipc/invalidation/ticl/a/ao;

    return-void
.end method

.method private constructor <init>(Lcom/google/ipc/invalidation/b/c;)V
    .locals 2

    .prologue
    .line 2080
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 2081
    const/4 v0, 0x0

    .line 2082
    if-eqz p1, :cond_0

    .line 2083
    const/4 v0, 0x1

    .line 2084
    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/ao;->c:Lcom/google/ipc/invalidation/b/c;

    .line 2088
    :goto_0
    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/ao;->b:J

    .line 2089
    return-void

    .line 2086
    :cond_0
    sget-object v1, Lcom/google/ipc/invalidation/b/c;->a:Lcom/google/ipc/invalidation/b/c;

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/ao;->c:Lcom/google/ipc/invalidation/b/c;

    goto :goto_0
.end method

.method static a(Lcom/google/b/a/a/aj;)Lcom/google/ipc/invalidation/ticl/a/ao;
    .locals 2

    .prologue
    .line 2129
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 2130
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/ao;

    iget-object v1, p0, Lcom/google/b/a/a/aj;->a:[B

    invoke-static {v1}, Lcom/google/ipc/invalidation/b/c;->a([B)Lcom/google/ipc/invalidation/b/c;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/ao;-><init>(Lcom/google/ipc/invalidation/b/c;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/ipc/invalidation/b/c;
    .locals 1

    .prologue
    .line 2091
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/ao;->c:Lcom/google/ipc/invalidation/b/c;

    return-object v0
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 2

    .prologue
    .line 2111
    const-string/jumbo v0, "<TokenControlMessage:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 2112
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/ao;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2113
    const-string/jumbo v0, " new_token="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/ao;->c:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 2115
    :cond_0
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 2116
    return-void
.end method

.method protected final b()I
    .locals 4

    .prologue
    .line 2103
    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/ao;->b:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 2104
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/ao;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2105
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/ao;->c:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2107
    :cond_0
    return v0
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 2092
    const-wide/16 v0, 0x1

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/ao;->b:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2095
    if-ne p0, p1, :cond_1

    .line 2098
    :cond_0
    :goto_0
    return v0

    .line 2096
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/ticl/a/ao;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 2097
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/ao;

    .line 2098
    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/ao;->b:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/ao;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/ao;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/ao;->c:Lcom/google/ipc/invalidation/b/c;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/ao;->c:Lcom/google/ipc/invalidation/b/c;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/ao;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
