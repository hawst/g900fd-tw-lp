.class public final Lcom/google/ipc/invalidation/ticl/a/O;
.super Lcom/google/ipc/invalidation/b/n;
.source "ClientProtocol.java"


# instance fields
.field private final a:J

.field private final b:Lcom/google/ipc/invalidation/ticl/a/N;

.field private final c:Lcom/google/ipc/invalidation/ticl/a/U;

.field private final d:Lcom/google/ipc/invalidation/ticl/a/ad;

.field private final e:Lcom/google/ipc/invalidation/ticl/a/aj;

.field private final f:Lcom/google/ipc/invalidation/ticl/a/V;

.field private final g:Lcom/google/ipc/invalidation/ticl/a/S;


# direct methods
.method private constructor <init>(Lcom/google/ipc/invalidation/ticl/a/N;Lcom/google/ipc/invalidation/ticl/a/U;Lcom/google/ipc/invalidation/ticl/a/ad;Lcom/google/ipc/invalidation/ticl/a/aj;Lcom/google/ipc/invalidation/ticl/a/V;Lcom/google/ipc/invalidation/ticl/a/S;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1032
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 1034
    const-string/jumbo v0, "header"

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/a/O;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1035
    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/O;->b:Lcom/google/ipc/invalidation/ticl/a/N;

    .line 1036
    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/a/O;->c:Lcom/google/ipc/invalidation/ticl/a/U;

    .line 1037
    iput-object p3, p0, Lcom/google/ipc/invalidation/ticl/a/O;->d:Lcom/google/ipc/invalidation/ticl/a/ad;

    .line 1038
    iput-object p4, p0, Lcom/google/ipc/invalidation/ticl/a/O;->e:Lcom/google/ipc/invalidation/ticl/a/aj;

    .line 1039
    iput-object p5, p0, Lcom/google/ipc/invalidation/ticl/a/O;->f:Lcom/google/ipc/invalidation/ticl/a/V;

    .line 1040
    if-eqz p6, :cond_0

    .line 1042
    iput-object p6, p0, Lcom/google/ipc/invalidation/ticl/a/O;->g:Lcom/google/ipc/invalidation/ticl/a/S;

    move v0, v1

    .line 1046
    :goto_0
    int-to-long v4, v0

    iput-wide v4, p0, Lcom/google/ipc/invalidation/ticl/a/O;->a:J

    .line 1047
    if-eqz p2, :cond_1

    :goto_1
    invoke-virtual {p1}, Lcom/google/ipc/invalidation/ticl/a/N;->a()Z

    move-result v0

    xor-int/2addr v0, v1

    const-string/jumbo v1, "There should either be a client token or an initialization request"

    invoke-virtual {p0, v0, v1}, Lcom/google/ipc/invalidation/ticl/a/O;->a(ZLjava/lang/String;)V

    .line 1049
    return-void

    .line 1044
    :cond_0
    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/S;->a:Lcom/google/ipc/invalidation/ticl/a/S;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/O;->g:Lcom/google/ipc/invalidation/ticl/a/S;

    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    .line 1047
    goto :goto_1
.end method

.method public static a(Lcom/google/ipc/invalidation/ticl/a/N;Lcom/google/ipc/invalidation/ticl/a/U;Lcom/google/ipc/invalidation/ticl/a/ad;Lcom/google/ipc/invalidation/ticl/a/aj;Lcom/google/ipc/invalidation/ticl/a/V;Lcom/google/ipc/invalidation/ticl/a/S;)Lcom/google/ipc/invalidation/ticl/a/O;
    .locals 7

    .prologue
    .line 1016
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/O;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/ipc/invalidation/ticl/a/O;-><init>(Lcom/google/ipc/invalidation/ticl/a/N;Lcom/google/ipc/invalidation/ticl/a/U;Lcom/google/ipc/invalidation/ticl/a/ad;Lcom/google/ipc/invalidation/ticl/a/aj;Lcom/google/ipc/invalidation/ticl/a/V;Lcom/google/ipc/invalidation/ticl/a/S;)V

    return-object v0
.end method

.method private c()Z
    .locals 4

    .prologue
    .line 1062
    const-wide/16 v0, 0x1

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/O;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 2

    .prologue
    .line 1099
    const-string/jumbo v0, "<ClientToServerMessage:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 1100
    const-string/jumbo v0, " header="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/O;->b:Lcom/google/ipc/invalidation/ticl/a/N;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1101
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/O;->c:Lcom/google/ipc/invalidation/ticl/a/U;

    if-eqz v0, :cond_0

    .line 1102
    const-string/jumbo v0, " initialize_message="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/O;->c:Lcom/google/ipc/invalidation/ticl/a/U;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1104
    :cond_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/O;->d:Lcom/google/ipc/invalidation/ticl/a/ad;

    if-eqz v0, :cond_1

    .line 1105
    const-string/jumbo v0, " registration_message="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/O;->d:Lcom/google/ipc/invalidation/ticl/a/ad;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1107
    :cond_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/O;->e:Lcom/google/ipc/invalidation/ticl/a/aj;

    if-eqz v0, :cond_2

    .line 1108
    const-string/jumbo v0, " registration_sync_message="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/O;->e:Lcom/google/ipc/invalidation/ticl/a/aj;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1110
    :cond_2
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/O;->f:Lcom/google/ipc/invalidation/ticl/a/V;

    if-eqz v0, :cond_3

    .line 1111
    const-string/jumbo v0, " invalidation_ack_message="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/O;->f:Lcom/google/ipc/invalidation/ticl/a/V;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1113
    :cond_3
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/O;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1114
    const-string/jumbo v0, " info_message="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/O;->g:Lcom/google/ipc/invalidation/ticl/a/S;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1116
    :cond_4
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 1117
    return-void
.end method

.method public final a()[B
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1140
    new-instance v2, Lcom/google/b/a/a/K;

    invoke-direct {v2}, Lcom/google/b/a/a/K;-><init>()V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/O;->b:Lcom/google/ipc/invalidation/ticl/a/N;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/N;->c()Lcom/google/b/a/a/J;

    move-result-object v0

    iput-object v0, v2, Lcom/google/b/a/a/K;->a:Lcom/google/b/a/a/J;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/O;->c:Lcom/google/ipc/invalidation/ticl/a/U;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/O;->c:Lcom/google/ipc/invalidation/ticl/a/U;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/U;->a()Lcom/google/b/a/a/Q;

    move-result-object v0

    :goto_0
    iput-object v0, v2, Lcom/google/b/a/a/K;->b:Lcom/google/b/a/a/Q;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/O;->d:Lcom/google/ipc/invalidation/ticl/a/ad;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/O;->d:Lcom/google/ipc/invalidation/ticl/a/ad;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/ad;->a()Lcom/google/b/a/a/Y;

    move-result-object v0

    :goto_1
    iput-object v0, v2, Lcom/google/b/a/a/K;->c:Lcom/google/b/a/a/Y;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/O;->e:Lcom/google/ipc/invalidation/ticl/a/aj;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/O;->e:Lcom/google/ipc/invalidation/ticl/a/aj;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/aj;->a()Lcom/google/b/a/a/ae;

    move-result-object v0

    :goto_2
    iput-object v0, v2, Lcom/google/b/a/a/K;->d:Lcom/google/b/a/a/ae;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/O;->f:Lcom/google/ipc/invalidation/ticl/a/V;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/O;->f:Lcom/google/ipc/invalidation/ticl/a/V;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/V;->d()Lcom/google/b/a/a/R;

    move-result-object v0

    :goto_3
    iput-object v0, v2, Lcom/google/b/a/a/K;->e:Lcom/google/b/a/a/R;

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/O;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/O;->g:Lcom/google/ipc/invalidation/ticl/a/S;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/S;->a()Lcom/google/b/a/a/O;

    move-result-object v1

    :cond_0
    iput-object v1, v2, Lcom/google/b/a/a/K;->f:Lcom/google/b/a/a/O;

    invoke-static {v2}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method protected final b()I
    .locals 4

    .prologue
    .line 1078
    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/O;->a:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 1079
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/O;->b:Lcom/google/ipc/invalidation/ticl/a/N;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/N;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1080
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/O;->c:Lcom/google/ipc/invalidation/ticl/a/U;

    if-eqz v1, :cond_0

    .line 1081
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/O;->c:Lcom/google/ipc/invalidation/ticl/a/U;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/U;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1083
    :cond_0
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/O;->d:Lcom/google/ipc/invalidation/ticl/a/ad;

    if-eqz v1, :cond_1

    .line 1084
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/O;->d:Lcom/google/ipc/invalidation/ticl/a/ad;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/ad;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1086
    :cond_1
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/O;->e:Lcom/google/ipc/invalidation/ticl/a/aj;

    if-eqz v1, :cond_2

    .line 1087
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/O;->e:Lcom/google/ipc/invalidation/ticl/a/aj;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/aj;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1089
    :cond_2
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/O;->f:Lcom/google/ipc/invalidation/ticl/a/V;

    if-eqz v1, :cond_3

    .line 1090
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/O;->f:Lcom/google/ipc/invalidation/ticl/a/V;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/V;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1092
    :cond_3
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/O;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1093
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/O;->g:Lcom/google/ipc/invalidation/ticl/a/S;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/S;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1095
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1065
    if-ne p0, p1, :cond_1

    .line 1068
    :cond_0
    :goto_0
    return v0

    .line 1066
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/ticl/a/O;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 1067
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/O;

    .line 1068
    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/O;->a:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/O;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/O;->b:Lcom/google/ipc/invalidation/ticl/a/N;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/O;->b:Lcom/google/ipc/invalidation/ticl/a/N;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/O;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/O;->c:Lcom/google/ipc/invalidation/ticl/a/U;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/O;->c:Lcom/google/ipc/invalidation/ticl/a/U;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/O;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/O;->d:Lcom/google/ipc/invalidation/ticl/a/ad;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/O;->d:Lcom/google/ipc/invalidation/ticl/a/ad;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/O;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/O;->e:Lcom/google/ipc/invalidation/ticl/a/aj;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/O;->e:Lcom/google/ipc/invalidation/ticl/a/aj;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/O;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/O;->f:Lcom/google/ipc/invalidation/ticl/a/V;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/O;->f:Lcom/google/ipc/invalidation/ticl/a/V;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/O;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/O;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/O;->g:Lcom/google/ipc/invalidation/ticl/a/S;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/O;->g:Lcom/google/ipc/invalidation/ticl/a/S;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/O;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
