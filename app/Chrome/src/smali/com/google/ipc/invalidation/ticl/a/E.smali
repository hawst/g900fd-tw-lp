.class public final Lcom/google/ipc/invalidation/ticl/a/E;
.super Lcom/google/ipc/invalidation/b/n;
.source "Client.java"


# instance fields
.field private final a:Lcom/google/ipc/invalidation/ticl/a/W;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/E;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/E;-><init>(Lcom/google/ipc/invalidation/ticl/a/W;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/ipc/invalidation/ticl/a/W;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/E;->a:Lcom/google/ipc/invalidation/ticl/a/W;

    .line 44
    return-void
.end method

.method public static a(Lcom/google/ipc/invalidation/ticl/a/W;)Lcom/google/ipc/invalidation/ticl/a/E;
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/E;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/ticl/a/E;-><init>(Lcom/google/ipc/invalidation/ticl/a/W;)V

    return-object v0
.end method

.method public static a([B)Lcom/google/ipc/invalidation/ticl/a/E;
    .locals 2

    .prologue
    .line 73
    :try_start_0
    new-instance v0, Lcom/google/b/a/a/C;

    invoke-direct {v0}, Lcom/google/b/a/a/C;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/a/C;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/ipc/invalidation/ticl/a/E;

    iget-object v0, v0, Lcom/google/b/a/a/C;->a:Lcom/google/b/a/a/S;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/a/W;->a(Lcom/google/b/a/a/S;)Lcom/google/ipc/invalidation/ticl/a/W;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/ipc/invalidation/ticl/a/E;-><init>(Lcom/google/ipc/invalidation/ticl/a/W;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/ipc/invalidation/b/o; {:try_start_0 .. :try_end_0} :catch_1

    move-object v0, v1

    goto :goto_0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    new-instance v1, Lcom/google/ipc/invalidation/b/p;

    invoke-direct {v1, v0}, Lcom/google/ipc/invalidation/b/p;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 76
    :catch_1
    move-exception v0

    .line 77
    new-instance v1, Lcom/google/ipc/invalidation/b/p;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/o;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/ipc/invalidation/b/p;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final a()Lcom/google/ipc/invalidation/ticl/a/W;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/E;->a:Lcom/google/ipc/invalidation/ticl/a/W;

    return-object v0
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 2

    .prologue
    .line 64
    const-string/jumbo v0, "<AckHandleP:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 65
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/E;->a:Lcom/google/ipc/invalidation/ticl/a/W;

    if-eqz v0, :cond_0

    .line 66
    const-string/jumbo v0, " invalidation="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/E;->a:Lcom/google/ipc/invalidation/ticl/a/W;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 68
    :cond_0
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 69
    return-void
.end method

.method protected final b()I
    .locals 2

    .prologue
    .line 56
    const/4 v0, 0x1

    .line 57
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/E;->a:Lcom/google/ipc/invalidation/ticl/a/W;

    if-eqz v1, :cond_0

    .line 58
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/E;->a:Lcom/google/ipc/invalidation/ticl/a/W;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/W;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 60
    :cond_0
    return v0
.end method

.method public final c()[B
    .locals 2

    .prologue
    .line 87
    new-instance v1, Lcom/google/b/a/a/C;

    invoke-direct {v1}, Lcom/google/b/a/a/C;-><init>()V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/E;->a:Lcom/google/ipc/invalidation/ticl/a/W;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/E;->a:Lcom/google/ipc/invalidation/ticl/a/W;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/W;->i()Lcom/google/b/a/a/S;

    move-result-object v0

    :goto_0
    iput-object v0, v1, Lcom/google/b/a/a/C;->a:Lcom/google/b/a/a/S;

    invoke-static {v1}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 49
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    .line 52
    :goto_0
    return v0

    .line 50
    :cond_0
    instance-of v0, p1, Lcom/google/ipc/invalidation/ticl/a/E;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 51
    :cond_1
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/E;

    .line 52
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/E;->a:Lcom/google/ipc/invalidation/ticl/a/W;

    iget-object v1, p1, Lcom/google/ipc/invalidation/ticl/a/E;->a:Lcom/google/ipc/invalidation/ticl/a/W;

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
