.class public final Lcom/google/ipc/invalidation/ticl/a/Q;
.super Lcom/google/ipc/invalidation/b/n;
.source "ClientProtocol.java"


# static fields
.field public static final a:Lcom/google/ipc/invalidation/ticl/a/Q;


# instance fields
.field private final b:J

.field private final c:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3026
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/Q;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/Q;-><init>(Ljava/lang/Long;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/Q;->a:Lcom/google/ipc/invalidation/ticl/a/Q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Long;)V
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3031
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 3033
    if-eqz p1, :cond_1

    .line 3035
    const-string/jumbo v2, "next_message_delay_ms"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v3, v4, v6

    if-gtz v3, :cond_0

    new-instance v3, Lcom/google/ipc/invalidation/b/o;

    sget-object v6, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string/jumbo v7, "Field \'%s\' must be positive: %d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v2, v8, v1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v8, v0

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/ipc/invalidation/b/o;-><init>(Ljava/lang/String;)V

    throw v3

    .line 3036
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/Q;->c:J

    .line 3040
    :goto_0
    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/Q;->b:J

    .line 3041
    return-void

    .line 3038
    :cond_1
    iput-wide v6, p0, Lcom/google/ipc/invalidation/ticl/a/Q;->c:J

    move v0, v1

    goto :goto_0
.end method

.method static a(Lcom/google/b/a/a/M;)Lcom/google/ipc/invalidation/ticl/a/Q;
    .locals 2

    .prologue
    .line 3081
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 3082
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/Q;

    iget-object v1, p0, Lcom/google/b/a/a/M;->a:Ljava/lang/Long;

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/Q;-><init>(Ljava/lang/Long;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 3043
    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/Q;->c:J

    return-wide v0
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 4

    .prologue
    .line 3063
    const-string/jumbo v0, "<ConfigChangeMessage:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 3064
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/Q;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3065
    const-string/jumbo v0, " next_message_delay_ms="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/Q;->c:J

    invoke-virtual {v0, v2, v3}, Lcom/google/ipc/invalidation/b/r;->a(J)Lcom/google/ipc/invalidation/b/r;

    .line 3067
    :cond_0
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 3068
    return-void
.end method

.method protected final b()I
    .locals 6

    .prologue
    const/16 v4, 0x20

    .line 3055
    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/Q;->b:J

    ushr-long v2, v0, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 3056
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/Q;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3057
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/Q;->c:J

    ushr-long v4, v2, v4

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 3059
    :cond_0
    return v0
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 3044
    const-wide/16 v0, 0x1

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/Q;->b:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3047
    if-ne p0, p1, :cond_1

    .line 3050
    :cond_0
    :goto_0
    return v0

    .line 3048
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/ticl/a/Q;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 3049
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/Q;

    .line 3050
    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/Q;->b:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/Q;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/Q;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/Q;->c:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/Q;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
