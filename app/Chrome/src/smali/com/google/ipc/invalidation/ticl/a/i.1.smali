.class public final Lcom/google/ipc/invalidation/ticl/a/i;
.super Lcom/google/ipc/invalidation/b/n;
.source "AndroidService.java"


# instance fields
.field private final a:Lcom/google/ipc/invalidation/ticl/a/ap;

.field private final b:Lcom/google/ipc/invalidation/ticl/a/ar;

.field private final c:Lcom/google/ipc/invalidation/ticl/a/j;


# direct methods
.method private constructor <init>(Lcom/google/ipc/invalidation/ticl/a/ap;Lcom/google/ipc/invalidation/ticl/a/ar;Lcom/google/ipc/invalidation/ticl/a/j;)V
    .locals 1

    .prologue
    .line 1869
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 1870
    const-string/jumbo v0, "version"

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/a/i;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1871
    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    .line 1872
    const-string/jumbo v0, "ticl_state"

    invoke-static {v0, p2}, Lcom/google/ipc/invalidation/ticl/a/i;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1873
    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/a/i;->b:Lcom/google/ipc/invalidation/ticl/a/ar;

    .line 1874
    const-string/jumbo v0, "metadata"

    invoke-static {v0, p3}, Lcom/google/ipc/invalidation/ticl/a/i;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1875
    iput-object p3, p0, Lcom/google/ipc/invalidation/ticl/a/i;->c:Lcom/google/ipc/invalidation/ticl/a/j;

    .line 1876
    return-void
.end method

.method static a(Lcom/google/b/a/a/i;)Lcom/google/ipc/invalidation/ticl/a/i;
    .locals 4

    .prologue
    .line 1920
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 1921
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/i;

    iget-object v1, p0, Lcom/google/b/a/a/i;->a:Lcom/google/b/a/a/ak;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/ap;->a(Lcom/google/b/a/a/ak;)Lcom/google/ipc/invalidation/ticl/a/ap;

    move-result-object v1

    iget-object v2, p0, Lcom/google/b/a/a/i;->b:Lcom/google/b/a/a/am;

    invoke-static {v2}, Lcom/google/ipc/invalidation/ticl/a/ar;->a(Lcom/google/b/a/a/am;)Lcom/google/ipc/invalidation/ticl/a/ar;

    move-result-object v2

    iget-object v3, p0, Lcom/google/b/a/a/i;->c:Lcom/google/b/a/a/j;

    invoke-static {v3}, Lcom/google/ipc/invalidation/ticl/a/j;->a(Lcom/google/b/a/a/j;)Lcom/google/ipc/invalidation/ticl/a/j;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ipc/invalidation/ticl/a/i;-><init>(Lcom/google/ipc/invalidation/ticl/a/ap;Lcom/google/ipc/invalidation/ticl/a/ar;Lcom/google/ipc/invalidation/ticl/a/j;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/ipc/invalidation/ticl/a/ap;Lcom/google/ipc/invalidation/ticl/a/ar;Lcom/google/ipc/invalidation/ticl/a/j;)Lcom/google/ipc/invalidation/ticl/a/i;
    .locals 1

    .prologue
    .line 1860
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/i;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/ipc/invalidation/ticl/a/i;-><init>(Lcom/google/ipc/invalidation/ticl/a/ap;Lcom/google/ipc/invalidation/ticl/a/ar;Lcom/google/ipc/invalidation/ticl/a/j;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/ipc/invalidation/ticl/a/ar;
    .locals 1

    .prologue
    .line 1880
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/i;->b:Lcom/google/ipc/invalidation/ticl/a/ar;

    return-object v0
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 2

    .prologue
    .line 1902
    const-string/jumbo v0, "<AndroidTiclState:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 1903
    const-string/jumbo v0, " version="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1904
    const-string/jumbo v0, " ticl_state="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/i;->b:Lcom/google/ipc/invalidation/ticl/a/ar;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1905
    const-string/jumbo v0, " metadata="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/i;->c:Lcom/google/ipc/invalidation/ticl/a/j;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1906
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 1907
    return-void
.end method

.method protected final b()I
    .locals 2

    .prologue
    .line 1894
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/ap;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 1896
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/i;->b:Lcom/google/ipc/invalidation/ticl/a/ar;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/ar;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1897
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/i;->c:Lcom/google/ipc/invalidation/ticl/a/j;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/j;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1898
    return v0
.end method

.method public final c()Lcom/google/ipc/invalidation/ticl/a/j;
    .locals 1

    .prologue
    .line 1882
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/i;->c:Lcom/google/ipc/invalidation/ticl/a/j;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 1927
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/i;->e()Lcom/google/b/a/a/i;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    return-object v0
.end method

.method final e()Lcom/google/b/a/a/i;
    .locals 2

    .prologue
    .line 1931
    new-instance v0, Lcom/google/b/a/a/i;

    invoke-direct {v0}, Lcom/google/b/a/a/i;-><init>()V

    .line 1932
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/ap;->c()Lcom/google/b/a/a/ak;

    move-result-object v1

    iput-object v1, v0, Lcom/google/b/a/a/i;->a:Lcom/google/b/a/a/ak;

    .line 1933
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/i;->b:Lcom/google/ipc/invalidation/ticl/a/ar;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/ar;->t()Lcom/google/b/a/a/am;

    move-result-object v1

    iput-object v1, v0, Lcom/google/b/a/a/i;->b:Lcom/google/b/a/a/am;

    .line 1934
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/i;->c:Lcom/google/ipc/invalidation/ticl/a/j;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/j;->f()Lcom/google/b/a/a/j;

    move-result-object v1

    iput-object v1, v0, Lcom/google/b/a/a/i;->c:Lcom/google/b/a/a/j;

    .line 1935
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1885
    if-ne p0, p1, :cond_1

    .line 1888
    :cond_0
    :goto_0
    return v0

    .line 1886
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/ticl/a/i;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 1887
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/i;

    .line 1888
    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/i;->b:Lcom/google/ipc/invalidation/ticl/a/ar;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/i;->b:Lcom/google/ipc/invalidation/ticl/a/ar;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/i;->c:Lcom/google/ipc/invalidation/ticl/a/j;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/i;->c:Lcom/google/ipc/invalidation/ticl/a/j;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
