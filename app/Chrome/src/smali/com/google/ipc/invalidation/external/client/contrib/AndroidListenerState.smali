.class final Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;
.super Ljava/lang/Object;
.source "AndroidListenerState.java"


# instance fields
.field private final clientId:Lcom/google/ipc/invalidation/b/c;

.field private final delayGenerators:Ljava/util/Map;

.field private final desiredRegistrations:Ljava/util/Set;

.field private final initialMaxDelayMs:I

.field private isDirty:Z

.field private final maxDelayFactor:I

.field private final random:Ljava/util/Random;

.field private requestCodeSeqNum:I


# direct methods
.method constructor <init>(II)V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    .line 73
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->random:Ljava/util/Random;

    .line 98
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    .line 99
    invoke-static {}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->createGloballyUniqueClientId()Lcom/google/ipc/invalidation/b/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->clientId:Lcom/google/ipc/invalidation/b/c;

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    .line 103
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->requestCodeSeqNum:I

    .line 104
    iput p1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->initialMaxDelayMs:I

    .line 105
    iput p2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->maxDelayFactor:I

    .line 106
    return-void
.end method

.method constructor <init>(IILcom/google/ipc/invalidation/ticl/a/c;)V
    .locals 6

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    .line 73
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->random:Ljava/util/Random;

    .line 111
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    .line 112
    invoke-virtual {p3}, Lcom/google/ipc/invalidation/ticl/a/c;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/Y;

    .line 113
    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-static {v0}, Landroid/support/v4/app/b;->a(Lcom/google/ipc/invalidation/ticl/a/Y;)Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 115
    :cond_0
    invoke-virtual {p3}, Lcom/google/ipc/invalidation/ticl/a/c;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/d;

    .line 116
    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/d;->a()Lcom/google/ipc/invalidation/ticl/a/Y;

    move-result-object v2

    .line 117
    if-eqz v2, :cond_1

    .line 118
    invoke-static {v2}, Landroid/support/v4/app/b;->a(Lcom/google/ipc/invalidation/ticl/a/Y;)Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v2

    .line 121
    iget-object v3, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    new-instance v4, Lcom/google/ipc/invalidation/ticl/ai;

    iget-object v5, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->random:Ljava/util/Random;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/d;->c()Lcom/google/ipc/invalidation/ticl/a/F;

    move-result-object v0

    invoke-direct {v4, v5, p1, p2, v0}, Lcom/google/ipc/invalidation/ticl/ai;-><init>(Ljava/util/Random;IILcom/google/ipc/invalidation/ticl/a/F;)V

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 124
    :cond_2
    invoke-virtual {p3}, Lcom/google/ipc/invalidation/ticl/a/c;->d()Lcom/google/ipc/invalidation/b/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->clientId:Lcom/google/ipc/invalidation/b/c;

    .line 125
    invoke-virtual {p3}, Lcom/google/ipc/invalidation/ticl/a/c;->f()I

    move-result v0

    iput v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->requestCodeSeqNum:I

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    .line 127
    iput p1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->initialMaxDelayMs:I

    .line 128
    iput p2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->maxDelayFactor:I

    .line 129
    return-void
.end method

.method private static createGloballyUniqueClientId()Lcom/google/ipc/invalidation/b/c;
    .locals 6

    .prologue
    .line 297
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 298
    const/16 v1, 0x10

    new-array v1, v1, [B

    .line 299
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 300
    invoke-virtual {v0}, Ljava/util/UUID;->getLeastSignificantBits()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 301
    invoke-virtual {v0}, Ljava/util/UUID;->getMostSignificantBits()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 302
    new-instance v0, Lcom/google/ipc/invalidation/b/c;

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/b/c;-><init>([B)V

    return-object v0
.end method

.method private static equals(Ljava/util/Map;Ljava/util/Map;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 272
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    move v0, v2

    .line 282
    :goto_0
    return v0

    .line 275
    :cond_0
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 276
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ipc/invalidation/ticl/ai;

    .line 277
    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/ai;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/ai;->a()Lcom/google/ipc/invalidation/ticl/a/F;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/ai;->a()Lcom/google/ipc/invalidation/ticl/a/F;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/support/v4/app/b;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_2
    move v0, v2

    .line 279
    goto :goto_0

    .line 282
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private resetDelayGeneratorFor(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    .line 185
    :cond_0
    return-void
.end method


# virtual methods
.method final addDesiredRegistration(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 189
    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 190
    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    .line 193
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final containsDesiredRegistration(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Z
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 251
    if-ne p0, p1, :cond_1

    .line 261
    :cond_0
    :goto_0
    return v0

    .line 255
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    if-nez v2, :cond_2

    move v0, v1

    .line 256
    goto :goto_0

    .line 259
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    .line 261
    iget-boolean v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    iget-boolean v3, p1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->requestCodeSeqNum:I

    iget v3, p1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->requestCodeSeqNum:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    iget-object v3, p1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    iget-object v3, p1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->clientId:Lcom/google/ipc/invalidation/b/c;

    iget-object v3, p1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->clientId:Lcom/google/ipc/invalidation/b/c;

    invoke-static {v2, v3}, Landroid/support/v4/app/b;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    iget-object v3, p1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->equals(Ljava/util/Map;Ljava/util/Map;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method final getClientId()Lcom/google/ipc/invalidation/b/c;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->clientId:Lcom/google/ipc/invalidation/b/c;

    return-object v0
.end method

.method final getIsDirty()Z
    .locals 1

    .prologue
    .line 236
    iget-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    return v0
.end method

.method final getNextDelay(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)I
    .locals 4

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/ai;

    .line 145
    if-nez v0, :cond_0

    .line 146
    new-instance v0, Lcom/google/ipc/invalidation/ticl/ai;

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->random:Ljava/util/Random;

    iget v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->initialMaxDelayMs:I

    iget v3, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->maxDelayFactor:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ipc/invalidation/ticl/ai;-><init>(Ljava/util/Random;II)V

    .line 148
    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    .line 152
    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/ai;->c()I

    move-result v0

    return v0
.end method

.method final getNextRequestCode()I
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    .line 134
    iget v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->requestCodeSeqNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->requestCodeSeqNum:I

    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->clientId:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/c;->hashCode()I

    move-result v0

    return v0
.end method

.method public final informRegistrationFailure(Lcom/google/ipc/invalidation/external/client/types/ObjectId;Z)V
    .locals 0

    .prologue
    .line 171
    invoke-virtual {p0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->removeDesiredRegistration(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Z

    .line 172
    if-nez p2, :cond_0

    .line 174
    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->resetDelayGeneratorFor(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V

    .line 176
    :cond_0
    return-void
.end method

.method final informRegistrationSuccess(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V
    .locals 0

    .prologue
    .line 159
    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->resetDelayGeneratorFor(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V

    .line 160
    return-void
.end method

.method public final marshal()Lcom/google/ipc/invalidation/ticl/a/c;
    .locals 4

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->clientId:Lcom/google/ipc/invalidation/b/c;

    iget v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->requestCodeSeqNum:I

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-static {v0, v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newAndroidListenerState(Lcom/google/ipc/invalidation/b/c;ILjava/util/Map;Ljava/util/Collection;)Lcom/google/ipc/invalidation/ticl/a/c;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic marshal()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->marshal()Lcom/google/ipc/invalidation/ticl/a/c;

    move-result-object v0

    return-object v0
.end method

.method final removeDesiredRegistration(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 198
    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 199
    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    .line 202
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final resetIsDirty()V
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    .line 210
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 287
    sget-object v0, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string/jumbo v1, "AndroidListenerState[%s]: isDirty = %b, desiredRegistrations.size() = %d, delayGenerators.size() = %d, requestCodeSeqNum = %d"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->clientId:Lcom/google/ipc/invalidation/b/c;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-boolean v4, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget v4, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->requestCodeSeqNum:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
