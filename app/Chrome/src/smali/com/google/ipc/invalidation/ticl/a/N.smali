.class public final Lcom/google/ipc/invalidation/ticl/a/N;
.super Lcom/google/ipc/invalidation/b/n;
.source "ClientProtocol.java"


# instance fields
.field private final a:J

.field private final b:Lcom/google/ipc/invalidation/ticl/a/ab;

.field private final c:Lcom/google/ipc/invalidation/b/c;

.field private final d:Lcom/google/ipc/invalidation/ticl/a/ai;

.field private final e:J

.field private final f:J

.field private final g:Ljava/lang/String;

.field private final h:I


# direct methods
.method private constructor <init>(Lcom/google/ipc/invalidation/ticl/a/ab;Lcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/ticl/a/ai;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 866
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 868
    const-string/jumbo v0, "protocol_version"

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/a/N;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 869
    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/N;->b:Lcom/google/ipc/invalidation/ticl/a/ab;

    .line 870
    if-eqz p2, :cond_0

    .line 871
    const/4 v0, 0x1

    .line 872
    const-string/jumbo v2, "client_token"

    invoke-static {v2, p2}, Lcom/google/ipc/invalidation/ticl/a/N;->a(Ljava/lang/String;Lcom/google/ipc/invalidation/b/c;)V

    .line 873
    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->c:Lcom/google/ipc/invalidation/b/c;

    .line 877
    :goto_0
    iput-object p3, p0, Lcom/google/ipc/invalidation/ticl/a/N;->d:Lcom/google/ipc/invalidation/ticl/a/ai;

    .line 878
    const-string/jumbo v2, "client_time_ms"

    invoke-static {v2, p4}, Lcom/google/ipc/invalidation/ticl/a/N;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 879
    const-string/jumbo v2, "client_time_ms"

    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/ipc/invalidation/ticl/a/N;->a(Ljava/lang/String;J)V

    .line 880
    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->e:J

    .line 881
    const-string/jumbo v2, "max_known_server_time_ms"

    invoke-static {v2, p5}, Lcom/google/ipc/invalidation/ticl/a/N;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 882
    const-string/jumbo v2, "max_known_server_time_ms"

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/ipc/invalidation/ticl/a/N;->a(Ljava/lang/String;J)V

    .line 883
    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->f:J

    .line 884
    if-eqz p6, :cond_1

    .line 885
    or-int/lit8 v0, v0, 0x2

    .line 886
    const-string/jumbo v2, "message_id"

    invoke-static {v2, p6}, Lcom/google/ipc/invalidation/ticl/a/N;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    iput-object p6, p0, Lcom/google/ipc/invalidation/ticl/a/N;->g:Ljava/lang/String;

    .line 891
    :goto_1
    if-eqz p7, :cond_2

    .line 892
    or-int/lit8 v0, v0, 0x4

    .line 893
    invoke-virtual {p7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/N;->h:I

    .line 897
    :goto_2
    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/N;->a:J

    .line 898
    return-void

    .line 875
    :cond_0
    sget-object v0, Lcom/google/ipc/invalidation/b/c;->a:Lcom/google/ipc/invalidation/b/c;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/N;->c:Lcom/google/ipc/invalidation/b/c;

    move v0, v1

    goto :goto_0

    .line 889
    :cond_1
    const-string/jumbo v2, ""

    iput-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->g:Ljava/lang/String;

    goto :goto_1

    .line 895
    :cond_2
    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/N;->h:I

    goto :goto_2
.end method

.method public static a(Lcom/google/ipc/invalidation/ticl/a/ab;Lcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/ticl/a/ai;JJLjava/lang/String;Ljava/lang/Integer;)Lcom/google/ipc/invalidation/ticl/a/N;
    .locals 9

    .prologue
    .line 848
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/N;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/google/ipc/invalidation/ticl/a/N;-><init>(Lcom/google/ipc/invalidation/ticl/a/ab;Lcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/ticl/a/ai;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method

.method private d()Z
    .locals 4

    .prologue
    .line 912
    const-wide/16 v0, 0x2

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Z
    .locals 4

    .prologue
    .line 915
    const-wide/16 v0, 0x4

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 4

    .prologue
    .line 952
    const-string/jumbo v0, "<ClientHeader:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 953
    const-string/jumbo v0, " protocol_version="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/N;->b:Lcom/google/ipc/invalidation/ticl/a/ab;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 954
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/N;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 955
    const-string/jumbo v0, " client_token="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/N;->c:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 957
    :cond_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/N;->d:Lcom/google/ipc/invalidation/ticl/a/ai;

    if-eqz v0, :cond_1

    .line 958
    const-string/jumbo v0, " registration_summary="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/N;->d:Lcom/google/ipc/invalidation/ticl/a/ai;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 960
    :cond_1
    const-string/jumbo v0, " client_time_ms="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->e:J

    invoke-virtual {v0, v2, v3}, Lcom/google/ipc/invalidation/b/r;->a(J)Lcom/google/ipc/invalidation/b/r;

    .line 961
    const-string/jumbo v0, " max_known_server_time_ms="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->f:J

    invoke-virtual {v0, v2, v3}, Lcom/google/ipc/invalidation/b/r;->a(J)Lcom/google/ipc/invalidation/b/r;

    .line 962
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/N;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 963
    const-string/jumbo v0, " message_id="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/N;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 965
    :cond_2
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/N;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 966
    const-string/jumbo v0, " client_type="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/N;->h:I

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(I)Lcom/google/ipc/invalidation/b/r;

    .line 968
    :cond_3
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 969
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 903
    const-wide/16 v0, 0x1

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 932
    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/N;->a:J

    ushr-long v2, v0, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 933
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/N;->b:Lcom/google/ipc/invalidation/ticl/a/ab;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/ab;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 934
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/N;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 935
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/N;->c:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 937
    :cond_0
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/N;->d:Lcom/google/ipc/invalidation/ticl/a/ai;

    if-eqz v1, :cond_1

    .line 938
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/N;->d:Lcom/google/ipc/invalidation/ticl/a/ai;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/ai;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 940
    :cond_1
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->e:J

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 941
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->f:J

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 942
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/N;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 943
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/N;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 945
    :cond_2
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/N;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 946
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/N;->h:I

    add-int/2addr v0, v1

    .line 948
    :cond_3
    return v0
.end method

.method final c()Lcom/google/b/a/a/J;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 997
    new-instance v2, Lcom/google/b/a/a/J;

    invoke-direct {v2}, Lcom/google/b/a/a/J;-><init>()V

    .line 998
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/N;->b:Lcom/google/ipc/invalidation/ticl/a/ab;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/ab;->c()Lcom/google/b/a/a/W;

    move-result-object v0

    iput-object v0, v2, Lcom/google/b/a/a/J;->a:Lcom/google/b/a/a/W;

    .line 999
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/N;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/N;->c:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/c;->b()[B

    move-result-object v0

    :goto_0
    iput-object v0, v2, Lcom/google/b/a/a/J;->b:[B

    .line 1000
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/N;->d:Lcom/google/ipc/invalidation/ticl/a/ai;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/N;->d:Lcom/google/ipc/invalidation/ticl/a/ai;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/ai;->a()Lcom/google/b/a/a/ad;

    move-result-object v0

    :goto_1
    iput-object v0, v2, Lcom/google/b/a/a/J;->c:Lcom/google/b/a/a/ad;

    .line 1001
    iget-wide v4, p0, Lcom/google/ipc/invalidation/ticl/a/N;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lcom/google/b/a/a/J;->d:Ljava/lang/Long;

    .line 1002
    iget-wide v4, p0, Lcom/google/ipc/invalidation/ticl/a/N;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lcom/google/b/a/a/J;->e:Ljava/lang/Long;

    .line 1003
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/N;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/N;->g:Ljava/lang/String;

    :goto_2
    iput-object v0, v2, Lcom/google/b/a/a/J;->f:Ljava/lang/String;

    .line 1004
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/N;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/N;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :cond_0
    iput-object v1, v2, Lcom/google/b/a/a/J;->g:Ljava/lang/Integer;

    .line 1005
    return-object v2

    :cond_1
    move-object v0, v1

    .line 999
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 1000
    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 1003
    goto :goto_2
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 918
    if-ne p0, p1, :cond_1

    .line 921
    :cond_0
    :goto_0
    return v0

    .line 919
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/ticl/a/N;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 920
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/N;

    .line 921
    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->a:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/N;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->b:Lcom/google/ipc/invalidation/ticl/a/ab;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/N;->b:Lcom/google/ipc/invalidation/ticl/a/ab;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/N;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/N;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->c:Lcom/google/ipc/invalidation/b/c;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/N;->c:Lcom/google/ipc/invalidation/b/c;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/N;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_3
    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->d:Lcom/google/ipc/invalidation/ticl/a/ai;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/N;->d:Lcom/google/ipc/invalidation/ticl/a/ai;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/N;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->e:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/N;->e:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->f:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/N;->f:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/N;->d()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/N;->g:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/N;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/N;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/ipc/invalidation/ticl/a/N;->h:I

    iget v3, p1, Lcom/google/ipc/invalidation/ticl/a/N;->h:I

    if-eq v2, v3, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method
