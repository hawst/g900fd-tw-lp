.class public final Lcom/google/ipc/invalidation/ticl/a/U;
.super Lcom/google/ipc/invalidation/b/n;
.source "ClientProtocol.java"


# instance fields
.field private final a:I

.field private final b:Lcom/google/ipc/invalidation/b/c;

.field private final c:Lcom/google/ipc/invalidation/ticl/a/K;

.field private final d:I


# direct methods
.method private constructor <init>(Ljava/lang/Integer;Lcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/ticl/a/K;Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 1176
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 1177
    const-string/jumbo v0, "client_type"

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/a/U;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1178
    const-string/jumbo v0, "client_type"

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/U;->a(Ljava/lang/String;I)V

    .line 1179
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/ipc/invalidation/ticl/a/U;->a:I

    .line 1180
    const-string/jumbo v0, "nonce"

    invoke-static {v0, p2}, Lcom/google/ipc/invalidation/ticl/a/U;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1181
    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/a/U;->b:Lcom/google/ipc/invalidation/b/c;

    .line 1182
    const-string/jumbo v0, "application_client_id"

    invoke-static {v0, p3}, Lcom/google/ipc/invalidation/ticl/a/U;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1183
    iput-object p3, p0, Lcom/google/ipc/invalidation/ticl/a/U;->c:Lcom/google/ipc/invalidation/ticl/a/K;

    .line 1184
    const-string/jumbo v0, "digest_serialization_type"

    invoke-static {v0, p4}, Lcom/google/ipc/invalidation/ticl/a/U;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1185
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/ipc/invalidation/ticl/a/U;->d:I

    .line 1186
    return-void
.end method

.method public static a(ILcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/ticl/a/K;I)Lcom/google/ipc/invalidation/ticl/a/U;
    .locals 3

    .prologue
    .line 1165
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/U;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/google/ipc/invalidation/ticl/a/U;-><init>(Ljava/lang/Integer;Lcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/ticl/a/K;Ljava/lang/Integer;)V

    return-object v0
.end method

.method static a(Lcom/google/b/a/a/Q;)Lcom/google/ipc/invalidation/ticl/a/U;
    .locals 5

    .prologue
    .line 1235
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 1236
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/U;

    iget-object v1, p0, Lcom/google/b/a/a/Q;->a:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/google/b/a/a/Q;->b:[B

    invoke-static {v2}, Lcom/google/ipc/invalidation/b/c;->a([B)Lcom/google/ipc/invalidation/b/c;

    move-result-object v2

    iget-object v3, p0, Lcom/google/b/a/a/Q;->c:Lcom/google/b/a/a/H;

    invoke-static {v3}, Lcom/google/ipc/invalidation/ticl/a/K;->a(Lcom/google/b/a/a/H;)Lcom/google/ipc/invalidation/ticl/a/K;

    move-result-object v3

    iget-object v4, p0, Lcom/google/b/a/a/Q;->d:Ljava/lang/Integer;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/ipc/invalidation/ticl/a/U;-><init>(Ljava/lang/Integer;Lcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/ticl/a/K;Ljava/lang/Integer;)V

    goto :goto_0
.end method


# virtual methods
.method final a()Lcom/google/b/a/a/Q;
    .locals 2

    .prologue
    .line 1247
    new-instance v0, Lcom/google/b/a/a/Q;

    invoke-direct {v0}, Lcom/google/b/a/a/Q;-><init>()V

    .line 1248
    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/U;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/b/a/a/Q;->a:Ljava/lang/Integer;

    .line 1249
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/U;->b:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->b()[B

    move-result-object v1

    iput-object v1, v0, Lcom/google/b/a/a/Q;->b:[B

    .line 1250
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/U;->c:Lcom/google/ipc/invalidation/ticl/a/K;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/K;->d()Lcom/google/b/a/a/H;

    move-result-object v1

    iput-object v1, v0, Lcom/google/b/a/a/Q;->c:Lcom/google/b/a/a/H;

    .line 1251
    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/U;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/b/a/a/Q;->d:Ljava/lang/Integer;

    .line 1252
    return-object v0
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 2

    .prologue
    .line 1216
    const-string/jumbo v0, "<InitializeMessage:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 1217
    const-string/jumbo v0, " client_type="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/U;->a:I

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(I)Lcom/google/ipc/invalidation/b/r;

    .line 1218
    const-string/jumbo v0, " nonce="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/U;->b:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1219
    const-string/jumbo v0, " application_client_id="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/U;->c:Lcom/google/ipc/invalidation/ticl/a/K;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1220
    const-string/jumbo v0, " digest_serialization_type="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/U;->d:I

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(I)Lcom/google/ipc/invalidation/b/r;

    .line 1221
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 1222
    return-void
.end method

.method protected final b()I
    .locals 2

    .prologue
    .line 1207
    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/U;->a:I

    add-int/lit8 v0, v0, 0x1f

    .line 1209
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/U;->b:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1210
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/U;->c:Lcom/google/ipc/invalidation/ticl/a/K;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/K;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1211
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/U;->d:I

    add-int/2addr v0, v1

    .line 1212
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1197
    if-ne p0, p1, :cond_1

    .line 1200
    :cond_0
    :goto_0
    return v0

    .line 1198
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/ticl/a/U;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 1199
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/U;

    .line 1200
    iget v2, p0, Lcom/google/ipc/invalidation/ticl/a/U;->a:I

    iget v3, p1, Lcom/google/ipc/invalidation/ticl/a/U;->a:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/U;->b:Lcom/google/ipc/invalidation/b/c;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/U;->b:Lcom/google/ipc/invalidation/b/c;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/U;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/U;->c:Lcom/google/ipc/invalidation/ticl/a/K;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/U;->c:Lcom/google/ipc/invalidation/ticl/a/K;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/U;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/google/ipc/invalidation/ticl/a/U;->d:I

    iget v3, p1, Lcom/google/ipc/invalidation/ticl/a/U;->d:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
