.class Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;
.super Ljava/lang/Object;
.source "AndroidListenerIntents.java"


# static fields
.field static final EXTRA_ACK:Ljava/lang/String; = "com.google.ipc.invalidation.android_listener.ACK"

.field static final EXTRA_REGISTRATION:Ljava/lang/String; = "com.google.ipc.invalidation.android_listener.REGISTRATION"

.field static final EXTRA_START:Ljava/lang/String; = "com.google.ipc.invalidation.android_listener.START"

.field static final EXTRA_STOP:Ljava/lang/String; = "com.google.ipc.invalidation.android_listener.STOP"

.field private static final logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->forPrefix(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    return-void
.end method

.method static createAckIntent(Landroid/content/Context;[B)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 174
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 175
    const-string/jumbo v1, "com.google.ipc.invalidation.android_listener.ACK"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 176
    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->setAndroidListenerClass(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static createRegistrationIntent(Landroid/content/Context;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Iterable;Z)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 183
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 184
    invoke-static {p1, p2, p3}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newRegistrationCommand(Lcom/google/ipc/invalidation/b/c;Ljava/lang/Iterable;Z)Lcom/google/ipc/invalidation/ticl/a/e;

    move-result-object v1

    .line 186
    const-string/jumbo v2, "com.google.ipc.invalidation.android_listener.REGISTRATION"

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/e;->i()[B

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 187
    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->setAndroidListenerClass(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static createStartIntent(Landroid/content/Context;ILcom/google/ipc/invalidation/b/c;Z)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 155
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 157
    invoke-static {p1, p2, p3}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newStartCommand(ILcom/google/ipc/invalidation/b/c;Z)Lcom/google/ipc/invalidation/ticl/a/f;

    move-result-object v1

    .line 159
    const-string/jumbo v2, "com.google.ipc.invalidation.android_listener.START"

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/f;->g()[B

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 160
    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->setAndroidListenerClass(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static createStopIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 166
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 167
    const-string/jumbo v1, "com.google.ipc.invalidation.android_listener.STOP"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 168
    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->setAndroidListenerClass(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static findAckHandle(Landroid/content/Intent;)[B
    .locals 1

    .prologue
    .line 84
    const-string/jumbo v0, "com.google.ipc.invalidation.android_listener.ACK"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method static findRegistrationCommand(Landroid/content/Intent;)Lcom/google/ipc/invalidation/ticl/a/e;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 93
    const-string/jumbo v1, "com.google.ipc.invalidation.android_listener.REGISTRATION"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    .line 94
    if-nez v1, :cond_0

    .line 103
    :goto_0
    return-object v0

    .line 100
    :cond_0
    :try_start_0
    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/e;->a([B)Lcom/google/ipc/invalidation/ticl/a/e;
    :try_end_0
    .catch Lcom/google/ipc/invalidation/b/p; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 101
    :catch_0
    move-exception v1

    .line 102
    sget-object v2, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string/jumbo v3, "Received invalid proto: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static findStartCommand(Landroid/content/Intent;)Lcom/google/ipc/invalidation/ticl/a/f;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 113
    const-string/jumbo v1, "com.google.ipc.invalidation.android_listener.START"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    .line 114
    if-nez v1, :cond_0

    .line 123
    :goto_0
    return-object v0

    .line 120
    :cond_0
    :try_start_0
    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/f;->a([B)Lcom/google/ipc/invalidation/ticl/a/f;
    :try_end_0
    .catch Lcom/google/ipc/invalidation/b/p; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 121
    :catch_0
    move-exception v1

    .line 122
    sget-object v2, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string/jumbo v3, "Received invalid proto: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static isAuthTokenRequest(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 198
    const-string/jumbo v0, "com.google.ipc.invalidation.AUTH_TOKEN_REQUEST"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static isStopIntent(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 129
    const-string/jumbo v0, "com.google.ipc.invalidation.android_listener.STOP"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static issueAndroidListenerIntent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 76
    invoke-static {p0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->setAndroidListenerClass(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 77
    return-void
.end method

.method static issueAuthTokenResponse(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 207
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "com.google.ipc.invalidation.AUTH_TOKEN"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "com.google.ipc.invalidation.AUTH_TOKEN_TYPE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 211
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1, p0, v1, v0}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 215
    :goto_0
    return-void

    .line 212
    :catch_0
    move-exception v0

    .line 213
    sget-object v1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string/jumbo v2, "Canceled auth request: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static issueDelayedRegistrationIntent(Landroid/content/Context;Lcom/google/ipc/invalidation/ticl/android2/a;Lcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/external/client/types/ObjectId;ZII)V
    .locals 6

    .prologue
    .line 135
    if-eqz p4, :cond_0

    invoke-static {p2, p3}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newDelayedRegisterCommand(Lcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/ipc/invalidation/ticl/a/e;

    move-result-object v0

    .line 138
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v2, "com.google.ipc.invalidation.android_listener.REGISTRATION"

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/e;->i()[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    const-class v1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$AlarmReceiver;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 143
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {p0, p6, v0, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 147
    const-string/jumbo v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 148
    invoke-virtual {p1}, Lcom/google/ipc/invalidation/ticl/android2/a;->a()J

    move-result-wide v2

    int-to-long v4, p5

    add-long/2addr v2, v4

    .line 149
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 150
    return-void

    .line 135
    :cond_0
    invoke-static {p2, p3}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newDelayedUnregisterCommand(Lcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/ipc/invalidation/ticl/a/e;

    move-result-object v0

    goto :goto_0
.end method

.method static issueTiclIntent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/g;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/ticl/android2/g;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/g;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 69
    return-void
.end method

.method static setAndroidListenerClass(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 192
    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/g;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/ticl/android2/g;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/g;->b()Ljava/lang/String;

    move-result-object v0

    .line 193
    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
