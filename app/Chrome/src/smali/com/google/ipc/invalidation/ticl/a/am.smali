.class public final Lcom/google/ipc/invalidation/ticl/a/am;
.super Lcom/google/ipc/invalidation/b/n;
.source "ClientProtocol.java"


# instance fields
.field private final a:J

.field private final b:Lcom/google/ipc/invalidation/ticl/a/al;

.field private final c:Lcom/google/ipc/invalidation/ticl/a/ao;

.field private final d:Lcom/google/ipc/invalidation/ticl/a/V;

.field private final e:Lcom/google/ipc/invalidation/ticl/a/ag;

.field private final f:Lcom/google/ipc/invalidation/ticl/a/ak;

.field private final g:Lcom/google/ipc/invalidation/ticl/a/Q;

.field private final h:Lcom/google/ipc/invalidation/ticl/a/T;

.field private final i:Lcom/google/ipc/invalidation/ticl/a/R;


# direct methods
.method private constructor <init>(Lcom/google/ipc/invalidation/ticl/a/al;Lcom/google/ipc/invalidation/ticl/a/ao;Lcom/google/ipc/invalidation/ticl/a/V;Lcom/google/ipc/invalidation/ticl/a/ag;Lcom/google/ipc/invalidation/ticl/a/ak;Lcom/google/ipc/invalidation/ticl/a/Q;Lcom/google/ipc/invalidation/ticl/a/T;Lcom/google/ipc/invalidation/ticl/a/R;)V
    .locals 2

    .prologue
    .line 1887
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 1888
    const/4 v0, 0x0

    .line 1889
    const-string/jumbo v1, "header"

    invoke-static {v1, p1}, Lcom/google/ipc/invalidation/ticl/a/am;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1890
    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->b:Lcom/google/ipc/invalidation/ticl/a/al;

    .line 1891
    if-eqz p2, :cond_0

    .line 1892
    const/4 v0, 0x1

    .line 1893
    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/a/am;->c:Lcom/google/ipc/invalidation/ticl/a/ao;

    .line 1897
    :goto_0
    iput-object p3, p0, Lcom/google/ipc/invalidation/ticl/a/am;->d:Lcom/google/ipc/invalidation/ticl/a/V;

    .line 1898
    iput-object p4, p0, Lcom/google/ipc/invalidation/ticl/a/am;->e:Lcom/google/ipc/invalidation/ticl/a/ag;

    .line 1899
    if-eqz p5, :cond_1

    .line 1900
    or-int/lit8 v0, v0, 0x2

    .line 1901
    iput-object p5, p0, Lcom/google/ipc/invalidation/ticl/a/am;->f:Lcom/google/ipc/invalidation/ticl/a/ak;

    .line 1905
    :goto_1
    if-eqz p6, :cond_2

    .line 1906
    or-int/lit8 v0, v0, 0x4

    .line 1907
    iput-object p6, p0, Lcom/google/ipc/invalidation/ticl/a/am;->g:Lcom/google/ipc/invalidation/ticl/a/Q;

    .line 1911
    :goto_2
    iput-object p7, p0, Lcom/google/ipc/invalidation/ticl/a/am;->h:Lcom/google/ipc/invalidation/ticl/a/T;

    .line 1912
    iput-object p8, p0, Lcom/google/ipc/invalidation/ticl/a/am;->i:Lcom/google/ipc/invalidation/ticl/a/R;

    .line 1913
    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/am;->a:J

    .line 1914
    return-void

    .line 1895
    :cond_0
    sget-object v1, Lcom/google/ipc/invalidation/ticl/a/ao;->a:Lcom/google/ipc/invalidation/ticl/a/ao;

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->c:Lcom/google/ipc/invalidation/ticl/a/ao;

    goto :goto_0

    .line 1903
    :cond_1
    sget-object v1, Lcom/google/ipc/invalidation/ticl/a/ak;->a:Lcom/google/ipc/invalidation/ticl/a/ak;

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->f:Lcom/google/ipc/invalidation/ticl/a/ak;

    goto :goto_1

    .line 1909
    :cond_2
    sget-object v1, Lcom/google/ipc/invalidation/ticl/a/Q;->a:Lcom/google/ipc/invalidation/ticl/a/Q;

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->g:Lcom/google/ipc/invalidation/ticl/a/Q;

    goto :goto_2
.end method

.method public static a([B)Lcom/google/ipc/invalidation/ticl/a/am;
    .locals 10

    .prologue
    .line 2032
    :try_start_0
    new-instance v1, Lcom/google/b/a/a/ah;

    invoke-direct {v1}, Lcom/google/b/a/a/ah;-><init>()V

    invoke-static {v1, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/google/b/a/a/ah;

    move-object v9, v0

    if-nez v9, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/google/ipc/invalidation/ticl/a/am;

    iget-object v2, v9, Lcom/google/b/a/a/ah;->a:Lcom/google/b/a/a/ag;

    invoke-static {v2}, Lcom/google/ipc/invalidation/ticl/a/al;->a(Lcom/google/b/a/a/ag;)Lcom/google/ipc/invalidation/ticl/a/al;

    move-result-object v2

    iget-object v3, v9, Lcom/google/b/a/a/ah;->b:Lcom/google/b/a/a/aj;

    invoke-static {v3}, Lcom/google/ipc/invalidation/ticl/a/ao;->a(Lcom/google/b/a/a/aj;)Lcom/google/ipc/invalidation/ticl/a/ao;

    move-result-object v3

    iget-object v4, v9, Lcom/google/b/a/a/ah;->c:Lcom/google/b/a/a/R;

    invoke-static {v4}, Lcom/google/ipc/invalidation/ticl/a/V;->a(Lcom/google/b/a/a/R;)Lcom/google/ipc/invalidation/ticl/a/V;

    move-result-object v4

    iget-object v5, v9, Lcom/google/b/a/a/ah;->d:Lcom/google/b/a/a/ab;

    invoke-static {v5}, Lcom/google/ipc/invalidation/ticl/a/ag;->a(Lcom/google/b/a/a/ab;)Lcom/google/ipc/invalidation/ticl/a/ag;

    move-result-object v5

    iget-object v6, v9, Lcom/google/b/a/a/ah;->e:Lcom/google/b/a/a/af;

    invoke-static {v6}, Lcom/google/ipc/invalidation/ticl/a/ak;->a(Lcom/google/b/a/a/af;)Lcom/google/ipc/invalidation/ticl/a/ak;

    move-result-object v6

    iget-object v7, v9, Lcom/google/b/a/a/ah;->f:Lcom/google/b/a/a/M;

    invoke-static {v7}, Lcom/google/ipc/invalidation/ticl/a/Q;->a(Lcom/google/b/a/a/M;)Lcom/google/ipc/invalidation/ticl/a/Q;

    move-result-object v7

    iget-object v8, v9, Lcom/google/b/a/a/ah;->g:Lcom/google/b/a/a/P;

    invoke-static {v8}, Lcom/google/ipc/invalidation/ticl/a/T;->a(Lcom/google/b/a/a/P;)Lcom/google/ipc/invalidation/ticl/a/T;

    move-result-object v8

    iget-object v9, v9, Lcom/google/b/a/a/ah;->h:Lcom/google/b/a/a/N;

    invoke-static {v9}, Lcom/google/ipc/invalidation/ticl/a/R;->a(Lcom/google/b/a/a/N;)Lcom/google/ipc/invalidation/ticl/a/R;

    move-result-object v9

    invoke-direct/range {v1 .. v9}, Lcom/google/ipc/invalidation/ticl/a/am;-><init>(Lcom/google/ipc/invalidation/ticl/a/al;Lcom/google/ipc/invalidation/ticl/a/ao;Lcom/google/ipc/invalidation/ticl/a/V;Lcom/google/ipc/invalidation/ticl/a/ag;Lcom/google/ipc/invalidation/ticl/a/ak;Lcom/google/ipc/invalidation/ticl/a/Q;Lcom/google/ipc/invalidation/ticl/a/T;Lcom/google/ipc/invalidation/ticl/a/R;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/ipc/invalidation/b/o; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 2033
    :catch_0
    move-exception v1

    .line 2034
    new-instance v2, Lcom/google/ipc/invalidation/b/p;

    invoke-direct {v2, v1}, Lcom/google/ipc/invalidation/b/p;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 2035
    :catch_1
    move-exception v1

    .line 2036
    new-instance v2, Lcom/google/ipc/invalidation/b/p;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/o;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/google/ipc/invalidation/b/p;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public final a()Lcom/google/ipc/invalidation/ticl/a/al;
    .locals 1

    .prologue
    .line 1916
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/am;->b:Lcom/google/ipc/invalidation/ticl/a/al;

    return-object v0
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 2

    .prologue
    .line 2004
    const-string/jumbo v0, "<ServerToClientMessage:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 2005
    const-string/jumbo v0, " header="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->b:Lcom/google/ipc/invalidation/ticl/a/al;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 2006
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/am;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2007
    const-string/jumbo v0, " token_control_message="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->c:Lcom/google/ipc/invalidation/ticl/a/ao;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 2009
    :cond_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/am;->d:Lcom/google/ipc/invalidation/ticl/a/V;

    if-eqz v0, :cond_1

    .line 2010
    const-string/jumbo v0, " invalidation_message="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->d:Lcom/google/ipc/invalidation/ticl/a/V;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 2012
    :cond_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/am;->e:Lcom/google/ipc/invalidation/ticl/a/ag;

    if-eqz v0, :cond_2

    .line 2013
    const-string/jumbo v0, " registration_status_message="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->e:Lcom/google/ipc/invalidation/ticl/a/ag;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 2015
    :cond_2
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/am;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2016
    const-string/jumbo v0, " registration_sync_request_message="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->f:Lcom/google/ipc/invalidation/ticl/a/ak;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 2018
    :cond_3
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/am;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2019
    const-string/jumbo v0, " config_change_message="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->g:Lcom/google/ipc/invalidation/ticl/a/Q;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 2021
    :cond_4
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/am;->h:Lcom/google/ipc/invalidation/ticl/a/T;

    if-eqz v0, :cond_5

    .line 2022
    const-string/jumbo v0, " info_request_message="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->h:Lcom/google/ipc/invalidation/ticl/a/T;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 2024
    :cond_5
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/am;->i:Lcom/google/ipc/invalidation/ticl/a/R;

    if-eqz v0, :cond_6

    .line 2025
    const-string/jumbo v0, " error_message="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->i:Lcom/google/ipc/invalidation/ticl/a/R;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 2027
    :cond_6
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 2028
    return-void
.end method

.method protected final b()I
    .locals 4

    .prologue
    .line 1977
    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/am;->a:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 1978
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->b:Lcom/google/ipc/invalidation/ticl/a/al;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/al;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1979
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/am;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1980
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->c:Lcom/google/ipc/invalidation/ticl/a/ao;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/ao;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1982
    :cond_0
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->d:Lcom/google/ipc/invalidation/ticl/a/V;

    if-eqz v1, :cond_1

    .line 1983
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->d:Lcom/google/ipc/invalidation/ticl/a/V;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/V;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1985
    :cond_1
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->e:Lcom/google/ipc/invalidation/ticl/a/ag;

    if-eqz v1, :cond_2

    .line 1986
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->e:Lcom/google/ipc/invalidation/ticl/a/ag;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/ag;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1988
    :cond_2
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/am;->h()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1989
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->f:Lcom/google/ipc/invalidation/ticl/a/ak;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/ak;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1991
    :cond_3
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/am;->j()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1992
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->g:Lcom/google/ipc/invalidation/ticl/a/Q;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/Q;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1994
    :cond_4
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->h:Lcom/google/ipc/invalidation/ticl/a/T;

    if-eqz v1, :cond_5

    .line 1995
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->h:Lcom/google/ipc/invalidation/ticl/a/T;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/T;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1997
    :cond_5
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->i:Lcom/google/ipc/invalidation/ticl/a/R;

    if-eqz v1, :cond_6

    .line 1998
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/am;->i:Lcom/google/ipc/invalidation/ticl/a/R;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/R;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2000
    :cond_6
    return v0
.end method

.method public final c()Lcom/google/ipc/invalidation/ticl/a/ao;
    .locals 1

    .prologue
    .line 1918
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/am;->c:Lcom/google/ipc/invalidation/ticl/a/ao;

    return-object v0
.end method

.method public final d()Z
    .locals 4

    .prologue
    .line 1919
    const-wide/16 v0, 0x1

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/am;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lcom/google/ipc/invalidation/ticl/a/V;
    .locals 1

    .prologue
    .line 1921
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/am;->d:Lcom/google/ipc/invalidation/ticl/a/V;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1962
    if-ne p0, p1, :cond_1

    .line 1965
    :cond_0
    :goto_0
    return v0

    .line 1963
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/ticl/a/am;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 1964
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/am;

    .line 1965
    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/am;->a:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/am;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/am;->b:Lcom/google/ipc/invalidation/ticl/a/al;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/am;->b:Lcom/google/ipc/invalidation/ticl/a/al;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/am;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/am;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/am;->c:Lcom/google/ipc/invalidation/ticl/a/ao;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/am;->c:Lcom/google/ipc/invalidation/ticl/a/ao;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/am;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_3
    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/am;->d:Lcom/google/ipc/invalidation/ticl/a/V;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/am;->d:Lcom/google/ipc/invalidation/ticl/a/V;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/am;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/am;->e:Lcom/google/ipc/invalidation/ticl/a/ag;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/am;->e:Lcom/google/ipc/invalidation/ticl/a/ag;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/am;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/am;->h()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/am;->f:Lcom/google/ipc/invalidation/ticl/a/ak;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/am;->f:Lcom/google/ipc/invalidation/ticl/a/ak;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/am;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_4
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/am;->j()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/am;->g:Lcom/google/ipc/invalidation/ticl/a/Q;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/am;->g:Lcom/google/ipc/invalidation/ticl/a/Q;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/am;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/am;->h:Lcom/google/ipc/invalidation/ticl/a/T;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/am;->h:Lcom/google/ipc/invalidation/ticl/a/T;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/am;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/am;->i:Lcom/google/ipc/invalidation/ticl/a/R;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/am;->i:Lcom/google/ipc/invalidation/ticl/a/R;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/am;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final f()Lcom/google/ipc/invalidation/ticl/a/ag;
    .locals 1

    .prologue
    .line 1923
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/am;->e:Lcom/google/ipc/invalidation/ticl/a/ag;

    return-object v0
.end method

.method public final g()Lcom/google/ipc/invalidation/ticl/a/ak;
    .locals 1

    .prologue
    .line 1925
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/am;->f:Lcom/google/ipc/invalidation/ticl/a/ak;

    return-object v0
.end method

.method public final h()Z
    .locals 4

    .prologue
    .line 1926
    const-wide/16 v0, 0x2

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/am;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Lcom/google/ipc/invalidation/ticl/a/Q;
    .locals 1

    .prologue
    .line 1928
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/am;->g:Lcom/google/ipc/invalidation/ticl/a/Q;

    return-object v0
.end method

.method public final j()Z
    .locals 4

    .prologue
    .line 1929
    const-wide/16 v0, 0x4

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/am;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Lcom/google/ipc/invalidation/ticl/a/T;
    .locals 1

    .prologue
    .line 1931
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/am;->h:Lcom/google/ipc/invalidation/ticl/a/T;

    return-object v0
.end method

.method public final l()Lcom/google/ipc/invalidation/ticl/a/R;
    .locals 1

    .prologue
    .line 1933
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/am;->i:Lcom/google/ipc/invalidation/ticl/a/R;

    return-object v0
.end method
