.class public final Lcom/google/ipc/invalidation/ticl/a/al;
.super Lcom/google/ipc/invalidation/b/n;
.source "ClientProtocol.java"


# instance fields
.field private final a:J

.field private final b:Lcom/google/ipc/invalidation/ticl/a/ab;

.field private final c:Lcom/google/ipc/invalidation/b/c;

.field private final d:Lcom/google/ipc/invalidation/ticl/a/ai;

.field private final e:J

.field private final f:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/ipc/invalidation/ticl/a/ab;Lcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/ticl/a/ai;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1724
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 1725
    const/4 v0, 0x0

    .line 1726
    const-string/jumbo v1, "protocol_version"

    invoke-static {v1, p1}, Lcom/google/ipc/invalidation/ticl/a/al;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1727
    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/al;->b:Lcom/google/ipc/invalidation/ticl/a/ab;

    .line 1728
    const-string/jumbo v1, "client_token"

    invoke-static {v1, p2}, Lcom/google/ipc/invalidation/ticl/a/al;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1729
    const-string/jumbo v1, "client_token"

    invoke-static {v1, p2}, Lcom/google/ipc/invalidation/ticl/a/al;->a(Ljava/lang/String;Lcom/google/ipc/invalidation/b/c;)V

    .line 1730
    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/a/al;->c:Lcom/google/ipc/invalidation/b/c;

    .line 1731
    iput-object p3, p0, Lcom/google/ipc/invalidation/ticl/a/al;->d:Lcom/google/ipc/invalidation/ticl/a/ai;

    .line 1732
    const-string/jumbo v1, "server_time_ms"

    invoke-static {v1, p4}, Lcom/google/ipc/invalidation/ticl/a/al;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1733
    const-string/jumbo v1, "server_time_ms"

    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/ipc/invalidation/ticl/a/al;->a(Ljava/lang/String;J)V

    .line 1734
    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/al;->e:J

    .line 1735
    if-eqz p5, :cond_0

    .line 1736
    const/4 v0, 0x1

    .line 1737
    const-string/jumbo v1, "message_id"

    invoke-static {v1, p5}, Lcom/google/ipc/invalidation/ticl/a/al;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1738
    iput-object p5, p0, Lcom/google/ipc/invalidation/ticl/a/al;->f:Ljava/lang/String;

    .line 1742
    :goto_0
    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/al;->a:J

    .line 1743
    return-void

    .line 1740
    :cond_0
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/al;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method static a(Lcom/google/b/a/a/ag;)Lcom/google/ipc/invalidation/ticl/a/al;
    .locals 6

    .prologue
    .line 1818
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 1819
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/al;

    iget-object v1, p0, Lcom/google/b/a/a/ag;->a:Lcom/google/b/a/a/W;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/ab;->a(Lcom/google/b/a/a/W;)Lcom/google/ipc/invalidation/ticl/a/ab;

    move-result-object v1

    iget-object v2, p0, Lcom/google/b/a/a/ag;->b:[B

    invoke-static {v2}, Lcom/google/ipc/invalidation/b/c;->a([B)Lcom/google/ipc/invalidation/b/c;

    move-result-object v2

    iget-object v3, p0, Lcom/google/b/a/a/ag;->c:Lcom/google/b/a/a/ad;

    invoke-static {v3}, Lcom/google/ipc/invalidation/ticl/a/ai;->a(Lcom/google/b/a/a/ad;)Lcom/google/ipc/invalidation/ticl/a/ai;

    move-result-object v3

    iget-object v4, p0, Lcom/google/b/a/a/ag;->d:Ljava/lang/Long;

    iget-object v5, p0, Lcom/google/b/a/a/ag;->e:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/google/ipc/invalidation/ticl/a/al;-><init>(Lcom/google/ipc/invalidation/ticl/a/ab;Lcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/ticl/a/ai;Ljava/lang/Long;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private f()Z
    .locals 4

    .prologue
    .line 1754
    const-wide/16 v0, 0x1

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/al;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/ipc/invalidation/ticl/a/ab;
    .locals 1

    .prologue
    .line 1745
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/al;->b:Lcom/google/ipc/invalidation/ticl/a/ab;

    return-object v0
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 4

    .prologue
    .line 1794
    const-string/jumbo v0, "<ServerHeader:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 1795
    const-string/jumbo v0, " protocol_version="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/al;->b:Lcom/google/ipc/invalidation/ticl/a/ab;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1796
    const-string/jumbo v0, " client_token="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/al;->c:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1797
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/al;->d:Lcom/google/ipc/invalidation/ticl/a/ai;

    if-eqz v0, :cond_0

    .line 1798
    const-string/jumbo v0, " registration_summary="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/al;->d:Lcom/google/ipc/invalidation/ticl/a/ai;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1800
    :cond_0
    const-string/jumbo v0, " server_time_ms="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/al;->e:J

    invoke-virtual {v0, v2, v3}, Lcom/google/ipc/invalidation/b/r;->a(J)Lcom/google/ipc/invalidation/b/r;

    .line 1801
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/al;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1802
    const-string/jumbo v0, " message_id="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/al;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 1804
    :cond_1
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 1805
    return-void
.end method

.method protected final b()I
    .locals 6

    .prologue
    const/16 v4, 0x20

    .line 1780
    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/al;->a:J

    ushr-long v2, v0, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 1781
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/al;->b:Lcom/google/ipc/invalidation/ticl/a/ab;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/ab;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1782
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/al;->c:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1783
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/al;->d:Lcom/google/ipc/invalidation/ticl/a/ai;

    if-eqz v1, :cond_0

    .line 1784
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/al;->d:Lcom/google/ipc/invalidation/ticl/a/ai;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/ai;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1786
    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/al;->e:J

    ushr-long v4, v2, v4

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 1787
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/al;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1788
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/al;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1790
    :cond_1
    return v0
.end method

.method public final c()Lcom/google/ipc/invalidation/b/c;
    .locals 1

    .prologue
    .line 1747
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/al;->c:Lcom/google/ipc/invalidation/b/c;

    return-object v0
.end method

.method public final d()Lcom/google/ipc/invalidation/ticl/a/ai;
    .locals 1

    .prologue
    .line 1749
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/al;->d:Lcom/google/ipc/invalidation/ticl/a/ai;

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 1751
    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/al;->e:J

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1768
    if-ne p0, p1, :cond_1

    .line 1771
    :cond_0
    :goto_0
    return v0

    .line 1769
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/ticl/a/al;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 1770
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/al;

    .line 1771
    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/al;->a:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/al;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/al;->b:Lcom/google/ipc/invalidation/ticl/a/ab;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/al;->b:Lcom/google/ipc/invalidation/ticl/a/ab;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/al;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/al;->c:Lcom/google/ipc/invalidation/b/c;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/al;->c:Lcom/google/ipc/invalidation/b/c;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/al;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/al;->d:Lcom/google/ipc/invalidation/ticl/a/ai;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/al;->d:Lcom/google/ipc/invalidation/ticl/a/ai;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/al;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/al;->e:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/al;->e:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/al;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/al;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/al;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/al;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
