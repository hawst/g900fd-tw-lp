.class public final Lcom/google/ipc/invalidation/ticl/a/ad;
.super Lcom/google/ipc/invalidation/b/n;
.source "ClientProtocol.java"


# instance fields
.field private final a:Ljava/util/List;


# direct methods
.method private constructor <init>(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 1263
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 1264
    const-string/jumbo v0, "registration"

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/a/ad;->b(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/ad;->a:Ljava/util/List;

    .line 1265
    return-void
.end method

.method public static a(Ljava/util/Collection;)Lcom/google/ipc/invalidation/ticl/a/ad;
    .locals 1

    .prologue
    .line 1258
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/ad;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/ticl/a/ad;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method


# virtual methods
.method final a()Lcom/google/b/a/a/Y;
    .locals 4

    .prologue
    .line 1312
    new-instance v2, Lcom/google/b/a/a/Y;

    invoke-direct {v2}, Lcom/google/b/a/a/Y;-><init>()V

    .line 1313
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/ad;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/b/a/a/Z;

    iput-object v0, v2, Lcom/google/b/a/a/Y;->a:[Lcom/google/b/a/a/Z;

    .line 1314
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, v2, Lcom/google/b/a/a/Y;->a:[Lcom/google/b/a/a/Z;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 1315
    iget-object v3, v2, Lcom/google/b/a/a/Y;->a:[Lcom/google/b/a/a/Z;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/ad;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/ae;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/ae;->d()Lcom/google/b/a/a/Z;

    move-result-object v0

    aput-object v0, v3, v1

    .line 1314
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1317
    :cond_0
    return-object v2
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 2

    .prologue
    .line 1283
    const-string/jumbo v0, "<RegistrationMessage:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 1284
    const-string/jumbo v0, " registration=["

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/ad;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/Iterable;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 1285
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 1286
    return-void
.end method

.method protected final b()I
    .locals 1

    .prologue
    .line 1277
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/ad;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 1279
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1270
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    .line 1273
    :goto_0
    return v0

    .line 1271
    :cond_0
    instance-of v0, p1, Lcom/google/ipc/invalidation/ticl/a/ad;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1272
    :cond_1
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/ad;

    .line 1273
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/ad;->a:Ljava/util/List;

    iget-object v1, p1, Lcom/google/ipc/invalidation/ticl/a/ad;->a:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
