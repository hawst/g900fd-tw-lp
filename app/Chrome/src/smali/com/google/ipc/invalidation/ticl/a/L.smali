.class public final Lcom/google/ipc/invalidation/ticl/a/L;
.super Lcom/google/ipc/invalidation/b/n;
.source "ClientProtocol.java"


# instance fields
.field private final a:J

.field private final b:Lcom/google/ipc/invalidation/ticl/a/ap;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:Z

.field private final j:I

.field private final k:Lcom/google/ipc/invalidation/ticl/a/aa;

.field private final l:Z

.field private final m:I

.field private final n:Z


# direct methods
.method private constructor <init>(Lcom/google/ipc/invalidation/ticl/a/ap;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Lcom/google/ipc/invalidation/ticl/a/aa;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 2721
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 2722
    const/4 v0, 0x0

    .line 2723
    const-string/jumbo v1, "version"

    invoke-static {v1, p1}, Lcom/google/ipc/invalidation/ticl/a/L;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2724
    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->b:Lcom/google/ipc/invalidation/ticl/a/ap;

    .line 2725
    if-eqz p2, :cond_0

    .line 2726
    const/4 v0, 0x1

    .line 2727
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->c:I

    .line 2731
    :goto_0
    if-eqz p3, :cond_1

    .line 2732
    or-int/lit8 v0, v0, 0x2

    .line 2733
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->d:I

    .line 2737
    :goto_1
    if-eqz p4, :cond_2

    .line 2738
    or-int/lit8 v0, v0, 0x4

    .line 2739
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->e:I

    .line 2743
    :goto_2
    if-eqz p5, :cond_3

    .line 2744
    or-int/lit8 v0, v0, 0x8

    .line 2745
    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->f:I

    .line 2749
    :goto_3
    if-eqz p6, :cond_4

    .line 2750
    or-int/lit8 v0, v0, 0x10

    .line 2751
    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->g:I

    .line 2755
    :goto_4
    if-eqz p7, :cond_5

    .line 2756
    or-int/lit8 v0, v0, 0x20

    .line 2757
    invoke-virtual {p7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->h:I

    .line 2761
    :goto_5
    if-eqz p8, :cond_6

    .line 2762
    or-int/lit8 v0, v0, 0x40

    .line 2763
    invoke-virtual {p8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->i:Z

    .line 2767
    :goto_6
    if-eqz p9, :cond_7

    .line 2768
    or-int/lit16 v0, v0, 0x80

    .line 2769
    invoke-virtual {p9}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->j:I

    .line 2773
    :goto_7
    const-string/jumbo v1, "protocol_handler_config"

    invoke-static {v1, p10}, Lcom/google/ipc/invalidation/ticl/a/L;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2774
    iput-object p10, p0, Lcom/google/ipc/invalidation/ticl/a/L;->k:Lcom/google/ipc/invalidation/ticl/a/aa;

    .line 2775
    if-eqz p11, :cond_8

    .line 2776
    or-int/lit16 v0, v0, 0x100

    .line 2777
    invoke-virtual {p11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->l:Z

    .line 2781
    :goto_8
    if-eqz p12, :cond_9

    .line 2782
    or-int/lit16 v0, v0, 0x200

    .line 2783
    invoke-virtual {p12}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->m:I

    .line 2787
    :goto_9
    if-eqz p13, :cond_a

    .line 2788
    or-int/lit16 v0, v0, 0x400

    .line 2789
    invoke-virtual {p13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->n:Z

    .line 2793
    :goto_a
    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->a:J

    .line 2794
    return-void

    .line 2729
    :cond_0
    const v1, 0xea60

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->c:I

    goto :goto_0

    .line 2735
    :cond_1
    const/16 v1, 0x2710

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->d:I

    goto :goto_1

    .line 2741
    :cond_2
    const v1, 0x124f80

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->e:I

    goto :goto_2

    .line 2747
    :cond_3
    const v1, 0x1499700

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->f:I

    goto :goto_3

    .line 2753
    :cond_4
    const/16 v1, 0x1f4

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->g:I

    goto :goto_4

    .line 2759
    :cond_5
    const/16 v1, 0x14

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->h:I

    goto :goto_5

    .line 2765
    :cond_6
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->i:Z

    goto :goto_6

    .line 2771
    :cond_7
    const/16 v1, 0x7d0

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->j:I

    goto :goto_7

    .line 2779
    :cond_8
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->l:Z

    goto :goto_8

    .line 2785
    :cond_9
    const v1, 0xea60

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->m:I

    goto :goto_9

    .line 2791
    :cond_a
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->n:Z

    goto :goto_a
.end method

.method synthetic constructor <init>(Lcom/google/ipc/invalidation/ticl/a/ap;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Lcom/google/ipc/invalidation/ticl/a/aa;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Boolean;B)V
    .locals 0

    .prologue
    .line 2654
    invoke-direct/range {p0 .. p13}, Lcom/google/ipc/invalidation/ticl/a/L;-><init>(Lcom/google/ipc/invalidation/ticl/a/ap;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Lcom/google/ipc/invalidation/ticl/a/aa;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    return-void
.end method

.method static a(Lcom/google/b/a/a/I;)Lcom/google/ipc/invalidation/ticl/a/L;
    .locals 14

    .prologue
    .line 2982
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 2983
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/L;

    iget-object v1, p0, Lcom/google/b/a/a/I;->a:Lcom/google/b/a/a/ak;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/ap;->a(Lcom/google/b/a/a/ak;)Lcom/google/ipc/invalidation/ticl/a/ap;

    move-result-object v1

    iget-object v2, p0, Lcom/google/b/a/a/I;->b:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/google/b/a/a/I;->c:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/google/b/a/a/I;->d:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/google/b/a/a/I;->e:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/google/b/a/a/I;->f:Ljava/lang/Integer;

    iget-object v7, p0, Lcom/google/b/a/a/I;->g:Ljava/lang/Integer;

    iget-object v8, p0, Lcom/google/b/a/a/I;->h:Ljava/lang/Boolean;

    iget-object v9, p0, Lcom/google/b/a/a/I;->i:Ljava/lang/Integer;

    iget-object v10, p0, Lcom/google/b/a/a/I;->j:Lcom/google/b/a/a/V;

    invoke-static {v10}, Lcom/google/ipc/invalidation/ticl/a/aa;->a(Lcom/google/b/a/a/V;)Lcom/google/ipc/invalidation/ticl/a/aa;

    move-result-object v10

    iget-object v11, p0, Lcom/google/b/a/a/I;->k:Ljava/lang/Boolean;

    iget-object v12, p0, Lcom/google/b/a/a/I;->l:Ljava/lang/Integer;

    iget-object v13, p0, Lcom/google/b/a/a/I;->m:Ljava/lang/Boolean;

    invoke-direct/range {v0 .. v13}, Lcom/google/ipc/invalidation/ticl/a/L;-><init>(Lcom/google/ipc/invalidation/ticl/a/ap;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Lcom/google/ipc/invalidation/ticl/a/aa;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method private o()Z
    .locals 4

    .prologue
    .line 2799
    const-wide/16 v0, 0x1

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()Z
    .locals 4

    .prologue
    .line 2802
    const-wide/16 v0, 0x2

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()Z
    .locals 4

    .prologue
    .line 2805
    const-wide/16 v0, 0x4

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()Z
    .locals 4

    .prologue
    .line 2808
    const-wide/16 v0, 0x8

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private s()Z
    .locals 4

    .prologue
    .line 2811
    const-wide/16 v0, 0x10

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()Z
    .locals 4

    .prologue
    .line 2814
    const-wide/16 v0, 0x20

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()Z
    .locals 4

    .prologue
    .line 2817
    const-wide/16 v0, 0x40

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private w()Z
    .locals 4

    .prologue
    .line 2820
    const-wide/16 v0, 0x80

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private x()Z
    .locals 4

    .prologue
    .line 2825
    const-wide/16 v0, 0x100

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private y()Z
    .locals 4

    .prologue
    .line 2828
    const-wide/16 v0, 0x200

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private z()Z
    .locals 4

    .prologue
    .line 2831
    const-wide/16 v0, 0x400

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2798
    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->c:I

    return v0
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 2

    .prologue
    .line 2932
    const-string/jumbo v0, "<ClientConfigP:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 2933
    const-string/jumbo v0, " version="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->b:Lcom/google/ipc/invalidation/ticl/a/ap;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 2934
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2935
    const-string/jumbo v0, " network_timeout_delay_ms="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->c:I

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(I)Lcom/google/ipc/invalidation/b/r;

    .line 2937
    :cond_0
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2938
    const-string/jumbo v0, " write_retry_delay_ms="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->d:I

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(I)Lcom/google/ipc/invalidation/b/r;

    .line 2940
    :cond_1
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2941
    const-string/jumbo v0, " heartbeat_interval_ms="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->e:I

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(I)Lcom/google/ipc/invalidation/b/r;

    .line 2943
    :cond_2
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->r()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2944
    const-string/jumbo v0, " perf_counter_delay_ms="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->f:I

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(I)Lcom/google/ipc/invalidation/b/r;

    .line 2946
    :cond_3
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->s()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2947
    const-string/jumbo v0, " max_exponential_backoff_factor="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->g:I

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(I)Lcom/google/ipc/invalidation/b/r;

    .line 2949
    :cond_4
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->t()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2950
    const-string/jumbo v0, " smear_percent="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->h:I

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(I)Lcom/google/ipc/invalidation/b/r;

    .line 2952
    :cond_5
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->v()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2953
    const-string/jumbo v0, " is_transient="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->i:Z

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Z)Lcom/google/ipc/invalidation/b/r;

    .line 2955
    :cond_6
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->w()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2956
    const-string/jumbo v0, " initial_persistent_heartbeat_delay_ms="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->j:I

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(I)Lcom/google/ipc/invalidation/b/r;

    .line 2958
    :cond_7
    const-string/jumbo v0, " protocol_handler_config="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->k:Lcom/google/ipc/invalidation/ticl/a/aa;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 2959
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->x()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2960
    const-string/jumbo v0, " channel_supports_offline_delivery="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->l:Z

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Z)Lcom/google/ipc/invalidation/b/r;

    .line 2962
    :cond_8
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->y()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2963
    const-string/jumbo v0, " offline_heartbeat_threshold_ms="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->m:I

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(I)Lcom/google/ipc/invalidation/b/r;

    .line 2965
    :cond_9
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->z()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2966
    const-string/jumbo v0, " allow_suppression="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->n:Z

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Z)Lcom/google/ipc/invalidation/b/r;

    .line 2968
    :cond_a
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 2969
    return-void
.end method

.method protected final b()I
    .locals 4

    .prologue
    .line 2892
    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->a:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 2893
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->b:Lcom/google/ipc/invalidation/ticl/a/ap;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/ap;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2894
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2895
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->c:I

    add-int/2addr v0, v1

    .line 2897
    :cond_0
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2898
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->d:I

    add-int/2addr v0, v1

    .line 2900
    :cond_1
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->q()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2901
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->e:I

    add-int/2addr v0, v1

    .line 2903
    :cond_2
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->r()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2904
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->f:I

    add-int/2addr v0, v1

    .line 2906
    :cond_3
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->s()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2907
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->g:I

    add-int/2addr v0, v1

    .line 2909
    :cond_4
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->t()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2910
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->h:I

    add-int/2addr v0, v1

    .line 2912
    :cond_5
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->v()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2913
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->i:Z

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/L;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 2915
    :cond_6
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->w()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2916
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->j:I

    add-int/2addr v0, v1

    .line 2918
    :cond_7
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->k:Lcom/google/ipc/invalidation/ticl/a/aa;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/aa;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2919
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->x()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2920
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->l:Z

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/L;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 2922
    :cond_8
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->y()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2923
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->m:I

    add-int/2addr v0, v1

    .line 2925
    :cond_9
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->z()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2926
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->n:Z

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/L;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 2928
    :cond_a
    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2801
    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->d:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2804
    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->e:I

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2807
    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->f:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2872
    if-ne p0, p1, :cond_1

    .line 2875
    :cond_0
    :goto_0
    return v0

    .line 2873
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/ticl/a/L;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 2874
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/L;

    .line 2875
    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->a:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/L;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_d

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->b:Lcom/google/ipc/invalidation/ticl/a/ap;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/L;->b:Lcom/google/ipc/invalidation/ticl/a/ap;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/L;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->o()Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->c:I

    iget v3, p1, Lcom/google/ipc/invalidation/ticl/a/L;->c:I

    if-ne v2, v3, :cond_d

    :cond_3
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->p()Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->d:I

    iget v3, p1, Lcom/google/ipc/invalidation/ticl/a/L;->d:I

    if-ne v2, v3, :cond_d

    :cond_4
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->q()Z

    move-result v2

    if-eqz v2, :cond_5

    iget v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->e:I

    iget v3, p1, Lcom/google/ipc/invalidation/ticl/a/L;->e:I

    if-ne v2, v3, :cond_d

    :cond_5
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->r()Z

    move-result v2

    if-eqz v2, :cond_6

    iget v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->f:I

    iget v3, p1, Lcom/google/ipc/invalidation/ticl/a/L;->f:I

    if-ne v2, v3, :cond_d

    :cond_6
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->s()Z

    move-result v2

    if-eqz v2, :cond_7

    iget v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->g:I

    iget v3, p1, Lcom/google/ipc/invalidation/ticl/a/L;->g:I

    if-ne v2, v3, :cond_d

    :cond_7
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->t()Z

    move-result v2

    if-eqz v2, :cond_8

    iget v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->h:I

    iget v3, p1, Lcom/google/ipc/invalidation/ticl/a/L;->h:I

    if-ne v2, v3, :cond_d

    :cond_8
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->v()Z

    move-result v2

    if-eqz v2, :cond_9

    iget-boolean v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->i:Z

    iget-boolean v3, p1, Lcom/google/ipc/invalidation/ticl/a/L;->i:Z

    if-ne v2, v3, :cond_d

    :cond_9
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->w()Z

    move-result v2

    if-eqz v2, :cond_a

    iget v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->j:I

    iget v3, p1, Lcom/google/ipc/invalidation/ticl/a/L;->j:I

    if-ne v2, v3, :cond_d

    :cond_a
    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->k:Lcom/google/ipc/invalidation/ticl/a/aa;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/L;->k:Lcom/google/ipc/invalidation/ticl/a/aa;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/L;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->x()Z

    move-result v2

    if-eqz v2, :cond_b

    iget-boolean v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->l:Z

    iget-boolean v3, p1, Lcom/google/ipc/invalidation/ticl/a/L;->l:Z

    if-ne v2, v3, :cond_d

    :cond_b
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->y()Z

    move-result v2

    if-eqz v2, :cond_c

    iget v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->m:I

    iget v3, p1, Lcom/google/ipc/invalidation/ticl/a/L;->m:I

    if-ne v2, v3, :cond_d

    :cond_c
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->z()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->n:Z

    iget-boolean v3, p1, Lcom/google/ipc/invalidation/ticl/a/L;->n:Z

    if-eq v2, v3, :cond_0

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2810
    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->g:I

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 2813
    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->h:I

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 2819
    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->j:I

    return v0
.end method

.method public final i()Lcom/google/ipc/invalidation/ticl/a/aa;
    .locals 1

    .prologue
    .line 2822
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->k:Lcom/google/ipc/invalidation/ticl/a/aa;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2824
    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->l:Z

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2827
    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->m:I

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2830
    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->n:Z

    return v0
.end method

.method public final m()Lcom/google/ipc/invalidation/ticl/a/M;
    .locals 3

    .prologue
    .line 2834
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/M;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->b:Lcom/google/ipc/invalidation/ticl/a/ap;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/L;->k:Lcom/google/ipc/invalidation/ticl/a/aa;

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/ticl/a/M;-><init>(Lcom/google/ipc/invalidation/ticl/a/ap;Lcom/google/ipc/invalidation/ticl/a/aa;)V

    .line 2835
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2836
    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ipc/invalidation/ticl/a/M;->a:Ljava/lang/Integer;

    .line 2838
    :cond_0
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2839
    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ipc/invalidation/ticl/a/M;->b:Ljava/lang/Integer;

    .line 2841
    :cond_1
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->q()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2842
    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ipc/invalidation/ticl/a/M;->c:Ljava/lang/Integer;

    .line 2844
    :cond_2
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->r()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2845
    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ipc/invalidation/ticl/a/M;->d:Ljava/lang/Integer;

    .line 2847
    :cond_3
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->s()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2848
    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ipc/invalidation/ticl/a/M;->e:Ljava/lang/Integer;

    .line 2850
    :cond_4
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->t()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2851
    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->h:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ipc/invalidation/ticl/a/M;->f:Ljava/lang/Integer;

    .line 2853
    :cond_5
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->v()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2854
    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->i:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ipc/invalidation/ticl/a/M;->g:Ljava/lang/Boolean;

    .line 2856
    :cond_6
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->w()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2857
    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ipc/invalidation/ticl/a/M;->h:Ljava/lang/Integer;

    .line 2859
    :cond_7
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->x()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2860
    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->l:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ipc/invalidation/ticl/a/M;->i:Ljava/lang/Boolean;

    .line 2862
    :cond_8
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->y()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2863
    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->m:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ipc/invalidation/ticl/a/M;->j:Ljava/lang/Integer;

    .line 2865
    :cond_9
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->z()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2866
    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/L;->n:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ipc/invalidation/ticl/a/M;->k:Ljava/lang/Boolean;

    .line 2868
    :cond_a
    return-object v0
.end method

.method final n()Lcom/google/b/a/a/I;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3003
    new-instance v2, Lcom/google/b/a/a/I;

    invoke-direct {v2}, Lcom/google/b/a/a/I;-><init>()V

    .line 3004
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->b:Lcom/google/ipc/invalidation/ticl/a/ap;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/ap;->c()Lcom/google/b/a/a/ak;

    move-result-object v0

    iput-object v0, v2, Lcom/google/b/a/a/I;->a:Lcom/google/b/a/a/ak;

    .line 3005
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    iput-object v0, v2, Lcom/google/b/a/a/I;->b:Ljava/lang/Integer;

    .line 3006
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_1
    iput-object v0, v2, Lcom/google/b/a/a/I;->c:Ljava/lang/Integer;

    .line 3007
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_2
    iput-object v0, v2, Lcom/google/b/a/a/I;->d:Ljava/lang/Integer;

    .line 3008
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->r()Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_3
    iput-object v0, v2, Lcom/google/b/a/a/I;->e:Ljava/lang/Integer;

    .line 3009
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->s()Z

    move-result v0

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_4
    iput-object v0, v2, Lcom/google/b/a/a/I;->f:Ljava/lang/Integer;

    .line 3010
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->t()Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_5
    iput-object v0, v2, Lcom/google/b/a/a/I;->g:Ljava/lang/Integer;

    .line 3011
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->v()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_6
    iput-object v0, v2, Lcom/google/b/a/a/I;->h:Ljava/lang/Boolean;

    .line 3012
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->w()Z

    move-result v0

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->j:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_7
    iput-object v0, v2, Lcom/google/b/a/a/I;->i:Ljava/lang/Integer;

    .line 3013
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->k:Lcom/google/ipc/invalidation/ticl/a/aa;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/aa;->c()Lcom/google/b/a/a/V;

    move-result-object v0

    iput-object v0, v2, Lcom/google/b/a/a/I;->j:Lcom/google/b/a/a/V;

    .line 3014
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->x()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->l:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_8
    iput-object v0, v2, Lcom/google/b/a/a/I;->k:Ljava/lang/Boolean;

    .line 3015
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->y()Z

    move-result v0

    if-eqz v0, :cond_a

    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->m:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_9
    iput-object v0, v2, Lcom/google/b/a/a/I;->l:Ljava/lang/Integer;

    .line 3016
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/L;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/L;->n:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :cond_0
    iput-object v1, v2, Lcom/google/b/a/a/I;->m:Ljava/lang/Boolean;

    .line 3017
    return-object v2

    :cond_1
    move-object v0, v1

    .line 3005
    goto/16 :goto_0

    :cond_2
    move-object v0, v1

    .line 3006
    goto/16 :goto_1

    :cond_3
    move-object v0, v1

    .line 3007
    goto/16 :goto_2

    :cond_4
    move-object v0, v1

    .line 3008
    goto :goto_3

    :cond_5
    move-object v0, v1

    .line 3009
    goto :goto_4

    :cond_6
    move-object v0, v1

    .line 3010
    goto :goto_5

    :cond_7
    move-object v0, v1

    .line 3011
    goto :goto_6

    :cond_8
    move-object v0, v1

    .line 3012
    goto :goto_7

    :cond_9
    move-object v0, v1

    .line 3014
    goto :goto_8

    :cond_a
    move-object v0, v1

    .line 3015
    goto :goto_9
.end method
