.class public final Lcom/google/ipc/invalidation/ticl/a/j;
.super Lcom/google/ipc/invalidation/b/n;
.source "AndroidService.java"


# instance fields
.field private final a:I

.field private final b:Lcom/google/ipc/invalidation/b/c;

.field private final c:J

.field private final d:Lcom/google/ipc/invalidation/ticl/a/L;


# direct methods
.method private constructor <init>(Ljava/lang/Integer;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Long;Lcom/google/ipc/invalidation/ticl/a/L;)V
    .locals 2

    .prologue
    .line 1779
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 1780
    const-string/jumbo v0, "client_type"

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/a/j;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1781
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/ipc/invalidation/ticl/a/j;->a:I

    .line 1782
    const-string/jumbo v0, "client_name"

    invoke-static {v0, p2}, Lcom/google/ipc/invalidation/ticl/a/j;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1783
    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/a/j;->b:Lcom/google/ipc/invalidation/b/c;

    .line 1784
    const-string/jumbo v0, "ticl_id"

    invoke-static {v0, p3}, Lcom/google/ipc/invalidation/ticl/a/j;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1785
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/j;->c:J

    .line 1786
    const-string/jumbo v0, "client_config"

    invoke-static {v0, p4}, Lcom/google/ipc/invalidation/ticl/a/j;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1787
    iput-object p4, p0, Lcom/google/ipc/invalidation/ticl/a/j;->d:Lcom/google/ipc/invalidation/ticl/a/L;

    .line 1788
    return-void
.end method

.method public static a(ILcom/google/ipc/invalidation/b/c;JLcom/google/ipc/invalidation/ticl/a/L;)Lcom/google/ipc/invalidation/ticl/a/j;
    .locals 4

    .prologue
    .line 1768
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/j;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2, p4}, Lcom/google/ipc/invalidation/ticl/a/j;-><init>(Ljava/lang/Integer;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Long;Lcom/google/ipc/invalidation/ticl/a/L;)V

    return-object v0
.end method

.method static a(Lcom/google/b/a/a/j;)Lcom/google/ipc/invalidation/ticl/a/j;
    .locals 5

    .prologue
    .line 1837
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 1838
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/j;

    iget-object v1, p0, Lcom/google/b/a/a/j;->a:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/google/b/a/a/j;->b:[B

    invoke-static {v2}, Lcom/google/ipc/invalidation/b/c;->a([B)Lcom/google/ipc/invalidation/b/c;

    move-result-object v2

    iget-object v3, p0, Lcom/google/b/a/a/j;->c:Ljava/lang/Long;

    iget-object v4, p0, Lcom/google/b/a/a/j;->d:Lcom/google/b/a/a/I;

    invoke-static {v4}, Lcom/google/ipc/invalidation/ticl/a/L;->a(Lcom/google/b/a/a/I;)Lcom/google/ipc/invalidation/ticl/a/L;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/ipc/invalidation/ticl/a/j;-><init>(Ljava/lang/Integer;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Long;Lcom/google/ipc/invalidation/ticl/a/L;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1790
    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/j;->a:I

    return v0
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 4

    .prologue
    .line 1818
    const-string/jumbo v0, "<Metadata:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 1819
    const-string/jumbo v0, " client_type="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/j;->a:I

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(I)Lcom/google/ipc/invalidation/b/r;

    .line 1820
    const-string/jumbo v0, " client_name="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/j;->b:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1821
    const-string/jumbo v0, " ticl_id="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/j;->c:J

    invoke-virtual {v0, v2, v3}, Lcom/google/ipc/invalidation/b/r;->a(J)Lcom/google/ipc/invalidation/b/r;

    .line 1822
    const-string/jumbo v0, " client_config="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/j;->d:Lcom/google/ipc/invalidation/ticl/a/L;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1823
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 1824
    return-void
.end method

.method protected final b()I
    .locals 6

    .prologue
    .line 1809
    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/j;->a:I

    add-int/lit8 v0, v0, 0x1f

    .line 1811
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/j;->b:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1812
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/j;->c:J

    const/16 v1, 0x20

    ushr-long v4, v2, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 1813
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/j;->d:Lcom/google/ipc/invalidation/ticl/a/L;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/L;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1814
    return v0
.end method

.method public final c()Lcom/google/ipc/invalidation/b/c;
    .locals 1

    .prologue
    .line 1792
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/j;->b:Lcom/google/ipc/invalidation/b/c;

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 1794
    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/j;->c:J

    return-wide v0
.end method

.method public final e()Lcom/google/ipc/invalidation/ticl/a/L;
    .locals 1

    .prologue
    .line 1796
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/j;->d:Lcom/google/ipc/invalidation/ticl/a/L;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1799
    if-ne p0, p1, :cond_1

    .line 1802
    :cond_0
    :goto_0
    return v0

    .line 1800
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/ticl/a/j;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 1801
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/j;

    .line 1802
    iget v2, p0, Lcom/google/ipc/invalidation/ticl/a/j;->a:I

    iget v3, p1, Lcom/google/ipc/invalidation/ticl/a/j;->a:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/j;->b:Lcom/google/ipc/invalidation/b/c;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/j;->b:Lcom/google/ipc/invalidation/b/c;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/j;->c:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/j;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/j;->d:Lcom/google/ipc/invalidation/ticl/a/L;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/j;->d:Lcom/google/ipc/invalidation/ticl/a/L;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method final f()Lcom/google/b/a/a/j;
    .locals 4

    .prologue
    .line 1849
    new-instance v0, Lcom/google/b/a/a/j;

    invoke-direct {v0}, Lcom/google/b/a/a/j;-><init>()V

    .line 1850
    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/j;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/b/a/a/j;->a:Ljava/lang/Integer;

    .line 1851
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/j;->b:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->b()[B

    move-result-object v1

    iput-object v1, v0, Lcom/google/b/a/a/j;->b:[B

    .line 1852
    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/j;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/b/a/a/j;->c:Ljava/lang/Long;

    .line 1853
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/j;->d:Lcom/google/ipc/invalidation/ticl/a/L;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/L;->n()Lcom/google/b/a/a/I;

    move-result-object v1

    iput-object v1, v0, Lcom/google/b/a/a/j;->d:Lcom/google/b/a/a/I;

    .line 1854
    return-object v0
.end method
