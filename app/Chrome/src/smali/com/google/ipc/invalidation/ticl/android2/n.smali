.class final Lcom/google/ipc/invalidation/ticl/android2/n;
.super Ljava/lang/Object;
.source "TiclStateManager.java"


# static fields
.field private static final a:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/n;->a:Ljava/util/Random;

    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;)Lcom/google/ipc/invalidation/ticl/a/i;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 142
    .line 144
    :try_start_0
    const-string/jumbo v0, "android_ticl_service_state.bin"

    invoke-virtual {p0, v0}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_b
    .catch Lcom/google/ipc/invalidation/b/p; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 145
    :try_start_1
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 146
    invoke-virtual {v2}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v5

    invoke-virtual {v5}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    .line 147
    const-wide/32 v8, 0x19000

    cmp-long v5, v6, v8

    if-lez v5, :cond_2

    .line 148
    const-string/jumbo v0, "Ignoring too-large Ticl state file with size %s > %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x1

    const v7, 0x19000

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-interface {p1, v0, v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/ipc/invalidation/b/p; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 172
    :goto_0
    if-eqz v2, :cond_0

    .line 173
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    :cond_0
    :goto_1
    move-object v0, v1

    .line 179
    :cond_1
    :goto_2
    return-object v0

    .line 152
    :cond_2
    long-to-int v5, v6

    :try_start_3
    new-array v5, v5, [B

    .line 153
    invoke-interface {v0, v5}, Ljava/io/DataInput;->readFully([B)V

    .line 154
    invoke-static {v5}, Lcom/google/ipc/invalidation/ticl/a/k;->a([B)Lcom/google/ipc/invalidation/ticl/a/k;

    move-result-object v5

    .line 157
    new-instance v0, Lcom/google/ipc/invalidation/a/a;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/a/a;-><init>()V

    invoke-virtual {v5}, Lcom/google/ipc/invalidation/ticl/a/k;->a()Lcom/google/ipc/invalidation/ticl/a/i;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/ipc/invalidation/ticl/a/i;->d()[B

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/ipc/invalidation/a/a;->a([B)V

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/a/a;->b()[B

    move-result-object v0

    new-instance v6, Lcom/google/ipc/invalidation/b/c;

    invoke-direct {v6, v0}, Lcom/google/ipc/invalidation/b/c;-><init>([B)V

    invoke-virtual {v5}, Lcom/google/ipc/invalidation/ticl/a/k;->c()Lcom/google/ipc/invalidation/b/c;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/support/v4/app/b;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string/jumbo v6, "Android TICL state digest mismatch; computed %s for %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    const/4 v0, 0x1

    aput-object v5, v7, v0

    invoke-interface {p1, v6, v7}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 158
    invoke-virtual {v5}, Lcom/google/ipc/invalidation/ticl/a/k;->a()Lcom/google/ipc/invalidation/ticl/a/i;

    move-result-object v0

    const-string/jumbo v5, "validator ensures that state is set"

    invoke-static {v0, v5}, Landroid/support/v4/app/b;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/i;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/ipc/invalidation/b/p; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 172
    if-eqz v2, :cond_1

    .line 173
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    .line 175
    :catch_0
    move-exception v1

    .line 176
    const-string/jumbo v2, "Exception closing Ticl state file: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    invoke-interface {p1, v2, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :cond_3
    move v0, v4

    .line 157
    goto :goto_3

    .line 161
    :cond_4
    :try_start_5
    const-string/jumbo v0, "Android Ticl state failed digest check: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    invoke-interface {p1, v0, v6}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lcom/google/ipc/invalidation/b/p; {:try_start_5 .. :try_end_5} :catch_a
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_9
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    .line 164
    :catch_1
    move-exception v0

    move-object v0, v2

    .line 165
    :goto_4
    :try_start_6
    const-string/jumbo v2, "Ticl state file does not exist: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "android_ticl_service_state.bin"

    aput-object v7, v5, v6

    invoke-interface {p1, v2, v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 172
    if-eqz v0, :cond_0

    .line 173
    :try_start_7
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_1

    .line 175
    :catch_2
    move-exception v0

    .line 176
    const-string/jumbo v2, "Exception closing Ticl state file: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v3

    invoke-interface {p1, v2, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 175
    :catch_3
    move-exception v0

    .line 176
    const-string/jumbo v2, "Exception closing Ticl state file: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v3

    invoke-interface {p1, v2, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 166
    :catch_4
    move-exception v0

    move-object v2, v1

    .line 167
    :goto_5
    :try_start_8
    const-string/jumbo v5, "Could not read Ticl state: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-interface {p1, v5, v6}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 172
    if-eqz v2, :cond_0

    .line 173
    :try_start_9
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    goto/16 :goto_1

    .line 175
    :catch_5
    move-exception v0

    .line 176
    const-string/jumbo v2, "Exception closing Ticl state file: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v3

    invoke-interface {p1, v2, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 168
    :catch_6
    move-exception v0

    move-object v2, v1

    .line 169
    :goto_6
    :try_start_a
    const-string/jumbo v5, "Could not read Ticl state: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-interface {p1, v5, v6}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 172
    if-eqz v2, :cond_0

    .line 173
    :try_start_b
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    goto/16 :goto_1

    .line 175
    :catch_7
    move-exception v0

    .line 176
    const-string/jumbo v2, "Exception closing Ticl state file: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v3

    invoke-interface {p1, v2, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 171
    :catchall_0
    move-exception v0

    move-object v2, v1

    .line 172
    :goto_7
    if-eqz v2, :cond_5

    .line 173
    :try_start_c
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    .line 177
    :cond_5
    :goto_8
    throw v0

    .line 175
    :catch_8
    move-exception v1

    .line 176
    const-string/jumbo v2, "Exception closing Ticl state file: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    invoke-interface {p1, v2, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_8

    .line 171
    :catchall_1
    move-exception v0

    goto :goto_7

    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_7

    .line 168
    :catch_9
    move-exception v0

    goto :goto_6

    .line 166
    :catch_a
    move-exception v0

    goto :goto_5

    .line 164
    :catch_b
    move-exception v0

    move-object v0, v1

    goto/16 :goto_4
.end method

.method static a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;)Lcom/google/ipc/invalidation/ticl/android2/b;
    .locals 3

    .prologue
    .line 68
    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;)Lcom/google/ipc/invalidation/ticl/a/i;

    move-result-object v1

    .line 69
    if-nez v1, :cond_0

    .line 70
    const/4 v0, 0x0

    .line 75
    :goto_0
    return-object v0

    .line 72
    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/b;

    sget-object v2, Lcom/google/ipc/invalidation/ticl/android2/n;->a:Ljava/util/Random;

    invoke-direct {v0, p0, p1, v2, v1}, Lcom/google/ipc/invalidation/ticl/android2/b;-><init>(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;Ljava/util/Random;Lcom/google/ipc/invalidation/ticl/a/i;)V

    .line 74
    invoke-static {p1, v0}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/ticl/android2/b;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 227
    const-string/jumbo v0, "android_ticl_service_state.bin"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 228
    return-void
.end method

.method static a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/ticl/android2/b;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 112
    const/4 v1, 0x0

    .line 116
    :try_start_0
    new-instance v0, Lcom/google/ipc/invalidation/a/a;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/a/a;-><init>()V

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/ticl/android2/b;->c()Lcom/google/ipc/invalidation/ticl/a/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/K;->a()I

    move-result v3

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/K;->c()Lcom/google/ipc/invalidation/b/c;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/ticl/android2/b;->j()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/ticl/android2/b;->b()Lcom/google/ipc/invalidation/ticl/a/L;

    move-result-object v6

    invoke-static {v3, v2, v4, v5, v6}, Lcom/google/ipc/invalidation/ticl/a/j;->a(ILcom/google/ipc/invalidation/b/c;JLcom/google/ipc/invalidation/ticl/a/L;)Lcom/google/ipc/invalidation/ticl/a/j;

    move-result-object v2

    sget-object v3, Lcom/google/ipc/invalidation/ticl/android2/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/ticl/android2/b;->i()Lcom/google/ipc/invalidation/ticl/a/ar;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/google/ipc/invalidation/ticl/a/i;->a(Lcom/google/ipc/invalidation/ticl/a/ap;Lcom/google/ipc/invalidation/ticl/a/ar;Lcom/google/ipc/invalidation/ticl/a/j;)Lcom/google/ipc/invalidation/ticl/a/i;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/i;->d()[B

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/ipc/invalidation/a/a;->a([B)V

    new-instance v3, Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/a/a;->b()[B

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/ipc/invalidation/b/c;-><init>([B)V

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/k;->a(Lcom/google/ipc/invalidation/ticl/a/i;Lcom/google/ipc/invalidation/b/c;)Lcom/google/ipc/invalidation/ticl/a/k;

    move-result-object v0

    .line 119
    const-string/jumbo v2, "android_ticl_service_state.bin"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 120
    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/k;->d()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 121
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    if-eqz v1, :cond_0

    .line 129
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 131
    :catch_0
    move-exception v0

    .line 132
    const-string/jumbo v1, "Exception closing Ticl state file: %s"

    new-array v2, v8, [Ljava/lang/Object;

    aput-object v0, v2, v7

    invoke-interface {p1, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 122
    :catch_1
    move-exception v0

    .line 123
    :try_start_2
    const-string/jumbo v2, "Could not write Ticl state: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-interface {p1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 128
    if-eqz v1, :cond_0

    .line 129
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 131
    :catch_2
    move-exception v0

    .line 132
    const-string/jumbo v1, "Exception closing Ticl state file: %s"

    new-array v2, v8, [Ljava/lang/Object;

    aput-object v0, v2, v7

    invoke-interface {p1, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 124
    :catch_3
    move-exception v0

    .line 125
    :try_start_4
    const-string/jumbo v2, "Could not write Ticl state: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-interface {p1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 128
    if-eqz v1, :cond_0

    .line 129
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_0

    .line 131
    :catch_4
    move-exception v0

    .line 132
    const-string/jumbo v1, "Exception closing Ticl state file: %s"

    new-array v2, v8, [Ljava/lang/Object;

    aput-object v0, v2, v7

    invoke-interface {p1, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 127
    :catchall_0
    move-exception v0

    .line 128
    if-eqz v1, :cond_1

    .line 129
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 133
    :cond_1
    :goto_1
    throw v0

    .line 131
    :catch_5
    move-exception v1

    .line 132
    const-string/jumbo v2, "Exception closing Ticl state file: %s"

    new-array v3, v8, [Ljava/lang/Object;

    aput-object v1, v3, v7

    invoke-interface {p1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method static a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;I[BLcom/google/ipc/invalidation/ticl/a/L;Z)V
    .locals 7

    .prologue
    .line 81
    const-string/jumbo v0, "android_ticl_service_state.bin"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string/jumbo v1, "Ticl already exists"

    invoke-static {v0, v1}, Landroid/support/v4/app/b;->b(ZLjava/lang/Object;)V

    .line 82
    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/b;

    sget-object v3, Lcom/google/ipc/invalidation/ticl/android2/n;->a:Ljava/util/Random;

    move-object v1, p0

    move-object v2, p1

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/ipc/invalidation/ticl/android2/b;-><init>(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;Ljava/util/Random;I[BLcom/google/ipc/invalidation/ticl/a/L;)V

    .line 84
    if-nez p5, :cond_0

    .line 88
    invoke-static {p1, v0}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/ticl/android2/b;)V

    .line 89
    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/b;->start()V

    .line 91
    :cond_0
    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/ticl/android2/b;)V

    .line 92
    return-void

    .line 81
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/ticl/android2/b;)V
    .locals 4

    .prologue
    .line 99
    invoke-interface {p0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;

    .line 101
    invoke-virtual {p1}, Lcom/google/ipc/invalidation/ticl/android2/b;->j()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->a(J)V

    .line 102
    return-void
.end method
