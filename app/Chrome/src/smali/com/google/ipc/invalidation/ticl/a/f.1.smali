.class public final Lcom/google/ipc/invalidation/ticl/a/f;
.super Lcom/google/ipc/invalidation/b/n;
.source "AndroidListenerProtocol.java"


# instance fields
.field private final a:J

.field private final b:I

.field private final c:Lcom/google/ipc/invalidation/b/c;

.field private final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 393
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/f;

    invoke-direct {v0, v1, v1, v1}, Lcom/google/ipc/invalidation/ticl/a/f;-><init>(Ljava/lang/Integer;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Boolean;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/Integer;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 402
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 404
    if-eqz p1, :cond_0

    .line 405
    const/4 v0, 0x1

    .line 406
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/google/ipc/invalidation/ticl/a/f;->b:I

    .line 410
    :goto_0
    if-eqz p2, :cond_1

    .line 411
    or-int/lit8 v0, v0, 0x2

    .line 412
    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/a/f;->c:Lcom/google/ipc/invalidation/b/c;

    .line 416
    :goto_1
    if-eqz p3, :cond_2

    .line 417
    or-int/lit8 v0, v0, 0x4

    .line 418
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/f;->d:Z

    .line 422
    :goto_2
    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/f;->a:J

    .line 423
    return-void

    .line 408
    :cond_0
    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/f;->b:I

    move v0, v1

    goto :goto_0

    .line 414
    :cond_1
    sget-object v2, Lcom/google/ipc/invalidation/b/c;->a:Lcom/google/ipc/invalidation/b/c;

    iput-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/f;->c:Lcom/google/ipc/invalidation/b/c;

    goto :goto_1

    .line 420
    :cond_2
    iput-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/f;->d:Z

    goto :goto_2
.end method

.method public static a(Ljava/lang/Integer;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Boolean;)Lcom/google/ipc/invalidation/ticl/a/f;
    .locals 1

    .prologue
    .line 390
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/f;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/ipc/invalidation/ticl/a/f;-><init>(Ljava/lang/Integer;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Boolean;)V

    return-object v0
.end method

.method public static a([B)Lcom/google/ipc/invalidation/ticl/a/f;
    .locals 4

    .prologue
    .line 474
    :try_start_0
    new-instance v0, Lcom/google/b/a/a/f;

    invoke-direct {v0}, Lcom/google/b/a/a/f;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/a/f;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/ipc/invalidation/ticl/a/f;

    iget-object v2, v0, Lcom/google/b/a/a/f;->a:Ljava/lang/Integer;

    iget-object v3, v0, Lcom/google/b/a/a/f;->b:[B

    invoke-static {v3}, Lcom/google/ipc/invalidation/b/c;->a([B)Lcom/google/ipc/invalidation/b/c;

    move-result-object v3

    iget-object v0, v0, Lcom/google/b/a/a/f;->c:Ljava/lang/Boolean;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/ipc/invalidation/ticl/a/f;-><init>(Ljava/lang/Integer;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Boolean;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/ipc/invalidation/b/o; {:try_start_0 .. :try_end_0} :catch_1

    move-object v0, v1

    goto :goto_0

    .line 475
    :catch_0
    move-exception v0

    .line 476
    new-instance v1, Lcom/google/ipc/invalidation/b/p;

    invoke-direct {v1, v0}, Lcom/google/ipc/invalidation/b/p;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 477
    :catch_1
    move-exception v0

    .line 478
    new-instance v1, Lcom/google/ipc/invalidation/b/p;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/o;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/ipc/invalidation/b/p;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private h()Z
    .locals 4

    .prologue
    .line 432
    const-wide/16 v0, 0x4

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/f;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 425
    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/f;->b:I

    return v0
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 2

    .prologue
    .line 459
    const-string/jumbo v0, "<StartCommand:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 460
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/f;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461
    const-string/jumbo v0, " client_type="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/f;->b:I

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(I)Lcom/google/ipc/invalidation/b/r;

    .line 463
    :cond_0
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 464
    const-string/jumbo v0, " client_name="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/f;->c:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 466
    :cond_1
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/f;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 467
    const-string/jumbo v0, " allow_suppression="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/f;->d:Z

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Z)Lcom/google/ipc/invalidation/b/r;

    .line 469
    :cond_2
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 470
    return-void
.end method

.method protected final b()I
    .locals 4

    .prologue
    .line 445
    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/f;->a:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 446
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/f;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 447
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/f;->b:I

    add-int/2addr v0, v1

    .line 449
    :cond_0
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/f;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 450
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/f;->c:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 452
    :cond_1
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/f;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 453
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/f;->d:Z

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/f;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 455
    :cond_2
    return v0
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 426
    const-wide/16 v0, 0x1

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/f;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/google/ipc/invalidation/b/c;
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/f;->c:Lcom/google/ipc/invalidation/b/c;

    return-object v0
.end method

.method public final e()Z
    .locals 4

    .prologue
    .line 429
    const-wide/16 v0, 0x2

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/f;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 435
    if-ne p0, p1, :cond_1

    .line 438
    :cond_0
    :goto_0
    return v0

    .line 436
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/ticl/a/f;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 437
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/f;

    .line 438
    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/f;->a:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/f;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/f;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/google/ipc/invalidation/ticl/a/f;->b:I

    iget v3, p1, Lcom/google/ipc/invalidation/ticl/a/f;->b:I

    if-ne v2, v3, :cond_5

    :cond_3
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/f;->e()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/f;->c:Lcom/google/ipc/invalidation/b/c;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/f;->c:Lcom/google/ipc/invalidation/b/c;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/f;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/ipc/invalidation/ticl/a/f;->d:Z

    iget-boolean v3, p1, Lcom/google/ipc/invalidation/ticl/a/f;->d:Z

    if-eq v2, v3, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 431
    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/f;->d:Z

    return v0
.end method

.method public final g()[B
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 490
    new-instance v2, Lcom/google/b/a/a/f;

    invoke-direct {v2}, Lcom/google/b/a/a/f;-><init>()V

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/f;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/f;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    iput-object v0, v2, Lcom/google/b/a/a/f;->a:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/f;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/f;->c:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/c;->b()[B

    move-result-object v0

    :goto_1
    iput-object v0, v2, Lcom/google/b/a/a/f;->b:[B

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/f;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/f;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :cond_0
    iput-object v1, v2, Lcom/google/b/a/a/f;->c:Ljava/lang/Boolean;

    invoke-static {v2}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method
