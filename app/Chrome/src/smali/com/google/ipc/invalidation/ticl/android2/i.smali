.class public final Lcom/google/ipc/invalidation/ticl/android2/i;
.super Ljava/lang/Object;
.source "ProtocolIntents.java"


# static fields
.field static final a:Lcom/google/ipc/invalidation/ticl/a/ap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/ap;->a(II)Lcom/google/ipc/invalidation/ticl/a/ap;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    return-void
.end method

.method public static a(Lcom/google/ipc/invalidation/ticl/a/V;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/V;->c()[B

    move-result-object v0

    .line 256
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v2, "ipcinv-background-inv"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;J)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 241
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    invoke-static {v0, p0, p1, p2}, Lcom/google/ipc/invalidation/ticl/a/h;->a(Lcom/google/ipc/invalidation/ticl/a/ap;Ljava/lang/String;J)Lcom/google/ipc/invalidation/ticl/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/h;->d()[B

    move-result-object v0

    .line 243
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v2, "ipcinv-scheduler"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a([B)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 248
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    new-instance v1, Lcom/google/ipc/invalidation/b/c;

    invoke-direct {v1, p0}, Lcom/google/ipc/invalidation/b/c;-><init>([B)V

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/g;->a(Lcom/google/ipc/invalidation/ticl/a/ap;Lcom/google/ipc/invalidation/b/c;)Lcom/google/ipc/invalidation/ticl/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/g;->c()[B

    move-result-object v0

    .line 250
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v2, "ipcinv-outbound-message"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
