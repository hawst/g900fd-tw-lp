.class public final Lcom/google/ipc/invalidation/ticl/a/W;
.super Lcom/google/ipc/invalidation/b/n;
.source "ClientProtocol.java"


# instance fields
.field private final a:J

.field private final b:Lcom/google/ipc/invalidation/ticl/a/Y;

.field private final c:Z

.field private final d:J

.field private final e:Lcom/google/ipc/invalidation/b/c;

.field private final f:J

.field private final g:Z


# direct methods
.method private constructor <init>(Lcom/google/ipc/invalidation/ticl/a/Y;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Long;Ljava/lang/Boolean;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 547
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 549
    const-string/jumbo v0, "object_id"

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/a/W;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 550
    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/W;->b:Lcom/google/ipc/invalidation/ticl/a/Y;

    .line 551
    const-string/jumbo v0, "is_known_version"

    invoke-static {v0, p2}, Lcom/google/ipc/invalidation/ticl/a/W;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 552
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/W;->c:Z

    .line 553
    const-string/jumbo v0, "version"

    invoke-static {v0, p3}, Lcom/google/ipc/invalidation/ticl/a/W;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 554
    const-string/jumbo v0, "version"

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v0, v4, v5}, Lcom/google/ipc/invalidation/ticl/a/W;->a(Ljava/lang/String;J)V

    .line 555
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/ipc/invalidation/ticl/a/W;->d:J

    .line 556
    if-eqz p4, :cond_1

    .line 558
    iput-object p4, p0, Lcom/google/ipc/invalidation/ticl/a/W;->e:Lcom/google/ipc/invalidation/b/c;

    move v0, v2

    .line 562
    :goto_0
    if-eqz p5, :cond_2

    .line 563
    or-int/lit8 v0, v0, 0x2

    .line 564
    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/ipc/invalidation/ticl/a/W;->f:J

    .line 568
    :goto_1
    if-eqz p6, :cond_3

    .line 569
    or-int/lit8 v0, v0, 0x4

    .line 570
    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/ipc/invalidation/ticl/a/W;->g:Z

    .line 574
    :goto_2
    int-to-long v4, v0

    iput-wide v4, p0, Lcom/google/ipc/invalidation/ticl/a/W;->a:J

    .line 575
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p6, :cond_0

    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v2

    :goto_3
    const-string/jumbo v1, "is_trickle_restart required if not is_known_version"

    invoke-virtual {p0, v0, v1}, Lcom/google/ipc/invalidation/ticl/a/W;->a(ZLjava/lang/String;)V

    .line 577
    return-void

    .line 560
    :cond_1
    sget-object v0, Lcom/google/ipc/invalidation/b/c;->a:Lcom/google/ipc/invalidation/b/c;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/W;->e:Lcom/google/ipc/invalidation/b/c;

    move v0, v1

    goto :goto_0

    .line 566
    :cond_2
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/google/ipc/invalidation/ticl/a/W;->f:J

    goto :goto_1

    .line 572
    :cond_3
    iput-boolean v2, p0, Lcom/google/ipc/invalidation/ticl/a/W;->g:Z

    goto :goto_2

    :cond_4
    move v0, v1

    .line 575
    goto :goto_3
.end method

.method synthetic constructor <init>(Lcom/google/ipc/invalidation/ticl/a/Y;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Long;Ljava/lang/Boolean;B)V
    .locals 0

    .prologue
    .line 507
    invoke-direct/range {p0 .. p6}, Lcom/google/ipc/invalidation/ticl/a/W;-><init>(Lcom/google/ipc/invalidation/ticl/a/Y;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Long;Ljava/lang/Boolean;)V

    return-void
.end method

.method static a(Lcom/google/b/a/a/S;)Lcom/google/ipc/invalidation/ticl/a/W;
    .locals 7

    .prologue
    .line 666
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 667
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/W;

    iget-object v1, p0, Lcom/google/b/a/a/S;->a:Lcom/google/b/a/a/T;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/Y;->a(Lcom/google/b/a/a/T;)Lcom/google/ipc/invalidation/ticl/a/Y;

    move-result-object v1

    iget-object v2, p0, Lcom/google/b/a/a/S;->b:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/google/b/a/a/S;->c:Ljava/lang/Long;

    iget-object v4, p0, Lcom/google/b/a/a/S;->e:[B

    invoke-static {v4}, Lcom/google/ipc/invalidation/b/c;->a([B)Lcom/google/ipc/invalidation/b/c;

    move-result-object v4

    iget-object v5, p0, Lcom/google/b/a/a/S;->f:Ljava/lang/Long;

    iget-object v6, p0, Lcom/google/b/a/a/S;->d:Ljava/lang/Boolean;

    invoke-direct/range {v0 .. v6}, Lcom/google/ipc/invalidation/ticl/a/W;-><init>(Lcom/google/ipc/invalidation/ticl/a/Y;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Long;Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/ipc/invalidation/ticl/a/Y;ZJLcom/google/ipc/invalidation/b/c;Ljava/lang/Long;Ljava/lang/Boolean;)Lcom/google/ipc/invalidation/ticl/a/W;
    .locals 8

    .prologue
    .line 531
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/W;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v5, 0x0

    move-object v1, p0

    move-object v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/ipc/invalidation/ticl/a/W;-><init>(Lcom/google/ipc/invalidation/ticl/a/Y;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Long;Ljava/lang/Boolean;)V

    return-object v0
.end method

.method private j()Z
    .locals 4

    .prologue
    .line 589
    const-wide/16 v0, 0x2

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/W;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()Z
    .locals 4

    .prologue
    .line 592
    const-wide/16 v0, 0x4

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/W;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/ipc/invalidation/ticl/a/Y;
    .locals 1

    .prologue
    .line 579
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/W;->b:Lcom/google/ipc/invalidation/ticl/a/Y;

    return-object v0
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 4

    .prologue
    .line 639
    const-string/jumbo v0, "<InvalidationP:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 640
    const-string/jumbo v0, " object_id="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/W;->b:Lcom/google/ipc/invalidation/ticl/a/Y;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 641
    const-string/jumbo v0, " is_known_version="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/W;->c:Z

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Z)Lcom/google/ipc/invalidation/b/r;

    .line 642
    const-string/jumbo v0, " version="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/W;->d:J

    invoke-virtual {v0, v2, v3}, Lcom/google/ipc/invalidation/b/r;->a(J)Lcom/google/ipc/invalidation/b/r;

    .line 643
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/W;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 644
    const-string/jumbo v0, " payload="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/W;->e:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 646
    :cond_0
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/W;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 647
    const-string/jumbo v0, " bridge_arrival_time_ms_deprecated="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/W;->f:J

    invoke-virtual {v0, v2, v3}, Lcom/google/ipc/invalidation/b/r;->a(J)Lcom/google/ipc/invalidation/b/r;

    .line 649
    :cond_1
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/W;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 650
    const-string/jumbo v0, " is_trickle_restart="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/W;->g:Z

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Z)Lcom/google/ipc/invalidation/b/r;

    .line 652
    :cond_2
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 653
    return-void
.end method

.method protected final b()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 622
    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/W;->a:J

    ushr-long v2, v0, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 623
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/W;->b:Lcom/google/ipc/invalidation/ticl/a/Y;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/Y;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 624
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/W;->c:Z

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/W;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 625
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/W;->d:J

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 626
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/W;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 627
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/W;->e:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 629
    :cond_0
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/W;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 630
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/W;->f:J

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 632
    :cond_1
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/W;->k()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 633
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/W;->g:Z

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/W;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 635
    :cond_2
    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 581
    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/W;->c:Z

    return v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 583
    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/W;->d:J

    return-wide v0
.end method

.method public final e()Lcom/google/ipc/invalidation/b/c;
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/W;->e:Lcom/google/ipc/invalidation/b/c;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 609
    if-ne p0, p1, :cond_1

    .line 612
    :cond_0
    :goto_0
    return v0

    .line 610
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/ticl/a/W;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 611
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/W;

    .line 612
    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/W;->a:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/W;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/W;->b:Lcom/google/ipc/invalidation/ticl/a/Y;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/W;->b:Lcom/google/ipc/invalidation/ticl/a/Y;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/W;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Lcom/google/ipc/invalidation/ticl/a/W;->c:Z

    iget-boolean v3, p1, Lcom/google/ipc/invalidation/ticl/a/W;->c:Z

    if-ne v2, v3, :cond_5

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/W;->d:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/W;->d:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/W;->f()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/W;->e:Lcom/google/ipc/invalidation/b/c;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/W;->e:Lcom/google/ipc/invalidation/b/c;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/W;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_3
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/W;->j()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/W;->f:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/W;->f:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    :cond_4
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/W;->k()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/ipc/invalidation/ticl/a/W;->g:Z

    iget-boolean v3, p1, Lcom/google/ipc/invalidation/ticl/a/W;->g:Z

    if-eq v2, v3, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final f()Z
    .locals 4

    .prologue
    .line 586
    const-wide/16 v0, 0x1

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/W;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 591
    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/W;->g:Z

    return v0
.end method

.method public final h()Lcom/google/ipc/invalidation/ticl/a/X;
    .locals 6

    .prologue
    .line 595
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/X;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/W;->b:Lcom/google/ipc/invalidation/ticl/a/Y;

    iget-boolean v2, p0, Lcom/google/ipc/invalidation/ticl/a/W;->c:Z

    iget-wide v4, p0, Lcom/google/ipc/invalidation/ticl/a/W;->d:J

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/google/ipc/invalidation/ticl/a/X;-><init>(Lcom/google/ipc/invalidation/ticl/a/Y;ZJ)V

    .line 596
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/W;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 597
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/W;->e:Lcom/google/ipc/invalidation/b/c;

    iput-object v1, v0, Lcom/google/ipc/invalidation/ticl/a/X;->a:Lcom/google/ipc/invalidation/b/c;

    .line 599
    :cond_0
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/W;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 600
    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/W;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ipc/invalidation/ticl/a/X;->b:Ljava/lang/Long;

    .line 602
    :cond_1
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/W;->k()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 603
    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/W;->g:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ipc/invalidation/ticl/a/X;->c:Ljava/lang/Boolean;

    .line 605
    :cond_2
    return-object v0
.end method

.method final i()Lcom/google/b/a/a/S;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 680
    new-instance v2, Lcom/google/b/a/a/S;

    invoke-direct {v2}, Lcom/google/b/a/a/S;-><init>()V

    .line 681
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/W;->b:Lcom/google/ipc/invalidation/ticl/a/Y;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/Y;->d()Lcom/google/b/a/a/T;

    move-result-object v0

    iput-object v0, v2, Lcom/google/b/a/a/S;->a:Lcom/google/b/a/a/T;

    .line 682
    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/W;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Lcom/google/b/a/a/S;->b:Ljava/lang/Boolean;

    .line 683
    iget-wide v4, p0, Lcom/google/ipc/invalidation/ticl/a/W;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lcom/google/b/a/a/S;->c:Ljava/lang/Long;

    .line 684
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/W;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/W;->e:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/c;->b()[B

    move-result-object v0

    :goto_0
    iput-object v0, v2, Lcom/google/b/a/a/S;->e:[B

    .line 685
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/W;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v4, p0, Lcom/google/ipc/invalidation/ticl/a/W;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_1
    iput-object v0, v2, Lcom/google/b/a/a/S;->f:Ljava/lang/Long;

    .line 686
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/W;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/W;->g:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :cond_0
    iput-object v1, v2, Lcom/google/b/a/a/S;->d:Ljava/lang/Boolean;

    .line 687
    return-object v2

    :cond_1
    move-object v0, v1

    .line 684
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 685
    goto :goto_1
.end method
