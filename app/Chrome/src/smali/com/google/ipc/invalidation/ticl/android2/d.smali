.class final Lcom/google/ipc/invalidation/ticl/android2/d;
.super Ljava/lang/Object;
.source "AndroidInvalidationClientStub.java"

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/InvalidationClient;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/app/b;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/d;->a:Landroid/content/Context;

    .line 52
    invoke-static {p2}, Landroid/support/v4/app/b;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/g;

    invoke-direct {v0, p1}, Lcom/google/ipc/invalidation/ticl/android2/g;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/g;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/d;->b:Ljava/lang/String;

    .line 54
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/d;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/d;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/d;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 107
    return-void
.end method


# virtual methods
.method public final acknowledge(Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V
    .locals 4

    .prologue
    .line 100
    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/AckHandle;->getHandleData()[B

    move-result-object v0

    new-instance v1, Lcom/google/ipc/invalidation/b/c;

    invoke-direct {v1, v0}, Lcom/google/ipc/invalidation/b/c;-><init>([B)V

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/m;->a(Lcom/google/ipc/invalidation/b/c;)Lcom/google/ipc/invalidation/ticl/a/m;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v2, "ipcinv-downcall"

    sget-object v3, Lcom/google/ipc/invalidation/ticl/android2/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    invoke-static {v3, v0}, Lcom/google/ipc/invalidation/ticl/a/l;->a(Lcom/google/ipc/invalidation/ticl/a/ap;Lcom/google/ipc/invalidation/ticl/a/m;)Lcom/google/ipc/invalidation/ticl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->f()[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-direct {p0, v1}, Lcom/google/ipc/invalidation/ticl/android2/d;->a(Landroid/content/Intent;)V

    .line 101
    return-void
.end method

.method public final register(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V
    .locals 1

    .prologue
    .line 72
    invoke-static {p1}, Landroid/support/v4/app/b;->a(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/ipc/invalidation/ticl/a/Y;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 74
    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/android2/a;->a(Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/d;->a(Landroid/content/Intent;)V

    .line 75
    return-void
.end method

.method public final register(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 79
    invoke-static {p1}, Landroid/support/v4/app/b;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    .line 81
    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/android2/a;->a(Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/d;->a(Landroid/content/Intent;)V

    .line 82
    return-void
.end method

.method public final start()V
    .locals 2

    .prologue
    .line 58
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Android clients are automatically started when created"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final stop()V
    .locals 4

    .prologue
    .line 67
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "ipcinv-downcall"

    sget-object v2, Lcom/google/ipc/invalidation/ticl/android2/i;->a:Lcom/google/ipc/invalidation/ticl/a/ap;

    sget-object v3, Lcom/google/ipc/invalidation/ticl/a/p;->a:Lcom/google/ipc/invalidation/ticl/a/p;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/l;->a(Lcom/google/ipc/invalidation/ticl/a/ap;Lcom/google/ipc/invalidation/ticl/a/p;)Lcom/google/ipc/invalidation/ticl/a/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/l;->f()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/d;->a(Landroid/content/Intent;)V

    .line 68
    return-void
.end method

.method public final unregister(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V
    .locals 1

    .prologue
    .line 86
    invoke-static {p1}, Landroid/support/v4/app/b;->a(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/ipc/invalidation/ticl/a/Y;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 88
    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/android2/a;->b(Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/d;->a(Landroid/content/Intent;)V

    .line 89
    return-void
.end method

.method public final unregister(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 93
    invoke-static {p1}, Landroid/support/v4/app/b;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    .line 95
    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/android2/a;->b(Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/d;->a(Landroid/content/Intent;)V

    .line 96
    return-void
.end method
