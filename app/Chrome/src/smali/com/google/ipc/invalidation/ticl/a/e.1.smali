.class public final Lcom/google/ipc/invalidation/ticl/a/e;
.super Lcom/google/ipc/invalidation/b/n;
.source "AndroidListenerProtocol.java"


# instance fields
.field private final a:J

.field private final b:Z

.field private final c:Ljava/util/List;

.field private final d:Lcom/google/ipc/invalidation/b/c;

.field private final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 260
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/e;

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/google/ipc/invalidation/ticl/a/e;-><init>(Ljava/lang/Boolean;Ljava/util/Collection;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Boolean;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/Boolean;Ljava/util/Collection;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 271
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 273
    if-eqz p1, :cond_0

    .line 274
    const/4 v0, 0x1

    .line 275
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/ipc/invalidation/ticl/a/e;->b:Z

    .line 279
    :goto_0
    const-string/jumbo v2, "object_id"

    invoke-static {v2, p2}, Lcom/google/ipc/invalidation/ticl/a/e;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/e;->c:Ljava/util/List;

    .line 280
    if-eqz p3, :cond_1

    .line 281
    or-int/lit8 v0, v0, 0x2

    .line 282
    iput-object p3, p0, Lcom/google/ipc/invalidation/ticl/a/e;->d:Lcom/google/ipc/invalidation/b/c;

    .line 286
    :goto_1
    if-eqz p4, :cond_2

    .line 287
    or-int/lit8 v0, v0, 0x4

    .line 288
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/e;->e:Z

    .line 292
    :goto_2
    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/e;->a:J

    .line 293
    return-void

    .line 277
    :cond_0
    iput-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/e;->b:Z

    move v0, v1

    goto :goto_0

    .line 284
    :cond_1
    sget-object v2, Lcom/google/ipc/invalidation/b/c;->a:Lcom/google/ipc/invalidation/b/c;

    iput-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/e;->d:Lcom/google/ipc/invalidation/b/c;

    goto :goto_1

    .line 290
    :cond_2
    iput-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/e;->e:Z

    goto :goto_2
.end method

.method public static a(Ljava/lang/Boolean;Ljava/util/Collection;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Boolean;)Lcom/google/ipc/invalidation/ticl/a/e;
    .locals 1

    .prologue
    .line 257
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/e;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/ipc/invalidation/ticl/a/e;-><init>(Ljava/lang/Boolean;Ljava/util/Collection;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Boolean;)V

    return-object v0
.end method

.method public static a([B)Lcom/google/ipc/invalidation/ticl/a/e;
    .locals 5

    .prologue
    .line 349
    :try_start_0
    new-instance v0, Lcom/google/b/a/a/e;

    invoke-direct {v0}, Lcom/google/b/a/a/e;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/a/e;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v1, v0, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    array-length v1, v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    :goto_1
    iget-object v3, v0, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    iget-object v3, v0, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    aget-object v3, v3, v1

    invoke-static {v3}, Lcom/google/ipc/invalidation/ticl/a/Y;->a(Lcom/google/b/a/a/T;)Lcom/google/ipc/invalidation/ticl/a/Y;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    new-instance v1, Lcom/google/ipc/invalidation/ticl/a/e;

    iget-object v3, v0, Lcom/google/b/a/a/e;->a:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/google/b/a/a/e;->c:[B

    invoke-static {v4}, Lcom/google/ipc/invalidation/b/c;->a([B)Lcom/google/ipc/invalidation/b/c;

    move-result-object v4

    iget-object v0, v0, Lcom/google/b/a/a/e;->d:Ljava/lang/Boolean;

    invoke-direct {v1, v3, v2, v4, v0}, Lcom/google/ipc/invalidation/ticl/a/e;-><init>(Ljava/lang/Boolean;Ljava/util/Collection;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Boolean;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/ipc/invalidation/b/o; {:try_start_0 .. :try_end_0} :catch_1

    move-object v0, v1

    goto :goto_0

    .line 350
    :catch_0
    move-exception v0

    .line 351
    new-instance v1, Lcom/google/ipc/invalidation/b/p;

    invoke-direct {v1, v0}, Lcom/google/ipc/invalidation/b/p;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 352
    :catch_1
    move-exception v0

    .line 353
    new-instance v1, Lcom/google/ipc/invalidation/b/p;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/o;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/ipc/invalidation/b/p;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 2

    .prologue
    .line 333
    const-string/jumbo v0, "<RegistrationCommand:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 334
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/e;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    const-string/jumbo v0, " is_register="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/e;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Z)Lcom/google/ipc/invalidation/b/r;

    .line 337
    :cond_0
    const-string/jumbo v0, " object_id=["

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/e;->c:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/Iterable;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 338
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/e;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 339
    const-string/jumbo v0, " client_id="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/e;->d:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 341
    :cond_1
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/e;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 342
    const-string/jumbo v0, " is_delayed="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/e;->e:Z

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Z)Lcom/google/ipc/invalidation/b/r;

    .line 344
    :cond_2
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 345
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 295
    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/e;->b:Z

    return v0
.end method

.method protected final b()I
    .locals 4

    .prologue
    .line 318
    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/e;->a:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 319
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/e;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 320
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/e;->b:Z

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/e;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 322
    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/e;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 323
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/e;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 324
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/e;->d:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 326
    :cond_1
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/e;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 327
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/e;->e:Z

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/e;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 329
    :cond_2
    return v0
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 296
    const-wide/16 v0, 0x1

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/e;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/util/List;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/e;->c:Ljava/util/List;

    return-object v0
.end method

.method public final e()Lcom/google/ipc/invalidation/b/c;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/e;->d:Lcom/google/ipc/invalidation/b/c;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 307
    if-ne p0, p1, :cond_1

    .line 310
    :cond_0
    :goto_0
    return v0

    .line 308
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/ticl/a/e;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 309
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/e;

    .line 310
    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/e;->a:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/e;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/e;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/ipc/invalidation/ticl/a/e;->b:Z

    iget-boolean v3, p1, Lcom/google/ipc/invalidation/ticl/a/e;->b:Z

    if-ne v2, v3, :cond_5

    :cond_3
    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/e;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/e;->c:Ljava/util/List;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/e;->f()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/e;->d:Lcom/google/ipc/invalidation/b/c;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/e;->d:Lcom/google/ipc/invalidation/b/c;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/e;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/ipc/invalidation/ticl/a/e;->e:Z

    iget-boolean v3, p1, Lcom/google/ipc/invalidation/ticl/a/e;->e:Z

    if-eq v2, v3, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final f()Z
    .locals 4

    .prologue
    .line 301
    const-wide/16 v0, 0x2

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/e;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 303
    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/e;->e:Z

    return v0
.end method

.method public final h()Z
    .locals 4

    .prologue
    .line 304
    const-wide/16 v0, 0x4

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/e;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()[B
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 370
    new-instance v3, Lcom/google/b/a/a/e;

    invoke-direct {v3}, Lcom/google/b/a/a/e;-><init>()V

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/e;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/e;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    iput-object v0, v3, Lcom/google/b/a/a/e;->a:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/e;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/b/a/a/T;

    iput-object v0, v3, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget-object v0, v3, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    array-length v0, v0

    if-ge v2, v0, :cond_1

    iget-object v4, v3, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/e;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/Y;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/Y;->d()Lcom/google/b/a/a/T;

    move-result-object v0

    aput-object v0, v4, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/e;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/e;->d:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/c;->b()[B

    move-result-object v0

    :goto_2
    iput-object v0, v3, Lcom/google/b/a/a/e;->c:[B

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/e;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/e;->e:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :cond_2
    iput-object v1, v3, Lcom/google/b/a/a/e;->d:Ljava/lang/Boolean;

    invoke-static {v3}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    return-object v0

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method
