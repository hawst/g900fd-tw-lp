.class public Lcom/google/ipc/invalidation/ticl/android2/TiclService;
.super Landroid/app/IntentService;
.source "TiclService.java"


# instance fields
.field private a:Lcom/google/ipc/invalidation/ticl/android2/j;

.field private final b:Lcom/google/ipc/invalidation/a/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    const-string/jumbo v0, "TiclService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 65
    new-instance v0, Lcom/google/ipc/invalidation/a/a;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->b:Lcom/google/ipc/invalidation/a/a;

    .line 72
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->setIntentRedelivery(Z)V

    .line 73
    return-void
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/ticl/android2/TiclService;)Lcom/google/ipc/invalidation/ticl/android2/j;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    return-object v0
.end method

.method private a([B)V
    .locals 5

    .prologue
    .line 313
    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/g;

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/ticl/android2/g;-><init>(Landroid/content/Context;)V

    .line 314
    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/g;->c()Ljava/lang/String;

    move-result-object v0

    .line 316
    if-eqz v0, :cond_0

    .line 318
    :try_start_0
    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/a/am;->a([B)Lcom/google/ipc/invalidation/ticl/a/am;

    move-result-object v1

    .line 319
    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/am;->e()Lcom/google/ipc/invalidation/ticl/a/V;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 320
    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/am;->e()Lcom/google/ipc/invalidation/ticl/a/V;

    move-result-object v1

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/android2/i;->a(Lcom/google/ipc/invalidation/ticl/a/V;)Landroid/content/Intent;

    move-result-object v1

    .line 322
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 323
    invoke-virtual {p0, v1}, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Lcom/google/ipc/invalidation/b/p; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 325
    :catch_0
    move-exception v0

    .line 326
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    const-string/jumbo v2, "Failed to parse message: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/p;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/ipc/invalidation/ticl/android2/TiclService;)Lcom/google/ipc/invalidation/a/a;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->b:Lcom/google/ipc/invalidation/a/a;

    return-object v0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 89
    if-nez p1, :cond_0

    .line 114
    :goto_0
    return-void

    .line 94
    :cond_0
    new-instance v2, Lcom/google/ipc/invalidation/ticl/android2/a;

    invoke-direct {v2}, Lcom/google/ipc/invalidation/ticl/android2/a;-><init>()V

    const-string/jumbo v3, "TiclService"

    new-instance v4, Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-direct {v4, p0, v2, v3, v1}, Lcom/google/ipc/invalidation/ticl/android2/j;-><init>(Landroid/content/Context;Lcom/google/ipc/invalidation/ticl/android2/a;Ljava/lang/String;B)V

    iput-object v4, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    .line 95
    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/android2/j;->start()V

    .line 96
    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v2

    const-string/jumbo v3, "onHandleIntent(%s)"

    new-array v4, v0, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    :try_start_0
    const-string/jumbo v2, "ipcinv-downcall"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 101
    const-string/jumbo v0, "ipcinv-downcall"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->a([B)Lcom/google/ipc/invalidation/ticl/a/l;
    :try_end_1
    .catch Lcom/google/ipc/invalidation/b/p; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    const-string/jumbo v2, "Handle client downcall: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-static {p0, v1}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;)Lcom/google/ipc/invalidation/ticl/android2/b;

    move-result-object v1

    if-nez v1, :cond_1

    const-string/jumbo v2, "Client does not exist on downcall"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v4, v2, v5}, Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;->newInstance(IZLjava/lang/String;Lcom/google/ipc/invalidation/external/client/types/ErrorContext;)Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;

    move-result-object v2

    invoke-static {v2}, Lcom/google/ipc/invalidation/ticl/android2/a;->a(Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)Landroid/content/Intent;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/ipc/invalidation/ticl/android2/c;->a(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_1
    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    const-string/jumbo v2, "Dropping client downcall since no Ticl: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 112
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/j;->stop()V

    .line 113
    iput-object v7, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    goto :goto_0

    .line 101
    :catch_0
    move-exception v1

    :try_start_3
    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v2

    const-string/jumbo v3, "Failed parsing ClientDowncall from %s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Lcom/google/ipc/invalidation/b/c;->b([B)Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/p;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 112
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/j;->stop()V

    .line 113
    iput-object v7, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    throw v0

    .line 101
    :cond_3
    :try_start_4
    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->d()Lcom/google/ipc/invalidation/ticl/a/m;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->d()Lcom/google/ipc/invalidation/ticl/a/m;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/m;->a()Lcom/google/ipc/invalidation/b/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/b/c;->b()[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/ipc/invalidation/external/client/types/AckHandle;->newInstance([B)Lcom/google/ipc/invalidation/external/client/types/AckHandle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/ipc/invalidation/ticl/android2/b;->acknowledge(Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    :cond_4
    :goto_2
    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->c()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {p0}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;)V

    goto :goto_1

    :cond_5
    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/b;->start()V

    goto :goto_2

    :cond_6
    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->c()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/b;->stop()V

    goto :goto_2

    :cond_7
    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->e()Lcom/google/ipc/invalidation/ticl/a/n;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->e()Lcom/google/ipc/invalidation/ticl/a/n;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/n;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/n;->a()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v4/app/b;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/ipc/invalidation/ticl/android2/b;->register(Ljava/util/Collection;)V

    :cond_8
    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/n;->c()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/n;->c()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/app/b;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/ipc/invalidation/ticl/android2/b;->unregister(Ljava/util/Collection;)V

    goto :goto_2

    :cond_9
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Invalid downcall passed validation: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_a
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/ticl/android2/b;)V

    goto/16 :goto_1

    .line 102
    :cond_b
    const-string/jumbo v2, "ipcinv-internal-downcall"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 103
    const-string/jumbo v2, "ipcinv-internal-downcall"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v2

    :try_start_5
    invoke-static {v2}, Lcom/google/ipc/invalidation/ticl/a/q;->a([B)Lcom/google/ipc/invalidation/ticl/a/q;
    :try_end_5
    .catch Lcom/google/ipc/invalidation/b/p; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v2

    :try_start_6
    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v3}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v3

    const-string/jumbo v4, "Handle internal downcall: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-interface {v3, v4, v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/q;->a()Lcom/google/ipc/invalidation/ticl/a/t;

    move-result-object v3

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-static {p0, v3}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;)Lcom/google/ipc/invalidation/ticl/android2/b;

    move-result-object v3

    if-eqz v3, :cond_c

    :goto_3
    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/q;->a()Lcom/google/ipc/invalidation/ticl/a/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/t;->a()Lcom/google/ipc/invalidation/b/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->b()[B

    move-result-object v1

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/j;->a()Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;->onMessageReceived([B)V

    :goto_4
    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    invoke-static {p0, v0, v3}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/ticl/android2/b;)V

    goto/16 :goto_1

    :catch_1
    move-exception v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    const-string/jumbo v3, "Failed parsing InternalDowncall from %s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2}, Lcom/google/ipc/invalidation/b/c;->b([B)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x1

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/p;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-interface {v1, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_c
    move v0, v1

    goto :goto_3

    :cond_d
    invoke-direct {p0, v1}, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a([B)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string/jumbo v1, "Message for unstarted Ticl; rewrite state"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/j;->getStorage()Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    move-result-object v0

    const-string/jumbo v1, "ClientToken"

    new-instance v2, Lcom/google/ipc/invalidation/ticl/android2/l;

    invoke-direct {v2, p0}, Lcom/google/ipc/invalidation/ticl/android2/l;-><init>(Lcom/google/ipc/invalidation/ticl/android2/TiclService;)V

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;->readKey(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/types/Callback;)V

    goto :goto_4

    :cond_e
    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/q;->c()Lcom/google/ipc/invalidation/ticl/a/s;

    move-result-object v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;)Lcom/google/ipc/invalidation/ticl/android2/b;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/j;->a()Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/q;->c()Lcom/google/ipc/invalidation/ticl/a/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/s;->a()Z

    move-result v2

    invoke-interface {v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;->onOnlineStatusChange(Z)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/ticl/android2/b;)V

    goto/16 :goto_1

    :cond_f
    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/q;->d()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;)Lcom/google/ipc/invalidation/ticl/android2/b;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/j;->a()Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;->onAddressChange()V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/ticl/android2/b;)V

    goto/16 :goto_1

    :cond_10
    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/q;->e()Lcom/google/ipc/invalidation/ticl/a/r;

    move-result-object v0

    if-eqz v0, :cond_11

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/q;->e()Lcom/google/ipc/invalidation/ticl/a/r;

    move-result-object v0

    invoke-static {p0}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    const-string/jumbo v2, "Create client: creating"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/r;->a()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/r;->c()Lcom/google/ipc/invalidation/b/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/ipc/invalidation/b/c;->b()[B

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/r;->d()Lcom/google/ipc/invalidation/ticl/a/L;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/r;->e()Z

    move-result v5

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;I[BLcom/google/ipc/invalidation/ticl/a/L;Z)V

    goto/16 :goto_1

    :cond_11
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Invalid internal downcall passed validation: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_12
    const-string/jumbo v0, "ipcinv-scheduler"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 105
    const-string/jumbo v0, "ipcinv-scheduler"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v0

    :try_start_7
    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/a/h;->a([B)Lcom/google/ipc/invalidation/ticl/a/h;
    :try_end_7
    .catch Lcom/google/ipc/invalidation/b/p; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v1

    :try_start_8
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string/jumbo v2, "Handle scheduler event: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;)Lcom/google/ipc/invalidation/ticl/android2/b;

    move-result-object v2

    if-nez v2, :cond_13

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string/jumbo v2, "Dropping event %s; Ticl state does not exist"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/h;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :catch_2
    move-exception v1

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v2

    const-string/jumbo v3, "Failed parsing SchedulerEvent from %s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Lcom/google/ipc/invalidation/b/c;->b([B)Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/p;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_13
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/j;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->a(Lcom/google/ipc/invalidation/ticl/a/h;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    invoke-static {p0, v0, v2}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/ticl/android2/b;)V

    goto/16 :goto_1

    .line 107
    :cond_14
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/j;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string/jumbo v1, "Received Intent without any recognized extras: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1
.end method
