.class public final Lcom/google/ipc/invalidation/ticl/a/c;
.super Lcom/google/ipc/invalidation/b/n;
.source "AndroidListenerProtocol.java"


# instance fields
.field private final a:J

.field private final b:Ljava/util/List;

.field private final c:Ljava/util/List;

.field private final d:Lcom/google/ipc/invalidation/b/c;

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 129
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/c;

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/google/ipc/invalidation/ticl/a/c;-><init>(Ljava/util/Collection;Ljava/util/Collection;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Integer;)V

    return-void
.end method

.method private constructor <init>(Ljava/util/Collection;Ljava/util/Collection;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Integer;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 140
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 142
    const-string/jumbo v0, "registration"

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/a/c;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/c;->b:Ljava/util/List;

    .line 143
    const-string/jumbo v0, "retry_registration_state"

    invoke-static {v0, p2}, Lcom/google/ipc/invalidation/ticl/a/c;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/c;->c:Ljava/util/List;

    .line 144
    if-eqz p3, :cond_0

    .line 145
    const/4 v0, 0x1

    .line 146
    iput-object p3, p0, Lcom/google/ipc/invalidation/ticl/a/c;->d:Lcom/google/ipc/invalidation/b/c;

    .line 150
    :goto_0
    if-eqz p4, :cond_1

    .line 151
    or-int/lit8 v0, v0, 0x2

    .line 152
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/c;->e:I

    .line 156
    :goto_1
    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/c;->a:J

    .line 157
    return-void

    .line 148
    :cond_0
    sget-object v0, Lcom/google/ipc/invalidation/b/c;->a:Lcom/google/ipc/invalidation/b/c;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/c;->d:Lcom/google/ipc/invalidation/b/c;

    move v0, v1

    goto :goto_0

    .line 154
    :cond_1
    iput v1, p0, Lcom/google/ipc/invalidation/ticl/a/c;->e:I

    goto :goto_1
.end method

.method public static a(Ljava/util/Collection;Ljava/util/Collection;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Integer;)Lcom/google/ipc/invalidation/ticl/a/c;
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/c;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/ipc/invalidation/ticl/a/c;-><init>(Ljava/util/Collection;Ljava/util/Collection;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a([B)Lcom/google/ipc/invalidation/ticl/a/c;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 208
    :try_start_0
    new-instance v0, Lcom/google/b/a/a/c;

    invoke-direct {v0}, Lcom/google/b/a/a/c;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/a/c;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    array-length v2, v2

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v1

    :goto_1
    iget-object v4, v0, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    array-length v4, v4

    if-ge v2, v4, :cond_1

    iget-object v4, v0, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    aget-object v4, v4, v2

    invoke-static {v4}, Lcom/google/ipc/invalidation/ticl/a/Y;->a(Lcom/google/b/a/a/T;)Lcom/google/ipc/invalidation/ticl/a/Y;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    iget-object v4, v0, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    array-length v4, v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    :goto_2
    iget-object v4, v0, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    array-length v4, v4

    if-ge v1, v4, :cond_2

    iget-object v4, v0, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    aget-object v4, v4, v1

    invoke-static {v4}, Lcom/google/ipc/invalidation/ticl/a/d;->a(Lcom/google/b/a/a/d;)Lcom/google/ipc/invalidation/ticl/a/d;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    new-instance v1, Lcom/google/ipc/invalidation/ticl/a/c;

    iget-object v4, v0, Lcom/google/b/a/a/c;->c:[B

    invoke-static {v4}, Lcom/google/ipc/invalidation/b/c;->a([B)Lcom/google/ipc/invalidation/b/c;

    move-result-object v4

    iget-object v0, v0, Lcom/google/b/a/a/c;->d:Ljava/lang/Integer;

    invoke-direct {v1, v3, v2, v4, v0}, Lcom/google/ipc/invalidation/ticl/a/c;-><init>(Ljava/util/Collection;Ljava/util/Collection;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Integer;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/ipc/invalidation/b/o; {:try_start_0 .. :try_end_0} :catch_1

    move-object v0, v1

    goto :goto_0

    .line 209
    :catch_0
    move-exception v0

    .line 210
    new-instance v1, Lcom/google/ipc/invalidation/b/p;

    invoke-direct {v1, v0}, Lcom/google/ipc/invalidation/b/p;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 211
    :catch_1
    move-exception v0

    .line 212
    new-instance v1, Lcom/google/ipc/invalidation/b/p;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/o;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/ipc/invalidation/b/p;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/c;->b:Ljava/util/List;

    return-object v0
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 3

    .prologue
    const/16 v2, 0x5d

    .line 194
    const-string/jumbo v0, "<AndroidListenerState:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 195
    const-string/jumbo v0, " registration=["

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/c;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/Iterable;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 196
    const-string/jumbo v0, " retry_registration_state=["

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/c;->c:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/Iterable;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 197
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/c;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    const-string/jumbo v0, " client_id="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/c;->d:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 200
    :cond_0
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/c;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    const-string/jumbo v0, " request_code_seq_num="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/c;->e:I

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(I)Lcom/google/ipc/invalidation/b/r;

    .line 203
    :cond_1
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 204
    return-void
.end method

.method protected final b()I
    .locals 4

    .prologue
    .line 181
    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/c;->a:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 182
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/c;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 183
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/c;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 184
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/c;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 185
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/c;->d:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 187
    :cond_0
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/c;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 188
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/c;->e:I

    add-int/2addr v0, v1

    .line 190
    :cond_1
    return v0
.end method

.method public final c()Ljava/util/List;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/c;->c:Ljava/util/List;

    return-object v0
.end method

.method public final d()Lcom/google/ipc/invalidation/b/c;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/c;->d:Lcom/google/ipc/invalidation/b/c;

    return-object v0
.end method

.method public final e()Z
    .locals 4

    .prologue
    .line 164
    const-wide/16 v0, 0x1

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/c;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 170
    if-ne p0, p1, :cond_1

    .line 173
    :cond_0
    :goto_0
    return v0

    .line 171
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/ticl/a/c;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 172
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/c;

    .line 173
    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/c;->a:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/c;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/c;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/c;->b:Ljava/util/List;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/c;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/c;->c:Ljava/util/List;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/c;->e()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/c;->d:Lcom/google/ipc/invalidation/b/c;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/c;->d:Lcom/google/ipc/invalidation/b/c;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/c;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/ipc/invalidation/ticl/a/c;->e:I

    iget v3, p1, Lcom/google/ipc/invalidation/ticl/a/c;->e:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/c;->e:I

    return v0
.end method

.method public final g()Z
    .locals 4

    .prologue
    .line 167
    const-wide/16 v0, 0x2

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/c;->a:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()[B
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 233
    new-instance v4, Lcom/google/b/a/a/c;

    invoke-direct {v4}, Lcom/google/b/a/a/c;-><init>()V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/b/a/a/T;

    iput-object v0, v4, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    move v1, v2

    :goto_0
    iget-object v0, v4, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    iget-object v5, v4, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/c;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/Y;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/Y;->d()Lcom/google/b/a/a/T;

    move-result-object v0

    aput-object v0, v5, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/b/a/a/d;

    iput-object v0, v4, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    :goto_1
    iget-object v0, v4, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    array-length v0, v0

    if-ge v2, v0, :cond_1

    iget-object v1, v4, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/c;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/d;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/d;->d()Lcom/google/b/a/a/d;

    move-result-object v0

    aput-object v0, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/c;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/c;->d:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/c;->b()[B

    move-result-object v0

    :goto_2
    iput-object v0, v4, Lcom/google/b/a/a/c;->c:[B

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/c;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/c;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :cond_2
    iput-object v3, v4, Lcom/google/b/a/a/c;->d:Ljava/lang/Integer;

    invoke-static {v4}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    return-object v0

    :cond_3
    move-object v0, v3

    goto :goto_2
.end method
