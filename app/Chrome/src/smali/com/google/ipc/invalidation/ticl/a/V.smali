.class public final Lcom/google/ipc/invalidation/ticl/a/V;
.super Lcom/google/ipc/invalidation/b/n;
.source "ClientProtocol.java"


# instance fields
.field private final a:Ljava/util/List;


# direct methods
.method private constructor <init>(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 2341
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 2342
    const-string/jumbo v0, "invalidation"

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/a/V;->b(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/V;->a:Ljava/util/List;

    .line 2343
    return-void
.end method

.method static a(Lcom/google/b/a/a/R;)Lcom/google/ipc/invalidation/ticl/a/V;
    .locals 3

    .prologue
    .line 2377
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 2382
    :goto_0
    return-object v0

    .line 2378
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/b/a/a/R;->a:[Lcom/google/b/a/a/S;

    array-length v0, v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2379
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/b/a/a/R;->a:[Lcom/google/b/a/a/S;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2380
    iget-object v2, p0, Lcom/google/b/a/a/R;->a:[Lcom/google/b/a/a/S;

    aget-object v2, v2, v0

    invoke-static {v2}, Lcom/google/ipc/invalidation/ticl/a/W;->a(Lcom/google/b/a/a/S;)Lcom/google/ipc/invalidation/ticl/a/W;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2379
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2382
    :cond_1
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/V;

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/V;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;)Lcom/google/ipc/invalidation/ticl/a/V;
    .locals 1

    .prologue
    .line 2336
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/V;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/ticl/a/V;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public static a([B)Lcom/google/ipc/invalidation/ticl/a/V;
    .locals 2

    .prologue
    .line 2368
    :try_start_0
    new-instance v0, Lcom/google/b/a/a/R;

    invoke-direct {v0}, Lcom/google/b/a/a/R;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/a/R;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/a/V;->a(Lcom/google/b/a/a/R;)Lcom/google/ipc/invalidation/ticl/a/V;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/ipc/invalidation/b/o; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 2369
    :catch_0
    move-exception v0

    .line 2370
    new-instance v1, Lcom/google/ipc/invalidation/b/p;

    invoke-direct {v1, v0}, Lcom/google/ipc/invalidation/b/p;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2371
    :catch_1
    move-exception v0

    .line 2372
    new-instance v1, Lcom/google/ipc/invalidation/b/p;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/o;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/ipc/invalidation/b/p;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1

    .prologue
    .line 2345
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/V;->a:Ljava/util/List;

    return-object v0
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 2

    .prologue
    .line 2361
    const-string/jumbo v0, "<InvalidationMessage:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 2362
    const-string/jumbo v0, " invalidation=["

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/V;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/Iterable;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 2363
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 2364
    return-void
.end method

.method protected final b()I
    .locals 1

    .prologue
    .line 2355
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/V;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 2357
    return v0
.end method

.method public final c()[B
    .locals 1

    .prologue
    .line 2386
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/V;->d()Lcom/google/b/a/a/R;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    return-object v0
.end method

.method final d()Lcom/google/b/a/a/R;
    .locals 4

    .prologue
    .line 2390
    new-instance v2, Lcom/google/b/a/a/R;

    invoke-direct {v2}, Lcom/google/b/a/a/R;-><init>()V

    .line 2391
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/V;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/b/a/a/S;

    iput-object v0, v2, Lcom/google/b/a/a/R;->a:[Lcom/google/b/a/a/S;

    .line 2392
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, v2, Lcom/google/b/a/a/R;->a:[Lcom/google/b/a/a/S;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 2393
    iget-object v3, v2, Lcom/google/b/a/a/R;->a:[Lcom/google/b/a/a/S;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/V;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/W;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/W;->i()Lcom/google/b/a/a/S;

    move-result-object v0

    aput-object v0, v3, v1

    .line 2392
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2395
    :cond_0
    return-object v2
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2348
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    .line 2351
    :goto_0
    return v0

    .line 2349
    :cond_0
    instance-of v0, p1, Lcom/google/ipc/invalidation/ticl/a/V;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2350
    :cond_1
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/V;

    .line 2351
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/V;->a:Ljava/util/List;

    iget-object v1, p1, Lcom/google/ipc/invalidation/ticl/a/V;->a:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/V;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
