.class Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;
.super Ljava/lang/Object;
.source "AndroidListener.java"

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/InvalidationListener;


# instance fields
.field final synthetic this$0:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;->this$0:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public informError(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;->this$0:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;

    invoke-virtual {v0, p2}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->informError(Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)V

    .line 188
    return-void
.end method

.method public final informRegistrationFailure(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;->this$0:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;

    # getter for: Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;
    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->access$000(Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;)Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->informRegistrationFailure(Lcom/google/ipc/invalidation/external/client/types/ObjectId;Z)V

    .line 164
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;->this$0:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;->this$0:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;

    # getter for: Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;
    invoke-static {v1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->access$000(Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;)Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->getClientId()Lcom/google/ipc/invalidation/b/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->b()[B

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->informRegistrationFailure([BLcom/google/ipc/invalidation/external/client/types/ObjectId;ZLjava/lang/String;)V

    .line 166
    return-void
.end method

.method public final informRegistrationStatus(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;->this$0:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;

    # getter for: Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;
    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->access$000(Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;)Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->informRegistrationSuccess(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V

    .line 156
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;->this$0:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;->this$0:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;

    # getter for: Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;
    invoke-static {v1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->access$000(Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;)Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->getClientId()Lcom/google/ipc/invalidation/b/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->b()[B

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->informRegistrationStatus([BLcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    .line 158
    return-void
.end method

.method public invalidate(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/Invalidation;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;->this$0:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;

    invoke-virtual {p3}, Lcom/google/ipc/invalidation/external/client/types/AckHandle;->getHandleData()[B

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->invalidate(Lcom/google/ipc/invalidation/external/client/types/Invalidation;[B)V

    .line 172
    return-void
.end method

.method public invalidateAll(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;->this$0:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/external/client/types/AckHandle;->getHandleData()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->invalidateAll([B)V

    .line 183
    return-void
.end method

.method public invalidateUnknownVersion(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;->this$0:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;

    invoke-virtual {p3}, Lcom/google/ipc/invalidation/external/client/types/AckHandle;->getHandleData()[B

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->invalidateUnknownVersion(Lcom/google/ipc/invalidation/external/client/types/ObjectId;[B)V

    .line 178
    return-void
.end method

.method public final ready(Lcom/google/ipc/invalidation/external/client/InvalidationClient;)V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;->this$0:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;

    # getter for: Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;
    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->access$000(Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;)Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->getClientId()Lcom/google/ipc/invalidation/b/c;

    move-result-object v0

    .line 142
    sput-object v0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->lastClientIdForTest:Lcom/google/ipc/invalidation/b/c;

    .line 143
    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;->this$0:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/c;->b()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->ready([B)V

    .line 144
    return-void
.end method

.method public final reissueRegistrations(Lcom/google/ipc/invalidation/external/client/InvalidationClient;[BI)V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;->this$0:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;->this$0:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;

    # getter for: Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;
    invoke-static {v1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->access$000(Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;)Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->getClientId()Lcom/google/ipc/invalidation/b/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->b()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->reissueRegistrations([B)V

    .line 150
    return-void
.end method
