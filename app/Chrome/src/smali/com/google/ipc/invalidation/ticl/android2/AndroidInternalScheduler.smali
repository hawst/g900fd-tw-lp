.class public final Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;
.super Ljava/lang/Object;
.source "AndroidInternalScheduler.java"

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;


# static fields
.field private static a:Z


# instance fields
.field private final b:Ljava/util/Map;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/ipc/invalidation/ticl/android2/a;

.field private e:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

.field private f:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->a:Z

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/ipc/invalidation/ticl/android2/a;)V
    .locals 2

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->b:Ljava/util/Map;

    .line 96
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->f:J

    .line 99
    invoke-static {p1}, Landroid/support/v4/app/b;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->c:Landroid/content/Context;

    .line 100
    invoke-static {p2}, Landroid/support/v4/app/b;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/android2/a;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->d:Lcom/google/ipc/invalidation/ticl/android2/a;

    .line 101
    return-void
.end method


# virtual methods
.method final a(J)V
    .locals 1

    .prologue
    .line 206
    iput-wide p1, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->f:J

    .line 207
    return-void
.end method

.method final a(Lcom/google/ipc/invalidation/ticl/a/h;)V
    .locals 6

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->b:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/ticl/a/h;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 139
    if-nez v0, :cond_0

    .line 140
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "No task registered for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/ticl/a/h;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :cond_0
    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->f:J

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/ticl/a/h;->c()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 143
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string/jumbo v1, "Ignoring event with wrong ticl id (not %s): %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 147
    :goto_0
    return-void

    .line 146
    :cond_1
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method final a(Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 156
    if-eqz v0, :cond_0

    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "Cannot overwrite task registered on "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; tasks = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 165
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 167
    :cond_0
    return-void
.end method

.method public final getCurrentTimeMs()J
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->d:Lcom/google/ipc/invalidation/ticl/android2/a;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/a;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public final isRunningOnThread()Z
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x1

    return v0
.end method

.method public final schedule(ILjava/lang/Runnable;)V
    .locals 6

    .prologue
    .line 110
    instance-of v0, p2, Lcom/google/ipc/invalidation/b/m;

    if-nez v0, :cond_0

    .line 111
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unsupported: can only schedule named runnables, not "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_0
    check-cast p2, Lcom/google/ipc/invalidation/b/m;

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/b/m;->a()Ljava/lang/String;

    move-result-object v0

    .line 118
    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->f:J

    invoke-static {v0, v2, v3}, Lcom/google/ipc/invalidation/ticl/android2/i;->a(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->c:Landroid/content/Context;

    const-class v2, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler$AlarmReceiver;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 122
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->c:Landroid/content/Context;

    const-wide v2, 0x41dfffffffc00000L    # 2.147483647E9

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-int v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 126
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->c:Landroid/content/Context;

    const-string/jumbo v2, "alarm"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 127
    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->d:Lcom/google/ipc/invalidation/ticl/android2/a;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/android2/a;->a()J

    move-result-wide v2

    int-to-long v4, p1

    add-long/2addr v2, v4

    .line 128
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 129
    return-void
.end method

.method public final setSystemResources(Lcom/google/ipc/invalidation/external/client/SystemResources;)V
    .locals 1

    .prologue
    .line 105
    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/app/b;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    .line 106
    return-void
.end method
