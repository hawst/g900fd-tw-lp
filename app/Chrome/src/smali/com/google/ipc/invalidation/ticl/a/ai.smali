.class public final Lcom/google/ipc/invalidation/ticl/a/ai;
.super Lcom/google/ipc/invalidation/b/n;
.source "ClientProtocol.java"


# instance fields
.field private final a:I

.field private final b:Lcom/google/ipc/invalidation/b/c;


# direct methods
.method private constructor <init>(Ljava/lang/Integer;Lcom/google/ipc/invalidation/b/c;)V
    .locals 2

    .prologue
    .line 777
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 778
    const-string/jumbo v0, "num_registrations"

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/a/ai;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 779
    const-string/jumbo v0, "num_registrations"

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/ai;->a(Ljava/lang/String;I)V

    .line 780
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/ipc/invalidation/ticl/a/ai;->a:I

    .line 781
    const-string/jumbo v0, "registration_digest"

    invoke-static {v0, p2}, Lcom/google/ipc/invalidation/ticl/a/ai;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 782
    const-string/jumbo v0, "registration_digest"

    invoke-static {v0, p2}, Lcom/google/ipc/invalidation/ticl/a/ai;->a(Ljava/lang/String;Lcom/google/ipc/invalidation/b/c;)V

    .line 783
    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/a/ai;->b:Lcom/google/ipc/invalidation/b/c;

    .line 784
    return-void
.end method

.method public static a(ILcom/google/ipc/invalidation/b/c;)Lcom/google/ipc/invalidation/ticl/a/ai;
    .locals 2

    .prologue
    .line 770
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/ai;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/ipc/invalidation/ticl/a/ai;-><init>(Ljava/lang/Integer;Lcom/google/ipc/invalidation/b/c;)V

    return-object v0
.end method

.method static a(Lcom/google/b/a/a/ad;)Lcom/google/ipc/invalidation/ticl/a/ai;
    .locals 3

    .prologue
    .line 823
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 824
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/ai;

    iget-object v1, p0, Lcom/google/b/a/a/ad;->a:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/google/b/a/a/ad;->b:[B

    invoke-static {v2}, Lcom/google/ipc/invalidation/b/c;->a([B)Lcom/google/ipc/invalidation/b/c;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/ticl/a/ai;-><init>(Ljava/lang/Integer;Lcom/google/ipc/invalidation/b/c;)V

    goto :goto_0
.end method


# virtual methods
.method final a()Lcom/google/b/a/a/ad;
    .locals 2

    .prologue
    .line 833
    new-instance v0, Lcom/google/b/a/a/ad;

    invoke-direct {v0}, Lcom/google/b/a/a/ad;-><init>()V

    .line 834
    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/ai;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/b/a/a/ad;->a:Ljava/lang/Integer;

    .line 835
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/ai;->b:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->b()[B

    move-result-object v1

    iput-object v1, v0, Lcom/google/b/a/a/ad;->b:[B

    .line 836
    return-object v0
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 2

    .prologue
    .line 806
    const-string/jumbo v0, "<RegistrationSummary:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 807
    const-string/jumbo v0, " num_registrations="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/ai;->a:I

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(I)Lcom/google/ipc/invalidation/b/r;

    .line 808
    const-string/jumbo v0, " registration_digest="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/ai;->b:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 809
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 810
    return-void
.end method

.method protected final b()I
    .locals 2

    .prologue
    .line 799
    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/ai;->a:I

    add-int/lit8 v0, v0, 0x1f

    .line 801
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/ai;->b:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/b/c;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 802
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 791
    if-ne p0, p1, :cond_1

    .line 794
    :cond_0
    :goto_0
    return v0

    .line 792
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/ticl/a/ai;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 793
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/ai;

    .line 794
    iget v2, p0, Lcom/google/ipc/invalidation/ticl/a/ai;->a:I

    iget v3, p1, Lcom/google/ipc/invalidation/ticl/a/ai;->a:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/ai;->b:Lcom/google/ipc/invalidation/b/c;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/ai;->b:Lcom/google/ipc/invalidation/b/c;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/ai;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
