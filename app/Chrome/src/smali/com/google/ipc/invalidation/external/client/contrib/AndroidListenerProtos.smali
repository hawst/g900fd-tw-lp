.class Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;
.super Ljava/lang/Object;
.source "AndroidListenerProtos.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    return-void
.end method

.method static isValidAndroidListenerState(Lcom/google/ipc/invalidation/ticl/a/c;)Z
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/c;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/c;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isValidRegistrationCommand(Lcom/google/ipc/invalidation/ticl/a/e;)Z
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/e;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/e;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/e;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isValidStartCommand(Lcom/google/ipc/invalidation/ticl/a/f;)Z
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/f;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/f;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static newAndroidListenerState(Lcom/google/ipc/invalidation/b/c;ILjava/util/Map;Ljava/util/Collection;)Lcom/google/ipc/invalidation/ticl/a/c;
    .locals 4

    .prologue
    .line 56
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 58
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 59
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/ai;

    invoke-static {v1, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newRetryRegistrationState(Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/ticl/ai;)Lcom/google/ipc/invalidation/ticl/a/d;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 62
    :cond_0
    invoke-static {p3}, Landroid/support/v4/app/b;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v2, p0, v1}, Lcom/google/ipc/invalidation/ticl/a/c;->a(Ljava/util/Collection;Ljava/util/Collection;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Integer;)Lcom/google/ipc/invalidation/ticl/a/c;

    move-result-object v0

    return-object v0
.end method

.method static newDelayedRegisterCommand(Lcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/ipc/invalidation/ticl/a/e;
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newDelayedRegistrationCommand(Lcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Z)Lcom/google/ipc/invalidation/ticl/a/e;

    move-result-object v0

    return-object v0
.end method

.method private static newDelayedRegistrationCommand(Lcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Z)Lcom/google/ipc/invalidation/ticl/a/e;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 105
    invoke-static {p1}, Landroid/support/v4/app/b;->a(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/ipc/invalidation/ticl/a/Y;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v0, p0, v2}, Lcom/google/ipc/invalidation/ticl/a/e;->a(Ljava/lang/Boolean;Ljava/util/Collection;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Boolean;)Lcom/google/ipc/invalidation/ticl/a/e;

    move-result-object v0

    return-object v0
.end method

.method static newDelayedUnregisterCommand(Lcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/ipc/invalidation/ticl/a/e;
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newDelayedRegistrationCommand(Lcom/google/ipc/invalidation/b/c;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Z)Lcom/google/ipc/invalidation/ticl/a/e;

    move-result-object v0

    return-object v0
.end method

.method static newRegistrationCommand(Lcom/google/ipc/invalidation/b/c;Ljava/lang/Iterable;Z)Lcom/google/ipc/invalidation/ticl/a/e;
    .locals 3

    .prologue
    .line 97
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1}, Landroid/support/v4/app/b;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v1, p0, v2}, Lcom/google/ipc/invalidation/ticl/a/e;->a(Ljava/lang/Boolean;Ljava/util/Collection;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Boolean;)Lcom/google/ipc/invalidation/ticl/a/e;

    move-result-object v0

    return-object v0
.end method

.method static newRetryRegistrationState(Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/ticl/ai;)Lcom/google/ipc/invalidation/ticl/a/d;
    .locals 2

    .prologue
    .line 70
    invoke-static {p0}, Landroid/support/v4/app/b;->a(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/ipc/invalidation/ticl/a/Y;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/ticl/ai;->a()Lcom/google/ipc/invalidation/ticl/a/F;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/d;->a(Lcom/google/ipc/invalidation/ticl/a/Y;Lcom/google/ipc/invalidation/ticl/a/F;)Lcom/google/ipc/invalidation/ticl/a/d;

    move-result-object v0

    return-object v0
.end method

.method static newStartCommand(ILcom/google/ipc/invalidation/b/c;Z)Lcom/google/ipc/invalidation/ticl/a/f;
    .locals 2

    .prologue
    .line 92
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/google/ipc/invalidation/ticl/a/f;->a(Ljava/lang/Integer;Lcom/google/ipc/invalidation/b/c;Ljava/lang/Boolean;)Lcom/google/ipc/invalidation/ticl/a/f;

    move-result-object v0

    return-object v0
.end method
