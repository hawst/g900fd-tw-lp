.class public Lcom/google/ipc/invalidation/ticl/ah;
.super Ljava/lang/Object;
.source "AndroidNetworkChannel.java"

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/google/ipc/invalidation/ticl/android2/j;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Landroid/support/v4/app/b;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/ah;->a:Landroid/content/Context;

    .line 39
    return-void
.end method


# virtual methods
.method public sendMessage([B)V
    .locals 3

    .prologue
    .line 43
    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/android2/i;->a([B)Landroid/content/Intent;

    move-result-object v0

    .line 44
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/ah;->a:Landroid/content/Context;

    const-class v2, Lcom/google/ipc/invalidation/ticl/android2/channel/AndroidMessageSenderService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 45
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/ah;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 46
    return-void
.end method

.method public setListener(Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ah;->b:Lcom/google/ipc/invalidation/ticl/android2/j;

    invoke-virtual {v0, p1}, Lcom/google/ipc/invalidation/ticl/android2/j;->a(Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;)V

    .line 51
    return-void
.end method

.method public setSystemResources(Lcom/google/ipc/invalidation/external/client/SystemResources;)V
    .locals 1

    .prologue
    .line 55
    invoke-static {p1}, Landroid/support/v4/app/b;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/android2/j;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/ah;->b:Lcom/google/ipc/invalidation/ticl/android2/j;

    .line 56
    return-void
.end method
