.class public final Lcom/google/ipc/invalidation/ticl/a/S;
.super Lcom/google/ipc/invalidation/b/n;
.source "ClientProtocol.java"


# static fields
.field public static final a:Lcom/google/ipc/invalidation/ticl/a/S;


# instance fields
.field private final b:J

.field private final c:Lcom/google/ipc/invalidation/ticl/a/P;

.field private final d:Ljava/util/List;

.field private final e:Ljava/util/List;

.field private final f:Z

.field private final g:Lcom/google/ipc/invalidation/ticl/a/L;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1462
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/S;

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/ipc/invalidation/ticl/a/S;-><init>(Lcom/google/ipc/invalidation/ticl/a/P;Ljava/util/Collection;Ljava/util/Collection;Ljava/lang/Boolean;Lcom/google/ipc/invalidation/ticl/a/L;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/S;->a:Lcom/google/ipc/invalidation/ticl/a/S;

    return-void
.end method

.method private constructor <init>(Lcom/google/ipc/invalidation/ticl/a/P;Ljava/util/Collection;Ljava/util/Collection;Ljava/lang/Boolean;Lcom/google/ipc/invalidation/ticl/a/L;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1475
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 1477
    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/S;->c:Lcom/google/ipc/invalidation/ticl/a/P;

    .line 1478
    const-string/jumbo v1, "config_parameter"

    invoke-static {v1, p2}, Lcom/google/ipc/invalidation/ticl/a/S;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/S;->d:Ljava/util/List;

    .line 1479
    const-string/jumbo v1, "performance_counter"

    invoke-static {v1, p3}, Lcom/google/ipc/invalidation/ticl/a/S;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/S;->e:Ljava/util/List;

    .line 1480
    if-eqz p4, :cond_0

    .line 1481
    const/4 v0, 0x1

    .line 1482
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/S;->f:Z

    .line 1486
    :goto_0
    iput-object p5, p0, Lcom/google/ipc/invalidation/ticl/a/S;->g:Lcom/google/ipc/invalidation/ticl/a/L;

    .line 1487
    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/S;->b:J

    .line 1488
    return-void

    .line 1484
    :cond_0
    iput-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/S;->f:Z

    goto :goto_0
.end method

.method static a(Lcom/google/b/a/a/O;)Lcom/google/ipc/invalidation/ticl/a/S;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1556
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 1565
    :goto_0
    return-object v0

    .line 1557
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    array-length v0, v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    .line 1558
    :goto_1
    iget-object v3, p0, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 1559
    iget-object v3, p0, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    aget-object v3, v3, v0

    invoke-static {v3}, Lcom/google/ipc/invalidation/ticl/a/Z;->a(Lcom/google/b/a/a/U;)Lcom/google/ipc/invalidation/ticl/a/Z;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1558
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1561
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    array-length v0, v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1562
    :goto_2
    iget-object v0, p0, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    array-length v0, v0

    if-ge v1, v0, :cond_2

    .line 1563
    iget-object v0, p0, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/a/Z;->a(Lcom/google/b/a/a/U;)Lcom/google/ipc/invalidation/ticl/a/Z;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1562
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1565
    :cond_2
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/S;

    iget-object v1, p0, Lcom/google/b/a/a/O;->a:Lcom/google/b/a/a/L;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/P;->a(Lcom/google/b/a/a/L;)Lcom/google/ipc/invalidation/ticl/a/P;

    move-result-object v1

    iget-object v4, p0, Lcom/google/b/a/a/O;->d:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/google/b/a/a/O;->e:Lcom/google/b/a/a/I;

    invoke-static {v5}, Lcom/google/ipc/invalidation/ticl/a/L;->a(Lcom/google/b/a/a/I;)Lcom/google/ipc/invalidation/ticl/a/L;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/ipc/invalidation/ticl/a/S;-><init>(Lcom/google/ipc/invalidation/ticl/a/P;Ljava/util/Collection;Ljava/util/Collection;Ljava/lang/Boolean;Lcom/google/ipc/invalidation/ticl/a/L;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/ipc/invalidation/ticl/a/P;Ljava/util/Collection;Ljava/util/Collection;Ljava/lang/Boolean;Lcom/google/ipc/invalidation/ticl/a/L;)Lcom/google/ipc/invalidation/ticl/a/S;
    .locals 6

    .prologue
    .line 1459
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/S;

    const/4 v2, 0x0

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/ipc/invalidation/ticl/a/S;-><init>(Lcom/google/ipc/invalidation/ticl/a/P;Ljava/util/Collection;Ljava/util/Collection;Ljava/lang/Boolean;Lcom/google/ipc/invalidation/ticl/a/L;)V

    return-object v0
.end method

.method private c()Z
    .locals 4

    .prologue
    .line 1497
    const-wide/16 v0, 0x1

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/S;->b:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a()Lcom/google/b/a/a/O;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 1577
    new-instance v4, Lcom/google/b/a/a/O;

    invoke-direct {v4}, Lcom/google/b/a/a/O;-><init>()V

    .line 1578
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/S;->c:Lcom/google/ipc/invalidation/ticl/a/P;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/S;->c:Lcom/google/ipc/invalidation/ticl/a/P;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/P;->a()Lcom/google/b/a/a/L;

    move-result-object v0

    :goto_0
    iput-object v0, v4, Lcom/google/b/a/a/O;->a:Lcom/google/b/a/a/L;

    .line 1579
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/S;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/b/a/a/U;

    iput-object v0, v4, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    move v2, v3

    .line 1580
    :goto_1
    iget-object v0, v4, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    array-length v0, v0

    if-ge v2, v0, :cond_1

    .line 1581
    iget-object v5, v4, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/S;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/Z;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/Z;->d()Lcom/google/b/a/a/U;

    move-result-object v0

    aput-object v0, v5, v2

    .line 1580
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    move-object v0, v1

    .line 1578
    goto :goto_0

    .line 1583
    :cond_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/S;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/b/a/a/U;

    iput-object v0, v4, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    .line 1584
    :goto_2
    iget-object v0, v4, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    array-length v0, v0

    if-ge v3, v0, :cond_2

    .line 1585
    iget-object v2, v4, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/S;->e:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/a/Z;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/Z;->d()Lcom/google/b/a/a/U;

    move-result-object v0

    aput-object v0, v2, v3

    .line 1584
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1587
    :cond_2
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/S;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/a/S;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_3
    iput-object v0, v4, Lcom/google/b/a/a/O;->d:Ljava/lang/Boolean;

    .line 1588
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/S;->g:Lcom/google/ipc/invalidation/ticl/a/L;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/S;->g:Lcom/google/ipc/invalidation/ticl/a/L;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/L;->n()Lcom/google/b/a/a/I;

    move-result-object v1

    :cond_3
    iput-object v1, v4, Lcom/google/b/a/a/O;->e:Lcom/google/b/a/a/I;

    .line 1589
    return-object v4

    :cond_4
    move-object v0, v1

    .line 1587
    goto :goto_3
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 3

    .prologue
    const/16 v2, 0x5d

    .line 1530
    const-string/jumbo v0, "<InfoMessage:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 1531
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/S;->c:Lcom/google/ipc/invalidation/ticl/a/P;

    if-eqz v0, :cond_0

    .line 1532
    const-string/jumbo v0, " client_version="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/S;->c:Lcom/google/ipc/invalidation/ticl/a/P;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1534
    :cond_0
    const-string/jumbo v0, " config_parameter=["

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/S;->d:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/Iterable;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 1535
    const-string/jumbo v0, " performance_counter=["

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/S;->e:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/Iterable;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 1536
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/S;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1537
    const-string/jumbo v0, " server_registration_summary_requested="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/S;->f:Z

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Z)Lcom/google/ipc/invalidation/b/r;

    .line 1539
    :cond_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/S;->g:Lcom/google/ipc/invalidation/ticl/a/L;

    if-eqz v0, :cond_2

    .line 1540
    const-string/jumbo v0, " client_config="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/S;->g:Lcom/google/ipc/invalidation/ticl/a/L;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 1542
    :cond_2
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 1543
    return-void
.end method

.method protected final b()I
    .locals 4

    .prologue
    .line 1514
    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/a/S;->b:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 1515
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/S;->c:Lcom/google/ipc/invalidation/ticl/a/P;

    if-eqz v1, :cond_0

    .line 1516
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/S;->c:Lcom/google/ipc/invalidation/ticl/a/P;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/P;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1518
    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/S;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1519
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/S;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1520
    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/S;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1521
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/a/S;->f:Z

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/S;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 1523
    :cond_1
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/S;->g:Lcom/google/ipc/invalidation/ticl/a/L;

    if-eqz v1, :cond_2

    .line 1524
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/S;->g:Lcom/google/ipc/invalidation/ticl/a/L;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/L;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1526
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1502
    if-ne p0, p1, :cond_1

    .line 1505
    :cond_0
    :goto_0
    return v0

    .line 1503
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/ticl/a/S;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 1504
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/S;

    .line 1505
    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/a/S;->b:J

    iget-wide v4, p1, Lcom/google/ipc/invalidation/ticl/a/S;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/S;->c:Lcom/google/ipc/invalidation/ticl/a/P;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/S;->c:Lcom/google/ipc/invalidation/ticl/a/P;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/S;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/S;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/S;->d:Ljava/util/List;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/S;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/S;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/S;->e:Ljava/util/List;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/S;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/S;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/ipc/invalidation/ticl/a/S;->f:Z

    iget-boolean v3, p1, Lcom/google/ipc/invalidation/ticl/a/S;->f:Z

    if-ne v2, v3, :cond_4

    :cond_3
    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/S;->g:Lcom/google/ipc/invalidation/ticl/a/L;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/S;->g:Lcom/google/ipc/invalidation/ticl/a/L;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/S;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method
