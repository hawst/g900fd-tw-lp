.class public final Lcom/google/ipc/invalidation/ticl/a/ae;
.super Lcom/google/ipc/invalidation/b/n;
.source "ClientProtocol.java"


# instance fields
.field private final a:Lcom/google/ipc/invalidation/ticl/a/Y;

.field private final b:I


# direct methods
.method private constructor <init>(Lcom/google/ipc/invalidation/ticl/a/Y;Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 706
    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/n;-><init>()V

    .line 707
    const-string/jumbo v0, "object_id"

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/ticl/a/ae;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 708
    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/ae;->a:Lcom/google/ipc/invalidation/ticl/a/Y;

    .line 709
    const-string/jumbo v0, "op_type"

    invoke-static {v0, p2}, Lcom/google/ipc/invalidation/ticl/a/ae;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 710
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/ipc/invalidation/ticl/a/ae;->b:I

    .line 711
    return-void
.end method

.method static a(Lcom/google/b/a/a/Z;)Lcom/google/ipc/invalidation/ticl/a/ae;
    .locals 3

    .prologue
    .line 750
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 751
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/ae;

    iget-object v1, p0, Lcom/google/b/a/a/Z;->a:Lcom/google/b/a/a/T;

    invoke-static {v1}, Lcom/google/ipc/invalidation/ticl/a/Y;->a(Lcom/google/b/a/a/T;)Lcom/google/ipc/invalidation/ticl/a/Y;

    move-result-object v1

    iget-object v2, p0, Lcom/google/b/a/a/Z;->b:Ljava/lang/Integer;

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/ticl/a/ae;-><init>(Lcom/google/ipc/invalidation/ticl/a/Y;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/ipc/invalidation/ticl/a/Y;I)Lcom/google/ipc/invalidation/ticl/a/ae;
    .locals 2

    .prologue
    .line 699
    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/ae;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/ipc/invalidation/ticl/a/ae;-><init>(Lcom/google/ipc/invalidation/ticl/a/Y;Ljava/lang/Integer;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/ipc/invalidation/ticl/a/Y;
    .locals 1

    .prologue
    .line 713
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/ae;->a:Lcom/google/ipc/invalidation/ticl/a/Y;

    return-object v0
.end method

.method public final a(Lcom/google/ipc/invalidation/b/r;)V
    .locals 2

    .prologue
    .line 733
    const-string/jumbo v0, "<RegistrationP:"

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    .line 734
    const-string/jumbo v0, " object_id="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/ae;->a:Lcom/google/ipc/invalidation/ticl/a/Y;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(Lcom/google/ipc/invalidation/b/h;)Lcom/google/ipc/invalidation/b/r;

    .line 735
    const-string/jumbo v0, " op_type="

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/r;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/ae;->b:I

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/r;->a(I)Lcom/google/ipc/invalidation/b/r;

    .line 736
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/ipc/invalidation/b/r;->a(C)Lcom/google/ipc/invalidation/b/r;

    .line 737
    return-void
.end method

.method protected final b()I
    .locals 2

    .prologue
    .line 726
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/ae;->a:Lcom/google/ipc/invalidation/ticl/a/Y;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/Y;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 728
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/ae;->b:I

    add-int/2addr v0, v1

    .line 729
    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 715
    iget v0, p0, Lcom/google/ipc/invalidation/ticl/a/ae;->b:I

    return v0
.end method

.method final d()Lcom/google/b/a/a/Z;
    .locals 2

    .prologue
    .line 760
    new-instance v0, Lcom/google/b/a/a/Z;

    invoke-direct {v0}, Lcom/google/b/a/a/Z;-><init>()V

    .line 761
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/ae;->a:Lcom/google/ipc/invalidation/ticl/a/Y;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/a/Y;->d()Lcom/google/b/a/a/T;

    move-result-object v1

    iput-object v1, v0, Lcom/google/b/a/a/Z;->a:Lcom/google/b/a/a/T;

    .line 762
    iget v1, p0, Lcom/google/ipc/invalidation/ticl/a/ae;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/b/a/a/Z;->b:Ljava/lang/Integer;

    .line 763
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 718
    if-ne p0, p1, :cond_1

    .line 721
    :cond_0
    :goto_0
    return v0

    .line 719
    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/ticl/a/ae;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 720
    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/ticl/a/ae;

    .line 721
    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/ae;->a:Lcom/google/ipc/invalidation/ticl/a/Y;

    iget-object v3, p1, Lcom/google/ipc/invalidation/ticl/a/ae;->a:Lcom/google/ipc/invalidation/ticl/a/Y;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/ticl/a/ae;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/google/ipc/invalidation/ticl/a/ae;->b:I

    iget v3, p1, Lcom/google/ipc/invalidation/ticl/a/ae;->b:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
