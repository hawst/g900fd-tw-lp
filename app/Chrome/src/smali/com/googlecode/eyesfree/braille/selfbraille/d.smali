.class public Lcom/googlecode/eyesfree/braille/selfbraille/d;
.super Ljava/lang/Object;
.source "SelfBrailleClient.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Landroid/content/Intent;

.field private static final c:[B


# instance fields
.field private final d:Landroid/os/Binder;

.field private final e:Landroid/content/Context;

.field private final f:Z

.field private final g:Lcom/googlecode/eyesfree/braille/selfbraille/f;

.field private h:Z

.field private volatile i:Lcom/googlecode/eyesfree/braille/selfbraille/e;

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 44
    const-class v0, Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->a:Ljava/lang/String;

    .line 50
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.googlecode.eyesfree.braille.service.ACTION_SELF_BRAILLE_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "com.googlecode.eyesfree.brailleback"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->b:Landroid/content/Intent;

    .line 61
    const/16 v0, 0x14

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->c:[B

    return-void

    nop

    :array_0
    .array-data 1
        -0x65t
        0x42t
        0x4ct
        0x2dt
        0x27t
        -0x53t
        0x51t
        -0x5ct
        0x2at
        0x33t
        0x7et
        0xbt
        -0x4at
        -0x67t
        0x1ct
        0x76t
        -0x14t
        -0x5ct
        0x44t
        0x61t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->d:Landroid/os/Binder;

    .line 78
    new-instance v0, Lcom/googlecode/eyesfree/braille/selfbraille/f;

    invoke-direct {v0, p0, v1}, Lcom/googlecode/eyesfree/braille/selfbraille/f;-><init>(Lcom/googlecode/eyesfree/braille/selfbraille/d;B)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->g:Lcom/googlecode/eyesfree/braille/selfbraille/f;

    .line 79
    iput-boolean v1, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->h:Z

    .line 87
    iput v1, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->j:I

    .line 97
    iput-object p1, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->e:Landroid/content/Context;

    .line 98
    iput-boolean p2, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->f:Z

    .line 99
    invoke-direct {p0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->c()V

    .line 100
    return-void
.end method

.method static synthetic a(Lcom/googlecode/eyesfree/braille/selfbraille/d;I)I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->j:I

    return v0
.end method

.method static synthetic a(Lcom/googlecode/eyesfree/braille/selfbraille/d;)Z
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->f()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/googlecode/eyesfree/braille/selfbraille/d;)Lcom/googlecode/eyesfree/braille/selfbraille/f;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->g:Lcom/googlecode/eyesfree/braille/selfbraille/f;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/googlecode/eyesfree/braille/selfbraille/d;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->j:I

    return v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 124
    new-instance v0, Lcom/googlecode/eyesfree/braille/selfbraille/e;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/googlecode/eyesfree/braille/selfbraille/e;-><init>(Lcom/googlecode/eyesfree/braille/selfbraille/d;B)V

    .line 125
    iget-object v1, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->e:Landroid/content/Context;

    sget-object v2, Lcom/googlecode/eyesfree/braille/selfbraille/d;->b:Landroid/content/Intent;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 127
    sget-object v0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->a:Ljava/lang/String;

    const-string/jumbo v1, "Failed to bind to service"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->g:Lcom/googlecode/eyesfree/braille/selfbraille/f;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/braille/selfbraille/f;->a()V

    .line 133
    :goto_0
    return-void

    .line 131
    :cond_0
    iput-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->i:Lcom/googlecode/eyesfree/braille/selfbraille/e;

    .line 132
    sget-object v0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->a:Ljava/lang/String;

    const-string/jumbo v1, "Bound to self braille service"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic d(Lcom/googlecode/eyesfree/braille/selfbraille/d;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->j:I

    return v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->i:Lcom/googlecode/eyesfree/braille/selfbraille/e;

    if-eqz v0, :cond_1

    .line 137
    invoke-direct {p0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->e()Lcom/googlecode/eyesfree/braille/selfbraille/a;

    move-result-object v0

    .line 138
    if-eqz v0, :cond_0

    .line 140
    :try_start_0
    iget-object v1, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->d:Landroid/os/Binder;

    invoke-interface {v0, v1}, Lcom/googlecode/eyesfree/braille/selfbraille/a;->a(Landroid/os/IBinder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->i:Lcom/googlecode/eyesfree/braille/selfbraille/e;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->i:Lcom/googlecode/eyesfree/braille/selfbraille/e;

    .line 148
    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private e()Lcom/googlecode/eyesfree/braille/selfbraille/a;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->i:Lcom/googlecode/eyesfree/braille/selfbraille/e;

    .line 152
    if-eqz v0, :cond_0

    .line 153
    invoke-static {v0}, Lcom/googlecode/eyesfree/braille/selfbraille/e;->a(Lcom/googlecode/eyesfree/braille/selfbraille/e;)Lcom/googlecode/eyesfree/braille/selfbraille/a;

    move-result-object v0

    .line 155
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/googlecode/eyesfree/braille/selfbraille/d;)Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->h:Z

    return v0
.end method

.method static synthetic f(Lcom/googlecode/eyesfree/braille/selfbraille/d;)Lcom/googlecode/eyesfree/braille/selfbraille/e;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->i:Lcom/googlecode/eyesfree/braille/selfbraille/e;

    return-object v0
.end method

.method private f()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 159
    iget-object v2, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 162
    :try_start_0
    const-string/jumbo v3, "com.googlecode.eyesfree.brailleback"

    const/16 v4, 0x40

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 171
    :try_start_1
    const-string/jumbo v3, "SHA-1"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 177
    iget-object v4, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 178
    invoke-virtual {v6}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/security/MessageDigest;->update([B)V

    .line 179
    sget-object v6, Lcom/googlecode/eyesfree/braille/selfbraille/d;->c:[B

    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v7

    invoke-static {v6, v7}, Ljava/security/MessageDigest;->isEqual([B[B)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 191
    :goto_1
    return v0

    .line 164
    :catch_0
    move-exception v0

    .line 165
    sget-object v2, Lcom/googlecode/eyesfree/braille/selfbraille/d;->a:Ljava/lang/String;

    const-string/jumbo v3, "Can\'t verify package com.googlecode.eyesfree.brailleback"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    .line 167
    goto :goto_1

    .line 172
    :catch_1
    move-exception v0

    .line 173
    sget-object v2, Lcom/googlecode/eyesfree/braille/selfbraille/d;->a:Ljava/lang/String;

    const-string/jumbo v3, "SHA-1 not supported"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    .line 174
    goto :goto_1

    .line 182
    :cond_0
    invoke-virtual {v3}, Ljava/security/MessageDigest;->reset()V

    .line 177
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 184
    :cond_1
    iget-boolean v2, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->f:Z

    if-eqz v2, :cond_2

    .line 185
    sget-object v2, Lcom/googlecode/eyesfree/braille/selfbraille/d;->a:Ljava/lang/String;

    const-string/jumbo v3, "*** %s connected to BrailleBack with invalid (debug?) signature ***"

    new-array v4, v0, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->e:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    move v0, v1

    .line 191
    goto :goto_1
.end method

.method static synthetic g(Lcom/googlecode/eyesfree/braille/selfbraille/d;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->d()V

    return-void
.end method

.method static synthetic h(Lcom/googlecode/eyesfree/braille/selfbraille/d;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->c()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->h:Z

    .line 108
    invoke-direct {p0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->d()V

    .line 109
    return-void
.end method

.method public final a(Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;)V
    .locals 3

    .prologue
    .line 112
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->a()V

    .line 113
    invoke-direct {p0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->e()Lcom/googlecode/eyesfree/braille/selfbraille/a;

    move-result-object v0

    .line 114
    if-eqz v0, :cond_0

    .line 116
    :try_start_0
    iget-object v1, p0, Lcom/googlecode/eyesfree/braille/selfbraille/d;->d:Landroid/os/Binder;

    invoke-interface {v0, v1, p1}, Lcom/googlecode/eyesfree/braille/selfbraille/a;->a(Landroid/os/IBinder;Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 117
    :catch_0
    move-exception v0

    .line 118
    sget-object v1, Lcom/googlecode/eyesfree/braille/selfbraille/d;->a:Ljava/lang/String;

    const-string/jumbo v2, "Self braille write failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
