.class public Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;
.super Ljava/lang/Object;
.source "WriteData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Landroid/view/accessibility/AccessibilityNodeInfo;

.field private b:Ljava/lang/CharSequence;

.field private c:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 143
    new-instance v0, Lcom/googlecode/eyesfree/braille/selfbraille/g;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/braille/selfbraille/g;-><init>()V

    sput-object v0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->c:Landroid/os/Bundle;

    .line 177
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->c:Landroid/os/Bundle;

    .line 180
    sget-object v0, Landroid/view/accessibility/AccessibilityNodeInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityNodeInfo;

    iput-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->a:Landroid/view/accessibility/AccessibilityNodeInfo;

    .line 182
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->b:Ljava/lang/CharSequence;

    .line 183
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->c:Landroid/os/Bundle;

    .line 184
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static a(Landroid/view/View;)Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;
    .locals 2

    .prologue
    .line 42
    invoke-static {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain(Landroid/view/View;)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 43
    new-instance v1, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;

    invoke-direct {v1}, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;-><init>()V

    .line 44
    iput-object v0, v1, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->a:Landroid/view/accessibility/AccessibilityNodeInfo;

    .line 45
    return-object v1
.end method

.method private b()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->c:Landroid/os/Bundle;

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    if-ne v0, v1, :cond_0

    .line 107
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->c:Landroid/os/Bundle;

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->c:Landroid/os/Bundle;

    return-object v0
.end method


# virtual methods
.method public final a(I)Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->b()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "selectionStart"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 74
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->b:Ljava/lang/CharSequence;

    .line 60
    return-object p0
.end method

.method public final a()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 118
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->a:Landroid/view/accessibility/AccessibilityNodeInfo;

    if-nez v0, :cond_0

    .line 119
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Accessibility node info can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "selectionStart"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 123
    iget-object v1, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "selectionEnd"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 124
    iget-object v2, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_2

    .line 125
    if-gtz v0, :cond_1

    if-lez v1, :cond_5

    .line 126
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Selection can\'t be set without text"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_2
    if-gez v0, :cond_3

    if-ltz v1, :cond_3

    .line 131
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Selection end without start"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134
    :cond_3
    iget-object v2, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->b:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 135
    if-gt v0, v2, :cond_4

    if-le v1, v2, :cond_5

    .line 136
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Selection out of bounds"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 139
    :cond_5
    return-void
.end method

.method public final b(I)Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;
    .locals 2

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->b()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "selectionEnd"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 95
    return-object p0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->a:Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {v0, p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 171
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->a:Landroid/view/accessibility/AccessibilityNodeInfo;

    .line 172
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->b:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 174
    return-void
.end method
