.class public Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;
.super Ljava/lang/Object;
.source "BrailleInputEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final a:Landroid/util/SparseArray;

.field private static final b:Ljava/util/HashMap;


# instance fields
.field private final c:I

.field private final d:I

.field private final e:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 147
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    .line 149
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->b:Ljava/util/HashMap;

    .line 152
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/4 v1, 0x1

    const-string/jumbo v2, "CMD_NAV_LINE_PREVIOUS"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 153
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/4 v1, 0x2

    const-string/jumbo v2, "CMD_NAV_LINE_NEXT"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 154
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/4 v1, 0x3

    const-string/jumbo v2, "CMD_NAV_ITEM_PREVIOUS"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 155
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/4 v1, 0x4

    const-string/jumbo v2, "CMD_NAV_ITEM_NEXT"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 156
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/4 v1, 0x5

    const-string/jumbo v2, "CMD_NAV_PAN_LEFT"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 157
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/4 v1, 0x6

    const-string/jumbo v2, "CMD_NAV_PAN_RIGHT"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 158
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/4 v1, 0x7

    const-string/jumbo v2, "CMD_NAV_TOP"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 159
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x8

    const-string/jumbo v2, "CMD_NAV_BOTTOM"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 160
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x14

    const-string/jumbo v2, "CMD_ACTIVATE_CURRENT"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 161
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1e

    const-string/jumbo v2, "CMD_SCROLL_BACKWARD"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 162
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1f

    const-string/jumbo v2, "CMD_SCROLL_FORWARD"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 163
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x28

    const-string/jumbo v2, "CMD_SELECTION_START"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 164
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x29

    const-string/jumbo v2, "CMD_SELECTION_END"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 165
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x2a

    const-string/jumbo v2, "CMD_SELECTION_SELECT_ALL"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 166
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x2b

    const-string/jumbo v2, "CMD_SELECTION_CUT"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 167
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x2c

    const-string/jumbo v2, "CMD_SELECTION_COPY"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 168
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x2d

    const-string/jumbo v2, "CMD_SELECTION_PASTE"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 169
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x32

    const-string/jumbo v2, "CMD_ROUTE"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 170
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x3c

    const-string/jumbo v2, "CMD_BRAILLE_KEY"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 171
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x46

    const-string/jumbo v2, "CMD_KEY_ENTER"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 172
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x47

    const-string/jumbo v2, "CMD_KEY_DEL"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 173
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x48

    const-string/jumbo v2, "CMD_KEY_FORWARD_DEL"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 174
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x5a

    const-string/jumbo v2, "CMD_GLOBAL_BACK"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 175
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x5b

    const-string/jumbo v2, "CMD_GLOBAL_HOME"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 176
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x5c

    const-string/jumbo v2, "CMD_GLOBAL_RECENTS"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 177
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x5d

    const-string/jumbo v2, "CMD_GLOBAL_NOTIFICATIONS"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 178
    sget-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    const/16 v1, 0x64

    const-string/jumbo v2, "CMD_HELP"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 179
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 180
    sget-object v1, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->b:Ljava/util/HashMap;

    sget-object v2, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 270
    :cond_0
    new-instance v0, Lcom/googlecode/eyesfree/braille/display/b;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/braille/display/b;-><init>()V

    sput-object v0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->c:I

    .line 297
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->d:I

    .line 298
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->e:J

    .line 299
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 285
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 258
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 259
    const-string/jumbo v0, "BrailleInputEvent {"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    const-string/jumbo v0, "amd="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    iget v0, p0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->c:I

    sget-object v2, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->a:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    const-string/jumbo v0, ", arg="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    iget v0, p0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->d:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 264
    const-string/jumbo v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 261
    :cond_0
    const-string/jumbo v0, "(unknown)"

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 290
    iget v0, p0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 291
    iget v0, p0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 292
    iget-wide v0, p0, Lcom/googlecode/eyesfree/braille/display/BrailleInputEvent;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 293
    return-void
.end method
