.class final Lcom/googlecode/eyesfree/braille/selfbraille/f;
.super Landroid/os/Handler;
.source "SelfBrailleClient.java"


# instance fields
.field private synthetic a:Lcom/googlecode/eyesfree/braille/selfbraille/d;


# direct methods
.method private constructor <init>(Lcom/googlecode/eyesfree/braille/selfbraille/d;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/googlecode/eyesfree/braille/selfbraille/f;->a:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/googlecode/eyesfree/braille/selfbraille/d;B)V
    .locals 0

    .prologue
    .line 223
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/braille/selfbraille/f;-><init>(Lcom/googlecode/eyesfree/braille/selfbraille/d;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 228
    monitor-enter p0

    .line 229
    :try_start_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/f;->a:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-static {v0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->c(Lcom/googlecode/eyesfree/braille/selfbraille/d;)I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 230
    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/googlecode/eyesfree/braille/selfbraille/f;->a:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-static {v1}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->c(Lcom/googlecode/eyesfree/braille/selfbraille/d;)I

    move-result v1

    shl-int/2addr v0, v1

    .line 231
    const/4 v1, 0x1

    int-to-long v2, v0

    invoke-virtual {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/braille/selfbraille/f;->sendEmptyMessageDelayed(IJ)Z

    .line 232
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/f;->a:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-static {v0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->d(Lcom/googlecode/eyesfree/braille/selfbraille/d;)I

    .line 234
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 238
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/googlecode/eyesfree/braille/selfbraille/f;->sendEmptyMessage(I)Z

    .line 239
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 243
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 245
    :pswitch_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/f;->a:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-static {v0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->e(Lcom/googlecode/eyesfree/braille/selfbraille/d;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/f;->a:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-static {v0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->f(Lcom/googlecode/eyesfree/braille/selfbraille/d;)Lcom/googlecode/eyesfree/braille/selfbraille/e;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/f;->a:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-static {v0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->g(Lcom/googlecode/eyesfree/braille/selfbraille/d;)V

    :cond_1
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/f;->a:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-static {v0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->h(Lcom/googlecode/eyesfree/braille/selfbraille/d;)V

    goto :goto_0

    .line 248
    :pswitch_1
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/f;->a:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-static {v0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->g(Lcom/googlecode/eyesfree/braille/selfbraille/d;)V

    goto :goto_0

    .line 243
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
