.class final Lcom/googlecode/eyesfree/braille/selfbraille/e;
.super Ljava/lang/Object;
.source "SelfBrailleClient.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private volatile a:Lcom/googlecode/eyesfree/braille/selfbraille/a;

.field private synthetic b:Lcom/googlecode/eyesfree/braille/selfbraille/d;


# direct methods
.method private constructor <init>(Lcom/googlecode/eyesfree/braille/selfbraille/d;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/googlecode/eyesfree/braille/selfbraille/e;->b:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/googlecode/eyesfree/braille/selfbraille/d;B)V
    .locals 0

    .prologue
    .line 193
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/braille/selfbraille/e;-><init>(Lcom/googlecode/eyesfree/braille/selfbraille/d;)V

    return-void
.end method

.method static synthetic a(Lcom/googlecode/eyesfree/braille/selfbraille/e;)Lcom/googlecode/eyesfree/braille/selfbraille/a;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/e;->a:Lcom/googlecode/eyesfree/braille/selfbraille/a;

    return-object v0
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 200
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/e;->b:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-static {v0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->a(Lcom/googlecode/eyesfree/braille/selfbraille/d;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    invoke-static {}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->b()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Service certificate mismatch for %s, dropping connection"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const-string/jumbo v3, "com.googlecode.eyesfree.brailleback"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/e;->b:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-static {v0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->b(Lcom/googlecode/eyesfree/braille/selfbraille/d;)Lcom/googlecode/eyesfree/braille/selfbraille/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/braille/selfbraille/f;->b()V

    .line 211
    :goto_0
    return-void

    .line 207
    :cond_0
    invoke-static {}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->b()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Connected to self braille service"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    invoke-static {p2}, Lcom/googlecode/eyesfree/braille/selfbraille/b;->b(Landroid/os/IBinder;)Lcom/googlecode/eyesfree/braille/selfbraille/a;

    move-result-object v0

    iput-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/e;->a:Lcom/googlecode/eyesfree/braille/selfbraille/a;

    .line 209
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/e;->b:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-static {v0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->b(Lcom/googlecode/eyesfree/braille/selfbraille/d;)Lcom/googlecode/eyesfree/braille/selfbraille/f;

    move-result-object v1

    monitor-enter v1

    .line 210
    :try_start_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/e;->b:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->a(Lcom/googlecode/eyesfree/braille/selfbraille/d;I)I

    .line 211
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 216
    invoke-static {}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->b()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Disconnected from self braille service"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/e;->a:Lcom/googlecode/eyesfree/braille/selfbraille/a;

    .line 219
    iget-object v0, p0, Lcom/googlecode/eyesfree/braille/selfbraille/e;->b:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-static {v0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->b(Lcom/googlecode/eyesfree/braille/selfbraille/d;)Lcom/googlecode/eyesfree/braille/selfbraille/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/braille/selfbraille/f;->a()V

    .line 220
    return-void
.end method
