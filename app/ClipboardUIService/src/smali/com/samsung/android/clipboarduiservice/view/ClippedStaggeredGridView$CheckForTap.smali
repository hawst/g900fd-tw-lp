.class final Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;
.super Ljava/lang/Object;
.source "ClippedStaggeredGridView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "CheckForTap"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;


# direct methods
.method constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)V
    .locals 0

    .prologue
    .line 2653
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x1

    .line 2655
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I
    invoke-static {v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$300(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    .line 2658
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    const/4 v5, 0x4

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I
    invoke-static {v4, v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$302(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;I)I

    .line 2659
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    iget-object v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMotionPosition:I
    invoke-static {v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$2400(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I
    invoke-static {v6}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1600(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v4, v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2660
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->hasFocusable()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2663
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDataChanged:Z
    invoke-static {v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$400(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 2664
    invoke-virtual {v0, v7}, Landroid/view/View;->setSelected(Z)V

    .line 2665
    invoke-virtual {v0, v7}, Landroid/view/View;->setPressed(Z)V

    .line 2668
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v4, v7}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setPressed(Z)V

    .line 2669
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    iget-object v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMotionPosition:I
    invoke-static {v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$2400(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I

    move-result v5

    invoke-virtual {v4, v5, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->positionSelector(ILandroid/view/View;)V

    .line 2670
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->refreshDrawableState()V

    .line 2673
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v3

    .line 2674
    .local v3, "longPressTimeout":I
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->isLongClickable()Z

    move-result v2

    .line 2677
    .local v2, "longClickable":Z
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    iget-object v4, v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_0

    .line 2678
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    iget-object v4, v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2679
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    instance-of v4, v1, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v4, :cond_0

    .line 2680
    if-eqz v2, :cond_3

    .line 2681
    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 2689
    :cond_0
    :goto_0
    if-eqz v2, :cond_4

    .line 2690
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPendingCheckForLongPress:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;
    invoke-static {v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$2500(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;

    move-result-object v4

    if-nez v4, :cond_1

    .line 2691
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    new-instance v5, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;

    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;)V

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPendingCheckForLongPress:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;
    invoke-static {v4, v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$2502(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;)Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;

    .line 2693
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPendingCheckForLongPress:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;
    invoke-static {v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$2500(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;->rememberWindowAttachCount()V

    .line 2694
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    iget-object v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPendingCheckForLongPress:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;
    invoke-static {v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$2500(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;

    move-result-object v5

    int-to-long v6, v3

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2700
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->postInvalidate()V

    .line 2706
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "longClickable":Z
    .end local v3    # "longPressTimeout":I
    :cond_2
    :goto_2
    return-void

    .line 2683
    .restart local v0    # "child":Landroid/view/View;
    .restart local v1    # "d":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "longClickable":Z
    .restart local v3    # "longPressTimeout":I
    :cond_3
    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    goto :goto_0

    .line 2696
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I
    invoke-static {v4, v8}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$302(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;I)I

    goto :goto_1

    .line 2702
    .end local v2    # "longClickable":Z
    .end local v3    # "longPressTimeout":I
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I
    invoke-static {v4, v8}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$302(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;I)I

    goto :goto_2
.end method
