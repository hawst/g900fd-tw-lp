.class Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;
.super Ljava/lang/Object;
.source "ClipboardNormalDialog.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V
    .locals 0

    .prologue
    .line 1187
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 11
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1202
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1203
    const-string v8, "ClipboardServiceEx"

    const-string v9, "clipped dialog. onAnimationEnd START"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1213
    :cond_0
    const/4 v0, 0x0

    .line 1214
    .local v0, "data":Landroid/sec/clipboard/data/file/WrapFileClipData;
    :try_start_0
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->isKnoxTwoEnabled()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1215
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v8

    invoke-virtual {v8}, Landroid/sec/clipboard/ClipboardExManager;->getPersonaId()I

    move-result v3

    .line 1218
    .local v3, "id":I
    if-nez v3, :cond_4

    .line 1219
    new-instance v4, Ljava/io/File;

    const-string v8, "/data/clipboard"

    invoke-direct {v4, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1220
    .local v4, "rootPath":Ljava/io/File;
    new-instance v6, Ljava/io/File;

    const-string v8, "/data/clipboard/shared"

    invoke-direct {v6, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1226
    .local v6, "sharedPath":Ljava/io/File;
    :goto_0
    const-string v8, "ClipboardServiceEx"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onAnimationEnd: rootPath = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ; sharedPath = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1228
    new-instance v5, Landroid/sec/clipboard/data/file/FileManager;

    new-instance v8, Ljava/io/File;

    const-string v9, "clips.info"

    invoke-direct {v8, v4, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v5, v8, v3}, Landroid/sec/clipboard/data/file/FileManager;-><init>(Ljava/io/File;I)V

    .line 1229
    .local v5, "rootfm":Landroid/sec/clipboard/data/file/FileManager;
    if-eqz v5, :cond_1

    .line 1230
    const-string v8, "ClipboardServiceEx"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onAnimationEnd: current user id = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ; mSelectedIndex = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I
    invoke-static {v10}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ; rootfm.size() = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Landroid/sec/clipboard/data/file/FileManager;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1233
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)I

    move-result v8

    invoke-virtual {v5}, Landroid/sec/clipboard/data/file/FileManager;->size()I

    move-result v9

    if-lt v8, v9, :cond_1

    .line 1234
    new-instance v7, Landroid/sec/clipboard/data/file/FileManager;

    new-instance v8, Ljava/io/File;

    const-string v9, "shared_clips.info"

    invoke-direct {v8, v6, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v7, v8, v3}, Landroid/sec/clipboard/data/file/FileManager;-><init>(Ljava/io/File;I)V

    .line 1236
    .local v7, "sharedfm":Landroid/sec/clipboard/data/file/FileManager;
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)I

    move-result v8

    invoke-virtual {v5}, Landroid/sec/clipboard/data/file/FileManager;->size()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-virtual {v7, v8}, Landroid/sec/clipboard/data/file/FileManager;->getWrap(I)Landroid/sec/clipboard/data/file/WrapFileClipData;

    move-result-object v0

    .line 1237
    const-string v8, "ClipboardServiceEx"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onAnimationEnd: data = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1242
    .end local v3    # "id":I
    .end local v4    # "rootPath":Ljava/io/File;
    .end local v5    # "rootfm":Landroid/sec/clipboard/data/file/FileManager;
    .end local v6    # "sharedPath":Ljava/io/File;
    .end local v7    # "sharedfm":Landroid/sec/clipboard/data/file/FileManager;
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataUiEvent:Landroid/sec/clipboard/IClipboardDataUiEvent;
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$400(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/IClipboardDataUiEvent;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I
    invoke-static {v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)I

    move-result v9

    invoke-interface {v8, v9}, Landroid/sec/clipboard/IClipboardDataUiEvent;->removeItem(I)V

    .line 1243
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I
    invoke-static {v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    invoke-virtual {v8}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->recycle()V

    .line 1244
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I
    invoke-static {v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1250
    sget v8, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CHILD_COUNT:I

    add-int/lit8 v8, v8, -0x1

    sput v8, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CHILD_COUNT:I

    .line 1251
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    const/4 v9, 0x0

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsDeletingItem:Z
    invoke-static {v8, v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1902(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z

    .line 1255
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->isKnoxTwoEnabled()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1256
    if-eqz v0, :cond_2

    .line 1257
    const-string v8, "ClipboardServiceEx"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onAnimationEnd: Sending clip removed broadcast! clip = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Landroid/sec/clipboard/data/file/WrapFileClipData;->getFile()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1260
    new-instance v2, Landroid/content/Intent;

    const-string v8, "com.samsung.knox.clipboard.clipremoved"

    invoke-direct {v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1261
    .local v2, "i":Landroid/content/Intent;
    const-string v8, "currentUserId"

    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v9

    invoke-virtual {v9}, Landroid/sec/clipboard/ClipboardExManager;->getPersonaId()I

    move-result v9

    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1262
    const-string v8, "RemovedClipFilePath"

    invoke-virtual {v0}, Landroid/sec/clipboard/data/file/WrapFileClipData;->getFile()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1263
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1272
    .end local v2    # "i":Landroid/content/Intent;
    :cond_2
    :goto_1
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v8

    invoke-virtual {v8}, Landroid/sec/clipboard/ClipboardExManager;->getDataListSize()I

    move-result v8

    if-nez v8, :cond_3

    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->dismiss()V

    .line 1274
    :cond_3
    return-void

    .line 1222
    .restart local v3    # "id":I
    :cond_4
    :try_start_1
    new-instance v4, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/data/clipboard"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1223
    .restart local v4    # "rootPath":Ljava/io/File;
    new-instance v6, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/data/clipboard"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/shared"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .restart local v6    # "sharedPath":Ljava/io/File;
    goto/16 :goto_0

    .line 1267
    .end local v3    # "id":I
    .end local v4    # "rootPath":Ljava/io/File;
    .end local v6    # "sharedPath":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 1269
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1196
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1197
    const-string v0, "ClipboardServiceEx"

    const-string v1, "clipped dialog. onAnimationRepeat START"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1199
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1190
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1191
    const-string v0, "ClipboardServiceEx"

    const-string v1, "clipped dialog. onAnimationStart START"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1193
    :cond_0
    return-void
.end method
