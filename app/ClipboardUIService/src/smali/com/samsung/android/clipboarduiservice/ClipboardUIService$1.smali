.class Lcom/samsung/android/clipboarduiservice/ClipboardUIService$1;
.super Landroid/os/Handler;
.source "ClipboardUIService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/ClipboardUIService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;


# direct methods
.method constructor <init>(Lcom/samsung/android/clipboarduiservice/ClipboardUIService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 210
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService$1;->this$0:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 213
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 231
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 215
    :pswitch_1
    # getter for: Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216
    const-string v0, "ClipboardUIService"

    const-string v1, "MSG_SHOW"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService$1;->this$0:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->createDialog(Z)V

    goto :goto_0

    .line 225
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService$1;->this$0:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    # getter for: Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->access$100(Lcom/samsung/android/clipboarduiservice/ClipboardUIService;)Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService$1;->this$0:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    # getter for: Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->access$100(Lcom/samsung/android/clipboarduiservice/ClipboardUIService;)Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->dismiss()V

    .line 227
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService$1;->this$0:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    invoke-static {v0, v1}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->access$102(Lcom/samsung/android/clipboarduiservice/ClipboardUIService;Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    goto :goto_0

    .line 213
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
