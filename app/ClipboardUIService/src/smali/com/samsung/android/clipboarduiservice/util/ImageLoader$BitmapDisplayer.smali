.class Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;
.super Ljava/lang/Object;
.source "ImageLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/util/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BitmapDisplayer"
.end annotation


# instance fields
.field drawable:Landroid/graphics/drawable/BitmapDrawable;

.field imageInfo:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/util/ImageLoader;


# direct methods
.method public constructor <init>(Lcom/samsung/android/clipboarduiservice/util/ImageLoader;Landroid/graphics/drawable/BitmapDrawable;Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;)V
    .locals 0
    .param p2, "d"    # Landroid/graphics/drawable/BitmapDrawable;
    .param p3, "i"    # Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;->this$0:Lcom/samsung/android/clipboarduiservice/util/ImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    iput-object p2, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;->drawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 167
    iput-object p3, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    .line 168
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 171
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    iget-object v1, v1, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->mImageView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 172
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;->this$0:Lcom/samsung/android/clipboarduiservice/util/ImageLoader;

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    invoke-virtual {v1, v2}, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->imageViewReused(Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;->drawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_2

    .line 177
    const-string v1, "TEST"

    const-string v2, "Start set Bitmap in ImageView"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    iget-object v1, v1, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->mImageView:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 180
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    iget-object v1, v1, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->mImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;->drawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 181
    const-string v1, "TEST"

    const-string v2, "Finish set Bitmap in ImageView"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;->this$0:Lcom/samsung/android/clipboarduiservice/util/ImageLoader;

    iget-object v1, v1, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->mContext:Landroid/content/Context;

    const v2, 0x7f040003

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 183
    .local v0, "ani":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    iget-object v1, v1, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 186
    .end local v0    # "ani":Landroid/view/animation/Animation;
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    iget-object v1, v1, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->mImageView:Landroid/widget/ImageView;

    invoke-static {v3, v3, v3, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0
.end method
