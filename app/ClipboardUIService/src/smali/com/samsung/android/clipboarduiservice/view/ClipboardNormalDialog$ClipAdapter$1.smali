.class Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter$1;
.super Ljava/lang/Object;
.source "ClipboardNormalDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;


# direct methods
.method constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;)V
    .locals 0

    .prologue
    .line 2023
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 2027
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->pointToPosition(II)I

    move-result v1

    .line 2029
    .local v1, "position":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataUiEvent:Landroid/sec/clipboard/IClipboardDataUiEvent;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$400(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/IClipboardDataUiEvent;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;

    iget-object v3, v3, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardUIService:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1600(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->getClipboardDataPasteEvent()Landroid/sec/clipboard/IClipboardDataPasteEvent;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Landroid/sec/clipboard/IClipboardDataUiEvent;->selectItem(ILandroid/sec/clipboard/IClipboardDataPasteEvent;)V

    .line 2030
    const-string v2, "ClipboardNormalDialog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Clipboard selectItem position : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2034
    :goto_0
    return-void

    .line 2031
    :catch_0
    move-exception v0

    .line 2032
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
