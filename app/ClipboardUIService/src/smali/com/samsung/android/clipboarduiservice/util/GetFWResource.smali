.class public Lcom/samsung/android/clipboarduiservice/util/GetFWResource;
.super Ljava/lang/Object;
.source "GetFWResource.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultTheme()I
    .locals 1

    .prologue
    .line 8
    const v0, 0x102004a

    return v0
.end method

.method public static getDefaultThemeLight()I
    .locals 1

    .prologue
    .line 11
    const v0, 0x103012b

    return v0
.end method

.method public static getTWClipboardTheme()I
    .locals 2

    .prologue
    .line 14
    const-string v0, "ro.build.scafe.cream"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "black"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15
    const v0, 0x10304d5

    .line 17
    :goto_0
    return v0

    :cond_0
    const v0, 0x103012b

    goto :goto_0
.end method
