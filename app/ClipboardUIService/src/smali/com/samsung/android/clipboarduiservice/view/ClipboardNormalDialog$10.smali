.class Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$10;
.super Ljava/lang/Object;
.source "ClipboardNormalDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->postDrawerToUpdateClearLayout(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

.field final synthetic val$visibility:I


# direct methods
.method constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)V
    .locals 0

    .prologue
    .line 1548
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$10;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iput p2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$10;->val$visibility:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1550
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$10;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearButton:Landroid/widget/Button;

    iget v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$10;->val$visibility:I

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1551
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$10;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipDrawer:Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1552
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$10;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipDrawer:Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->getHandle()Landroid/view/View;

    move-result-object v1

    .line 1553
    .local v1, "handle":Landroid/view/View;
    if-eqz v1, :cond_0

    instance-of v2, v1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    move-object v2, v1

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_0

    .line 1554
    check-cast v1, Landroid/view/ViewGroup;

    .end local v1    # "handle":Landroid/view/View;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1555
    .local v0, "divider":Landroid/view/View;
    if-eqz v0, :cond_0

    instance-of v2, v0, Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    .line 1556
    iget v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$10;->val$visibility:I

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1560
    .end local v0    # "divider":Landroid/view/View;
    :cond_0
    return-void
.end method
