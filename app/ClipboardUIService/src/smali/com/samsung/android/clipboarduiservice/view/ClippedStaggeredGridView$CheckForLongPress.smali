.class Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;
.super Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$WindowRunnnable;
.source "ClippedStaggeredGridView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckForLongPress"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)V
    .locals 1

    .prologue
    .line 2710
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$WindowRunnnable;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;
    .param p2, "x1"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;

    .prologue
    .line 2710
    invoke-direct {p0, p1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2712
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMotionPosition:I
    invoke-static {v6}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$2400(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I

    move-result v5

    .line 2713
    .local v5, "motionPosition":I
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    iget-object v7, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I
    invoke-static {v7}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1600(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I

    move-result v7

    sub-int v7, v5, v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2714
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 2715
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMotionPosition:I
    invoke-static {v6}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$2400(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I

    move-result v4

    .line 2716
    .local v4, "longPressPosition":I
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;
    invoke-static {v6}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$800(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Landroid/widget/ListAdapter;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMotionPosition:I
    invoke-static {v7}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$2400(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I

    move-result v7

    invoke-interface {v6, v7}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    .line 2719
    .local v2, "longPressId":J
    const/4 v1, 0x0

    .line 2720
    .local v1, "handled":Z
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDataChanged:Z
    invoke-static {v6}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$400(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 2721
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v6, v0, v4, v2, v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->performLongPress(Landroid/view/View;IJ)Z

    move-result v1

    .line 2723
    :cond_0
    if-eqz v1, :cond_2

    .line 2724
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    const/4 v7, 0x6

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I
    invoke-static {v6, v7}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$302(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;I)I

    .line 2725
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v6, v8}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setPressed(Z)V

    .line 2726
    invoke-virtual {v0, v8}, Landroid/view/View;->setPressed(Z)V

    .line 2731
    .end local v1    # "handled":Z
    .end local v2    # "longPressId":J
    .end local v4    # "longPressPosition":I
    :cond_1
    :goto_0
    return-void

    .line 2728
    .restart local v1    # "handled":Z
    .restart local v2    # "longPressId":J
    .restart local v4    # "longPressPosition":I
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    const/4 v7, 0x5

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I
    invoke-static {v6, v7}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$302(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;I)I

    goto :goto_0
.end method
