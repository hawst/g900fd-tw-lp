.class public Lcom/samsung/android/clipboarduiservice/ClipboardUIService;
.super Landroid/sec/clipboard/IClipboardUIManager$Stub;
.source "ClipboardUIService.java"


# static fields
.field private static final DEBUG:Z

.field private static final MSG_DISMISS:I = 0x2

.field private static final MSG_SHOW:I = 0x1

.field private static final MSG_UPDATE_DIALOG:I = 0x3

.field private static final SURVEY_LOG:Z

.field private static final TAG:Ljava/lang/String; = "ClipboardUIService"

.field public static final TYPE_EDITOR:I = 0x1

.field public static final TYPE_TEMPLATE:I = 0x2

.field private static mForceHideFloatingIcon:Z

.field private static mInstance:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

.field private static mScrapMode:I


# instance fields
.field private mAniRect:Landroid/graphics/Rect;

.field private mClientID:I

.field private mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

.field private mContext:Landroid/content/Context;

.field private mCurrentFormatType:I

.field private mCurrentPasteEvent:Landroid/sec/clipboard/IClipboardDataPasteEvent;

.field private final mHandler:Landroid/os/Handler;

.field private mNeedAnimation:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 30
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->DEBUG:Z

    .line 32
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->SURVEY_LOG:Z

    .line 59
    sput v2, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mScrapMode:I

    .line 61
    sput-boolean v2, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mForceHideFloatingIcon:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-direct {p0}, Landroid/sec/clipboard/IClipboardUIManager$Stub;-><init>()V

    .line 42
    iput v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClientID:I

    .line 51
    iput v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mCurrentFormatType:I

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mCurrentPasteEvent:Landroid/sec/clipboard/IClipboardDataPasteEvent;

    .line 210
    new-instance v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService$1;-><init>(Lcom/samsung/android/clipboarduiservice/ClipboardUIService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mHandler:Landroid/os/Handler;

    .line 73
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mContext:Landroid/content/Context;

    .line 74
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->sendMessage(I)V

    .line 75
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mAniRect:Landroid/graphics/Rect;

    .line 76
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 26
    sget-boolean v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->DEBUG:Z

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/clipboarduiservice/ClipboardUIService;)Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/clipboarduiservice/ClipboardUIService;Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/ClipboardUIService;
    .param p1, "x1"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    return-object p1
.end method

.method public static getInstance()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 198
    sget-object v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mInstance:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    if-nez v0, :cond_0

    .line 200
    const/4 v0, 0x0

    .line 202
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mInstance:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/os/IBinder;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 192
    sget-object v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mInstance:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    if-nez v0, :cond_0

    .line 193
    new-instance v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    invoke-direct {v0, p0}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mInstance:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    .line 194
    :cond_0
    sget-object v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mInstance:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    return-object v0
.end method

.method private sendMessage(I)V
    .locals 1
    .param p1, "what"    # I

    .prologue
    .line 235
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 241
    :goto_0
    return-void

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 240
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mInstance:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    return-object v0
.end method

.method public createDialog(Z)V
    .locals 6
    .param p1, "force"    # Z

    .prologue
    const/4 v5, 0x1

    .line 246
    const-string v3, "ClipboardUIService"

    const-string v4, "Create Dialog : CLIPBOARD_TYPE_NORMAL"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    if-ne p1, v5, :cond_0

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    if-eqz v3, :cond_0

    .line 249
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v3}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->dismiss()V

    .line 250
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .line 252
    :cond_0
    const/4 v3, 0x0

    sput v3, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mScrapMode:I

    .line 256
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v3}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 284
    :goto_0
    return-void

    .line 259
    :cond_1
    new-instance v3, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;-><init>(Landroid/content/Context;Lcom/samsung/android/clipboarduiservice/ClipboardUIService;)V

    iput-object v3, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .line 262
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v3}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 263
    .local v2, "window":Landroid/view/Window;
    const v3, 0x10102

    invoke-virtual {v2, v3}, Landroid/view/Window;->clearFlags(I)V

    .line 266
    const v3, 0x1040300

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 274
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 276
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 277
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 278
    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 280
    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 281
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v3, v5}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->requestWindowFeature(I)Z

    .line 282
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v3}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->show()V

    goto :goto_0
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 287
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->sendMessage(I)V

    .line 288
    return-void
.end method

.method public dismiss(IZ)V
    .locals 3
    .param p1, "clientId"    # I
    .param p2, "immediate"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 99
    sget-boolean v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "ClipboardUIService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Dismiss WritingBuddy dialog. clientID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :cond_0
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->sendMessage(I)V

    .line 103
    return-void
.end method

.method public getAnimationRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mAniRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getClipboardDataPasteEvent()Landroid/sec/clipboard/IClipboardDataPasteEvent;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mCurrentPasteEvent:Landroid/sec/clipboard/IClipboardDataPasteEvent;

    return-object v0
.end method

.method public getClipboardFormatType()I
    .locals 1

    .prologue
    .line 184
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mCurrentFormatType:I

    return v0
.end method

.method public getClipboardUIMode()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 160
    sget v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mScrapMode:I

    return v0
.end method

.method public getIconXpos()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 149
    const/4 v0, 0x0

    return v0
.end method

.method public getIconYpos()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 154
    const/4 v0, 0x0

    return v0
.end method

.method public getNeedToDrawAnimation()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mNeedAnimation:Z

    return v0
.end method

.method public hideFloatingIconForScrap()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 135
    return-void
.end method

.method public insertLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "feature"    # Ljava/lang/String;
    .param p3, "extra"    # Ljava/lang/String;
    .param p4, "value"    # J

    .prologue
    .line 302
    sget-boolean v2, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->SURVEY_LOG:Z

    if-eqz v2, :cond_0

    .line 303
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 304
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v2, "app_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    const-string v2, "feature"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 309
    .local v0, "broadcastIntent":Landroid/content/Intent;
    const-string v2, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 310
    const-string v2, "data"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 312
    const-string v2, "com.samsung.android.providers.context"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 314
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 316
    .end local v0    # "broadcastIntent":Landroid/content/Intent;
    .end local v1    # "cv":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method

.method public isShowing()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->isShowing()Z

    move-result v0

    .line 121
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowingClipboard()Z
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    if-eqz v0, :cond_0

    .line 127
    const/4 v0, 0x1

    .line 129
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 292
    sget-boolean v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 293
    const-string v0, "ClipboardUIService"

    const-string v1, "onConfigurationChanged()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v0, p1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 299
    :cond_1
    return-void
.end method

.method public sendCropRectforAni(IIIIZ)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I
    .param p5, "showAni"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mAniRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 168
    iput-boolean p5, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mNeedAnimation:Z

    .line 169
    return-void
.end method

.method public setClipboardUIMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 207
    sput p1, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mScrapMode:I

    .line 208
    return-void
.end method

.method public setNeedToDrawAnimation(Z)V
    .locals 0
    .param p1, "done"    # Z

    .prologue
    .line 180
    iput-boolean p1, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mNeedAnimation:Z

    .line 181
    return-void
.end method

.method public setPasteTargetViewType(ILandroid/sec/clipboard/IClipboardDataPasteEvent;)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "clPasteEvent"    # Landroid/sec/clipboard/IClipboardDataPasteEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 107
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mCurrentFormatType:I

    if-eq v0, p1, :cond_1

    .line 108
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mClipboardDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->setPasteTargetViewInfo(ILandroid/sec/clipboard/IClipboardDataPasteEvent;)V

    .line 111
    :cond_0
    iput p1, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mCurrentFormatType:I

    .line 113
    :cond_1
    iput-object p2, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mCurrentPasteEvent:Landroid/sec/clipboard/IClipboardDataPasteEvent;

    .line 114
    return-void
.end method

.method public show(ILandroid/os/IBinder;)V
    .locals 2
    .param p1, "clientId"    # I
    .param p2, "client"    # Landroid/os/IBinder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 91
    sget-boolean v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 92
    const-string v0, "ClipboardUIService"

    const-string v1, "Show ClipboardUIService"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->sendMessage(I)V

    .line 95
    return-void
.end method

.method public showFloatingIconForScrap()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 139
    return-void
.end method

.method public showFloatingIconForScrap(Z)V
    .locals 0
    .param p1, "dialogCall"    # Z

    .prologue
    .line 142
    return-void
.end method

.method public startClipboardUIService()V
    .locals 5

    .prologue
    .line 319
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 320
    .local v1, "intent":Landroid/content/Intent;
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.samsung.android.clipboarduiservice"

    const-string v4, "com.samsung.android.clipboarduiservice.ClipboardUIServiceStarter"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 322
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 326
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 323
    :catch_0
    move-exception v0

    .line 324
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "ClipboardUIService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Starting clipboarduiservice service failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateFloatingIconForScrap()V
    .locals 0

    .prologue
    .line 145
    return-void
.end method
