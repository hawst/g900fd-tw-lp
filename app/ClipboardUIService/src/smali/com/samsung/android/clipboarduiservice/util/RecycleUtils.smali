.class public Lcom/samsung/android/clipboarduiservice/util/RecycleUtils;
.super Ljava/lang/Object;
.source "RecycleUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static recursiveBitmapRecycle(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p0, "recycleList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/ref/WeakReference<Landroid/graphics/Bitmap;>;>;"
    return-void
.end method

.method public static recursiveRecycle(Landroid/view/View;)V
    .locals 5
    .param p0, "root"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 15
    if-nez p0, :cond_0

    .line 33
    :goto_0
    return-void

    .line 18
    :cond_0
    invoke-virtual {p0, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 19
    instance-of v3, p0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_2

    move-object v1, p0

    .line 20
    check-cast v1, Landroid/view/ViewGroup;

    .line 21
    .local v1, "group":Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 22
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_1

    .line 23
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/util/RecycleUtils;->recursiveRecycle(Landroid/view/View;)V

    .line 22
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 25
    :cond_1
    instance-of v3, p0, Landroid/widget/AdapterView;

    if-nez v3, :cond_2

    .line 26
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 29
    .end local v0    # "count":I
    .end local v1    # "group":Landroid/view/ViewGroup;
    .end local v2    # "i":I
    :cond_2
    instance-of v3, p0, Landroid/widget/ImageView;

    if-eqz v3, :cond_3

    .line 30
    check-cast p0, Landroid/widget/ImageView;

    .end local p0    # "root":Landroid/view/View;
    invoke-virtual {p0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 32
    :cond_3
    const/4 p0, 0x0

    .line 33
    .restart local p0    # "root":Landroid/view/View;
    goto :goto_0
.end method

.method public static recursiveRecycle(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p0, "recycleList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/ref/WeakReference<Landroid/view/View;>;>;"
    return-void
.end method
