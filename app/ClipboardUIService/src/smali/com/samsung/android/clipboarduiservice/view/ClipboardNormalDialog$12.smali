.class Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$12;
.super Ljava/lang/Object;
.source "ClipboardNormalDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->dismiss()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V
    .locals 0

    .prologue
    .line 1722
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$12;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1724
    new-instance v0, Landroid/content/Intent;

    const-string v1, "dismissClipboardDialog"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1725
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$12;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1726
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1727
    const-string v1, "ClipboardServiceEx"

    const-string v2, "dismissClipboardDialog intent will be sent"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1729
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$12;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/os/UserHandle;

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$12;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/sec/clipboard/ClipboardExManager;->getPersonaId()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 1731
    :cond_1
    return-void
.end method
