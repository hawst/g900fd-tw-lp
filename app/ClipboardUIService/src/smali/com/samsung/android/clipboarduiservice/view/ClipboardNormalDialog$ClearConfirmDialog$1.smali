.class Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;
.super Ljava/lang/Object;
.source "ClipboardNormalDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 19
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 354
    const/16 v16, -0x1

    move/from16 v0, p2

    move/from16 v1, v16

    if-ne v0, v1, :cond_e

    .line 355
    const-string v16, "ClipboardNormalDialog"

    const-string v17, "pressed OK"

    invoke-static/range {v16 .. v17}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v16

    if-nez v16, :cond_1

    .line 518
    :cond_0
    :goto_0
    return-void

    .line 361
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/sec/clipboard/ClipboardExManager;->getDataListSize()I

    move-result v4

    .line 362
    .local v4, "count":I
    const/4 v11, 0x0

    .line 364
    .local v11, "protectCount":I
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v16

    if-eqz v16, :cond_2

    .line 365
    const-string v16, "ClipboardNormalDialog"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "clear button touched... count :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 374
    .local v2, "clearedClipsArr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/sec/clipboard/ClipboardExManager;->getPersonaId()I

    move-result v10

    .line 377
    .local v10, "id":I
    if-nez v10, :cond_5

    .line 378
    new-instance v12, Ljava/io/File;

    const-string v16, "/data/clipboard"

    move-object/from16 v0, v16

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 379
    .local v12, "rootPath":Ljava/io/File;
    new-instance v14, Ljava/io/File;

    const-string v16, "/data/clipboard/shared"

    move-object/from16 v0, v16

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 386
    .local v14, "sharedPath":Ljava/io/File;
    :goto_1
    const-string v16, "ClipboardNormalDialog"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "onClear: Current User = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " ; rootPath = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " ; sharedPath = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_2
    if-ge v9, v4, :cond_a

    .line 390
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v16

    if-eqz v16, :cond_3

    .line 391
    const-string v16, "ClipboardNormalDialog"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "mCbm.getDataListSize() :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/sec/clipboard/ClipboardExManager;->getDataListSize()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    # invokes: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->isCheckProtectedItem(I)Z
    invoke-static {v0, v11}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$300(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 394
    add-int/lit8 v11, v11, 0x1

    .line 389
    :cond_4
    :goto_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 381
    .end local v9    # "i":I
    .end local v12    # "rootPath":Ljava/io/File;
    .end local v14    # "sharedPath":Ljava/io/File;
    :cond_5
    new-instance v12, Ljava/io/File;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "/data/clipboard"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 383
    .restart local v12    # "rootPath":Ljava/io/File;
    new-instance v14, Ljava/io/File;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "/data/clipboard"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "/shared"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v14    # "sharedPath":Ljava/io/File;
    goto/16 :goto_1

    .line 397
    .restart local v9    # "i":I
    :cond_6
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 398
    .local v6, "deleteItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 406
    const/4 v5, 0x0

    .line 407
    .local v5, "data":Landroid/sec/clipboard/data/file/WrapFileClipData;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->isKnoxTwoEnabled()Z

    move-result v16

    if-eqz v16, :cond_8

    .line 408
    new-instance v13, Landroid/sec/clipboard/data/file/FileManager;

    new-instance v16, Ljava/io/File;

    const-string v17, "clips.info"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v0, v12, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-direct {v13, v0, v10}, Landroid/sec/clipboard/data/file/FileManager;-><init>(Ljava/io/File;I)V

    .line 410
    .local v13, "rootfm":Landroid/sec/clipboard/data/file/FileManager;
    const-string v16, "ClipboardNormalDialog"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "onClear: protectCount = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " ; rootfm.size() = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v13}, Landroid/sec/clipboard/data/file/FileManager;->size()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    if-eqz v13, :cond_8

    invoke-virtual {v13}, Landroid/sec/clipboard/data/file/FileManager;->size()I

    move-result v16

    move/from16 v0, v16

    if-lt v11, v0, :cond_8

    .line 414
    new-instance v15, Landroid/sec/clipboard/data/file/FileManager;

    new-instance v16, Ljava/io/File;

    const-string v17, "shared_clips.info"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v0, v14, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-direct {v15, v0, v10}, Landroid/sec/clipboard/data/file/FileManager;-><init>(Ljava/io/File;I)V

    .line 416
    .local v15, "sharedfm":Landroid/sec/clipboard/data/file/FileManager;
    if-eqz v15, :cond_7

    .line 417
    invoke-virtual {v13}, Landroid/sec/clipboard/data/file/FileManager;->size()I

    move-result v16

    sub-int v16, v11, v16

    invoke-virtual/range {v15 .. v16}, Landroid/sec/clipboard/data/file/FileManager;->getWrap(I)Landroid/sec/clipboard/data/file/WrapFileClipData;

    move-result-object v5

    .line 420
    :cond_7
    const-string v16, "ClipboardNormalDialog"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "onClear: data = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    .end local v13    # "rootfm":Landroid/sec/clipboard/data/file/FileManager;
    .end local v15    # "sharedfm":Landroid/sec/clipboard/data/file/FileManager;
    :cond_8
    move v8, v11

    .line 448
    .local v8, "final_protectCount":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataUiEvent:Landroid/sec/clipboard/IClipboardDataUiEvent;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$400(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/IClipboardDataUiEvent;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v0, v8}, Landroid/sec/clipboard/IClipboardDataUiEvent;->removeItem(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 454
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    if-eqz v16, :cond_9

    .line 455
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->recycle()V

    .line 457
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 463
    sget v16, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CHILD_COUNT:I

    add-int/lit8 v16, v16, -0x1

    sput v16, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CHILD_COUNT:I

    .line 466
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->isKnoxTwoEnabled()Z

    move-result v16

    if-eqz v16, :cond_4

    .line 467
    if-eqz v5, :cond_4

    if-eqz v2, :cond_4

    .line 468
    const-string v16, "ClipboardNormalDialog"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "onClear: A shared clip is deleted! data = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v5}, Landroid/sec/clipboard/data/file/WrapFileClipData;->getFile()Ljava/io/File;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    invoke-virtual {v5}, Landroid/sec/clipboard/data/file/WrapFileClipData;->getFile()Ljava/io/File;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 449
    :catch_0
    move-exception v7

    .line 451
    .local v7, "e":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_4

    .line 483
    .end local v5    # "data":Landroid/sec/clipboard/data/file/WrapFileClipData;
    .end local v6    # "deleteItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v7    # "e":Landroid/os/RemoteException;
    .end local v8    # "final_protectCount":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->isKnoxTwoEnabled()Z

    move-result v16

    if-eqz v16, :cond_b

    .line 484
    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v16

    if-lez v16, :cond_b

    .line 485
    const-string v16, "ClipboardNormalDialog"

    const-string v17, "onClear: Sending ClipsCleared broadcast"

    invoke-static/range {v16 .. v17}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    new-instance v9, Landroid/content/Intent;

    .end local v9    # "i":I
    const-string v16, "com.samsung.knox.clipboard.clipscleared"

    move-object/from16 v0, v16

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 488
    .local v9, "i":Landroid/content/Intent;
    const-string v16, "currentUserId"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/sec/clipboard/ClipboardExManager;->getPersonaId()I

    move-result v17

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 489
    const-string v16, "ClearedClipsArray"

    move-object/from16 v0, v16

    invoke-virtual {v9, v0, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->mContext:Landroid/content/Context;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->access$600(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;)Landroid/content/Context;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 498
    .end local v9    # "i":Landroid/content/Intent;
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsDarkTheme:Z
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$700(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z

    move-result v16

    if-eqz v16, :cond_c

    .line 499
    new-instance v3, Landroid/view/ContextThemeWrapper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->mContext:Landroid/content/Context;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->access$600(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;)Landroid/content/Context;

    move-result-object v16

    invoke-static {}, Lcom/samsung/android/clipboarduiservice/util/GetFWResource;->getDefaultTheme()I

    move-result v17

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v3, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 503
    .local v3, "context":Landroid/content/Context;
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsSupportLockFunc:Z
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z

    move-result v16

    if-eqz v16, :cond_d

    .line 504
    const v16, 0x7f080011

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v3, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/widget/Toast;->show()V

    .line 510
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/sec/clipboard/ClipboardExManager;->getDataListSize()I

    move-result v16

    if-nez v16, :cond_0

    .line 511
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    goto/16 :goto_0

    .line 501
    .end local v3    # "context":Landroid/content/Context;
    :cond_c
    new-instance v3, Landroid/view/ContextThemeWrapper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->mContext:Landroid/content/Context;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->access$600(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;)Landroid/content/Context;

    move-result-object v16

    invoke-static {}, Lcom/samsung/android/clipboarduiservice/util/GetFWResource;->getDefaultThemeLight()I

    move-result v17

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v3, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .restart local v3    # "context":Landroid/content/Context;
    goto :goto_5

    .line 506
    :cond_d
    const v16, 0x7f080010

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v3, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/widget/Toast;->show()V

    goto :goto_6

    .line 513
    .end local v2    # "clearedClipsArr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "context":Landroid/content/Context;
    .end local v4    # "count":I
    .end local v10    # "id":I
    .end local v11    # "protectCount":I
    .end local v12    # "rootPath":Ljava/io/File;
    .end local v14    # "sharedPath":Ljava/io/File;
    :cond_e
    const/16 v16, -0x2

    move/from16 v0, p2

    move/from16 v1, v16

    if-ne v0, v1, :cond_f

    .line 514
    const-string v16, "ClipboardNormalDialog"

    const-string v17, "pressed CANCEL"

    invoke-static/range {v16 .. v17}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 516
    :cond_f
    const-string v16, "ClipboardNormalDialog"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "pressed invalid button. arg1 :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
