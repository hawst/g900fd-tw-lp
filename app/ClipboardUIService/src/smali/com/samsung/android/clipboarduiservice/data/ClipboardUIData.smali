.class public Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
.super Ljava/lang/Object;
.source "ClipboardUIData.java"


# instance fields
.field final LIMIT_FILE_SIZE:I

.field private bitmap:Landroid/graphics/Bitmap;

.field private clipData:Landroid/content/ClipData;

.field private context:Landroid/content/Context;

.field private drawable:Landroid/graphics/drawable/BitmapDrawable;

.field private htmlText:Ljava/lang/String;

.field private image_height:I

.field private image_width:I

.field private intentValue:Landroid/content/Intent;

.field private mColor:I

.field private metaText:Ljava/lang/String;

.field private multiUri:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private ocrText:Ljava/lang/String;

.field private path:Ljava/lang/String;

.field private protect:Z

.field private strokeText:Ljava/lang/String;

.field private text:Ljava/lang/String;

.field private type:I

.field private uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/ClipData;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "clip"    # Landroid/content/ClipData;
    .param p3, "type"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->context:Landroid/content/Context;

    .line 19
    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->clipData:Landroid/content/ClipData;

    .line 20
    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->bitmap:Landroid/graphics/Bitmap;

    .line 21
    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->drawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 22
    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->path:Ljava/lang/String;

    .line 23
    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->text:Ljava/lang/String;

    .line 24
    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->htmlText:Ljava/lang/String;

    .line 25
    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->ocrText:Ljava/lang/String;

    .line 26
    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->metaText:Ljava/lang/String;

    .line 27
    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->strokeText:Ljava/lang/String;

    .line 28
    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->intentValue:Landroid/content/Intent;

    .line 29
    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->uri:Landroid/net/Uri;

    .line 30
    iput v3, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->type:I

    .line 32
    iput v3, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->mColor:I

    .line 34
    iput-boolean v3, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->protect:Z

    .line 36
    iput v3, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->image_width:I

    .line 37
    iput v3, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->image_height:I

    .line 38
    const/4 v4, 0x6

    iput v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->LIMIT_FILE_SIZE:I

    .line 40
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->context:Landroid/content/Context;

    .line 42
    const/4 v1, 0x0

    .line 43
    .local v1, "length":I
    packed-switch p3, :pswitch_data_0

    .line 122
    :cond_0
    :goto_0
    invoke-virtual {p0, p3}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setType(I)V

    .line 123
    invoke-virtual {p0, p2}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setClipData(Landroid/content/ClipData;)V

    .line 124
    return-void

    .line 45
    :pswitch_0
    invoke-virtual {p2, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setText(Ljava/lang/String;)V

    .line 46
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getText()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    .line 47
    :goto_1
    if-lez v1, :cond_2

    .line 48
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    add-int/2addr v3, v1

    rem-int/lit8 v3, v3, 0x3

    invoke-virtual {p0, v3}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setColor(I)V

    goto :goto_0

    :cond_1
    move v1, v3

    .line 46
    goto :goto_1

    .line 50
    :cond_2
    invoke-virtual {p0, v3}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setColor(I)V

    goto :goto_0

    .line 55
    :pswitch_1
    invoke-virtual {p2, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setPath(Ljava/lang/String;)V

    .line 56
    invoke-virtual {p2, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setUri(Landroid/net/Uri;)V

    .line 57
    invoke-virtual {p0, v3}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setColor(I)V

    goto :goto_0

    .line 61
    :pswitch_2
    invoke-virtual {p2, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setText(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p2, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getHtmlText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setHtmlText(Ljava/lang/String;)V

    .line 63
    invoke-virtual {p2, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 64
    invoke-virtual {p2, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setUri(Landroid/net/Uri;)V

    .line 65
    invoke-virtual {p2, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setPath(Ljava/lang/String;)V

    .line 68
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getHtmlText()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getHtmlText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    .line 69
    :goto_2
    if-lez v1, :cond_5

    .line 70
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getHtmlText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    add-int/2addr v3, v1

    rem-int/lit8 v3, v3, 0x3

    invoke-virtual {p0, v3}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setColor(I)V

    goto/16 :goto_0

    :cond_4
    move v1, v3

    .line 68
    goto :goto_2

    .line 72
    :cond_5
    invoke-virtual {p0, v3}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setColor(I)V

    goto/16 :goto_0

    .line 76
    :pswitch_3
    invoke-virtual {p2, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setUri(Landroid/net/Uri;)V

    .line 77
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getUri()Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 79
    .local v2, "str":Ljava/lang/String;
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 80
    :goto_3
    if-lez v1, :cond_7

    .line 81
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    add-int/2addr v3, v1

    rem-int/lit8 v3, v3, 0x3

    invoke-virtual {p0, v3}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setColor(I)V

    goto/16 :goto_0

    :cond_6
    move v1, v3

    .line 79
    goto :goto_3

    .line 83
    :cond_7
    invoke-virtual {p0, v3}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setColor(I)V

    goto/16 :goto_0

    .line 88
    .end local v2    # "str":Ljava/lang/String;
    :pswitch_4
    invoke-virtual {p2, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setIntentValue(Landroid/content/Intent;)V

    .line 89
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getIntentValue()Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getIntentValue()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v2

    .line 91
    .restart local v2    # "str":Ljava/lang/String;
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 92
    :goto_4
    if-lez v1, :cond_9

    .line 93
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getIntentValue()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    add-int/2addr v3, v1

    rem-int/lit8 v3, v3, 0x3

    invoke-virtual {p0, v3}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setColor(I)V

    goto/16 :goto_0

    :cond_8
    move v1, v3

    .line 91
    goto :goto_4

    .line 95
    :cond_9
    invoke-virtual {p0, v3}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setColor(I)V

    goto/16 :goto_0

    .line 100
    .end local v2    # "str":Ljava/lang/String;
    :pswitch_5
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->multiUri:Ljava/util/ArrayList;

    .line 101
    const/4 v0, 0x0

    .local v0, "cnt":I
    :goto_5
    invoke-virtual {p2}, Landroid/content/ClipData;->getItemCount()I

    move-result v4

    if-ge v0, v4, :cond_a

    .line 102
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->multiUri:Ljava/util/ArrayList;

    invoke-virtual {p2, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 104
    :cond_a
    invoke-virtual {p0, v3}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setColor(I)V

    goto/16 :goto_0

    .line 107
    .end local v0    # "cnt":I
    :pswitch_6
    iput-object p2, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->clipData:Landroid/content/ClipData;

    .line 108
    invoke-virtual {p0, v3}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setColor(I)V

    goto/16 :goto_0

    .line 111
    :pswitch_7
    invoke-virtual {p2, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setPath(Ljava/lang/String;)V

    .line 112
    invoke-virtual {p2, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setUri(Landroid/net/Uri;)V

    .line 113
    invoke-virtual {p0, v3}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setColor(I)V

    goto/16 :goto_0

    .line 117
    :pswitch_8
    invoke-virtual {p2, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setPath(Ljava/lang/String;)V

    .line 118
    invoke-virtual {p2, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setUri(Landroid/net/Uri;)V

    .line 119
    invoke-virtual {p0, v3}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setColor(I)V

    goto/16 :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->bitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 134
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->settingBitmap(Z)V

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->bitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getBitmapDrawable()Landroid/graphics/drawable/BitmapDrawable;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->drawable:Landroid/graphics/drawable/BitmapDrawable;

    return-object v0
.end method

.method public getBitmapHeight()I
    .locals 1

    .prologue
    .line 212
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->image_height:I

    return v0
.end method

.method public getBitmapWidth()I
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->image_width:I

    return v0
.end method

.method public getClipData()Landroid/content/ClipData;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->clipData:Landroid/content/ClipData;

    return-object v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->mColor:I

    return v0
.end method

.method public getFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 349
    move-object v1, p1

    .line 351
    .local v1, "tempStr":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "cnt":I
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 352
    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2f

    if-ne v2, v3, :cond_0

    .line 353
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 354
    const/4 v0, 0x0

    .line 351
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 358
    :cond_1
    return-object v1
.end method

.method public getHtmlText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->htmlText:Ljava/lang/String;

    return-object v0
.end method

.method public getIntentValue()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->intentValue:Landroid/content/Intent;

    return-object v0
.end method

.method public getMultiUri()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->multiUri:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getProtect()Z
    .locals 1

    .prologue
    .line 216
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->protect:Z

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->text:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 184
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->type:I

    return v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->uri:Landroid/net/Uri;

    return-object v0
.end method

.method public isGalleryImage(Ljava/lang/String;)Z
    .locals 9
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 362
    const/4 v0, 0x1

    .line 363
    .local v0, "check":Z
    invoke-virtual {p0, p1}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 365
    .local v1, "fileName":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0x1c

    if-eq v7, v8, :cond_0

    .line 391
    :goto_0
    return v6

    .line 369
    :cond_0
    const/4 v7, 0x7

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 370
    .local v2, "first":Ljava/lang/String;
    const/16 v6, 0x8

    invoke-virtual {v1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 371
    .local v4, "second":C
    const/16 v6, 0x9

    const/16 v7, 0xe

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 374
    .local v5, "third":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v3, v6, :cond_1

    .line 375
    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    if-nez v6, :cond_4

    .line 376
    const/4 v0, 0x0

    .line 380
    :cond_1
    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v3, v6, :cond_2

    .line 381
    invoke-virtual {v5, v3}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    if-nez v6, :cond_5

    .line 382
    const/4 v0, 0x0

    .line 386
    :cond_2
    const/16 v6, 0x5f

    if-eq v4, v6, :cond_3

    .line 387
    const/4 v0, 0x0

    :cond_3
    move v6, v0

    .line 391
    goto :goto_0

    .line 374
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 380
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method public recycle()V
    .locals 0

    .prologue
    .line 401
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->bitmap:Landroid/graphics/Bitmap;

    .line 144
    return-void
.end method

.method public setBitmapDrawable(Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 0
    .param p1, "drawable"    # Landroid/graphics/drawable/BitmapDrawable;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->drawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 150
    return-void
.end method

.method public setBitmapHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 201
    iput p1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->image_height:I

    .line 202
    return-void
.end method

.method public setBitmapWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 197
    iput p1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->image_width:I

    .line 198
    return-void
.end method

.method public setClipData(Landroid/content/ClipData;)V
    .locals 0
    .param p1, "clipData"    # Landroid/content/ClipData;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->clipData:Landroid/content/ClipData;

    .line 131
    return-void
.end method

.method public setColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 230
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->mColor:I

    .line 231
    return-void
.end method

.method public setHtmlText(Ljava/lang/String;)V
    .locals 0
    .param p1, "htmlText"    # Ljava/lang/String;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->htmlText:Ljava/lang/String;

    .line 169
    return-void
.end method

.method public setIntentValue(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intentValue"    # Landroid/content/Intent;

    .prologue
    .line 175
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->intentValue:Landroid/content/Intent;

    .line 176
    return-void
.end method

.method public setMultiUri(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 193
    .local p1, "multiUri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->multiUri:Ljava/util/ArrayList;

    .line 194
    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->path:Ljava/lang/String;

    .line 157
    return-void
.end method

.method public setProtect(Z)V
    .locals 0
    .param p1, "protect"    # Z

    .prologue
    .line 220
    iput-boolean p1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->protect:Z

    .line 221
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->text:Ljava/lang/String;

    .line 163
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 187
    iput p1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->type:I

    .line 188
    return-void
.end method

.method public setUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->uri:Landroid/net/Uri;

    .line 182
    return-void
.end method

.method public settingBitmap()V
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->settingBitmap(Z)V

    .line 235
    return-void
.end method

.method public settingBitmap(II)V
    .locals 7
    .param p1, "reqWidth"    # I
    .param p2, "minHeight"    # I

    .prologue
    .line 263
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->path:Ljava/lang/String;

    .line 265
    .local v0, "bitmapPath":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 266
    sget-boolean v4, Landroid/sec/clipboard/data/ClipboardDefine;->DEBUG:Z

    if-eqz v4, :cond_0

    const-string v4, "ClipboardServiceEx"

    const-string v5, "return empty data because of bitmapPath"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    sget-boolean v4, Landroid/sec/clipboard/data/ClipboardDefine;->INFO_DEBUG:Z

    if-eqz v4, :cond_2

    .line 269
    const-string v4, "ClipboardServiceEx"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "bitmapPath = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_2
    :try_start_0
    sget-boolean v4, Landroid/sec/clipboard/data/ClipboardDefine;->DEBUG:Z

    if-eqz v4, :cond_3

    const-string v4, "ClipboardServiceEx"

    const-string v5, "BitmapFactory.decodeFile(bitmapPath, bitmapOption"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    :cond_3
    const-string v4, "jpg"

    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "_thum.jpg"

    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_4
    const-string v4, "png"

    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 277
    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_thum.jpg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 278
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 279
    .local v3, "thumbFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_6

    .line 280
    const-string v4, "_thum.jpg"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 286
    .end local v3    # "thumbFile":Ljava/io/File;
    :cond_6
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 288
    .local v2, "tempBm":Landroid/graphics/Bitmap;
    invoke-static {v2, p1, p2}, Lcom/samsung/android/clipboarduiservice/util/BitmapUtil;->getHeightScaleBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 289
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->path:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/samsung/android/clipboarduiservice/util/BitmapUtil;->getRotateBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->bitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 291
    .end local v2    # "tempBm":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v1

    .line 292
    .local v1, "e":Ljava/lang/Exception;
    sget-boolean v4, Landroid/sec/clipboard/data/ClipboardDefine;->DEBUG:Z

    if-eqz v4, :cond_0

    .line 293
    const-string v4, "ClipboardServiceEx"

    const-string v5, "exception arised during bm = BitmapFactory.decodeFile(bitmapPath, bitmapOption);"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public settingBitmap(III)V
    .locals 7
    .param p1, "reqWidth"    # I
    .param p2, "minHeight"    # I
    .param p3, "maxHeight"    # I

    .prologue
    .line 303
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->path:Ljava/lang/String;

    .line 305
    .local v0, "bitmapPath":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 306
    sget-boolean v4, Landroid/sec/clipboard/data/ClipboardDefine;->DEBUG:Z

    if-eqz v4, :cond_0

    const-string v4, "ClipboardServiceEx"

    const-string v5, "return empty data because of bitmapPath"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 308
    :cond_1
    sget-boolean v4, Landroid/sec/clipboard/data/ClipboardDefine;->INFO_DEBUG:Z

    if-eqz v4, :cond_2

    .line 310
    const-string v4, "ClipboardServiceEx"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "bitmapPath = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    :cond_2
    :try_start_0
    sget-boolean v4, Landroid/sec/clipboard/data/ClipboardDefine;->DEBUG:Z

    if-eqz v4, :cond_3

    const-string v4, "ClipboardServiceEx"

    const-string v5, "BitmapFactory.decodeFile(bitmapPath, bitmapOption"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    :cond_3
    const-string v4, "jpg"

    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "_thum.jpg"

    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_4
    const-string v4, "png"

    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 318
    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_thum.jpg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 319
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 320
    .local v3, "thumbFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_6

    .line 321
    const-string v4, "_thum.jpg"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 327
    .end local v3    # "thumbFile":Ljava/io/File;
    :cond_6
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 330
    .local v2, "tempBm":Landroid/graphics/Bitmap;
    invoke-static {v2, p1, p2, p3}, Lcom/samsung/android/clipboarduiservice/util/BitmapUtil;->getScaleBitmap(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 331
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->path:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/samsung/android/clipboarduiservice/util/BitmapUtil;->getRotateBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->bitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 333
    .end local v2    # "tempBm":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v1

    .line 334
    .local v1, "e":Ljava/lang/Exception;
    sget-boolean v4, Landroid/sec/clipboard/data/ClipboardDefine;->DEBUG:Z

    if-eqz v4, :cond_0

    .line 335
    const-string v4, "ClipboardServiceEx"

    const-string v5, "exception arised during bm = BitmapFactory.decodeFile(bitmapPath, bitmapOption);"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public settingBitmap(IILandroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "reqWidth"    # I
    .param p2, "minHeight"    # I
    .param p3, "b"    # Landroid/graphics/Bitmap;

    .prologue
    .line 342
    if-eqz p3, :cond_0

    .line 343
    invoke-static {p3, p1, p2}, Lcom/samsung/android/clipboarduiservice/util/BitmapUtil;->getHeightScaleBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->bitmap:Landroid/graphics/Bitmap;

    .line 345
    :cond_0
    return-void
.end method

.method public settingBitmap(Z)V
    .locals 4
    .param p1, "bGetBitmap"    # Z

    .prologue
    .line 238
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->path:Ljava/lang/String;

    .line 240
    .local v0, "bitmapPath":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 241
    sget-boolean v1, Landroid/sec/clipboard/data/ClipboardDefine;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "ClipboardServiceEx"

    const-string v2, "return empty data because of bitmapPath"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 243
    :cond_1
    sget-boolean v1, Landroid/sec/clipboard/data/ClipboardDefine;->INFO_DEBUG:Z

    if-eqz v1, :cond_2

    .line 244
    const-string v1, "ClipboardServiceEx"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bitmapPath = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->context:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/samsung/android/clipboarduiservice/util/BitmapUtil;->getBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->drawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 247
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->drawable:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v1, :cond_3

    .line 248
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->path:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/samsung/android/clipboarduiservice/util/BitmapUtil;->decodeFile(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->bitmap:Landroid/graphics/Bitmap;

    .line 249
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->drawable:Landroid/graphics/drawable/BitmapDrawable;

    goto :goto_0

    .line 251
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->drawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->bitmap:Landroid/graphics/Bitmap;

    .line 252
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 253
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->image_height:I

    .line 254
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->image_width:I

    goto :goto_0
.end method
