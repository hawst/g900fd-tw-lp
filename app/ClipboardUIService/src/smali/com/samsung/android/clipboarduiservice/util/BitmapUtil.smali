.class public Lcom/samsung/android/clipboarduiservice/util/BitmapUtil;
.super Ljava/lang/Object;
.source "BitmapUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static convert(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    const/4 v5, 0x0

    .line 343
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-static {v3, v4, p1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 344
    .local v1, "convertedBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 345
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 346
    .local v2, "paint":Landroid/graphics/Paint;
    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 347
    invoke-virtual {v0, p0, v5, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 348
    return-object v1
.end method

.method public static decodeFile(Ljava/io/File;)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "f"    # Ljava/io/File;

    .prologue
    const/4 v3, 0x0

    .line 61
    if-nez p0, :cond_1

    move-object v2, v3

    .line 83
    :cond_0
    :goto_0
    return-object v2

    .line 64
    :cond_1
    const/4 v2, 0x0

    .line 65
    .local v2, "resized":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 66
    .local v1, "o":Landroid/graphics/BitmapFactory$Options;
    const/4 v4, 0x0

    iput-boolean v4, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 67
    const/4 v4, 0x2

    iput v4, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 69
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/4 v5, 0x0

    invoke-static {v4, v5, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 71
    if-eqz v2, :cond_0

    .line 72
    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v4}, Lcom/samsung/android/clipboarduiservice/util/BitmapUtil;->convert(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    goto :goto_0

    .line 76
    .end local v1    # "o":Landroid/graphics/BitmapFactory$Options;
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :goto_1
    move-object v2, v3

    .line 83
    goto :goto_0

    .line 79
    :catch_1
    move-exception v0

    .line 81
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static decodeFile(Ljava/io/File;I)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "file"    # Ljava/io/File;
    .param p1, "width"    # I

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 210
    const/4 v0, 0x0

    .line 211
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v5, 0x0

    .line 212
    .local v5, "resized":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 213
    .local v4, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x0

    .line 215
    .local v2, "fis":Ljava/io/FileInputStream;
    const/4 v8, 0x2

    iput v8, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 216
    iput-boolean v10, v4, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 217
    const/4 v8, 0x0

    iput-boolean v8, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 218
    iput-boolean v10, v4, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 219
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v8, v4, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 220
    iput-boolean v10, v4, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    .line 223
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 224
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .local v3, "fis":Ljava/io/FileInputStream;
    const/4 v8, 0x0

    :try_start_1
    invoke-static {v3, v8, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 225
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-object v2, v3

    .line 234
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :goto_0
    const/4 v6, 0x0

    .line 235
    .local v6, "scaledHeight":F
    const/4 v7, 0x0

    .line 237
    .local v7, "yRate":F
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    if-le v8, p1, :cond_1

    .line 238
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-float v8, v8

    int-to-float v9, p1

    div-float v7, v8, v9

    .line 239
    cmpl-float v8, v7, v11

    if-nez v8, :cond_0

    const/high16 v7, 0x3f800000    # 1.0f

    .line 240
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-float v8, v8

    div-float v6, v8, v7

    .line 247
    :goto_1
    const/high16 v8, 0x3f800000    # 1.0f

    cmpl-float v8, v7, v8

    if-nez v8, :cond_3

    .line 248
    move-object v5, v0

    .line 252
    :goto_2
    return-object v5

    .line 226
    .end local v6    # "scaledHeight":F
    .end local v7    # "yRate":F
    :catch_0
    move-exception v1

    .line 228
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_3
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 229
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 231
    .local v1, "e":Ljava/io/IOException;
    :goto_4
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 242
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v6    # "scaledHeight":F
    .restart local v7    # "yRate":F
    :cond_1
    int-to-float v8, p1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    div-float v7, v8, v9

    .line 243
    cmpl-float v8, v7, v11

    if-nez v8, :cond_2

    const/high16 v7, 0x3f800000    # 1.0f

    .line 244
    :cond_2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-float v8, v8

    mul-float v6, v8, v7

    goto :goto_1

    .line 250
    :cond_3
    float-to-int v8, v6

    invoke-static {v0, p1, v8, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v5

    goto :goto_2

    .line 229
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "scaledHeight":F
    .end local v7    # "yRate":F
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catch_2
    move-exception v1

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 226
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catch_3
    move-exception v1

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_3
.end method

.method public static decodeFile(Ljava/io/File;II)Landroid/graphics/Bitmap;
    .locals 15
    .param p0, "file"    # Ljava/io/File;
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 256
    const/4 v1, 0x0

    .line 257
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    const/4 v8, 0x0

    .line 258
    .local v8, "resized":Landroid/graphics/Bitmap;
    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 259
    .local v7, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v4, 0x0

    .line 261
    .local v4, "fis":Ljava/io/FileInputStream;
    const/4 v13, 0x1

    iput-boolean v13, v7, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 264
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 265
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .local v5, "fis":Ljava/io/FileInputStream;
    const/4 v13, 0x0

    :try_start_1
    invoke-static {v5, v13, v7}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 266
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6

    .line 275
    :goto_0
    const/4 v11, 0x0

    .line 276
    .local v11, "xRate":F
    const/4 v12, 0x0

    .line 278
    .local v12, "yRate":F
    if-lez p1, :cond_0

    .line 279
    iget v13, v7, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move/from16 v0, p1

    if-ge v13, v0, :cond_3

    const/4 v11, 0x0

    .line 282
    :cond_0
    :goto_1
    if-lez p2, :cond_1

    .line 283
    iget v13, v7, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move/from16 v0, p2

    if-ge v13, v0, :cond_4

    const/4 v12, 0x0

    .line 286
    :cond_1
    :goto_2
    const/4 v9, 0x1

    .line 289
    .local v9, "scale":I
    cmpg-float v13, v11, v12

    if-gez v13, :cond_6

    .line 290
    const/high16 v13, 0x3f800000    # 1.0f

    cmpl-float v13, v12, v13

    if-ltz v13, :cond_5

    const/high16 v13, 0x40000000    # 2.0f

    mul-float/2addr v13, v12

    float-to-int v9, v13

    .line 294
    :goto_3
    if-nez v9, :cond_2

    const/4 v9, 0x2

    .line 298
    :cond_2
    iput v9, v7, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 299
    const/4 v13, 0x1

    iput-boolean v13, v7, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 300
    const/4 v13, 0x0

    iput-boolean v13, v7, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 301
    const/4 v13, 0x1

    iput-boolean v13, v7, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 302
    sget-object v13, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v13, v7, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 303
    const/4 v13, 0x1

    iput-boolean v13, v7, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    .line 305
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->getInstance()Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;

    move-result-object v13

    invoke-virtual {v13, v7}, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->getBitmapFromReusableSet(Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 307
    .local v6, "inBitmap":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_8

    move-object v4, v5

    .line 339
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "inBitmap":Landroid/graphics/Bitmap;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :goto_4
    return-object v6

    .line 267
    .end local v9    # "scale":I
    .end local v11    # "xRate":F
    .end local v12    # "yRate":F
    :catch_0
    move-exception v3

    .line 269
    .local v3, "e1":Ljava/io/FileNotFoundException;
    :goto_5
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move-object v5, v4

    .line 273
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .line 270
    .end local v3    # "e1":Ljava/io/FileNotFoundException;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v2

    .line 272
    .local v2, "e":Ljava/io/IOException;
    :goto_6
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v5, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .line 279
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v11    # "xRate":F
    .restart local v12    # "yRate":F
    :cond_3
    iget v13, v7, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    div-int v13, v13, p1

    int-to-float v11, v13

    goto :goto_1

    .line 283
    :cond_4
    iget v13, v7, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    div-int v13, v13, p2

    int-to-float v12, v13

    goto :goto_2

    .line 290
    .restart local v9    # "scale":I
    :cond_5
    const/4 v9, 0x1

    goto :goto_3

    .line 292
    :cond_6
    const/high16 v13, 0x3f800000    # 1.0f

    cmpl-float v13, v11, v13

    if-ltz v13, :cond_7

    const/high16 v13, 0x40000000    # 2.0f

    mul-float/2addr v13, v11

    float-to-int v9, v13

    :goto_7
    goto :goto_3

    :cond_7
    const/4 v9, 0x1

    goto :goto_7

    .line 312
    .restart local v6    # "inBitmap":Landroid/graphics/Bitmap;
    :cond_8
    :try_start_2
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 313
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    const/4 v13, 0x0

    :try_start_3
    invoke-static {v4, v13, v7}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 314
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 323
    :goto_8
    const/4 v13, 0x0

    cmpl-float v13, v11, v13

    if-nez v13, :cond_b

    const/4 v13, 0x0

    cmpl-float v13, v12, v13

    if-nez v13, :cond_b

    .line 324
    move-object v8, v1

    .line 335
    :cond_9
    :goto_9
    if-eqz v8, :cond_a

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    const/16 v14, 0x1f4

    if-le v13, v14, :cond_a

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    const/16 v14, 0x1f4

    if-le v13, v14, :cond_a

    .line 336
    sget-object v13, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v13}, Lcom/samsung/android/clipboarduiservice/util/BitmapUtil;->convert(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    :cond_a
    move-object v6, v8

    .line 339
    goto :goto_4

    .line 315
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_2
    move-exception v2

    move-object v4, v5

    .line 317
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .local v2, "e":Ljava/io/FileNotFoundException;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :goto_a
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_8

    .line 318
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_3
    move-exception v2

    move-object v4, v5

    .line 320
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .local v2, "e":Ljava/io/IOException;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :goto_b
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 325
    .end local v2    # "e":Ljava/io/IOException;
    :cond_b
    cmpl-float v13, v11, v12

    if-ltz v13, :cond_c

    .line 326
    iget v13, v7, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    mul-int v13, v13, p1

    iget v14, v7, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    div-int/2addr v13, v14

    const/4 v14, 0x1

    move/from16 v0, p1

    invoke-static {v1, v0, v13, v14}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v8

    goto :goto_9

    .line 327
    :cond_c
    cmpg-float v13, v11, v12

    if-gez v13, :cond_9

    .line 328
    const/4 v10, 0x1

    .line 329
    .local v10, "tempWidth":I
    iget v13, v7, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v13, v13

    div-float/2addr v13, v12

    float-to-int v13, v13

    if-eqz v13, :cond_d

    .line 330
    iget v13, v7, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v13, v13

    div-float/2addr v13, v12

    float-to-int v10, v13

    .line 332
    :cond_d
    iget v13, v7, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v13, v13

    div-float/2addr v13, v12

    float-to-int v13, v13

    const/4 v14, 0x1

    invoke-static {v1, v10, v13, v14}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v8

    goto :goto_9

    .line 318
    .end local v10    # "tempWidth":I
    :catch_4
    move-exception v2

    goto :goto_b

    .line 315
    :catch_5
    move-exception v2

    goto :goto_a

    .line 270
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "inBitmap":Landroid/graphics/Bitmap;
    .end local v9    # "scale":I
    .end local v11    # "xRate":F
    .end local v12    # "yRate":F
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_6
    move-exception v2

    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .line 267
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_7
    move-exception v3

    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_5
.end method

.method public static decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "bitmapPath"    # Ljava/lang/String;
    .param p1, "bitmapOption"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 36
    const/4 v0, 0x0

    .line 37
    .local v0, "b":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    .line 39
    .local v2, "fis":Ljava/io/FileInputStream;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 42
    .local v4, "nf":Ljava/io/File;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 44
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .local v3, "fis":Ljava/io/FileInputStream;
    move-object v5, p1

    .line 45
    .local v5, "o":Landroid/graphics/BitmapFactory$Options;
    const/4 v6, 0x0

    :try_start_1
    iput-boolean v6, v5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 46
    const/4 v6, 0x2

    iput v6, v5, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 47
    const/4 v6, 0x0

    invoke-static {v3, v6, v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 48
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-object v2, v3

    .line 56
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v5    # "o":Landroid/graphics/BitmapFactory$Options;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :goto_0
    return-object v0

    .line 49
    :catch_0
    move-exception v1

    .line 51
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_1
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 52
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 54
    .local v1, "e":Ljava/io/IOException;
    :goto_2
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 52
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "o":Landroid/graphics/BitmapFactory$Options;
    :catch_2
    move-exception v1

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .line 49
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catch_3
    move-exception v1

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public static decodeOriginalFile(Ljava/io/File;)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "f"    # Ljava/io/File;

    .prologue
    const/4 v3, 0x0

    .line 88
    if-nez p0, :cond_1

    move-object v2, v3

    .line 109
    :cond_0
    :goto_0
    return-object v2

    .line 91
    :cond_1
    const/4 v2, 0x0

    .line 92
    .local v2, "resized":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 93
    .local v1, "o":Landroid/graphics/BitmapFactory$Options;
    const/4 v4, 0x0

    iput-boolean v4, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 94
    const/4 v4, 0x1

    iput v4, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 95
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/4 v5, 0x0

    invoke-static {v4, v5, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 97
    if-eqz v2, :cond_0

    .line 98
    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v4}, Lcom/samsung/android/clipboarduiservice/util/BitmapUtil;->convert(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    goto :goto_0

    .line 102
    .end local v1    # "o":Landroid/graphics/BitmapFactory$Options;
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :goto_1
    move-object v2, v3

    .line 109
    goto :goto_0

    .line 105
    :catch_1
    move-exception v0

    .line 107
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static getBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->getInstance()Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->getBitmapFromMemCache(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    .line 25
    .local v1, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    if-nez v1, :cond_0

    .line 26
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/util/BitmapUtil;->decodeFile(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 27
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 28
    .restart local v1    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->getInstance()Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->addBitmapToCache(Ljava/lang/String;Landroid/graphics/drawable/BitmapDrawable;)V

    .line 31
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    return-object v1
.end method

.method public static getDegree(I)I
    .locals 2
    .param p0, "exifOrientation"    # I

    .prologue
    .line 399
    const/4 v1, 0x6

    if-ne p0, v1, :cond_0

    .line 400
    const/16 v0, 0x5a

    .line 410
    .local v0, "degree":I
    :goto_0
    return v0

    .line 402
    .end local v0    # "degree":I
    :cond_0
    const/4 v1, 0x3

    if-ne p0, v1, :cond_1

    .line 403
    const/16 v0, 0xb4

    .restart local v0    # "degree":I
    goto :goto_0

    .line 405
    .end local v0    # "degree":I
    :cond_1
    const/16 v1, 0x8

    if-ne p0, v1, :cond_2

    .line 406
    const/16 v0, 0x10e

    .restart local v0    # "degree":I
    goto :goto_0

    .line 408
    .end local v0    # "degree":I
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "degree":I
    goto :goto_0
.end method

.method public static getHeightScaleBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "bm"    # Landroid/graphics/Bitmap;
    .param p1, "reqWidth"    # I
    .param p2, "minHeight"    # I

    .prologue
    const/4 v6, 0x1

    .line 114
    const/4 v3, 0x0

    .line 116
    .local v3, "resized":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 117
    .local v1, "bmWidth":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 118
    .local v0, "bmHeight":I
    const/4 v2, 0x0

    .line 119
    .local v2, "reqHeight":I
    const/4 v4, 0x0

    .line 121
    .local v4, "scaleWidth":I
    mul-int v5, v0, p1

    div-int v2, v5, v1

    .line 123
    if-ge v2, p2, :cond_0

    .line 124
    mul-int v5, v1, p2

    div-int v4, v5, v0

    .line 125
    invoke-static {p0, v4, p2, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 130
    :goto_0
    return-object v3

    .line 127
    :cond_0
    invoke-static {p0, p1, v2, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_0
.end method

.method public static getRotateBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v1, 0x0

    .line 353
    if-eqz p0, :cond_0

    .line 355
    const/4 v7, 0x0

    .line 357
    .local v7, "rotateBitmap":Landroid/graphics/Bitmap;
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 358
    .local v5, "matrix":Landroid/graphics/Matrix;
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 359
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 363
    .end local v5    # "matrix":Landroid/graphics/Matrix;
    .end local v7    # "rotateBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v7

    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public static getRotateBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "bm"    # Landroid/graphics/Bitmap;
    .param p1, "degree"    # I

    .prologue
    const/4 v1, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    .line 368
    const/4 v7, 0x0

    .line 369
    .local v7, "rotateBitmap":Landroid/graphics/Bitmap;
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    .line 370
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 371
    .local v5, "matrix":Landroid/graphics/Matrix;
    int-to-float v0, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    invoke-virtual {v5, v0, v2, v3}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 372
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    move-object p0, v7

    .line 377
    .end local v5    # "matrix":Landroid/graphics/Matrix;
    .end local p0    # "bm":Landroid/graphics/Bitmap;
    :cond_0
    return-object p0
.end method

.method public static getRotateBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "bm"    # Landroid/graphics/Bitmap;
    .param p1, "imagePath"    # Ljava/lang/String;

    .prologue
    .line 384
    :try_start_0
    new-instance v1, Landroid/media/ExifInterface;

    invoke-direct {v1, p1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 385
    .local v1, "exif":Landroid/media/ExifInterface;
    const-string v4, "Orientation"

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v3

    .line 386
    .local v3, "exifOrientation":I
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/util/BitmapUtil;->getDegree(I)I

    move-result v2

    .line 387
    .local v2, "exifDegree":I
    if-eqz p0, :cond_0

    .line 388
    invoke-static {p0, v2}, Lcom/samsung/android/clipboarduiservice/util/BitmapUtil;->getRotateBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 394
    .end local v1    # "exif":Landroid/media/ExifInterface;
    .end local v2    # "exifDegree":I
    .end local v3    # "exifOrientation":I
    :cond_0
    :goto_0
    return-object p0

    .line 390
    :catch_0
    move-exception v0

    .line 391
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getScaleBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "bm"    # Landroid/graphics/Bitmap;
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I

    .prologue
    const/4 v5, 0x0

    .line 135
    const/4 v2, 0x0

    .line 138
    .local v2, "resized":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 139
    .local v1, "bmWidth":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 141
    .local v0, "bmHeight":I
    const/4 v3, 0x0

    .line 142
    .local v3, "xRate":F
    const/4 v4, 0x0

    .line 144
    .local v4, "yRate":F
    if-ge v1, p1, :cond_1

    move v3, v5

    .line 145
    :goto_0
    if-ge v0, p2, :cond_2

    move v4, v5

    .line 147
    :goto_1
    cmpl-float v6, v3, v5

    if-nez v6, :cond_3

    cmpl-float v5, v4, v5

    if-nez v5, :cond_3

    .line 148
    move-object v2, p0

    .line 157
    .end local v0    # "bmHeight":I
    .end local v1    # "bmWidth":I
    .end local v3    # "xRate":F
    .end local v4    # "yRate":F
    :cond_0
    :goto_2
    return-object v2

    .line 144
    .restart local v0    # "bmHeight":I
    .restart local v1    # "bmWidth":I
    .restart local v3    # "xRate":F
    .restart local v4    # "yRate":F
    :cond_1
    div-int v6, p1, v1

    int-to-float v3, v6

    goto :goto_0

    .line 145
    :cond_2
    int-to-float v6, p2

    int-to-float v7, v0

    div-float v4, v6, v7

    goto :goto_1

    .line 149
    :cond_3
    cmpl-float v5, v3, v4

    if-lez v5, :cond_4

    .line 150
    int-to-float v5, v1

    mul-float/2addr v5, v3

    float-to-int v5, v5

    int-to-float v6, v0

    mul-float/2addr v6, v3

    float-to-int v6, v6

    const/4 v7, 0x1

    invoke-static {p0, v5, v6, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_2

    .line 151
    :cond_4
    cmpg-float v5, v3, v4

    if-gez v5, :cond_0

    .line 152
    int-to-float v5, v1

    mul-float/2addr v5, v4

    float-to-int v5, v5

    int-to-float v6, v0

    mul-float/2addr v6, v4

    float-to-int v6, v6

    const/4 v7, 0x1

    invoke-static {p0, v5, v6, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_2

    .line 154
    .end local v0    # "bmHeight":I
    .end local v1    # "bmWidth":I
    .end local v3    # "xRate":F
    .end local v4    # "yRate":F
    :catch_0
    move-exception v5

    goto :goto_2
.end method

.method public static getScaleBitmap(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "bm"    # Landroid/graphics/Bitmap;
    .param p1, "reqWidth"    # I
    .param p2, "minHeight"    # I
    .param p3, "maxHeight"    # I

    .prologue
    .line 161
    const/4 v2, 0x0

    .line 164
    .local v2, "resized":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 165
    .local v1, "bmWidth":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 167
    .local v0, "bmHeight":I
    const/4 v3, 0x0

    .line 168
    .local v3, "xRate":F
    const/4 v4, 0x0

    .line 172
    .local v4, "yRate":F
    int-to-float v5, p1

    int-to-float v6, v1

    div-float v3, v5, v6

    .line 173
    int-to-float v5, p2

    int-to-float v6, v0

    div-float v4, v5, v6

    .line 175
    if-ne v1, p1, :cond_2

    if-eq v0, p2, :cond_0

    if-ne v0, p3, :cond_2

    .line 176
    :cond_0
    move-object v2, p0

    .line 206
    .end local v0    # "bmHeight":I
    .end local v1    # "bmWidth":I
    .end local v3    # "xRate":F
    .end local v4    # "yRate":F
    :cond_1
    :goto_0
    return-object v2

    .line 178
    .restart local v0    # "bmHeight":I
    .restart local v1    # "bmWidth":I
    .restart local v3    # "xRate":F
    .restart local v4    # "yRate":F
    :cond_2
    if-gt v1, v0, :cond_3

    .line 179
    int-to-float v5, v1

    mul-float/2addr v5, v3

    float-to-int v5, v5

    int-to-float v6, v0

    mul-float/2addr v6, v3

    float-to-int v6, v6

    const/4 v7, 0x1

    invoke-static {p0, v5, v6, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 180
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    if-le v5, p3, :cond_1

    .line 181
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v2, v5, v6, p1, p3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0

    .line 185
    :cond_3
    int-to-float v5, v1

    mul-float/2addr v5, v4

    float-to-int v5, v5

    int-to-float v6, v0

    mul-float/2addr v6, v4

    float-to-int v6, v6

    const/4 v7, 0x1

    invoke-static {p0, v5, v6, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 186
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-le v5, p1, :cond_1

    .line 187
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v2, v5, v6, p1, p2}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 203
    .end local v0    # "bmHeight":I
    .end local v1    # "bmWidth":I
    .end local v3    # "xRate":F
    .end local v4    # "yRate":F
    :catch_0
    move-exception v5

    goto :goto_0
.end method
