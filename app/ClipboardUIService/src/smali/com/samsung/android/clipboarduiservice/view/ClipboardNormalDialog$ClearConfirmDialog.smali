.class Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;
.super Landroid/app/AlertDialog;
.source "ClipboardNormalDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ClearConfirmDialog"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mListener:Landroid/content/DialogInterface$OnClickListener;

.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;


# direct methods
.method public constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Landroid/content/Context;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "style"    # I

    .prologue
    .line 344
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .line 345
    invoke-direct {p0, p2, p3}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;I)V

    .line 346
    iput-object p2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->mContext:Landroid/content/Context;

    .line 347
    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    .prologue
    .line 339
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 351
    new-instance v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog$1;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;)V

    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->mListener:Landroid/content/DialogInterface$OnClickListener;

    .line 521
    const v2, 0x7f08000b

    invoke-virtual {p0, v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->setTitle(I)V

    .line 522
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsSupportLockFunc:Z
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 523
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->mContext:Landroid/content/Context;

    const v3, 0x7f08000e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 528
    :goto_0
    const/4 v2, -0x1

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->mContext:Landroid/content/Context;

    const v4, 0x7f08000c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->mListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0, v2, v3, v4}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 529
    const/4 v2, -0x2

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->mContext:Landroid/content/Context;

    const/high16 v4, 0x1040000

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->mListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0, v2, v3, v4}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 531
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 532
    .local v1, "theWindow":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 533
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    const/16 v2, 0x7d8

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 535
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 537
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 539
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    .line 540
    return-void

    .line 525
    .end local v0    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v1    # "theWindow":Landroid/view/Window;
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->mContext:Landroid/content/Context;

    const v3, 0x7f08000d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
