.class Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;
.super Ljava/lang/Object;
.source "ClipboardNormalDialog.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V
    .locals 0

    .prologue
    .line 649
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 18

    .prologue
    .line 652
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteScenario:Z
    invoke-static {v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$900(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 653
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteScenario:Z
    invoke-static {v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$900(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z

    move-result v14

    if-nez v14, :cond_1

    const/4 v14, 0x1

    :goto_0
    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteScenario:Z
    invoke-static {v15, v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$902(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z

    .line 654
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v14, v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteScenario:Z
    invoke-static {v15}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$900(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z

    move-result v15

    invoke-virtual {v14, v15}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setDeleteScenario(Z)V

    .line 657
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAddScenario:Z
    invoke-static {v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 658
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v14, v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v14}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildCount()I

    move-result v2

    .line 659
    .local v2, "childCount":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;
    invoke-static {v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v14

    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V

    .line 660
    const/4 v4, 0x0

    .local v4, "i":I
    const/4 v10, 0x0

    .local v10, "viewX":I
    const/4 v11, 0x0

    .local v11, "viewY":I
    :goto_1
    if-ge v4, v2, :cond_2

    .line 661
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v14, v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v14, v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v14

    invoke-virtual {v14}, Landroid/view/View;->getX()F

    move-result v14

    float-to-int v10, v14

    .line 662
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v14, v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v14, v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v14

    invoke-virtual {v14}, Landroid/view/View;->getY()F

    move-result v14

    float-to-int v11, v14

    .line 664
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;
    invoke-static {v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v14

    new-instance v15, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v15, v0, v10, v11}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;II)V

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 660
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 653
    .end local v2    # "childCount":I
    .end local v4    # "i":I
    .end local v10    # "viewX":I
    .end local v11    # "viewY":I
    :cond_1
    const/4 v14, 0x0

    goto :goto_0

    .line 667
    .restart local v2    # "childCount":I
    .restart local v4    # "i":I
    .restart local v10    # "viewX":I
    .restart local v11    # "viewY":I
    :cond_2
    const/4 v5, 0x0

    .line 668
    .local v5, "itemView":Landroid/view/View;
    const/4 v6, 0x0

    .local v6, "position":I
    :goto_2
    if-ge v6, v2, :cond_4

    .line 669
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v14, v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v14, v6}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 670
    if-nez v6, :cond_3

    .line 671
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v14

    const/high16 v15, 0x7f040000

    invoke-static {v14, v15}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    .line 672
    .local v3, "fadeIn":Landroid/view/animation/Animation;
    const-wide/16 v14, 0x64

    invoke-virtual {v3, v14, v15}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 673
    invoke-virtual {v5, v3}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 674
    invoke-virtual {v5, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 668
    .end local v3    # "fadeIn":Landroid/view/animation/Animation;
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 676
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;
    invoke-static {v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;

    iget v7, v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;->x:I

    .line 677
    .local v7, "preX":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;
    invoke-static {v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;

    iget v8, v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;->y:I

    .line 678
    .local v8, "preY":I
    const/4 v9, 0x0

    .line 680
    .local v9, "trans":Landroid/view/animation/TranslateAnimation;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;
    invoke-static {v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v14

    add-int/lit8 v15, v6, -0x1

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;

    iget v12, v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;->x:I

    .line 681
    .local v12, "x":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;
    invoke-static {v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v14

    add-int/lit8 v15, v6, -0x1

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;

    iget v13, v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;->y:I

    .line 682
    .local v13, "y":I
    new-instance v9, Landroid/view/animation/TranslateAnimation;

    .end local v9    # "trans":Landroid/view/animation/TranslateAnimation;
    sub-int v14, v12, v7

    int-to-float v14, v14

    const/4 v15, 0x0

    sub-int v16, v13, v8

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v9, v14, v15, v0, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 684
    .restart local v9    # "trans":Landroid/view/animation/TranslateAnimation;
    const-wide/16 v14, 0x12c

    invoke-virtual {v9, v14, v15}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 685
    new-instance v14, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v14}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v9, v14}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 686
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    const/16 v15, 0x1e

    # += operator for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteAnimDelay:I
    invoke-static {v14, v15}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1212(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)I

    move-result v14

    int-to-long v14, v14

    invoke-virtual {v9, v14, v15}, Landroid/view/animation/TranslateAnimation;->setStartOffset(J)V

    .line 687
    invoke-virtual {v5, v9}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 688
    invoke-virtual {v5, v9}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_3

    .line 691
    .end local v7    # "preX":I
    .end local v8    # "preY":I
    .end local v9    # "trans":Landroid/view/animation/TranslateAnimation;
    .end local v12    # "x":I
    .end local v13    # "y":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAddScenario:Z
    invoke-static {v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z

    move-result v14

    if-nez v14, :cond_6

    const/4 v14, 0x1

    :goto_4
    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAddScenario:Z
    invoke-static {v15, v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1002(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z

    .line 692
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v14, v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAddScenario:Z
    invoke-static {v15}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z

    move-result v15

    invoke-virtual {v14, v15}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setAddScenario(Z)V

    .line 694
    .end local v2    # "childCount":I
    .end local v4    # "i":I
    .end local v5    # "itemView":Landroid/view/View;
    .end local v6    # "position":I
    .end local v10    # "viewX":I
    .end local v11    # "viewY":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    const/4 v15, 0x0

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteAnimDelay:I
    invoke-static {v14, v15}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1202(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)I

    .line 695
    return-void

    .line 691
    .restart local v2    # "childCount":I
    .restart local v4    # "i":I
    .restart local v5    # "itemView":Landroid/view/View;
    .restart local v6    # "position":I
    .restart local v10    # "viewX":I
    .restart local v11    # "viewY":I
    :cond_6
    const/4 v14, 0x0

    goto :goto_4
.end method
