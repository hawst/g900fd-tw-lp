.class Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;
.super Landroid/os/Handler;
.source "ClipboardNormalDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V
    .locals 0

    .prologue
    .line 1611
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1614
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1649
    :cond_0
    :goto_0
    return-void

    .line 1617
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 1618
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->getDataListSize()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    goto :goto_0

    .line 1622
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # invokes: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->updateClearLayout()V
    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2200(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    goto :goto_0

    .line 1625
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iput-boolean v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSyncIsOnGoing:Z

    .line 1626
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 1627
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 1631
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iput-boolean v1, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSyncIsOnGoing:Z

    .line 1632
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearButton:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 1633
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1635
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mNeedSyncAfterFinish:Z
    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2300(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z

    move-result v0

    if-ne v0, v2, :cond_2

    .line 1636
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mNeedSyncAfterFinish:Z
    invoke-static {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2302(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z

    .line 1637
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # invokes: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->syncClipboardData()V
    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2400(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    .line 1640
    :cond_2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11$1;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1614
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
