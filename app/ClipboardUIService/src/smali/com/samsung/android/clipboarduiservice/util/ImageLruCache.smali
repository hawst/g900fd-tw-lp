.class public Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;
.super Ljava/lang/Object;
.source "ImageLruCache.java"


# static fields
.field private static final CACHE_SIZE:I = 0x6400000

.field static _this:Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;


# instance fields
.field mImageCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/BitmapDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private mReusableBitmaps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;

    invoke-direct {v0}, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;-><init>()V

    sput-object v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->_this:Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache$1;

    const/high16 v1, 0x6400000

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache$1;-><init>(Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;I)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->mImageCache:Landroid/util/LruCache;

    .line 25
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->mReusableBitmaps:Ljava/util/Set;

    .line 26
    return-void
.end method

.method private static canUseForInBitmap(Landroid/graphics/Bitmap;Landroid/graphics/BitmapFactory$Options;)Z
    .locals 5
    .param p0, "candidate"    # Landroid/graphics/Bitmap;
    .param p1, "targetOptions"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 104
    iget v3, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v4, p1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v2, v3, v4

    .line 105
    .local v2, "width":I
    iget v3, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget v4, p1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v1, v3, v4

    .line 106
    .local v1, "height":I
    mul-int v3, v2, v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->getBytesPerPixel(Landroid/graphics/Bitmap$Config;)I

    move-result v4

    mul-int v0, v3, v4

    .line 108
    .local v0, "byteCount":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getAllocationByteCount()I

    move-result v3

    if-gt v0, v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static getBytesPerPixel(Landroid/graphics/Bitmap$Config;)I
    .locals 3
    .param p0, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 88
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne p0, v2, :cond_1

    .line 89
    const/4 v0, 0x4

    .line 98
    :cond_0
    :goto_0
    return v0

    .line 90
    :cond_1
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-eq p0, v2, :cond_0

    .line 92
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    if-eq p0, v2, :cond_0

    .line 94
    sget-object v0, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    if-ne p0, v0, :cond_2

    move v0, v1

    .line 95
    goto :goto_0

    :cond_2
    move v0, v1

    .line 98
    goto :goto_0
.end method

.method public static getInstance()Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->_this:Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;

    return-object v0
.end method


# virtual methods
.method public addBitmapToCache(Ljava/lang/String;Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 2
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "value"    # Landroid/graphics/drawable/BitmapDrawable;

    .prologue
    .line 64
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    const-class v0, Lcom/samsung/android/clipboarduiservice/util/RecyclingBitmapDrawable;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p2

    .line 71
    check-cast v0, Lcom/samsung/android/clipboarduiservice/util/RecyclingBitmapDrawable;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/util/RecyclingBitmapDrawable;->setIsCached(Z)V

    .line 74
    :cond_2
    sget-object v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->_this:Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->mImageCache:Landroid/util/LruCache;

    invoke-virtual {v0, p1, p2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public clearCache()V
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->_this:Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->mImageCache:Landroid/util/LruCache;

    if-eqz v0, :cond_0

    .line 113
    sget-object v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->_this:Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->mImageCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 115
    :cond_0
    return-void
.end method

.method public clearCache(Ljava/lang/String;)V
    .locals 1
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 118
    sget-object v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->_this:Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->mImageCache:Landroid/util/LruCache;

    if-eqz v0, :cond_0

    .line 119
    sget-object v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->_this:Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->mImageCache:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    :cond_0
    return-void
.end method

.method public clearLruCache()V
    .locals 2

    .prologue
    .line 58
    sget-object v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->_this:Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;

    iget-object v1, v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->mImageCache:Landroid/util/LruCache;

    monitor-enter v1

    .line 59
    :try_start_0
    sget-object v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->_this:Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->mImageCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 60
    monitor-exit v1

    .line 61
    return-void

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getBitmapFromMemCache(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 2
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 78
    const/4 v0, 0x0

    .line 80
    .local v0, "memValue":Landroid/graphics/drawable/BitmapDrawable;
    sget-object v1, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->_this:Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;

    iget-object v1, v1, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->mImageCache:Landroid/util/LruCache;

    if-eqz v1, :cond_0

    .line 81
    sget-object v1, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->_this:Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;

    iget-object v1, v1, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->mImageCache:Landroid/util/LruCache;

    invoke-virtual {v1, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "memValue":Landroid/graphics/drawable/BitmapDrawable;
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 84
    .restart local v0    # "memValue":Landroid/graphics/drawable/BitmapDrawable;
    :cond_0
    return-object v0
.end method

.method public getBitmapFromReusableSet(Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 124
    const/4 v0, 0x0

    .line 150
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    return-object v0
.end method
