.class public Lcom/samsung/android/clipboarduiservice/util/ImageLoader;
.super Ljava/lang/Object;
.source "ImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/clipboarduiservice/util/ImageLoader$OnImageLoaderListener;,
        Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;,
        Lcom/samsung/android/clipboarduiservice/util/ImageLoader$PhotosLoader;,
        Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;
    }
.end annotation


# instance fields
.field private final MAX_IMAGE_THREAD_POOL_VALUE:I

.field executorService:Ljava/util/concurrent/ExecutorService;

.field handler:Landroid/os/Handler;

.field mContext:Landroid/content/Context;

.field mCr:Landroid/content/ContentResolver;

.field mListener:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$OnImageLoaderListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x2

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->handler:Landroid/os/Handler;

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->mListener:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$OnImageLoaderListener;

    .line 56
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->MAX_IMAGE_THREAD_POOL_VALUE:I

    .line 59
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->mContext:Landroid/content/Context;

    .line 60
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->mCr:Landroid/content/ContentResolver;

    .line 61
    invoke-static {v1}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->executorService:Ljava/util/concurrent/ExecutorService;

    .line 62
    return-void
.end method


# virtual methods
.method public getBitmap(Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 7
    .param p1, "imageInfo"    # Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    .prologue
    .line 139
    const/4 v2, 0x0

    .line 140
    .local v2, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    const/4 v1, 0x0

    .line 142
    .local v1, "data":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    if-eqz p1, :cond_0

    .line 143
    invoke-virtual {p0, p1}, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->makeClipBitmapData(Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;)Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    move-result-object v1

    .line 146
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {v1}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 147
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    .end local v2    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 148
    .restart local v2    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    iget-object v4, p1, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->mClipData:Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    invoke-virtual {v4}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getBitmapWidth()I

    move-result v4

    const/16 v5, 0x12c

    if-gt v4, v5, :cond_3

    const-string v3, "small"

    .line 150
    .local v3, "postfix":Ljava/lang/String;
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p1, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->mClipData:Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    invoke-virtual {v6}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 151
    .local v0, "cacheKey":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->getInstance()Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;

    move-result-object v4

    invoke-virtual {v4, v0, v2}, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;->addBitmapToCache(Ljava/lang/String;Landroid/graphics/drawable/BitmapDrawable;)V

    .line 153
    .end local v0    # "cacheKey":Ljava/lang/String;
    .end local v3    # "postfix":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->mListener:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$OnImageLoaderListener;

    if-eqz v4, :cond_2

    if-eqz p1, :cond_2

    .line 154
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->mListener:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$OnImageLoaderListener;

    iget v5, p1, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->mPosition:I

    invoke-interface {v4, v5, v1}, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$OnImageLoaderListener;->OnCompleted(ILcom/samsung/android/clipboarduiservice/data/ClipboardUIData;)V

    .line 156
    :cond_2
    return-object v2

    .line 148
    :cond_3
    const-string v3, "big"

    goto :goto_0
.end method

.method imageViewReused(Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;)Z
    .locals 2
    .param p1, "photoToLoad"    # Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    .prologue
    .line 120
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->mImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->mClipData:Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    invoke-virtual {v0}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 122
    const/4 v0, 0x1

    .line 125
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public makeClipBitmapData(Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;)Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    .locals 1
    .param p1, "holder"    # Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    .prologue
    .line 130
    iget-object v0, p1, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->mClipData:Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p1, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->mClipData:Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    invoke-virtual {v0}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->settingBitmap()V

    .line 134
    :cond_0
    iget-object v0, p1, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->mClipData:Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    return-object v0
.end method

.method public queuePhoto(Landroid/widget/ImageView;Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;I)V
    .locals 3
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "clipData"    # Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    .param p3, "position"    # I

    .prologue
    .line 65
    new-instance v0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;-><init>(Lcom/samsung/android/clipboarduiservice/util/ImageLoader;Landroid/widget/ImageView;Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;I)V

    .line 67
    .local v0, "p":Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->executorService:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$PhotosLoader;

    invoke-direct {v2, p0, v0}, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$PhotosLoader;-><init>(Lcom/samsung/android/clipboarduiservice/util/ImageLoader;Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 69
    return-void
.end method

.method public resetThreadPool()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->executorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->executorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 74
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->executorService:Ljava/util/concurrent/ExecutorService;

    .line 76
    :cond_0
    return-void
.end method

.method public setImageLoaderListener(Lcom/samsung/android/clipboarduiservice/util/ImageLoader$OnImageLoaderListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/clipboarduiservice/util/ImageLoader$OnImageLoaderListener;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->mListener:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$OnImageLoaderListener;

    .line 80
    return-void
.end method
