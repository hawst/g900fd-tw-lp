.class public Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;
.super Landroid/widget/FrameLayout;
.source "ClippedItemViewContainer.java"


# instance fields
.field private final LOGTAG:Ljava/lang/String;

.field private mColorFilter:Landroid/graphics/PorterDuffColorFilter;

.field private mDisableColorFilter:Landroid/graphics/PorterDuffColorFilter;

.field private mIsDisabled:Z

.field private mIsPressed:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 75
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    const-string v0, "ClipBoardDialog"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->LOGTAG:Ljava/lang/String;

    .line 61
    iput-boolean v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->mIsPressed:Z

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->mIsDisabled:Z

    .line 64
    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    const v1, 0x4d3abcff    # 1.95809264E8f

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1, v2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->mColorFilter:Landroid/graphics/PorterDuffColorFilter;

    .line 68
    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v3, v1}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->mDisableColorFilter:Landroid/graphics/PorterDuffColorFilter;

    .line 76
    return-void
.end method


# virtual methods
.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "drawingTime"    # J

    .prologue
    .line 116
    iget-boolean v10, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->mIsPressed:Z

    if-nez v10, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->isEnabled()Z

    move-result v10

    if-nez v10, :cond_e

    .line 118
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->mColorFilter:Landroid/graphics/PorterDuffColorFilter;

    .line 119
    .local v2, "colorFilter":Landroid/graphics/PorterDuffColorFilter;
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->isEnabled()Z

    move-result v10

    if-nez v10, :cond_1

    .line 120
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->mDisableColorFilter:Landroid/graphics/PorterDuffColorFilter;

    .line 123
    const v10, 0x7f0a0019

    invoke-virtual {p0, v10}, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 125
    .local v4, "dim":Landroid/widget/ImageView;
    if-eqz v4, :cond_1

    .line 126
    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 130
    .end local v4    # "dim":Landroid/widget/ImageView;
    :cond_1
    instance-of v10, p2, Landroid/widget/ImageView;

    if-eqz v10, :cond_4

    move-object v6, p2

    .line 132
    check-cast v6, Landroid/widget/ImageView;

    .line 133
    .local v6, "iv":Landroid/widget/ImageView;
    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 135
    .local v3, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_2

    invoke-virtual {v3, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 140
    :cond_2
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v8

    .line 250
    .end local v2    # "colorFilter":Landroid/graphics/PorterDuffColorFilter;
    .end local v3    # "d":Landroid/graphics/drawable/Drawable;
    .end local v6    # "iv":Landroid/widget/ImageView;
    :cond_3
    :goto_0
    return v8

    .line 143
    .restart local v2    # "colorFilter":Landroid/graphics/PorterDuffColorFilter;
    :cond_4
    instance-of v10, p2, Landroid/webkit/WebView;

    if-eqz v10, :cond_6

    .line 145
    invoke-virtual {p2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 146
    .local v0, "bg":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_5

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 148
    :cond_5
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v8

    .line 149
    .local v8, "ret":Z
    goto :goto_0

    .line 151
    .end local v0    # "bg":Landroid/graphics/drawable/Drawable;
    .end local v8    # "ret":Z
    :cond_6
    instance-of v10, p2, Landroid/widget/TextView;

    if-eqz v10, :cond_a

    .line 152
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout;

    .line 153
    .local v7, "layout":Landroid/widget/FrameLayout;
    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 154
    .restart local v0    # "bg":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_7

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 156
    :cond_7
    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    .line 157
    .local v1, "childCount":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-ge v5, v1, :cond_9

    .line 158
    invoke-virtual {v7, v5}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 159
    .local v9, "v":Landroid/view/View;
    instance-of v10, v9, Landroid/widget/ImageView;

    if-eqz v10, :cond_8

    move-object v6, v9

    .line 160
    check-cast v6, Landroid/widget/ImageView;

    .line 161
    .restart local v6    # "iv":Landroid/widget/ImageView;
    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 162
    .restart local v3    # "d":Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_8

    invoke-virtual {v3, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 157
    .end local v3    # "d":Landroid/graphics/drawable/Drawable;
    .end local v6    # "iv":Landroid/widget/ImageView;
    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 165
    .end local v9    # "v":Landroid/view/View;
    :cond_9
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v8

    .line 166
    .restart local v8    # "ret":Z
    goto :goto_0

    .line 167
    .end local v0    # "bg":Landroid/graphics/drawable/Drawable;
    .end local v1    # "childCount":I
    .end local v5    # "i":I
    .end local v7    # "layout":Landroid/widget/FrameLayout;
    .end local v8    # "ret":Z
    :cond_a
    instance-of v10, p2, Landroid/widget/LinearLayout;

    if-eqz v10, :cond_17

    move-object v7, p2

    .line 168
    check-cast v7, Landroid/widget/LinearLayout;

    .line 169
    .local v7, "layout":Landroid/widget/LinearLayout;
    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 170
    .restart local v0    # "bg":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_b

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 172
    :cond_b
    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    .line 173
    .restart local v1    # "childCount":I
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_2
    if-ge v5, v1, :cond_d

    .line 174
    invoke-virtual {v7, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 175
    .restart local v9    # "v":Landroid/view/View;
    instance-of v10, v9, Landroid/widget/ImageView;

    if-eqz v10, :cond_c

    move-object v6, v9

    .line 176
    check-cast v6, Landroid/widget/ImageView;

    .line 177
    .restart local v6    # "iv":Landroid/widget/ImageView;
    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 178
    .restart local v3    # "d":Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_c

    invoke-virtual {v3, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 173
    .end local v3    # "d":Landroid/graphics/drawable/Drawable;
    .end local v6    # "iv":Landroid/widget/ImageView;
    :cond_c
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 181
    .end local v9    # "v":Landroid/view/View;
    :cond_d
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v8

    .line 182
    .restart local v8    # "ret":Z
    goto :goto_0

    .line 186
    .end local v0    # "bg":Landroid/graphics/drawable/Drawable;
    .end local v1    # "childCount":I
    .end local v2    # "colorFilter":Landroid/graphics/PorterDuffColorFilter;
    .end local v5    # "i":I
    .end local v7    # "layout":Landroid/widget/LinearLayout;
    .end local v8    # "ret":Z
    :cond_e
    instance-of v10, p2, Landroid/widget/ImageView;

    if-eqz v10, :cond_10

    move-object v6, p2

    .line 188
    check-cast v6, Landroid/widget/ImageView;

    .line 189
    .restart local v6    # "iv":Landroid/widget/ImageView;
    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 190
    .restart local v3    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v6}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 192
    .restart local v0    # "bg":Landroid/graphics/drawable/Drawable;
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v8

    .line 194
    .restart local v8    # "ret":Z
    if-eqz v3, :cond_f

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 195
    :cond_f
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    goto/16 :goto_0

    .line 199
    .end local v0    # "bg":Landroid/graphics/drawable/Drawable;
    .end local v3    # "d":Landroid/graphics/drawable/Drawable;
    .end local v6    # "iv":Landroid/widget/ImageView;
    .end local v8    # "ret":Z
    :cond_10
    instance-of v10, p2, Landroid/webkit/WebView;

    if-eqz v10, :cond_11

    .line 201
    invoke-virtual {p2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 203
    .restart local v0    # "bg":Landroid/graphics/drawable/Drawable;
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v8

    .line 205
    .restart local v8    # "ret":Z
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    goto/16 :goto_0

    .line 210
    .end local v0    # "bg":Landroid/graphics/drawable/Drawable;
    .end local v8    # "ret":Z
    :cond_11
    instance-of v10, p2, Landroid/widget/TextView;

    if-eqz v10, :cond_14

    .line 212
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout;

    .line 213
    .local v7, "layout":Landroid/widget/FrameLayout;
    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 215
    .restart local v0    # "bg":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    .line 217
    .restart local v1    # "childCount":I
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v8

    .line 218
    .restart local v8    # "ret":Z
    if-eqz v0, :cond_12

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 219
    :cond_12
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_3
    if-ge v5, v1, :cond_3

    .line 220
    invoke-virtual {v7, v5}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 221
    .restart local v9    # "v":Landroid/view/View;
    instance-of v10, v9, Landroid/widget/ImageView;

    if-eqz v10, :cond_13

    move-object v6, v9

    .line 222
    check-cast v6, Landroid/widget/ImageView;

    .line 223
    .restart local v6    # "iv":Landroid/widget/ImageView;
    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 224
    .restart local v3    # "d":Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_13

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 219
    .end local v3    # "d":Landroid/graphics/drawable/Drawable;
    .end local v6    # "iv":Landroid/widget/ImageView;
    :cond_13
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 228
    .end local v0    # "bg":Landroid/graphics/drawable/Drawable;
    .end local v1    # "childCount":I
    .end local v5    # "i":I
    .end local v7    # "layout":Landroid/widget/FrameLayout;
    .end local v8    # "ret":Z
    .end local v9    # "v":Landroid/view/View;
    :cond_14
    instance-of v10, p2, Landroid/widget/LinearLayout;

    if-eqz v10, :cond_17

    move-object v7, p2

    .line 229
    check-cast v7, Landroid/widget/LinearLayout;

    .line 230
    .local v7, "layout":Landroid/widget/LinearLayout;
    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 232
    .restart local v0    # "bg":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    .line 234
    .restart local v1    # "childCount":I
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v8

    .line 235
    .restart local v8    # "ret":Z
    if-eqz v0, :cond_15

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 236
    :cond_15
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_4
    if-ge v5, v1, :cond_3

    .line 237
    invoke-virtual {v7, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 238
    .restart local v9    # "v":Landroid/view/View;
    instance-of v10, v9, Landroid/widget/ImageView;

    if-eqz v10, :cond_16

    move-object v6, v9

    .line 239
    check-cast v6, Landroid/widget/ImageView;

    .line 240
    .restart local v6    # "iv":Landroid/widget/ImageView;
    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 241
    .restart local v3    # "d":Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_16

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 236
    .end local v3    # "d":Landroid/graphics/drawable/Drawable;
    .end local v6    # "iv":Landroid/widget/ImageView;
    :cond_16
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 250
    .end local v0    # "bg":Landroid/graphics/drawable/Drawable;
    .end local v1    # "childCount":I
    .end local v5    # "i":I
    .end local v7    # "layout":Landroid/widget/LinearLayout;
    .end local v8    # "ret":Z
    .end local v9    # "v":Landroid/view/View;
    :cond_17
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v8

    goto/16 :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 81
    invoke-super {p0}, Landroid/widget/FrameLayout;->drawableStateChanged()V

    .line 84
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->getDrawableState()[I

    move-result-object v2

    .line 86
    .local v2, "state":[I
    array-length v0, v2

    .line 87
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 90
    aget v3, v2, v1

    const v4, 0x101009e

    if-ne v3, v4, :cond_0

    .line 91
    iput-boolean v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->mIsDisabled:Z

    .line 94
    :cond_0
    aget v3, v2, v1

    const v4, 0x10100a7

    if-ne v3, v4, :cond_2

    .line 95
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->mIsPressed:Z

    .line 96
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->invalidate()V

    .line 97
    sget-boolean v3, Landroid/sec/clipboard/data/ClipboardDefine;->DEBUG:Z

    if-eqz v3, :cond_1

    const-string v3, "ClipBoardDialog"

    const-string v4, "pressed !!! "

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_1
    :goto_1
    return-void

    .line 87
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 103
    :cond_3
    iget-boolean v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->mIsPressed:Z

    if-nez v3, :cond_4

    iget-boolean v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->mIsDisabled:Z

    if-eqz v3, :cond_1

    .line 104
    :cond_4
    iput-boolean v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->mIsPressed:Z

    .line 105
    iput-boolean v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->mIsDisabled:Z

    .line 107
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedItemViewContainer;->invalidate()V

    goto :goto_1
.end method
