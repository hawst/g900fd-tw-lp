.class Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp$1;
.super Landroid/sec/clipboard/IClipboardWorkingFormUiInterface$Stub;
.source "ClipboardNormalDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;


# direct methods
.method constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;)V
    .locals 0

    .prologue
    .line 1789
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;

    invoke-direct {p0}, Landroid/sec/clipboard/IClipboardWorkingFormUiInterface$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public setClipboardDataListChange(I)V
    .locals 1
    .param p1, "arg0"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1799
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;

    invoke-virtual {v0, p1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->setClipboardDataListChange(I)V

    .line 1800
    return-void
.end method

.method public setClipboardDataMgr(Landroid/sec/clipboard/data/IClipboardDataList;)V
    .locals 1
    .param p1, "arg0"    # Landroid/sec/clipboard/data/IClipboardDataList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1794
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;

    invoke-virtual {v0, p1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->setClipboardDataMgr(Landroid/sec/clipboard/data/IClipboardDataList;)V

    .line 1795
    return-void
.end method

.method public setClipboardDataUiEvent(Landroid/sec/clipboard/IClipboardDataUiEvent;)V
    .locals 1
    .param p1, "arg0"    # Landroid/sec/clipboard/IClipboardDataUiEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1803
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp$1;->this$1:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;

    invoke-virtual {v0, p1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->setClipboardDataUiEvent(Landroid/sec/clipboard/IClipboardDataUiEvent;)V

    .line 1804
    return-void
.end method
