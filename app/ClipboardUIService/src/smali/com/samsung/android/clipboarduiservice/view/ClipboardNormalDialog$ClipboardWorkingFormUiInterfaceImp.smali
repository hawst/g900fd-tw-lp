.class Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;
.super Ljava/lang/Object;
.source "ClipboardNormalDialog.java"

# interfaces
.implements Landroid/sec/clipboard/IClipboardWorkingFormUiInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClipboardWorkingFormUiInterfaceImp"
.end annotation


# instance fields
.field private final mBinder:Landroid/sec/clipboard/IClipboardWorkingFormUiInterface$Stub;

.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;


# direct methods
.method private constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V
    .locals 1

    .prologue
    .line 1788
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1789
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp$1;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->mBinder:Landroid/sec/clipboard/IClipboardWorkingFormUiInterface$Stub;

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .param p2, "x1"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$1;

    .prologue
    .line 1788
    invoke-direct {p0, p1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 1907
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1908
    const-string v0, "ClipboardServiceEx"

    const-string v1, "asBinder"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1910
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->mBinder:Landroid/sec/clipboard/IClipboardWorkingFormUiInterface$Stub;

    return-object v0
.end method

.method public setClipboardDataListChange(I)V
    .locals 12
    .param p1, "arg0"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1845
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataList:Landroid/sec/clipboard/data/IClipboardDataList;
    invoke-static {v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2700(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/data/IClipboardDataList;

    move-result-object v9

    invoke-interface {v9}, Landroid/sec/clipboard/data/IClipboardDataList;->size()I

    move-result v1

    .line 1846
    .local v1, "currentDataCnt":I
    const/16 v9, 0x14

    if-le v1, v9, :cond_0

    .line 1847
    const/16 v1, 0x14

    .line 1850
    :cond_0
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1851
    const-string v9, "ClipboardServiceEx"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "setClipboardDataListChange() mDataCount = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDataCount:I
    invoke-static {v11}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " currentDataCnt = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1854
    :cond_1
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDataCount:I
    invoke-static {v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)I

    move-result v9

    if-gt v9, v1, :cond_3

    .line 1855
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    const/4 v10, 0x1

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAddScenario:Z
    invoke-static {v9, v10}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1002(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z

    .line 1856
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v9, v9, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    iget-object v10, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAddScenario:Z
    invoke-static {v10}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z

    move-result v10

    invoke-virtual {v9, v10}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setAddScenario(Z)V

    .line 1858
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 1859
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v9, v9, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v9}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildCount()I

    move-result v9

    if-ge v4, v9, :cond_2

    .line 1860
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v9, v9, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v9, v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getX()F

    move-result v9

    float-to-int v7, v9

    .line 1861
    .local v7, "viewX":I
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v9, v9, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v9, v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getY()F

    move-result v9

    float-to-int v8, v9

    .line 1863
    .local v8, "viewY":I
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v9

    new-instance v10, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;

    iget-object v11, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-direct {v10, v11, v7, v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;II)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1859
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1865
    .end local v7    # "viewX":I
    .end local v8    # "viewY":I
    :cond_2
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v10, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v10, v10, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v10}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getFirstPosition()I

    move-result v10

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mFirstPosition:I
    invoke-static {v9, v10}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2902(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)I

    .line 1868
    .end local v4    # "i":I
    :cond_3
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDataCount:I
    invoke-static {v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)I

    move-result v9

    if-le v9, v1, :cond_8

    .line 1869
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    const/4 v10, 0x1

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteScenario:Z
    invoke-static {v9, v10}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$902(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z

    .line 1870
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    const/4 v10, 0x0

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteAnimDelay:I
    invoke-static {v9, v10}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1202(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)I

    .line 1875
    :goto_1
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDataCount:I
    invoke-static {v9, v1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2802(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)I

    .line 1877
    move v3, p1

    .line 1878
    .local v3, "flag":I
    const/4 v9, 0x1

    if-ne v3, v9, :cond_4

    .line 1879
    sput v1, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CHILD_COUNT:I

    .line 1882
    :cond_4
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAddScenario:Z
    invoke-static {v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1883
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1884
    .local v6, "itemCount":I
    const/16 v9, 0x14

    if-lt v6, v9, :cond_6

    .line 1885
    const/4 v5, 0x0

    .line 1886
    .local v5, "index":I
    const/4 v2, 0x0

    .line 1887
    .local v2, "deleteclipUIData":Landroid/sec/clipboard/data/ClipboardData;
    const/4 v5, 0x0

    :goto_2
    if-ge v5, v6, :cond_5

    .line 1888
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataList:Landroid/sec/clipboard/data/IClipboardDataList;
    invoke-static {v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2700(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/data/IClipboardDataList;

    move-result-object v9

    add-int/lit8 v10, v6, -0x1

    sub-int/2addr v10, v5

    invoke-interface {v9, v10}, Landroid/sec/clipboard/data/IClipboardDataList;->getItem(I)Landroid/sec/clipboard/data/ClipboardData;

    move-result-object v2

    .line 1889
    invoke-virtual {v2}, Landroid/sec/clipboard/data/ClipboardData;->GetProtectState()Z

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_9

    .line 1893
    :cond_5
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v9

    add-int/lit8 v10, v6, -0x1

    sub-int/2addr v10, v5

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    invoke-virtual {v9}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->recycle()V

    .line 1894
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v9

    add-int/lit8 v10, v6, -0x1

    sub-int/2addr v10, v5

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1896
    .end local v2    # "deleteclipUIData":Landroid/sec/clipboard/data/ClipboardData;
    .end local v5    # "index":I
    :cond_6
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    const/4 v10, 0x0

    # invokes: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->ClipDataToUIData(I)Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    invoke-static {v9, v10}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$3000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    move-result-object v0

    .line 1897
    .local v0, "clipUIData":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1900
    .end local v0    # "clipUIData":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    .end local v6    # "itemCount":I
    :cond_7
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    const/4 v10, 0x1

    # invokes: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->sendMessage(I)V
    invoke-static {v9, v10}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)V

    .line 1902
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    const/4 v10, 0x2

    # invokes: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->sendMessage(I)V
    invoke-static {v9, v10}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)V

    .line 1903
    return-void

    .line 1872
    .end local v3    # "flag":I
    :cond_8
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    const/4 v10, 0x0

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteScenario:Z
    invoke-static {v9, v10}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$902(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z

    goto :goto_1

    .line 1887
    .restart local v2    # "deleteclipUIData":Landroid/sec/clipboard/data/ClipboardData;
    .restart local v3    # "flag":I
    .restart local v5    # "index":I
    .restart local v6    # "itemCount":I
    :cond_9
    add-int/lit8 v5, v5, 0x1

    goto :goto_2
.end method

.method public setClipboardDataMgr(Landroid/sec/clipboard/data/IClipboardDataList;)V
    .locals 4
    .param p1, "arg0"    # Landroid/sec/clipboard/data/IClipboardDataList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1811
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1812
    const-string v0, "ClipboardServiceEx"

    const-string v1, "setClipboardDataMgr called!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1814
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataList:Landroid/sec/clipboard/data/IClipboardDataList;
    invoke-static {v0, p1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2702(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Landroid/sec/clipboard/data/IClipboardDataList;)Landroid/sec/clipboard/data/IClipboardDataList;

    .line 1815
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataList:Landroid/sec/clipboard/data/IClipboardDataList;
    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2700(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/data/IClipboardDataList;

    move-result-object v0

    invoke-interface {v0}, Landroid/sec/clipboard/data/IClipboardDataList;->size()I

    move-result v0

    sput v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CHILD_COUNT:I

    .line 1819
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataList:Landroid/sec/clipboard/data/IClipboardDataList;
    invoke-static {v1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2700(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/data/IClipboardDataList;

    move-result-object v1

    invoke-interface {v1}, Landroid/sec/clipboard/data/IClipboardDataList;->size()I

    move-result v1

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDataCount:I
    invoke-static {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2802(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)I

    .line 1820
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1821
    const-string v0, "ClipboardServiceEx"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setClipboardDataMgr() mDataCount = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDataCount:I
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1827
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    new-instance v1, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Landroid/content/Context;)V

    iput-object v1, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAdapter:Landroid/widget/BaseAdapter;

    .line 1828
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v1, v1, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1830
    return-void
.end method

.method public setClipboardDataUiEvent(Landroid/sec/clipboard/IClipboardDataUiEvent;)V
    .locals 2
    .param p1, "arg0"    # Landroid/sec/clipboard/IClipboardDataUiEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1836
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837
    const-string v0, "ClipboardServiceEx"

    const-string v1, "setClipboardDataUiEvent(IClipboardDataUiEvent arg0) called!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1839
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataUiEvent:Landroid/sec/clipboard/IClipboardDataUiEvent;
    invoke-static {v0, p1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$402(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Landroid/sec/clipboard/IClipboardDataUiEvent;)Landroid/sec/clipboard/IClipboardDataUiEvent;

    .line 1840
    return-void
.end method
