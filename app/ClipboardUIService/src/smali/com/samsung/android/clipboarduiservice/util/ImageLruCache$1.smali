.class Lcom/samsung/android/clipboarduiservice/util/ImageLruCache$1;
.super Landroid/util/LruCache;
.source "ImageLruCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Landroid/graphics/drawable/BitmapDrawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;


# direct methods
.method constructor <init>(Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;I)V
    .locals 0
    .param p2, "x0"    # I

    .prologue
    .line 32
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache$1;->this$0:Lcom/samsung/android/clipboarduiservice/util/ImageLruCache;

    invoke-direct {p0, p2}, Landroid/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Z
    .param p2, "x1"    # Ljava/lang/Object;
    .param p3, "x2"    # Ljava/lang/Object;
    .param p4, "x3"    # Ljava/lang/Object;

    .prologue
    .line 32
    check-cast p2, Ljava/lang/String;

    .end local p2    # "x1":Ljava/lang/Object;
    check-cast p3, Landroid/graphics/drawable/BitmapDrawable;

    .end local p3    # "x2":Ljava/lang/Object;
    check-cast p4, Landroid/graphics/drawable/BitmapDrawable;

    .end local p4    # "x3":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache$1;->entryRemoved(ZLjava/lang/String;Landroid/graphics/drawable/BitmapDrawable;Landroid/graphics/drawable/BitmapDrawable;)V

    return-void
.end method

.method protected entryRemoved(ZLjava/lang/String;Landroid/graphics/drawable/BitmapDrawable;Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 1
    .param p1, "evicted"    # Z
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "oldValue"    # Landroid/graphics/drawable/BitmapDrawable;
    .param p4, "newValue"    # Landroid/graphics/drawable/BitmapDrawable;

    .prologue
    .line 49
    const-class v0, Lcom/samsung/android/clipboarduiservice/util/RecyclingBitmapDrawable;

    invoke-virtual {v0, p3}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    check-cast p3, Lcom/samsung/android/clipboarduiservice/util/RecyclingBitmapDrawable;

    .end local p3    # "oldValue":Landroid/graphics/drawable/BitmapDrawable;
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lcom/samsung/android/clipboarduiservice/util/RecyclingBitmapDrawable;->setIsCached(Z)V

    .line 54
    :cond_0
    return-void
.end method

.method protected bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 32
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/graphics/drawable/BitmapDrawable;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/clipboarduiservice/util/ImageLruCache$1;->sizeOf(Ljava/lang/String;Landroid/graphics/drawable/BitmapDrawable;)I

    move-result v0

    return v0
.end method

.method protected sizeOf(Ljava/lang/String;Landroid/graphics/drawable/BitmapDrawable;)I
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "drawable"    # Landroid/graphics/drawable/BitmapDrawable;

    .prologue
    .line 37
    const/4 v1, 0x0

    .line 38
    .local v1, "spaceTaken":I
    invoke-virtual {p2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 40
    .local v0, "map":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 41
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v1

    .line 43
    :cond_0
    return v1
.end method
