.class public Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;
.super Landroid/view/ViewGroup;
.source "ClippedStaggeredGridView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemLongClickListener;,
        Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemClickListener;,
        Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterContextMenuInfo;,
        Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$PerformClick;,
        Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;,
        Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;,
        Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SelectionBoundsAdjuster;,
        Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$WindowRunnnable;,
        Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SavedState;,
        Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$ColMap;,
        Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;,
        Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;,
        Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;,
        Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    }
.end annotation


# static fields
.field public static final COLUMN_COUNT_AUTO:I = -0x1

.field private static final DEBUG:Z

.field private static final INVALID_POSITION:I = -0x1

.field private static final TAG:Ljava/lang/String; = "ClippedStaggeredGridView"

.field private static final TOUCH_MODE_DONE_WAITING:I = 0x5

.field private static final TOUCH_MODE_DOWN:I = 0x3

.field private static final TOUCH_MODE_DRAGGING:I = 0x1

.field private static final TOUCH_MODE_FLINGING:I = 0x2

.field private static final TOUCH_MODE_IDLE:I = 0x0

.field private static final TOUCH_MODE_REST:I = 0x6

.field private static final TOUCH_MODE_TAP:I = 0x4


# instance fields
.field private mActivePointerId:I

.field private mAdapter:Landroid/widget/ListAdapter;

.field private mAddScenario:Z

.field private mBeginClick:Z

.field private final mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private mColCount:I

.field private mColCountSetting:I

.field private mColMappings:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private mColWidth:I

.field private mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

.field private mDataChanged:Z

.field private mDeleteScenario:Z

.field mDrawSelectorOnTop:Z

.field private mFastChildLayout:Z

.field private mFirstAdapterId:J

.field private mFirstPosition:I

.field private mFlingVelocity:I

.field private mHasStableIds:Z

.field private mInLayout:Z

.field private mIsChildViewEnabled:Z

.field private mItemBottoms:[I

.field private mItemCount:I

.field private mItemMargin:I

.field private mItemTops:[I

.field private mLastTouchX:F

.field private mLastTouchY:F

.field private final mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/SparseArrayCompat",
            "<",
            "Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mMaximumVelocity:I

.field private mMinColWidth:I

.field private mMotionPosition:I

.field private mNumCols:I

.field private final mObserver:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;

.field mOnItemClickListener:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemClickListener;

.field mOnItemLongClickListener:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemLongClickListener;

.field private mPendingCheckForLongPress:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;

.field private mPendingCheckForTap:Ljava/lang/Runnable;

.field private mPerformClick:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$PerformClick;

.field private mPopulating:Z

.field private final mRecycler:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;

.field private mRestoreOffsets:[I

.field private final mScroller:Landroid/widget/Scroller;

.field mSelectionBottomPadding:I

.field mSelectionLeftPadding:I

.field mSelectionRightPadding:I

.field mSelectionTopPadding:I

.field mSelector:Landroid/graphics/drawable/Drawable;

.field mSelectorPosition:I

.field mSelectorRect:Landroid/graphics/Rect;

.field private final mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private mTouchFrame:Landroid/graphics/Rect;

.field private mTouchMode:I

.field private mTouchModeReset:Ljava/lang/Runnable;

.field private mTouchRemainderY:F

.field private mTouchSlop:I

.field private final mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 81
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 349
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 350
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 354
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 355
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 359
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 118
    iput v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCountSetting:I

    .line 119
    iput v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    .line 120
    iput v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMinColWidth:I

    .line 126
    iput-boolean v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDeleteScenario:Z

    .line 127
    iput-boolean v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAddScenario:Z

    .line 135
    new-instance v2, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;)V

    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRecycler:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;

    .line 138
    new-instance v2, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;)V

    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mObserver:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;

    .line 175
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 183
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColMappings:Ljava/util/ArrayList;

    .line 189
    iput-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 198
    iput-boolean v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDrawSelectorOnTop:Z

    .line 210
    iput v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectionLeftPadding:I

    .line 216
    iput v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectionTopPadding:I

    .line 222
    iput v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectionRightPadding:I

    .line 228
    iput v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectionBottomPadding:I

    .line 240
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectorRect:Landroid/graphics/Rect;

    .line 246
    const/4 v2, -0x1

    iput v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectorPosition:I

    .line 344
    new-instance v2, Landroid/support/v4/util/SparseArrayCompat;

    invoke-direct {v2}, Landroid/support/v4/util/SparseArrayCompat;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    .line 361
    const/4 v0, 0x0

    .line 362
    .local v0, "ta":Landroid/content/res/TypedArray;
    if-eqz p2, :cond_2

    .line 363
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/clipboarduiservice/R$styleable;->ClippedStaggeredGridView:[I

    invoke-virtual {v2, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 364
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    .line 365
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDrawSelectorOnTop:Z

    .line 366
    const/4 v2, 0x0

    invoke-virtual {v0, v5, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemMargin:I

    .line 373
    :goto_0
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 374
    .local v1, "vc":Landroid/view/ViewConfiguration;
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchSlop:I

    .line 375
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMaximumVelocity:I

    .line 376
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFlingVelocity:I

    .line 377
    new-instance v2, Landroid/widget/Scroller;

    invoke-direct {v2, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mScroller:Landroid/widget/Scroller;

    .line 381
    new-instance v2, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v2, p1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 382
    new-instance v2, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v2, p1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 383
    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setWillNotDraw(Z)V

    .line 384
    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setClipToPadding(Z)V

    .line 385
    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setFocusableInTouchMode(Z)V

    .line 387
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_0

    .line 388
    invoke-direct {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->useDefaultSelector()V

    .line 391
    :cond_0
    if-eqz v0, :cond_1

    .line 392
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 394
    :cond_1
    return-void

    .line 368
    .end local v1    # "vc":Landroid/view/ViewConfiguration;
    :cond_2
    iput v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    .line 369
    iput-boolean v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDrawSelectorOnTop:Z

    goto :goto_0
.end method

.method static synthetic access$1000(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mHasStableIds:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Landroid/support/v4/util/SparseArrayCompat;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->recycleAllViews()V

    return-void
.end method

.method static synthetic access$1300(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    return v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)[I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    return-object v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)[I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    return v0
.end method

.method static synthetic access$1602(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;
    .param p1, "x1"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    return p1
.end method

.method static synthetic access$1700(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAddScenario:Z

    return v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDeleteScenario:Z

    return v0
.end method

.method static synthetic access$1900(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)[I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRestoreOffsets:[I

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$2300(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$2400(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMotionPosition:I

    return v0
.end method

.method static synthetic access$2500(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPendingCheckForLongPress:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;)Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;
    .param p1, "x1"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPendingCheckForLongPress:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;
    .param p1, "x1"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDataChanged:Z

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;
    .param p1, "x1"    # Z

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDataChanged:Z

    return p1
.end method

.method static synthetic access$700(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemCount:I

    return v0
.end method

.method static synthetic access$702(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;
    .param p1, "x1"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemCount:I

    return p1
.end method

.method static synthetic access$800(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Landroid/widget/ListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRecycler:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;

    return-object v0
.end method

.method private clearAllState()V
    .locals 1

    .prologue
    .line 2007
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v0}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    .line 2008
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->removeAllViews()V

    .line 2012
    invoke-direct {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->resetStateForGridTop()V

    .line 2016
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRecycler:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;

    invoke-virtual {v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;->clear()V

    .line 2019
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 2020
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectorPosition:I

    .line 2021
    return-void
.end method

.method private final contentFits()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 775
    iget v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildCount()I

    move-result v4

    iget v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemCount:I

    if-eq v4, v5, :cond_1

    .line 792
    :cond_0
    :goto_0
    return v3

    .line 780
    :cond_1
    const v2, 0x7fffffff

    .line 781
    .local v2, "topmost":I
    const/high16 v0, -0x80000000

    .line 782
    .local v0, "bottommost":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    if-ge v1, v4, :cond_4

    .line 783
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    aget v4, v4, v1

    if-ge v4, v2, :cond_2

    .line 784
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    aget v2, v4, v1

    .line 786
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    aget v4, v4, v1

    if-le v4, v0, :cond_3

    .line 787
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    aget v0, v4, v1

    .line 782
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 792
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getPaddingTop()I

    move-result v4

    if-lt v2, v4, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    if-gt v0, v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0
.end method

.method private displayMapping()V
    .locals 8

    .prologue
    .line 1748
    const-string v6, "DISPLAY"

    const-string v7, "MAP ****************"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1749
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 1750
    .local v5, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .line 1753
    .local v0, "col":I
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColMappings:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashSet;

    .line 1754
    .local v4, "map":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "COL"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1755
    const/16 v6, 0x20

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1756
    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1757
    .local v1, "i":Ljava/lang/Integer;
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1758
    const-string v6, " , "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1760
    .end local v1    # "i":Ljava/lang/Integer;
    :cond_0
    const-string v6, "DISPLAY"

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1761
    new-instance v5, Ljava/lang/StringBuilder;

    .end local v5    # "sb":Ljava/lang/StringBuilder;
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 1762
    .restart local v5    # "sb":Ljava/lang/StringBuilder;
    add-int/lit8 v0, v0, 0x1

    .line 1763
    goto :goto_0

    .line 1764
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "map":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    :cond_1
    const-string v6, "DISPLAY"

    const-string v7, "MAP END ****************"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1765
    return-void
.end method

.method private drawSelector(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 957
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mBeginClick:Z

    if-eqz v1, :cond_0

    .line 958
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    .line 959
    .local v0, "selector":Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 960
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 962
    .end local v0    # "selector":Landroid/graphics/drawable/Drawable;
    :cond_0
    return-void
.end method

.method private getFirstChildAtColumn(I)Landroid/view/View;
    .locals 6
    .param p1, "column"    # I

    .prologue
    .line 1542
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildCount()I

    move-result v4

    if-le v4, p1, :cond_2

    .line 1543
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    if-ge v2, v4, :cond_2

    .line 1544
    invoke-virtual {p0, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1547
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 1548
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 1549
    .local v3, "left":I
    const/4 v1, 0x0

    .line 1553
    .local v1, "col":I
    :goto_1
    iget v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColWidth:I

    iget v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemMargin:I

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    mul-int/2addr v4, v1

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getPaddingLeft()I

    move-result v5

    add-int/2addr v4, v5

    if-le v3, v4, :cond_0

    .line 1554
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1558
    :cond_0
    if-ne v1, p1, :cond_1

    .line 1568
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "col":I
    .end local v2    # "i":I
    .end local v3    # "left":I
    :goto_2
    return-object v0

    .line 1543
    .restart local v0    # "child":Landroid/view/View;
    .restart local v2    # "i":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1568
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "i":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private getSelectedItemPosition()I
    .locals 1

    .prologue
    .line 2602
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectorPosition:I

    return v0
.end method

.method private populate(Z)V
    .locals 8
    .param p1, "clearData"    # Z

    .prologue
    const/4 v5, 0x0

    .line 1064
    sget-boolean v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->DEBUG:Z

    if-eqz v4, :cond_0

    .line 1065
    const-string v4, "ClippedStaggeredGridView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "populate()_0 clearData : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1068
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getWidth()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getHeight()I

    move-result v4

    if-nez v4, :cond_2

    .line 1135
    :cond_1
    :goto_0
    return-void

    .line 1073
    :cond_2
    iget v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    const/4 v6, -0x1

    if-ne v4, v6, :cond_3

    .line 1074
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getWidth()I

    move-result v4

    iget v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMinColWidth:I

    div-int v0, v4, v6

    .line 1075
    .local v0, "colCount":I
    iget v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    if-eq v0, v4, :cond_3

    .line 1076
    iput v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    .line 1081
    .end local v0    # "colCount":I
    :cond_3
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    .line 1085
    .restart local v0    # "colCount":I
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColMappings:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    if-eq v4, v6, :cond_4

    .line 1086
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColMappings:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 1087
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    if-ge v1, v4, :cond_4

    .line 1088
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColMappings:Ljava/util/ArrayList;

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1087
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1093
    .end local v1    # "i":I
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    array-length v4, v4

    if-eq v4, v0, :cond_6

    .line 1094
    :cond_5
    new-array v4, v0, [I

    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    .line 1095
    new-array v4, v0, [I

    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    .line 1098
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v4}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    .line 1099
    iget-boolean v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mInLayout:Z

    if-eqz v4, :cond_8

    .line 1100
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->removeAllViewsInLayout()V

    .line 1107
    :cond_6
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getPaddingTop()I

    move-result v3

    .line 1108
    .local v3, "top":I
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    if-ge v1, v0, :cond_b

    .line 1109
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRestoreOffsets:[I

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRestoreOffsets:[I

    aget v4, v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    :goto_4
    add-int v2, v3, v4

    .line 1110
    .local v2, "offset":I
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    if-nez v2, :cond_a

    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    aget v4, v4, v1

    :goto_5
    aput v4, v6, v1

    .line 1111
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    if-nez v2, :cond_7

    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    aget v2, v6, v1

    .end local v2    # "offset":I
    :cond_7
    aput v2, v4, v1

    .line 1108
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1102
    .end local v1    # "i":I
    .end local v3    # "top":I
    :cond_8
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->removeAllViews()V

    goto :goto_2

    .restart local v1    # "i":I
    .restart local v3    # "top":I
    :cond_9
    move v4, v5

    .line 1109
    goto :goto_4

    .restart local v2    # "offset":I
    :cond_a
    move v4, v2

    .line 1110
    goto :goto_5

    .line 1115
    .end local v2    # "offset":I
    :cond_b
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPopulating:Z

    .line 1123
    iget-boolean v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDataChanged:Z

    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->layoutChildren(Z)V

    .line 1124
    iget v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildCount()I

    move-result v6

    add-int/2addr v4, v6

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->fillDown(II)I

    .line 1125
    iget v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->fillUp(II)I

    .line 1127
    iput-boolean v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPopulating:Z

    .line 1128
    iput-boolean v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDataChanged:Z

    .line 1131
    if-eqz p1, :cond_1

    .line 1132
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRestoreOffsets:[I

    if-eqz v4, :cond_1

    .line 1133
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRestoreOffsets:[I

    invoke-static {v4, v5}, Ljava/util/Arrays;->fill([II)V

    goto/16 :goto_0
.end method

.method private positionSelector(IIII)V
    .locals 5
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "r"    # I
    .param p4, "b"    # I

    .prologue
    .line 2648
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectorRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectionLeftPadding:I

    sub-int v1, p1, v1

    iget v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectionTopPadding:I

    sub-int v2, p2, v2

    iget v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectionRightPadding:I

    add-int/2addr v3, p3

    iget v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectionBottomPadding:I

    add-int/2addr v4, p4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2650
    return-void
.end method

.method private recycleAllViews()V
    .locals 3

    .prologue
    .line 797
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 798
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRecycler:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;

    invoke-virtual {p0, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;->addScrap(Landroid/view/View;)V

    .line 797
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 802
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mInLayout:Z

    if-eqz v1, :cond_1

    .line 803
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->removeAllViewsInLayout()V

    .line 807
    :goto_1
    return-void

    .line 805
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->removeAllViews()V

    goto :goto_1
.end method

.method private recycleOffscreenViews()V
    .locals 21

    .prologue
    .line 816
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getHeight()I

    move-result v12

    .line 817
    .local v12, "height":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemMargin:I

    move/from16 v18, v0

    move/from16 v0, v18

    neg-int v6, v0

    .line 818
    .local v6, "clearAbove":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemMargin:I

    move/from16 v18, v0

    add-int v7, v12, v18

    .line 819
    .local v7, "clearBelow":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildCount()I

    move-result v18

    add-int/lit8 v13, v18, -0x1

    .local v13, "i":I
    :goto_0
    if-ltz v13, :cond_0

    .line 820
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 821
    .local v4, "child":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v18

    move/from16 v0, v18

    if-gt v0, v7, :cond_6

    .line 839
    .end local v4    # "child":Landroid/view/View;
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildCount()I

    move-result v18

    if-lez v18, :cond_1

    .line 840
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 841
    .restart local v4    # "child":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v18

    move/from16 v0, v18

    if-lt v0, v6, :cond_8

    .line 860
    .end local v4    # "child":Landroid/view/View;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildCount()I

    move-result v5

    .line 861
    .local v5, "childCount":I
    if-lez v5, :cond_d

    .line 863
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    move-object/from16 v18, v0

    const v19, 0x7fffffff

    invoke-static/range {v18 .. v19}, Ljava/util/Arrays;->fill([II)V

    .line 864
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v18, v0

    const/high16 v19, -0x80000000

    invoke-static/range {v18 .. v19}, Ljava/util/Arrays;->fill([II)V

    .line 867
    const/4 v13, 0x0

    :goto_2
    if-ge v13, v5, :cond_b

    .line 868
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 869
    .restart local v4    # "child":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    .line 870
    .local v14, "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemMargin:I

    move/from16 v19, v0

    sub-int v17, v18, v19

    .line 871
    .local v17, "top":I
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v3

    .line 872
    .local v3, "bottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    move/from16 v19, v0

    add-int v19, v19, v13

    invoke-virtual/range {v18 .. v19}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;

    .line 874
    .local v15, "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    iget v0, v14, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->column:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    move/from16 v19, v0

    iget v0, v14, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->span:I

    move/from16 v20, v0

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->min(II)I

    move-result v19

    add-int v10, v18, v19

    .line 875
    .local v10, "colEnd":I
    iget v8, v14, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->column:I

    .local v8, "col":I
    :goto_3
    if-ge v8, v10, :cond_a

    .line 877
    const/4 v11, 0x0

    .line 878
    .local v11, "colTop":I
    if-eqz v15, :cond_2

    .line 879
    iget v0, v14, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->column:I

    move/from16 v18, v0

    sub-int v18, v8, v18

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->getMarginAbove(I)I

    move-result v18

    sub-int v11, v17, v18

    .line 882
    :cond_2
    const/16 v16, 0x0

    .line 883
    .local v16, "recMarginBelow":I
    if-eqz v15, :cond_3

    .line 884
    iget v0, v14, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->column:I

    move/from16 v18, v0

    sub-int v18, v8, v18

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->getMarginBelow(I)I

    move-result v16

    .line 886
    :cond_3
    add-int v9, v3, v16

    .line 887
    .local v9, "colBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    move-object/from16 v18, v0

    aget v18, v18, v8

    move/from16 v0, v18

    if-ge v11, v0, :cond_4

    .line 888
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    move-object/from16 v18, v0

    aput v11, v18, v8

    .line 890
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v18, v0

    aget v18, v18, v8

    move/from16 v0, v18

    if-le v9, v0, :cond_5

    .line 891
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v18, v0

    aput v9, v18, v8

    .line 875
    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 828
    .end local v3    # "bottom":I
    .end local v5    # "childCount":I
    .end local v8    # "col":I
    .end local v9    # "colBottom":I
    .end local v10    # "colEnd":I
    .end local v11    # "colTop":I
    .end local v14    # "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    .end local v15    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    .end local v16    # "recMarginBelow":I
    .end local v17    # "top":I
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mInLayout:Z

    move/from16 v18, v0

    if-eqz v18, :cond_7

    .line 829
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v13, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->removeViewsInLayout(II)V

    .line 835
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRecycler:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;->addScrap(Landroid/view/View;)V

    .line 819
    add-int/lit8 v13, v13, -0x1

    goto/16 :goto_0

    .line 831
    :cond_7
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->removeViewAt(I)V

    goto :goto_4

    .line 848
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mInLayout:Z

    move/from16 v18, v0

    if-eqz v18, :cond_9

    .line 849
    const/16 v18, 0x0

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->removeViewsInLayout(II)V

    .line 855
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRecycler:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;->addScrap(Landroid/view/View;)V

    .line 856
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    goto/16 :goto_1

    .line 851
    :cond_9
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->removeViewAt(I)V

    goto :goto_5

    .line 867
    .restart local v3    # "bottom":I
    .restart local v5    # "childCount":I
    .restart local v8    # "col":I
    .restart local v10    # "colEnd":I
    .restart local v14    # "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    .restart local v15    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    .restart local v17    # "top":I
    :cond_a
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_2

    .line 897
    .end local v3    # "bottom":I
    .end local v4    # "child":Landroid/view/View;
    .end local v8    # "col":I
    .end local v10    # "colEnd":I
    .end local v14    # "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    .end local v15    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    .end local v17    # "top":I
    :cond_b
    const/4 v8, 0x0

    .restart local v8    # "col":I
    :goto_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v8, v0, :cond_d

    .line 898
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    move-object/from16 v18, v0

    aget v18, v18, v8

    const v19, 0x7fffffff

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_c

    .line 900
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput v19, v18, v8

    .line 901
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput v19, v18, v8

    .line 897
    :cond_c
    add-int/lit8 v8, v8, 0x1

    goto :goto_6

    .line 905
    .end local v8    # "col":I
    :cond_d
    return-void
.end method

.method private resetStateForGridTop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2029
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    .line 2030
    .local v0, "colCount":I
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    array-length v2, v2

    if-eq v2, v0, :cond_1

    .line 2031
    :cond_0
    new-array v2, v0, [I

    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    .line 2032
    new-array v2, v0, [I

    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    .line 2034
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getPaddingTop()I

    move-result v1

    .line 2035
    .local v1, "top":I
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    invoke-static {v2, v1}, Ljava/util/Arrays;->fill([II)V

    .line 2036
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    invoke-static {v2, v1}, Ljava/util/Arrays;->fill([II)V

    .line 2040
    iput v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    .line 2041
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRestoreOffsets:[I

    if-eqz v2, :cond_2

    .line 2042
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRestoreOffsets:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([II)V

    .line 2043
    :cond_2
    return-void
.end method

.method private useDefaultSelector()V
    .locals 2

    .prologue
    .line 2550
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1080062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 2551
    return-void
.end method


# virtual methods
.method public beginFastChildLayout()V
    .locals 1

    .prologue
    .line 998
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFastChildLayout:Z

    .line 999
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "lp"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 2078
    instance-of v0, p1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    return v0
.end method

.method public computeScroll()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 909
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 910
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrY()I

    move-result v4

    .line 911
    .local v4, "y":I
    int-to-float v6, v4

    iget v7, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLastTouchY:F

    sub-float/2addr v6, v7

    float-to-int v0, v6

    .line 912
    .local v0, "dy":I
    int-to-float v6, v4

    iput v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLastTouchY:F

    .line 913
    invoke-virtual {p0, v0, v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->trackMotionScroll(IZ)Z

    move-result v6

    if-nez v6, :cond_1

    const/4 v3, 0x1

    .line 916
    .local v3, "stopped":Z
    :goto_0
    if-nez v3, :cond_2

    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->isFinished()Z

    move-result v6

    if-nez v6, :cond_2

    .line 917
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->postInvalidate()V

    .line 936
    .end local v0    # "dy":I
    .end local v3    # "stopped":Z
    .end local v4    # "y":I
    :cond_0
    :goto_1
    return-void

    .restart local v0    # "dy":I
    .restart local v4    # "y":I
    :cond_1
    move v3, v5

    .line 913
    goto :goto_0

    .line 919
    .restart local v3    # "stopped":Z
    :cond_2
    if-eqz v3, :cond_4

    .line 920
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->getOverScrollMode(Landroid/view/View;)I

    move-result v2

    .line 921
    .local v2, "overScrollMode":I
    const/4 v6, 0x2

    if-eq v2, v6, :cond_3

    .line 923
    if-lez v0, :cond_5

    .line 924
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 928
    .local v1, "edge":Landroid/support/v4/widget/EdgeEffectCompat;
    :goto_2
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrVelocity()F

    move-result v6

    float-to-int v6, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/support/v4/widget/EdgeEffectCompat;->onAbsorb(I)Z

    .line 929
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->postInvalidate()V

    .line 931
    .end local v1    # "edge":Landroid/support/v4/widget/EdgeEffectCompat;
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->abortAnimation()V

    .line 933
    .end local v2    # "overScrollMode":I
    :cond_4
    iput v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    goto :goto_1

    .line 926
    .restart local v2    # "overScrollMode":I
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .restart local v1    # "edge":Landroid/support/v4/widget/EdgeEffectCompat;
    goto :goto_2
.end method

.method createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "id"    # J

    .prologue
    .line 2816
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterContextMenuInfo;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterContextMenuInfo;-><init>(Landroid/view/View;IJ)V

    return-object v0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 941
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDrawSelectorOnTop:Z

    .line 942
    .local v0, "drawSelectorOnTop":Z
    if-nez v0, :cond_0

    .line 943
    invoke-direct {p0, p1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->drawSelector(Landroid/graphics/Canvas;)V

    .line 947
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 950
    if-eqz v0, :cond_1

    .line 951
    invoke-direct {p0, p1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->drawSelector(Landroid/graphics/Canvas;)V

    .line 953
    :cond_1
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 967
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 970
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v3, :cond_2

    .line 971
    const/4 v0, 0x0

    .line 972
    .local v0, "needsInvalidate":Z
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v3

    if-nez v3, :cond_0

    .line 973
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    .line 974
    const/4 v0, 0x1

    .line 976
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v3

    if-nez v3, :cond_1

    .line 977
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 978
    .local v1, "restoreCount":I
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getWidth()I

    move-result v2

    .line 979
    .local v2, "width":I
    neg-int v3, v2

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 980
    const/high16 v3, 0x43340000    # 180.0f

    int-to-float v4, v2

    const/4 v5, 0x0

    invoke-virtual {p1, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 981
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    .line 982
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 983
    const/4 v0, 0x1

    .line 987
    .end local v1    # "restoreCount":I
    .end local v2    # "width":I
    :cond_1
    if-eqz v0, :cond_2

    .line 988
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->invalidate()V

    .line 994
    .end local v0    # "needsInvalidate":Z
    :cond_2
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 2933
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 2934
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->updateSelectorState()V

    .line 2935
    return-void
.end method

.method public endFastChildLayout()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1003
    iput-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFastChildLayout:Z

    .line 1004
    invoke-direct {p0, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->populate(Z)V

    .line 1005
    return-void
.end method

.method final fillDown(II)I
    .locals 37
    .param p1, "fromPosition"    # I
    .param p2, "overhang"    # I

    .prologue
    .line 1582
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getPaddingLeft()I

    move-result v26

    .line 1583
    .local v26, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getPaddingRight()I

    move-result v27

    .line 1584
    .local v27, "paddingRight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemMargin:I

    move/from16 v21, v0

    .line 1585
    .local v21, "itemMargin":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getWidth()I

    move-result v35

    sub-int v35, v35, v26

    sub-int v35, v35, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    move/from16 v36, v0

    add-int/lit8 v36, v36, -0x1

    mul-int v36, v36, v21

    sub-int v35, v35, v36

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    move/from16 v36, v0

    div-int v11, v35, v36

    .line 1586
    .local v11, "colWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getHeight()I

    move-result v35

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getPaddingBottom()I

    move-result v36

    sub-int v14, v35, v36

    .line 1587
    .local v14, "gridBottom":I
    add-int v13, v14, p2

    .line 1588
    .local v13, "fillTo":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getNextColumnDown(I)I

    move-result v25

    .line 1589
    .local v25, "nextCol":I
    move/from16 v28, p1

    .line 1592
    .local v28, "position":I
    :cond_0
    :goto_0
    if-ltz v25, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v35, v0

    aget v35, v35, v25

    move/from16 v0, v35

    if-ge v0, v13, :cond_11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemCount:I

    move/from16 v35, v0

    move/from16 v0, v28

    move/from16 v1, v35

    if-ge v0, v1, :cond_11

    .line 1595
    const/16 v35, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v28

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->obtainView(ILandroid/view/View;)Landroid/view/View;

    move-result-object v5

    .line 1597
    .local v5, "child":Landroid/view/View;
    if-eqz v5, :cond_0

    .line 1600
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v24

    check-cast v24, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    .line 1601
    .local v24, "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    if-nez v24, :cond_1

    .line 1602
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->generateDefaultLayoutParams()Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    move-result-object v24

    .line 1603
    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1605
    :cond_1
    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    if-eq v0, v1, :cond_2

    .line 1606
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mInLayout:Z

    move/from16 v35, v0

    if-eqz v35, :cond_8

    .line 1607
    const/16 v35, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v35

    move-object/from16 v2, v24

    invoke-virtual {v0, v5, v1, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 1614
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    move/from16 v35, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->span:I

    move/from16 v36, v0

    invoke-static/range {v35 .. v36}, Ljava/lang/Math;->min(II)I

    move-result v31

    .line 1615
    .local v31, "span":I
    mul-int v35, v11, v31

    add-int/lit8 v36, v31, -0x1

    mul-int v36, v36, v21

    add-int v33, v35, v36

    .line 1616
    .local v33, "widthSize":I
    const/high16 v35, 0x40000000    # 2.0f

    move/from16 v0, v33

    move/from16 v1, v35

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v34

    .line 1620
    .local v34, "widthSpec":I
    const/16 v35, 0x1

    move/from16 v0, v31

    move/from16 v1, v35

    if-le v0, v1, :cond_9

    .line 1621
    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getNextRecordDown(II)Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;

    move-result-object v30

    .line 1628
    .local v30, "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    :goto_2
    const/16 v20, 0x0

    .line 1629
    .local v20, "invalidateAfter":Z
    if-nez v30, :cond_a

    .line 1630
    new-instance v30, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;

    .end local v30    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    const/16 v35, 0x0

    move-object/from16 v0, v30

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;)V

    .line 1631
    .restart local v30    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v28

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 1632
    move/from16 v0, v25

    move-object/from16 v1, v30

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->column:I

    .line 1633
    move/from16 v0, v31

    move-object/from16 v1, v30

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->span:I

    .line 1643
    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mHasStableIds:Z

    move/from16 v35, v0

    if-eqz v35, :cond_4

    .line 1644
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v18

    .line 1645
    .local v18, "id":J
    move-wide/from16 v0, v18

    move-object/from16 v2, v30

    iput-wide v0, v2, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->id:J

    .line 1646
    move-wide/from16 v0, v18

    move-object/from16 v2, v24

    iput-wide v0, v2, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->id:J

    .line 1650
    .end local v18    # "id":J
    :cond_4
    move/from16 v0, v25

    move-object/from16 v1, v24

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->column:I

    .line 1654
    move-object/from16 v0, v24

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->height:I

    move/from16 v35, v0

    const/16 v36, -0x2

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_b

    .line 1655
    const/16 v35, 0x0

    const/16 v36, 0x0

    invoke-static/range {v35 .. v36}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    .line 1659
    .local v15, "heightSpec":I
    :goto_4
    move/from16 v0, v34

    invoke-virtual {v5, v0, v15}, Landroid/view/View;->measure(II)V

    .line 1662
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    .line 1663
    .local v7, "childHeight":I
    if-nez v20, :cond_5

    move-object/from16 v0, v30

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->height:I

    move/from16 v35, v0

    move/from16 v0, v35

    if-eq v7, v0, :cond_6

    move-object/from16 v0, v30

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->height:I

    move/from16 v35, v0

    if-lez v35, :cond_6

    .line 1664
    :cond_5
    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->invalidateLayoutRecordsAfterPosition(I)V

    .line 1673
    :cond_6
    move-object/from16 v0, v30

    iput v7, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->height:I

    .line 1677
    const/16 v35, 0x1

    move/from16 v0, v31

    move/from16 v1, v35

    if-le v0, v1, :cond_d

    .line 1678
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v35, v0

    aget v22, v35, v25

    .line 1679
    .local v22, "lowest":I
    add-int/lit8 v16, v25, 0x1

    .local v16, "i":I
    :goto_5
    add-int v35, v25, v31

    move/from16 v0, v16

    move/from16 v1, v35

    if-ge v0, v1, :cond_c

    .line 1680
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v35, v0

    aget v4, v35, v16

    .line 1681
    .local v4, "bottom":I
    move/from16 v0, v22

    if-le v4, v0, :cond_7

    .line 1682
    move/from16 v22, v4

    .line 1679
    :cond_7
    add-int/lit8 v16, v16, 0x1

    goto :goto_5

    .line 1609
    .end local v4    # "bottom":I
    .end local v7    # "childHeight":I
    .end local v15    # "heightSpec":I
    .end local v16    # "i":I
    .end local v20    # "invalidateAfter":Z
    .end local v22    # "lowest":I
    .end local v30    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    .end local v31    # "span":I
    .end local v33    # "widthSize":I
    .end local v34    # "widthSpec":I
    :cond_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 1624
    .restart local v31    # "span":I
    .restart local v33    # "widthSize":I
    .restart local v34    # "widthSpec":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;

    .restart local v30    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    goto/16 :goto_2

    .line 1634
    .restart local v20    # "invalidateAfter":Z
    :cond_a
    move-object/from16 v0, v30

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->span:I

    move/from16 v35, v0

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_3

    .line 1635
    move/from16 v0, v31

    move-object/from16 v1, v30

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->span:I

    .line 1636
    move/from16 v0, v25

    move-object/from16 v1, v30

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->column:I

    .line 1637
    const/16 v20, 0x1

    goto/16 :goto_3

    .line 1657
    :cond_b
    move-object/from16 v0, v24

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->height:I

    move/from16 v35, v0

    const/high16 v36, 0x40000000    # 2.0f

    invoke-static/range {v35 .. v36}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    .restart local v15    # "heightSpec":I
    goto/16 :goto_4

    .line 1685
    .restart local v7    # "childHeight":I
    .restart local v16    # "i":I
    .restart local v22    # "lowest":I
    :cond_c
    move/from16 v32, v22

    .line 1695
    .end local v16    # "i":I
    .end local v22    # "lowest":I
    .local v32, "startFrom":I
    :goto_6
    add-int v10, v32, v21

    .line 1696
    .local v10, "childTop":I
    add-int v6, v10, v7

    .line 1697
    .local v6, "childBottom":I
    add-int v35, v11, v21

    mul-int v35, v35, v25

    add-int v8, v26, v35

    .line 1698
    .local v8, "childLeft":I
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v35

    add-int v9, v8, v35

    .line 1699
    .local v9, "childRight":I
    invoke-virtual {v5, v8, v10, v9, v6}, Landroid/view/View;->layout(IIII)V

    .line 1705
    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    .line 1706
    .local v29, "positionInt":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColMappings:Ljava/util/ArrayList;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/util/HashSet;

    move-object/from16 v0, v35

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v35

    if-nez v35, :cond_f

    .line 1711
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColMappings:Ljava/util/ArrayList;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :goto_7
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v35

    if-eqz v35, :cond_e

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/HashSet;

    .line 1712
    .local v12, "cols":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    move-object/from16 v0, v29

    invoke-virtual {v12, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_7

    .line 1687
    .end local v6    # "childBottom":I
    .end local v8    # "childLeft":I
    .end local v9    # "childRight":I
    .end local v10    # "childTop":I
    .end local v12    # "cols":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v29    # "positionInt":Ljava/lang/Integer;
    .end local v32    # "startFrom":I
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v35, v0

    aget v32, v35, v25

    .restart local v32    # "startFrom":I
    goto :goto_6

    .line 1716
    .restart local v6    # "childBottom":I
    .restart local v8    # "childLeft":I
    .restart local v9    # "childRight":I
    .restart local v10    # "childTop":I
    .restart local v17    # "i$":Ljava/util/Iterator;
    .restart local v29    # "positionInt":Ljava/lang/Integer;
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColMappings:Ljava/util/ArrayList;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/util/HashSet;

    move-object/from16 v0, v35

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1722
    .end local v17    # "i$":Ljava/util/Iterator;
    :cond_f
    move/from16 v16, v25

    .restart local v16    # "i":I
    :goto_8
    add-int v35, v25, v31

    move/from16 v0, v16

    move/from16 v1, v35

    if-ge v0, v1, :cond_10

    .line 1723
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v35, v0

    sub-int v36, v16, v25

    move-object/from16 v0, v30

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->getMarginBelow(I)I

    move-result v36

    add-int v36, v36, v6

    aput v36, v35, v16

    .line 1722
    add-int/lit8 v16, v16, 0x1

    goto :goto_8

    .line 1729
    :cond_10
    add-int/lit8 v28, v28, 0x1

    .line 1730
    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getNextColumnDown(I)I

    move-result v25

    .line 1731
    goto/16 :goto_0

    .line 1734
    .end local v5    # "child":Landroid/view/View;
    .end local v6    # "childBottom":I
    .end local v7    # "childHeight":I
    .end local v8    # "childLeft":I
    .end local v9    # "childRight":I
    .end local v10    # "childTop":I
    .end local v15    # "heightSpec":I
    .end local v16    # "i":I
    .end local v20    # "invalidateAfter":Z
    .end local v24    # "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    .end local v29    # "positionInt":Ljava/lang/Integer;
    .end local v30    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    .end local v31    # "span":I
    .end local v32    # "startFrom":I
    .end local v33    # "widthSize":I
    .end local v34    # "widthSpec":I
    :cond_11
    const/16 v23, 0x0

    .line 1735
    .local v23, "lowestView":I
    const/16 v16, 0x0

    .restart local v16    # "i":I
    :goto_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    move/from16 v35, v0

    move/from16 v0, v16

    move/from16 v1, v35

    if-ge v0, v1, :cond_13

    .line 1736
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v35, v0

    aget v35, v35, v16

    move/from16 v0, v35

    move/from16 v1, v23

    if-le v0, v1, :cond_12

    .line 1737
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v35, v0

    aget v23, v35, v16

    .line 1735
    :cond_12
    add-int/lit8 v16, v16, 0x1

    goto :goto_9

    .line 1740
    :cond_13
    sub-int v35, v23, v14

    return v35
.end method

.method final fillUp(II)I
    .locals 38
    .param p1, "fromPosition"    # I
    .param p2, "overhang"    # I

    .prologue
    .line 1342
    sget-boolean v35, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->DEBUG:Z

    if-eqz v35, :cond_0

    .line 1343
    const-string v35, "ClippedStaggeredGridView"

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "fillUp() fromPosition : "

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, ", overhang : "

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1346
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getPaddingLeft()I

    move-result v24

    .line 1347
    .local v24, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getPaddingRight()I

    move-result v25

    .line 1348
    .local v25, "paddingRight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemMargin:I

    move/from16 v20, v0

    .line 1349
    .local v20, "itemMargin":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getWidth()I

    move-result v35

    sub-int v35, v35, v24

    sub-int v35, v35, v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    move/from16 v36, v0

    add-int/lit8 v36, v36, -0x1

    mul-int v36, v36, v20

    sub-int v35, v35, v36

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    move/from16 v36, v0

    div-int v10, v35, v36

    .line 1351
    .local v10, "colWidth":I
    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColWidth:I

    .line 1352
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getPaddingTop()I

    move-result v12

    .line 1353
    .local v12, "gridTop":I
    sub-int v11, v12, p2

    .line 1354
    .local v11, "fillTo":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getNextColumnUp()I

    move-result v23

    .line 1355
    .local v23, "nextCol":I
    move/from16 v26, p1

    .local v26, "position":I
    move/from16 v27, v26

    .line 1358
    .end local v26    # "position":I
    .local v27, "position":I
    :cond_1
    :goto_0
    if-ltz v23, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    move-object/from16 v35, v0

    aget v35, v35, v23

    move/from16 v0, v35

    if-le v0, v11, :cond_12

    if-ltz v27, :cond_12

    .line 1361
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    .line 1362
    .local v28, "positionInt":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColMappings:Ljava/util/ArrayList;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/util/HashSet;

    move-object/from16 v0, v35

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v35

    if-nez v35, :cond_2

    .line 1363
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColMappings:Ljava/util/ArrayList;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Ljava/util/ArrayList;->size()I

    move-result v35

    move/from16 v0, v16

    move/from16 v1, v35

    if-ge v0, v1, :cond_2

    .line 1364
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColMappings:Ljava/util/ArrayList;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/util/HashSet;

    move-object/from16 v0, v35

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_a

    .line 1365
    move/from16 v23, v16

    .line 1375
    .end local v16    # "i":I
    :cond_2
    const/16 v35, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v27

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->obtainView(ILandroid/view/View;)Landroid/view/View;

    move-result-object v4

    .line 1378
    .local v4, "child":Landroid/view/View;
    if-eqz v4, :cond_1

    .line 1381
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v22

    check-cast v22, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    .line 1384
    .local v22, "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    if-nez v22, :cond_3

    .line 1385
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->generateDefaultLayoutParams()Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    move-result-object v22

    .line 1386
    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1390
    :cond_3
    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    if-eq v0, v1, :cond_4

    .line 1391
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mInLayout:Z

    move/from16 v35, v0

    if-eqz v35, :cond_b

    .line 1392
    const/16 v35, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v35

    move-object/from16 v2, v22

    invoke-virtual {v0, v4, v1, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 1399
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    move/from16 v35, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->span:I

    move/from16 v36, v0

    invoke-static/range {v35 .. v36}, Ljava/lang/Math;->min(II)I

    move-result v30

    .line 1400
    .local v30, "span":I
    mul-int v35, v10, v30

    add-int/lit8 v36, v30, -0x1

    mul-int v36, v36, v20

    add-int v33, v35, v36

    .line 1401
    .local v33, "widthSize":I
    const/high16 v35, 0x40000000    # 2.0f

    move/from16 v0, v33

    move/from16 v1, v35

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v34

    .line 1405
    .local v34, "widthSpec":I
    const/16 v35, 0x1

    move/from16 v0, v30

    move/from16 v1, v35

    if-le v0, v1, :cond_c

    .line 1406
    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getNextRecordUp(II)Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;

    move-result-object v29

    .line 1413
    .local v29, "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    :goto_3
    const/16 v17, 0x0

    .line 1414
    .local v17, "invalidateBefore":Z
    if-nez v29, :cond_d

    .line 1415
    new-instance v29, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;

    .end local v29    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    const/16 v35, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;)V

    .line 1416
    .restart local v29    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v27

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 1417
    move/from16 v0, v23

    move-object/from16 v1, v29

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->column:I

    .line 1418
    move/from16 v0, v30

    move-object/from16 v1, v29

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->span:I

    .line 1428
    :cond_5
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mHasStableIds:Z

    move/from16 v35, v0

    if-eqz v35, :cond_6

    .line 1429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v18

    .line 1430
    .local v18, "id":J
    move-wide/from16 v0, v18

    move-object/from16 v2, v29

    iput-wide v0, v2, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->id:J

    .line 1431
    move-wide/from16 v0, v18

    move-object/from16 v2, v22

    iput-wide v0, v2, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->id:J

    .line 1435
    .end local v18    # "id":J
    :cond_6
    move/from16 v0, v23

    move-object/from16 v1, v22

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->column:I

    .line 1439
    move-object/from16 v0, v22

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->height:I

    move/from16 v35, v0

    const/16 v36, -0x2

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_e

    .line 1440
    const/16 v35, 0x0

    const/16 v36, 0x0

    invoke-static/range {v35 .. v36}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    .line 1444
    .local v13, "heightSpec":I
    :goto_5
    move/from16 v0, v34

    invoke-virtual {v4, v0, v13}, Landroid/view/View;->measure(II)V

    .line 1447
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 1448
    .local v6, "childHeight":I
    if-nez v17, :cond_7

    move-object/from16 v0, v29

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->height:I

    move/from16 v35, v0

    move/from16 v0, v35

    if-eq v6, v0, :cond_8

    move-object/from16 v0, v29

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->height:I

    move/from16 v35, v0

    if-lez v35, :cond_8

    .line 1449
    :cond_7
    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->invalidateLayoutRecordsBeforePosition(I)V

    .line 1458
    :cond_8
    move-object/from16 v0, v29

    iput v6, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->height:I

    .line 1461
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    move-object/from16 v35, v0

    aget v21, v35, v23

    .line 1465
    .local v21, "itemTop":I
    const/16 v35, 0x1

    move/from16 v0, v30

    move/from16 v1, v35

    if-le v0, v1, :cond_10

    .line 1466
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    move-object/from16 v35, v0

    aget v14, v35, v23

    .line 1467
    .local v14, "highest":I
    add-int/lit8 v16, v23, 0x1

    .restart local v16    # "i":I
    :goto_6
    add-int v35, v23, v30

    move/from16 v0, v16

    move/from16 v1, v35

    if-ge v0, v1, :cond_f

    .line 1468
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    move-object/from16 v35, v0

    aget v32, v35, v16

    .line 1469
    .local v32, "top":I
    move/from16 v0, v32

    if-ge v0, v14, :cond_9

    .line 1470
    move/from16 v14, v32

    .line 1467
    :cond_9
    add-int/lit8 v16, v16, 0x1

    goto :goto_6

    .line 1363
    .end local v4    # "child":Landroid/view/View;
    .end local v6    # "childHeight":I
    .end local v13    # "heightSpec":I
    .end local v14    # "highest":I
    .end local v17    # "invalidateBefore":Z
    .end local v21    # "itemTop":I
    .end local v22    # "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    .end local v29    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    .end local v30    # "span":I
    .end local v32    # "top":I
    .end local v33    # "widthSize":I
    .end local v34    # "widthSpec":I
    :cond_a
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1

    .line 1394
    .end local v16    # "i":I
    .restart local v4    # "child":Landroid/view/View;
    .restart local v22    # "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    :cond_b
    const/16 v35, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->addView(Landroid/view/View;I)V

    goto/16 :goto_2

    .line 1409
    .restart local v30    # "span":I
    .restart local v33    # "widthSize":I
    .restart local v34    # "widthSpec":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;

    .restart local v29    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    goto/16 :goto_3

    .line 1419
    .restart local v17    # "invalidateBefore":Z
    :cond_d
    move-object/from16 v0, v29

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->span:I

    move/from16 v35, v0

    move/from16 v0, v30

    move/from16 v1, v35

    if-eq v0, v1, :cond_5

    .line 1420
    move/from16 v0, v30

    move-object/from16 v1, v29

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->span:I

    .line 1421
    move/from16 v0, v23

    move-object/from16 v1, v29

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->column:I

    .line 1422
    const/16 v17, 0x1

    goto/16 :goto_4

    .line 1442
    :cond_e
    move-object/from16 v0, v22

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->height:I

    move/from16 v35, v0

    const/high16 v36, 0x40000000    # 2.0f

    invoke-static/range {v35 .. v36}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    .restart local v13    # "heightSpec":I
    goto/16 :goto_5

    .line 1473
    .restart local v6    # "childHeight":I
    .restart local v14    # "highest":I
    .restart local v16    # "i":I
    .restart local v21    # "itemTop":I
    :cond_f
    move/from16 v31, v14

    .line 1481
    .end local v14    # "highest":I
    .end local v16    # "i":I
    .local v31, "startFrom":I
    :goto_7
    move/from16 v5, v31

    .line 1482
    .local v5, "childBottom":I
    sub-int v9, v5, v6

    .line 1483
    .local v9, "childTop":I
    add-int v35, v10, v20

    mul-int v35, v35, v23

    add-int v7, v24, v35

    .line 1484
    .local v7, "childLeft":I
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v35

    add-int v8, v7, v35

    .line 1495
    .local v8, "childRight":I
    invoke-virtual {v4, v7, v9, v8, v5}, Landroid/view/View;->layout(IIII)V

    .line 1500
    move/from16 v16, v23

    .restart local v16    # "i":I
    :goto_8
    add-int v35, v23, v30

    move/from16 v0, v16

    move/from16 v1, v35

    if-ge v0, v1, :cond_11

    .line 1501
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    move-object/from16 v35, v0

    sub-int v36, v16, v23

    move-object/from16 v0, v29

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->getMarginAbove(I)I

    move-result v36

    sub-int v36, v9, v36

    sub-int v36, v36, v20

    aput v36, v35, v16

    .line 1500
    add-int/lit8 v16, v16, 0x1

    goto :goto_8

    .line 1475
    .end local v5    # "childBottom":I
    .end local v7    # "childLeft":I
    .end local v8    # "childRight":I
    .end local v9    # "childTop":I
    .end local v16    # "i":I
    .end local v31    # "startFrom":I
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    move-object/from16 v35, v0

    aget v31, v35, v23

    .restart local v31    # "startFrom":I
    goto :goto_7

    .line 1505
    .restart local v5    # "childBottom":I
    .restart local v7    # "childLeft":I
    .restart local v8    # "childRight":I
    .restart local v9    # "childTop":I
    .restart local v16    # "i":I
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getNextColumnUp()I

    move-result v23

    .line 1506
    add-int/lit8 v26, v27, -0x1

    .end local v27    # "position":I
    .restart local v26    # "position":I
    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    move/from16 v27, v26

    .line 1507
    .end local v26    # "position":I
    .restart local v27    # "position":I
    goto/16 :goto_0

    .line 1510
    .end local v4    # "child":Landroid/view/View;
    .end local v5    # "childBottom":I
    .end local v6    # "childHeight":I
    .end local v7    # "childLeft":I
    .end local v8    # "childRight":I
    .end local v9    # "childTop":I
    .end local v13    # "heightSpec":I
    .end local v16    # "i":I
    .end local v17    # "invalidateBefore":Z
    .end local v21    # "itemTop":I
    .end local v22    # "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    .end local v28    # "positionInt":Ljava/lang/Integer;
    .end local v29    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    .end local v30    # "span":I
    .end local v31    # "startFrom":I
    .end local v33    # "widthSize":I
    .end local v34    # "widthSpec":I
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getHeight()I

    move-result v15

    .line 1513
    .local v15, "highestView":I
    const/16 v16, 0x0

    .restart local v16    # "i":I
    :goto_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    move/from16 v35, v0

    move/from16 v0, v16

    move/from16 v1, v35

    if-ge v0, v1, :cond_15

    .line 1514
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getFirstChildAtColumn(I)Landroid/view/View;

    move-result-object v4

    .line 1517
    .restart local v4    # "child":Landroid/view/View;
    if-nez v4, :cond_14

    .line 1513
    :cond_13
    :goto_a
    add-int/lit8 v16, v16, 0x1

    goto :goto_9

    .line 1520
    :cond_14
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v22

    check-cast v22, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    .line 1524
    .restart local v22    # "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v32

    .line 1527
    .restart local v32    # "top":I
    move/from16 v0, v32

    if-ge v0, v15, :cond_13

    .line 1528
    move/from16 v15, v32

    goto :goto_a

    .line 1533
    .end local v4    # "child":Landroid/view/View;
    .end local v22    # "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    .end local v32    # "top":I
    :cond_15
    sub-int v35, v12, v15

    return v35
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->generateDefaultLayoutParams()Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    .locals 2

    .prologue
    .line 2066
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;-><init>(I)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 2084
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "x0"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 78
    invoke-virtual {p0, p1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    .locals 1
    .param p1, "lp"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 2072
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    invoke-direct {v0, p1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 1973
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getColumnCount()I
    .locals 1

    .prologue
    .line 420
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    return v0
.end method

.method protected getContextMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 2800
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method public getFirstPosition()I
    .locals 1

    .prologue
    .line 456
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    return v0
.end method

.method public getItemBottom(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 3096
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    if-lt p1, v0, :cond_0

    .line 3097
    const/4 v0, -0x1

    .line 3099
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method public getItemTop(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 3088
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    if-lt p1, v0, :cond_0

    .line 3089
    const/4 v0, -0x1

    .line 3091
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method final getNextColumnDown(I)I
    .locals 8
    .param p1, "position"    # I

    .prologue
    .line 1842
    sget-boolean v5, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->DEBUG:Z

    if-eqz v5, :cond_0

    .line 1843
    const-string v5, "ClippedStaggeredGridView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getNextColumnDown() position : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1846
    :cond_0
    const/4 v3, -0x1

    .line 1847
    .local v3, "result":I
    const v4, 0x7fffffff

    .line 1850
    .local v4, "topMost":I
    iget v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    .line 1853
    .local v1, "colCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 1854
    iget-object v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    aget v0, v5, v2

    .line 1855
    .local v0, "bottom":I
    if-ge v0, v4, :cond_1

    .line 1856
    move v4, v0

    .line 1857
    move v3, v2

    .line 1853
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1862
    .end local v0    # "bottom":I
    :cond_2
    return v3
.end method

.method final getNextColumnUp()I
    .locals 6

    .prologue
    .line 1773
    const/4 v3, -0x1

    .line 1774
    .local v3, "result":I
    const/high16 v0, -0x80000000

    .line 1777
    .local v0, "bottomMost":I
    iget v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    .line 1778
    .local v1, "colCount":I
    add-int/lit8 v2, v1, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_1

    .line 1779
    iget-object v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    aget v4, v5, v2

    .line 1780
    .local v4, "top":I
    if-le v4, v0, :cond_0

    .line 1781
    move v0, v4

    .line 1782
    move v3, v2

    .line 1778
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 1785
    .end local v4    # "top":I
    :cond_1
    return v3
.end method

.method final getNextRecordDown(II)Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    .locals 11
    .param p1, "position"    # I
    .param p2, "span"    # I

    .prologue
    .line 1867
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v8, p1}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;

    .line 1868
    .local v4, "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    if-nez v4, :cond_2

    .line 1869
    new-instance v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;

    .end local v4    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    const/4 v8, 0x0

    invoke-direct {v4, v8}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;)V

    .line 1870
    .restart local v4    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    iput p2, v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->span:I

    .line 1871
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v8, p1, v4}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 1876
    :cond_0
    const/4 v6, -0x1

    .line 1877
    .local v6, "targetCol":I
    const v7, 0x7fffffff

    .line 1880
    .local v7, "topMost":I
    iget v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    .line 1881
    .local v1, "colCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sub-int v8, v1, p2

    if-gt v2, v8, :cond_5

    .line 1882
    const/high16 v0, -0x80000000

    .line 1883
    .local v0, "bottom":I
    move v3, v2

    .local v3, "j":I
    :goto_1
    add-int v8, v2, p2

    if-ge v3, v8, :cond_3

    .line 1884
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    aget v5, v8, v3

    .line 1885
    .local v5, "singleBottom":I
    if-le v5, v0, :cond_1

    .line 1886
    move v0, v5

    .line 1883
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1872
    .end local v0    # "bottom":I
    .end local v1    # "colCount":I
    .end local v2    # "i":I
    .end local v3    # "j":I
    .end local v5    # "singleBottom":I
    .end local v6    # "targetCol":I
    .end local v7    # "topMost":I
    :cond_2
    iget v8, v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->span:I

    if-eq v8, p2, :cond_0

    .line 1873
    new-instance v8, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Invalid LayoutRecord! Record had span="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->span:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " but caller requested span="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " for position="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1889
    .restart local v0    # "bottom":I
    .restart local v1    # "colCount":I
    .restart local v2    # "i":I
    .restart local v3    # "j":I
    .restart local v6    # "targetCol":I
    .restart local v7    # "topMost":I
    :cond_3
    if-ge v0, v7, :cond_4

    .line 1890
    move v7, v0

    .line 1891
    move v6, v2

    .line 1881
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1896
    .end local v0    # "bottom":I
    .end local v3    # "j":I
    :cond_5
    iput v6, v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->column:I

    .line 1899
    const/4 v2, 0x0

    :goto_2
    if-ge v2, p2, :cond_6

    .line 1900
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    add-int v9, v2, v6

    aget v8, v8, v9

    sub-int v8, v7, v8

    invoke-virtual {v4, v2, v8}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->setMarginAbove(II)V

    .line 1899
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1904
    :cond_6
    return-object v4
.end method

.method final getNextRecordUp(II)Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    .locals 11
    .param p1, "position"    # I
    .param p2, "span"    # I

    .prologue
    .line 1796
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v8, p1}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;

    .line 1797
    .local v4, "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    if-nez v4, :cond_2

    .line 1798
    new-instance v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;

    .end local v4    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    const/4 v8, 0x0

    invoke-direct {v4, v8}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;)V

    .line 1799
    .restart local v4    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    iput p2, v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->span:I

    .line 1800
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v8, p1, v4}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 1805
    :cond_0
    const/4 v6, -0x1

    .line 1806
    .local v6, "targetCol":I
    const/high16 v0, -0x80000000

    .line 1809
    .local v0, "bottomMost":I
    iget v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    .line 1810
    .local v1, "colCount":I
    sub-int v2, v1, p2

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_5

    .line 1811
    const v7, 0x7fffffff

    .line 1812
    .local v7, "top":I
    move v3, v2

    .local v3, "j":I
    :goto_1
    add-int v8, v2, p2

    if-ge v3, v8, :cond_3

    .line 1813
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    aget v5, v8, v3

    .line 1814
    .local v5, "singleTop":I
    if-ge v5, v7, :cond_1

    .line 1815
    move v7, v5

    .line 1812
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1801
    .end local v0    # "bottomMost":I
    .end local v1    # "colCount":I
    .end local v2    # "i":I
    .end local v3    # "j":I
    .end local v5    # "singleTop":I
    .end local v6    # "targetCol":I
    .end local v7    # "top":I
    :cond_2
    iget v8, v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->span:I

    if-eq v8, p2, :cond_0

    .line 1802
    new-instance v8, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Invalid LayoutRecord! Record had span="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->span:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " but caller requested span="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " for position="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1818
    .restart local v0    # "bottomMost":I
    .restart local v1    # "colCount":I
    .restart local v2    # "i":I
    .restart local v3    # "j":I
    .restart local v6    # "targetCol":I
    .restart local v7    # "top":I
    :cond_3
    if-le v7, v0, :cond_4

    .line 1819
    move v0, v7

    .line 1820
    move v6, v2

    .line 1810
    :cond_4
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 1825
    .end local v3    # "j":I
    .end local v7    # "top":I
    :cond_5
    iput v6, v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->column:I

    .line 1828
    const/4 v2, 0x0

    :goto_2
    if-ge v2, p2, :cond_6

    .line 1829
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    add-int v9, v2, v6

    aget v8, v8, v9

    sub-int/2addr v8, v0

    invoke-virtual {v4, v2, v8}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->setMarginBelow(II)V

    .line 1828
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1833
    :cond_6
    return-object v4
.end method

.method public final getOnItemClickListener()Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemClickListener;
    .locals 1

    .prologue
    .line 2982
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mOnItemClickListener:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemClickListener;

    return-object v0
.end method

.method public final getOnItemLongClickListener()Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 3025
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mOnItemLongClickListener:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemLongClickListener;

    return-object v0
.end method

.method public getSelector()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 2864
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method hideSelector()V
    .locals 2

    .prologue
    .line 504
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectorPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 507
    :cond_0
    return-void
.end method

.method final invalidateLayoutRecordsAfterPosition(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 1325
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v1}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .line 1326
    .local v0, "beginAt":I
    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v1, v0}, Landroid/support/v4/util/SparseArrayCompat;->keyAt(I)I

    move-result v1

    if-le v1, p1, :cond_0

    .line 1327
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1329
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 1330
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v3}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v3

    sub-int/2addr v3, v0

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/util/SparseArrayCompat;->removeAtRange(II)V

    .line 1331
    return-void
.end method

.method final invalidateLayoutRecordsBeforePosition(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 1316
    const/4 v0, 0x0

    .line 1317
    .local v0, "endAt":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v1}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v1, v0}, Landroid/support/v4/util/SparseArrayCompat;->keyAt(I)I

    move-result v1

    if-ge v1, p1, :cond_0

    .line 1318
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1320
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/util/SparseArrayCompat;->removeAtRange(II)V

    .line 1321
    return-void
.end method

.method public isDrawSelectorOnTop()Z
    .locals 1

    .prologue
    .line 3079
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDrawSelectorOnTop:Z

    return v0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 2889
    invoke-super {p0}, Landroid/view/ViewGroup;->jumpDrawablesToCurrentState()V

    .line 2890
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 2891
    :cond_0
    return-void
.end method

.method final layoutChildren(Z)V
    .locals 34
    .param p1, "queryAdapter"    # Z

    .prologue
    .line 1160
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getPaddingLeft()I

    move-result v23

    .line 1161
    .local v23, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getPaddingRight()I

    move-result v24

    .line 1162
    .local v24, "paddingRight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemMargin:I

    move/from16 v17, v0

    .line 1163
    .local v17, "itemMargin":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getWidth()I

    move-result v32

    sub-int v32, v32, v23

    sub-int v32, v32, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    move/from16 v33, v0

    add-int/lit8 v33, v33, -0x1

    mul-int v33, v33, v17

    sub-int v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    move/from16 v33, v0

    div-int v14, v32, v33

    .line 1164
    .local v14, "colWidth":I
    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColWidth:I

    .line 1165
    const/16 v27, -0x1

    .line 1166
    .local v27, "rebuildLayoutRecordsBefore":I
    const/16 v26, -0x1

    .line 1169
    .local v26, "rebuildLayoutRecordsAfter":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v32, v0

    const/high16 v33, -0x80000000

    invoke-static/range {v32 .. v33}, Ljava/util/Arrays;->fill([II)V

    .line 1172
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildCount()I

    move-result v8

    .line 1173
    .local v8, "childCount":I
    const/4 v4, 0x0

    .line 1176
    .local v4, "amountRemoved":I
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_0
    move/from16 v0, v16

    if-ge v0, v8, :cond_f

    .line 1177
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1178
    .local v6, "child":Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    check-cast v20, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    .line 1179
    .local v20, "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    move-object/from16 v0, v20

    iget v13, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->column:I

    .line 1180
    .local v13, "col":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    move/from16 v32, v0

    add-int v25, v32, v16

    .line 1181
    .local v25, "position":I
    if-nez p1, :cond_0

    invoke-virtual {v6}, Landroid/view/View;->isLayoutRequested()Z

    move-result v32

    if-eqz v32, :cond_3

    :cond_0
    const/16 v21, 0x1

    .line 1183
    .local v21, "needsLayout":Z
    :goto_1
    if-eqz p1, :cond_6

    .line 1186
    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1, v6}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->obtainView(ILandroid/view/View;)Landroid/view/View;

    move-result-object v22

    .line 1187
    .local v22, "newView":Landroid/view/View;
    if-nez v22, :cond_4

    .line 1189
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->removeViewAt(I)V

    .line 1190
    add-int/lit8 v32, v16, -0x1

    if-ltz v32, :cond_1

    add-int/lit8 v32, v16, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->invalidateLayoutRecordsAfterPosition(I)V

    .line 1191
    :cond_1
    add-int/lit8 v4, v4, 0x1

    .line 1176
    .end local v22    # "newView":Landroid/view/View;
    :cond_2
    :goto_2
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 1181
    .end local v21    # "needsLayout":Z
    :cond_3
    const/16 v21, 0x0

    goto :goto_1

    .line 1193
    .restart local v21    # "needsLayout":Z
    .restart local v22    # "newView":Landroid/view/View;
    :cond_4
    move-object/from16 v0, v22

    if-eq v0, v6, :cond_5

    .line 1194
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->removeViewAt(I)V

    .line 1195
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->addView(Landroid/view/View;I)V

    .line 1196
    move-object/from16 v6, v22

    .line 1198
    :cond_5
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    .end local v20    # "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    check-cast v20, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    .line 1202
    .end local v22    # "newView":Landroid/view/View;
    .restart local v20    # "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    move/from16 v32, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->span:I

    move/from16 v33, v0

    invoke-static/range {v32 .. v33}, Ljava/lang/Math;->min(II)I

    move-result v29

    .line 1203
    .local v29, "span":I
    mul-int v32, v14, v29

    add-int/lit8 v33, v29, -0x1

    mul-int v33, v33, v17

    add-int v30, v32, v33

    .line 1206
    .local v30, "widthSize":I
    if-eqz v21, :cond_7

    .line 1207
    const/high16 v32, 0x40000000    # 2.0f

    move/from16 v0, v30

    move/from16 v1, v32

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v31

    .line 1211
    .local v31, "widthSpec":I
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->height:I

    move/from16 v32, v0

    const/16 v33, -0x2

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_9

    .line 1212
    const/16 v32, 0x0

    const/16 v33, 0x0

    invoke-static/range {v32 .. v33}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    .line 1218
    .local v15, "heightSpec":I
    :goto_3
    move/from16 v0, v31

    invoke-virtual {v6, v0, v15}, Landroid/view/View;->measure(II)V

    .line 1222
    .end local v15    # "heightSpec":I
    .end local v31    # "widthSpec":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v32, v0

    aget v32, v32, v13

    const/high16 v33, -0x80000000

    move/from16 v0, v32

    move/from16 v1, v33

    if-le v0, v1, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v32, v0

    aget v32, v32, v13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemMargin:I

    move/from16 v33, v0

    add-int v12, v32, v33

    .line 1225
    .local v12, "childTop":I
    :goto_4
    const/16 v32, 0x1

    move/from16 v0, v29

    move/from16 v1, v32

    if-le v0, v1, :cond_c

    .line 1226
    move/from16 v19, v12

    .line 1227
    .local v19, "lowest":I
    add-int/lit8 v18, v13, 0x1

    .local v18, "j":I
    :goto_5
    add-int v32, v13, v29

    move/from16 v0, v18

    move/from16 v1, v32

    if-ge v0, v1, :cond_b

    .line 1228
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v32, v0

    aget v32, v32, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemMargin:I

    move/from16 v33, v0

    add-int v5, v32, v33

    .line 1229
    .local v5, "bottom":I
    move/from16 v0, v19

    if-le v5, v0, :cond_8

    .line 1230
    move/from16 v19, v5

    .line 1227
    :cond_8
    add-int/lit8 v18, v18, 0x1

    goto :goto_5

    .line 1214
    .end local v5    # "bottom":I
    .end local v12    # "childTop":I
    .end local v18    # "j":I
    .end local v19    # "lowest":I
    .restart local v31    # "widthSpec":I
    :cond_9
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->height:I

    move/from16 v32, v0

    const/high16 v33, 0x40000000    # 2.0f

    invoke-static/range {v32 .. v33}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    .restart local v15    # "heightSpec":I
    goto :goto_3

    .line 1222
    .end local v15    # "heightSpec":I
    .end local v31    # "widthSpec":I
    :cond_a
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v12

    goto :goto_4

    .line 1233
    .restart local v12    # "childTop":I
    .restart local v18    # "j":I
    .restart local v19    # "lowest":I
    :cond_b
    move/from16 v12, v19

    .line 1236
    .end local v18    # "j":I
    .end local v19    # "lowest":I
    :cond_c
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    .line 1244
    .local v9, "childHeight":I
    add-int v7, v12, v9

    .line 1245
    .local v7, "childBottom":I
    add-int v32, v14, v17

    mul-int v32, v32, v13

    add-int v10, v23, v32

    .line 1246
    .local v10, "childLeft":I
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v32

    add-int v11, v10, v32

    .line 1247
    .local v11, "childRight":I
    invoke-virtual {v6, v10, v12, v11, v7}, Landroid/view/View;->layout(IIII)V

    .line 1250
    move/from16 v18, v13

    .restart local v18    # "j":I
    :goto_6
    add-int v32, v13, v29

    move/from16 v0, v18

    move/from16 v1, v32

    if-ge v0, v1, :cond_d

    .line 1251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v32, v0

    aput v7, v32, v18

    .line 1250
    add-int/lit8 v18, v18, 0x1

    goto :goto_6

    .line 1255
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;

    .line 1256
    .local v28, "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    if-eqz v28, :cond_e

    move-object/from16 v0, v28

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->height:I

    move/from16 v32, v0

    move/from16 v0, v32

    if-eq v0, v9, :cond_e

    .line 1258
    move-object/from16 v0, v28

    iput v9, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->height:I

    .line 1259
    move/from16 v27, v25

    .line 1263
    :cond_e
    if-eqz v28, :cond_2

    move-object/from16 v0, v28

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->span:I

    move/from16 v32, v0

    move/from16 v0, v32

    move/from16 v1, v29

    if-eq v0, v1, :cond_2

    .line 1265
    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->span:I

    .line 1266
    move/from16 v26, v25

    goto/16 :goto_2

    .line 1272
    .end local v6    # "child":Landroid/view/View;
    .end local v7    # "childBottom":I
    .end local v9    # "childHeight":I
    .end local v10    # "childLeft":I
    .end local v11    # "childRight":I
    .end local v12    # "childTop":I
    .end local v13    # "col":I
    .end local v18    # "j":I
    .end local v20    # "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    .end local v21    # "needsLayout":Z
    .end local v25    # "position":I
    .end local v28    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    .end local v29    # "span":I
    .end local v30    # "widthSize":I
    :cond_f
    const/16 v16, 0x0

    :goto_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    move/from16 v32, v0

    move/from16 v0, v16

    move/from16 v1, v32

    if-ge v0, v1, :cond_11

    .line 1273
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v32, v0

    aget v32, v32, v16

    const/high16 v33, -0x80000000

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_10

    .line 1274
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    move-object/from16 v33, v0

    aget v33, v33, v16

    aput v33, v32, v16

    .line 1272
    :cond_10
    add-int/lit8 v16, v16, 0x1

    goto :goto_7

    .line 1279
    :cond_11
    if-gez v27, :cond_12

    if-ltz v26, :cond_16

    .line 1280
    :cond_12
    if-ltz v27, :cond_13

    .line 1281
    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->invalidateLayoutRecordsBeforePosition(I)V

    .line 1283
    :cond_13
    if-ltz v26, :cond_14

    .line 1284
    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->invalidateLayoutRecordsAfterPosition(I)V

    .line 1286
    :cond_14
    const/16 v16, 0x0

    :goto_8
    sub-int v32, v8, v4

    move/from16 v0, v16

    move/from16 v1, v32

    if-ge v0, v1, :cond_16

    .line 1287
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    move/from16 v32, v0

    add-int v25, v32, v16

    .line 1288
    .restart local v25    # "position":I
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1289
    .restart local v6    # "child":Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    check-cast v20, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    .line 1290
    .restart local v20    # "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;

    .line 1291
    .restart local v28    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    if-nez v28, :cond_15

    .line 1292
    new-instance v28, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;

    .end local v28    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    const/16 v32, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;)V

    .line 1293
    .restart local v28    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v25

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 1295
    :cond_15
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->column:I

    move/from16 v32, v0

    move/from16 v0, v32

    move-object/from16 v1, v28

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->column:I

    .line 1296
    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v32

    move/from16 v0, v32

    move-object/from16 v1, v28

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->height:I

    .line 1297
    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->id:J

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    move-object/from16 v2, v28

    iput-wide v0, v2, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->id:J

    .line 1298
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    move/from16 v32, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->span:I

    move/from16 v33, v0

    invoke-static/range {v32 .. v33}, Ljava/lang/Math;->min(II)I

    move-result v32

    move/from16 v0, v32

    move-object/from16 v1, v28

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;->span:I

    .line 1286
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_8

    .line 1303
    .end local v6    # "child":Landroid/view/View;
    .end local v20    # "lp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    .end local v25    # "position":I
    .end local v28    # "rec":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutRecord;
    :cond_16
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectorPosition:I

    move/from16 v32, v0

    const/16 v33, -0x1

    move/from16 v0, v32

    move/from16 v1, v33

    if-eq v0, v1, :cond_18

    .line 1304
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMotionPosition:I

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    move/from16 v33, v0

    sub-int v32, v32, v33

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1305
    .restart local v6    # "child":Landroid/view/View;
    if-eqz v6, :cond_17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMotionPosition:I

    move/from16 v32, v0

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1, v6}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->positionSelector(ILandroid/view/View;)V

    .line 1312
    .end local v6    # "child":Landroid/view/View;
    :cond_17
    :goto_9
    return-void

    .line 1306
    :cond_18
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    move/from16 v32, v0

    const/16 v33, 0x3

    move/from16 v0, v32

    move/from16 v1, v33

    if-le v0, v1, :cond_19

    .line 1307
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMotionPosition:I

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    move/from16 v33, v0

    sub-int v32, v32, v33

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1308
    .restart local v6    # "child":Landroid/view/View;
    if-eqz v6, :cond_17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMotionPosition:I

    move/from16 v32, v0

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1, v6}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->positionSelector(ILandroid/view/View;)V

    goto :goto_9

    .line 1310
    .end local v6    # "child":Landroid/view/View;
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectorRect:Landroid/graphics/Rect;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/graphics/Rect;->setEmpty()V

    goto :goto_9
.end method

.method final obtainView(ILandroid/view/View;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "optScrap"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    .line 1917
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRecycler:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;

    invoke-virtual {v6, p1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;->getTransientStateView(I)Landroid/view/View;

    move-result-object v5

    .line 1918
    .local v5, "view":Landroid/view/View;
    if-eqz v5, :cond_0

    move-object v6, v5

    .line 1968
    :goto_0
    return-object v6

    .line 1923
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v6}, Landroid/widget/ListAdapter;->getCount()I

    move-result v6

    if-lt p1, v6, :cond_1

    .line 1924
    const/4 v5, 0x0

    move-object v6, v7

    .line 1925
    goto :goto_0

    .line 1930
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    iget v1, v6, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->viewType:I

    .line 1932
    .local v1, "optType":I
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v6, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v2

    .line 1933
    .local v2, "positionViewType":I
    if-ne v1, v2, :cond_3

    move-object v3, p2

    .line 1937
    .local v3, "scrap":Landroid/view/View;
    :goto_2
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v6, p1, v3, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 1939
    if-nez v5, :cond_4

    move-object v6, v7

    .line 1940
    goto :goto_0

    .line 1930
    .end local v1    # "optType":I
    .end local v2    # "positionViewType":I
    .end local v3    # "scrap":Landroid/view/View;
    :cond_2
    const/4 v1, -0x1

    goto :goto_1

    .line 1933
    .restart local v1    # "optType":I
    .restart local v2    # "positionViewType":I
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRecycler:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;

    invoke-virtual {v6, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;->getScrapView(I)Landroid/view/View;

    move-result-object v3

    goto :goto_2

    .line 1943
    .restart local v3    # "scrap":Landroid/view/View;
    :cond_4
    if-eq v5, v3, :cond_5

    if-eqz v3, :cond_5

    .line 1945
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRecycler:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;

    invoke-virtual {v6, v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;->addScrap(Landroid/view/View;)V

    .line 1949
    :cond_5
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1952
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    if-eq v6, p0, :cond_6

    .line 1953
    if-nez v0, :cond_7

    .line 1954
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->generateDefaultLayoutParams()Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    move-result-object v0

    :cond_6
    :goto_3
    move-object v4, v0

    .line 1961
    check-cast v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    .line 1962
    .local v4, "sglp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    iput p1, v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->position:I

    .line 1963
    iput v2, v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;->viewType:I

    .line 1967
    invoke-virtual {v5, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v6, v5

    .line 1968
    goto :goto_0

    .line 1955
    .end local v4    # "sglp":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;
    :cond_7
    invoke-virtual {p0, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 1956
    invoke-virtual {p0, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$LayoutParams;

    move-result-object v0

    goto :goto_3
.end method

.method final offsetChildren(I)V
    .locals 8
    .param p1, "offset"    # I

    .prologue
    .line 1138
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildCount()I

    move-result v1

    .line 1139
    .local v1, "childCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 1140
    invoke-virtual {p0, v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1141
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    add-int/2addr v5, p1

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v6

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v7

    add-int/2addr v7, p1

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    .line 1139
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1146
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    iget v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    .line 1147
    .local v2, "colCount":I
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_1

    .line 1148
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I

    aget v5, v4, v3

    add-int/2addr v5, p1

    aput v5, v4, v3

    .line 1149
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I

    aget v5, v4, v3

    add-int/2addr v5, p1

    aput v5, v4, v3

    .line 1147
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1151
    :cond_1
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 6
    .param p1, "extraSpace"    # I

    .prologue
    .line 2609
    iget-boolean v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mIsChildViewEnabled:Z

    if-eqz v4, :cond_1

    .line 2611
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onCreateDrawableState(I)[I

    move-result-object v3

    .line 2641
    :cond_0
    :goto_0
    return-object v3

    .line 2618
    :cond_1
    sget-object v4, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->ENABLED_STATE_SET:[I

    const/4 v5, 0x0

    aget v1, v4, v5

    .line 2624
    .local v1, "enabledState":I
    add-int/lit8 v4, p1, 0x1

    invoke-super {p0, v4}, Landroid/view/ViewGroup;->onCreateDrawableState(I)[I

    move-result-object v3

    .line 2625
    .local v3, "state":[I
    const/4 v0, -0x1

    .line 2626
    .local v0, "enabledPos":I
    array-length v4, v3

    add-int/lit8 v2, v4, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_2

    .line 2627
    aget v4, v3, v2

    if-ne v4, v1, :cond_3

    .line 2628
    move v0, v2

    .line 2635
    :cond_2
    if-ltz v0, :cond_0

    .line 2636
    add-int/lit8 v4, v0, 0x1

    array-length v5, v3

    sub-int/2addr v5, v0

    add-int/lit8 v5, v5, -0x1

    invoke-static {v3, v4, v3, v0, v5}, Ljava/lang/System;->arraycopy([II[III)V

    goto :goto_0

    .line 2626
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_1
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 462
    iget-object v7, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v7, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 463
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    and-int/lit16 v0, v7, 0xff

    .line 464
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v5, v6

    .line 499
    :goto_0
    return v5

    .line 466
    :pswitch_1
    iget-object v7, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v7}, Landroid/view/VelocityTracker;->clear()V

    .line 467
    iget-object v7, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->abortAnimation()V

    .line 468
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iput v7, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLastTouchY:F

    .line 469
    invoke-static {p1, v6}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v7

    iput v7, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mActivePointerId:I

    .line 470
    const/4 v7, 0x0

    iput v7, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchRemainderY:F

    .line 471
    iget v7, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_0

    .line 473
    iput v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    goto :goto_0

    .line 480
    :pswitch_2
    iget v7, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mActivePointerId:I

    invoke-static {p1, v7}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v3

    .line 481
    .local v3, "index":I
    if-gez v3, :cond_1

    .line 482
    const-string v5, "ClippedStaggeredGridView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onInterceptTouchEvent could not find pointer with id "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mActivePointerId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " - did StaggeredGridView receive an inconsistent "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "event stream?"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v6

    .line 485
    goto :goto_0

    .line 487
    :cond_1
    invoke-static {p1, v3}, Landroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 488
    .local v4, "y":F
    iget v7, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLastTouchY:F

    sub-float v7, v4, v7

    iget v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchRemainderY:F

    add-float v2, v7, v8

    .line 489
    .local v2, "dy":F
    float-to-int v1, v2

    .line 490
    .local v1, "deltaY":I
    int-to-float v7, v1

    sub-float v7, v2, v7

    iput v7, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchRemainderY:F

    .line 492
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v7

    iget v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchSlop:I

    int-to-float v8, v8

    cmpl-float v7, v7, v8

    if-lez v7, :cond_0

    .line 493
    iput v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    goto :goto_0

    .line 464
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v5, 0x0

    .line 1048
    sget-boolean v2, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 1049
    const-string v2, "ClippedStaggeredGridView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onLayout() changed : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", l : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", t : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", r : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", b : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1051
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mInLayout:Z

    .line 1052
    invoke-direct {p0, v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->populate(Z)V

    .line 1053
    iput-boolean v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mInLayout:Z

    .line 1056
    sub-int v1, p4, p2

    .line 1057
    .local v1, "width":I
    sub-int v0, p5, p3

    .line 1058
    .local v0, "height":I
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v2, v1, v0}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 1059
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v2, v1, v0}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 1060
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 1020
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 1021
    .local v3, "widthMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 1022
    .local v1, "heightMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 1023
    .local v4, "widthSize":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 1026
    .local v2, "heightSize":I
    if-eq v3, v5, :cond_0

    .line 1027
    const/high16 v3, 0x40000000    # 2.0f

    .line 1029
    :cond_0
    if-eq v1, v5, :cond_1

    .line 1030
    const/high16 v1, 0x40000000    # 2.0f

    .line 1034
    :cond_1
    invoke-virtual {p0, v4, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setMeasuredDimension(II)V

    .line 1037
    iget v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCountSetting:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_2

    .line 1038
    iget v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMinColWidth:I

    div-int v0, v4, v5

    .line 1039
    .local v0, "colCount":I
    iget v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    if-eq v0, v5, :cond_2

    .line 1040
    iput v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    .line 1043
    .end local v0    # "colCount":I
    :cond_2
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 8
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 2147
    move-object v3, p1

    check-cast v3, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SavedState;

    .line 2148
    .local v3, "ss":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SavedState;
    invoke-virtual {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v4

    invoke-super {p0, v4}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 2149
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDataChanged:Z

    .line 2150
    iget v4, v3, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SavedState;->position:I

    iput v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    .line 2151
    iget-object v4, v3, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SavedState;->topOffsets:[I

    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRestoreOffsets:[I

    .line 2154
    iget-object v1, v3, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SavedState;->mapping:Ljava/util/ArrayList;

    .line 2157
    .local v1, "convert":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$ColMap;>;"
    if-eqz v1, :cond_0

    .line 2158
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColMappings:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 2159
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$ColMap;

    .line 2160
    .local v0, "colMap":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$ColMap;
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColMappings:Ljava/util/ArrayList;

    new-instance v5, Ljava/util/HashSet;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$ColMap;->values:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$ColMap;->access$600(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$ColMap;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2165
    .end local v0    # "colMap":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$ColMap;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    iget-wide v4, v3, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SavedState;->firstId:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    .line 2166
    iget-wide v4, v3, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SavedState;->firstId:J

    iput-wide v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstAdapterId:J

    .line 2167
    const/4 v4, -0x1

    iput v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectorPosition:I

    .line 2171
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->requestLayout()V

    .line 2172
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 14

    .prologue
    .line 2090
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v9

    .line 2091
    .local v9, "superState":Landroid/os/Parcelable;
    new-instance v8, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SavedState;

    invoke-direct {v8, v9}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 2092
    .local v8, "ss":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SavedState;
    iget v7, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    .line 2093
    .local v7, "position":I
    iget v11, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    iput v11, v8, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SavedState;->position:I

    .line 2096
    if-ltz v7, :cond_0

    iget-object v11, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v11, :cond_0

    iget-object v11, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v11}, Landroid/widget/ListAdapter;->getCount()I

    move-result v11

    if-ge v7, v11, :cond_0

    .line 2097
    iget-object v11, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v11, v7}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v12

    iput-wide v12, v8, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SavedState;->firstId:J

    .line 2101
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildCount()I

    move-result v11

    if-lez v11, :cond_5

    .line 2104
    iget v11, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    new-array v10, v11, [I

    .line 2107
    .local v10, "topOffsets":[I
    iget v11, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColWidth:I

    if-lez v11, :cond_3

    .line 2108
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget v11, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    if-ge v4, v11, :cond_3

    .line 2109
    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    if-eqz v11, :cond_2

    .line 2110
    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2111
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v6

    .line 2112
    .local v6, "left":I
    const/4 v1, 0x0

    .line 2113
    .local v1, "col":I
    const-string v11, "mColWidth"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    iget v13, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColWidth:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2117
    :goto_1
    iget v11, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColWidth:I

    iget v12, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemMargin:I

    mul-int/lit8 v12, v12, 0x2

    add-int/2addr v11, v12

    mul-int/2addr v11, v1

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getPaddingLeft()I

    move-result v12

    add-int/2addr v11, v12

    if-le v6, v11, :cond_1

    .line 2118
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2122
    :cond_1
    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/View;->getTop()I

    move-result v11

    iget v12, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemMargin:I

    sub-int/2addr v11, v12

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getPaddingTop()I

    move-result v12

    sub-int/2addr v11, v12

    aput v11, v10, v1

    .line 2108
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "col":I
    .end local v6    # "left":I
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2129
    .end local v4    # "i":I
    :cond_3
    iput-object v10, v8, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SavedState;->topOffsets:[I

    .line 2133
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2134
    .local v3, "convert":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$ColMap;>;"
    iget-object v11, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColMappings:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashSet;

    .line 2135
    .local v2, "cols":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    new-instance v11, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$ColMap;

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v11, v12}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$ColMap;-><init>(Ljava/util/ArrayList;)V

    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2139
    .end local v2    # "cols":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    :cond_4
    iput-object v3, v8, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SavedState;->mapping:Ljava/util/ArrayList;

    .line 2141
    .end local v3    # "convert":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$ColMap;>;"
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v10    # "topOffsets":[I
    :cond_5
    return-object v8
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 27
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 512
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 513
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v11, v2, 0xff

    .line 515
    .local v11, "action":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->pointToPosition(II)I

    move-result v20

    .line 517
    .local v20, "motionPosition":I
    packed-switch v11, :pswitch_data_0

    .line 702
    :goto_0
    const/4 v2, 0x1

    :goto_1
    return v2

    .line 519
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    .line 520
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->abortAnimation()V

    .line 521
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLastTouchY:F

    .line 522
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLastTouchX:F

    .line 523
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLastTouchX:F

    float-to-int v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLastTouchY:F

    float-to-int v3, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->pointToPosition(II)I

    move-result v20

    .line 524
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mActivePointerId:I

    .line 525
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchRemainderY:F

    .line 527
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDataChanged:Z

    if-nez v2, :cond_1

    if-ltz v20, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    move/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 529
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    .line 531
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mBeginClick:Z

    .line 533
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPendingCheckForTap:Ljava/lang/Runnable;

    if-nez v2, :cond_0

    .line 534
    new-instance v2, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForTap;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPendingCheckForTap:Ljava/lang/Runnable;

    .line 537
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPendingCheckForTap:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v3

    int-to-long v4, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4, v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 540
    :cond_1
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMotionPosition:I

    .line 541
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->invalidate()V

    goto/16 :goto_0

    .line 546
    :pswitch_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mActivePointerId:I

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v19

    .line 547
    .local v19, "index":I
    if-gez v19, :cond_2

    .line 548
    const-string v2, "ClippedStaggeredGridView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onInterceptTouchEvent could not find pointer with id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mActivePointerId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - did StaggeredGridView receive an inconsistent "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "event stream?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 553
    :cond_2
    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-static {v0, v1}, Landroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v26

    .line 554
    .local v26, "y":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLastTouchY:F

    sub-float v2, v26, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchRemainderY:F

    add-float v15, v2, v3

    .line 555
    .local v15, "dy":F
    float-to-int v14, v15

    .line 556
    .local v14, "deltaY":I
    int-to-float v2, v14

    sub-float v2, v15, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchRemainderY:F

    .line 558
    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchSlop:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    .line 559
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    .line 562
    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 563
    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLastTouchY:F

    .line 565
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->trackMotionScroll(IZ)Z

    move-result v2

    if-nez v2, :cond_4

    .line 567
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    .line 571
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->updateSelectorState()V

    goto/16 :goto_0

    .line 576
    .end local v14    # "deltaY":I
    .end local v15    # "dy":F
    .end local v19    # "index":I
    .end local v26    # "y":F
    :pswitch_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    .line 577
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->updateSelectorState()V

    .line 578
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setPressed(Z)V

    .line 579
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMotionPosition:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    sub-int/2addr v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v21

    .line 580
    .local v21, "motionView":Landroid/view/View;
    if-eqz v21, :cond_5

    .line 581
    const/4 v2, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/view/View;->setPressed(Z)V

    .line 583
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getHandler()Landroid/os/Handler;

    move-result-object v16

    .line 584
    .local v16, "handler":Landroid/os/Handler;
    if-eqz v16, :cond_6

    .line 585
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPendingCheckForLongPress:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 588
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v2, :cond_7

    .line 589
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v2}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    .line 590
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v2}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    .line 593
    :cond_7
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    goto/16 :goto_0

    .line 598
    .end local v16    # "handler":Landroid/os/Handler;
    .end local v21    # "motionView":Landroid/view/View;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v3, 0x3e8

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMaximumVelocity:I

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 599
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mActivePointerId:I

    invoke-static {v2, v3}, Landroid/support/v4/view/VelocityTrackerCompat;->getYVelocity(Landroid/view/VelocityTracker;I)F

    move-result v24

    .line 600
    .local v24, "velocity":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    move/from16 v23, v0

    .line 602
    .local v23, "prevTouchMode":I
    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->abs(F)F

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFlingVelocity:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_9

    .line 603
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    .line 604
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mScroller:Landroid/widget/Scroller;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move/from16 v0, v24

    float-to-int v6, v0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/high16 v9, -0x80000000

    const v10, 0x7fffffff

    invoke-virtual/range {v2 .. v10}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 605
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLastTouchY:F

    .line 606
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->invalidate()V

    .line 611
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDataChanged:Z

    if-nez v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    move/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 613
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    .line 618
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v2, :cond_8

    .line 619
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v2}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    .line 620
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v2}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    .line 623
    :cond_8
    packed-switch v23, :pswitch_data_1

    .line 696
    :goto_4
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mBeginClick:Z

    .line 698
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->updateSelectorState()V

    goto/16 :goto_0

    .line 608
    :cond_9
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    goto :goto_2

    .line 615
    :cond_a
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    goto :goto_3

    .line 627
    :pswitch_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    sub-int v2, v20, v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    .line 628
    .local v12, "child":Landroid/view/View;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v25

    .line 629
    .local v25, "x":F
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v25, v2

    if-lez v2, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getWidth()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpg-float v2, v25, v2

    if-gez v2, :cond_11

    const/16 v18, 0x1

    .line 630
    .local v18, "inList":Z
    :goto_5
    if-eqz v12, :cond_15

    invoke-virtual {v12}, Landroid/view/View;->hasFocusable()Z

    move-result v2

    if-nez v2, :cond_15

    if-eqz v18, :cond_15

    .line 631
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_b

    .line 632
    const/4 v2, 0x0

    invoke-virtual {v12, v2}, Landroid/view/View;->setPressed(Z)V

    .line 635
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPerformClick:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$PerformClick;

    if-nez v2, :cond_c

    .line 636
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->invalidate()V

    .line 637
    new-instance v2, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$PerformClick;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$PerformClick;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPerformClick:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$PerformClick;

    .line 640
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPerformClick:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$PerformClick;

    move-object/from16 v22, v0

    .line 641
    .local v22, "performClick":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$PerformClick;
    move/from16 v0, v20

    move-object/from16 v1, v22

    iput v0, v1, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$PerformClick;->mClickMotionPosition:I

    .line 642
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$PerformClick;->rememberWindowAttachCount()V

    .line 644
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_d

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_14

    .line 645
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getHandler()Landroid/os/Handler;

    move-result-object v17

    .line 646
    .local v17, "handlerTouch":Landroid/os/Handler;
    if-eqz v17, :cond_e

    .line 647
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPendingCheckForTap:Ljava/lang/Runnable;

    :goto_6
    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 651
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDataChanged:Z

    if-nez v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    move/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 652
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    .line 654
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDataChanged:Z

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->layoutChildren(Z)V

    .line 655
    const/4 v2, 0x1

    invoke-virtual {v12, v2}, Landroid/view/View;->setPressed(Z)V

    .line 656
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMotionPosition:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v12}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->positionSelector(ILandroid/view/View;)V

    .line 657
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setPressed(Z)V

    .line 659
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_f

    .line 660
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 661
    .local v13, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v13, :cond_f

    instance-of v2, v13, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v2, :cond_f

    .line 662
    check-cast v13, Landroid/graphics/drawable/TransitionDrawable;

    .end local v13    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v13}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    .line 666
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchModeReset:Ljava/lang/Runnable;

    if-eqz v2, :cond_10

    .line 667
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchModeReset:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 670
    :cond_10
    new-instance v2, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v2, v0, v12, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;Landroid/view/View;Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$PerformClick;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchModeReset:Ljava/lang/Runnable;

    .line 681
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchModeReset:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    move-result v3

    int-to-long v4, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4, v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 686
    :goto_7
    const/4 v2, 0x1

    goto/16 :goto_1

    .line 629
    .end local v17    # "handlerTouch":Landroid/os/Handler;
    .end local v18    # "inList":Z
    .end local v22    # "performClick":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$PerformClick;
    :cond_11
    const/16 v18, 0x0

    goto/16 :goto_5

    .line 647
    .restart local v17    # "handlerTouch":Landroid/os/Handler;
    .restart local v18    # "inList":Z
    .restart local v22    # "performClick":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$PerformClick;
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPendingCheckForLongPress:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$CheckForLongPress;

    goto/16 :goto_6

    .line 684
    :cond_13
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    goto :goto_7

    .line 688
    .end local v17    # "handlerTouch":Landroid/os/Handler;
    :cond_14
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDataChanged:Z

    if-nez v2, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v2, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    move/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 689
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$PerformClick;->run()V

    .line 693
    .end local v22    # "performClick":Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$PerformClick;
    :cond_15
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    goto/16 :goto_4

    .line 517
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 623
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public performItemClick(Landroid/view/View;IJ)Z
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "id"    # J

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 2762
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mOnItemClickListener:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemClickListener;

    if-eqz v1, :cond_1

    .line 2763
    invoke-virtual {p0, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->playSoundEffect(I)V

    .line 2764
    if-eqz p1, :cond_0

    .line 2765
    invoke-virtual {p1, v6}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 2767
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mOnItemClickListener:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemClickListener;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemClickListener;->onItemClick(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;Landroid/view/View;IJ)V

    move v0, v6

    .line 2772
    :cond_1
    return v0
.end method

.method performLongPress(Landroid/view/View;IJ)Z
    .locals 7
    .param p1, "child"    # Landroid/view/View;
    .param p2, "longPressPosition"    # I
    .param p3, "longPressId"    # J

    .prologue
    .line 2783
    const/4 v6, 0x0

    .line 2784
    .local v6, "handled":Z
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mOnItemLongClickListener:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemLongClickListener;

    if-eqz v0, :cond_0

    .line 2785
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mOnItemLongClickListener:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemLongClickListener;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemLongClickListener;->onItemLongClick(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;Landroid/view/View;IJ)Z

    move-result v6

    .line 2787
    :cond_0
    if-nez v6, :cond_1

    .line 2788
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 2789
    invoke-super {p0, p0}, Landroid/view/ViewGroup;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v6

    .line 2791
    :cond_1
    if-eqz v6, :cond_2

    .line 2792
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->performHapticFeedback(I)Z

    .line 2794
    :cond_2
    return v6
.end method

.method public pointToPosition(II)I
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 3057
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchFrame:Landroid/graphics/Rect;

    .line 3058
    .local v2, "frame":Landroid/graphics/Rect;
    if-nez v2, :cond_0

    .line 3059
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchFrame:Landroid/graphics/Rect;

    .line 3060
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchFrame:Landroid/graphics/Rect;

    .line 3064
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildCount()I

    move-result v1

    .line 3065
    .local v1, "count":I
    add-int/lit8 v3, v1, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_2

    .line 3066
    invoke-virtual {p0, v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3067
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    .line 3068
    invoke-virtual {v0, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 3069
    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 3070
    iget v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    add-int/2addr v4, v3

    .line 3074
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return v4

    .line 3065
    .restart local v0    # "child":Landroid/view/View;
    :cond_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 3074
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    const/4 v4, -0x1

    goto :goto_1
.end method

.method positionSelector(ILandroid/view/View;)V
    .locals 7
    .param p1, "position"    # I
    .param p2, "sel"    # Landroid/view/View;

    .prologue
    const/4 v6, -0x1

    .line 2557
    if-eq p1, v6, :cond_0

    .line 2558
    iput p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectorPosition:I

    .line 2562
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectorRect:Landroid/graphics/Rect;

    .line 2563
    .local v1, "selectorRect":Landroid/graphics/Rect;
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 2564
    instance-of v2, p2, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SelectionBoundsAdjuster;

    if-eqz v2, :cond_1

    move-object v2, p2

    .line 2565
    check-cast v2, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SelectionBoundsAdjuster;

    invoke-interface {v2, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$SelectionBoundsAdjuster;->adjustListItemSelectionBounds(Landroid/graphics/Rect;)V

    .line 2569
    :cond_1
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, v1, Landroid/graphics/Rect;->top:I

    iget v4, v1, Landroid/graphics/Rect;->right:I

    iget v5, v1, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->positionSelector(IIII)V

    .line 2573
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mIsChildViewEnabled:Z

    .line 2574
    .local v0, "isChildViewEnabled":Z
    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eq v2, v0, :cond_2

    .line 2575
    if-nez v0, :cond_3

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mIsChildViewEnabled:Z

    .line 2576
    invoke-direct {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getSelectedItemPosition()I

    move-result v2

    if-eq v2, v6, :cond_2

    .line 2577
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->refreshDrawableState()V

    .line 2580
    :cond_2
    return-void

    .line 2575
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 1010
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPopulating:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFastChildLayout:Z

    if-nez v0, :cond_0

    .line 1011
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 1013
    :cond_0
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 4
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1978
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 1979
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mObserver:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1983
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->clearAllState()V

    .line 1984
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;

    .line 1985
    iput-boolean v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDataChanged:Z

    .line 1988
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemCount:I

    .line 1991
    if-eqz p1, :cond_3

    .line 1992
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mObserver:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1993
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRecycler:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;->setViewTypeCount(I)V

    .line 1994
    invoke-interface {p1}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mHasStableIds:Z

    .line 1998
    :goto_1
    if-eqz p1, :cond_1

    move v1, v2

    :cond_1
    invoke-direct {p0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->populate(Z)V

    .line 1999
    return-void

    :cond_2
    move v0, v1

    .line 1988
    goto :goto_0

    .line 1996
    :cond_3
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mHasStableIds:Z

    goto :goto_1
.end method

.method public setAddScenario(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 3108
    iput-boolean p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAddScenario:Z

    .line 3109
    return-void
.end method

.method public setColumnCount(I)V
    .locals 4
    .param p1, "colCount"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 407
    if-ge p1, v0, :cond_0

    const/4 v2, -0x1

    if-eq p1, v2, :cond_0

    .line 408
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Column count must be at least 1 - received "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 411
    :cond_0
    iget v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    if-eq p1, v2, :cond_2

    .line 412
    .local v0, "needsPopulate":Z
    :goto_0
    iput p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCountSetting:I

    iput p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I

    .line 413
    if-eqz v0, :cond_1

    .line 414
    invoke-direct {p0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->populate(Z)V

    .line 416
    :cond_1
    return-void

    .end local v0    # "needsPopulate":Z
    :cond_2
    move v0, v1

    .line 411
    goto :goto_0
.end method

.method public setDeleteScenario(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 3104
    iput-boolean p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDeleteScenario:Z

    .line 3105
    return-void
.end method

.method public setDrawSelectorOnTop(Z)V
    .locals 0
    .param p1, "mDrawSelectorOnTop"    # Z

    .prologue
    .line 3084
    iput-boolean p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDrawSelectorOnTop:Z

    .line 3085
    return-void
.end method

.method public setItemMargin(I)V
    .locals 3
    .param p1, "marginPixels"    # I

    .prologue
    const/4 v1, 0x0

    .line 441
    iget v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemMargin:I

    if-eq p1, v2, :cond_1

    const/4 v0, 0x1

    .line 442
    .local v0, "needsPopulate":Z
    :goto_0
    iput p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemMargin:I

    .line 443
    if-eqz v0, :cond_0

    .line 444
    invoke-direct {p0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->populate(Z)V

    .line 446
    :cond_0
    return-void

    .end local v0    # "needsPopulate":Z
    :cond_1
    move v0, v1

    .line 441
    goto :goto_0
.end method

.method public setMinColumnWidth(I)V
    .locals 1
    .param p1, "minColWidth"    # I

    .prologue
    .line 429
    iput p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mMinColWidth:I

    .line 430
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setColumnCount(I)V

    .line 431
    return-void
.end method

.method public setOnItemClickListener(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemClickListener;

    .prologue
    .line 2973
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mOnItemClickListener:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemClickListener;

    .line 2974
    return-void
.end method

.method public setOnItemLongClickListener(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemLongClickListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemLongClickListener;

    .prologue
    .line 3013
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->isLongClickable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3014
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setLongClickable(Z)V

    .line 3016
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mOnItemLongClickListener:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$OnItemLongClickListener;

    .line 3017
    return-void
.end method

.method public setSelectionToTop()V
    .locals 1

    .prologue
    .line 2052
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->removeAllViews()V

    .line 2056
    invoke-direct {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->resetStateForGridTop()V

    .line 2060
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->populate(Z)V

    .line 2061
    return-void
.end method

.method public setSelector(I)V
    .locals 1
    .param p1, "resID"    # I

    .prologue
    .line 2876
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 2877
    return-void
.end method

.method public setSelector(Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p1, "sel"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 2895
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 2896
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2897
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2901
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    .line 2904
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_1

    .line 2917
    :goto_0
    return-void

    .line 2909
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 2910
    .local v0, "padding":Landroid/graphics/Rect;
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 2911
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectionLeftPadding:I

    .line 2912
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectionTopPadding:I

    .line 2913
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectionRightPadding:I

    .line 2914
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectionBottomPadding:I

    .line 2915
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2916
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->updateSelectorState()V

    goto :goto_0
.end method

.method shouldShowSelector()Z
    .locals 1

    .prologue
    .line 2946
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->touchModeDrawsInPressedState()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mBeginClick:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method touchModeDrawsInPressedState()Z
    .locals 1

    .prologue
    .line 2956
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTouchMode:I

    packed-switch v0, :pswitch_data_0

    .line 2961
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2959
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 2956
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public trackMotionScroll(IZ)Z
    .locals 12
    .param p1, "deltaY"    # I
    .param p2, "allowOverScroll"    # Z

    .prologue
    .line 714
    sget-boolean v9, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->DEBUG:Z

    if-eqz v9, :cond_0

    .line 715
    const-string v9, "ClippedStaggeredGridView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "trackMotionScroll() deltaY : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", allowOverScroll : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 717
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->contentFits()Z

    move-result v2

    .line 718
    .local v2, "contentFits":Z
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 723
    .local v0, "allowOverhang":I
    if-nez v2, :cond_7

    .line 726
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPopulating:Z

    .line 727
    if-lez p1, :cond_5

    .line 728
    iget v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {p0, v9, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->fillUp(II)I

    move-result v9

    iget v10, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemMargin:I

    add-int v7, v9, v10

    .line 729
    .local v7, "overhang":I
    const/4 v8, 0x1

    .line 734
    .local v8, "up":Z
    :goto_0
    invoke-static {v7, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 735
    .local v4, "movedBy":I
    if-eqz v8, :cond_6

    move v9, v4

    :goto_1
    invoke-virtual {p0, v9}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->offsetChildren(I)V

    .line 736
    invoke-direct {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->recycleOffscreenViews()V

    .line 737
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mPopulating:Z

    .line 738
    sub-int v6, v0, v7

    .line 745
    .end local v7    # "overhang":I
    .end local v8    # "up":Z
    .local v6, "overScrolledBy":I
    :goto_2
    if-eqz p2, :cond_2

    .line 746
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->getOverScrollMode(Landroid/view/View;)I

    move-result v5

    .line 749
    .local v5, "overScrollMode":I
    if-eqz v5, :cond_1

    const/4 v9, 0x1

    if-ne v5, v9, :cond_2

    if-nez v2, :cond_2

    .line 751
    :cond_1
    if-lez v6, :cond_2

    .line 752
    if-lez p1, :cond_8

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 753
    .local v3, "edge":Landroid/support/v4/widget/EdgeEffectCompat;
    :goto_3
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getHeight()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    invoke-virtual {v3, v9}, Landroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    .line 754
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->invalidate()V

    .line 760
    .end local v3    # "edge":Landroid/support/v4/widget/EdgeEffectCompat;
    .end local v5    # "overScrollMode":I
    :cond_2
    iget v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectorPosition:I

    const/4 v10, -0x1

    if-eq v9, v10, :cond_9

    .line 761
    iget v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectorPosition:I

    iget v10, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    sub-int v1, v9, v10

    .line 762
    .local v1, "childIndex":I
    if-ltz v1, :cond_3

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildCount()I

    move-result v9

    if-ge v1, v9, :cond_3

    .line 763
    const/4 v9, -0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {p0, v9, v10}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->positionSelector(ILandroid/view/View;)V

    .line 770
    .end local v1    # "childIndex":I
    :cond_3
    :goto_4
    if-eqz p1, :cond_4

    if-eqz v4, :cond_a

    :cond_4
    const/4 v9, 0x1

    :goto_5
    return v9

    .line 731
    .end local v4    # "movedBy":I
    .end local v6    # "overScrolledBy":I
    :cond_5
    iget v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildCount()I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {p0, v9, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->fillDown(II)I

    move-result v9

    iget v10, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemMargin:I

    add-int v7, v9, v10

    .line 732
    .restart local v7    # "overhang":I
    const/4 v8, 0x0

    .restart local v8    # "up":Z
    goto :goto_0

    .line 735
    .restart local v4    # "movedBy":I
    :cond_6
    neg-int v9, v4

    goto :goto_1

    .line 740
    .end local v4    # "movedBy":I
    .end local v7    # "overhang":I
    .end local v8    # "up":Z
    :cond_7
    move v6, v0

    .line 741
    .restart local v6    # "overScrolledBy":I
    const/4 v4, 0x0

    .restart local v4    # "movedBy":I
    goto :goto_2

    .line 752
    .restart local v5    # "overScrollMode":I
    :cond_8
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    goto :goto_3

    .line 766
    .end local v5    # "overScrollMode":I
    :cond_9
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->setEmpty()V

    goto :goto_4

    .line 770
    :cond_a
    const/4 v9, 0x0

    goto :goto_5
.end method

.method updateSelectorState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2921
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 2922
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->shouldShowSelector()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2923
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 2928
    :cond_0
    :goto_0
    return-void

    .line 2925
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x1

    new-array v1, v1, [I

    aput v2, v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    goto :goto_0
.end method

.method public verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1, "dr"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 2882
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
