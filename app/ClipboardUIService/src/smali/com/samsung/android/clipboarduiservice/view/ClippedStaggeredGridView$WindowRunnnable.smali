.class Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$WindowRunnnable;
.super Ljava/lang/Object;
.source "ClippedStaggeredGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WindowRunnnable"
.end annotation


# instance fields
.field private mOriginalAttachCount:I

.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)V
    .locals 0

    .prologue
    .line 2534
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$WindowRunnnable;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;
    .param p2, "x1"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;

    .prologue
    .line 2534
    invoke-direct {p0, p1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$WindowRunnnable;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)V

    return-void
.end method


# virtual methods
.method public rememberWindowAttachCount()V
    .locals 1

    .prologue
    .line 2539
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$WindowRunnnable;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # invokes: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getWindowAttachCount()I
    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$2200(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$WindowRunnnable;->mOriginalAttachCount:I

    .line 2540
    return-void
.end method

.method public sameWindow()Z
    .locals 2

    .prologue
    .line 2544
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$WindowRunnnable;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$WindowRunnnable;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # invokes: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getWindowAttachCount()I
    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$2300(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I

    move-result v0

    iget v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$WindowRunnnable;->mOriginalAttachCount:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
