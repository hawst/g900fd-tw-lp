.class Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;
.super Ljava/lang/Object;
.source "ImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/util/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageHolder"
.end annotation


# instance fields
.field public mClipData:Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

.field public mImageView:Landroid/widget/ImageView;

.field public mPosition:I

.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/util/ImageLoader;


# direct methods
.method public constructor <init>(Lcom/samsung/android/clipboarduiservice/util/ImageLoader;Landroid/widget/ImageView;Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;I)V
    .locals 0
    .param p2, "i"    # Landroid/widget/ImageView;
    .param p3, "clipData"    # Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    .param p4, "position"    # I

    .prologue
    .line 89
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->this$0:Lcom/samsung/android/clipboarduiservice/util/ImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p2, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->mImageView:Landroid/widget/ImageView;

    .line 91
    iput p4, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->mPosition:I

    .line 92
    iput-object p3, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;->mClipData:Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    .line 93
    return-void
.end method
