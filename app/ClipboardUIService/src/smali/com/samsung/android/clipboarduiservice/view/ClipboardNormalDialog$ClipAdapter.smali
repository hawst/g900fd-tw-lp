.class public Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;
.super Landroid/widget/BaseAdapter;
.source "ClipboardNormalDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ClipAdapter"
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private mBackgroundImageIDs:[I

.field private mChildViewIDs:[I

.field private mLayoutInflator:Landroid/view/LayoutInflater;

.field private mRecycleBitmapList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field private mRecycleList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;


# direct methods
.method public constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Landroid/content/Context;)V
    .locals 2
    .param p2, "c"    # Landroid/content/Context;

    .prologue
    .line 1962
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1952
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mRecycleList:Ljava/util/List;

    .line 1953
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mRecycleBitmapList:Ljava/util/List;

    .line 1955
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mChildViewIDs:[I

    .line 1959
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mBackgroundImageIDs:[I

    .line 1963
    iput-object p2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->context:Landroid/content/Context;

    .line 1964
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mLayoutInflator:Landroid/view/LayoutInflater;

    .line 1969
    return-void

    .line 1955
    :array_0
    .array-data 4
        0x7f0a0012
        0x7f0a0013
        0x7f0a0014
        0x7f0a0015
        0x7f0a0019
    .end array-data

    .line 1959
    :array_1
    .array-data 4
        0x7f020009
        0x7f02000a
        0x7f02000b
    .end array-data
.end method

.method static synthetic access$2500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;

    .prologue
    .line 1948
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mRecycleList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;

    .prologue
    .line 1948
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mRecycleBitmapList:Ljava/util/List;

    return-object v0
.end method

.method private getFilename(Landroid/net/Uri;)Ljava/lang/String;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 2333
    const-string v7, ""

    .line 2334
    .local v7, "fileName":Ljava/lang/String;
    const/4 v9, 0x0

    .line 2335
    .local v9, "path":Ljava/lang/String;
    const-string v0, "content"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2336
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2337
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 2338
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 2339
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 2340
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2343
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_0
    if-nez v9, :cond_1

    .line 2344
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    .line 2346
    :cond_1
    const-string v0, "/"

    invoke-virtual {v9, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 2348
    .local v8, "parts":[Ljava/lang/String;
    array-length v0, v8

    if-lez v0, :cond_2

    .line 2349
    array-length v0, v8

    add-int/lit8 v0, v0, -0x1

    aget-object v7, v8, v0

    .line 2353
    :goto_0
    return-object v7

    .line 2351
    :cond_2
    move-object v7, v9

    goto :goto_0
.end method

.method private getTextSize(Ljava/lang/String;)I
    .locals 14
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x46

    .line 2282
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v11

    .line 2283
    .local v11, "textLength":I
    const/4 v12, 0x0

    .line 2284
    .local v12, "textSize":I
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f050022

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 2285
    .local v10, "minSize":I
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f050021

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 2286
    .local v9, "maxSize":I
    sub-int v1, v9, v10

    div-int/lit8 v7, v1, 0x7

    .line 2287
    .local v7, "diffSize":I
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f050026

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v3}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050014

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sub-int/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v3}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050017

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sub-int v13, v1, v3

    .line 2291
    .local v13, "textViewWidth":I
    const/16 v1, 0xc

    if-gt v11, v1, :cond_0

    .line 2292
    move v12, v9

    .line 2299
    :goto_0
    if-ge v11, v5, :cond_4

    .line 2300
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    .line 2301
    .local v0, "paint":Landroid/text/TextPaint;
    int-to-float v1, v9

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 2302
    const/4 v2, 0x0

    .line 2303
    .local v2, "index":I
    const/4 v8, 0x0

    .line 2304
    .local v8, "lineCount":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 2305
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    add-int/lit8 v1, v13, -0x1

    int-to-float v5, v1

    const/4 v6, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Landroid/text/TextPaint;->breakText(Ljava/lang/CharSequence;IIZF[F)I

    move-result v1

    add-int/2addr v2, v1

    .line 2306
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 2293
    .end local v0    # "paint":Landroid/text/TextPaint;
    .end local v2    # "index":I
    .end local v8    # "lineCount":I
    :cond_0
    if-lt v11, v5, :cond_1

    .line 2294
    move v12, v10

    goto :goto_0

    .line 2296
    :cond_1
    add-int/lit8 v1, v10, 0x46

    sub-int v12, v1, v11

    goto :goto_0

    .line 2309
    .restart local v0    # "paint":Landroid/text/TextPaint;
    .restart local v2    # "index":I
    .restart local v8    # "lineCount":I
    :cond_2
    :goto_2
    const-string v1, "\n"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v1, -0x1

    if-eq v2, v1, :cond_3

    .line 2310
    add-int/lit8 v8, v8, 0x1

    .line 2311
    add-int/lit8 v1, v2, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    .line 2314
    :cond_3
    const/16 v1, 0x8

    if-lt v8, v1, :cond_5

    .line 2315
    move v12, v10

    .line 2323
    :goto_3
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2324
    const-string v1, "ClipboardNormalDialog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getTextSize() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", lineCount : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", textSize : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2329
    .end local v0    # "paint":Landroid/text/TextPaint;
    .end local v2    # "index":I
    .end local v8    # "lineCount":I
    :cond_4
    return v12

    .line 2317
    .restart local v0    # "paint":Landroid/text/TextPaint;
    .restart local v2    # "index":I
    .restart local v8    # "lineCount":I
    :cond_5
    const/4 v1, 0x3

    if-ge v8, v1, :cond_6

    .line 2318
    move v12, v9

    goto :goto_3

    .line 2321
    :cond_6
    mul-int v1, v7, v8

    sub-int v12, v9, v1

    goto :goto_3
.end method


# virtual methods
.method public clearChildView(Landroid/view/View;)V
    .locals 6
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    .line 2083
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mChildViewIDs:[I

    array-length v1, v2

    .line 2084
    .local v1, "cnt":I
    :cond_0
    :goto_0
    if-lez v1, :cond_4

    .line 2085
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mChildViewIDs:[I

    add-int/lit8 v1, v1, -0x1

    aget v2, v2, v1

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2086
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_3

    .line 2087
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2088
    const-string v2, "ClipboardNormalDialog"

    const-string v3, "clearChildView() clear childView"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2090
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eq v2, v5, :cond_2

    .line 2091
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2093
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2094
    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 2096
    invoke-virtual {p1, v4}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 2099
    :cond_3
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2100
    const-string v2, "ClipboardNormalDialog"

    const-string v3, "clearChildView() childView is null. so, can not clear it"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2104
    .end local v0    # "child":Landroid/view/View;
    :cond_4
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDataCount:I
    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1980
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1981
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1983
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1989
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x0

    const/4 v11, 0x0

    .line 1995
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt p1, v8, :cond_1

    move-object v2, v9

    .line 2079
    :cond_0
    :goto_0
    return-object v2

    .line 1999
    :cond_1
    const/4 v2, 0x0

    .line 2000
    .local v2, "itemView":Landroid/view/View;
    if-nez p2, :cond_5

    .line 2001
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->context:Landroid/content/Context;

    const-string v10, "layout_inflater"

    invoke-virtual {v8, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/LayoutInflater;

    iput-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mLayoutInflator:Landroid/view/LayoutInflater;

    .line 2002
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mLayoutInflator:Landroid/view/LayoutInflater;

    const v10, 0x7f030003

    invoke-virtual {v8, v10, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2007
    :goto_1
    invoke-virtual {p0, v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->clearChildView(Landroid/view/View;)V

    .line 2010
    :try_start_0
    invoke-virtual {p0, v2, p1, p3}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->settingBodyView(Landroid/view/View;ILandroid/view/View;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2017
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mRecycleList:Ljava/util/List;

    new-instance v9, Ljava/lang/ref/WeakReference;

    invoke-direct {v9, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2023
    new-instance v8, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter$1;

    invoke-direct {v8, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter$1;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;)V

    invoke-virtual {v2, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2037
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsSupportLockFunc:Z
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 2038
    new-instance v8, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter$2;

    invoke-direct {v8, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter$2;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;)V

    invoke-virtual {v2, v8}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2048
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mFirstPosition:I
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2900(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)I

    move-result v8

    sub-int v0, p1, v8

    .line 2051
    .local v0, "childPos":I
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteScenario:Z
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$900(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)I

    move-result v8

    if-lt p1, v8, :cond_4

    if-ltz v0, :cond_4

    .line 2052
    const/4 v3, 0x0

    .line 2053
    .local v3, "preX":I
    const/4 v4, 0x0

    .line 2054
    .local v4, "preY":I
    const/4 v5, 0x0

    .line 2055
    .local v5, "trans":Landroid/view/animation/TranslateAnimation;
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v0, v8, :cond_3

    .line 2056
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;

    iget v3, v8, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;->x:I

    .line 2057
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;

    iget v4, v8, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;->y:I

    .line 2059
    :cond_3
    add-int/lit8 v8, v0, 0x1

    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v8, v9, :cond_6

    .line 2060
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v8

    add-int/lit8 v9, v0, 0x1

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;

    iget v6, v8, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;->x:I

    .line 2061
    .local v6, "x":I
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v8

    add-int/lit8 v9, v0, 0x1

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;

    iget v7, v8, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;->y:I

    .line 2062
    .local v7, "y":I
    new-instance v5, Landroid/view/animation/TranslateAnimation;

    .end local v5    # "trans":Landroid/view/animation/TranslateAnimation;
    sub-int v8, v6, v3

    int-to-float v8, v8

    sub-int v9, v7, v4

    int-to-float v9, v9

    invoke-direct {v5, v8, v11, v9, v11}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2066
    .end local v6    # "x":I
    .end local v7    # "y":I
    .restart local v5    # "trans":Landroid/view/animation/TranslateAnimation;
    :goto_2
    const-wide/16 v8, 0x12c

    invoke-virtual {v5, v8, v9}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 2067
    new-instance v8, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v8}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v5, v8}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2068
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    const/16 v9, 0x32

    # += operator for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteAnimDelay:I
    invoke-static {v8, v9}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1212(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v5, v8, v9}, Landroid/view/animation/TranslateAnimation;->setStartOffset(J)V

    .line 2069
    invoke-virtual {v2, v5}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 2070
    invoke-virtual {v2, v5}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2076
    .end local v3    # "preX":I
    .end local v4    # "preY":I
    .end local v5    # "trans":Landroid/view/animation/TranslateAnimation;
    :cond_4
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsSupportLockFunc:Z
    invoke-static {v8}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2077
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-virtual {v8, v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->registerForContextMenu(Landroid/view/View;)V

    goto/16 :goto_0

    .line 2004
    .end local v0    # "childPos":I
    :cond_5
    move-object v2, p2

    goto/16 :goto_1

    .line 2011
    :catch_0
    move-exception v1

    .line 2012
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->recycleHalf()V

    .line 2013
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->recycleHalfBitmap()V

    .line 2014
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    goto/16 :goto_0

    .line 2064
    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v0    # "childPos":I
    .restart local v3    # "preX":I
    .restart local v4    # "preY":I
    .restart local v5    # "trans":Landroid/view/animation/TranslateAnimation;
    :cond_6
    new-instance v5, Landroid/view/animation/TranslateAnimation;

    .end local v5    # "trans":Landroid/view/animation/TranslateAnimation;
    const/high16 v8, 0x43fa0000    # 500.0f

    invoke-direct {v5, v11, v11, v8, v11}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v5    # "trans":Landroid/view/animation/TranslateAnimation;
    goto :goto_2
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 2372
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mRecycleList:Ljava/util/List;

    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/util/RecycleUtils;->recursiveRecycle(Ljava/util/List;)V

    .line 2373
    return-void
.end method

.method public recycleBitmap()V
    .locals 1

    .prologue
    .line 2376
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mRecycleBitmapList:Ljava/util/List;

    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/util/RecycleUtils;->recursiveBitmapRecycle(Ljava/util/List;)V

    .line 2377
    return-void
.end method

.method public recycleHalf()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2357
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mRecycleList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    div-int/lit8 v0, v3, 0x2

    .line 2358
    .local v0, "halfSize":I
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mRecycleList:Ljava/util/List;

    invoke-interface {v3, v4, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 2359
    .local v2, "recycleHalfList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/ref/WeakReference<Landroid/view/View;>;>;"
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/util/RecycleUtils;->recursiveRecycle(Ljava/util/List;)V

    .line 2360
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 2361
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mRecycleList:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2360
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2362
    :cond_0
    return-void
.end method

.method public recycleHalfBitmap()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2364
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mRecycleBitmapList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    div-int/lit8 v0, v3, 0x2

    .line 2365
    .local v0, "halfSize":I
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mRecycleBitmapList:Ljava/util/List;

    invoke-interface {v3, v4, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 2366
    .local v2, "recycleHalfList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/ref/WeakReference<Landroid/graphics/Bitmap;>;>;"
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/util/RecycleUtils;->recursiveBitmapRecycle(Ljava/util/List;)V

    .line 2367
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 2368
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mRecycleBitmapList:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2367
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2369
    :cond_0
    return-void
.end method

.method public settingBodyView(Landroid/view/View;ILandroid/view/View;)Landroid/view/View;
    .locals 26
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "parentView"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 2107
    const/4 v6, 0x0

    .line 2108
    .local v6, "bodyView":Landroid/view/View;
    const/4 v4, 0x0

    .line 2109
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardUIService:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1600(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->getClipboardFormatType()I

    move-result v11

    .line 2110
    .local v11, "currentTargetDataType":I
    const/16 v18, -0x1

    .line 2112
    .local v18, "type":I
    const/4 v7, 0x0

    .line 2114
    .local v7, "clipUIData":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v21

    move/from16 v1, p2

    if-le v0, v1, :cond_0

    .line 2115
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "clipUIData":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    check-cast v7, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    .line 2117
    .restart local v7    # "clipUIData":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    :cond_0
    if-nez v7, :cond_2

    .line 2118
    const v21, 0x7f0a0012

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 2119
    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2120
    const/16 v21, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2278
    :cond_1
    :goto_0
    return-object v6

    .line 2122
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->showProtectedMarker(Landroid/view/View;I)V

    .line 2124
    invoke-virtual {v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getType()I

    move-result v18

    .line 2126
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f050022

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v15

    .line 2128
    .local v15, "minFontSize":I
    packed-switch v18, :pswitch_data_0

    .line 2263
    :cond_3
    :goto_1
    :pswitch_0
    const/4 v8, 0x0

    .line 2264
    .local v8, "colorNumber":I
    if-eqz v7, :cond_4

    .line 2265
    invoke-virtual {v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getColor()I

    move-result v8

    .line 2268
    :cond_4
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2269
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mBackgroundImageIDs:[I

    move-object/from16 v21, v0

    aget v21, v21, v8

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2271
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2273
    if-eqz v4, :cond_1

    .line 2274
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mRecycleBitmapList:Ljava/util/List;

    move-object/from16 v21, v0

    new-instance v22, Ljava/lang/ref/WeakReference;

    move-object/from16 v0, v22

    invoke-direct {v0, v4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface/range {v21 .. v22}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2130
    .end local v8    # "colorNumber":I
    :pswitch_1
    const v21, 0x7f0a0012

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 2131
    if-eqz v6, :cond_3

    .line 2132
    invoke-virtual {v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getText()Ljava/lang/String;

    move-result-object v17

    .local v17, "text":Ljava/lang/String;
    move-object/from16 v21, v6

    .line 2133
    check-cast v21, Landroid/widget/TextView;

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v21, v6

    .line 2134
    check-cast v21, Landroid/widget/TextView;

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->getTextSize(Ljava/lang/String;)I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    invoke-virtual/range {v21 .. v23}, Landroid/widget/TextView;->setTextSize(IF)V

    move-object/from16 v21, v6

    .line 2135
    check-cast v21, Landroid/widget/TextView;

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->enableMultiSelection(Z)V

    .line 2136
    const/16 v21, 0x3

    move/from16 v0, v21

    if-ne v11, v0, :cond_3

    .line 2137
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 2138
    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v21

    check-cast v21, Landroid/widget/FrameLayout;

    check-cast v21, Landroid/widget/FrameLayout;

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 2139
    const v21, 0x7f0a0019

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 2140
    .local v12, "dim":Landroid/widget/ImageView;
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2146
    .end local v12    # "dim":Landroid/widget/ImageView;
    .end local v17    # "text":Ljava/lang/String;
    :pswitch_2
    const v21, 0x7f0a0013

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 2147
    invoke-virtual {v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2148
    if-eqz v4, :cond_6

    if-eqz v6, :cond_6

    .line 2149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f050025

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    .line 2150
    .local v14, "maxHeight":I
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v21

    move/from16 v0, v21

    if-le v0, v14, :cond_5

    .line 2151
    invoke-virtual {v4, v14}, Landroid/graphics/Bitmap;->setHeight(I)V

    :cond_5
    move-object/from16 v21, v6

    .line 2153
    check-cast v21, Landroid/widget/ImageView;

    invoke-virtual {v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2155
    .end local v14    # "maxHeight":I
    :cond_6
    if-eqz v6, :cond_3

    const/16 v21, 0x2

    move/from16 v0, v21

    if-ne v11, v0, :cond_3

    .line 2156
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 2157
    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v21

    check-cast v21, Landroid/widget/FrameLayout;

    check-cast v21, Landroid/widget/FrameLayout;

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 2158
    const v21, 0x7f0a0019

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 2159
    .restart local v12    # "dim":Landroid/widget/ImageView;
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2164
    .end local v12    # "dim":Landroid/widget/ImageView;
    :pswitch_3
    const v21, 0x7f0a0014

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 2165
    if-eqz v6, :cond_3

    .line 2166
    invoke-virtual {v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2167
    if-eqz v4, :cond_8

    .line 2168
    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v5, v0, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 2169
    .local v5, "bmDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImageHeight:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_7

    .line 2170
    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImageWidth:I

    move/from16 v23, v0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v24

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    :goto_2
    move-object/from16 v21, v6

    .line 2175
    check-cast v21, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImagePadding:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    move-object/from16 v21, v6

    .line 2176
    check-cast v21, Landroid/widget/TextView;

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v5, v2, v3}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v21, v6

    .line 2177
    check-cast v21, Landroid/widget/TextView;

    const/16 v22, 0x0

    int-to-float v0, v15

    move/from16 v23, v0

    invoke-virtual/range {v21 .. v23}, Landroid/widget/TextView;->setTextSize(IF)V

    .end local v5    # "bmDrawable":Landroid/graphics/drawable/BitmapDrawable;
    :goto_3
    move-object/from16 v21, v6

    .line 2184
    check-cast v21, Landroid/widget/TextView;

    invoke-virtual {v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getText()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v21, v6

    .line 2185
    check-cast v21, Landroid/widget/TextView;

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->enableMultiSelection(Z)V

    .line 2187
    const/16 v21, 0x3

    move/from16 v0, v21

    if-ne v11, v0, :cond_3

    .line 2188
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 2189
    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v21

    check-cast v21, Landroid/widget/FrameLayout;

    check-cast v21, Landroid/widget/FrameLayout;

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 2190
    const v21, 0x7f0a0019

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 2191
    .restart local v12    # "dim":Landroid/widget/ImageView;
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2172
    .end local v12    # "dim":Landroid/widget/ImageView;
    .restart local v5    # "bmDrawable":Landroid/graphics/drawable/BitmapDrawable;
    :cond_7
    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImageWidth:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImageHeight:I

    move/from16 v24, v0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    goto/16 :goto_2

    .end local v5    # "bmDrawable":Landroid/graphics/drawable/BitmapDrawable;
    :cond_8
    move-object/from16 v21, v6

    .line 2179
    check-cast v21, Landroid/widget/TextView;

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-virtual/range {v21 .. v25}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2180
    invoke-virtual {v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getText()Ljava/lang/String;

    move-result-object v13

    .local v13, "htmlText":Ljava/lang/String;
    move-object/from16 v21, v6

    .line 2181
    check-cast v21, Landroid/widget/TextView;

    const/16 v22, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->getTextSize(Ljava/lang/String;)I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    invoke-virtual/range {v21 .. v23}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_3

    .line 2197
    .end local v13    # "htmlText":Ljava/lang/String;
    :pswitch_4
    const v21, 0x7f0a0013

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 2198
    if-eqz v6, :cond_3

    .line 2199
    invoke-virtual {v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2200
    if-eqz v4, :cond_9

    move-object/from16 v21, v6

    .line 2201
    check-cast v21, Landroid/widget/ImageView;

    invoke-virtual {v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2203
    :cond_9
    const/16 v21, 0x2

    move/from16 v0, v21

    if-ne v11, v0, :cond_3

    .line 2204
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 2205
    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v21

    check-cast v21, Landroid/widget/FrameLayout;

    check-cast v21, Landroid/widget/FrameLayout;

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 2206
    const v21, 0x7f0a0019

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 2207
    .restart local v12    # "dim":Landroid/widget/ImageView;
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2213
    .end local v12    # "dim":Landroid/widget/ImageView;
    :pswitch_5
    invoke-virtual {v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getPath()Ljava/lang/String;

    move-result-object v21

    const-string v22, ""

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 2214
    const v21, 0x7f0a0012

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 2215
    if-eqz v6, :cond_3

    move-object/from16 v21, v6

    .line 2216
    check-cast v21, Landroid/widget/TextView;

    invoke-virtual {v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getUri()Landroid/net/Uri;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v21, v6

    .line 2217
    check-cast v21, Landroid/widget/TextView;

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->enableMultiSelection(Z)V

    goto/16 :goto_1

    .line 2220
    :cond_a
    const v21, 0x7f0a0013

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 2221
    if-eqz v6, :cond_3

    .line 2222
    invoke-virtual {v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2223
    if-eqz v4, :cond_3

    move-object/from16 v21, v6

    .line 2224
    check-cast v21, Landroid/widget/ImageView;

    invoke-virtual {v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1

    .line 2231
    :pswitch_6
    const v21, 0x7f0a0012

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 2232
    if-eqz v6, :cond_3

    move-object/from16 v21, v6

    .line 2233
    check-cast v21, Landroid/widget/TextView;

    invoke-virtual {v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getIntentValue()Landroid/content/Intent;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v21, v6

    .line 2234
    check-cast v21, Landroid/widget/TextView;

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->enableMultiSelection(Z)V

    goto/16 :goto_1

    .line 2239
    :pswitch_7
    const v21, 0x7f0a0015

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .end local v6    # "bodyView":Landroid/view/View;
    check-cast v6, Landroid/widget/LinearLayout;

    .line 2240
    .restart local v6    # "bodyView":Landroid/view/View;
    if-eqz v6, :cond_3

    .line 2241
    const v21, 0x7f0a0016

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 2242
    .local v10, "contentNameView":Landroid/widget/TextView;
    const v21, 0x7f0a0017

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 2243
    .local v9, "contentCountView":Landroid/widget/TextView;
    invoke-virtual {v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getMultiUri()Ljava/util/ArrayList;

    move-result-object v20

    .line 2244
    .local v20, "uries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/16 v16, 0x0

    .line 2245
    .local v16, "number":I
    if-eqz v20, :cond_b

    .line 2246
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v16

    .line 2248
    :cond_b
    if-lez v16, :cond_3

    .line 2249
    const/16 v21, 0x1

    move/from16 v0, v16

    move/from16 v1, v21

    if-ne v0, v1, :cond_c

    .line 2250
    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/net/Uri;

    .line 2251
    .local v19, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->getFilename(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 2253
    .end local v19    # "uri":Landroid/net/Uri;
    :cond_c
    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/net/Uri;

    goto/16 :goto_1

    .line 2128
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
