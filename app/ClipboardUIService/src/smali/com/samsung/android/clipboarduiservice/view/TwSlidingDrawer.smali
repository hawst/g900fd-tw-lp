.class public Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;
.super Landroid/widget/SlidingDrawer;
.source "TwSlidingDrawer.java"


# instance fields
.field private mArrayRect:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private mClipboardUIService:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

.field private mHandle:Landroid/view/View;

.field private mHandleId:I

.field private mHandleRect:Landroid/graphics/Rect;

.field private mIsSetRect:Z

.field public mTouched:Z

.field private mTrackingTouchForClose:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x0

    .line 66
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/SlidingDrawer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    iput-boolean v3, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mTouched:Z

    .line 42
    iput-boolean v3, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mIsSetRect:Z

    .line 43
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandleRect:Landroid/graphics/Rect;

    .line 44
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mTrackingTouchForClose:Z

    .line 46
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mArrayRect:Ljava/util/ArrayList;

    .line 68
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mContext:Landroid/content/Context;

    .line 70
    sget-object v2, Lcom/android/internal/R$styleable;->SlidingDrawer:[I

    invoke-virtual {p1, p2, v2, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 71
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 72
    .local v1, "handleId":I
    if-nez v1, :cond_0

    .line 73
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "The handle attribute is required and must refer to a valid child."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 77
    :cond_0
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandleId:I

    .line 79
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 81
    iput-boolean v3, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mIsSetRect:Z

    .line 82
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 86
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandleId:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandle:Landroid/view/View;

    .line 87
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandle:Landroid/view/View;

    if-nez v0, :cond_0

    .line 88
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The handle attribute is must refer to an existing child."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    invoke-super {p0}, Landroid/widget/SlidingDrawer;->onFinishInflate()V

    .line 93
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x0

    .line 97
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    .line 98
    .local v5, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    .line 99
    .local v6, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 101
    .local v0, "action":I
    if-nez v0, :cond_0

    .line 102
    iput-boolean v8, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mIsSetRect:Z

    .line 103
    new-instance v7, Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandle:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getLeft()I

    move-result v9

    iget-object v10, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandle:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v10

    iget-object v11, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandle:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v11

    iget-object v12, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandle:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getBottom()I

    move-result v12

    invoke-direct {v7, v9, v10, v11, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v7, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandleRect:Landroid/graphics/Rect;

    .line 106
    :cond_0
    iget-boolean v7, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mIsSetRect:Z

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandle:Landroid/view/View;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandle:Landroid/view/View;

    instance-of v7, v7, Landroid/view/ViewGroup;

    if-eqz v7, :cond_2

    .line 107
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v7, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandle:Landroid/view/View;

    check-cast v7, Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v7

    if-ge v3, v7, :cond_2

    .line 108
    iget-object v7, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandle:Landroid/view/View;

    check-cast v7, Landroid/view/ViewGroup;

    invoke-virtual {v7, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 109
    .local v2, "child":Landroid/view/View;
    if-eqz v2, :cond_1

    instance-of v7, v2, Landroid/widget/Button;

    if-eqz v7, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-nez v7, :cond_1

    .line 110
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 111
    .local v4, "rect":Landroid/graphics/Rect;
    invoke-virtual {v2, v4}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 112
    iget-object v7, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mArrayRect:Ljava/util/ArrayList;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mIsSetRect:Z

    .line 107
    .end local v4    # "rect":Landroid/graphics/Rect;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 119
    .end local v2    # "child":Landroid/view/View;
    .end local v3    # "i":I
    :cond_2
    const/4 v7, 0x2

    if-ne v0, v7, :cond_3

    .line 120
    iput-boolean v8, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mTrackingTouchForClose:Z

    move v7, v8

    .line 133
    :goto_1
    return v7

    .line 124
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandle:Landroid/view/View;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mArrayRect:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_5

    .line 125
    new-instance v1, Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mArrayRect:Ljava/util/ArrayList;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/Rect;

    iget v9, v7, Landroid/graphics/Rect;->left:I

    iget-object v7, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandle:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v10

    iget-object v7, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mArrayRect:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mArrayRect:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    iget-object v11, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandle:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getBottom()I

    move-result v11

    invoke-direct {v1, v9, v10, v7, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 126
    .local v1, "buttonFrame":Landroid/graphics/Rect;
    if-eqz v1, :cond_4

    float-to-int v7, v5

    float-to-int v9, v6

    invoke-virtual {v1, v7, v9}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_4

    iget-boolean v7, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mIsSetRect:Z

    if-eqz v7, :cond_4

    .line 127
    iput-boolean v8, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mTrackingTouchForClose:Z

    move v7, v8

    .line 128
    goto :goto_1

    .line 130
    :cond_4
    invoke-super {p0, p1}, Landroid/widget/SlidingDrawer;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v7

    goto :goto_1

    .line 133
    .end local v1    # "buttonFrame":Landroid/graphics/Rect;
    :cond_5
    invoke-super {p0, p1}, Landroid/widget/SlidingDrawer;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v7

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 139
    const/4 v1, 0x0

    .line 140
    .local v1, "downX":F
    const/4 v2, 0x0

    .line 141
    .local v2, "downY":F
    const/4 v4, 0x0

    .line 142
    .local v4, "upX":F
    const/4 v5, 0x0

    .line 144
    .local v5, "upY":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 145
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 167
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/SlidingDrawer;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    .line 170
    .local v3, "superResult":Z
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandleRect:Landroid/graphics/Rect;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandleRect:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandleRect:Landroid/graphics/Rect;

    float-to-int v7, v4

    float-to-int v8, v5

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-boolean v6, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mTrackingTouchForClose:Z

    if-eqz v6, :cond_3

    if-eq v0, v10, :cond_1

    const/4 v6, 0x3

    if-ne v0, v6, :cond_3

    .line 171
    :cond_1
    sget-boolean v6, Landroid/sec/clipboard/data/ClipboardDefine;->DEBUG:Z

    if-eqz v6, :cond_2

    const-string v6, "ClipboardServiceEx"

    const-string v7, "Clipboard will be closed because touch position exist in title area"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->animateClose()V

    .line 173
    iput-boolean v9, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mTrackingTouchForClose:Z

    .line 177
    :cond_3
    return v3

    .line 147
    .end local v3    # "superResult":Z
    :pswitch_0
    iput-boolean v10, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mTrackingTouchForClose:Z

    .line 148
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 149
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 150
    goto :goto_0

    .line 153
    :pswitch_1
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandleRect:Landroid/graphics/Rect;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mHandleRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-nez v6, :cond_0

    .line 154
    iget-boolean v6, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mTrackingTouchForClose:Z

    if-eqz v6, :cond_0

    .line 155
    sget-boolean v6, Landroid/sec/clipboard/data/ClipboardDefine;->DEBUG:Z

    if-eqz v6, :cond_4

    const-string v6, "ClipboardServiceEx"

    const-string v7, "mTrackingTouchForClose will be set to false because of touch event position"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :cond_4
    iput-boolean v9, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mTrackingTouchForClose:Z

    goto :goto_0

    .line 161
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 162
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    goto :goto_0

    .line 145
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setClipboardUIService(Lcom/samsung/android/clipboarduiservice/ClipboardUIService;)V
    .locals 0
    .param p1, "clipboarduiservice"    # Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->mClipboardUIService:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    .line 183
    return-void
.end method
