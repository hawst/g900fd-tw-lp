.class Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$15;
.super Ljava/lang/Object;
.source "ClipboardNormalDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->syncClipboardData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

.field final synthetic val$dataCount:I


# direct methods
.method constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)V
    .locals 0

    .prologue
    .line 2408
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$15;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iput p2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$15;->val$dataCount:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 2412
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$15;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    const/4 v3, 0x3

    # invokes: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->sendMessage(I)V
    invoke-static {v2, v3}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)V

    .line 2413
    const/4 v1, 0x0

    .local v1, "cnt":I
    :goto_0
    iget v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$15;->val$dataCount:I

    if-ge v1, v2, :cond_0

    .line 2414
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$15;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 2421
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$15;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    const/4 v3, 0x4

    # invokes: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->sendMessage(I)V
    invoke-static {v2, v3}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)V

    .line 2422
    return-void

    .line 2417
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$15;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # invokes: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->ClipDataToUIData(I)Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    invoke-static {v2, v1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$3000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    move-result-object v0

    .line 2418
    .local v0, "clipUIData":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$15;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2419
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$15;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    const/4 v3, 0x1

    # invokes: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->sendMessage(I)V
    invoke-static {v2, v3}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$2000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)V

    .line 2413
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
