.class Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;
.super Landroid/content/BroadcastReceiver;
.source "ClipboardNormalDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V
    .locals 0

    .prologue
    .line 893
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 896
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 897
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 898
    const-string v2, "reason"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 899
    .local v1, "reason":Ljava/lang/String;
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 900
    const-string v2, "ClipboardServiceEx"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reason :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 903
    :cond_0
    const-string v2, "homekey"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 904
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 905
    const-string v2, "ClipboardServiceEx"

    const-string v3, "clipboard dialog get ACTION_CLOSE_SYSTEM_DIALOGS"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 907
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 908
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mShouldBeDismissed:Z
    invoke-static {v2, v5}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1502(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z

    .line 909
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    .line 910
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 911
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->dismiss()V

    .line 944
    :cond_2
    :goto_0
    const-string v2, "lock"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 945
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardUIService:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1600(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->dismiss()V

    .line 994
    .end local v1    # "reason":Ljava/lang/String;
    :cond_3
    :goto_1
    return-void

    .line 914
    .restart local v1    # "reason":Ljava/lang/String;
    :cond_4
    const-string v2, "recentapps"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 915
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 916
    const-string v2, "ClipboardServiceEx"

    const-string v3, "clipboard dialog get SYSTEM_DIALOG_REASON_RECENT_APPS"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 919
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 920
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mShouldBeDismissed:Z
    invoke-static {v2, v5}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1502(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z

    .line 921
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    .line 922
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 923
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->dismiss()V

    goto :goto_0

    .line 927
    :cond_6
    const-string v2, "globalactions"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 929
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 930
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 931
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->dismiss()V

    goto :goto_0

    .line 934
    :cond_7
    if-nez v1, :cond_2

    .line 936
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 937
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mShouldBeDismissed:Z
    invoke-static {v2, v5}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1502(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z

    .line 938
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    .line 939
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 940
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->dismiss()V

    goto/16 :goto_0

    .line 947
    .end local v1    # "reason":Ljava/lang/String;
    :cond_8
    const-string v2, "DismissClipboardDialogFromIMMService"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 948
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCalledDismissIntentFromSIPFlag:Z
    invoke-static {v2, v5}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1702(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z

    .line 949
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 950
    const-string v2, "ClipboardServiceEx"

    const-string v3, "clipboard dialog get DismissClipboardDialogFromIMMService"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    :cond_9
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 953
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mShouldBeDismissed:Z
    invoke-static {v2, v5}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1502(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z

    .line 954
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    .line 955
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 956
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->dismiss()V

    .line 959
    :cond_a
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardUIService:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1600(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->dismiss()V

    goto/16 :goto_1

    .line 960
    :cond_b
    const-string v2, "DismissClipboardDialogFromPhoneStatusBar"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 961
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 962
    const-string v2, "ClipboardServiceEx"

    const-string v3, "clipboard dialog get DismissClipboardDialogFromPhoneStatusBar"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 964
    :cond_c
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 965
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mShouldBeDismissed:Z
    invoke-static {v2, v5}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1502(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z

    .line 966
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    .line 967
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 968
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->dismiss()V

    .line 971
    :cond_d
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardUIService:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1600(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->dismiss()V

    goto/16 :goto_1

    .line 972
    :cond_e
    const-string v2, "com.android.internal.policy.impl.sec.UserActivityByShortcut"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 973
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 974
    const-string v2, "ClipboardServiceEx"

    const-string v3, "clipboard dialog get DismissClipboardDialogFromShortCut"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 976
    :cond_f
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v2

    if-eqz v2, :cond_10

    .line 977
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mShouldBeDismissed:Z
    invoke-static {v2, v5}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1502(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z

    .line 978
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    .line 979
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 980
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v2, v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->dismiss()V

    .line 983
    :cond_10
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardUIService:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1600(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->dismiss()V

    goto/16 :goto_1

    .line 984
    :cond_11
    const-string v2, "android.intent.action.USER_PRESENT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 985
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 986
    const-string v2, "ClipboardServiceEx"

    const-string v3, "clipboard dialog get ACTION_USER_PRESENT"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 988
    :cond_12
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v2

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mShouldBeDismissed:Z
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 989
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/sec/clipboard/ClipboardExManager;->showUIDataDialog()V

    goto/16 :goto_1

    .line 991
    :cond_13
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardUIService:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1600(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->dismiss()V

    goto/16 :goto_1
.end method
