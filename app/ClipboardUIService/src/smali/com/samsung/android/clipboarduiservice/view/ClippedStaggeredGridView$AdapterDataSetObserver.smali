.class Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;
.super Landroid/database/DataSetObserver;
.source "ClippedStaggeredGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AdapterDataSetObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)V
    .locals 0

    .prologue
    .line 2368
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;
    .param p2, "x1"    # Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$1;

    .prologue
    .line 2368
    invoke-direct {p0, p1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2371
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    const/4 v4, 0x1

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDataChanged:Z
    invoke-static {v3, v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$402(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;Z)Z

    .line 2372
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAdapter:Landroid/widget/ListAdapter;
    invoke-static {v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$800(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Landroid/widget/ListAdapter;

    move-result-object v4

    invoke-interface {v4}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemCount:I
    invoke-static {v3, v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$702(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;I)I

    .line 2373
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildCount()I

    move-result v0

    .line 2376
    .local v0, "childCount":I
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRecycler:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$900(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$RecycleBin;->clearTransientViews()V

    .line 2379
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mHasStableIds:Z
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1000(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2384
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1100(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Landroid/support/v4/util/SparseArrayCompat;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    .line 2385
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # invokes: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->recycleAllViews()V
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1200(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)V

    .line 2389
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1300(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I

    move-result v1

    .line 2390
    .local v1, "colCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 2391
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1400(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)[I

    move-result-object v3

    array-length v3, v3

    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I
    invoke-static {v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1300(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I

    move-result v4

    if-le v3, v4, :cond_0

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1500(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)[I

    move-result-object v3

    array-length v3, v3

    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mColCount:I
    invoke-static {v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1300(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I

    move-result v4

    if-gt v3, v4, :cond_3

    .line 2400
    .end local v1    # "colCount":I
    .end local v2    # "i":I
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1600(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemCount:I
    invoke-static {v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$700(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-gt v3, v4, :cond_1

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mAddScenario:Z
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1700(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2402
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mDeleteScenario:Z
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1800(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2403
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # setter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mFirstPosition:I
    invoke-static {v3, v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1602(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;I)I

    .line 2404
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1500(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)[I

    move-result-object v3

    invoke-static {v3, v5}, Ljava/util/Arrays;->fill([II)V

    .line 2405
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1400(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)[I

    move-result-object v3

    invoke-static {v3, v5}, Ljava/util/Arrays;->fill([II)V

    .line 2406
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRestoreOffsets:[I
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1900(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)[I

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2407
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mRestoreOffsets:[I
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1900(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)[I

    move-result-object v3

    invoke-static {v3, v5}, Ljava/util/Arrays;->fill([II)V

    .line 2413
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->requestLayout()V

    .line 2414
    return-void

    .line 2394
    .restart local v1    # "colCount":I
    .restart local v2    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemBottoms:[I
    invoke-static {v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1400(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)[I

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView$AdapterDataSetObserver;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->mItemTops:[I
    invoke-static {v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->access$1500(Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;)[I

    move-result-object v4

    aget v4, v4, v2

    aput v4, v3, v2

    .line 2390
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public onInvalidated()V
    .locals 0

    .prologue
    .line 2419
    return-void
.end method
