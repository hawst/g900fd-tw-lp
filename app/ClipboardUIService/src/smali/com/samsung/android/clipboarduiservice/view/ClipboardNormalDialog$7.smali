.class Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$7;
.super Landroid/telephony/PhoneStateListener;
.source "ClipboardNormalDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V
    .locals 0

    .prologue
    .line 998
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$7;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 3
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 1000
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1001
    const-string v0, "ClipboardServiceEx"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clipboard onCallStateChanged state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1003
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_3

    .line 1004
    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$200()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1005
    const-string v0, "ClipboardServiceEx"

    const-string v1, "clipboard dialog get TelephonyManager.CALL_STATE_RINGING"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1007
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$7;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1008
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$7;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    .line 1009
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$7;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$7;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1010
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$7;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->dismiss()V

    .line 1013
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$7;->this$0:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardUIService:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;
    invoke-static {v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->access$1600(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->dismiss()V

    .line 1015
    :cond_3
    return-void
.end method
