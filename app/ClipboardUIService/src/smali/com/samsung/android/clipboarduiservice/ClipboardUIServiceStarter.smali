.class public Lcom/samsung/android/clipboarduiservice/ClipboardUIServiceStarter;
.super Landroid/app/Service;
.source "ClipboardUIServiceStarter.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private final SERVICE_NAME:Ljava/lang/String;

.field private mService:Landroid/os/IBinder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-string v0, "ClipboardUIServiceStarter"

    sput-object v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIServiceStarter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 14
    const-string v0, "clipboarduiservice"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIServiceStarter;->SERVICE_NAME:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 34
    const/4 v0, 0x0

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIServiceStarter;->mService:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIServiceStarter;->mService:Landroid/os/IBinder;

    check-cast v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    invoke-virtual {v0, p1}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 48
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Service;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 49
    return-void
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 20
    const-string v0, "clipboarduiservice"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_1

    .line 21
    invoke-static {p0}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->getInstance(Landroid/content/Context;)Landroid/os/IBinder;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIServiceStarter;->mService:Landroid/os/IBinder;

    .line 22
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIServiceStarter;->mService:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    .line 23
    const-string v0, "clipboarduiservice"

    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/ClipboardUIServiceStarter;->mService:Landroid/os/IBinder;

    invoke-static {v0, v1}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 30
    :goto_0
    return-void

    .line 25
    :cond_0
    sget-object v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIServiceStarter;->TAG:Ljava/lang/String;

    const-string v1, "Creating clipboard service object is failed."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 28
    :cond_1
    sget-object v0, Lcom/samsung/android/clipboarduiservice/ClipboardUIServiceStarter;->TAG:Ljava/lang/String;

    const-string v1, "clipboard service is already registered."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 39
    const/4 v0, 0x2

    return v0
.end method
