.class public final Lcom/samsung/android/clipboarduiservice/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final clipboard_content_panel_height:I = 0x7f05003d

.field public static final clipboard_content_panel_height_land:I = 0x7f050040

.field public static final clipboard_content_panel_hieght:I = 0x7f050008

.field public static final clipboard_gridview_horizontal_spacing:I = 0x7f050009

.field public static final clipboard_gridview_item_height:I = 0x7f05000f

.field public static final clipboard_gridview_item_width:I = 0x7f05000e

.field public static final clipboard_gridview_padding_botton:I = 0x7f05000b

.field public static final clipboard_gridview_padding_left:I = 0x7f05000c

.field public static final clipboard_gridview_padding_top:I = 0x7f05000d

.field public static final clipboard_gridview_vertical_spacing:I = 0x7f05000a

.field public static final clipboard_item_html_image_height:I = 0x7f050011

.field public static final clipboard_item_html_image_padding:I = 0x7f050012

.field public static final clipboard_item_html_image_width:I = 0x7f050010

.field public static final clipboard_item_html_padding_drawable:I = 0x7f050019

.field public static final clipboard_item_lock_icon_padding_bottom:I = 0x7f05001d

.field public static final clipboard_item_lock_icon_padding_left:I = 0x7f05001a

.field public static final clipboard_item_lock_icon_padding_right:I = 0x7f05001c

.field public static final clipboard_item_lock_icon_padding_top:I = 0x7f05001b

.field public static final clipboard_item_max_height:I = 0x7f050025

.field public static final clipboard_item_min_height:I = 0x7f050024

.field public static final clipboard_item_padding:I = 0x7f050027

.field public static final clipboard_item_text_padding:I = 0x7f050018

.field public static final clipboard_item_text_padding_bottom:I = 0x7f050016

.field public static final clipboard_item_text_padding_left:I = 0x7f050014

.field public static final clipboard_item_text_padding_right:I = 0x7f050017

.field public static final clipboard_item_text_padding_top:I = 0x7f050015

.field public static final clipboard_item_text_size:I = 0x7f050013

.field public static final clipboard_item_text_size_biggest:I = 0x7f050021

.field public static final clipboard_item_text_size_smallest:I = 0x7f050022

.field public static final clipboard_item_textview_height:I = 0x7f050023

.field public static final clipboard_item_textview_land_height:I = 0x7f050045

.field public static final clipboard_item_width:I = 0x7f050026

.field public static final clipboard_panel_clear_button_width:I = 0x7f050007

.field public static final clipboard_panel_height:I = 0x7f050001

.field public static final clipboard_panel_height_land:I = 0x7f05003f

.field public static final clipboard_panel_holo_image_height:I = 0x7f050006

.field public static final clipboard_panel_title_textview:I = 0x7f050002

.field public static final clipboard_panel_title_textview_margin_right:I = 0x7f050004

.field public static final clipboard_panel_title_textview_padding_left:I = 0x7f050003

.field public static final clipboard_panel_title_textview_text_size:I = 0x7f050005

.field public static final clipboard_preview_list_max_height:I = 0x7f050049

.field public static final clipboard_scrap_keep_activity_title_back_text_size:I = 0x7f050048

.field public static final clipboard_scrap_keep_activity_title_height:I = 0x7f050047

.field public static final clipboard_scrap_keep_dialog_width:I = 0x7f050046

.field public static final clipboard_scrapitem_height:I = 0x7f050042

.field public static final clipboard_scrapitem_height_land:I = 0x7f050044

.field public static final clipboard_scrapitem_width:I = 0x7f050041

.field public static final clipboard_scrapitem_width_land:I = 0x7f050043

.field public static final clipboard_sharevia_gridview_icon_height:I = 0x7f05001f

.field public static final clipboard_sharevia_gridview_icon_width:I = 0x7f050020

.field public static final clipboard_sharevia_gridview_top_padding:I = 0x7f05001e

.field public static final clipboard_slidingDrawer_height:I = 0x7f050000

.field public static final clipboard_slidingDrawer_height_land:I = 0x7f05003e

.field public static final easyclip_close_button_padding_right:I = 0x7f050034

.field public static final easyclip_close_button_padding_top:I = 0x7f050035

.field public static final easyclip_help_view_height:I = 0x7f050033

.field public static final easyclip_help_view_width:I = 0x7f050032

.field public static final easyclip_overlay_image_padding_left:I = 0x7f050037

.field public static final easyclip_overlay_image_padding_top:I = 0x7f050036

.field public static final easyclip_text_padding_left:I = 0x7f050039

.field public static final easyclip_text_padding_right:I = 0x7f05003a

.field public static final easyclip_text_padding_top:I = 0x7f050038

.field public static final easyclip_text_size:I = 0x7f05003b

.field public static final penable_close_button_padding_right:I = 0x7f05002a

.field public static final penable_close_button_padding_top:I = 0x7f05002b

.field public static final penable_help_view_height:I = 0x7f050029

.field public static final penable_help_view_width:I = 0x7f050028

.field public static final penable_overlay_image_padding_left:I = 0x7f05002d

.field public static final penable_overlay_image_padding_top:I = 0x7f05002c

.field public static final penable_pancard_width:I = 0x7f05003c

.field public static final penable_text_padding_left:I = 0x7f05002f

.field public static final penable_text_padding_right:I = 0x7f050030

.field public static final penable_text_padding_top:I = 0x7f05002e

.field public static final penable_text_size:I = 0x7f050031


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
