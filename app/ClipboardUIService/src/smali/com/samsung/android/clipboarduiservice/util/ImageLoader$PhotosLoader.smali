.class Lcom/samsung/android/clipboarduiservice/util/ImageLoader$PhotosLoader;
.super Ljava/lang/Object;
.source "ImageLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/clipboarduiservice/util/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PhotosLoader"
.end annotation


# instance fields
.field photoToLoad:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

.field final synthetic this$0:Lcom/samsung/android/clipboarduiservice/util/ImageLoader;


# direct methods
.method constructor <init>(Lcom/samsung/android/clipboarduiservice/util/ImageLoader;Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;)V
    .locals 0
    .param p2, "photoToLoad"    # Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$PhotosLoader;->this$0:Lcom/samsung/android/clipboarduiservice/util/ImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    iput-object p2, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$PhotosLoader;->photoToLoad:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    .line 101
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 107
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$PhotosLoader;->this$0:Lcom/samsung/android/clipboarduiservice/util/ImageLoader;

    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$PhotosLoader;->photoToLoad:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    invoke-virtual {v3, v4}, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->imageViewReused(Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 116
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$PhotosLoader;->this$0:Lcom/samsung/android/clipboarduiservice/util/ImageLoader;

    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$PhotosLoader;->photoToLoad:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    invoke-virtual {v3, v4}, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->getBitmap(Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    .line 111
    .local v1, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    new-instance v0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$PhotosLoader;->this$0:Lcom/samsung/android/clipboarduiservice/util/ImageLoader;

    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$PhotosLoader;->photoToLoad:Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;

    invoke-direct {v0, v3, v1, v4}, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;-><init>(Lcom/samsung/android/clipboarduiservice/util/ImageLoader;Landroid/graphics/drawable/BitmapDrawable;Lcom/samsung/android/clipboarduiservice/util/ImageLoader$ImageHolder;)V

    .line 112
    .local v0, "bd":Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/util/ImageLoader$PhotosLoader;->this$0:Lcom/samsung/android/clipboarduiservice/util/ImageLoader;

    iget-object v3, v3, Lcom/samsung/android/clipboarduiservice/util/ImageLoader;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 113
    .end local v0    # "bd":Lcom/samsung/android/clipboarduiservice/util/ImageLoader$BitmapDisplayer;
    .end local v1    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    :catch_0
    move-exception v2

    .line 114
    .local v2, "th":Ljava/lang/Throwable;
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
