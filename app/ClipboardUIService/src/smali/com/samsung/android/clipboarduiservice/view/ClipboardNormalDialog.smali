.class public Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
.super Landroid/app/Dialog;
.source "ClipboardNormalDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;,
        Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;,
        Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;,
        Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;
    }
.end annotation


# static fields
.field static CHILD_COUNT:I = 0x0

.field private static final DEBUG:Z

.field private static final MSG_NOTIFY_ADAPTER:I = 0x1

.field private static final MSG_NOTIFY_SYNC_END:I = 0x4

.field private static final MSG_NOTIFY_SYNC_START:I = 0x3

.field private static final MSG_SHOW_PROTECTED:I = 0x2

.field private static final TAG:Ljava/lang/String; = "ClipboardNormalDialog"


# instance fields
.field final ADD_ITEM:I

.field final CLIPS_CLEARED_BROADCAST:Ljava/lang/String;

.field final CLIPS_INFO:Ljava/lang/String;

.field final CLIP_REMOVED_BROADCAST:Ljava/lang/String;

.field final KNOX_VERSION:Ljava/lang/String;

.field final MAX_PROTECTED_COUNT:I

.field final SHARED_CLIPS_INFO:Ljava/lang/String;

.field mAdapter:Landroid/widget/BaseAdapter;

.field private mAddScenario:Z

.field protected mAnimationIsOnGoing:Z

.field private mBroadCastListener:Landroid/content/BroadcastReceiver;

.field private mButtonClick:Landroid/view/View$OnClickListener;

.field private mCalledDismissIntentFromSIPFlag:Z

.field private mCbm:Landroid/sec/clipboard/ClipboardExManager;

.field mClearButton:Landroid/widget/Button;

.field mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

.field private mClipBoardHeight:I

.field private mClipBoardPanelHeight:I

.field private mClipDrawer:Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;

.field private mClipboardDataList:Landroid/sec/clipboard/data/IClipboardDataList;

.field private mClipboardDataUiEvent:Landroid/sec/clipboard/IClipboardDataUiEvent;

.field private mClipboardUIService:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

.field private mClipboardWorkingFormUiInterface:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;

.field mContentPanel:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mCurrentOrientation:I

.field private mCurrentPasteEvent:Landroid/sec/clipboard/IClipboardDataPasteEvent;

.field private mDataCount:I

.field private mDeleteAnimDelay:I

.field final mDeleteAnimation:Landroid/view/animation/Animation$AnimationListener;

.field private mDeleteCount:I

.field private mDeleteScenario:Z

.field final mDismissIntentFromSIP:Ljava/lang/String;

.field final mDismissIntentFromShortCut:Ljava/lang/String;

.field final mDismissIntentFromStatusBar:Ljava/lang/String;

.field private mEnabledDismissIntent:Z

.field private mFirstPosition:I

.field mGridItemHeight:I

.field mGridItemWidth:I

.field mGridViewAniGoingOn:Z

.field mHTMLImageHeight:I

.field mHTMLImagePadding:I

.field mHTMLImageWidth:I

.field private final mHandler:Landroid/os/Handler;

.field private mIsAddedFilter:Z

.field private mIsDarkTheme:Z

.field private mIsDeletingItem:Z

.field private mIsInstalledMemo:Z

.field private mIsInstalledSMemo:Z

.field private mIsInstalledSNote:Z

.field private mIsProtect:Z

.field mIsShowingContextmenu:Z

.field private mIsSupportLockFunc:Z

.field mItemMaxHeight:I

.field mItemMinHeight:I

.field mItemPadding:I

.field mItemWidth:I

.field private mNeedSyncAfterFinish:Z

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPopupMenu:Landroid/widget/PopupMenu;

.field private mScrollEnable:Z

.field private mScrollY:I

.field private mSelectedIndex:I

.field private mSelectedX:I

.field private mSelectedY:I

.field private mShouldBeDismissed:Z

.field mShowingMenu:Landroid/view/Menu;

.field mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

.field protected mSyncIsOnGoing:Z

.field final mTextViewMaxLineLand:I

.field final mTextViewMaxLinePort:I

.field final mTmpInsets:Landroid/inputmethodservice/InputMethodService$Insets;

.field final mTmpLocation:[I

.field private mUIDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdatingView:Z

.field private mViewPosition:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 168
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    .line 241
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CHILD_COUNT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 544
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/util/GetFWResource;->getTWClipboardTheme()I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 180
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .line 182
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAdapter:Landroid/widget/BaseAdapter;

    .line 185
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mGridViewAniGoingOn:Z

    .line 186
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDataCount:I

    .line 187
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAddScenario:Z

    .line 188
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteScenario:Z

    .line 189
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mScrollEnable:Z

    .line 191
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteCount:I

    .line 193
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mGridItemWidth:I

    .line 194
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mGridItemHeight:I

    .line 196
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImageWidth:I

    .line 197
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImageHeight:I

    .line 198
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImagePadding:I

    .line 200
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemPadding:I

    .line 201
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemMinHeight:I

    .line 202
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemMaxHeight:I

    .line 203
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemWidth:I

    .line 205
    iput v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mTextViewMaxLinePort:I

    .line 206
    iput v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mTextViewMaxLineLand:I

    .line 208
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    .line 209
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataUiEvent:Landroid/sec/clipboard/IClipboardDataUiEvent;

    .line 210
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardWorkingFormUiInterface:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;

    .line 211
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataList:Landroid/sec/clipboard/data/IClipboardDataList;

    .line 217
    new-instance v0, Landroid/inputmethodservice/InputMethodService$Insets;

    invoke-direct {v0}, Landroid/inputmethodservice/InputMethodService$Insets;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mTmpInsets:Landroid/inputmethodservice/InputMethodService$Insets;

    .line 218
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mTmpLocation:[I

    .line 220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    .line 226
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipBoardHeight:I

    .line 227
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipBoardPanelHeight:I

    .line 228
    iput v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCurrentOrientation:I

    .line 230
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteAnimDelay:I

    .line 232
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCurrentPasteEvent:Landroid/sec/clipboard/IClipboardDataPasteEvent;

    .line 233
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I

    .line 234
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedX:I

    .line 235
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedY:I

    .line 236
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mScrollY:I

    .line 237
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mFirstPosition:I

    .line 239
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsProtect:Z

    .line 240
    iput v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->ADD_ITEM:I

    .line 243
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->MAX_PROTECTED_COUNT:I

    .line 246
    const-string v0, "DismissClipboardDialogFromPhoneStatusBar"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDismissIntentFromStatusBar:Ljava/lang/String;

    .line 247
    const-string v0, "DismissClipboardDialogFromIMMService"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDismissIntentFromSIP:Ljava/lang/String;

    .line 248
    const-string v0, "com.android.internal.policy.impl.sec.UserActivityByShortcut"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDismissIntentFromShortCut:Ljava/lang/String;

    .line 249
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCalledDismissIntentFromSIPFlag:Z

    .line 250
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mShouldBeDismissed:Z

    .line 252
    iput-boolean v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mEnabledDismissIntent:Z

    .line 254
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsInstalledSMemo:Z

    .line 255
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsInstalledSNote:Z

    .line 256
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsInstalledMemo:Z

    .line 260
    const-string v0, "clips.info"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CLIPS_INFO:Ljava/lang/String;

    .line 261
    const-string v0, "shared_clips.info"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->SHARED_CLIPS_INFO:Ljava/lang/String;

    .line 262
    const-string v0, "com.samsung.knox.clipboard.clipscleared"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CLIPS_CLEARED_BROADCAST:Ljava/lang/String;

    .line 263
    const-string v0, "com.samsung.knox.clipboard.clipremoved"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CLIP_REMOVED_BROADCAST:Ljava/lang/String;

    .line 264
    const-string v0, "2.0"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->KNOX_VERSION:Ljava/lang/String;

    .line 268
    iput-boolean v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsDarkTheme:Z

    .line 270
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsAddedFilter:Z

    .line 272
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsDeletingItem:Z

    .line 274
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSyncIsOnGoing:Z

    .line 276
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAnimationIsOnGoing:Z

    .line 278
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mNeedSyncAfterFinish:Z

    .line 280
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUpdatingView:Z

    .line 282
    iput-boolean v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsSupportLockFunc:Z

    .line 284
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$1;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mButtonClick:Landroid/view/View$OnClickListener;

    .line 297
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearButton:Landroid/widget/Button;

    .line 298
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    .line 893
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mBroadCastListener:Landroid/content/BroadcastReceiver;

    .line 998
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$7;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 1053
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsShowingContextmenu:Z

    .line 1187
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteAnimation:Landroid/view/animation/Animation$AnimationListener;

    .line 1611
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHandler:Landroid/os/Handler;

    .line 545
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    .line 547
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/sec/clipboard/ClipboardExManager;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mgr"    # Landroid/sec/clipboard/ClipboardExManager;

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 557
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/util/GetFWResource;->getTWClipboardTheme()I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 180
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .line 182
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAdapter:Landroid/widget/BaseAdapter;

    .line 185
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mGridViewAniGoingOn:Z

    .line 186
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDataCount:I

    .line 187
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAddScenario:Z

    .line 188
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteScenario:Z

    .line 189
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mScrollEnable:Z

    .line 191
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteCount:I

    .line 193
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mGridItemWidth:I

    .line 194
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mGridItemHeight:I

    .line 196
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImageWidth:I

    .line 197
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImageHeight:I

    .line 198
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImagePadding:I

    .line 200
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemPadding:I

    .line 201
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemMinHeight:I

    .line 202
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemMaxHeight:I

    .line 203
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemWidth:I

    .line 205
    iput v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mTextViewMaxLinePort:I

    .line 206
    iput v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mTextViewMaxLineLand:I

    .line 208
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    .line 209
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataUiEvent:Landroid/sec/clipboard/IClipboardDataUiEvent;

    .line 210
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardWorkingFormUiInterface:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;

    .line 211
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataList:Landroid/sec/clipboard/data/IClipboardDataList;

    .line 217
    new-instance v0, Landroid/inputmethodservice/InputMethodService$Insets;

    invoke-direct {v0}, Landroid/inputmethodservice/InputMethodService$Insets;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mTmpInsets:Landroid/inputmethodservice/InputMethodService$Insets;

    .line 218
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mTmpLocation:[I

    .line 220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    .line 226
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipBoardHeight:I

    .line 227
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipBoardPanelHeight:I

    .line 228
    iput v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCurrentOrientation:I

    .line 230
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteAnimDelay:I

    .line 232
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCurrentPasteEvent:Landroid/sec/clipboard/IClipboardDataPasteEvent;

    .line 233
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I

    .line 234
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedX:I

    .line 235
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedY:I

    .line 236
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mScrollY:I

    .line 237
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mFirstPosition:I

    .line 239
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsProtect:Z

    .line 240
    iput v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->ADD_ITEM:I

    .line 243
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->MAX_PROTECTED_COUNT:I

    .line 246
    const-string v0, "DismissClipboardDialogFromPhoneStatusBar"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDismissIntentFromStatusBar:Ljava/lang/String;

    .line 247
    const-string v0, "DismissClipboardDialogFromIMMService"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDismissIntentFromSIP:Ljava/lang/String;

    .line 248
    const-string v0, "com.android.internal.policy.impl.sec.UserActivityByShortcut"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDismissIntentFromShortCut:Ljava/lang/String;

    .line 249
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCalledDismissIntentFromSIPFlag:Z

    .line 250
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mShouldBeDismissed:Z

    .line 252
    iput-boolean v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mEnabledDismissIntent:Z

    .line 254
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsInstalledSMemo:Z

    .line 255
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsInstalledSNote:Z

    .line 256
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsInstalledMemo:Z

    .line 260
    const-string v0, "clips.info"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CLIPS_INFO:Ljava/lang/String;

    .line 261
    const-string v0, "shared_clips.info"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->SHARED_CLIPS_INFO:Ljava/lang/String;

    .line 262
    const-string v0, "com.samsung.knox.clipboard.clipscleared"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CLIPS_CLEARED_BROADCAST:Ljava/lang/String;

    .line 263
    const-string v0, "com.samsung.knox.clipboard.clipremoved"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CLIP_REMOVED_BROADCAST:Ljava/lang/String;

    .line 264
    const-string v0, "2.0"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->KNOX_VERSION:Ljava/lang/String;

    .line 268
    iput-boolean v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsDarkTheme:Z

    .line 270
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsAddedFilter:Z

    .line 272
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsDeletingItem:Z

    .line 274
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSyncIsOnGoing:Z

    .line 276
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAnimationIsOnGoing:Z

    .line 278
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mNeedSyncAfterFinish:Z

    .line 280
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUpdatingView:Z

    .line 282
    iput-boolean v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsSupportLockFunc:Z

    .line 284
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$1;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mButtonClick:Landroid/view/View$OnClickListener;

    .line 297
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearButton:Landroid/widget/Button;

    .line 298
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    .line 893
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mBroadCastListener:Landroid/content/BroadcastReceiver;

    .line 998
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$7;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 1053
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsShowingContextmenu:Z

    .line 1187
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteAnimation:Landroid/view/animation/Animation$AnimationListener;

    .line 1611
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHandler:Landroid/os/Handler;

    .line 558
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    .line 560
    iput-object p2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    .line 561
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/clipboarduiservice/ClipboardUIService;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cuiservice"    # Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 550
    invoke-static {}, Lcom/samsung/android/clipboarduiservice/util/GetFWResource;->getTWClipboardTheme()I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 180
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .line 182
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAdapter:Landroid/widget/BaseAdapter;

    .line 185
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mGridViewAniGoingOn:Z

    .line 186
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDataCount:I

    .line 187
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAddScenario:Z

    .line 188
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteScenario:Z

    .line 189
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mScrollEnable:Z

    .line 191
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteCount:I

    .line 193
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mGridItemWidth:I

    .line 194
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mGridItemHeight:I

    .line 196
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImageWidth:I

    .line 197
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImageHeight:I

    .line 198
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImagePadding:I

    .line 200
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemPadding:I

    .line 201
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemMinHeight:I

    .line 202
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemMaxHeight:I

    .line 203
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemWidth:I

    .line 205
    iput v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mTextViewMaxLinePort:I

    .line 206
    iput v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mTextViewMaxLineLand:I

    .line 208
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    .line 209
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataUiEvent:Landroid/sec/clipboard/IClipboardDataUiEvent;

    .line 210
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardWorkingFormUiInterface:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;

    .line 211
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataList:Landroid/sec/clipboard/data/IClipboardDataList;

    .line 217
    new-instance v0, Landroid/inputmethodservice/InputMethodService$Insets;

    invoke-direct {v0}, Landroid/inputmethodservice/InputMethodService$Insets;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mTmpInsets:Landroid/inputmethodservice/InputMethodService$Insets;

    .line 218
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mTmpLocation:[I

    .line 220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    .line 226
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipBoardHeight:I

    .line 227
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipBoardPanelHeight:I

    .line 228
    iput v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCurrentOrientation:I

    .line 230
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteAnimDelay:I

    .line 232
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCurrentPasteEvent:Landroid/sec/clipboard/IClipboardDataPasteEvent;

    .line 233
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I

    .line 234
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedX:I

    .line 235
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedY:I

    .line 236
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mScrollY:I

    .line 237
    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mFirstPosition:I

    .line 239
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsProtect:Z

    .line 240
    iput v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->ADD_ITEM:I

    .line 243
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->MAX_PROTECTED_COUNT:I

    .line 246
    const-string v0, "DismissClipboardDialogFromPhoneStatusBar"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDismissIntentFromStatusBar:Ljava/lang/String;

    .line 247
    const-string v0, "DismissClipboardDialogFromIMMService"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDismissIntentFromSIP:Ljava/lang/String;

    .line 248
    const-string v0, "com.android.internal.policy.impl.sec.UserActivityByShortcut"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDismissIntentFromShortCut:Ljava/lang/String;

    .line 249
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCalledDismissIntentFromSIPFlag:Z

    .line 250
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mShouldBeDismissed:Z

    .line 252
    iput-boolean v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mEnabledDismissIntent:Z

    .line 254
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsInstalledSMemo:Z

    .line 255
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsInstalledSNote:Z

    .line 256
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsInstalledMemo:Z

    .line 260
    const-string v0, "clips.info"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CLIPS_INFO:Ljava/lang/String;

    .line 261
    const-string v0, "shared_clips.info"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->SHARED_CLIPS_INFO:Ljava/lang/String;

    .line 262
    const-string v0, "com.samsung.knox.clipboard.clipscleared"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CLIPS_CLEARED_BROADCAST:Ljava/lang/String;

    .line 263
    const-string v0, "com.samsung.knox.clipboard.clipremoved"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CLIP_REMOVED_BROADCAST:Ljava/lang/String;

    .line 264
    const-string v0, "2.0"

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->KNOX_VERSION:Ljava/lang/String;

    .line 268
    iput-boolean v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsDarkTheme:Z

    .line 270
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsAddedFilter:Z

    .line 272
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsDeletingItem:Z

    .line 274
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSyncIsOnGoing:Z

    .line 276
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAnimationIsOnGoing:Z

    .line 278
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mNeedSyncAfterFinish:Z

    .line 280
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUpdatingView:Z

    .line 282
    iput-boolean v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsSupportLockFunc:Z

    .line 284
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$1;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mButtonClick:Landroid/view/View$OnClickListener;

    .line 297
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearButton:Landroid/widget/Button;

    .line 298
    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    .line 893
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$6;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mBroadCastListener:Landroid/content/BroadcastReceiver;

    .line 998
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$7;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 1053
    iput-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsShowingContextmenu:Z

    .line 1187
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$8;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteAnimation:Landroid/view/animation/Animation$AnimationListener;

    .line 1611
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$11;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHandler:Landroid/os/Handler;

    .line 551
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    .line 552
    iput-object p2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardUIService:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    .line 554
    return-void
.end method

.method private ClipDataToUIData(I)Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    .locals 11
    .param p1, "index"    # I

    .prologue
    .line 2428
    invoke-direct {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getItemWidth()I

    move-result v8

    iput v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemWidth:I

    .line 2429
    const/4 v1, 0x0

    .line 2430
    .local v1, "cbData":Landroid/sec/clipboard/data/ClipboardData;
    const/4 v2, 0x0

    .line 2431
    .local v2, "clipData":Landroid/content/ClipData;
    const/4 v3, 0x0

    .line 2432
    .local v3, "clipUIData":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    const/4 v7, 0x0

    .line 2435
    .local v7, "protect":Z
    :try_start_0
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardUIService:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->setClipboardUIMode(I)V

    .line 2436
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataList:Landroid/sec/clipboard/data/IClipboardDataList;

    invoke-interface {v8, p1}, Landroid/sec/clipboard/data/IClipboardDataList;->getItem(I)Landroid/sec/clipboard/data/ClipboardData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2441
    :goto_0
    if-nez v1, :cond_0

    .line 2442
    const/4 v8, 0x0

    .line 2497
    .end local v1    # "cbData":Landroid/sec/clipboard/data/ClipboardData;
    :goto_1
    return-object v8

    .line 2437
    .restart local v1    # "cbData":Landroid/sec/clipboard/data/ClipboardData;
    :catch_0
    move-exception v4

    .line 2438
    .local v4, "e":Landroid/os/RemoteException;
    invoke-virtual {v4}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 2445
    .end local v4    # "e":Landroid/os/RemoteException;
    :cond_0
    invoke-virtual {v1}, Landroid/sec/clipboard/data/ClipboardData;->GetFomat()I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_4

    move-object v8, v1

    .line 2446
    check-cast v8, Landroid/sec/clipboard/data/list/ClipboardDataText;

    invoke-virtual {v8}, Landroid/sec/clipboard/data/list/ClipboardDataText;->getClipData()Landroid/content/ClipData;

    move-result-object v2

    move-object v8, v1

    .line 2447
    check-cast v8, Landroid/sec/clipboard/data/list/ClipboardDataText;

    invoke-virtual {v8}, Landroid/sec/clipboard/data/list/ClipboardDataText;->GetProtectState()Z

    move-result v7

    .line 2448
    new-instance v3, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    .end local v3    # "clipUIData":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    const/4 v9, 0x2

    invoke-direct {v3, v8, v2, v9}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;-><init>(Landroid/content/Context;Landroid/content/ClipData;I)V

    .line 2449
    .restart local v3    # "clipUIData":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    invoke-virtual {v3, v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setProtect(Z)V

    .line 2480
    :cond_1
    :goto_2
    if-eqz v3, :cond_3

    .line 2481
    invoke-virtual {v1}, Landroid/sec/clipboard/data/ClipboardData;->GetFomat()I

    move-result v8

    const/4 v9, 0x4

    if-ne v8, v9, :cond_a

    .line 2482
    const-string v8, ""

    invoke-virtual {v3}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    const-string v8, "/"

    invoke-virtual {v3}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 2483
    :cond_2
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2484
    .local v5, "mRecycleBitmapList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/ref/WeakReference<Landroid/graphics/Bitmap;>;>;"
    const/4 v0, 0x0

    .line 2485
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    check-cast v1, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;

    .end local v1    # "cbData":Landroid/sec/clipboard/data/ClipboardData;
    iget v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImageWidth:I

    iget v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImageHeight:I

    invoke-virtual {v1, v8, v9}, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;->getFirstImage(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2486
    iget v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImageWidth:I

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9, v0}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->settingBitmap(IILandroid/graphics/Bitmap;)V

    .line 2487
    if-eqz v0, :cond_3

    .line 2488
    new-instance v8, Ljava/lang/ref/WeakReference;

    invoke-direct {v8, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "mRecycleBitmapList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/ref/WeakReference<Landroid/graphics/Bitmap;>;>;"
    :cond_3
    :goto_3
    move-object v8, v3

    .line 2497
    goto :goto_1

    .line 2450
    .restart local v1    # "cbData":Landroid/sec/clipboard/data/ClipboardData;
    :cond_4
    invoke-virtual {v1}, Landroid/sec/clipboard/data/ClipboardData;->GetFomat()I

    move-result v8

    const/4 v9, 0x3

    if-ne v8, v9, :cond_5

    move-object v8, v1

    .line 2451
    check-cast v8, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;

    invoke-virtual {v8}, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;->getClipData()Landroid/content/ClipData;

    move-result-object v2

    move-object v8, v1

    .line 2452
    check-cast v8, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;

    invoke-virtual {v8}, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;->GetProtectState()Z

    move-result v7

    .line 2453
    new-instance v3, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    .end local v3    # "clipUIData":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    const/4 v9, 0x3

    invoke-direct {v3, v8, v2, v9}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;-><init>(Landroid/content/Context;Landroid/content/ClipData;I)V

    .line 2454
    .restart local v3    # "clipUIData":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    invoke-virtual {v3, v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setProtect(Z)V

    goto :goto_2

    .line 2455
    :cond_5
    invoke-virtual {v1}, Landroid/sec/clipboard/data/ClipboardData;->GetFomat()I

    move-result v8

    const/4 v9, 0x4

    if-ne v8, v9, :cond_6

    move-object v8, v1

    .line 2456
    check-cast v8, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;

    invoke-virtual {v8}, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;->getClipData()Landroid/content/ClipData;

    move-result-object v2

    move-object v8, v1

    .line 2457
    check-cast v8, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;

    invoke-virtual {v8}, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;->GetProtectState()Z

    move-result v7

    .line 2458
    new-instance v3, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    .end local v3    # "clipUIData":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    const/4 v9, 0x4

    invoke-direct {v3, v8, v2, v9}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;-><init>(Landroid/content/Context;Landroid/content/ClipData;I)V

    .line 2459
    .restart local v3    # "clipUIData":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    invoke-virtual {v3, v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setProtect(Z)V

    goto/16 :goto_2

    .line 2460
    :cond_6
    invoke-virtual {v1}, Landroid/sec/clipboard/data/ClipboardData;->GetFomat()I

    move-result v8

    const/16 v9, 0xa

    if-ne v8, v9, :cond_7

    move-object v8, v1

    .line 2461
    check-cast v8, Landroid/sec/clipboard/data/list/ClipboardDataScrapBitmap;

    invoke-virtual {v8}, Landroid/sec/clipboard/data/list/ClipboardDataScrapBitmap;->getClipData()Landroid/content/ClipData;

    move-result-object v2

    move-object v8, v1

    .line 2462
    check-cast v8, Landroid/sec/clipboard/data/list/ClipboardDataScrapBitmap;

    invoke-virtual {v8}, Landroid/sec/clipboard/data/list/ClipboardDataScrapBitmap;->GetProtectState()Z

    move-result v7

    .line 2463
    new-instance v3, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    .end local v3    # "clipUIData":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    const/16 v9, 0xa

    invoke-direct {v3, v8, v2, v9}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;-><init>(Landroid/content/Context;Landroid/content/ClipData;I)V

    .line 2464
    .restart local v3    # "clipUIData":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    invoke-virtual {v3, v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setProtect(Z)V

    goto/16 :goto_2

    .line 2465
    :cond_7
    invoke-virtual {v1}, Landroid/sec/clipboard/data/ClipboardData;->GetFomat()I

    move-result v8

    const/4 v9, 0x5

    if-ne v8, v9, :cond_1

    move-object v8, v1

    .line 2466
    check-cast v8, Landroid/sec/clipboard/data/list/ClipboardDataUri;

    invoke-virtual {v8}, Landroid/sec/clipboard/data/list/ClipboardDataUri;->getClipData()Landroid/content/ClipData;

    move-result-object v2

    move-object v8, v1

    .line 2467
    check-cast v8, Landroid/sec/clipboard/data/list/ClipboardDataUri;

    invoke-virtual {v8}, Landroid/sec/clipboard/data/list/ClipboardDataUri;->GetProtectState()Z

    move-result v7

    .line 2468
    new-instance v3, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    .end local v3    # "clipUIData":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    iget-object v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    const/4 v9, 0x5

    invoke-direct {v3, v8, v2, v9}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;-><init>(Landroid/content/Context;Landroid/content/ClipData;I)V

    .line 2469
    .restart local v3    # "clipUIData":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    invoke-virtual {v3, v7}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setProtect(Z)V

    move-object v8, v1

    .line 2471
    check-cast v8, Landroid/sec/clipboard/data/list/ClipboardDataUri;

    invoke-virtual {v8}, Landroid/sec/clipboard/data/list/ClipboardDataUri;->getPreviewImgPath()Ljava/lang/String;

    move-result-object v6

    .line 2472
    .local v6, "previewPath":Ljava/lang/String;
    invoke-virtual {v3, v6}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setPath(Ljava/lang/String;)V

    .line 2474
    const-string v8, "jpg"

    invoke-virtual {v6, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_8

    const-string v8, "png"

    invoke-virtual {v6, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_8

    const-string v8, "bmp"

    invoke-virtual {v6, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_8

    const-string v8, "gif"

    invoke-virtual {v6, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2475
    :cond_8
    invoke-virtual {v3}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->settingBitmap()V

    goto/16 :goto_2

    .line 2491
    .end local v6    # "previewPath":Ljava/lang/String;
    :cond_9
    iget v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImageWidth:I

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->settingBitmap(II)V

    goto/16 :goto_3

    .line 2494
    :cond_a
    iget v8, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemWidth:I

    iget v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemMinHeight:I

    iget v10, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemMaxHeight:I

    invoke-virtual {v3, v8, v9, v10}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->settingBitmap(III)V

    goto/16 :goto_3
.end method

.method static synthetic access$000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/ClipboardExManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAddScenario:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAddScenario:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .param p1, "x1"    # I

    .prologue
    .line 166
    iput p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteAnimDelay:I

    return p1
.end method

.method static synthetic access$1212(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .param p1, "x1"    # I

    .prologue
    .line 166
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteAnimDelay:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteAnimDelay:I

    return v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mScrollEnable:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mScrollEnable:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mShouldBeDismissed:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mShouldBeDismissed:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Lcom/samsung/android/clipboarduiservice/ClipboardUIService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardUIService:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCalledDismissIntentFromSIPFlag:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I

    return v0
.end method

.method static synthetic access$1902(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsDeletingItem:Z

    return p1
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 166
    sget-boolean v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .param p1, "x1"    # I

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->sendMessage(I)V

    return-void
.end method

.method static synthetic access$2100(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipDrawer:Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->updateClearLayout()V

    return-void
.end method

.method static synthetic access$2300(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mNeedSyncAfterFinish:Z

    return v0
.end method

.method static synthetic access$2302(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mNeedSyncAfterFinish:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->syncClipboardData()V

    return-void
.end method

.method static synthetic access$2700(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/data/IClipboardDataList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataList:Landroid/sec/clipboard/data/IClipboardDataList;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Landroid/sec/clipboard/data/IClipboardDataList;)Landroid/sec/clipboard/data/IClipboardDataList;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .param p1, "x1"    # Landroid/sec/clipboard/data/IClipboardDataList;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataList:Landroid/sec/clipboard/data/IClipboardDataList;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDataCount:I

    return v0
.end method

.method static synthetic access$2802(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .param p1, "x1"    # I

    .prologue
    .line 166
    iput p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDataCount:I

    return p1
.end method

.method static synthetic access$2900(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mFirstPosition:I

    return v0
.end method

.method static synthetic access$2902(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .param p1, "x1"    # I

    .prologue
    .line 166
    iput p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mFirstPosition:I

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .param p1, "x1"    # I

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->isCheckProtectedItem(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3000(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .param p1, "x1"    # I

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->ClipDataToUIData(I)Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Landroid/sec/clipboard/IClipboardDataUiEvent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataUiEvent:Landroid/sec/clipboard/IClipboardDataUiEvent;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Landroid/sec/clipboard/IClipboardDataUiEvent;)Landroid/sec/clipboard/IClipboardDataUiEvent;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .param p1, "x1"    # Landroid/sec/clipboard/IClipboardDataUiEvent;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataUiEvent:Landroid/sec/clipboard/IClipboardDataUiEvent;

    return-object p1
.end method

.method static synthetic access$500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsDarkTheme:Z

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsSupportLockFunc:Z

    return v0
.end method

.method static synthetic access$900(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteScenario:Z

    return v0
.end method

.method static synthetic access$902(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteScenario:Z

    return p1
.end method

.method private getItemWidth()I
    .locals 6

    .prologue
    const v5, 0x7f05000c

    .line 2501
    iget v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemWidth:I

    .line 2502
    .local v2, "width":I
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 2504
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    sub-int v1, v3, v4

    .line 2510
    .local v1, "staggeredViewWidth":I
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05000a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sub-int v3, v1, v3

    iget v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemPadding:I

    iget-object v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getColumnCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    mul-int/2addr v4, v5

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v4}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getColumnCount()I

    move-result v4

    div-int v2, v3, v4

    .line 2516
    sget-boolean v3, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 2517
    const-string v3, "ClipboardNormalDialog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getItemWidth() staggeredViewWidth : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mItemPadding : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemPadding:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", width : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2521
    :cond_0
    return v2
.end method

.method private isCheckProtectedItem(I)Z
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 1020
    const/4 v2, 0x0

    .line 1022
    .local v2, "isProtected":Z
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataList:Landroid/sec/clipboard/data/IClipboardDataList;

    invoke-interface {v3, p1}, Landroid/sec/clipboard/data/IClipboardDataList;->getItem(I)Landroid/sec/clipboard/data/ClipboardData;

    move-result-object v0

    .line 1023
    .local v0, "data":Landroid/sec/clipboard/data/ClipboardData;
    if-eqz v0, :cond_0

    .line 1024
    invoke-virtual {v0}, Landroid/sec/clipboard/data/ClipboardData;->GetProtectState()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1030
    .end local v0    # "data":Landroid/sec/clipboard/data/ClipboardData;
    :cond_0
    :goto_0
    return v2

    .line 1026
    :catch_0
    move-exception v1

    .line 1028
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private isProtectedPossible()Z
    .locals 6

    .prologue
    const/16 v5, 0xa

    .line 1035
    iget v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDataCount:I

    .line 1036
    .local v0, "currentSize":I
    if-le v0, v5, :cond_1

    .line 1038
    const/4 v3, 0x0

    .line 1040
    .local v3, "protectedCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 1041
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    .line 1042
    .local v1, "data":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getProtect()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1043
    add-int/lit8 v3, v3, 0x1

    .line 1044
    if-lt v3, v5, :cond_0

    .line 1045
    const/4 v4, 0x0

    .line 1050
    .end local v1    # "data":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    .end local v2    # "i":I
    .end local v3    # "protectedCount":I
    :goto_1
    return v4

    .line 1040
    .restart local v1    # "data":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    .restart local v2    # "i":I
    .restart local v3    # "protectedCount":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1050
    .end local v1    # "data":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    .end local v2    # "i":I
    .end local v3    # "protectedCount":I
    :cond_1
    const/4 v4, 0x1

    goto :goto_1
.end method

.method private postDrawerToUpdateClearLayout(Z)V
    .locals 3
    .param p1, "isShow"    # Z

    .prologue
    .line 1546
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 1547
    .local v0, "visibility":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipDrawer:Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;

    if-eqz v1, :cond_0

    .line 1548
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipDrawer:Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;

    new-instance v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$10;

    invoke-direct {v2, p0, v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$10;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->post(Ljava/lang/Runnable;)Z

    .line 1563
    :cond_0
    return-void

    .line 1546
    .end local v0    # "visibility":I
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private sendMessage(I)V
    .locals 1
    .param p1, "what"    # I

    .prologue
    .line 1653
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 1659
    :goto_0
    return-void

    .line 1657
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1658
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method private syncClipboardData()V
    .locals 7

    .prologue
    .line 2381
    sget-boolean v5, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v5, :cond_0

    .line 2382
    const-string v5, "ClipboardNormalDialog"

    const-string v6, "syncClipboardData()"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2385
    :cond_0
    iget-boolean v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSyncIsOnGoing:Z

    if-eqz v5, :cond_1

    .line 2386
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mNeedSyncAfterFinish:Z

    .line 2425
    :goto_0
    return-void

    .line 2390
    :cond_1
    const/4 v3, 0x0

    .local v3, "index":I
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_2

    .line 2391
    iget-object v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    invoke-virtual {v5}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->recycle()V

    .line 2390
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2395
    :cond_2
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataList:Landroid/sec/clipboard/data/IClipboardDataList;

    invoke-interface {v5}, Landroid/sec/clipboard/data/IClipboardDataList;->size()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDataCount:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2400
    :goto_2
    iget v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDataCount:I

    .line 2402
    .local v1, "dataCount":I
    iget-object v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 2404
    const/4 v0, 0x0

    .local v0, "cnt":I
    :goto_3
    if-ge v0, v1, :cond_3

    .line 2405
    iget-object v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2404
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2396
    .end local v0    # "cnt":I
    .end local v1    # "dataCount":I
    :catch_0
    move-exception v2

    .line 2398
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 2408
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v0    # "cnt":I
    .restart local v1    # "dataCount":I
    :cond_3
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$15;

    invoke-direct {v5, p0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$15;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;I)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2424
    .local v4, "thread":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private updateClearLayout()V
    .locals 5

    .prologue
    .line 1566
    const/4 v1, 0x0

    .line 1567
    .local v1, "isShow":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDataCount:I

    if-ge v0, v2, :cond_1

    .line 1568
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getProtect()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1569
    sget-boolean v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 1570
    const-string v2, "ClipboardServiceEx"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "some item is not unlocked...index :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1572
    :cond_0
    const/4 v1, 0x1

    .line 1576
    :cond_1
    invoke-direct {p0, v1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->postDrawerToUpdateClearLayout(Z)V

    .line 1577
    return-void

    .line 1567
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public IsSetProtectAction(I)V
    .locals 13
    .param p1, "selectItem"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v9, 0x0

    .line 1486
    :try_start_0
    iget-object v10, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    .line 1487
    .local v2, "cilpUIdata":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    if-nez v2, :cond_0

    .line 1543
    .end local v2    # "cilpUIdata":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    :goto_0
    return-void

    .line 1490
    .restart local v2    # "cilpUIdata":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    :cond_0
    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->getProtect()Z

    move-result v10

    if-nez v10, :cond_1

    .line 1493
    .local v6, "isProtected":Z
    :goto_1
    if-eqz v6, :cond_3

    invoke-direct {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->isProtectedPossible()Z

    move-result v9

    if-nez v9, :cond_3

    .line 1496
    iget-boolean v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsDarkTheme:Z

    if-eqz v9, :cond_2

    .line 1497
    new-instance v4, Landroid/view/ContextThemeWrapper;

    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/android/clipboarduiservice/util/GetFWResource;->getDefaultTheme()I

    move-result v10

    invoke-direct {v4, v9, v10}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 1501
    .local v4, "context":Landroid/content/Context;
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f08000a

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/16 v12, 0xa

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v4, v9, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1538
    .end local v2    # "cilpUIdata":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    .end local v4    # "context":Landroid/content/Context;
    .end local v6    # "isProtected":Z
    :catch_0
    move-exception v5

    .line 1539
    .local v5, "e":Landroid/os/RemoteException;
    invoke-virtual {v5}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .end local v5    # "e":Landroid/os/RemoteException;
    .restart local v2    # "cilpUIdata":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    :cond_1
    move v6, v9

    .line 1490
    goto :goto_1

    .line 1499
    .restart local v6    # "isProtected":Z
    :cond_2
    :try_start_1
    new-instance v4, Landroid/view/ContextThemeWrapper;

    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/android/clipboarduiservice/util/GetFWResource;->getDefaultThemeLight()I

    move-result v10

    invoke-direct {v4, v9, v10}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .restart local v4    # "context":Landroid/content/Context;
    goto :goto_2

    .line 1506
    .end local v4    # "context":Landroid/content/Context;
    :cond_3
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    invoke-virtual {v9, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    invoke-virtual {v9, v6}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->setProtect(Z)V

    .line 1507
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataList:Landroid/sec/clipboard/data/IClipboardDataList;

    invoke-interface {v9, p1}, Landroid/sec/clipboard/data/IClipboardDataList;->getItem(I)Landroid/sec/clipboard/data/ClipboardData;

    move-result-object v3

    .line 1508
    .local v3, "clipData":Landroid/sec/clipboard/data/ClipboardData;
    invoke-virtual {v3, v6}, Landroid/sec/clipboard/data/ClipboardData;->SetProtectState(Z)V

    .line 1509
    move v8, p1

    .line 1510
    .local v8, "selectedIndex":I
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataList:Landroid/sec/clipboard/data/IClipboardDataList;

    invoke-interface {v9, v8, v3}, Landroid/sec/clipboard/data/IClipboardDataList;->updateData(ILandroid/sec/clipboard/data/ClipboardData;)Z

    .line 1512
    invoke-direct {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->updateClearLayout()V

    .line 1516
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v9}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getFirstPosition()I

    move-result v9

    sub-int v1, p1, v9

    .line 1517
    .local v1, "childPos":I
    iget-object v9, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v9, v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    const v10, 0x7f0a0018

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 1518
    .local v7, "lockImage":Landroid/widget/ImageView;
    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1520
    if-eqz v6, :cond_4

    .line 1521
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-direct {v0, v9, v10}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1526
    .local v0, "alpha":Landroid/view/animation/AlphaAnimation;
    :goto_3
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 1527
    const-wide/16 v10, 0xc8

    invoke-virtual {v0, v10, v11}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 1528
    new-instance v9, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$9;

    invoke-direct {v9, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$9;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    invoke-virtual {v0, v9}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1536
    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1537
    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 1540
    .end local v0    # "alpha":Landroid/view/animation/AlphaAnimation;
    .end local v1    # "childPos":I
    .end local v2    # "cilpUIdata":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    .end local v3    # "clipData":Landroid/sec/clipboard/data/ClipboardData;
    .end local v6    # "isProtected":Z
    .end local v7    # "lockImage":Landroid/widget/ImageView;
    .end local v8    # "selectedIndex":I
    :catch_1
    move-exception v5

    .line 1541
    .local v5, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v5}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_0

    .line 1523
    .end local v5    # "e":Ljava/lang/NullPointerException;
    .restart local v1    # "childPos":I
    .restart local v2    # "cilpUIdata":Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;
    .restart local v3    # "clipData":Landroid/sec/clipboard/data/ClipboardData;
    .restart local v6    # "isProtected":Z
    .restart local v7    # "lockImage":Landroid/widget/ImageView;
    .restart local v8    # "selectedIndex":I
    :cond_4
    :try_start_2
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    invoke-direct {v0, v9, v10}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    .restart local v0    # "alpha":Landroid/view/animation/AlphaAnimation;
    goto :goto_3
.end method

.method createClearConfirmDialog(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 332
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsDarkTheme:Z

    if-eqz v0, :cond_0

    .line 333
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    const/4 v1, 0x4

    invoke-direct {v0, p0, p1, v1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    .line 337
    :goto_0
    return-void

    .line 335
    :cond_0
    new-instance v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    const/4 v1, 0x5

    invoke-direct {v0, p0, p1, v1}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    goto :goto_0
.end method

.method public deleteAnimation()V
    .locals 20

    .prologue
    .line 1288
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getFirstPosition()I

    move-result v15

    .line 1290
    .local v15, "first":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I

    sub-int/2addr v6, v15

    invoke-virtual {v5, v6}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v17

    .line 1292
    .local v17, "view":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setDeleteScenario(Z)V

    .line 1294
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 1295
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildCount()I

    move-result v5

    move/from16 v0, v16

    if-ge v0, v5, :cond_0

    .line 1296
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getX()F

    move-result v5

    float-to-int v0, v5

    move/from16 v18, v0

    .line 1297
    .local v18, "viewX":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getY()F

    move-result v5

    float-to-int v0, v5

    move/from16 v19, v0

    .line 1299
    .local v19, "viewY":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;

    new-instance v6, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ViewPoint;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;II)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1295
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 1302
    .end local v18    # "viewX":I
    .end local v19    # "viewY":I
    :cond_0
    new-instance v14, Landroid/view/animation/AnimationSet;

    const/4 v5, 0x1

    invoke-direct {v14, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1303
    .local v14, "animSet":Landroid/view/animation/AnimationSet;
    new-instance v4, Landroid/view/animation/ScaleAnimation;

    const/high16 v5, 0x3f800000    # 1.0f

    const v6, 0x3f4ccccd    # 0.8f

    const/high16 v7, 0x3f800000    # 1.0f

    const v8, 0x3f4ccccd    # 0.8f

    const/4 v9, 0x1

    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v11, 0x1

    const/high16 v12, 0x3f000000    # 0.5f

    invoke-direct/range {v4 .. v12}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 1304
    .local v4, "scale":Landroid/view/animation/ScaleAnimation;
    new-instance v13, Landroid/view/animation/AlphaAnimation;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-direct {v13, v5, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1307
    .local v13, "alpha":Landroid/view/animation/AlphaAnimation;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mDeleteAnimation:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v14, v5}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1308
    const-wide/16 v6, 0x1f4

    invoke-virtual {v14, v6, v7}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 1310
    if-eqz v17, :cond_1

    .line 1312
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1314
    :cond_1
    return-void
.end method

.method public dismiss()V
    .locals 5

    .prologue
    .line 1685
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    if-nez v2, :cond_0

    .line 1686
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "clipboardEx"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/sec/clipboard/ClipboardExManager;

    iput-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    .line 1688
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    if-eqz v2, :cond_1

    .line 1689
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v2}, Landroid/sec/clipboard/ClipboardExManager;->unRegistClipboardWorkingFormUiInterface()V

    .line 1692
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1693
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearDialog:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClearConfirmDialog;->dismiss()V

    .line 1696
    :cond_2
    iget-boolean v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsDeletingItem:Z

    if-eqz v2, :cond_3

    .line 1697
    const-string v2, "ClipboardServiceEx"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "delete item...in dismiss()...CHILD_COUNT :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CHILD_COUNT:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mSelectedIndex :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1699
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataUiEvent:Landroid/sec/clipboard/IClipboardDataUiEvent;

    iget v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I

    invoke-interface {v2, v3}, Landroid/sec/clipboard/IClipboardDataUiEvent;->removeItem(I)V

    .line 1700
    sget v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CHILD_COUNT:I

    add-int/lit8 v2, v2, -0x1

    sput v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CHILD_COUNT:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1705
    :goto_0
    const-string v2, "ClipboardServiceEx"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "after delete item...in dismiss()...CHILD_COUNT :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->CHILD_COUNT:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1708
    :cond_3
    sget-boolean v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v2, :cond_4

    .line 1709
    const-string v2, "ClipboardServiceEx"

    const-string v3, "Clipboard dialog is closed."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1712
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAdapter:Landroid/widget/BaseAdapter;

    check-cast v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mRecycleList:Ljava/util/List;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->access$2500(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 1713
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAdapter:Landroid/widget/BaseAdapter;

    check-cast v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->recycle()V

    .line 1715
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAdapter:Landroid/widget/BaseAdapter;

    check-cast v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;

    # getter for: Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->mRecycleBitmapList:Ljava/util/List;
    invoke-static {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->access$2600(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_6

    .line 1716
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mAdapter:Landroid/widget/BaseAdapter;

    check-cast v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipAdapter;->recycleBitmap()V

    .line 1719
    :cond_6
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 1721
    iget-boolean v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mEnabledDismissIntent:Z

    if-eqz v2, :cond_7

    .line 1722
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$12;

    invoke-direct {v2, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$12;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1733
    .local v1, "t":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1735
    .end local v1    # "t":Ljava/lang/Thread;
    :cond_7
    return-void

    .line 1701
    :catch_0
    move-exception v0

    .line 1703
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1743
    invoke-super {p0, p1}, Landroid/app/Dialog;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public enabledDismissIntent(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 1738
    iput-boolean p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mEnabledDismissIntent:Z

    .line 1739
    return-void
.end method

.method isKnoxTwoEnabled()Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 302
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v4

    .line 303
    .local v4, "versionInfo":Landroid/os/Bundle;
    const-string v6, "2.0"

    const-string v7, "version"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 304
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    const-string v7, "persona"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PersonaManager;

    .line 306
    .local v1, "mPersonaManager":Landroid/os/PersonaManager;
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 307
    .local v2, "ident":J
    iget-object v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v6}, Landroid/sec/clipboard/ClipboardExManager;->getPersonaId()I

    move-result v0

    .line 308
    .local v0, "currUser":I
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 309
    if-eqz v1, :cond_1

    invoke-virtual {v1, v0}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v6

    if-nez v6, :cond_1

    .line 310
    sget-boolean v6, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v6, :cond_0

    .line 311
    const-string v6, "ClipboardNormalDialog"

    const-string v7, "Current user is a USER, hence return false"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    .end local v0    # "currUser":I
    .end local v1    # "mPersonaManager":Landroid/os/PersonaManager;
    .end local v2    # "ident":J
    :cond_0
    :goto_0
    return v5

    .line 315
    .restart local v0    # "currUser":I
    .restart local v1    # "mPersonaManager":Landroid/os/PersonaManager;
    .restart local v2    # "ident":J
    :cond_1
    sget-boolean v5, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v5, :cond_2

    .line 316
    const-string v5, "ClipboardNormalDialog"

    const-string v6, "Current user is a persona, hence return true"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    :cond_2
    const/4 v5, 0x1

    goto :goto_0

    .line 321
    .end local v0    # "currUser":I
    .end local v1    # "mPersonaManager":Landroid/os/PersonaManager;
    .end local v2    # "ident":J
    :cond_3
    sget-boolean v6, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v6, :cond_0

    .line 322
    const-string v6, "ClipboardNormalDialog"

    const-string v7, "\'ro.build.knox.container\' system property is not set to \'2.0\', hence return false"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onClipBoardDialogSizeChanged()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 1777
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1778
    .local v0, "cfg":Landroid/content/res/Configuration;
    iget v2, v0, Landroid/content/res/Configuration;->orientation:I

    iget v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCurrentOrientation:I

    if-eq v2, v3, :cond_0

    .line 1779
    const v2, 0x7f0a0001

    invoke-virtual {p0, v2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1780
    .local v1, "clipBoardBottomPanel":Landroid/widget/LinearLayout;
    iget v2, v0, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCurrentOrientation:I

    .line 1782
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipDrawer:Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipBoardHeight:I

    invoke-direct {v3, v5, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1783
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    iget v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipBoardPanelHeight:I

    invoke-direct {v2, v5, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1785
    .end local v1    # "clipBoardBottomPanel":Landroid/widget/LinearLayout;
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 2525
    sget-boolean v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 2526
    const-string v0, "ClipboardServiceEx"

    const-string v1, "clipboard dialog get ACTION_CONFIGURATION_CHANGED"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2530
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCalledDismissIntentFromSIPFlag:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mShouldBeDismissed:Z

    if-nez v0, :cond_1

    .line 2534
    iget-object v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardUIService:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/clipboarduiservice/ClipboardUIService;->createDialog(Z)V

    .line 2542
    :goto_0
    return-void

    .line 2536
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCalledDismissIntentFromSIPFlag:Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 17
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 565
    invoke-super/range {p0 .. p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 568
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 569
    .local v3, "filter":Landroid/content/IntentFilter;
    const-string v14, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v3, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 570
    const-string v14, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v3, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 571
    const-string v14, "DismissClipboardDialogFromIMMService"

    invoke-virtual {v3, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 572
    const-string v14, "com.android.internal.policy.impl.sec.UserActivityByShortcut"

    invoke-virtual {v3, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 573
    const-string v14, "DismissClipboardDialogFromPhoneStatusBar"

    invoke-virtual {v3, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 574
    const-string v14, "android.intent.action.USER_PRESENT"

    invoke-virtual {v3, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 576
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsSupportLockFunc:Z

    .line 580
    sget-boolean v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v14, :cond_0

    .line 581
    const-string v14, "ClipboardNormalDialog"

    const-string v15, "register PhoneStateListener"

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v14

    const-string v15, "phone"

    invoke-virtual {v14, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/telephony/TelephonyManager;

    .line 584
    .local v13, "tmgr":Landroid/telephony/TelephonyManager;
    if-eqz v13, :cond_1

    .line 585
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v15, 0x20

    invoke-virtual {v13, v14, v15}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 590
    :cond_1
    const-string v14, "ro.build.scafe.cream"

    invoke-static {v14}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v15, "black"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_10

    .line 591
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsDarkTheme:Z

    .line 596
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mBroadCastListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v14, v15, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 597
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsAddedFilter:Z

    .line 600
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getWindow()Landroid/view/Window;

    move-result-object v14

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/view/Window;->addFlags(I)V

    .line 602
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getWindow()Landroid/view/Window;

    move-result-object v14

    const/high16 v15, 0x1000000

    invoke-virtual {v14, v15}, Landroid/view/Window;->addFlags(I)V

    .line 604
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const/high16 v15, 0x7f050000

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    float-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipBoardHeight:I

    .line 605
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f050001

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    float-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipBoardPanelHeight:I

    .line 607
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f05000e

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    float-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mGridItemWidth:I

    .line 608
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f05000f

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    float-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mGridItemHeight:I

    .line 610
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f050010

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    float-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImageWidth:I

    .line 611
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f050011

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    float-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImageHeight:I

    .line 612
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f050012

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    float-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mHTMLImagePadding:I

    .line 614
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f050027

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    float-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemPadding:I

    .line 616
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f050024

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    float-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemMinHeight:I

    .line 617
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f050025

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    float-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemMaxHeight:I

    .line 618
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f050026

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    float-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemWidth:I

    .line 620
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 621
    .local v1, "config":Landroid/content/res/Configuration;
    if-eqz v1, :cond_2

    .line 622
    iget v14, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v15, 0x2

    if-ne v14, v15, :cond_11

    .line 623
    const/4 v14, 0x2

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCurrentOrientation:I

    .line 629
    :cond_2
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getWindow()Landroid/view/Window;

    move-result-object v11

    .line 630
    .local v11, "theWindow":Landroid/view/Window;
    const/16 v14, 0x57

    invoke-virtual {v11, v14}, Landroid/view/Window;->setGravity(I)V

    .line 631
    new-instance v14, Landroid/graphics/drawable/ColorDrawable;

    const/4 v15, 0x0

    invoke-direct {v14, v15}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v11, v14}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 632
    sget-boolean v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v14, :cond_3

    .line 633
    const-string v14, "ClipboardNormalDialog"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Width :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mGridItemWidth:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "   Height :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mGridItemHeight:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    :cond_3
    const v14, 0x7f030002

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->setContentView(I)V

    .line 639
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mViewPosition:Ljava/util/ArrayList;

    .line 642
    const v14, 0x7f0a0005

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    .line 643
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCurrentOrientation:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_12

    .line 644
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    const/4 v15, 0x3

    invoke-virtual {v14, v15}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setColumnCount(I)V

    .line 648
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mItemPadding:I

    invoke-virtual {v14, v15}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setItemMargin(I)V

    .line 649
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v14}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v14

    new-instance v15, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$2;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    invoke-virtual {v14, v15}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 698
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v14}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v14

    new-instance v15, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$3;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$3;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    invoke-virtual {v14, v15}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 706
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    new-instance v15, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$4;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$4;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    invoke-virtual {v14, v15}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 718
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    if-nez v14, :cond_5

    .line 719
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v14

    const-string v15, "clipboardEx"

    invoke-virtual {v14, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/sec/clipboard/ClipboardExManager;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    .line 722
    :cond_5
    new-instance v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v15}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$1;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardWorkingFormUiInterface:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;

    .line 723
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    if-eqz v14, :cond_6

    .line 724
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardWorkingFormUiInterface:Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$ClipboardWorkingFormUiInterfaceImp;

    invoke-virtual {v14, v15}, Landroid/sec/clipboard/ClipboardExManager;->RegistClipboardWorkingFormUiInterface(Landroid/sec/clipboard/IClipboardWorkingFormUiInterface;)Z

    .line 727
    :cond_6
    const/4 v14, -0x1

    const/4 v15, -0x2

    invoke-virtual {v11, v14, v15}, Landroid/view/Window;->setLayout(II)V

    .line 729
    const/high16 v14, 0x7f0a0000

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipDrawer:Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;

    .line 731
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipDrawer:Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;

    if-eqz v14, :cond_7

    .line 732
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipDrawer:Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardUIService:Lcom/samsung/android/clipboarduiservice/ClipboardUIService;

    invoke-virtual {v14, v15}, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->setClipboardUIService(Lcom/samsung/android/clipboarduiservice/ClipboardUIService;)V

    .line 733
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipDrawer:Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;

    new-instance v15, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$5;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$5;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    invoke-virtual {v14, v15}, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->setOnDrawerCloseListener(Landroid/widget/SlidingDrawer$OnDrawerCloseListener;)V

    .line 743
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->rebuildView()V

    .line 745
    const-string v9, ""

    .line 746
    .local v9, "prefix":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    if-eqz v14, :cond_9

    .line 747
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v14}, Landroid/sec/clipboard/ClipboardExManager;->getContainerID()I

    move-result v14

    if-eqz v14, :cond_8

    .line 748
    const-string v9, "sec_container_1."

    .line 750
    :cond_8
    const-string v14, "ClipboardNormalDialog"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "mCbm.getContainerID() :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/sec/clipboard/ClipboardExManager;->getContainerID()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 753
    :cond_9
    sget-boolean v14, Landroid/sec/clipboard/data/ClipboardDefine;->SUPPORT_SAVE_MODE:Z

    if-eqz v14, :cond_13

    .line 754
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 755
    .local v8, "packageManager":Landroid/content/pm/PackageManager;
    new-instance v14, Landroid/content/Intent;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "android.intent.action.CLIPBOARD_TO_MEMO_INSERT"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v15, 0x0

    invoke-virtual {v8, v14, v15}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v10

    .line 756
    .local v10, "receiver":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v10, :cond_13

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v14

    if-lez v14, :cond_13

    .line 758
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_3
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v14

    if-ge v5, v14, :cond_13

    .line 759
    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/content/pm/ResolveInfo;

    iget-object v14, v14, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v14, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 760
    .local v6, "installedPackage":Ljava/lang/String;
    if-eqz v6, :cond_b

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "com.sec.android.widgetapp.diotek.smemo"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_a

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "com.sec.android.provider.smemo"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 761
    :cond_a
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsInstalledSMemo:Z

    .line 763
    :cond_b
    if-eqz v6, :cond_d

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "com.sec.android.app.snotebook"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_c

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "com.samsung.android.snote"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_d

    .line 764
    :cond_c
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsInstalledSNote:Z

    .line 766
    :cond_d
    if-eqz v6, :cond_f

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "com.sec.android.app.memo"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_e

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "com.samsung.android.app.memo"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_f

    .line 767
    :cond_e
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsInstalledMemo:Z

    .line 758
    :cond_f
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_3

    .line 593
    .end local v1    # "config":Landroid/content/res/Configuration;
    .end local v5    # "i":I
    .end local v6    # "installedPackage":Ljava/lang/String;
    .end local v8    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v9    # "prefix":Ljava/lang/String;
    .end local v10    # "receiver":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v11    # "theWindow":Landroid/view/Window;
    :cond_10
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsDarkTheme:Z

    goto/16 :goto_0

    .line 624
    .restart local v1    # "config":Landroid/content/res/Configuration;
    :cond_11
    iget v14, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_2

    .line 625
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCurrentOrientation:I

    goto/16 :goto_1

    .line 645
    .restart local v11    # "theWindow":Landroid/view/Window;
    :cond_12
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCurrentOrientation:I

    const/4 v15, 0x2

    if-ne v14, v15, :cond_4

    .line 646
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    const/4 v15, 0x5

    invoke-virtual {v14, v15}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->setColumnCount(I)V

    goto/16 :goto_2

    .line 775
    .restart local v9    # "prefix":Ljava/lang/String;
    :cond_13
    const v14, 0x7f0a0003

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 776
    .local v12, "titleText":Landroid/widget/TextView;
    if-eqz v12, :cond_14

    .line 777
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f080009

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f080012

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 779
    const/4 v14, 0x0

    invoke-virtual {v12, v14}, Landroid/widget/TextView;->setHoverPopupType(I)V

    .line 780
    const/4 v14, 0x0

    invoke-virtual {v12, v14}, Landroid/widget/TextView;->enableMultiSelection(Z)V

    .line 783
    :cond_14
    const v14, 0x7f0a0004

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearButton:Landroid/widget/Button;

    .line 784
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearButton:Landroid/widget/Button;

    if-eqz v14, :cond_15

    .line 785
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearButton:Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mButtonClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v14, v15}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 786
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearButton:Landroid/widget/Button;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/Button;->enableMultiSelection(Z)V

    .line 790
    :cond_15
    const/4 v7, 0x1

    .line 791
    .local v7, "isAllDisabled":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    if-eqz v14, :cond_18

    .line 792
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v14}, Landroid/sec/clipboard/ClipboardExManager;->getDataListSize()I

    move-result v14

    if-ge v5, v14, :cond_18

    .line 793
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->isCheckProtectedItem(I)Z

    move-result v14

    if-nez v14, :cond_17

    .line 794
    sget-boolean v14, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v14, :cond_16

    .line 795
    const-string v14, "ClipboardNormalDialog"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "some item is not unlocked...index :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    :cond_16
    const/4 v7, 0x0

    .line 792
    :cond_17
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 802
    .end local v5    # "i":I
    :cond_18
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    if-eqz v14, :cond_1a

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearButton:Landroid/widget/Button;

    if-eqz v14, :cond_1a

    .line 803
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v14}, Landroid/sec/clipboard/ClipboardExManager;->getDataListSize()I

    move-result v14

    if-eqz v14, :cond_19

    if-eqz v7, :cond_1a

    .line 804
    :cond_19
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClearButton:Landroid/widget/Button;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/widget/Button;->setVisibility(I)V

    .line 805
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipDrawer:Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;

    if-eqz v14, :cond_1a

    .line 806
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipDrawer:Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;

    invoke-virtual {v14}, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->getHandle()Landroid/view/View;

    move-result-object v4

    .line 807
    .local v4, "handle":Landroid/view/View;
    if-eqz v4, :cond_1a

    instance-of v14, v4, Landroid/view/ViewGroup;

    if-eqz v14, :cond_1a

    move-object v14, v4

    check-cast v14, Landroid/view/ViewGroup;

    invoke-virtual {v14}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v14

    const/4 v15, 0x3

    if-lt v14, v15, :cond_1a

    .line 808
    check-cast v4, Landroid/view/ViewGroup;

    .end local v4    # "handle":Landroid/view/View;
    const/4 v14, 0x1

    invoke-virtual {v4, v14}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 809
    .local v2, "divider":Landroid/view/View;
    if-eqz v2, :cond_1a

    instance-of v14, v2, Landroid/widget/ImageView;

    if-eqz v14, :cond_1a

    .line 810
    const/16 v14, 0x8

    invoke-virtual {v2, v14}, Landroid/view/View;->setVisibility(I)V

    .line 816
    .end local v2    # "divider":Landroid/view/View;
    :cond_1a
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 8
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    const/4 v6, 0x1

    .line 1058
    iget-boolean v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSyncIsOnGoing:Z

    if-eqz v5, :cond_1

    .line 1159
    :cond_0
    :goto_0
    return-void

    .line 1062
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mShowingMenu:Landroid/view/Menu;

    .line 1063
    iput-boolean v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsShowingContextmenu:Z

    .line 1066
    invoke-virtual {p2, v6}, Landroid/view/View;->twSetContextMenuZOrderToTop(Z)V

    .line 1068
    invoke-super {p0, p1, p2, p3}, Landroid/app/Dialog;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 1070
    invoke-virtual {p2}, Landroid/view/View;->getX()F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedX:I

    .line 1071
    invoke-virtual {p2}, Landroid/view/View;->getY()F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedY:I

    .line 1073
    iget-object v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    iget v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedX:I

    iget v7, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedY:I

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->pointToPosition(II)I

    move-result v5

    iput v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I

    .line 1075
    iget-object v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v5}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getFirstPosition()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mFirstPosition:I

    .line 1077
    new-instance v2, Landroid/view/MenuInflater;

    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v2, v5}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    .line 1078
    .local v2, "inflater":Landroid/view/MenuInflater;
    const v5, 0x7f080009

    invoke-interface {p1, v5}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    .line 1080
    iget v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I

    invoke-direct {p0, v5}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->isCheckProtectedItem(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1082
    const v5, 0x7f090002

    invoke-virtual {v2, v5, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 1085
    :cond_2
    const/high16 v5, 0x7f090000

    invoke-virtual {v2, v5, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1091
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->isKnoxTwoEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1092
    iget-object v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v5}, Landroid/sec/clipboard/ClipboardExManager;->getPersonaId()I

    move-result v1

    .line 1094
    .local v1, "id":I
    if-nez v1, :cond_3

    .line 1095
    new-instance v3, Ljava/io/File;

    const-string v5, "/data/clipboard"

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1099
    .local v3, "rootPath":Ljava/io/File;
    :goto_1
    const-string v5, "ClipboardServiceEx"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onCreateContextMenu: rootPath = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ; current user id = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1101
    new-instance v4, Landroid/sec/clipboard/data/file/FileManager;

    new-instance v5, Ljava/io/File;

    const-string v6, "clips.info"

    invoke-direct {v5, v3, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v4, v5, v1}, Landroid/sec/clipboard/data/file/FileManager;-><init>(Ljava/io/File;I)V

    .line 1102
    .local v4, "rootfm":Landroid/sec/clipboard/data/file/FileManager;
    if-eqz v4, :cond_0

    .line 1103
    const-string v5, "ClipboardServiceEx"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onCreateContextMenu: mSelectedIndex = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ; rootfm.size() = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Landroid/sec/clipboard/data/file/FileManager;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1105
    iget v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I

    invoke-virtual {v4}, Landroid/sec/clipboard/data/file/FileManager;->size()I

    move-result v6

    if-lt v5, v6, :cond_0

    .line 1107
    const/4 v5, 0x1

    :try_start_0
    invoke-interface {p1, v5}, Landroid/view/ContextMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1108
    :catch_0
    move-exception v0

    .line 1109
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v5, "ClipboardServiceEx"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onCreateContextMenu: IndexOutOfBoundsException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1097
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    .end local v3    # "rootPath":Ljava/io/File;
    .end local v4    # "rootfm":Landroid/sec/clipboard/data/file/FileManager;
    :cond_3
    new-instance v3, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "/data/clipboard"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v3    # "rootPath":Ljava/io/File;
    goto/16 :goto_1
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 7
    .param p1, "featureId"    # I
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x1

    .line 1317
    sget-boolean v4, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v4, :cond_0

    .line 1318
    const-string v4, "ClipboardServiceEx"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mSelectedIndex :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1321
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v4}, Landroid/sec/clipboard/ClipboardExManager;->getContainerID()I

    move-result v0

    .line 1324
    .local v0, "currentMode":I
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipboardDataList:Landroid/sec/clipboard/data/IClipboardDataList;

    iget v5, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I

    invoke-interface {v4, v5}, Landroid/sec/clipboard/data/IClipboardDataList;->getItem(I)Landroid/sec/clipboard/data/ClipboardData;

    move-result-object v1

    .line 1326
    .local v1, "data":Landroid/sec/clipboard/data/ClipboardData;
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 1476
    invoke-super {p0, p2}, Landroid/app/Dialog;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    .line 1481
    .end local v1    # "data":Landroid/sec/clipboard/data/ClipboardData;
    :goto_0
    return v3

    .line 1328
    .restart local v1    # "data":Landroid/sec/clipboard/data/ClipboardData;
    :pswitch_0
    sget-boolean v4, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 1329
    const-string v4, "ClipboardServiceEx"

    const-string v5, "select delete menu"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1333
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->deleteAnimation()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1478
    .end local v1    # "data":Landroid/sec/clipboard/data/ClipboardData;
    :catch_0
    move-exception v2

    .line 1479
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1481
    invoke-super {p0, p2}, Landroid/app/Dialog;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    goto :goto_0

    .line 1337
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v1    # "data":Landroid/sec/clipboard/data/ClipboardData;
    :pswitch_1
    :try_start_1
    sget-boolean v4, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v4, :cond_2

    .line 1338
    const-string v4, "ClipboardServiceEx"

    const-string v5, "select lock menu"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1343
    :cond_2
    iget v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I

    invoke-virtual {p0, v4}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->IsSetProtectAction(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1326
    :pswitch_data_0
    .packed-switch 0x7f0a001a
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .locals 1
    .param p1, "featureId"    # I
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsShowingContextmenu:Z

    .line 1164
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onPanelClosed(ILandroid/view/Menu;)V

    .line 1165
    return-void
.end method

.method protected onStart()V
    .locals 4

    .prologue
    .line 822
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipDrawer:Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;

    if-eqz v2, :cond_0

    .line 823
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mClipDrawer:Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/view/TwSlidingDrawer;->open()V

    .line 827
    :cond_0
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 829
    iget-boolean v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsAddedFilter:Z

    if-eqz v2, :cond_1

    .line 830
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsAddedFilter:Z

    .line 857
    :goto_0
    return-void

    .line 833
    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 834
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 835
    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 836
    const-string v2, "DismissClipboardDialogFromIMMService"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 837
    const-string v2, "com.android.internal.policy.impl.sec.UserActivityByShortcut"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 838
    const-string v2, "DismissClipboardDialogFromPhoneStatusBar"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 839
    const-string v2, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 843
    sget-boolean v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v2, :cond_2

    .line 844
    const-string v2, "ClipboardNormalDialog"

    const-string v3, "register PhoneStateListener"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 846
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 847
    .local v1, "tmgr":Landroid/telephony/TelephonyManager;
    if-eqz v1, :cond_3

    .line 848
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v3, 0x20

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 852
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mBroadCastListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 853
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsAddedFilter:Z

    goto :goto_0
.end method

.method protected onStop()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 863
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 864
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 865
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 866
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mUIDataList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;

    invoke-virtual {v2}, Lcom/samsung/android/clipboarduiservice/data/ClipboardUIData;->recycle()V

    .line 864
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 871
    .end local v0    # "index":I
    :cond_1
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 873
    iget-boolean v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsAddedFilter:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 874
    iput-boolean v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsAddedFilter:Z

    .line 875
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mBroadCastListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 878
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mBroadCastListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 880
    sget-boolean v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v2, :cond_3

    .line 881
    const-string v2, "ClipboardServiceEx"

    const-string v3, "unregister PhoneStateListener"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 883
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 884
    .local v1, "tmgr":Landroid/telephony/TelephonyManager;
    if-eqz v1, :cond_4

    .line 885
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v1, v2, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 886
    const/4 v1, 0x0

    .line 889
    :cond_4
    return-void
.end method

.method public rebuildView()V
    .locals 3

    .prologue
    .line 1668
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1670
    .local v1, "theWindow":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1672
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    const/16 v2, 0x7dc

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 1674
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 1676
    const-string v2, "Clipboard"

    invoke-virtual {v0, v2}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 1680
    return-void
.end method

.method public setPasteTargetViewInfo(ILandroid/sec/clipboard/IClipboardDataPasteEvent;)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "clPasteEvent"    # Landroid/sec/clipboard/IClipboardDataPasteEvent;

    .prologue
    .line 1663
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->sendMessage(I)V

    .line 1664
    return-void
.end method

.method public show()V
    .locals 5

    .prologue
    .line 1747
    invoke-super {p0}, Landroid/app/Dialog;->show()V

    .line 1749
    invoke-direct {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->syncClipboardData()V

    .line 1751
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$13;

    invoke-direct {v2, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$13;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1762
    .local v1, "t":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1765
    invoke-virtual {p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1766
    .local v0, "config":Landroid/content/res/Configuration;
    if-eqz v0, :cond_1

    .line 1767
    iget v2, v0, Landroid/content/res/Configuration;->orientation:I

    iget v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCurrentOrientation:I

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    if-eqz v2, :cond_1

    .line 1768
    sget-boolean v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 1769
    const-string v2, "ClipboardServiceEx"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Clipboard will be re-created. config.orientation :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mCurrentOrientation :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCurrentOrientation:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1771
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mCbm:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v2}, Landroid/sec/clipboard/ClipboardExManager;->showUIDataDialog()V

    .line 1774
    :cond_1
    return-void
.end method

.method public showDeletePopup(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1918
    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedX:I

    .line 1919
    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedY:I

    .line 1921
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    iget v2, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedX:I

    iget v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedY:I

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->pointToPosition(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mSelectedIndex:I

    .line 1923
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mStagGrid:Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;

    invoke-virtual {v1}, Lcom/samsung/android/clipboarduiservice/view/ClippedStaggeredGridView;->getFirstPosition()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mFirstPosition:I

    .line 1926
    const/4 v0, 0x0

    .line 1927
    .local v0, "context":Landroid/content/Context;
    iget-boolean v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mIsDarkTheme:Z

    if-eqz v1, :cond_0

    .line 1928
    new-instance v0, Landroid/view/ContextThemeWrapper;

    .end local v0    # "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/android/clipboarduiservice/util/GetFWResource;->getDefaultTheme()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 1933
    .restart local v0    # "context":Landroid/content/Context;
    :goto_0
    new-instance v1, Landroid/widget/PopupMenu;

    invoke-direct {v1, v0, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mPopupMenu:Landroid/widget/PopupMenu;

    .line 1934
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f090001

    iget-object v3, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1935
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mPopupMenu:Landroid/widget/PopupMenu;

    new-instance v2, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$14;

    invoke-direct {v2, p0}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog$14;-><init>(Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 1945
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    .line 1946
    return-void

    .line 1930
    :cond_0
    new-instance v0, Landroid/view/ContextThemeWrapper;

    .end local v0    # "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/android/clipboarduiservice/util/GetFWResource;->getDefaultThemeLight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .restart local v0    # "context":Landroid/content/Context;
    goto :goto_0
.end method

.method public showProtectedMarker(Landroid/view/View;I)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "index"    # I

    .prologue
    .line 1580
    if-nez p1, :cond_1

    .line 1609
    :cond_0
    :goto_0
    return-void

    .line 1583
    :cond_1
    const v3, 0x7f0a0018

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1585
    .local v1, "lockimage":Landroid/widget/ImageView;
    invoke-direct {p0, p2}, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->isCheckProtectedItem(I)Z

    move-result v0

    .line 1586
    .local v0, "isProtected":Z
    if-eqz v0, :cond_4

    .line 1589
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1590
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1591
    .local v2, "params":Landroid/view/ViewGroup$LayoutParams;
    sget-boolean v3, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v3, :cond_2

    .line 1592
    const-string v3, "ClipboardServiceEx"

    const-string v4, "showProtectedMarker@v.getTag() != null no!!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1601
    :cond_2
    :goto_1
    if-eqz v1, :cond_0

    .line 1602
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1595
    .end local v2    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1596
    .restart local v2    # "params":Landroid/view/ViewGroup$LayoutParams;
    sget-boolean v3, Lcom/samsung/android/clipboarduiservice/view/ClipboardNormalDialog;->DEBUG:Z

    if-eqz v3, :cond_2

    .line 1597
    const-string v3, "ClipboardServiceEx"

    const-string v4, "showProtectedMarker@v.getTag() != null !!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1605
    .end local v2    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_4
    if-eqz v1, :cond_0

    .line 1606
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
