.class Lcom/sec/allsharecastplayer/MainActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/allsharecastplayer/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/allsharecastplayer/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/allsharecastplayer/MainActivity;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 92
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, "action":Ljava/lang/String;
    const-string v4, "AllShareCastPlayer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "BroadCast Received Action : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    const-string v4, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 96
    const-string v4, "wifi_p2p_state"

    invoke-virtual {p2, v4, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v7, :cond_0

    .line 97
    const-string v4, "AllShareCastPlayer"

    const-string v5, "WIFI_P2P_STATE_CHANGED_ACTION / WIFI_P2P_STATE_ENABLED"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v4, p0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # invokes: Lcom/sec/allsharecastplayer/MainActivity;->turnOnListenMode()V
    invoke-static {v4}, Lcom/sec/allsharecastplayer/MainActivity;->access$000(Lcom/sec/allsharecastplayer/MainActivity;)V

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    const-string v4, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 103
    const-string v4, "wifiP2pInfo"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/p2p/WifiP2pInfo;

    .line 104
    .local v3, "wifiP2pInfo":Landroid/net/wifi/p2p/WifiP2pInfo;
    const-string v4, "networkInfo"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    .line 105
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    iget-object v4, p0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->DEBUG:Z
    invoke-static {v4}, Lcom/sec/allsharecastplayer/MainActivity;->access$100(Lcom/sec/allsharecastplayer/MainActivity;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 106
    const-string v4, "AllShareCastPlayer"

    const-string v5, "WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-nez v4, :cond_3

    .line 110
    const/4 v4, 0x0

    # setter for: Lcom/sec/allsharecastplayer/MainActivity;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-static {v4}, Lcom/sec/allsharecastplayer/MainActivity;->access$202(Landroid/net/wifi/p2p/WifiP2pGroup;)Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 111
    iget-object v4, p0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # invokes: Lcom/sec/allsharecastplayer/MainActivity;->turnOnListenMode()V
    invoke-static {v4}, Lcom/sec/allsharecastplayer/MainActivity;->access$000(Lcom/sec/allsharecastplayer/MainActivity;)V

    goto :goto_0

    .line 113
    :cond_3
    const-string v4, "AllShareCastPlayer"

    const-string v5, "BroadCast Received Action : P2p connected"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {}, Lcom/sec/allsharecastplayer/MainActivity;->access$1300()Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    invoke-static {v5}, Lcom/sec/allsharecastplayer/MainActivity;->access$300(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v5

    new-instance v6, Lcom/sec/allsharecastplayer/MainActivity$1$1;

    invoke-direct {v6, p0, v3}, Lcom/sec/allsharecastplayer/MainActivity$1$1;-><init>(Lcom/sec/allsharecastplayer/MainActivity$1;Landroid/net/wifi/p2p/WifiP2pInfo;)V

    invoke-virtual {v4, v5, v6}, Landroid/net/wifi/p2p/WifiP2pManager;->requestGroupInfo(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;)V

    .line 174
    iget-object v4, p0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mShowPinDialog:Landroid/app/AlertDialog;
    invoke-static {v4}, Lcom/sec/allsharecastplayer/MainActivity;->access$1400(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/app/AlertDialog;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 175
    iget-object v4, p0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mShowPinDialog:Landroid/app/AlertDialog;
    invoke-static {v4}, Lcom/sec/allsharecastplayer/MainActivity;->access$1400(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/app/AlertDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog;->dismiss()V

    .line 176
    iget-object v4, p0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mWpsTimer:Landroid/os/CountDownTimer;
    invoke-static {v4}, Lcom/sec/allsharecastplayer/MainActivity;->access$1500(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/os/CountDownTimer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/CountDownTimer;->cancel()V

    goto :goto_0

    .line 179
    .end local v1    # "networkInfo":Landroid/net/NetworkInfo;
    .end local v3    # "wifiP2pInfo":Landroid/net/wifi/p2p/WifiP2pInfo;
    :cond_4
    const-string v4, "android.intent.action.WIFI_DISPLAY"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 180
    const-string v4, "state"

    invoke-virtual {p2, v4, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v10, :cond_0

    .line 182
    iget-object v4, p0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    invoke-virtual {v4}, Lcom/sec/allsharecastplayer/MainActivity;->finish()V

    goto/16 :goto_0

    .line 184
    :cond_5
    const-string v4, "com.sec.android.spc.SPC_CONTROLLER_DEVICE"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 185
    const-string v4, "device_count"

    invoke-virtual {p2, v4, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 186
    .local v2, "spcControllerCount":I
    if-lez v2, :cond_0

    .line 187
    const-string v4, "AllShareCastPlayer"

    const-string v5, "Remote Control of SPC connected, finish the application"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    iget-object v4, p0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/allsharecastplayer/MainActivity;->access$1600(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    const v6, 0x7f050008

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    const v9, 0x7f05000d

    invoke-virtual {v8, v9}, Lcom/sec/allsharecastplayer/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v11

    iget-object v8, p0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    const v9, 0x7f05000e

    invoke-virtual {v8, v9}, Lcom/sec/allsharecastplayer/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v5, v6, v7}, Lcom/sec/allsharecastplayer/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 189
    iget-object v4, p0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    invoke-virtual {v4}, Lcom/sec/allsharecastplayer/MainActivity;->finish()V

    goto/16 :goto_0
.end method
