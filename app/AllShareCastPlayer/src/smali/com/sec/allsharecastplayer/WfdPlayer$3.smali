.class Lcom/sec/allsharecastplayer/WfdPlayer$3;
.super Ljava/lang/Object;
.source "WfdPlayer.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/allsharecastplayer/WfdPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/allsharecastplayer/WfdPlayer;


# direct methods
.method constructor <init>(Lcom/sec/allsharecastplayer/WfdPlayer;)V
    .locals 0

    .prologue
    .line 332
    iput-object p1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$3;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x1

    .line 334
    const-string v0, "WfdPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WfdPlayer::onKey action = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    const-string v0, "WfdPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "char = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getCharacters()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    const-string v0, "WfdPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "code = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    const-string v0, "WfdPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "number = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getNumber()C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    const-string v0, "WfdPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unicode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$3;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;
    invoke-static {v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$500(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/UIBCInputHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$3;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;
    invoke-static {v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$500(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/UIBCInputHandler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/UIBCInputHandler;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$3;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # setter for: Lcom/sec/allsharecastplayer/WfdPlayer;->key1:I
    invoke-static {v0, p2}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$602(Lcom/sec/allsharecastplayer/WfdPlayer;I)I

    .line 341
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$3;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    const/4 v1, 0x0

    # setter for: Lcom/sec/allsharecastplayer/WfdPlayer;->key2:I
    invoke-static {v0, v1}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$702(Lcom/sec/allsharecastplayer/WfdPlayer;I)I

    .line 342
    const-string v0, "WfdPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "key1 = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 344
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$3;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;
    invoke-static {v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$500(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/UIBCInputHandler;

    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$3;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->key1:I
    invoke-static {v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$600(Lcom/sec/allsharecastplayer/WfdPlayer;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$3;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->key2:I
    invoke-static {v1}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$700(Lcom/sec/allsharecastplayer/WfdPlayer;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/media/UIBCInputHandler;->keyUp(II)V

    .line 355
    :cond_0
    :goto_0
    return v3

    .line 346
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    .line 348
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$3;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;
    invoke-static {v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$500(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/UIBCInputHandler;

    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$3;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->key1:I
    invoke-static {v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$600(Lcom/sec/allsharecastplayer/WfdPlayer;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$3;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->key2:I
    invoke-static {v1}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$700(Lcom/sec/allsharecastplayer/WfdPlayer;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/media/UIBCInputHandler;->keyDown(II)V

    goto :goto_0

    .line 350
    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 352
    const-string v0, "WfdPlayer"

    const-string v1, "Multiple press: key2 not zero"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
