.class Lcom/sec/allsharecastplayer/WfdPlayer$2;
.super Ljava/lang/Object;
.source "WfdPlayer.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/allsharecastplayer/WfdPlayer;->initSurfaceview()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/sec/allsharecastplayer/WfdPlayer;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 293
    iput-object p1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$2;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    iput-object p2, p0, Lcom/sec/allsharecastplayer/WfdPlayer$2;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 3
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 327
    const-string v0, "WfdPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WfdPlayer/surfaceChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 310
    const-string v0, "WfdPlayer"

    const-string v1, "WfdPlayer/surfaceCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$2;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$200(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 314
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$2;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$200(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 315
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$2;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$2;->val$intent:Landroid/content/Intent;

    # invokes: Lcom/sec/allsharecastplayer/WfdPlayer;->prepareWfdPlayer(Landroid/content/Intent;)V
    invoke-static {v0, v1}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$400(Lcom/sec/allsharecastplayer/WfdPlayer;Landroid/content/Intent;)V

    .line 320
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$2;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mLaunchMode:I
    invoke-static {v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$000(Lcom/sec/allsharecastplayer/WfdPlayer;)I

    move-result v0

    if-nez v0, :cond_1

    .line 321
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$2;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/WfdPlayer;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$2;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    const v2, 0x7f050002

    invoke-virtual {v1, v2}, Lcom/sec/allsharecastplayer/WfdPlayer;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 323
    :cond_1
    return-void

    .line 317
    :cond_2
    const-string v0, "WfdPlayer"

    const-string v1, "mMediaPlayer is already null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    const/4 v2, 0x0

    .line 296
    const-string v0, "WfdPlayer"

    const-string v1, "WfdPlayer/surfaceDestroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$2;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # invokes: Lcom/sec/allsharecastplayer/WfdPlayer;->releaseWfdPlayer()V
    invoke-static {v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$100(Lcom/sec/allsharecastplayer/WfdPlayer;)V

    .line 299
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$2;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/WfdPlayer;->mSHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 300
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$2;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    iput-object v2, v0, Lcom/sec/allsharecastplayer/WfdPlayer;->mSfView:Landroid/view/SurfaceView;

    .line 301
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$2;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    iput-object v2, v0, Lcom/sec/allsharecastplayer/WfdPlayer;->mSHolder:Landroid/view/SurfaceHolder;

    .line 303
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$2;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    invoke-virtual {v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 304
    const-string v0, "WfdPlayer"

    const-string v1, "WfdPlayer/surfaceDestroyed so we have to finish the activity, if not already finishing"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$2;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    invoke-virtual {v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->finish()V

    .line 307
    :cond_0
    return-void
.end method
