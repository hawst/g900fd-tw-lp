.class Lcom/sec/allsharecastplayer/WfdPlayer$6;
.super Ljava/lang/Object;
.source "WfdPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/allsharecastplayer/WfdPlayer;->prepareWfdPlayer(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

.field final synthetic val$mAudioManager:Landroid/media/AudioManager;

.field final synthetic val$originalVolume:I


# direct methods
.method constructor <init>(Lcom/sec/allsharecastplayer/WfdPlayer;Landroid/media/AudioManager;I)V
    .locals 0

    .prologue
    .line 514
    iput-object p1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$6;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    iput-object p2, p0, Lcom/sec/allsharecastplayer/WfdPlayer$6;->val$mAudioManager:Landroid/media/AudioManager;

    iput p3, p0, Lcom/sec/allsharecastplayer/WfdPlayer$6;->val$originalVolume:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 518
    const-string v0, "WfdPlayer"

    const-string v1, "WfdPlayer/onCompletion"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$6;->val$mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x7

    iget v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer$6;->val$originalVolume:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 521
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$6;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # invokes: Lcom/sec/allsharecastplayer/WfdPlayer;->releaseWfdPlayer()V
    invoke-static {v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$100(Lcom/sec/allsharecastplayer/WfdPlayer;)V

    .line 522
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$6;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/allsharecastplayer/WfdPlayer;->mSfView:Landroid/view/SurfaceView;

    .line 523
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$6;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    invoke-virtual {v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->finish()V

    .line 524
    return-void
.end method
