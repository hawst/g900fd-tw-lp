.class Lcom/sec/allsharecastplayer/WfdPlayer$8;
.super Ljava/lang/Object;
.source "WfdPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/allsharecastplayer/WfdPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/allsharecastplayer/WfdPlayer;


# direct methods
.method constructor <init>(Lcom/sec/allsharecastplayer/WfdPlayer;)V
    .locals 0

    .prologue
    .line 547
    iput-object p1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$8;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 549
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 550
    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$8;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    iget-object v1, v1, Lcom/sec/allsharecastplayer/WfdPlayer;->mSfView:Landroid/view/SurfaceView;

    if-eqz v1, :cond_0

    .line 551
    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$8;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    iget-object v1, v1, Lcom/sec/allsharecastplayer/WfdPlayer;->mSfView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 552
    .local v0, "sh":Landroid/view/SurfaceHolder;
    const-string v1, "WfdPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onVideoSizeChanged width="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    if-eqz v0, :cond_0

    .line 554
    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$8;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    iget-object v1, v1, Lcom/sec/allsharecastplayer/WfdPlayer;->mSfView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->requestLayout()V

    .line 558
    .end local v0    # "sh":Landroid/view/SurfaceHolder;
    :cond_0
    return-void
.end method
