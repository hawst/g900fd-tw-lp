.class Lcom/sec/allsharecastplayer/WfdPlayer$9;
.super Ljava/lang/Object;
.source "WfdPlayer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/allsharecastplayer/WfdPlayer;->startWfdPlayer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field flag:Z

.field final synthetic this$0:Lcom/sec/allsharecastplayer/WfdPlayer;


# direct methods
.method constructor <init>(Lcom/sec/allsharecastplayer/WfdPlayer;)V
    .locals 1

    .prologue
    .line 566
    iput-object p1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$9;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 567
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer$9;->flag:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 569
    :goto_0
    iget-boolean v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$9;->flag:Z

    if-eqz v1, :cond_1

    .line 570
    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$9;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$200(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 571
    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$9;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    iget-object v1, v1, Lcom/sec/allsharecastplayer/WfdPlayer;->mConnectingScreen:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 573
    :try_start_0
    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$9;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    iget-object v1, v1, Lcom/sec/allsharecastplayer/WfdPlayer;->mConnectingScreen:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 574
    const-wide/16 v2, 0x1f4

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 575
    const-string v1, "WfdPlayer"

    const-string v2, "gone"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 581
    :cond_0
    :goto_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$9;->flag:Z

    goto :goto_0

    .line 576
    :catch_0
    move-exception v0

    .line 577
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 583
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    return-void
.end method
