.class Lcom/sec/allsharecastplayer/MainActivity$6$1$1;
.super Landroid/os/CountDownTimer;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/allsharecastplayer/MainActivity$6$1;->onSuccess()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/sec/allsharecastplayer/MainActivity$6$1;

.field final synthetic val$invitationMsg:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/sec/allsharecastplayer/MainActivity$6$1;JJLandroid/widget/TextView;)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 461
    iput-object p1, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$6$1;

    iput-object p6, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1$1;->val$invitationMsg:Landroid/widget/TextView;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 469
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$6$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$6$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$6;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    const/16 v1, 0x1e

    # setter for: Lcom/sec/allsharecastplayer/MainActivity;->mLapseTime:I
    invoke-static {v0, v1}, Lcom/sec/allsharecastplayer/MainActivity;->access$2202(Lcom/sec/allsharecastplayer/MainActivity;I)I

    .line 470
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$6$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$6$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$6;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mShowPinDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$1400(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 472
    return-void
.end method

.method public onTick(J)V
    .locals 6
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 463
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$6$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$6$1;->val$wifiP2pConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    if-eqz v0, :cond_0

    .line 464
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1$1;->val$invitationMsg:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$6$1;

    iget-object v1, v1, Lcom/sec/allsharecastplayer/MainActivity$6$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$6;

    iget-object v1, v1, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    const v2, 0x7f050005

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$6$1;

    iget-object v5, v5, Lcom/sec/allsharecastplayer/MainActivity$6$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$6;

    iget-object v5, v5, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mConnectedDevName:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/allsharecastplayer/MainActivity;->access$600(Lcom/sec/allsharecastplayer/MainActivity;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$6$1;

    iget-object v5, v5, Lcom/sec/allsharecastplayer/MainActivity$6$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$6;

    iget-object v5, v5, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # --operator for: Lcom/sec/allsharecastplayer/MainActivity;->mLapseTime:I
    invoke-static {v5}, Lcom/sec/allsharecastplayer/MainActivity;->access$2206(Lcom/sec/allsharecastplayer/MainActivity;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/sec/allsharecastplayer/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 467
    :cond_0
    return-void
.end method
