.class Lcom/sec/allsharecastplayer/MainActivity$1$1$1;
.super Landroid/os/CountDownTimer;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/allsharecastplayer/MainActivity$1$1;->onGroupInfoAvailable(Landroid/net/wifi/p2p/WifiP2pGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mTickCount:I

.field final synthetic this$2:Lcom/sec/allsharecastplayer/MainActivity$1$1;


# direct methods
.method constructor <init>(Lcom/sec/allsharecastplayer/MainActivity$1$1;JJ)V
    .locals 2
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 146
    iput-object p1, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$1$1;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    .line 147
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->mTickCount:I

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 160
    const-string v0, "AllShareCastPlayer"

    const-string v1, "mIntentTimer onFinish"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    iget v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->mTickCount:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->mTickCount:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 162
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$1$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # invokes: Lcom/sec/allsharecastplayer/MainActivity;->launchWfdPlayer()V
    invoke-static {v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$1000(Lcom/sec/allsharecastplayer/MainActivity;)V

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$1$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mIpAddr:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$900(Lcom/sec/allsharecastplayer/MainActivity;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$1$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mAlreadyLaunched:Z
    invoke-static {v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$800(Lcom/sec/allsharecastplayer/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$1$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/allsharecastplayer/MainActivity;->disconnectCurrentP2p(Z)V
    invoke-static {v0, v1}, Lcom/sec/allsharecastplayer/MainActivity;->access$1200(Lcom/sec/allsharecastplayer/MainActivity;Z)V

    .line 165
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$1$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # invokes: Lcom/sec/allsharecastplayer/MainActivity;->turnOnListenMode()V
    invoke-static {v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$000(Lcom/sec/allsharecastplayer/MainActivity;)V

    goto :goto_0
.end method

.method public onTick(J)V
    .locals 3
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$1$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mIpAddr:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$900(Lcom/sec/allsharecastplayer/MainActivity;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$1$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mMacAddr:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$400(Lcom/sec/allsharecastplayer/MainActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$1$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$1$1;

    iget-object v1, v1, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v1, v1, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    iget-object v2, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$1$1;

    iget-object v2, v2, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v2, v2, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mMacAddr:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/allsharecastplayer/MainActivity;->access$400(Lcom/sec/allsharecastplayer/MainActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/allsharecastplayer/MainActivity;->getIpFromArpTable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/allsharecastplayer/MainActivity;->mIpAddr:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/allsharecastplayer/MainActivity;->access$902(Lcom/sec/allsharecastplayer/MainActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 152
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->mTickCount:I

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    iget v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->mTickCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->mTickCount:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->mTickCount:I

    if-nez v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;->this$2:Lcom/sec/allsharecastplayer/MainActivity$1$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # invokes: Lcom/sec/allsharecastplayer/MainActivity;->launchWfdPlayer()V
    invoke-static {v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$1000(Lcom/sec/allsharecastplayer/MainActivity;)V

    goto :goto_0
.end method
