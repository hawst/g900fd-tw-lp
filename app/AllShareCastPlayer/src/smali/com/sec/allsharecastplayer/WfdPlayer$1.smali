.class Lcom/sec/allsharecastplayer/WfdPlayer$1;
.super Landroid/content/BroadcastReceiver;
.source "WfdPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/allsharecastplayer/WfdPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/allsharecastplayer/WfdPlayer;


# direct methods
.method constructor <init>(Lcom/sec/allsharecastplayer/WfdPlayer;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$1;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 78
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "action":Ljava/lang/String;
    const-string v2, "WfdPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "intent received "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    const-string v2, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 82
    const-string v2, "networkInfo"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    .line 84
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    iget-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer$1;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mLaunchMode:I
    invoke-static {v2}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$000(Lcom/sec/allsharecastplayer/WfdPlayer;)I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 85
    const-string v2, "WfdPlayer"

    const-string v3, "WfdPlayer/network disconnected"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iget-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer$1;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # invokes: Lcom/sec/allsharecastplayer/WfdPlayer;->releaseWfdPlayer()V
    invoke-static {v2}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$100(Lcom/sec/allsharecastplayer/WfdPlayer;)V

    .line 87
    iget-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer$1;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    invoke-virtual {v2}, Lcom/sec/allsharecastplayer/WfdPlayer;->finish()V

    .line 123
    .end local v1    # "networkInfo":Landroid/net/NetworkInfo;
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    iget-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer$1;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$200(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/MediaPlayer;

    move-result-object v2

    if-nez v2, :cond_2

    .line 97
    const-string v2, "WfdPlayer"

    const-string v3, "MediaPlayer is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 101
    :cond_2
    iget-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer$1;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->bPrepared:Z
    invoke-static {v2}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$300(Lcom/sec/allsharecastplayer/WfdPlayer;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 103
    const-string v2, "WfdPlayer"

    const-string v3, "WfdPlayer is not prepared!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 107
    :cond_3
    const-string v2, "android.intent.action.WIFI_DISPLAY_SINK_PLAY"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 109
    const-string v2, "WfdPlayer"

    const-string v3, "WfdPlayer issuing RTSP PLAY to mediaplayer"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer$1;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$200(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/MediaPlayer;

    move-result-object v2

    const/16 v3, 0x708

    const-string v4, "WIFIDISPLAY_PLAY"

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaPlayer;->setParameter(ILjava/lang/String;)Z

    goto :goto_0

    .line 112
    :cond_4
    const-string v2, "android.intent.action.WIFI_DISPLAY_SINK_PAUSE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 114
    const-string v2, "WfdPlayer"

    const-string v3, "WfdPlayer issuing RTSP PAUSE to mediaplayer"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer$1;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$200(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/MediaPlayer;

    move-result-object v2

    const/16 v3, 0x709

    const-string v4, "WIFIDISPLAY_PAUSE"

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaPlayer;->setParameter(ILjava/lang/String;)Z

    goto :goto_0

    .line 117
    :cond_5
    const-string v2, "android.intent.action.WIFI_DISPLAY_SINK_TEARDOWN"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 119
    const-string v2, "WfdPlayer"

    const-string v3, "WfdPlayer issuing RTSP TEARDOWN to mediaplayer"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iget-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer$1;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$200(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/MediaPlayer;

    move-result-object v2

    const/16 v3, 0x70a

    const-string v4, "WIFIDISPLAY_TEARDOWN"

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaPlayer;->setParameter(ILjava/lang/String;)Z

    goto :goto_0
.end method
