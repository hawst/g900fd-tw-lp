.class Lcom/sec/allsharecastplayer/MainActivity$1$1;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/allsharecastplayer/MainActivity$1;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

.field final synthetic val$wifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;


# direct methods
.method constructor <init>(Lcom/sec/allsharecastplayer/MainActivity$1;Landroid/net/wifi/p2p/WifiP2pInfo;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iput-object p2, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->val$wifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGroupInfoAvailable(Landroid/net/wifi/p2p/WifiP2pGroup;)V
    .locals 7
    .param p1, "group"    # Landroid/net/wifi/p2p/WifiP2pGroup;

    .prologue
    .line 119
    # setter for: Lcom/sec/allsharecastplayer/MainActivity;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-static {p1}, Lcom/sec/allsharecastplayer/MainActivity;->access$202(Landroid/net/wifi/p2p/WifiP2pGroup;)Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 120
    const-string v0, "AllShareCastPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onGroupInfoAvailable mCreatedGroupInfo = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-static {}, Lcom/sec/allsharecastplayer/MainActivity;->access$200()Landroid/net/wifi/p2p/WifiP2pGroup;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-static {}, Lcom/sec/allsharecastplayer/MainActivity;->access$200()Landroid/net/wifi/p2p/WifiP2pGroup;

    move-result-object v0

    if-nez v0, :cond_1

    .line 123
    const-string v0, "AllShareCastPlayer"

    const-string v1, "mCreatedGroupInfo is null."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->val$wifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->val$wifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;

    iget-boolean v0, v0, Landroid/net/wifi/p2p/WifiP2pInfo;->isGroupOwner:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mMacAddr:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$400(Lcom/sec/allsharecastplayer/MainActivity;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 128
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-static {}, Lcom/sec/allsharecastplayer/MainActivity;->access$200()Landroid/net/wifi/p2p/WifiP2pGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v1

    iget-object v1, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    # setter for: Lcom/sec/allsharecastplayer/MainActivity;->mMacAddr:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/allsharecastplayer/MainActivity;->access$402(Lcom/sec/allsharecastplayer/MainActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 132
    :cond_2
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mHasConnectedHistory:Z
    invoke-static {v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$500(Lcom/sec/allsharecastplayer/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 133
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v1, v1, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mMacAddr:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/allsharecastplayer/MainActivity;->access$400(Lcom/sec/allsharecastplayer/MainActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v2, v2, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mConnectedDevName:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/allsharecastplayer/MainActivity;->access$600(Lcom/sec/allsharecastplayer/MainActivity;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/allsharecastplayer/MainActivity;->updateSavedDeviceDb(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/sec/allsharecastplayer/MainActivity;->access$700(Lcom/sec/allsharecastplayer/MainActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :cond_3
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mAlreadyLaunched:Z
    invoke-static {v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$800(Lcom/sec/allsharecastplayer/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mIpAddr:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$900(Lcom/sec/allsharecastplayer/MainActivity;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->val$wifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->val$wifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    if-eqz v0, :cond_4

    .line 136
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->val$wifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;

    iget-boolean v0, v0, Landroid/net/wifi/p2p/WifiP2pInfo;->isGroupOwner:Z

    if-nez v0, :cond_5

    .line 137
    const-string v0, "AllShareCastPlayer"

    const-string v1, "I am GC"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->val$wifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;

    iget-object v1, v1, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/allsharecastplayer/MainActivity;->mIpAddr:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/allsharecastplayer/MainActivity;->access$902(Lcom/sec/allsharecastplayer/MainActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 139
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # invokes: Lcom/sec/allsharecastplayer/MainActivity;->launchWfdPlayer()V
    invoke-static {v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$1000(Lcom/sec/allsharecastplayer/MainActivity;)V

    .line 145
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mAlreadyLaunched:Z
    invoke-static {v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$800(Lcom/sec/allsharecastplayer/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v6, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    new-instance v0, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;

    const-wide/16 v2, 0x4e20

    const-wide/16 v4, 0x64

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/allsharecastplayer/MainActivity$1$1$1;-><init>(Lcom/sec/allsharecastplayer/MainActivity$1$1;JJ)V

    # setter for: Lcom/sec/allsharecastplayer/MainActivity;->mIntentTimer:Landroid/os/CountDownTimer;
    invoke-static {v6, v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$1102(Lcom/sec/allsharecastplayer/MainActivity;Landroid/os/CountDownTimer;)Landroid/os/CountDownTimer;

    .line 169
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$1$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$1;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$1;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mIntentTimer:Landroid/os/CountDownTimer;
    invoke-static {v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$1100(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/os/CountDownTimer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto/16 :goto_0

    .line 141
    :cond_5
    const-string v0, "AllShareCastPlayer"

    const-string v1, "I am GO"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
