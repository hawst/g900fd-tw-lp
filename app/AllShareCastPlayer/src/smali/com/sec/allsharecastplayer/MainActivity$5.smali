.class Lcom/sec/allsharecastplayer/MainActivity$5;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/allsharecastplayer/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field currentIndex:I

.field final synthetic this$0:Lcom/sec/allsharecastplayer/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/allsharecastplayer/MainActivity;)V
    .locals 1

    .prologue
    .line 352
    iput-object p1, p0, Lcom/sec/allsharecastplayer/MainActivity$5;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 353
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/allsharecastplayer/MainActivity$5;->currentIndex:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 357
    :try_start_0
    iget v5, p0, Lcom/sec/allsharecastplayer/MainActivity$5;->currentIndex:I

    rem-int/lit8 v5, v5, 0x6

    iput v5, p0, Lcom/sec/allsharecastplayer/MainActivity$5;->currentIndex:I

    .line 358
    iget-object v5, p0, Lcom/sec/allsharecastplayer/MainActivity$5;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    iget-object v6, p0, Lcom/sec/allsharecastplayer/MainActivity$5;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mLayoutIds:[I
    invoke-static {v6}, Lcom/sec/allsharecastplayer/MainActivity;->access$1700(Lcom/sec/allsharecastplayer/MainActivity;)[I

    move-result-object v6

    iget v7, p0, Lcom/sec/allsharecastplayer/MainActivity$5;->currentIndex:I

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Lcom/sec/allsharecastplayer/MainActivity;->setContentView(I)V

    .line 360
    iget v5, p0, Lcom/sec/allsharecastplayer/MainActivity$5;->currentIndex:I

    if-nez v5, :cond_0

    .line 361
    iget-object v5, p0, Lcom/sec/allsharecastplayer/MainActivity$5;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    const v6, 0x7f070007

    invoke-virtual {v5, v6}, Lcom/sec/allsharecastplayer/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 362
    .local v3, "tv1":Landroid/widget/TextView;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "1. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 364
    iget-object v5, p0, Lcom/sec/allsharecastplayer/MainActivity$5;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    const v6, 0x7f070008

    invoke-virtual {v5, v6}, Lcom/sec/allsharecastplayer/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 365
    .local v4, "tv2":Landroid/widget/TextView;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "2. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    .end local v3    # "tv1":Landroid/widget/TextView;
    .end local v4    # "tv2":Landroid/widget/TextView;
    :cond_0
    iget-object v5, p0, Lcom/sec/allsharecastplayer/MainActivity$5;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    const v6, 0x7f070006

    invoke-virtual {v5, v6}, Lcom/sec/allsharecastplayer/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 368
    .local v2, "tv":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/sec/allsharecastplayer/MainActivity$5;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/allsharecastplayer/MainActivity;->access$1600(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f050019

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/allsharecastplayer/MainActivity$5;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    iget-object v9, p0, Lcom/sec/allsharecastplayer/MainActivity$5;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/sec/allsharecastplayer/MainActivity;->access$1600(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/content/Context;

    move-result-object v9

    # invokes: Lcom/sec/allsharecastplayer/MainActivity;->getDevName(Landroid/content/Context;)Ljava/lang/String;
    invoke-static {v8, v9}, Lcom/sec/allsharecastplayer/MainActivity;->access$1800(Lcom/sec/allsharecastplayer/MainActivity;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 369
    .local v0, "devStr":Ljava/lang/String;
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 370
    const/4 v5, 0x6

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setSystemUiVisibility(I)V

    .line 372
    iget v5, p0, Lcom/sec/allsharecastplayer/MainActivity$5;->currentIndex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/allsharecastplayer/MainActivity$5;->currentIndex:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 378
    .end local v0    # "devStr":Ljava/lang/String;
    .end local v2    # "tv":Landroid/widget/TextView;
    :goto_0
    iget-object v5, p0, Lcom/sec/allsharecastplayer/MainActivity$5;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/sec/allsharecastplayer/MainActivity;->access$1900(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/os/Handler;

    move-result-object v5

    const-wide/16 v6, 0x1388

    invoke-virtual {v5, p0, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 379
    return-void

    .line 373
    :catch_0
    move-exception v1

    .line 374
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 375
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 376
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method
