.class public Lcom/sec/allsharecastplayer/DBAdapter;
.super Ljava/lang/Object;
.source "DBAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/allsharecastplayer/DBAdapter$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "savedDevice.db"

.field private static final DATABASE_TABLE:Ljava/lang/String; = "tbl_savedDevice"

.field private static final DATABASE_TABLE_CREATE:Ljava/lang/String; = "CREATE TABLE tbl_savedDevice (id INTEGER PRIMARY KEY AUTOINCREMENT,macAddr TEXT NOT NULL,devName TEXT NOT NULL);"

.field private static final DATABASE_TABLE_DROP:Ljava/lang/String; = "DROP TABLE IF EXISTS tbl_savedDevice"

.field private static final DATABASE_VERSION:I = 0x1

.field public static final DEV_NAME:Ljava/lang/String; = "devName"

.field public static final MAC_ADDR:Ljava/lang/String; = "macAddr"

.field private static final TAG:Ljava/lang/String; = "AllShareCastPlayer_DB"


# instance fields
.field private context:Landroid/content/Context;

.field private mDb:Landroid/database/sqlite/SQLiteDatabase;

.field private mDbHelper:Lcom/sec/allsharecastplayer/DBAdapter$DatabaseHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "_context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/sec/allsharecastplayer/DBAdapter;->context:Landroid/content/Context;

    .line 38
    new-instance v0, Lcom/sec/allsharecastplayer/DBAdapter$DatabaseHelper;

    iget-object v2, p0, Lcom/sec/allsharecastplayer/DBAdapter;->context:Landroid/content/Context;

    const-string v3, "savedDevice.db"

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/allsharecastplayer/DBAdapter$DatabaseHelper;-><init>(Lcom/sec/allsharecastplayer/DBAdapter;Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object v0, p0, Lcom/sec/allsharecastplayer/DBAdapter;->mDbHelper:Lcom/sec/allsharecastplayer/DBAdapter$DatabaseHelper;

    .line 39
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/allsharecastplayer/DBAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 69
    return-void
.end method

.method public deleteEntry(J)Z
    .locals 5
    .param p1, "rowID"    # J

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/allsharecastplayer/DBAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "tbl_savedDevice"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAllEntries()Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 90
    iget-object v0, p0, Lcom/sec/allsharecastplayer/DBAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "tbl_savedDevice"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "macAddr"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "devName"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getEntry(J)Landroid/database/Cursor;
    .locals 11
    .param p1, "rowID"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 95
    iget-object v0, p0, Lcom/sec/allsharecastplayer/DBAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "tbl_savedDevice"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "id"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "macAddr"

    aput-object v5, v2, v3

    const/4 v3, 0x2

    const-string v5, "devName"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "id="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 98
    .local v9, "mCursor":Landroid/database/Cursor;
    if-eqz v9, :cond_0

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 100
    :cond_0
    return-object v9
.end method

.method public getEntry(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "macAddr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 104
    iget-object v0, p0, Lcom/sec/allsharecastplayer/DBAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "tbl_savedDevice"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "id"

    aput-object v3, v2, v6

    const-string v3, "macAddr"

    aput-object v3, v2, v7

    const/4 v3, 0x2

    const-string v4, "devName"

    aput-object v4, v2, v3

    const-string v3, "macAddr=?"

    new-array v4, v7, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 106
    .local v8, "mCursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 108
    :cond_0
    return-object v8
.end method

.method public insertEntry(Ljava/lang/String;Ljava/lang/String;)J
    .locals 4
    .param p1, "macAddr"    # Ljava/lang/String;
    .param p2, "devName"    # Ljava/lang/String;

    .prologue
    .line 72
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 73
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "macAddr"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v1, "devName"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iget-object v1, p0, Lcom/sec/allsharecastplayer/DBAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "tbl_savedDevice"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    return-wide v2
.end method

.method public open()Lcom/sec/allsharecastplayer/DBAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/allsharecastplayer/DBAdapter;->mDbHelper:Lcom/sec/allsharecastplayer/DBAdapter$DatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/allsharecastplayer/DBAdapter$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/allsharecastplayer/DBAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 64
    return-object p0
.end method

.method public updateEntry(JLjava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "rowID"    # J
    .param p3, "macAddr"    # Ljava/lang/String;
    .param p4, "devName"    # Ljava/lang/String;

    .prologue
    .line 79
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 80
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "macAddr"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string v1, "devName"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v1, p0, Lcom/sec/allsharecastplayer/DBAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "tbl_savedDevice"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
