.class Lcom/sec/allsharecastplayer/WfdPlayer$10;
.super Ljava/lang/Object;
.source "WfdPlayer.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/allsharecastplayer/WfdPlayer;->disconnectP2pConnection()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/allsharecastplayer/WfdPlayer;


# direct methods
.method constructor <init>(Lcom/sec/allsharecastplayer/WfdPlayer;)V
    .locals 0

    .prologue
    .line 646
    iput-object p1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$10;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .locals 3
    .param p1, "reason"    # I

    .prologue
    .line 651
    const-string v0, "WfdPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " remove group fail "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    return-void
.end method

.method public onSuccess()V
    .locals 2

    .prologue
    .line 648
    const-string v0, "WfdPlayer"

    const-string v1, " remove group success while exiting wfdPlayer"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    return-void
.end method
