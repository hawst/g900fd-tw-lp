.class Lcom/sec/allsharecastplayer/WfdPlayer$5;
.super Ljava/lang/Object;
.source "WfdPlayer.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/allsharecastplayer/WfdPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/allsharecastplayer/WfdPlayer;


# direct methods
.method constructor <init>(Lcom/sec/allsharecastplayer/WfdPlayer;)V
    .locals 0

    .prologue
    .line 429
    iput-object p1, p0, Lcom/sec/allsharecastplayer/WfdPlayer$5;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private switchCoordinates([I[I[II)V
    .locals 5
    .param p1, "id"    # [I
    .param p2, "x"    # [I
    .param p3, "y"    # [I
    .param p4, "index"    # I

    .prologue
    const/4 v4, 0x0

    .line 433
    aget v0, p1, v4

    .line 434
    .local v0, "tempID":I
    aget v1, p2, v4

    .line 435
    .local v1, "tempX":I
    aget v2, p3, v4

    .line 436
    .local v2, "tempY":I
    aget v3, p1, p4

    aput v3, p1, v4

    .line 437
    aget v3, p2, p4

    aput v3, p2, v4

    .line 438
    aget v3, p2, p4

    aput v3, p3, v4

    .line 439
    aput v0, p1, p4

    .line 440
    aput v1, p2, p4

    .line 441
    aput v2, p3, p4

    .line 442
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const v10, 0x461c4000    # 10000.0f

    .line 445
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 446
    .local v0, "action":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    .line 452
    .local v6, "pointerCount":I
    iget-object v9, p0, Lcom/sec/allsharecastplayer/WfdPlayer$5;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;
    invoke-static {v9}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$500(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/UIBCInputHandler;

    move-result-object v9

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/sec/allsharecastplayer/WfdPlayer$5;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;
    invoke-static {v9}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$500(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/UIBCInputHandler;

    move-result-object v9

    invoke-virtual {v9}, Landroid/media/UIBCInputHandler;->isActive()Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/sec/allsharecastplayer/WfdPlayer$5;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    iget-object v9, v9, Lcom/sec/allsharecastplayer/WfdPlayer;->mSfView:Landroid/view/SurfaceView;

    if-eqz v9, :cond_1

    .line 455
    iget-object v9, p0, Lcom/sec/allsharecastplayer/WfdPlayer$5;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    iget-object v9, v9, Lcom/sec/allsharecastplayer/WfdPlayer;->mSfView:Landroid/view/SurfaceView;

    invoke-virtual {v9}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 456
    .local v4, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget v9, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    int-to-float v9, v9

    div-float v2, v10, v9

    .line 457
    .local v2, "kX":F
    iget v9, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    int-to-float v9, v9

    div-float v3, v10, v9

    .line 459
    .local v3, "kY":F
    new-array v8, v6, [I

    .line 460
    .local v8, "y":[I
    new-array v7, v6, [I

    .line 461
    .local v7, "x":[I
    new-array v1, v6, [I

    .line 462
    .local v1, "id":[I
    const/4 v5, 0x0

    .local v5, "p":I
    :goto_0
    if-ge v5, v6, :cond_0

    .line 463
    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v9

    mul-float/2addr v9, v2

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v9

    aput v9, v7, v5

    .line 464
    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v9

    mul-float/2addr v9, v3

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v9

    aput v9, v8, v5

    .line 465
    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v9

    aput v9, v1, v5

    .line 462
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 468
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 488
    .end local v1    # "id":[I
    .end local v2    # "kX":F
    .end local v3    # "kY":F
    .end local v4    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .end local v5    # "p":I
    .end local v7    # "x":[I
    .end local v8    # "y":[I
    :cond_1
    :goto_1
    :pswitch_0
    const/4 v9, 0x1

    return v9

    .line 470
    .restart local v1    # "id":[I
    .restart local v2    # "kX":F
    .restart local v3    # "kY":F
    .restart local v4    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .restart local v5    # "p":I
    .restart local v7    # "x":[I
    .restart local v8    # "y":[I
    :pswitch_1
    iget-object v9, p0, Lcom/sec/allsharecastplayer/WfdPlayer$5;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;
    invoke-static {v9}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$500(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/UIBCInputHandler;

    invoke-static {v6, v1, v7, v8}, Landroid/media/UIBCInputHandler;->handleMove(I[I[I[I)V

    goto :goto_1

    .line 474
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v9

    invoke-direct {p0, v1, v7, v8, v9}, Lcom/sec/allsharecastplayer/WfdPlayer$5;->switchCoordinates([I[I[II)V

    .line 476
    :pswitch_3
    iget-object v9, p0, Lcom/sec/allsharecastplayer/WfdPlayer$5;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;
    invoke-static {v9}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$500(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/UIBCInputHandler;

    invoke-static {v6, v1, v7, v8}, Landroid/media/UIBCInputHandler;->handleDown(I[I[I[I)V

    goto :goto_1

    .line 479
    :pswitch_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v9

    invoke-direct {p0, v1, v7, v8, v9}, Lcom/sec/allsharecastplayer/WfdPlayer$5;->switchCoordinates([I[I[II)V

    .line 481
    :pswitch_5
    iget-object v9, p0, Lcom/sec/allsharecastplayer/WfdPlayer$5;->this$0:Lcom/sec/allsharecastplayer/WfdPlayer;

    # getter for: Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;
    invoke-static {v9}, Lcom/sec/allsharecastplayer/WfdPlayer;->access$500(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/UIBCInputHandler;

    invoke-static {v6, v1, v7, v8}, Landroid/media/UIBCInputHandler;->handleUp(I[I[I[I)V

    goto :goto_1

    .line 468
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method
