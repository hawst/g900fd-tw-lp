.class public Lcom/sec/allsharecastplayer/WfdPlayer;
.super Landroid/app/Activity;
.source "WfdPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# static fields
.field private static final DEBUG:Z = false

.field public static final KEY_PARAMETER_WIFIDISPLAY_PAUSE:I = 0x709

.field public static final KEY_PARAMETER_WIFIDISPLAY_PLAY:I = 0x708

.field public static final KEY_PARAMETER_WIFIDISPLAY_TEARDOWN:I = 0x70a

.field private static final TAG:Ljava/lang/String; = "WfdPlayer"

.field private static mCpuBooster:Landroid/os/DVFSHelper;


# instance fields
.field private bPrepared:Z

.field private displayMetrics:Landroid/util/DisplayMetrics;

.field private key1:I

.field private key2:I

.field mConnectingScreen:Landroid/widget/RelativeLayout;

.field mContext:Landroid/content/Context;

.field private mFinishToast:Landroid/widget/Toast;

.field private final mIntentFilter:Landroid/content/IntentFilter;

.field private mIsBackPressed:Z

.field private mIsSPC:Z

.field private mKeyListener:Landroid/view/View$OnKeyListener;

.field private mLaunchMode:I

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field mSHolder:Landroid/view/SurfaceHolder;

.field mSfView:Landroid/view/SurfaceView;

.field private mTouchListener:Landroid/view/View$OnTouchListener;

.field private mUIBCHdl:Landroid/media/UIBCInputHandler;

.field private final mVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field private mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

.field private m_close_handler:Landroid/os/Handler;

.field private wm:Landroid/view/WindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/allsharecastplayer/WfdPlayer;->mCpuBooster:Landroid/os/DVFSHelper;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 48
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mIntentFilter:Landroid/content/IntentFilter;

    .line 50
    iput-boolean v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->bPrepared:Z

    .line 51
    iput-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;

    .line 52
    iput v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->key1:I

    .line 53
    iput v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->key2:I

    .line 54
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->displayMetrics:Landroid/util/DisplayMetrics;

    .line 55
    iput-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->wm:Landroid/view/WindowManager;

    .line 56
    iput-boolean v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mIsBackPressed:Z

    .line 57
    iput-boolean v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mIsSPC:Z

    .line 58
    iput-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 59
    iput-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mFinishToast:Landroid/widget/Toast;

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mLaunchMode:I

    .line 75
    new-instance v0, Lcom/sec/allsharecastplayer/WfdPlayer$1;

    invoke-direct {v0, p0}, Lcom/sec/allsharecastplayer/WfdPlayer$1;-><init>(Lcom/sec/allsharecastplayer/WfdPlayer;)V

    iput-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 332
    new-instance v0, Lcom/sec/allsharecastplayer/WfdPlayer$3;

    invoke-direct {v0, p0}, Lcom/sec/allsharecastplayer/WfdPlayer$3;-><init>(Lcom/sec/allsharecastplayer/WfdPlayer;)V

    iput-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mKeyListener:Landroid/view/View$OnKeyListener;

    .line 419
    new-instance v0, Lcom/sec/allsharecastplayer/WfdPlayer$4;

    invoke-direct {v0, p0}, Lcom/sec/allsharecastplayer/WfdPlayer$4;-><init>(Lcom/sec/allsharecastplayer/WfdPlayer;)V

    iput-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->m_close_handler:Landroid/os/Handler;

    .line 429
    new-instance v0, Lcom/sec/allsharecastplayer/WfdPlayer$5;

    invoke-direct {v0, p0}, Lcom/sec/allsharecastplayer/WfdPlayer$5;-><init>(Lcom/sec/allsharecastplayer/WfdPlayer;)V

    iput-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 547
    new-instance v0, Lcom/sec/allsharecastplayer/WfdPlayer$8;

    invoke-direct {v0, p0}, Lcom/sec/allsharecastplayer/WfdPlayer$8;-><init>(Lcom/sec/allsharecastplayer/WfdPlayer;)V

    iput-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/allsharecastplayer/WfdPlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/WfdPlayer;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mLaunchMode:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/allsharecastplayer/WfdPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/WfdPlayer;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->releaseWfdPlayer()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/WfdPlayer;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/allsharecastplayer/WfdPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/WfdPlayer;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->bPrepared:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/allsharecastplayer/WfdPlayer;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/WfdPlayer;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/sec/allsharecastplayer/WfdPlayer;->prepareWfdPlayer(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/allsharecastplayer/WfdPlayer;)Landroid/media/UIBCInputHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/WfdPlayer;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/allsharecastplayer/WfdPlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/WfdPlayer;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->key1:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/allsharecastplayer/WfdPlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/WfdPlayer;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->key1:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/allsharecastplayer/WfdPlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/WfdPlayer;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->key2:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/allsharecastplayer/WfdPlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/WfdPlayer;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->key2:I

    return p1
.end method

.method static synthetic access$802(Lcom/sec/allsharecastplayer/WfdPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/WfdPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mIsBackPressed:Z

    return p1
.end method

.method static synthetic access$900()Landroid/os/DVFSHelper;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/sec/allsharecastplayer/WfdPlayer;->mCpuBooster:Landroid/os/DVFSHelper;

    return-object v0
.end method

.method private broadcastWfdSessionInfo(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    .line 161
    const-string v1, "WfdPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "broadcastWfdSessionInfo :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.WIFI_DISPLAY_SINK_STATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 164
    .local v0, "activityIntent":Landroid/content/Intent;
    const-string v1, "state"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 166
    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 167
    return-void
.end method

.method private disconnectP2pConnection()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 636
    const-string v1, "WfdPlayer"

    const-string v2, "disconnectP2pConnection"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    const-string v1, "wifip2p"

    invoke-virtual {p0, v1}, Lcom/sec/allsharecastplayer/WfdPlayer;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 638
    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v1, :cond_0

    .line 639
    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    invoke-virtual {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v1, p0, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    .line 641
    .local v0, "channel":Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    if-nez v0, :cond_1

    .line 642
    const-string v1, "WfdPlayer"

    const-string v2, "Failed to set up connection with wifi p2p service"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    iput-object v3, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 656
    .end local v0    # "channel":Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    :cond_0
    :goto_0
    return-void

    .line 646
    .restart local v0    # "channel":Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    :cond_1
    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    new-instance v2, Lcom/sec/allsharecastplayer/WfdPlayer$10;

    invoke-direct {v2, p0}, Lcom/sec/allsharecastplayer/WfdPlayer$10;-><init>(Lcom/sec/allsharecastplayer/WfdPlayer;)V

    invoke-virtual {v1, v0, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    goto :goto_0
.end method

.method private initSurfaceview()V
    .locals 5

    .prologue
    .line 272
    const v2, 0x7f07000f

    invoke-virtual {p0, v2}, Lcom/sec/allsharecastplayer/WfdPlayer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mConnectingScreen:Landroid/widget/RelativeLayout;

    .line 274
    const-string v2, "ro.product.board"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 275
    .local v0, "board":Ljava/lang/String;
    const-string v2, "WfdPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "board info: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    const-string v2, ".*spc.*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 277
    const-string v2, "WfdPlayer"

    const-string v3, "SPC feature is on"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mIsSPC:Z

    .line 281
    :cond_0
    const-string v2, "WfdPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "spc is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mIsSPC:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    iget-boolean v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mIsSPC:Z

    if-eqz v2, :cond_1

    .line 283
    iget-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mConnectingScreen:Landroid/widget/RelativeLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 285
    :cond_1
    const v2, 0x7f07000e

    invoke-virtual {p0, v2}, Lcom/sec/allsharecastplayer/WfdPlayer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/SurfaceView;

    iput-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mSfView:Landroid/view/SurfaceView;

    .line 286
    iget-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mSfView:Landroid/view/SurfaceView;

    invoke-virtual {v2}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mSHolder:Landroid/view/SurfaceHolder;

    .line 287
    iget-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mSfView:Landroid/view/SurfaceView;

    iget-object v3, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/view/SurfaceView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 288
    iget-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mSfView:Landroid/view/SurfaceView;

    iget-object v3, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/view/SurfaceView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 289
    iget-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mContext:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    iput-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->wm:Landroid/view/WindowManager;

    .line 290
    iget-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->wm:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->displayMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v2, v3}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 291
    iget-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mSfView:Landroid/view/SurfaceView;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Landroid/view/SurfaceView;->setSystemUiVisibility(I)V

    .line 292
    invoke-virtual {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 293
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mSHolder:Landroid/view/SurfaceHolder;

    new-instance v3, Lcom/sec/allsharecastplayer/WfdPlayer$2;

    invoke-direct {v3, p0, v1}, Lcom/sec/allsharecastplayer/WfdPlayer$2;-><init>(Lcom/sec/allsharecastplayer/WfdPlayer;Landroid/content/Intent;)V

    invoke-interface {v2, v3}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 330
    return-void
.end method

.method private pauseWfdPlayer()V
    .locals 2

    .prologue
    .line 608
    const-string v0, "WfdPlayer"

    const-string v1, "WfdPlayer/pauseWfdPlayer"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 610
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mSfView:Landroid/view/SurfaceView;

    if-eqz v0, :cond_0

    .line 612
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 615
    :cond_0
    return-void
.end method

.method private prepareWfdPlayer(Landroid/content/Intent;)V
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v10, 0x0

    .line 493
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 495
    .local v4, "uri":Landroid/net/Uri;
    :try_start_0
    const-string v5, "WfdPlayer"

    const-string v6, "WfdPlayer/prepareWfdPlayer"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    const/4 v5, 0x1

    invoke-direct {p0, v5}, Lcom/sec/allsharecastplayer/WfdPlayer;->broadcastWfdSessionInfo(I)V

    .line 498
    new-instance v5, Landroid/os/DVFSHelper;

    iget-object v6, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mContext:Landroid/content/Context;

    const/16 v7, 0xc

    invoke-direct {v5, v6, v7}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    sput-object v5, Lcom/sec/allsharecastplayer/WfdPlayer;->mCpuBooster:Landroid/os/DVFSHelper;

    .line 499
    sget-object v5, Lcom/sec/allsharecastplayer/WfdPlayer;->mCpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v5}, Landroid/os/DVFSHelper;->getSupportedCPUFrequency()[I

    move-result-object v3

    .line 500
    .local v3, "supportedCPUFreqTable":[I
    if-eqz v3, :cond_0

    .line 503
    sget-object v5, Lcom/sec/allsharecastplayer/WfdPlayer;->mCpuBooster:Landroid/os/DVFSHelper;

    const-string v6, "CPU"

    const/4 v7, 0x0

    aget v7, v3, v7

    int-to-long v8, v7

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 505
    :cond_0
    sget-object v5, Lcom/sec/allsharecastplayer/WfdPlayer;->mCpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v5}, Landroid/os/DVFSHelper;->acquire()V

    .line 507
    const-string v5, "audio"

    invoke-virtual {p0, v5}, Lcom/sec/allsharecastplayer/WfdPlayer;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    .line 508
    .local v1, "mAudioManager":Landroid/media/AudioManager;
    const/4 v5, 0x7

    invoke-virtual {v1, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v2

    .line 509
    .local v2, "originalVolume":I
    const/4 v5, 0x7

    const/4 v6, 0x7

    invoke-virtual {v1, v6}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual {v1, v5, v6, v7}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 510
    iget-object v5, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v6, 0x7

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 511
    iget-object v5, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 512
    iget-object v5, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 513
    iget-object v5, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v6, Lcom/sec/allsharecastplayer/WfdPlayer$6;

    invoke-direct {v6, p0, v1, v2}, Lcom/sec/allsharecastplayer/WfdPlayer$6;-><init>(Lcom/sec/allsharecastplayer/WfdPlayer;Landroid/media/AudioManager;I)V

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 526
    iget-object v5, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v6, Lcom/sec/allsharecastplayer/WfdPlayer$7;

    invoke-direct {v6, p0}, Lcom/sec/allsharecastplayer/WfdPlayer$7;-><init>(Lcom/sec/allsharecastplayer/WfdPlayer;)V

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 536
    iget-object v5, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 538
    iget-object v5, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 539
    iget-object v5, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 545
    .end local v1    # "mAudioManager":Landroid/media/AudioManager;
    .end local v2    # "originalVolume":I
    .end local v3    # "supportedCPUFreqTable":[I
    :goto_0
    return-void

    .line 540
    :catch_0
    move-exception v0

    .line 541
    .local v0, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "error : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 542
    invoke-direct {p0, v10}, Lcom/sec/allsharecastplayer/WfdPlayer;->broadcastWfdSessionInfo(I)V

    .line 543
    sget-object v5, Lcom/sec/allsharecastplayer/WfdPlayer;->mCpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v5}, Landroid/os/DVFSHelper;->release()V

    goto :goto_0
.end method

.method private releaseWfdPlayer()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 594
    const-string v0, "WfdPlayer"

    const-string v1, "WfdPlayer/releaseWfdPlayer"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 596
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 599
    :cond_0
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 600
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 601
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 602
    invoke-direct {p0, v2}, Lcom/sec/allsharecastplayer/WfdPlayer;->broadcastWfdSessionInfo(I)V

    .line 604
    :cond_1
    iput-boolean v2, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->bPrepared:Z

    .line 605
    return-void
.end method

.method private resumeWfdPlayer()V
    .locals 2

    .prologue
    .line 618
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 619
    const-string v0, "WfdPlayer"

    const-string v1, "WfdPlayer/resumeWfdPlayer"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    invoke-direct {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->startWfdPlayer()V

    .line 622
    :cond_0
    return-void
.end method

.method private startPeriodicScan()V
    .locals 5

    .prologue
    .line 673
    const-string v3, "WfdPlayer"

    const-string v4, "startPeriodicScan"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    const-string v3, "wifi"

    invoke-virtual {p0, v3}, Lcom/sec/allsharecastplayer/WfdPlayer;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 676
    .local v2, "tWifiManager":Landroid/net/wifi/WifiManager;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 677
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 678
    .local v1, "msg":Landroid/os/Message;
    const/16 v3, 0x12

    iput v3, v1, Landroid/os/Message;->what:I

    .line 679
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 680
    .local v0, "args":Landroid/os/Bundle;
    const-string v3, "stop"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 681
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 682
    invoke-virtual {v2, v1}, Landroid/net/wifi/WifiManager;->callSECApi(Landroid/os/Message;)I

    .line 684
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method private startWfdPlayer()V
    .locals 4

    .prologue
    .line 562
    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 564
    const-string v1, "WfdPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WfdPlayer/startWfdPlayer="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->bPrepared:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 566
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/sec/allsharecastplayer/WfdPlayer$9;

    invoke-direct {v1, p0}, Lcom/sec/allsharecastplayer/WfdPlayer$9;-><init>(Lcom/sec/allsharecastplayer/WfdPlayer;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 585
    iget-boolean v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->bPrepared:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    .line 586
    const-string v1, "WfdPlayer"

    const-string v2, "WfdPlayer/startWfdPlayer"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 591
    .end local v0    # "handler":Landroid/os/Handler;
    :cond_0
    return-void
.end method

.method private stopPeriodicScan()V
    .locals 5

    .prologue
    .line 659
    const-string v3, "WfdPlayer"

    const-string v4, "stopPeriodicScan"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    const-string v3, "wifi"

    invoke-virtual {p0, v3}, Lcom/sec/allsharecastplayer/WfdPlayer;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 662
    .local v2, "tWifiManager":Landroid/net/wifi/WifiManager;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 663
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 664
    .local v1, "msg":Landroid/os/Message;
    const/16 v3, 0x12

    iput v3, v1, Landroid/os/Message;->what:I

    .line 665
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 666
    .local v0, "args":Landroid/os/Bundle;
    const-string v3, "stop"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 667
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 668
    invoke-virtual {v2, v1}, Landroid/net/wifi/WifiManager;->callSECApi(Landroid/os/Message;)I

    .line 670
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 4

    .prologue
    .line 402
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 403
    const-string v0, "WfdPlayer"

    const-string v1, "skip the back key!! It was pressed before started"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    :goto_0
    return-void

    .line 407
    :cond_0
    iget-boolean v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mIsBackPressed:Z

    if-nez v0, :cond_1

    .line 408
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mFinishToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 409
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mIsBackPressed:Z

    .line 410
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->m_close_handler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 412
    :cond_1
    invoke-direct {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->releaseWfdPlayer()V

    .line 413
    iget v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mLaunchMode:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 414
    invoke-direct {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->disconnectP2pConnection()V

    .line 415
    :cond_2
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v4, 0x400

    const/4 v3, 0x0

    .line 128
    invoke-virtual {p0, v3, v3}, Lcom/sec/allsharecastplayer/WfdPlayer;->overridePendingTransition(II)V

    .line 129
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 130
    const-string v0, "WfdPlayer"

    const-string v1, "WfdPlayer/onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    const v0, 0x1030128

    invoke-virtual {p0, v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->setTheme(I)V

    .line 132
    invoke-virtual {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mContext:Landroid/content/Context;

    .line 134
    invoke-virtual {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "launchMode"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mLaunchMode:I

    .line 135
    const v0, 0x7f050001

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mFinishToast:Landroid/widget/Toast;

    .line 137
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->requestWindowFeature(I)Z

    .line 138
    invoke-virtual {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 141
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 142
    const v0, 0x7f030007

    invoke-virtual {p0, v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->setContentView(I)V

    .line 143
    invoke-direct {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->initSurfaceview()V

    .line 145
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.wfd_support"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.WIFI_DISPLAY_SINK_PLAY"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.WIFI_DISPLAY_SINK_PAUSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.WIFI_DISPLAY_SINK_TEARDOWN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/allsharecastplayer/WfdPlayer;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 156
    new-instance v0, Landroid/media/UIBCInputHandler;

    iget-object v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/media/UIBCInputHandler;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;

    .line 157
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 626
    const-string v0, "WfdPlayer"

    const-string v1, "WfdPlayer/onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    iget v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mLaunchMode:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 628
    invoke-direct {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->disconnectP2pConnection()V

    .line 630
    :cond_0
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 632
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 633
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 361
    const-string v0, "WfdPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WfdPlayer::onKeyDown action = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " code = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;

    invoke-virtual {v0}, Landroid/media/UIBCInputHandler;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    iput p1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->key1:I

    .line 370
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->key2:I

    .line 371
    const-string v0, "WfdPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "key1 = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;

    iget v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->key1:I

    iget v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->key2:I

    invoke-static {v0, v1}, Landroid/media/UIBCInputHandler;->keyDown(II)V

    .line 375
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 380
    const-string v0, "WfdPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WfdPlayer::onKeyUp action = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " code = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;

    invoke-virtual {v0}, Landroid/media/UIBCInputHandler;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388
    iput p1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->key1:I

    .line 389
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->key2:I

    .line 390
    const-string v0, "WfdPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "key1 = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mUIBCHdl:Landroid/media/UIBCInputHandler;

    iget v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->key1:I

    iget v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->key2:I

    invoke-static {v0, v1}, Landroid/media/UIBCInputHandler;->keyUp(II)V

    .line 392
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 393
    invoke-virtual {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->onBackPressed()V

    .line 396
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 171
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 172
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "launchMode"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mLaunchMode:I

    .line 173
    const-string v1, "WfdPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNewIntent, intent received "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 178
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 179
    const-string v0, "WfdPlayer"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    invoke-direct {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->startPeriodicScan()V

    .line 182
    return-void
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 11
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    const/4 v10, 0x1

    .line 223
    const-string v7, "WfdPlayer"

    const-string v8, "WfdPlayer/onPrepared"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    const-string v7, "WfdPlayer"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "WfdPlayer/onPrepared "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mConnectingScreen:Landroid/widget/RelativeLayout;

    invoke-virtual {v9}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    invoke-direct {p0, v10}, Lcom/sec/allsharecastplayer/WfdPlayer;->broadcastWfdSessionInfo(I)V

    .line 229
    iput-boolean v10, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->bPrepared:Z

    .line 232
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v6

    .line 233
    .local v6, "videoWidth":I
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v4

    .line 234
    .local v4, "videoHeight":I
    int-to-float v7, v6

    int-to-float v8, v4

    div-float v5, v7, v8

    .line 235
    .local v5, "videoRatio":F
    invoke-virtual {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v7

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Display;->getWidth()I

    move-result v3

    .line 236
    .local v3, "screenWidth":I
    invoke-virtual {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v7

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Display;->getHeight()I

    move-result v1

    .line 237
    .local v1, "screenHeight":I
    iget-object v7, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mSfView:Landroid/view/SurfaceView;

    invoke-virtual {v7}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 238
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    const/4 v2, 0x0

    .line 240
    .local v2, "screenRatio":F
    if-le v1, v3, :cond_1

    .line 241
    const-string v7, "WfdPlayer"

    const-string v8, "screenHeight > screenWidth"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    int-to-float v7, v1

    int-to-float v8, v3

    div-float v2, v7, v8

    .line 244
    cmpl-float v7, v5, v2

    if-lez v7, :cond_0

    .line 245
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 246
    int-to-float v7, v1

    div-float/2addr v7, v5

    float-to-int v7, v7

    iput v7, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 263
    :goto_0
    const-string v7, "WfdPlayer"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "size : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": SCW"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": SCH"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    iget-object v7, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mSfView:Landroid/view/SurfaceView;

    invoke-virtual {v7, v0}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 266
    invoke-direct {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->startWfdPlayer()V

    .line 267
    sget-object v7, Lcom/sec/allsharecastplayer/WfdPlayer;->mCpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v7}, Landroid/os/DVFSHelper;->release()V

    .line 268
    return-void

    .line 248
    :cond_0
    int-to-float v7, v3

    mul-float/2addr v7, v5

    float-to-int v7, v7

    iput v7, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 249
    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    .line 253
    :cond_1
    int-to-float v7, v3

    int-to-float v8, v1

    div-float v2, v7, v8

    .line 255
    cmpl-float v7, v5, v2

    if-lez v7, :cond_2

    .line 256
    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 257
    int-to-float v7, v3

    div-float/2addr v7, v5

    float-to-int v7, v7

    iput v7, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    .line 259
    :cond_2
    int-to-float v7, v1

    mul-float/2addr v7, v5

    float-to-int v7, v7

    iput v7, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 260
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 186
    const-string v0, "WfdPlayer"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 188
    invoke-direct {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->stopPeriodicScan()V

    .line 198
    iget-object v0, p0, Lcom/sec/allsharecastplayer/WfdPlayer;->mSfView:Landroid/view/SurfaceView;

    if-nez v0, :cond_0

    .line 199
    invoke-virtual {p0}, Lcom/sec/allsharecastplayer/WfdPlayer;->finish()V

    .line 204
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 208
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 209
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const/4 v1, 0x0

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 212
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 216
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/allsharecastplayer/WfdPlayer;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 218
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 219
    return-void
.end method
