.class Lcom/sec/allsharecastplayer/MainActivity$6;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/allsharecastplayer/MainActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/allsharecastplayer/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/allsharecastplayer/MainActivity;)V
    .locals 0

    .prologue
    .line 410
    iput-object p1, p0, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttached()V
    .locals 0

    .prologue
    .line 492
    return-void
.end method

.method public onConnectionRequested(Landroid/net/wifi/p2p/WifiP2pDevice;Landroid/net/wifi/p2p/WifiP2pConfig;)V
    .locals 5
    .param p1, "device"    # Landroid/net/wifi/p2p/WifiP2pDevice;
    .param p2, "config"    # Landroid/net/wifi/p2p/WifiP2pConfig;

    .prologue
    const/4 v4, 0x1

    .line 415
    const-string v1, "AllShareCastPlayer"

    const-string v2, " onConnectionRequested"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {}, Lcom/sec/allsharecastplayer/MainActivity;->access$1300()Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v1

    if-nez v1, :cond_0

    .line 417
    const-string v1, "AllShareCastPlayer"

    const-string v2, "mWifiP2pManager == null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    :goto_0
    return-void

    .line 421
    :cond_0
    if-eqz p1, :cond_1

    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->wfdInfo:Landroid/net/wifi/p2p/WifiP2pWfdInfo;

    if-eqz v1, :cond_1

    .line 422
    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    iget-object v2, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->wfdInfo:Landroid/net/wifi/p2p/WifiP2pWfdInfo;

    invoke-virtual {v2}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;->getControlPort()I

    move-result v2

    # setter for: Lcom/sec/allsharecastplayer/MainActivity;->mCtrlPort:I
    invoke-static {v1, v2}, Lcom/sec/allsharecastplayer/MainActivity;->access$2002(Lcom/sec/allsharecastplayer/MainActivity;I)I

    .line 423
    const-string v1, "AllShareCastPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onConnectionRequested wfdInfo="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mCtrlPort:I
    invoke-static {v3}, Lcom/sec/allsharecastplayer/MainActivity;->access$2000(Lcom/sec/allsharecastplayer/MainActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    iget-object v2, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    # setter for: Lcom/sec/allsharecastplayer/MainActivity;->mConnectedDevName:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/allsharecastplayer/MainActivity;->access$602(Lcom/sec/allsharecastplayer/MainActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 426
    const-string v1, "AllShareCastPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "devName = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mConnectedDevName:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/allsharecastplayer/MainActivity;->access$600(Lcom/sec/allsharecastplayer/MainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    :cond_1
    iget-object v1, p2, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    iget v1, v1, Landroid/net/wifi/WpsInfo;->setup:I

    if-nez v1, :cond_2

    .line 430
    const-string v1, "AllShareCastPlayer"

    const-string v2, "onConnectionRequested PBC"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    :cond_2
    iget-object v1, p2, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    iget v1, v1, Landroid/net/wifi/WpsInfo;->setup:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 433
    const-string v1, "AllShareCastPlayer"

    const-string v2, "onConnectionRequested KEYPAD"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    :cond_3
    iget-object v1, p2, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    iget v1, v1, Landroid/net/wifi/WpsInfo;->setup:I

    if-ne v1, v4, :cond_4

    .line 436
    const-string v1, "AllShareCastPlayer"

    const-string v2, "onConnectionRequested DISPLAY"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    :cond_4
    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    iget-object v2, p0, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    iget-object v3, p2, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    # invokes: Lcom/sec/allsharecastplayer/MainActivity;->convertDevAddress(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/allsharecastplayer/MainActivity;->access$2100(Lcom/sec/allsharecastplayer/MainActivity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/sec/allsharecastplayer/MainActivity;->mMacAddr:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/allsharecastplayer/MainActivity;->access$402(Lcom/sec/allsharecastplayer/MainActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 441
    const-string v1, "AllShareCastPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "macAddr = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mMacAddr:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/allsharecastplayer/MainActivity;->access$400(Lcom/sec/allsharecastplayer/MainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    const/4 v1, 0x0

    iput v1, p2, Landroid/net/wifi/p2p/WifiP2pConfig;->groupOwnerIntent:I

    .line 444
    move-object v0, p2

    .line 446
    .local v0, "wifiP2pConfig":Landroid/net/wifi/p2p/WifiP2pConfig;
    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # setter for: Lcom/sec/allsharecastplayer/MainActivity;->mHasConnectedHistory:Z
    invoke-static {v1, v4}, Lcom/sec/allsharecastplayer/MainActivity;->access$502(Lcom/sec/allsharecastplayer/MainActivity;Z)Z

    .line 448
    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {}, Lcom/sec/allsharecastplayer/MainActivity;->access$1300()Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    invoke-static {v2}, Lcom/sec/allsharecastplayer/MainActivity;->access$300(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v2

    new-instance v3, Lcom/sec/allsharecastplayer/MainActivity$6$1;

    invoke-direct {v3, p0, v0}, Lcom/sec/allsharecastplayer/MainActivity$6$1;-><init>(Lcom/sec/allsharecastplayer/MainActivity$6;Landroid/net/wifi/p2p/WifiP2pConfig;)V

    invoke-virtual {v1, v2, v0, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->connect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pConfig;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    goto/16 :goto_0
.end method

.method public onDetached(I)V
    .locals 0
    .param p1, "reason"    # I

    .prologue
    .line 493
    return-void
.end method

.method public onShowPinRequested(Ljava/lang/String;)V
    .locals 3
    .param p1, "pin"    # Ljava/lang/String;

    .prologue
    .line 412
    const-string v0, "AllShareCastPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onShowPinRequested = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    return-void
.end method
