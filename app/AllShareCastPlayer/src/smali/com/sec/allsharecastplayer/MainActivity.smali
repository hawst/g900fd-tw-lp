.class public Lcom/sec/allsharecastplayer/MainActivity;
.super Landroid/app/Activity;
.source "MainActivity.java"


# static fields
.field private static final CONNECTION_TIMED_OUT:I = 0x1e

.field private static final FLAG_ACTIVITY:I = 0x30808000

.field private static final PATTERN:Ljava/lang/String; = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$"

.field private static final TAG:Ljava/lang/String; = "AllShareCastPlayer"

.field private static mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

.field private static mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;


# instance fields
.field private DEBUG:Z

.field private mAlreadyLaunched:Z

.field private mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field private mConnectedDevName:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mCtrlPort:I

.field final mFlipperRunnable:Ljava/lang/Runnable;

.field private mHandler:Landroid/os/Handler;

.field private mHasConnectedHistory:Z

.field private final mIntentFilter:Landroid/content/IntentFilter;

.field private mIntentTimer:Landroid/os/CountDownTimer;

.field private mIpAddr:Ljava/lang/String;

.field private mLapseTime:I

.field private mLayoutIds:[I

.field private mMacAddr:Ljava/lang/String;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mShowPinDialog:Landroid/app/AlertDialog;

.field private mUri:Landroid/net/Uri;

.field private mWpsTimer:Landroid/os/CountDownTimer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    sput-object v0, Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 61
    sput-object v0, Lcom/sec/allsharecastplayer/MainActivity;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 58
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mIntentFilter:Landroid/content/IntentFilter;

    .line 62
    iput-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mIntentTimer:Landroid/os/CountDownTimer;

    .line 64
    iput-boolean v2, p0, Lcom/sec/allsharecastplayer/MainActivity;->mAlreadyLaunched:Z

    .line 66
    const/16 v0, 0x1c44

    iput v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mCtrlPort:I

    .line 67
    iput-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mMacAddr:Ljava/lang/String;

    .line 68
    iput-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mIpAddr:Ljava/lang/String;

    .line 69
    iput-boolean v2, p0, Lcom/sec/allsharecastplayer/MainActivity;->mHasConnectedHistory:Z

    .line 70
    iput-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mConnectedDevName:Ljava/lang/String;

    .line 71
    iput-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mShowPinDialog:Landroid/app/AlertDialog;

    .line 78
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mLayoutIds:[I

    .line 86
    iput-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mHandler:Landroid/os/Handler;

    .line 87
    iput-boolean v2, p0, Lcom/sec/allsharecastplayer/MainActivity;->DEBUG:Z

    .line 88
    new-instance v0, Lcom/sec/allsharecastplayer/MainActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/allsharecastplayer/MainActivity$1;-><init>(Lcom/sec/allsharecastplayer/MainActivity;)V

    iput-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 351
    new-instance v0, Lcom/sec/allsharecastplayer/MainActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/allsharecastplayer/MainActivity$5;-><init>(Lcom/sec/allsharecastplayer/MainActivity;)V

    iput-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mFlipperRunnable:Ljava/lang/Runnable;

    return-void

    .line 78
    nop

    :array_0
    .array-data 4
        0x7f030001
        0x7f030002
        0x7f030003
        0x7f030004
        0x7f030005
        0x7f030006
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/allsharecastplayer/MainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/allsharecastplayer/MainActivity;->turnOnListenMode()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/allsharecastplayer/MainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->DEBUG:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/allsharecastplayer/MainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/allsharecastplayer/MainActivity;->launchWfdPlayer()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/os/CountDownTimer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mIntentTimer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/allsharecastplayer/MainActivity;Landroid/os/CountDownTimer;)Landroid/os/CountDownTimer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;
    .param p1, "x1"    # Landroid/os/CountDownTimer;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mIntentTimer:Landroid/os/CountDownTimer;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/allsharecastplayer/MainActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/allsharecastplayer/MainActivity;->disconnectCurrentP2p(Z)V

    return-void
.end method

.method static synthetic access$1300()Landroid/net/wifi/p2p/WifiP2pManager;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mShowPinDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/allsharecastplayer/MainActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mShowPinDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/os/CountDownTimer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mWpsTimer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/allsharecastplayer/MainActivity;Landroid/os/CountDownTimer;)Landroid/os/CountDownTimer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;
    .param p1, "x1"    # Landroid/os/CountDownTimer;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mWpsTimer:Landroid/os/CountDownTimer;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/allsharecastplayer/MainActivity;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mLayoutIds:[I

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/allsharecastplayer/MainActivity;Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/allsharecastplayer/MainActivity;->getDevName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200()Landroid/net/wifi/p2p/WifiP2pGroup;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/sec/allsharecastplayer/MainActivity;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/allsharecastplayer/MainActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mCtrlPort:I

    return v0
.end method

.method static synthetic access$2002(Lcom/sec/allsharecastplayer/MainActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;
    .param p1, "x1"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mCtrlPort:I

    return p1
.end method

.method static synthetic access$202(Landroid/net/wifi/p2p/WifiP2pGroup;)Landroid/net/wifi/p2p/WifiP2pGroup;
    .locals 0
    .param p0, "x0"    # Landroid/net/wifi/p2p/WifiP2pGroup;

    .prologue
    .line 56
    sput-object p0, Lcom/sec/allsharecastplayer/MainActivity;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    return-object p0
.end method

.method static synthetic access$2100(Lcom/sec/allsharecastplayer/MainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/allsharecastplayer/MainActivity;->convertDevAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2202(Lcom/sec/allsharecastplayer/MainActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;
    .param p1, "x1"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mLapseTime:I

    return p1
.end method

.method static synthetic access$2206(Lcom/sec/allsharecastplayer/MainActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mLapseTime:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mLapseTime:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/allsharecastplayer/MainActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mMacAddr:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/allsharecastplayer/MainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mMacAddr:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/allsharecastplayer/MainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mHasConnectedHistory:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/allsharecastplayer/MainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mHasConnectedHistory:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/allsharecastplayer/MainActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mConnectedDevName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/allsharecastplayer/MainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mConnectedDevName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/allsharecastplayer/MainActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/sec/allsharecastplayer/MainActivity;->updateSavedDeviceDb(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/allsharecastplayer/MainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mAlreadyLaunched:Z

    return v0
.end method

.method static synthetic access$900(Lcom/sec/allsharecastplayer/MainActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mIpAddr:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/allsharecastplayer/MainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/allsharecastplayer/MainActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mIpAddr:Ljava/lang/String;

    return-object p1
.end method

.method private addActionsTo(Landroid/content/IntentFilter;)V
    .locals 1
    .param p1, "intentFilter"    # Landroid/content/IntentFilter;

    .prologue
    .line 601
    const-string v0, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 602
    const-string v0, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 603
    const-string v0, "android.intent.action.WIFI_DISPLAY"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 604
    const-string v0, "com.sec.android.spc.SPC_CONTROLLER_DEVICE"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 605
    return-void
.end method

.method private broadcastWFDListeningChanged(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    .line 707
    const-string v1, "AllShareCastPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "broadcastWFDListeningChanged :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.WIFI_DISPLAY_SINK_LISTENING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 710
    .local v0, "activityIntent":Landroid/content/Intent;
    const-string v1, "state"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 712
    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 713
    return-void
.end method

.method private convertDevAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "addr"    # Ljava/lang/String;

    .prologue
    const/16 v11, 0xe

    const/16 v10, 0xc

    const/4 v9, 0x0

    .line 575
    new-instance v3, Ljava/util/Formatter;

    invoke-direct {v3}, Ljava/util/Formatter;-><init>()V

    .line 576
    .local v3, "partialMacAddr":Ljava/util/Formatter;
    invoke-virtual {p1, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 577
    .local v4, "subString":Ljava/lang/String;
    const-string v2, ""

    .line 579
    .local v2, "formattedStr":Ljava/lang/String;
    const/16 v5, 0x10

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    .line 581
    .local v1, "enable":I
    xor-int/lit16 v1, v1, 0x80

    .line 582
    const-string v5, "%02x"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v5, v6}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 583
    const-string v5, "%02x"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 584
    iget-boolean v5, p0, Lcom/sec/allsharecastplayer/MainActivity;->DEBUG:Z

    if-eqz v5, :cond_0

    .line 585
    const-string v5, "AllShareCastPlayer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "convertDevAddress by fomatter : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "by String : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    :cond_0
    invoke-virtual {v3}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 588
    new-instance v5, Ljava/lang/NumberFormatException;

    invoke-direct {v5}, Ljava/lang/NumberFormatException;-><init>()V

    throw v5
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 590
    .end local v1    # "enable":I
    :catch_0
    move-exception v0

    .line 591
    .local v0, "e":Ljava/lang/NumberFormatException;
    :try_start_1
    const-string v5, "AllShareCastPlayer"

    const-string v6, "Exception occurd"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595
    invoke-virtual {v3}, Ljava/util/Formatter;->close()V

    .line 597
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p1, v11, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 595
    .restart local v1    # "enable":I
    :cond_1
    invoke-virtual {v3}, Ljava/util/Formatter;->close()V

    goto :goto_0

    .end local v1    # "enable":I
    :catchall_0
    move-exception v5

    invoke-virtual {v3}, Ljava/util/Formatter;->close()V

    throw v5
.end method

.method private disconnectCurrentP2p(Z)V
    .locals 4
    .param p1, "listen"    # Z

    .prologue
    .line 197
    move v0, p1

    .line 198
    .local v0, "canListen":Z
    sget-object v1, Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v2, p0, Lcom/sec/allsharecastplayer/MainActivity;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v3, Lcom/sec/allsharecastplayer/MainActivity$2;

    invoke-direct {v3, p0, v0}, Lcom/sec/allsharecastplayer/MainActivity$2;-><init>(Lcom/sec/allsharecastplayer/MainActivity;Z)V

    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 212
    return-void
.end method

.method private getDevName(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 555
    invoke-virtual {p0}, Lcom/sec/allsharecastplayer/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "device_name"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 557
    .local v0, "deviceName":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 558
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "wifi_p2p_device_name"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 560
    if-nez v0, :cond_0

    .line 563
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "android_id"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 565
    .local v1, "id":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 566
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Android_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 569
    .end local v1    # "id":Ljava/lang/String;
    :goto_0
    return-object v2

    :cond_0
    move-object v2, v0

    goto :goto_0
.end method

.method private isSavedDevice(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "macAddr"    # Ljava/lang/String;
    .param p2, "devName"    # Ljava/lang/String;

    .prologue
    .line 519
    new-instance v1, Lcom/sec/allsharecastplayer/DBAdapter;

    invoke-virtual {p0}, Lcom/sec/allsharecastplayer/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/allsharecastplayer/DBAdapter;-><init>(Landroid/content/Context;)V

    .line 520
    .local v1, "db":Lcom/sec/allsharecastplayer/DBAdapter;
    invoke-virtual {v1}, Lcom/sec/allsharecastplayer/DBAdapter;->open()Lcom/sec/allsharecastplayer/DBAdapter;

    .line 522
    invoke-virtual {v1, p1}, Lcom/sec/allsharecastplayer/DBAdapter;->getEntry(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 523
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 524
    const-string v2, "AllShareCastPlayer"

    const-string v3, "It\'s not saved one. new device!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 526
    invoke-virtual {v1}, Lcom/sec/allsharecastplayer/DBAdapter;->close()V

    .line 527
    const/4 v2, 0x0

    .line 540
    :goto_0
    return v2

    .line 530
    :cond_0
    const-string v2, "devName"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_1

    .line 532
    const-string v2, "id"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3, p1, p2}, Lcom/sec/allsharecastplayer/DBAdapter;->updateEntry(JLjava/lang/String;Ljava/lang/String;)Z

    .line 533
    iget-boolean v2, p0, Lcom/sec/allsharecastplayer/MainActivity;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 534
    const-string v2, "AllShareCastPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "devName"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is changed to"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 539
    invoke-virtual {v1}, Lcom/sec/allsharecastplayer/DBAdapter;->close()V

    .line 540
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isWFDSourceRunning()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 504
    const-string v2, "display"

    invoke-virtual {p0, v2}, Lcom/sec/allsharecastplayer/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 505
    .local v0, "displayManager":Landroid/hardware/display/DisplayManager;
    if-eqz v0, :cond_1

    .line 507
    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->isWfdEngineRunning()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 508
    const-string v2, "AllShareCastPlayer"

    const-string v3, "wfd is connected from sec-wfd"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    :goto_0
    return v1

    .line 510
    :cond_0
    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 511
    const-string v2, "AllShareCastPlayer"

    const-string v3, "wfd is connected from google-wfd"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 515
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private launchWfdPlayer()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 225
    const-string v1, "AllShareCastPlayer"

    const-string v2, "launchWfdPlayer"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    iget-boolean v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mAlreadyLaunched:Z

    if-eqz v1, :cond_0

    .line 250
    :goto_0
    return-void

    .line 231
    :cond_0
    iput-boolean v3, p0, Lcom/sec/allsharecastplayer/MainActivity;->mAlreadyLaunched:Z

    .line 232
    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mIpAddr:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 234
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 235
    .local v0, "tempIntent":Landroid/content/Intent;
    const-string v1, "com.sec.allsharecastplayer"

    const-string v2, "com.sec.allsharecastplayer.WfdPlayer"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 236
    const v1, 0x30808000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 238
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wfd://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/allsharecastplayer/MainActivity;->mIpAddr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/allsharecastplayer/MainActivity;->mCtrlPort:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mUri:Landroid/net/Uri;

    .line 239
    iget-boolean v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 240
    const-string v1, "AllShareCastPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "address "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/allsharecastplayer/MainActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    :cond_1
    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 243
    invoke-virtual {p0, v0}, Lcom/sec/allsharecastplayer/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 246
    .end local v0    # "tempIntent":Landroid/content/Intent;
    :cond_2
    const-string v1, "AllShareCastPlayer"

    const-string v2, "Can\'t launch Player, source IP is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    invoke-direct {p0, v3}, Lcom/sec/allsharecastplayer/MainActivity;->disconnectCurrentP2p(Z)V

    goto :goto_0
.end method

.method private requestP2pListen()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 313
    const-string v1, "AllShareCastPlayer"

    const-string v2, "requestP2pListen"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    sget-object v1, Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v1, :cond_0

    .line 316
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pWfdInfo;

    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;-><init>()V

    .line 318
    .local v0, "wfdInfo":Landroid/net/wifi/p2p/WifiP2pWfdInfo;
    invoke-virtual {v0, v3}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;->setSessionAvailable(Z)V

    .line 319
    invoke-virtual {v0, v3}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;->setWfdEnabled(Z)V

    .line 320
    const v1, 0xc000

    invoke-virtual {v0, v1}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;->setControlPort(I)V

    .line 321
    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;->setMaxThroughput(I)V

    .line 322
    invoke-virtual {v0, v3}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;->setDeviceType(I)Z

    .line 325
    sget-object v1, Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v2, p0, Lcom/sec/allsharecastplayer/MainActivity;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v3, Lcom/sec/allsharecastplayer/MainActivity$3;

    invoke-direct {v3, p0}, Lcom/sec/allsharecastplayer/MainActivity$3;-><init>(Lcom/sec/allsharecastplayer/MainActivity;)V

    invoke-virtual {v1, v2, v0, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->setWFDInfo(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pWfdInfo;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 338
    sget-object v1, Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v2, p0, Lcom/sec/allsharecastplayer/MainActivity;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v3, Lcom/sec/allsharecastplayer/MainActivity$4;

    invoke-direct {v3, p0}, Lcom/sec/allsharecastplayer/MainActivity$4;-><init>(Lcom/sec/allsharecastplayer/MainActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->requestP2pListen(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 349
    .end local v0    # "wfdInfo":Landroid/net/wifi/p2p/WifiP2pWfdInfo;
    :cond_0
    return-void
.end method

.method private stopWifiPeriodicScanning(Z)V
    .locals 4
    .param p1, "isStop"    # Z

    .prologue
    .line 616
    const-string v3, "wifi"

    invoke-virtual {p0, v3}, Lcom/sec/allsharecastplayer/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 617
    .local v2, "tWifiManager":Landroid/net/wifi/WifiManager;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 618
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 619
    .local v1, "msg":Landroid/os/Message;
    const/16 v3, 0x12

    iput v3, v1, Landroid/os/Message;->what:I

    .line 620
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 621
    .local v0, "args":Landroid/os/Bundle;
    const-string v3, "stop"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 622
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 623
    invoke-virtual {v2, v1}, Landroid/net/wifi/WifiManager;->callSECApi(Landroid/os/Message;)I

    .line 625
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method private turnOnListenMode()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 608
    iput-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mConnectedDevName:Ljava/lang/String;

    .line 609
    iput-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mMacAddr:Ljava/lang/String;

    .line 610
    iput-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mIpAddr:Ljava/lang/String;

    .line 611
    invoke-direct {p0}, Lcom/sec/allsharecastplayer/MainActivity;->requestP2pListen()V

    .line 612
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/allsharecastplayer/MainActivity;->broadcastWFDListeningChanged(I)V

    .line 613
    return-void
.end method

.method private updateSavedDeviceDb(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "macAddr"    # Ljava/lang/String;
    .param p2, "devName"    # Ljava/lang/String;

    .prologue
    .line 544
    iget-boolean v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 545
    const-string v1, "AllShareCastPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateSavedDeviceDb, macAddr="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    :cond_0
    new-instance v0, Lcom/sec/allsharecastplayer/DBAdapter;

    invoke-virtual {p0}, Lcom/sec/allsharecastplayer/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/allsharecastplayer/DBAdapter;-><init>(Landroid/content/Context;)V

    .line 548
    .local v0, "db":Lcom/sec/allsharecastplayer/DBAdapter;
    invoke-virtual {v0}, Lcom/sec/allsharecastplayer/DBAdapter;->open()Lcom/sec/allsharecastplayer/DBAdapter;

    .line 549
    invoke-virtual {v0, p1, p2}, Lcom/sec/allsharecastplayer/DBAdapter;->insertEntry(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 550
    const-string v1, "AllShareCastPlayer"

    const-string v2, "updateSavedDeviceDb error occured in inserting"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    :cond_1
    invoke-virtual {v0}, Lcom/sec/allsharecastplayer/DBAdapter;->close()V

    .line 552
    return-void
.end method


# virtual methods
.method public getIpFromArpTable(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "macAddr"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v10, 0x4

    .line 253
    iget-boolean v7, p0, Lcom/sec/allsharecastplayer/MainActivity;->DEBUG:Z

    if-eqz v7, :cond_0

    .line 254
    const-string v7, "AllShareCastPlayer"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getIpFromArpTable, mac address = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :cond_0
    if-nez p1, :cond_1

    move-object v3, v6

    .line 295
    :goto_0
    return-object v3

    .line 260
    :cond_1
    const/4 v0, 0x0

    .line 263
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    new-instance v8, Ljava/io/FileInputStream;

    const-string v9, "/proc/net/arp"

    invoke-direct {v8, v9}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    const-string v9, "UTF-8"

    invoke-direct {v7, v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v1, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 264
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 267
    .local v4, "line":Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    .line 268
    if-nez v4, :cond_3

    .line 291
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v4    # "line":Ljava/lang/String;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :goto_1
    move-object v3, v6

    .line 295
    goto :goto_0

    .line 271
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "line":Ljava/lang/String;
    :cond_3
    :try_start_3
    const-string v7, " +"

    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 272
    .local v5, "splitted":[Ljava/lang/String;
    if-eqz v5, :cond_5

    array-length v7, v5

    if-lt v7, v10, :cond_5

    const/4 v7, 0x3

    aget-object v7, v5, v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 273
    const/4 v7, 0x0

    aget-object v3, v5, v7

    .line 274
    .local v3, "ip":Ljava/lang/String;
    iget-boolean v7, p0, Lcom/sec/allsharecastplayer/MainActivity;->DEBUG:Z

    if-eqz v7, :cond_4

    .line 275
    const-string v7, "AllShareCastPlayer"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "case 1. converted to"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 291
    :cond_4
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 292
    :catch_0
    move-exception v6

    goto :goto_0

    .line 279
    .end local v3    # "ip":Ljava/lang/String;
    :cond_5
    if-eqz v5, :cond_2

    :try_start_5
    array-length v7, v5

    if-lt v7, v10, :cond_2

    invoke-direct {p0, p1}, Lcom/sec/allsharecastplayer/MainActivity;->convertDevAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x3

    aget-object v8, v5, v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 280
    const/4 v7, 0x0

    aget-object v3, v5, v7

    .line 281
    .restart local v3    # "ip":Ljava/lang/String;
    iget-boolean v7, p0, Lcom/sec/allsharecastplayer/MainActivity;->DEBUG:Z

    if-eqz v7, :cond_6

    .line 282
    const-string v7, "AllShareCastPlayer"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "case2. converted to"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 291
    :cond_6
    :try_start_6
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_0

    .line 292
    :catch_1
    move-exception v6

    goto/16 :goto_0

    .end local v3    # "ip":Ljava/lang/String;
    .end local v5    # "splitted":[Ljava/lang/String;
    :catch_2
    move-exception v7

    move-object v0, v1

    .line 294
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_1

    .line 287
    .end local v4    # "line":Ljava/lang/String;
    :catch_3
    move-exception v2

    .line 288
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_7
    const-string v7, "AllShareCastPlayer"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 291
    :try_start_8
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto/16 :goto_1

    .line 292
    :catch_4
    move-exception v7

    goto/16 :goto_1

    .line 290
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    .line 291
    :goto_3
    :try_start_9
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 293
    :goto_4
    throw v6

    .line 292
    :catch_5
    move-exception v7

    goto :goto_4

    .line 290
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "line":Ljava/lang/String;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_3

    .line 287
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_6
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method public isWiFiConnected()Z
    .locals 4

    .prologue
    .line 215
    const/4 v2, 0x0

    .line 216
    .local v2, "result":Z
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Lcom/sec/allsharecastplayer/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 218
    .local v1, "manager":Landroid/net/ConnectivityManager;
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 219
    .local v0, "info":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v2

    .line 221
    :cond_0
    return v2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v6, 0x7f05000e

    const v5, 0x7f050008

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 384
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 385
    const-string v0, "AllShareCastPlayer"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    invoke-direct {p0}, Lcom/sec/allsharecastplayer/MainActivity;->isWFDSourceRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 388
    new-array v0, v4, [Ljava/lang/Object;

    const v1, 0x7f050007

    invoke-virtual {p0, v1}, Lcom/sec/allsharecastplayer/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p0, v6}, Lcom/sec/allsharecastplayer/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v5, v0}, Lcom/sec/allsharecastplayer/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 389
    invoke-virtual {p0}, Lcom/sec/allsharecastplayer/MainActivity;->finish()V

    .line 395
    :cond_0
    :goto_0
    iput-object p0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mContext:Landroid/content/Context;

    .line 397
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mHandler:Landroid/os/Handler;

    .line 399
    const-string v0, "wifip2p"

    invoke-virtual {p0, v0}, Lcom/sec/allsharecastplayer/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    sput-object v0, Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 400
    sget-object v0, Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_3

    .line 401
    sget-object v0, Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    invoke-virtual {p0}, Lcom/sec/allsharecastplayer/MainActivity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 403
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez v0, :cond_2

    .line 404
    const-string v0, "AllShareCastPlayer"

    const-string v1, "Failed to set up connection with wifi p2p service"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 497
    :goto_1
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-direct {p0, v0}, Lcom/sec/allsharecastplayer/MainActivity;->addActionsTo(Landroid/content/IntentFilter;)V

    .line 501
    :goto_2
    return-void

    .line 390
    :cond_1
    const-string v0, "wlan.mirror.enabling"

    const-string v1, "false"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ready"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    new-array v0, v4, [Ljava/lang/Object;

    const v1, 0x7f05000d

    invoke-virtual {p0, v1}, Lcom/sec/allsharecastplayer/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p0, v6}, Lcom/sec/allsharecastplayer/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v5, v0}, Lcom/sec/allsharecastplayer/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 392
    invoke-virtual {p0}, Lcom/sec/allsharecastplayer/MainActivity;->finish()V

    goto :goto_0

    .line 408
    :cond_2
    sget-object v0, Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v0, v1}, Landroid/net/wifi/p2p/WifiP2pManager;->enableP2p(Landroid/net/wifi/p2p/WifiP2pManager$Channel;)V

    .line 410
    sget-object v0, Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/sec/allsharecastplayer/MainActivity$6;

    invoke-direct {v2, p0}, Lcom/sec/allsharecastplayer/MainActivity$6;-><init>(Lcom/sec/allsharecastplayer/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->setDialogListener(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;)V

    goto :goto_1

    .line 499
    :cond_3
    const-string v0, "AllShareCastPlayer"

    const-string v1, "mWifiP2pManager is null !"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 629
    const-string v0, "AllShareCastPlayer"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    invoke-direct {p0}, Lcom/sec/allsharecastplayer/MainActivity;->isWFDSourceRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 631
    invoke-direct {p0, v2}, Lcom/sec/allsharecastplayer/MainActivity;->disconnectCurrentP2p(Z)V

    .line 634
    invoke-direct {p0, v2}, Lcom/sec/allsharecastplayer/MainActivity;->stopWifiPeriodicScanning(Z)V

    .line 635
    sget-object v0, Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    .line 636
    sget-object v0, Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->setDialogListener(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;)V

    .line 640
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 641
    return-void
.end method

.method protected onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 645
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 646
    const-string v1, "AllShareCastPlayer"

    const-string v2, "onPause"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 648
    invoke-direct {p0, v3}, Lcom/sec/allsharecastplayer/MainActivity;->broadcastWFDListeningChanged(I)V

    .line 650
    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mIntentTimer:Landroid/os/CountDownTimer;

    if-eqz v1, :cond_0

    .line 651
    const-string v1, "AllShareCastPlayer"

    const-string v2, "onPause, cancel timer"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mIntentTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v1}, Landroid/os/CountDownTimer;->cancel()V

    .line 655
    :cond_0
    sget-object v1, Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v1, :cond_1

    .line 656
    const-string v1, "AllShareCastPlayer"

    const-string v2, "clearing the wifi p2p info"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pWfdInfo;

    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;-><init>()V

    .line 660
    .local v0, "wfdInfo":Landroid/net/wifi/p2p/WifiP2pWfdInfo;
    invoke-virtual {v0, v3}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;->setSessionAvailable(Z)V

    .line 661
    invoke-virtual {v0, v3}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;->setWfdEnabled(Z)V

    .line 663
    sget-object v1, Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v2, p0, Lcom/sec/allsharecastplayer/MainActivity;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v3, Lcom/sec/allsharecastplayer/MainActivity$7;

    invoke-direct {v3, p0}, Lcom/sec/allsharecastplayer/MainActivity$7;-><init>(Lcom/sec/allsharecastplayer/MainActivity;)V

    invoke-virtual {v1, v2, v0, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->setWFDInfo(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pWfdInfo;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 676
    const-string v1, "AllShareCastPlayer"

    const-string v2, "onPause, stopPeerDiscovery"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    sget-object v1, Lcom/sec/allsharecastplayer/MainActivity;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v2, p0, Lcom/sec/allsharecastplayer/MainActivity;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v1, v2, v4}, Landroid/net/wifi/p2p/WifiP2pManager;->stopPeerDiscovery(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 680
    .end local v0    # "wfdInfo":Landroid/net/wifi/p2p/WifiP2pWfdInfo;
    :cond_1
    iput-object v4, p0, Lcom/sec/allsharecastplayer/MainActivity;->mIntentTimer:Landroid/os/CountDownTimer;

    .line 681
    iput-object v4, p0, Lcom/sec/allsharecastplayer/MainActivity;->mConnectedDevName:Ljava/lang/String;

    .line 682
    iput-object v4, p0, Lcom/sec/allsharecastplayer/MainActivity;->mMacAddr:Ljava/lang/String;

    .line 683
    iput-object v4, p0, Lcom/sec/allsharecastplayer/MainActivity;->mIpAddr:Ljava/lang/String;

    .line 684
    sput-object v4, Lcom/sec/allsharecastplayer/MainActivity;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 685
    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/allsharecastplayer/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 686
    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/allsharecastplayer/MainActivity;->mFlipperRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 687
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 691
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 692
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mAlreadyLaunched:Z

    .line 694
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mFlipperRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 695
    const-string v0, "AllShareCastPlayer"

    const-string v1, "onResume, stop Wifi periodic scanning"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 698
    invoke-direct {p0, v2}, Lcom/sec/allsharecastplayer/MainActivity;->stopWifiPeriodicScanning(Z)V

    .line 701
    invoke-direct {p0, v2}, Lcom/sec/allsharecastplayer/MainActivity;->disconnectCurrentP2p(Z)V

    .line 702
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/allsharecastplayer/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 703
    return-void
.end method

.method public validate(Ljava/lang/String;)Z
    .locals 3
    .param p1, "ip"    # Ljava/lang/String;

    .prologue
    .line 305
    const-string v2, "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 306
    .local v1, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 307
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    return v2
.end method
