.class Lcom/sec/allsharecastplayer/MainActivity$6$1;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/allsharecastplayer/MainActivity$6;->onConnectionRequested(Landroid/net/wifi/p2p/WifiP2pDevice;Landroid/net/wifi/p2p/WifiP2pConfig;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/allsharecastplayer/MainActivity$6;

.field final synthetic val$wifiP2pConfig:Landroid/net/wifi/p2p/WifiP2pConfig;


# direct methods
.method constructor <init>(Lcom/sec/allsharecastplayer/MainActivity$6;Landroid/net/wifi/p2p/WifiP2pConfig;)V
    .locals 0

    .prologue
    .line 448
    iput-object p1, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$6;

    iput-object p2, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1;->val$wifiP2pConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .locals 3
    .param p1, "reason"    # I

    .prologue
    .line 488
    const-string v0, "AllShareCastPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " connect fail "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    return-void
.end method

.method public onSuccess()V
    .locals 10

    .prologue
    .line 450
    const-string v0, "AllShareCastPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " connect success config.wps.pin = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1;->val$wifiP2pConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    iget-object v2, v2, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    iget-object v2, v2, Landroid/net/wifi/WpsInfo;->pin:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1;->val$wifiP2pConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    iget-object v0, v0, Landroid/net/wifi/WpsInfo;->pin:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 454
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$6;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$1600(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/high16 v1, 0x7f030000

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 456
    .local v8, "pinView":Landroid/view/View;
    const v0, 0x7f070001

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 457
    .local v6, "invitationMsg":Landroid/widget/TextView;
    const v0, 0x7f070002

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 458
    .local v7, "pinText":Landroid/widget/TextView;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$6;

    iget-object v1, v1, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    const v2, 0x7f05000c

    invoke-virtual {v1, v2}, Lcom/sec/allsharecastplayer/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1;->val$wifiP2pConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    iget-object v1, v1, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    iget-object v1, v1, Landroid/net/wifi/WpsInfo;->pin:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 459
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$6;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    const/16 v1, 0x1e

    # setter for: Lcom/sec/allsharecastplayer/MainActivity;->mLapseTime:I
    invoke-static {v0, v1}, Lcom/sec/allsharecastplayer/MainActivity;->access$2202(Lcom/sec/allsharecastplayer/MainActivity;I)I

    .line 461
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$6;

    iget-object v9, v0, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    new-instance v0, Lcom/sec/allsharecastplayer/MainActivity$6$1$1;

    const-wide/16 v2, 0x7530

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/allsharecastplayer/MainActivity$6$1$1;-><init>(Lcom/sec/allsharecastplayer/MainActivity$6$1;JJLandroid/widget/TextView;)V

    invoke-virtual {v0}, Lcom/sec/allsharecastplayer/MainActivity$6$1$1;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    # setter for: Lcom/sec/allsharecastplayer/MainActivity;->mWpsTimer:Landroid/os/CountDownTimer;
    invoke-static {v9, v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$1502(Lcom/sec/allsharecastplayer/MainActivity;Landroid/os/CountDownTimer;)Landroid/os/CountDownTimer;

    .line 475
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$6;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$6;

    iget-object v2, v2, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/allsharecastplayer/MainActivity;->access$1600(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$6;

    iget-object v2, v2, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    const v3, 0x7f05000b

    invoke-virtual {v2, v3}, Lcom/sec/allsharecastplayer/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$6;

    iget-object v2, v2, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    const v3, 0x7f05000a

    invoke-virtual {v2, v3}, Lcom/sec/allsharecastplayer/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/allsharecastplayer/MainActivity$6$1$2;

    invoke-direct {v3, p0}, Lcom/sec/allsharecastplayer/MainActivity$6$1$2;-><init>(Lcom/sec/allsharecastplayer/MainActivity$6$1;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    # setter for: Lcom/sec/allsharecastplayer/MainActivity;->mShowPinDialog:Landroid/app/AlertDialog;
    invoke-static {v0, v1}, Lcom/sec/allsharecastplayer/MainActivity;->access$1402(Lcom/sec/allsharecastplayer/MainActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 484
    iget-object v0, p0, Lcom/sec/allsharecastplayer/MainActivity$6$1;->this$1:Lcom/sec/allsharecastplayer/MainActivity$6;

    iget-object v0, v0, Lcom/sec/allsharecastplayer/MainActivity$6;->this$0:Lcom/sec/allsharecastplayer/MainActivity;

    # getter for: Lcom/sec/allsharecastplayer/MainActivity;->mShowPinDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/allsharecastplayer/MainActivity;->access$1400(Lcom/sec/allsharecastplayer/MainActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 486
    .end local v6    # "invitationMsg":Landroid/widget/TextView;
    .end local v7    # "pinText":Landroid/widget/TextView;
    .end local v8    # "pinView":Landroid/view/View;
    :cond_0
    return-void
.end method
