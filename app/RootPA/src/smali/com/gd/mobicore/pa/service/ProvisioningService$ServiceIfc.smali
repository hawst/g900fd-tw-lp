.class Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;
.super Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;
.source "ProvisioningService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/gd/mobicore/pa/service/ProvisioningService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceIfc"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;


# direct methods
.method public constructor <init>(Lcom/gd/mobicore/pa/service/ProvisioningService;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    .line 74
    invoke-direct {p0}, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;-><init>()V

    .line 75
    return-void
.end method

.method private commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    invoke-virtual {v0}, Lcom/gd/mobicore/pa/service/ProvisioningService;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v0

    return-object v0
.end method

.method private mapSpContainerState(I)Lcom/gd/mobicore/pa/ifc/SPContainerState;
    .locals 4
    .param p1, "containerState"    # I

    .prologue
    .line 481
    sget-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;->UNDEFINED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 482
    .local v0, "state":Lcom/gd/mobicore/pa/ifc/SPContainerState;
    packed-switch p1, :pswitch_data_0

    .line 502
    const-string v1, "RootPA-J"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mapSpContainerState returning undefined: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    sget-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;->UNDEFINED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 506
    :goto_0
    return-object v0

    .line 484
    :pswitch_0
    sget-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;->DOES_NOT_EXIST:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 485
    goto :goto_0

    .line 487
    :pswitch_1
    sget-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;->REGISTERED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 488
    goto :goto_0

    .line 490
    :pswitch_2
    sget-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;->ACTIVATED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 491
    goto :goto_0

    .line 493
    :pswitch_3
    sget-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;->ROOT_LOCKED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 494
    goto :goto_0

    .line 496
    :pswitch_4
    sget-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;->SP_LOCKED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 497
    goto :goto_0

    .line 499
    :pswitch_5
    sget-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;->ROOT_SP_LOCKED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 500
    goto :goto_0

    .line 482
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private mapTltContainerState(I)Lcom/gd/mobicore/pa/ifc/TrustletContainerState;
    .locals 4
    .param p1, "containerState"    # I

    .prologue
    .line 461
    sget-object v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->UNDEFINED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    .line 462
    .local v0, "state":Lcom/gd/mobicore/pa/ifc/TrustletContainerState;
    packed-switch p1, :pswitch_data_0

    .line 473
    :pswitch_0
    const-string v1, "RootPA-J"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mapTltContainerState returning undefined: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    sget-object v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->UNDEFINED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    .line 477
    :goto_0
    return-object v0

    .line 464
    :pswitch_1
    sget-object v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->REGISTERED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    .line 465
    goto :goto_0

    .line 467
    :pswitch_2
    sget-object v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->ACTIVATED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    .line 468
    goto :goto_0

    .line 470
    :pswitch_3
    sget-object v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->SP_LOCKED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    .line 471
    goto :goto_0

    .line 462
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public acquireLock(I)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 2
    .param p1, "uid"    # I

    .prologue
    .line 262
    iget-object v0, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/gd/mobicore/pa/service/ProvisioningService;->acquireLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v0

    return-object v0
.end method

.method public doProvisioning(ILcom/gd/mobicore/pa/ifc/SPID;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 7
    .param p1, "uid"    # I
    .param p2, "spid"    # Lcom/gd/mobicore/pa/ifc/SPID;

    .prologue
    const/4 v6, 0x0

    .line 270
    const-string v3, "RootPA-J"

    const-string v4, ">>RootPAServiceIfc.Stub.doProvisioning"

    invoke-static {v3, v4}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    const/4 v1, 0x0

    .line 273
    .local v1, "ret":I
    if-nez p2, :cond_0

    .line 274
    const-string v3, "RootPA-J"

    const-string v4, "RootPAServiceIfc.Stub.doProvisioning spid==null"

    invoke-static {v3, v4}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    new-instance v3, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/16 v4, 0x8

    invoke-direct {v3, v4}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    .line 305
    :goto_0
    return-object v3

    .line 281
    :cond_0
    const/high16 v3, 0x11110000

    add-int/2addr v3, p1

    new-instance v4, Ljava/util/Random;

    invoke-direct {v4}, Ljava/util/Random;-><init>()V

    invoke-virtual {v4}, Ljava/util/Random;->nextInt()I

    move-result v4

    add-int v2, v3, v4

    .line 283
    .local v2, "tmpSuid":I
    iget-object v3, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    invoke-virtual {v3, v2, v6}, Lcom/gd/mobicore/pa/service/ProvisioningService;->acquireLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v3

    if-nez v3, :cond_1

    .line 284
    new-instance v3, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto :goto_0

    .line 286
    :cond_1
    iget-object v3, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    iput v2, v3, Lcom/gd/mobicore/pa/service/ProvisioningService;->doProvisioningLockSuid_:I

    .line 289
    :try_start_0
    iget-object v3, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    invoke-virtual {v3}, Lcom/gd/mobicore/pa/service/ProvisioningService;->setupProxy()V

    .line 290
    invoke-direct {p0}, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v3

    invoke-virtual {p2}, Lcom/gd/mobicore/pa/ifc/SPID;->spid()I

    move-result v4

    iget-object v5, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    iget-object v5, v5, Lcom/gd/mobicore/pa/service/ProvisioningService;->se_:[B

    invoke-virtual {v3, p1, v4, v5}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->doProvisioning(II[B)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 296
    :goto_1
    const-string v3, "RootPA-J"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CommonPAWrapper()).doProvisioning returned "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    if-eqz v1, :cond_3

    .line 298
    iget-object v3, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    iget-object v4, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    iget v4, v4, Lcom/gd/mobicore/pa/service/ProvisioningService;->doProvisioningLockSuid_:I

    invoke-virtual {v3, v4, v6}, Lcom/gd/mobicore/pa/service/ProvisioningService;->releaseLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v3

    if-nez v3, :cond_2

    .line 299
    const-string v3, "RootPA-J"

    const-string v4, "releasing lock failed after doProvisioning returned an error"

    invoke-static {v3, v4}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    :cond_2
    iget-object v3, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    iput v6, v3, Lcom/gd/mobicore/pa/service/ProvisioningService;->doProvisioningLockSuid_:I

    .line 304
    :cond_3
    const-string v3, "RootPA-J"

    const-string v4, "<<RootPAServiceIfc.Stub.doProvisioning"

    invoke-static {v3, v4}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    new-instance v3, Lcom/gd/mobicore/pa/ifc/CommandResult;

    invoke-direct {v3, v1}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto :goto_0

    .line 291
    :catch_0
    move-exception v0

    .line 292
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "RootPA-J"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CommonPAWrapper()).doProvisioning failed "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    const/4 v1, 0x7

    goto :goto_1
.end method

.method public executeCmpCommands(ILjava/util/List;Ljava/util/List;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 5
    .param p1, "uid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/gd/mobicore/pa/ifc/CmpCommand;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/gd/mobicore/pa/ifc/CmpResponse;",
            ">;)",
            "Lcom/gd/mobicore/pa/ifc/CommandResult;"
        }
    .end annotation

    .prologue
    .line 83
    .local p2, "commands":Ljava/util/List;, "Ljava/util/List<Lcom/gd/mobicore/pa/ifc/CmpCommand;>;"
    .local p3, "responses":Ljava/util/List;, "Ljava/util/List<Lcom/gd/mobicore/pa/ifc/CmpResponse;>;"
    const-string v2, "RootPA-J"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ">>RootPAServiceIfc.Stub.executeCmpCommands "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 86
    :cond_0
    const-string v2, "RootPA-J"

    const-string v3, "RootPAServiceIfc.Stub.executeCmpCommands, illegal argument"

    invoke-static {v2, v3}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    new-instance v2, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/16 v3, 0x8

    invoke-direct {v2, v3}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    .line 103
    :goto_0
    return-object v2

    .line 90
    :cond_1
    iget-object v2, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    invoke-virtual {v2, p1}, Lcom/gd/mobicore/pa/service/ProvisioningService;->locked(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 91
    new-instance v2, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto :goto_0

    .line 94
    :cond_2
    const/4 v1, 0x0

    .line 96
    .local v1, "ret":I
    :try_start_0
    invoke-direct {p0}, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v2

    invoke-virtual {v2, p1, p2, p3}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->executeCmpCommands(ILjava/util/List;Ljava/util/List;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 102
    :goto_1
    const-string v2, "RootPA-J"

    const-string v3, "<<RootPAServiceIfc.Stub.executeCmpCommands"

    invoke-static {v2, v3}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    new-instance v2, Lcom/gd/mobicore/pa/ifc/CommandResult;

    invoke-direct {v2, v1}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto :goto_0

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "RootPA-J"

    const-string v3, "CommonPAWrapper().executeCmpCommands exception: "

    invoke-static {v2, v3, v0}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 99
    const/4 v1, 0x7

    goto :goto_1
.end method

.method public getDeviceId(Lcom/gd/mobicore/pa/ifc/SUID;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 8
    .param p1, "suid"    # Lcom/gd/mobicore/pa/ifc/SUID;

    .prologue
    const/4 v7, 0x0

    .line 230
    const-string v5, "RootPA-J"

    const-string v6, ">>RootPAServiceIfc.Stub.getDeviceId"

    invoke-static {v5, v6}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    new-instance v5, Ljava/util/Random;

    invoke-direct {v5}, Ljava/util/Random;-><init>()V

    invoke-virtual {v5}, Ljava/util/Random;->nextInt()I

    move-result v1

    .line 233
    .local v1, "internalUidForLock":I
    iget-object v5, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    invoke-virtual {v5, v1, v7}, Lcom/gd/mobicore/pa/service/ProvisioningService;->acquireLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v5

    invoke-virtual {v5}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v5

    if-nez v5, :cond_0

    .line 235
    new-instance v5, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/4 v6, 0x2

    invoke-direct {v5, v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    .line 258
    :goto_0
    return-object v5

    .line 238
    :cond_0
    const/4 v3, 0x0

    .line 239
    .local v3, "ret":I
    const/16 v5, 0x10

    new-array v4, v5, [B

    .line 241
    .local v4, "suidArray":[B
    :try_start_0
    invoke-direct {p0}, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->getSuid([B)I

    move-result v3

    .line 242
    invoke-virtual {p1, v4}, Lcom/gd/mobicore/pa/ifc/SUID;->setSuid([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 248
    :goto_1
    iget-object v5, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    invoke-virtual {v5, v1, v7}, Lcom/gd/mobicore/pa/service/ProvisioningService;->releaseLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v2

    .line 249
    .local v2, "res":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {v2}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v5

    if-nez v5, :cond_1

    .line 250
    const-string v5, "RootPA-J"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "releasing lock failed, res: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/gd/mobicore/pa/ifc/CommandResult;->result()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :cond_1
    const-string v5, "RootPA-J"

    const-string v6, "<<RootPAServiceIfc.Stub.getDeviceId"

    invoke-static {v5, v6}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    new-instance v5, Lcom/gd/mobicore/pa/ifc/CommandResult;

    invoke-direct {v5, v3}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto :goto_0

    .line 243
    .end local v2    # "res":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :catch_0
    move-exception v0

    .line 244
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "RootPA-J"

    const-string v6, "CommonPAWrapper().getSuid exception: "

    invoke-static {v5, v6, v0}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 245
    const/4 v3, 0x7

    goto :goto_1
.end method

.method public getSPContainerState(Lcom/gd/mobicore/pa/ifc/SPID;Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 10
    .param p1, "spid"    # Lcom/gd/mobicore/pa/ifc/SPID;
    .param p2, "state"    # Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;

    .prologue
    const/4 v9, 0x0

    .line 386
    const-string v6, "RootPA-J"

    const-string v7, ">>RootPAServiceIfc.Stub.getSPContainerState"

    invoke-static {v6, v7}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 389
    :cond_0
    new-instance v6, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/16 v7, 0x8

    invoke-direct {v6, v7}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    .line 432
    :goto_0
    return-object v6

    .line 392
    :cond_1
    new-instance v6, Ljava/util/Random;

    invoke-direct {v6}, Ljava/util/Random;-><init>()V

    invoke-virtual {v6}, Ljava/util/Random;->nextInt()I

    move-result v2

    .line 393
    .local v2, "internalUidForLock":I
    iget-object v6, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    invoke-virtual {v6, v2, v9}, Lcom/gd/mobicore/pa/service/ProvisioningService;->acquireLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v6

    invoke-virtual {v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v6

    if-nez v6, :cond_2

    .line 395
    new-instance v6, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/4 v7, 0x2

    invoke-direct {v6, v7}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto :goto_0

    .line 398
    :cond_2
    const/4 v4, 0x0

    .line 399
    .local v4, "ret":I
    const/4 v6, 0x1

    new-array v0, v6, [I

    .line 402
    .local v0, "containerState":[I
    :try_start_0
    invoke-direct {p0}, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v6

    invoke-virtual {p1}, Lcom/gd/mobicore/pa/ifc/SPID;->spid()I

    move-result v7

    invoke-virtual {v6, v7, v0}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->getSPContainerState(I[I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 408
    :goto_1
    const-string v6, "RootPA-J"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "RootPAServiceIfc.Stub.getSPContainerState received "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget v8, v0, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    if-nez v4, :cond_5

    .line 412
    aget v6, v0, v9

    invoke-direct {p0, v6}, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->mapSpContainerState(I)Lcom/gd/mobicore/pa/ifc/SPContainerState;

    move-result-object v5

    .line 413
    .local v5, "s":Lcom/gd/mobicore/pa/ifc/SPContainerState;
    invoke-virtual {p2, v5}, Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;->setEnumeratedValue(Ljava/lang/Enum;)V

    .line 414
    sget-object v6, Lcom/gd/mobicore/pa/ifc/SPContainerState;->UNDEFINED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    if-ne v5, v6, :cond_3

    .line 415
    const/4 v4, 0x7

    .line 422
    .end local v5    # "s":Lcom/gd/mobicore/pa/ifc/SPContainerState;
    :cond_3
    :goto_2
    iget-object v6, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    invoke-virtual {v6, v2, v9}, Lcom/gd/mobicore/pa/service/ProvisioningService;->releaseLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v3

    .line 423
    .local v3, "res":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {v3}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v6

    if-nez v6, :cond_4

    .line 424
    const-string v6, "RootPA-J"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "releasing lock failed, res: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/gd/mobicore/pa/ifc/CommandResult;->result()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    :cond_4
    const-string v6, "RootPA-J"

    const-string v7, "<<RootPAServiceIfc.Stub.getSPContainerState"

    invoke-static {v6, v7}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    new-instance v6, Lcom/gd/mobicore/pa/ifc/CommandResult;

    invoke-direct {v6, v4}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto/16 :goto_0

    .line 403
    .end local v3    # "res":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :catch_0
    move-exception v1

    .line 404
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "RootPA-J"

    const-string v7, "CommonPAWrapper().getSPContainerState exception: "

    invoke-static {v6, v7, v1}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 405
    const/4 v4, 0x7

    goto :goto_1

    .line 417
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_5
    const/16 v6, 0x30

    if-ne v4, v6, :cond_3

    .line 418
    sget-object v6, Lcom/gd/mobicore/pa/ifc/SPContainerState;->DOES_NOT_EXIST:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    invoke-virtual {p2, v6}, Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;->setEnumeratedValue(Ljava/lang/Enum;)V

    .line 419
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public getSPContainerStructure(Lcom/gd/mobicore/pa/ifc/SPID;Lcom/gd/mobicore/pa/ifc/SPContainerStructure;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 32
    .param p1, "spid"    # Lcom/gd/mobicore/pa/ifc/SPID;
    .param p2, "cs"    # Lcom/gd/mobicore/pa/ifc/SPContainerStructure;

    .prologue
    .line 309
    const-string v25, "RootPA-J"

    const-string v26, ">>RootPAServiceIfc.Stub.getSPContainerStructure"

    invoke-static/range {v25 .. v26}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 312
    :cond_0
    new-instance v25, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/16 v26, 0x8

    invoke-direct/range {v25 .. v26}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    .line 381
    :goto_0
    return-object v25

    .line 315
    :cond_1
    new-instance v25, Ljava/util/Random;

    invoke-direct/range {v25 .. v25}, Ljava/util/Random;-><init>()V

    invoke-virtual/range {v25 .. v25}, Ljava/util/Random;->nextInt()I

    move-result v13

    .line 316
    .local v13, "internalUidForLock":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v0, v13, v1}, Lcom/gd/mobicore/pa/service/ProvisioningService;->acquireLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v25

    if-nez v25, :cond_2

    .line 318
    new-instance v25, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/16 v26, 0x2

    invoke-direct/range {v25 .. v26}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto :goto_0

    .line 321
    :cond_2
    const/16 v20, 0x0

    .line 323
    .local v20, "ret":I
    const/4 v6, 0x0

    .line 324
    .local v6, "CONTAINER_STATE_IDX":I
    const/4 v9, 0x1

    .line 325
    .local v9, "NUMBER_OF_TLTS_IDX":I
    const/4 v8, 0x2

    .line 326
    .local v8, "NUMBER_OF_ELEMENTS":I
    const/16 v7, 0x10

    .line 327
    .local v7, "MAX_NUMBER_OF_TRUSTLETS":I
    const/16 v10, 0x10

    .line 329
    .local v10, "UUID_LENGTH":I
    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v14, v0, [I

    .line 330
    .local v14, "ints":[I
    const/16 v25, 0x10

    move/from16 v0, v25

    new-array v0, v0, [I

    move-object/from16 v22, v0

    .line 331
    .local v22, "trustletStates":[I
    const/16 v25, 0x10

    move/from16 v0, v25

    new-array v0, v0, [[B

    move-object/from16 v24, v0

    .line 334
    .local v24, "uuidArray":[[B
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v25

    invoke-virtual/range {p1 .. p1}, Lcom/gd/mobicore/pa/ifc/SPID;->spid()I

    move-result v26

    move-object/from16 v0, v25

    move/from16 v1, v26

    move-object/from16 v2, v24

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v14, v2, v3}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->getSPContainerStructure(I[I[[B[I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v20

    .line 340
    :goto_1
    if-nez v20, :cond_5

    .line 342
    const/16 v25, 0x0

    aget v25, v14, v25

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->mapSpContainerState(I)Lcom/gd/mobicore/pa/ifc/SPContainerState;

    move-result-object v21

    .line 343
    .local v21, "s":Lcom/gd/mobicore/pa/ifc/SPContainerState;
    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->setState(Lcom/gd/mobicore/pa/ifc/SPContainerState;)V

    .line 344
    sget-object v25, Lcom/gd/mobicore/pa/ifc/SPContainerState;->UNDEFINED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    if-ne v0, v1, :cond_3

    .line 345
    const/16 v20, 0x7

    .line 348
    :cond_3
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_2
    const/16 v25, 0x1

    aget v25, v14, v25

    move/from16 v0, v25

    if-ge v12, v0, :cond_6

    .line 349
    const/16 v25, 0x8

    move/from16 v0, v25

    new-array v0, v0, [B

    move-object/from16 v18, v0

    .line 350
    .local v18, "msBytes":[B
    aget-object v25, v24, v12

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x8

    move-object/from16 v0, v25

    move/from16 v1, v26

    move-object/from16 v2, v18

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 351
    new-instance v17, Ljava/math/BigInteger;

    invoke-direct/range {v17 .. v18}, Ljava/math/BigInteger;-><init>([B)V

    .line 353
    .local v17, "mostSignificant":Ljava/math/BigInteger;
    const/16 v25, 0x8

    move/from16 v0, v25

    new-array v0, v0, [B

    move-object/from16 v16, v0

    .line 354
    .local v16, "lsBytes":[B
    aget-object v25, v24, v12

    const/16 v26, 0x8

    const/16 v27, 0x0

    const/16 v28, 0x8

    move-object/from16 v0, v25

    move/from16 v1, v26

    move-object/from16 v2, v16

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 355
    new-instance v15, Ljava/math/BigInteger;

    invoke-direct/range {v15 .. v16}, Ljava/math/BigInteger;-><init>([B)V

    .line 357
    .local v15, "leastSignificant":Ljava/math/BigInteger;
    const-string v25, "RootPA-J"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "UUID: ls ms"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    aget v25, v22, v12

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->mapTltContainerState(I)Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    move-result-object v23

    .line 360
    .local v23, "ts":Lcom/gd/mobicore/pa/ifc/TrustletContainerState;
    sget-object v25, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->UNDEFINED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    if-ne v0, v1, :cond_4

    .line 361
    const/16 v20, 0x7

    .line 363
    :cond_4
    new-instance v25, Lcom/gd/mobicore/pa/ifc/TrustletContainer;

    new-instance v26, Ljava/util/UUID;

    invoke-virtual/range {v17 .. v17}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v28

    invoke-virtual {v15}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v30

    move-object/from16 v0, v26

    move-wide/from16 v1, v28

    move-wide/from16 v3, v30

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/util/UUID;-><init>(JJ)V

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/gd/mobicore/pa/ifc/TrustletContainer;-><init>(Ljava/util/UUID;Lcom/gd/mobicore/pa/ifc/TrustletContainerState;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->add(Lcom/gd/mobicore/pa/ifc/TrustletContainer;)V

    .line 348
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_2

    .line 335
    .end local v12    # "i":I
    .end local v15    # "leastSignificant":Ljava/math/BigInteger;
    .end local v16    # "lsBytes":[B
    .end local v17    # "mostSignificant":Ljava/math/BigInteger;
    .end local v18    # "msBytes":[B
    .end local v21    # "s":Lcom/gd/mobicore/pa/ifc/SPContainerState;
    .end local v23    # "ts":Lcom/gd/mobicore/pa/ifc/TrustletContainerState;
    :catch_0
    move-exception v11

    .line 336
    .local v11, "e":Ljava/lang/Exception;
    const-string v25, "RootPA-J"

    const-string v26, "CommonPAWrapper().getSPContainerStructure exception: "

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v1, v11}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 337
    const/16 v20, 0x7

    goto/16 :goto_1

    .line 366
    .end local v11    # "e":Ljava/lang/Exception;
    :cond_5
    const/16 v25, 0x30

    move/from16 v0, v20

    move/from16 v1, v25

    if-ne v0, v1, :cond_6

    .line 367
    sget-object v25, Lcom/gd/mobicore/pa/ifc/SPContainerState;->DOES_NOT_EXIST:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->setState(Lcom/gd/mobicore/pa/ifc/SPContainerState;)V

    .line 368
    const/16 v20, 0x0

    .line 371
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v0, v13, v1}, Lcom/gd/mobicore/pa/service/ProvisioningService;->releaseLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v19

    .line 372
    .local v19, "res":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual/range {v19 .. v19}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v25

    if-nez v25, :cond_7

    .line 373
    const-string v25, "RootPA-J"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "releasing lock failed, res: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v19 .. v19}, Lcom/gd/mobicore/pa/ifc/CommandResult;->result()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    :cond_7
    const-string v25, "RootPA-J"

    const-string v26, "<<RootPAServiceIfc.Stub.getSPContainerStructure"

    invoke-static/range {v25 .. v26}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    new-instance v25, Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-object/from16 v0, v25

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto/16 :goto_0
.end method

.method public getVersion(Lcom/gd/mobicore/pa/ifc/Version;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 12
    .param p1, "version"    # Lcom/gd/mobicore/pa/ifc/Version;

    .prologue
    const/4 v11, 0x0

    .line 182
    const-string v9, "RootPA-J"

    const-string v10, ">>RootPAServiceIfc.Stub.getVersion"

    invoke-static {v9, v10}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    new-instance v9, Ljava/util/Random;

    invoke-direct {v9}, Ljava/util/Random;-><init>()V

    invoke-virtual {v9}, Ljava/util/Random;->nextInt()I

    move-result v2

    .line 185
    .local v2, "internalUidForLock":I
    iget-object v9, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    invoke-virtual {v9, v2, v11}, Lcom/gd/mobicore/pa/service/ProvisioningService;->acquireLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v9

    invoke-virtual {v9}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v9

    if-nez v9, :cond_0

    .line 187
    new-instance v9, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/4 v10, 0x2

    invoke-direct {v9, v10}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    .line 225
    :goto_0
    return-object v9

    .line 190
    :cond_0
    const/4 v6, 0x0

    .line 191
    .local v6, "ret":I
    const/16 v9, 0x40

    new-array v4, v9, [B

    .line 192
    .local v4, "productId":[B
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 193
    .local v8, "versionBundle":Landroid/os/Bundle;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 194
    .local v3, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 197
    .local v7, "values":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :try_start_0
    invoke-direct {p0}, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v9

    invoke-virtual {v9, v4, v3, v7}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->getVersion([BLjava/util/List;Ljava/util/List;)I

    move-result v6

    .line 198
    if-nez v6, :cond_1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    invoke-interface {v7}, Ljava/util/List;->size()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    if-eq v9, v10, :cond_1

    .line 199
    const/4 v6, 0x7

    .line 206
    :cond_1
    :goto_1
    if-nez v6, :cond_3

    .line 207
    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v4}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Lcom/gd/mobicore/pa/ifc/Version;->setProductId(Ljava/lang/String;)V

    .line 209
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    if-ge v1, v9, :cond_2

    .line 210
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 209
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 201
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Ljava/lang/Exception;
    const-string v9, "RootPA-J"

    const-string v10, "CommonPAWrapper().getVersion exception: "

    invoke-static {v9, v10, v0}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 203
    const/4 v6, 0x7

    goto :goto_1

    .line 213
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "i":I
    :cond_2
    invoke-virtual {p1, v8}, Lcom/gd/mobicore/pa/ifc/Version;->setVersion(Landroid/os/Bundle;)V

    .line 216
    .end local v1    # "i":I
    :cond_3
    iget-object v9, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    invoke-virtual {v9, v2, v11}, Lcom/gd/mobicore/pa/service/ProvisioningService;->releaseLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v5

    .line 217
    .local v5, "res":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {v5}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v9

    if-nez v9, :cond_4

    .line 218
    const-string v9, "RootPA-J"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "releasing lock failed, res: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v5}, Lcom/gd/mobicore/pa/ifc/CommandResult;->result()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    :cond_4
    const-string v9, "RootPA-J"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<<RootPAServiceIfc.Stub.getVersion "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    new-instance v9, Lcom/gd/mobicore/pa/ifc/CommandResult;

    invoke-direct {v9, v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto/16 :goto_0
.end method

.method public isRootContainerRegistered(Lcom/gd/mobicore/pa/ifc/BooleanResult;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 8
    .param p1, "result"    # Lcom/gd/mobicore/pa/ifc/BooleanResult;

    .prologue
    const/4 v7, 0x0

    .line 107
    const-string v5, "RootPA-J"

    const-string v6, ">>RootPAServiceIfc.Stub.isRootContainerRegistered"

    invoke-static {v5, v6}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    if-nez p1, :cond_0

    .line 110
    const-string v5, "RootPA-J"

    const-string v6, "RootPAServiceIfc.Stub.isRootContainerRegistered result null"

    invoke-static {v5, v6}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    new-instance v5, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/16 v6, 0x8

    invoke-direct {v5, v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    .line 140
    :goto_0
    return-object v5

    .line 114
    :cond_0
    new-instance v5, Ljava/util/Random;

    invoke-direct {v5}, Ljava/util/Random;-><init>()V

    invoke-virtual {v5}, Ljava/util/Random;->nextInt()I

    move-result v1

    .line 116
    .local v1, "internalUidForLock":I
    iget-object v5, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    invoke-virtual {v5, v1, v7}, Lcom/gd/mobicore/pa/service/ProvisioningService;->acquireLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v5

    invoke-virtual {v5}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v5

    if-nez v5, :cond_1

    .line 118
    new-instance v5, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/4 v6, 0x2

    invoke-direct {v5, v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto :goto_0

    .line 121
    :cond_1
    const/4 v5, 0x1

    new-array v2, v5, [Z

    .line 122
    .local v2, "isRegistered":[Z
    const/4 v4, 0x0

    .line 124
    .local v4, "ret":I
    :try_start_0
    invoke-direct {p0}, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->isRootContainerRegistered([Z)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 129
    :goto_1
    aget-boolean v5, v2, v7

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/gd/mobicore/pa/ifc/BooleanResult;->setResult(Ljava/lang/Boolean;)V

    .line 131
    iget-object v5, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    invoke-virtual {v5, v1, v7}, Lcom/gd/mobicore/pa/service/ProvisioningService;->releaseLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v3

    .line 132
    .local v3, "res":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {v3}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v5

    if-nez v5, :cond_2

    .line 133
    const-string v5, "RootPA-J"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "releasing lock failed, res: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/gd/mobicore/pa/ifc/CommandResult;->result()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_2
    const-string v5, "RootPA-J"

    const-string v6, "<<RootPAServiceIfc.Stub.isRootContainerRegistered"

    invoke-static {v5, v6}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    new-instance v5, Lcom/gd/mobicore/pa/ifc/CommandResult;

    invoke-direct {v5, v4}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto :goto_0

    .line 125
    .end local v3    # "res":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "RootPA-J"

    const-string v6, "CommonPAWrapper().isRootContainerRegistered exception: "

    invoke-static {v5, v6, v0}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 127
    const/4 v4, 0x7

    goto :goto_1
.end method

.method public isSPContainerRegistered(Lcom/gd/mobicore/pa/ifc/SPID;Lcom/gd/mobicore/pa/ifc/BooleanResult;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 8
    .param p1, "spid"    # Lcom/gd/mobicore/pa/ifc/SPID;
    .param p2, "result"    # Lcom/gd/mobicore/pa/ifc/BooleanResult;

    .prologue
    const/4 v7, 0x0

    .line 144
    const-string v5, "RootPA-J"

    const-string v6, ">>RootPAServiceIfc.Stub.isSPContainerRegistered"

    invoke-static {v5, v6}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 147
    :cond_0
    const-string v5, "RootPA-J"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RootPAServiceIfc.Stub.isSPContainerRegistered spid "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " result "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    new-instance v5, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/16 v6, 0x8

    invoke-direct {v5, v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    .line 178
    :goto_0
    return-object v5

    .line 151
    :cond_1
    const/4 v5, 0x1

    new-array v2, v5, [Z

    .line 152
    .local v2, "isRegistered":[Z
    const/4 v4, 0x0

    .line 154
    .local v4, "ret":I
    new-instance v5, Ljava/util/Random;

    invoke-direct {v5}, Ljava/util/Random;-><init>()V

    invoke-virtual {v5}, Ljava/util/Random;->nextInt()I

    move-result v1

    .line 155
    .local v1, "internalUidForLock":I
    iget-object v5, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    invoke-virtual {v5, v1, v7}, Lcom/gd/mobicore/pa/service/ProvisioningService;->acquireLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v5

    invoke-virtual {v5}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v5

    if-nez v5, :cond_2

    .line 157
    new-instance v5, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/4 v6, 0x2

    invoke-direct {v5, v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto :goto_0

    .line 161
    :cond_2
    :try_start_0
    invoke-direct {p0}, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v5

    invoke-virtual {p1}, Lcom/gd/mobicore/pa/ifc/SPID;->spid()I

    move-result v6

    invoke-virtual {v5, v6, v2}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->isSpContainerRegistered(I[Z)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 166
    :goto_1
    aget-boolean v5, v2, v7

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {p2, v5}, Lcom/gd/mobicore/pa/ifc/BooleanResult;->setResult(Ljava/lang/Boolean;)V

    .line 168
    iget-object v5, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    invoke-virtual {v5, v1, v7}, Lcom/gd/mobicore/pa/service/ProvisioningService;->releaseLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v3

    .line 169
    .local v3, "res":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {v3}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v5

    if-nez v5, :cond_3

    .line 170
    const-string v5, "RootPA-J"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "releasing lock failed, res: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/gd/mobicore/pa/ifc/CommandResult;->result()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_3
    const-string v5, "RootPA-J"

    const-string v6, "<<RootPAServiceIfc.Stub.isSPContainerRegistered "

    invoke-static {v5, v6}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    new-instance v5, Lcom/gd/mobicore/pa/ifc/CommandResult;

    invoke-direct {v5, v4}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto :goto_0

    .line 162
    .end local v3    # "res":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :catch_0
    move-exception v0

    .line 163
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "RootPA-J"

    const-string v6, "CommonPAWrapper().isSpContainerRegistered exception: "

    invoke-static {v5, v6, v0}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 164
    const/4 v4, 0x7

    goto :goto_1
.end method

.method public releaseLock(I)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 2
    .param p1, "uid"    # I

    .prologue
    .line 266
    iget-object v0, p0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/ProvisioningService;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/gd/mobicore/pa/service/ProvisioningService;->releaseLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v0

    return-object v0
.end method

.method public storeTA(Lcom/gd/mobicore/pa/ifc/SPID;[B[B)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 4
    .param p1, "spid"    # Lcom/gd/mobicore/pa/ifc/SPID;
    .param p2, "uuid"    # [B
    .param p3, "taBinary"    # [B

    .prologue
    .line 436
    const-string v2, "RootPA-J"

    const-string v3, ">>RootPAServiceIfc.Stub.storeTA"

    invoke-static {v2, v3}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    array-length v2, p3

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/gd/mobicore/pa/ifc/SPID;->spid()I

    move-result v2

    if-nez v2, :cond_1

    .line 438
    :cond_0
    new-instance v2, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/16 v3, 0x8

    invoke-direct {v2, v3}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    .line 450
    :goto_0
    return-object v2

    .line 441
    :cond_1
    const/4 v1, 0x0

    .line 443
    .local v1, "ret":I
    :try_start_0
    invoke-direct {p0}, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v2

    invoke-virtual {p1}, Lcom/gd/mobicore/pa/ifc/SPID;->spid()I

    move-result v3

    invoke-virtual {v2, v3, p2, p3}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->storeTA(I[B[B)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 449
    :goto_1
    const-string v2, "RootPA-J"

    const-string v3, "<<RootPAServiceIfc.Stub.storeTA"

    invoke-static {v2, v3}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    new-instance v2, Lcom/gd/mobicore/pa/ifc/CommandResult;

    invoke-direct {v2, v1}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto :goto_0

    .line 444
    :catch_0
    move-exception v0

    .line 445
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "RootPA-J"

    const-string v3, "CommonPAWrapper().storeTA exception: "

    invoke-static {v2, v3, v0}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 446
    const/4 v1, 0x7

    goto :goto_1
.end method
