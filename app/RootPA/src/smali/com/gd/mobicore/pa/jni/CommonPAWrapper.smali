.class public Lcom/gd/mobicore/pa/jni/CommonPAWrapper;
.super Ljava/lang/Object;
.source "CommonPAWrapper.java"


# instance fields
.field private service_:Lcom/gd/mobicore/pa/service/BaseService;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 84
    const-string v1, "RootPA-J"

    const-string v2, "CommonPAWrapper.java: static"

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    :try_start_0
    const-string v1, "commonpawrapper"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    .local v0, "e":Ljava/lang/Throwable;
    :goto_0
    return-void

    .line 87
    .end local v0    # "e":Ljava/lang/Throwable;
    :catch_0
    move-exception v0

    .line 88
    .restart local v0    # "e":Ljava/lang/Throwable;
    const-string v1, "RootPA-J"

    const-string v2, "loading common wrapper failed, trying to load test"

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    const-string v1, "commonpawrapper_test"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Lcom/gd/mobicore/pa/service/BaseService;)V
    .locals 2
    .param p1, "service"    # Lcom/gd/mobicore/pa/service/BaseService;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->service_:Lcom/gd/mobicore/pa/service/BaseService;

    .line 56
    const-string v0, "RootPA-J"

    const-string v1, "CommonPAWrapper.java: constructor"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    return-void
.end method


# virtual methods
.method public native closeSession()V
.end method

.method public native doProvisioning(II[B)I
.end method

.method public native executeCmpCommands(ILjava/util/List;Ljava/util/List;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/gd/mobicore/pa/ifc/CmpCommand;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/gd/mobicore/pa/ifc/CmpResponse;",
            ">;)I"
        }
    .end annotation
.end method

.method public getFilesDirPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 96
    const-string v0, "RootPA-J"

    const-string v1, "CommonPAWrapper.getFilesDirPath"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iget-object v0, p0, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->service_:Lcom/gd/mobicore/pa/service/BaseService;

    invoke-virtual {v0}, Lcom/gd/mobicore/pa/service/BaseService;->getFilesDirPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public native getSPContainerState(I[I)I
.end method

.method public native getSPContainerStructure(I[I[[B[I)I
.end method

.method public native getSuid([B)I
.end method

.method public getSystemInfo()[Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 115
    const-string v2, "RootPA-J"

    const-string v3, ">>CommonPAWrapper.getSystemInfo"

    invoke-static {v2, v3}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    const/4 v2, 0x7

    new-array v0, v2, [Ljava/lang/String;

    .line 117
    .local v0, "response":[Ljava/lang/String;
    iget-object v2, p0, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->service_:Lcom/gd/mobicore/pa/service/BaseService;

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Lcom/gd/mobicore/pa/service/BaseService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 118
    .local v1, "telephonyManager":Landroid/telephony/TelephonyManager;
    if-eqz v1, :cond_0

    .line 119
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    .line 120
    const/4 v2, 0x1

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperatorName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 122
    :cond_0
    const/4 v2, 0x2

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    aput-object v3, v0, v2

    .line 123
    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    aput-object v2, v0, v6

    .line 124
    const/4 v2, 0x4

    sget-object v3, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    aput-object v3, v0, v2

    .line 125
    const/4 v2, 0x5

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v3, v0, v2

    .line 126
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    .line 128
    const-string v2, "RootPA-J"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<<CommonPAWrapper.getSystemInfo "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    return-object v0
.end method

.method public native getVersion([BLjava/util/List;Ljava/util/List;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation
.end method

.method public native installTrustlet(I[BI[BI[BIII[B)I
.end method

.method public native isRootContainerRegistered([Z)I
.end method

.method public native isSpContainerRegistered(I[Z)I
.end method

.method public native openSession()I
.end method

.method public provisioningStateCallback(II)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "ret"    # I

    .prologue
    .line 101
    const-string v0, "RootPA-J"

    const-string v1, "CommonPAWrapper.provisioningStateCallback"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v0, p0, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->service_:Lcom/gd/mobicore/pa/service/BaseService;

    invoke-virtual {v0, p1, p2}, Lcom/gd/mobicore/pa/service/BaseService;->provisioningStateCallback(II)V

    .line 103
    return-void
.end method

.method public native setEnvironmentVariable([B[B)V
.end method

.method public native storeTA(I[B[B)I
.end method

.method public trustletInstallCallback([B)V
    .locals 3
    .param p1, "trustlet"    # [B

    .prologue
    .line 134
    const-string v0, "RootPA-J"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ">>CommonPAWrapper.trustletInstallCallback "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    iget-object v0, p0, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->service_:Lcom/gd/mobicore/pa/service/BaseService;

    invoke-virtual {v0, p1}, Lcom/gd/mobicore/pa/service/BaseService;->trustletInstallCallback([B)V

    .line 136
    const-string v0, "RootPA-J"

    const-string v1, "<<CommonPAWrapper.trustletInstallCallback"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    return-void
.end method

.method public native unregisterRootContainer([B)I
.end method
