.class public Lcom/gd/mobicore/pa/ifc/SPID;
.super Ljava/lang/Object;
.source "SPID.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/gd/mobicore/pa/ifc/SPID;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private spid_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/gd/mobicore/pa/ifc/SPID$1;

    invoke-direct {v0}, Lcom/gd/mobicore/pa/ifc/SPID$1;-><init>()V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/SPID;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "spid"    # I

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput p1, p0, Lcom/gd/mobicore/pa/ifc/SPID;->spid_:I

    .line 45
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/gd/mobicore/pa/ifc/SPID;->spid_:I

    .line 68
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/gd/mobicore/pa/ifc/SPID$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/gd/mobicore/pa/ifc/SPID$1;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/gd/mobicore/pa/ifc/SPID;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    if-ne p0, p1, :cond_1

    .line 85
    :cond_0
    :goto_0
    return v1

    .line 83
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 84
    check-cast v0, Lcom/gd/mobicore/pa/ifc/SPID;

    .line 85
    .local v0, "spid":Lcom/gd/mobicore/pa/ifc/SPID;
    iget v3, p0, Lcom/gd/mobicore/pa/ifc/SPID;->spid_:I

    iget v4, v0, Lcom/gd/mobicore/pa/ifc/SPID;->spid_:I

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/gd/mobicore/pa/ifc/SPID;->spid_:I

    return v0
.end method

.method public spid()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/gd/mobicore/pa/ifc/SPID;->spid_:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 77
    iget v0, p0, Lcom/gd/mobicore/pa/ifc/SPID;->spid_:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78
    return-void
.end method
