.class public Lcom/gd/mobicore/pa/ifc/CmpResponse;
.super Lcom/gd/mobicore/pa/ifc/CmpMsg;
.source "CmpResponse.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/gd/mobicore/pa/ifc/CmpResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    new-instance v0, Lcom/gd/mobicore/pa/ifc/CmpResponse$1;

    invoke-direct {v0}, Lcom/gd/mobicore/pa/ifc/CmpResponse$1;-><init>()V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/CmpResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/gd/mobicore/pa/ifc/CmpMsg;-><init>()V

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/gd/mobicore/pa/ifc/CmpMsg;-><init>(Landroid/os/Parcel;)V

    .line 107
    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .param p1, "content"    # [B

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/gd/mobicore/pa/ifc/CmpMsg;-><init>([B)V

    .line 100
    return-void
.end method


# virtual methods
.method public responseId()I
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/gd/mobicore/pa/ifc/CmpResponse;->msgId()I

    move-result v0

    return v0
.end method

.method public returnCode()I
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/gd/mobicore/pa/ifc/CmpResponse;->getInt(I)I

    move-result v0

    return v0
.end method

.method public setLength(I)V
    .locals 4
    .param p1, "length"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 77
    invoke-virtual {p0}, Lcom/gd/mobicore/pa/ifc/CmpResponse;->responseId()I

    move-result v2

    const/4 v3, 0x5

    invoke-static {v3}, Lcom/gd/mobicore/pa/ifc/CmpResponse;->responseIdToCommandId(I)I

    move-result v3

    if-eq v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/gd/mobicore/pa/ifc/CmpResponse;->responseId()I

    move-result v2

    const/4 v3, 0x4

    invoke-static {v3}, Lcom/gd/mobicore/pa/ifc/CmpResponse;->responseIdToCommandId(I)I

    move-result v3

    if-eq v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/gd/mobicore/pa/ifc/CmpResponse;->responseId()I

    move-result v2

    const/16 v3, 0x1b

    invoke-static {v3}, Lcom/gd/mobicore/pa/ifc/CmpResponse;->responseIdToCommandId(I)I

    move-result v3

    if-eq v2, v3, :cond_1

    move v2, v0

    :goto_0
    if-lez p1, :cond_2

    :goto_1
    and-int/2addr v0, v2

    if-eqz v0, :cond_0

    .line 82
    const/16 v0, 0x8

    invoke-virtual {p0, v0, p1}, Lcom/gd/mobicore/pa/ifc/CmpResponse;->setInt(II)V

    .line 84
    :cond_0
    return-void

    :cond_1
    move v2, v1

    .line 77
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public setResponseId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/gd/mobicore/pa/ifc/CmpResponse;->setMsgId(I)V

    .line 58
    return-void
.end method

.method public setReturnCode(I)V
    .locals 1
    .param p1, "ret"    # I

    .prologue
    .line 71
    const/4 v0, 0x4

    invoke-virtual {p0, v0, p1}, Lcom/gd/mobicore/pa/ifc/CmpResponse;->setInt(II)V

    .line 72
    return-void
.end method
