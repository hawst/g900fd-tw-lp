.class Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub$Proxy;
.super Ljava/lang/Object;
.source "RootPADeveloperIfc.java"

# interfaces
.implements Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 0
    .param p1, "remote"    # Landroid/os/IBinder;

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    iput-object p1, p0, Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    .line 120
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    const-string v0, "com.gd.mobicore.pa.ifc.RootPADeveloperIfc"

    return-object v0
.end method

.method public installTrustlet(I[B[BI[BIII)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 6
    .param p1, "spid"    # I
    .param p2, "uuid"    # [B
    .param p3, "taBinary"    # [B
    .param p4, "minTltVersion"    # I
    .param p5, "tltPukHash"    # [B
    .param p6, "memoryType"    # I
    .param p7, "numberOfInstances"    # I
    .param p8, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 222
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 223
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 226
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.gd.mobicore.pa.ifc.RootPADeveloperIfc"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 227
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 228
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 229
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 230
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 231
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 232
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V

    .line 233
    invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeInt(I)V

    .line 234
    invoke-virtual {v0, p8}, Landroid/os/Parcel;->writeInt(I)V

    .line 235
    iget-object v3, p0, Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 236
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 237
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 238
    sget-object v3, Lcom/gd/mobicore/pa/ifc/CommandResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/gd/mobicore/pa/ifc/CommandResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    .local v2, "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 246
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 248
    return-object v2

    .line 241
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    goto :goto_0

    .line 245
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 246
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3
.end method

.method public installTrustletOrKey(I[B[B[BI[B)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 6
    .param p1, "spid"    # I
    .param p2, "uuid"    # [B
    .param p3, "taBinary"    # [B
    .param p4, "key"    # [B
    .param p5, "minTltVersion"    # I
    .param p6, "tltPukHash"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 164
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 165
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 168
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.gd.mobicore.pa.ifc.RootPADeveloperIfc"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 169
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 170
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 171
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 172
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 173
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    .line 174
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 175
    iget-object v3, p0, Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 176
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 177
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 178
    sget-object v3, Lcom/gd/mobicore/pa/ifc/CommandResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/gd/mobicore/pa/ifc/CommandResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    .local v2, "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 186
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 188
    return-object v2

    .line 181
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    goto :goto_0

    .line 185
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 186
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3
.end method
