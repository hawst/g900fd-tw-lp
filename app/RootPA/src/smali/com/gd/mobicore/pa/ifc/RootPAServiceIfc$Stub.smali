.class public abstract Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;
.super Landroid/os/Binder;
.source "RootPAServiceIfc.java"

# interfaces
.implements Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 22
    const-string v0, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {p0, p0, v0}, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 30
    if-nez p0, :cond_0

    .line 31
    const/4 v0, 0x0

    .line 37
    :goto_0
    return-object v0

    .line 33
    :cond_0
    const-string v1, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 34
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc;

    if-eqz v1, :cond_1

    .line 35
    check-cast v0, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc;

    goto :goto_0

    .line 37
    :cond_1
    new-instance v0, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 41
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 9
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    .line 45
    sparse-switch p1, :sswitch_data_0

    .line 313
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 49
    :sswitch_0
    const-string v7, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 54
    :sswitch_1
    const-string v7, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 56
    new-instance v0, Lcom/gd/mobicore/pa/ifc/BooleanResult;

    invoke-direct {v0}, Lcom/gd/mobicore/pa/ifc/BooleanResult;-><init>()V

    .line 57
    .local v0, "_arg0":Lcom/gd/mobicore/pa/ifc/BooleanResult;
    invoke-virtual {p0, v0}, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;->isRootContainerRegistered(Lcom/gd/mobicore/pa/ifc/BooleanResult;)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v5

    .line 58
    .local v5, "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 59
    if-eqz v5, :cond_0

    .line 60
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 61
    invoke-virtual {v5, p3, v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 66
    :goto_1
    if-eqz v0, :cond_1

    .line 67
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 68
    invoke-virtual {v0, p3, v6}, Lcom/gd/mobicore/pa/ifc/BooleanResult;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 64
    :cond_0
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    .line 71
    :cond_1
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 77
    .end local v0    # "_arg0":Lcom/gd/mobicore/pa/ifc/BooleanResult;
    .end local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :sswitch_2
    const-string v7, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 79
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_2

    .line 80
    sget-object v7, Lcom/gd/mobicore/pa/ifc/SPID;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/gd/mobicore/pa/ifc/SPID;

    .line 86
    .local v0, "_arg0":Lcom/gd/mobicore/pa/ifc/SPID;
    :goto_2
    new-instance v1, Lcom/gd/mobicore/pa/ifc/BooleanResult;

    invoke-direct {v1}, Lcom/gd/mobicore/pa/ifc/BooleanResult;-><init>()V

    .line 87
    .local v1, "_arg1":Lcom/gd/mobicore/pa/ifc/BooleanResult;
    invoke-virtual {p0, v0, v1}, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;->isSPContainerRegistered(Lcom/gd/mobicore/pa/ifc/SPID;Lcom/gd/mobicore/pa/ifc/BooleanResult;)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v5

    .line 88
    .restart local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 89
    if-eqz v5, :cond_3

    .line 90
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 91
    invoke-virtual {v5, p3, v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 96
    :goto_3
    if-eqz v1, :cond_4

    .line 97
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 98
    invoke-virtual {v1, p3, v6}, Lcom/gd/mobicore/pa/ifc/BooleanResult;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 83
    .end local v0    # "_arg0":Lcom/gd/mobicore/pa/ifc/SPID;
    .end local v1    # "_arg1":Lcom/gd/mobicore/pa/ifc/BooleanResult;
    .end local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/gd/mobicore/pa/ifc/SPID;
    goto :goto_2

    .line 94
    .restart local v1    # "_arg1":Lcom/gd/mobicore/pa/ifc/BooleanResult;
    .restart local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_3
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3

    .line 101
    :cond_4
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 107
    .end local v0    # "_arg0":Lcom/gd/mobicore/pa/ifc/SPID;
    .end local v1    # "_arg1":Lcom/gd/mobicore/pa/ifc/BooleanResult;
    .end local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :sswitch_3
    const-string v7, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 109
    new-instance v0, Lcom/gd/mobicore/pa/ifc/Version;

    invoke-direct {v0}, Lcom/gd/mobicore/pa/ifc/Version;-><init>()V

    .line 110
    .local v0, "_arg0":Lcom/gd/mobicore/pa/ifc/Version;
    invoke-virtual {p0, v0}, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;->getVersion(Lcom/gd/mobicore/pa/ifc/Version;)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v5

    .line 111
    .restart local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 112
    if-eqz v5, :cond_5

    .line 113
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 114
    invoke-virtual {v5, p3, v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 119
    :goto_4
    if-eqz v0, :cond_6

    .line 120
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 121
    invoke-virtual {v0, p3, v6}, Lcom/gd/mobicore/pa/ifc/Version;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 117
    :cond_5
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_4

    .line 124
    :cond_6
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 130
    .end local v0    # "_arg0":Lcom/gd/mobicore/pa/ifc/Version;
    .end local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :sswitch_4
    const-string v7, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 132
    new-instance v0, Lcom/gd/mobicore/pa/ifc/SUID;

    invoke-direct {v0}, Lcom/gd/mobicore/pa/ifc/SUID;-><init>()V

    .line 133
    .local v0, "_arg0":Lcom/gd/mobicore/pa/ifc/SUID;
    invoke-virtual {p0, v0}, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;->getDeviceId(Lcom/gd/mobicore/pa/ifc/SUID;)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v5

    .line 134
    .restart local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 135
    if-eqz v5, :cond_7

    .line 136
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 137
    invoke-virtual {v5, p3, v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 142
    :goto_5
    if-eqz v0, :cond_8

    .line 143
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 144
    invoke-virtual {v0, p3, v6}, Lcom/gd/mobicore/pa/ifc/SUID;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 140
    :cond_7
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_5

    .line 147
    :cond_8
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 153
    .end local v0    # "_arg0":Lcom/gd/mobicore/pa/ifc/SUID;
    .end local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :sswitch_5
    const-string v7, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 155
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 156
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;->acquireLock(I)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v5

    .line 157
    .restart local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 158
    if-eqz v5, :cond_9

    .line 159
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 160
    invoke-virtual {v5, p3, v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 163
    :cond_9
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 169
    .end local v0    # "_arg0":I
    .end local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :sswitch_6
    const-string v7, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 171
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 172
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;->releaseLock(I)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v5

    .line 173
    .restart local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 174
    if-eqz v5, :cond_a

    .line 175
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 176
    invoke-virtual {v5, p3, v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 179
    :cond_a
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 185
    .end local v0    # "_arg0":I
    .end local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :sswitch_7
    const-string v7, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 187
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 189
    .restart local v0    # "_arg0":I
    sget-object v7, Lcom/gd/mobicore/pa/ifc/CmpCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v2

    .line 191
    .local v2, "_arg1":Ljava/util/List;, "Ljava/util/List<Lcom/gd/mobicore/pa/ifc/CmpCommand;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 192
    .local v4, "_arg2":Ljava/util/List;, "Ljava/util/List<Lcom/gd/mobicore/pa/ifc/CmpResponse;>;"
    invoke-virtual {p0, v0, v2, v4}, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;->executeCmpCommands(ILjava/util/List;Ljava/util/List;)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v5

    .line 193
    .restart local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 194
    if-eqz v5, :cond_b

    .line 195
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 196
    invoke-virtual {v5, p3, v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 201
    :goto_6
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 199
    :cond_b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_6

    .line 206
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Lcom/gd/mobicore/pa/ifc/CmpCommand;>;"
    .end local v4    # "_arg2":Ljava/util/List;, "Ljava/util/List<Lcom/gd/mobicore/pa/ifc/CmpResponse;>;"
    .end local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :sswitch_8
    const-string v7, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 208
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 210
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_c

    .line 211
    sget-object v7, Lcom/gd/mobicore/pa/ifc/SPID;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/gd/mobicore/pa/ifc/SPID;

    .line 216
    .local v1, "_arg1":Lcom/gd/mobicore/pa/ifc/SPID;
    :goto_7
    invoke-virtual {p0, v0, v1}, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;->doProvisioning(ILcom/gd/mobicore/pa/ifc/SPID;)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v5

    .line 217
    .restart local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 218
    if-eqz v5, :cond_d

    .line 219
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 220
    invoke-virtual {v5, p3, v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 214
    .end local v1    # "_arg1":Lcom/gd/mobicore/pa/ifc/SPID;
    .end local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_c
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/gd/mobicore/pa/ifc/SPID;
    goto :goto_7

    .line 223
    .restart local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 229
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/gd/mobicore/pa/ifc/SPID;
    .end local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :sswitch_9
    const-string v7, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 231
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_e

    .line 232
    sget-object v7, Lcom/gd/mobicore/pa/ifc/SPID;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/gd/mobicore/pa/ifc/SPID;

    .line 238
    .local v0, "_arg0":Lcom/gd/mobicore/pa/ifc/SPID;
    :goto_8
    new-instance v1, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;

    invoke-direct {v1}, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;-><init>()V

    .line 239
    .local v1, "_arg1":Lcom/gd/mobicore/pa/ifc/SPContainerStructure;
    invoke-virtual {p0, v0, v1}, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;->getSPContainerStructure(Lcom/gd/mobicore/pa/ifc/SPID;Lcom/gd/mobicore/pa/ifc/SPContainerStructure;)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v5

    .line 240
    .restart local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 241
    if-eqz v5, :cond_f

    .line 242
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 243
    invoke-virtual {v5, p3, v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 248
    :goto_9
    if-eqz v1, :cond_10

    .line 249
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 250
    invoke-virtual {v1, p3, v6}, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 235
    .end local v0    # "_arg0":Lcom/gd/mobicore/pa/ifc/SPID;
    .end local v1    # "_arg1":Lcom/gd/mobicore/pa/ifc/SPContainerStructure;
    .end local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/gd/mobicore/pa/ifc/SPID;
    goto :goto_8

    .line 246
    .restart local v1    # "_arg1":Lcom/gd/mobicore/pa/ifc/SPContainerStructure;
    .restart local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_f
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_9

    .line 253
    :cond_10
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 259
    .end local v0    # "_arg0":Lcom/gd/mobicore/pa/ifc/SPID;
    .end local v1    # "_arg1":Lcom/gd/mobicore/pa/ifc/SPContainerStructure;
    .end local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :sswitch_a
    const-string v7, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 261
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_11

    .line 262
    sget-object v7, Lcom/gd/mobicore/pa/ifc/SPID;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/gd/mobicore/pa/ifc/SPID;

    .line 268
    .restart local v0    # "_arg0":Lcom/gd/mobicore/pa/ifc/SPID;
    :goto_a
    new-instance v1, Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;

    invoke-direct {v1}, Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;-><init>()V

    .line 269
    .local v1, "_arg1":Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;
    invoke-virtual {p0, v0, v1}, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;->getSPContainerState(Lcom/gd/mobicore/pa/ifc/SPID;Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v5

    .line 270
    .restart local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 271
    if-eqz v5, :cond_12

    .line 272
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 273
    invoke-virtual {v5, p3, v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 278
    :goto_b
    if-eqz v1, :cond_13

    .line 279
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 280
    invoke-virtual {v1, p3, v6}, Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 265
    .end local v0    # "_arg0":Lcom/gd/mobicore/pa/ifc/SPID;
    .end local v1    # "_arg1":Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;
    .end local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_11
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/gd/mobicore/pa/ifc/SPID;
    goto :goto_a

    .line 276
    .restart local v1    # "_arg1":Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;
    .restart local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_12
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_b

    .line 283
    :cond_13
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 289
    .end local v0    # "_arg0":Lcom/gd/mobicore/pa/ifc/SPID;
    .end local v1    # "_arg1":Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;
    .end local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :sswitch_b
    const-string v7, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 291
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_14

    .line 292
    sget-object v7, Lcom/gd/mobicore/pa/ifc/SPID;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/gd/mobicore/pa/ifc/SPID;

    .line 298
    .restart local v0    # "_arg0":Lcom/gd/mobicore/pa/ifc/SPID;
    :goto_c
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 300
    .local v1, "_arg1":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 301
    .local v3, "_arg2":[B
    invoke-virtual {p0, v0, v1, v3}, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;->storeTA(Lcom/gd/mobicore/pa/ifc/SPID;[B[B)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v5

    .line 302
    .restart local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 303
    if-eqz v5, :cond_15

    .line 304
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 305
    invoke-virtual {v5, p3, v6}, Lcom/gd/mobicore/pa/ifc/CommandResult;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 295
    .end local v0    # "_arg0":Lcom/gd/mobicore/pa/ifc/SPID;
    .end local v1    # "_arg1":[B
    .end local v3    # "_arg2":[B
    .end local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_14
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/gd/mobicore/pa/ifc/SPID;
    goto :goto_c

    .line 308
    .restart local v1    # "_arg1":[B
    .restart local v3    # "_arg2":[B
    .restart local v5    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_15
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 45
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
