.class public Lcom/gd/mobicore/pa/ifc/RootPAProvisioningIntents;
.super Ljava/lang/Object;
.source "RootPAProvisioningIntents.java"


# static fields
.field public static final AUTHENTICATING_ROOT:I = 0x190

.field public static final AUTHENTICATING_SOC:I = 0xc8

.field public static final CONNECTING_SERVICE_ENABLER:I = 0x64

.field public static final CREATING_ROOT_CONTAINER:I = 0x12c

.field public static final CREATING_SP_CONTAINER:I = 0x1f4

.field public static final DEVELOPER_SERVICE:Ljava/lang/String; = "com.gd.mobicore.pa.service.DEVELOPER_SERVICE"

.field public static final ERROR:Ljava/lang/String; = "com.gd.mobicore.pa.ifc.Error"

.field public static final FINISHED_PROVISIONING:I = 0x3e8

.field public static final FINISHED_ROOT_PROVISIONING:Ljava/lang/String; = "com.gd.mobicore.pa.service.PROVISIONING_FINISHED"

.field public static final INSTALL_TRUSTLET:Ljava/lang/String; = "com.gd.mobicore.pa.service.INSTALL_TRUSTLET"

.field public static final OEM_SERVICE:Ljava/lang/String; = "com.gd.mobicore.pa.service.OEM_SERVICE"

.field public static final PROVISIONING_ERROR:Ljava/lang/String; = "com.gd.mobicore.pa.service.PROVISIONING_ERROR"

.field public static final PROVISIONING_PROGRESS_UPDATE:Ljava/lang/String; = "com.gd.mobicore.pa.service.PROVISIONING_PROGRESS_UPDATE"

.field public static final PROVISIONING_SERVICE:Ljava/lang/String; = "com.gd.mobicore.pa.service.PROVISIONING_SERVICE"

.field public static final STATE:Ljava/lang/String; = "com.gd.mobicore.pa.ifc.State"

.field public static final TRUSTLET:Ljava/lang/String; = "com.gd.mobicore.pa.ifc.Trustlet"

.field public static final UNREGISTERING_ROOT_CONTAINER:I = 0xbb8


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
