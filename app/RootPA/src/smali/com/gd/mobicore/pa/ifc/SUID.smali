.class public Lcom/gd/mobicore/pa/ifc/SUID;
.super Ljava/lang/Object;
.source "SUID.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/gd/mobicore/pa/ifc/SUID;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private suid_:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/gd/mobicore/pa/ifc/SUID$1;

    invoke-direct {v0}, Lcom/gd/mobicore/pa/ifc/SUID$1;-><init>()V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/SUID;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    invoke-virtual {p0, p1}, Lcom/gd/mobicore/pa/ifc/SUID;->readFromParcel(Landroid/os/Parcel;)V

    .line 77
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/gd/mobicore/pa/ifc/SUID$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/gd/mobicore/pa/ifc/SUID$1;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/gd/mobicore/pa/ifc/SUID;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .param p1, "suid"    # [B

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-virtual {p0, p1}, Lcom/gd/mobicore/pa/ifc/SUID;->setSuid([B)V

    .line 49
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/16 v1, 0x10

    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->dataAvail()I

    move-result v0

    if-lt v0, v1, :cond_0

    .line 81
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/gd/mobicore/pa/ifc/SUID;->suid_:[B

    .line 82
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/SUID;->suid_:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 84
    :cond_0
    return-void
.end method

.method public setSuid([B)V
    .locals 2
    .param p1, "suid"    # [B

    .prologue
    .line 52
    if-nez p1, :cond_0

    .line 53
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot set null SUID."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_0
    array-length v0, p1

    const/16 v1, 0x10

    if-eq v0, v1, :cond_1

    .line 55
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad length given, SUID has to be 16 bytes in length"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_1
    invoke-virtual {p1}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/gd/mobicore/pa/ifc/SUID;->suid_:[B

    .line 58
    return-void
.end method

.method public suid()[B
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/SUID;->suid_:[B

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 93
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/SUID;->suid_:[B

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/SUID;->suid_:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 96
    :cond_0
    return-void
.end method
