.class public abstract Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub;
.super Landroid/os/Binder;
.source "RootPADeveloperIfc.java"

# interfaces
.implements Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 25
    const-string v0, "com.gd.mobicore.pa.ifc.RootPADeveloperIfc"

    invoke-virtual {p0, p0, v0}, Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 26
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 33
    if-nez p0, :cond_0

    .line 34
    const/4 v0, 0x0

    .line 40
    :goto_0
    return-object v0

    .line 36
    :cond_0
    const-string v1, "com.gd.mobicore.pa.ifc.RootPADeveloperIfc"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 37
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc;

    if-eqz v1, :cond_1

    .line 38
    check-cast v0, Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc;

    goto :goto_0

    .line 40
    :cond_1
    new-instance v0, Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 44
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 10
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 48
    sparse-switch p1, :sswitch_data_0

    .line 112
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 52
    :sswitch_0
    const-string v0, "com.gd.mobicore.pa.ifc.RootPADeveloperIfc"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 53
    const/4 v0, 0x1

    goto :goto_0

    .line 57
    :sswitch_1
    const-string v0, "com.gd.mobicore.pa.ifc.RootPADeveloperIfc"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 59
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 61
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 63
    .local v2, "_arg1":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 65
    .local v3, "_arg2":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v4

    .line 67
    .local v4, "_arg3":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 69
    .local v5, "_arg4":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v6

    .local v6, "_arg5":[B
    move-object v0, p0

    .line 70
    invoke-virtual/range {v0 .. v6}, Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub;->installTrustletOrKey(I[B[B[BI[B)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v9

    .line 71
    .local v9, "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 72
    if-eqz v9, :cond_0

    .line 73
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 74
    const/4 v0, 0x1

    invoke-virtual {v9, p3, v0}, Lcom/gd/mobicore/pa/ifc/CommandResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 79
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 77
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    .line 83
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":[B
    .end local v3    # "_arg2":[B
    .end local v4    # "_arg3":[B
    .end local v5    # "_arg4":I
    .end local v6    # "_arg5":[B
    .end local v9    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :sswitch_2
    const-string v0, "com.gd.mobicore.pa.ifc.RootPADeveloperIfc"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 87
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 89
    .restart local v2    # "_arg1":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 91
    .restart local v3    # "_arg2":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 93
    .local v4, "_arg3":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v5

    .line 95
    .local v5, "_arg4":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 97
    .local v6, "_arg5":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 99
    .local v7, "_arg6":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .local v8, "_arg7":I
    move-object v0, p0

    .line 100
    invoke-virtual/range {v0 .. v8}, Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub;->installTrustlet(I[B[BI[BIII)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v9

    .line 101
    .restart local v9    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 102
    if-eqz v9, :cond_1

    .line 103
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    const/4 v0, 0x1

    invoke-virtual {v9, p3, v0}, Lcom/gd/mobicore/pa/ifc/CommandResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 109
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 107
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2

    .line 48
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
