.class Lcom/gd/mobicore/pa/ifc/RootPAOemIfc$Stub$Proxy;
.super Ljava/lang/Object;
.source "RootPAOemIfc.java"

# interfaces
.implements Lcom/gd/mobicore/pa/ifc/RootPAOemIfc;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/gd/mobicore/pa/ifc/RootPAOemIfc$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 0
    .param p1, "remote"    # Landroid/os/IBinder;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/gd/mobicore/pa/ifc/RootPAOemIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    .line 74
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/RootPAOemIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const-string v0, "com.gd.mobicore.pa.ifc.RootPAOemIfc"

    return-object v0
.end method

.method public unregisterRootContainer()Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 98
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 99
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 102
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.gd.mobicore.pa.ifc.RootPAOemIfc"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 103
    iget-object v3, p0, Lcom/gd/mobicore/pa/ifc/RootPAOemIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 104
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 105
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 106
    sget-object v3, Lcom/gd/mobicore/pa/ifc/CommandResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/gd/mobicore/pa/ifc/CommandResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    .local v2, "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 114
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 116
    return-object v2

    .line 109
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    goto :goto_0

    .line 113
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 114
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3
.end method
