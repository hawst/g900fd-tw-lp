.class public abstract Lcom/gd/mobicore/pa/ifc/CmpMsg;
.super Ljava/lang/Object;
.source "CmpMsg.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final INT_LENGTH:I = 0x4

.field public static final MC_CMP_CMD_AUTHENTICATE:I = 0x0

.field public static final MC_CMP_CMD_AUTHENTICATE_TERMINATE:I = 0x1c

.field public static final MC_CMP_CMD_BEGIN_ROOT_AUTHENTICATION:I = 0x1

.field public static final MC_CMP_CMD_BEGIN_SOC_AUTHENTICATION:I = 0x2

.field public static final MC_CMP_CMD_BEGIN_SP_AUTHENTICATION:I = 0x3

.field public static final MC_CMP_CMD_GENERATE_AUTH_TOKEN:I = 0x4

.field public static final MC_CMP_CMD_GET_SUID:I = 0x1b

.field public static final MC_CMP_CMD_GET_VERSION:I = 0x5

.field public static final MC_CMP_CMD_ROOT_CONT_LOCK_BY_ROOT:I = 0x7

.field public static final MC_CMP_CMD_ROOT_CONT_REGISTER_ACTIVATE:I = 0x9

.field public static final MC_CMP_CMD_ROOT_CONT_UNLOCK_BY_ROOT:I = 0xa

.field public static final MC_CMP_CMD_ROOT_CONT_UNREGISTER:I = 0xb

.field public static final MC_CMP_CMD_SP_CONT_ACTIVATE:I = 0xc

.field public static final MC_CMP_CMD_SP_CONT_LOCK_BY_ROOT:I = 0xd

.field public static final MC_CMP_CMD_SP_CONT_LOCK_BY_SP:I = 0xe

.field public static final MC_CMP_CMD_SP_CONT_REGISTER:I = 0xf

.field public static final MC_CMP_CMD_SP_CONT_REGISTER_ACTIVATE:I = 0x10

.field public static final MC_CMP_CMD_SP_CONT_UNLOCK_BY_ROOT:I = 0x11

.field public static final MC_CMP_CMD_SP_CONT_UNLOCK_BY_SP:I = 0x12

.field public static final MC_CMP_CMD_SP_CONT_UNREGISTER:I = 0x13

.field public static final MC_CMP_CMD_TLT_CONT_ACTIVATE:I = 0x14

.field public static final MC_CMP_CMD_TLT_CONT_LOCK_BY_SP:I = 0x15

.field public static final MC_CMP_CMD_TLT_CONT_PERSONALIZE:I = 0x16

.field public static final MC_CMP_CMD_TLT_CONT_REGISTER:I = 0x17

.field public static final MC_CMP_CMD_TLT_CONT_REGISTER_ACTIVATE:I = 0x18

.field public static final MC_CMP_CMD_TLT_CONT_UNLOCK_BY_SP:I = 0x19

.field public static final MC_CMP_CMD_TLT_CONT_UNREGISTER:I = 0x1a

.field public static final MSG_ID_IDX:I = 0x0

.field public static final RSP_ID_MASK:I = -0x80000000


# instance fields
.field private content_:[B

.field ignoreError_:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->ignoreError_:Z

    .line 112
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->ignoreError_:Z

    .line 251
    invoke-virtual {p0, p1}, Lcom/gd/mobicore/pa/ifc/CmpMsg;->readFromParcel(Landroid/os/Parcel;)V

    .line 252
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "content"    # [B

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->ignoreError_:Z

    .line 104
    invoke-virtual {p0, p1}, Lcom/gd/mobicore/pa/ifc/CmpMsg;->setContent([B)V

    .line 105
    return-void
.end method

.method public static final commandIdToResponseId(I)I
    .locals 1
    .param p0, "commandId"    # I

    .prologue
    .line 85
    const/high16 v0, -0x80000000

    or-int/2addr v0, p0

    return v0
.end method

.method private createEmptyContentIfNeeded(II)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "size"    # I

    .prologue
    const/4 v3, 0x0

    .line 155
    iget-object v1, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    if-nez v1, :cond_1

    .line 156
    new-array v1, p2, [B

    iput-object v1, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    .line 157
    iget-object v1, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    array-length v1, v1

    invoke-virtual {p0, v1}, Lcom/gd/mobicore/pa/ifc/CmpMsg;->setLength(I)V

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    iget-object v1, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    array-length v1, v1

    add-int v2, p1, p2

    if-ge v1, v2, :cond_0

    .line 159
    add-int v1, p1, p2

    new-array v0, v1, [B

    .line 160
    .local v0, "newarray":[B
    iget-object v1, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    iget-object v2, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 161
    iput-object v0, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    .line 162
    iget-object v1, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    array-length v1, v1

    invoke-virtual {p0, v1}, Lcom/gd/mobicore/pa/ifc/CmpMsg;->setLength(I)V

    goto :goto_0
.end method

.method public static final responseIdToCommandId(I)I
    .locals 1
    .param p0, "responseId"    # I

    .prologue
    .line 94
    const/high16 v0, -0x80000000

    xor-int/2addr v0, p0

    return v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x0

    return v0
.end method

.method public getByteArray(II)[B
    .locals 3
    .param p1, "index"    # I
    .param p2, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ArrayIndexOutOfBoundsException;
        }
    .end annotation

    .prologue
    .line 218
    new-array v0, p2, [B

    .line 219
    .local v0, "newarray":[B
    iget-object v1, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    const/4 v2, 0x0

    invoke-static {v1, p1, v0, v2, p2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 220
    return-object v0
.end method

.method public getInt(I)I
    .locals 3
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ArrayIndexOutOfBoundsException;
        }
    .end annotation

    .prologue
    .line 203
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    aget-byte v0, v0, p1

    and-int/lit16 v0, v0, 0xff

    iget-object v1, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    add-int/lit8 v2, p1, 0x1

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    add-int/lit8 v2, p1, 0x2

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    add-int/lit8 v2, p1, 0x3

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    add-int/2addr v0, v1

    return v0
.end method

.method public getString(II)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I
    .param p2, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ArrayIndexOutOfBoundsException;
        }
    .end annotation

    .prologue
    .line 233
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    invoke-direct {v0, v1, p1, p2}, Ljava/lang/String;-><init>([BII)V

    return-object v0
.end method

.method protected msgId()I
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/gd/mobicore/pa/ifc/CmpMsg;->getInt(I)I

    move-result v0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 265
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    .line 266
    return-void
.end method

.method public setByteArray(I[B)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "addBytes"    # [B

    .prologue
    .line 188
    array-length v0, p2

    invoke-direct {p0, p1, v0}, Lcom/gd/mobicore/pa/ifc/CmpMsg;->createEmptyContentIfNeeded(II)V

    .line 189
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    array-length v2, p2

    invoke-static {p2, v0, v1, p1, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 190
    return-void
.end method

.method public setContent([B)V
    .locals 1
    .param p1, "content"    # [B

    .prologue
    .line 128
    iput-object p1, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    .line 129
    array-length v0, p1

    invoke-virtual {p0, v0}, Lcom/gd/mobicore/pa/ifc/CmpMsg;->setLength(I)V

    .line 130
    return-void
.end method

.method public setInt(II)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "value"    # I

    .prologue
    .line 173
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lcom/gd/mobicore/pa/ifc/CmpMsg;->createEmptyContentIfNeeded(II)V

    .line 174
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    add-int/lit8 v1, p1, 0x0

    and-int/lit16 v2, p2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 175
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    add-int/lit8 v1, p1, 0x1

    shr-int/lit8 v2, p2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 176
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    add-int/lit8 v1, p1, 0x2

    shr-int/lit8 v2, p2, 0x10

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 177
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    add-int/lit8 v1, p1, 0x3

    shr-int/lit8 v2, p2, 0x18

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 178
    return-void
.end method

.method public abstract setLength(I)V
.end method

.method protected setMsgId(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 148
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/gd/mobicore/pa/ifc/CmpMsg;->setInt(II)V

    .line 149
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 119
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    array-length v0, v0

    goto :goto_0
.end method

.method public toByteArray()[B
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 261
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/CmpMsg;->content_:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 262
    return-void
.end method
