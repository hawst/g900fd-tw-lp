.class public abstract Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;
.super Ljava/lang/Object;
.source "AbstractEnumParcel.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Enum",
        "<TE;>;>",
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# instance fields
.field private enumeratedValue:Ljava/lang/Enum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    .local p0, "this":Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;, "Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel<TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/lang/Enum;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "this":Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;, "Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel<TE;>;"
    .local p1, "enumeratedValue":Ljava/lang/Enum;, "TE;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;->enumeratedValue:Ljava/lang/Enum;

    .line 48
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 60
    .local p0, "this":Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;, "Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel<TE;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public getEnumeratedValue()Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "this":Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;, "Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel<TE;>;"
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;->enumeratedValue:Ljava/lang/Enum;

    return-object v0
.end method

.method public setEnumeratedValue(Ljava/lang/Enum;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "this":Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;, "Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel<TE;>;"
    .local p1, "enumeratedValue":Ljava/lang/Enum;, "TE;"
    iput-object p1, p0, Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;->enumeratedValue:Ljava/lang/Enum;

    .line 56
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 72
    .local p0, "this":Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;, "Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel<TE;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;->enumeratedValue:Ljava/lang/Enum;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 65
    .local p0, "this":Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;, "Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel<TE;>;"
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;->enumeratedValue:Ljava/lang/Enum;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;->enumeratedValue:Ljava/lang/Enum;

    invoke-virtual {v0}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 68
    :cond_0
    return-void
.end method
