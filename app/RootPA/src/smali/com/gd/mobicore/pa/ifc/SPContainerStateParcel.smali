.class public Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;
.super Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;
.source "SPContainerStateParcel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel",
        "<",
        "Lcom/gd/mobicore/pa/ifc/SPContainerState;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel$1;

    invoke-direct {v0}, Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel$1;-><init>()V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;-><init>()V

    return-void
.end method


# virtual methods
.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 48
    invoke-static {v0}, Lcom/gd/mobicore/pa/ifc/SPContainerState;->valueOf(Ljava/lang/String;)Lcom/gd/mobicore/pa/ifc/SPContainerState;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;->setEnumeratedValue(Ljava/lang/Enum;)V

    .line 49
    :cond_0
    return-void
.end method
