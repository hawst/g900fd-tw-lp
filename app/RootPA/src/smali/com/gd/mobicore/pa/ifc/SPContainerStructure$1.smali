.class final Lcom/gd/mobicore/pa/ifc/SPContainerStructure$1;
.super Ljava/lang/Object;
.source "SPContainerStructure.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/gd/mobicore/pa/ifc/SPContainerStructure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/gd/mobicore/pa/ifc/SPContainerStructure;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/gd/mobicore/pa/ifc/SPContainerStructure;
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 91
    new-instance v0, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;

    invoke-direct {v0}, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;-><init>()V

    .line 92
    .local v0, "cs":Lcom/gd/mobicore/pa/ifc/SPContainerStructure;
    invoke-virtual {v0, p1}, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->readFromParcel(Landroid/os/Parcel;)V

    .line 93
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 88
    invoke-virtual {p0, p1}, Lcom/gd/mobicore/pa/ifc/SPContainerStructure$1;->createFromParcel(Landroid/os/Parcel;)Lcom/gd/mobicore/pa/ifc/SPContainerStructure;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/gd/mobicore/pa/ifc/SPContainerStructure;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 98
    new-array v0, p1, [Lcom/gd/mobicore/pa/ifc/SPContainerStructure;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 88
    invoke-virtual {p0, p1}, Lcom/gd/mobicore/pa/ifc/SPContainerStructure$1;->newArray(I)[Lcom/gd/mobicore/pa/ifc/SPContainerStructure;

    move-result-object v0

    return-object v0
.end method
