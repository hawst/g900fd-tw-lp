.class public Lcom/gd/mobicore/pa/service/ProvisioningService;
.super Lcom/gd/mobicore/pa/service/BaseService;
.source "ProvisioningService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;
    }
.end annotation


# instance fields
.field private final mBinder:Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/gd/mobicore/pa/service/BaseService;-><init>()V

    .line 68
    new-instance v0, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;

    invoke-direct {v0, p0}, Lcom/gd/mobicore/pa/service/ProvisioningService$ServiceIfc;-><init>(Lcom/gd/mobicore/pa/service/ProvisioningService;)V

    iput-object v0, p0, Lcom/gd/mobicore/pa/service/ProvisioningService;->mBinder:Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;

    .line 71
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 530
    :try_start_0
    const-string v1, "SE"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    iput-object v1, p0, Lcom/gd/mobicore/pa/service/ProvisioningService;->se_:[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 536
    :goto_0
    :try_start_1
    const-string v1, "LOG"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Lcom/gd/mobicore/pa/service/Log;->setLoggingLevel(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 540
    :goto_1
    const-string v1, "RootPA-J"

    const-string v2, "ProvisioningService binding, IfcVersion: 1.1"

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    iget-object v1, p0, Lcom/gd/mobicore/pa/service/ProvisioningService;->se_:[B

    if-eqz v1, :cond_0

    const-string v1, "RootPA-J"

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/gd/mobicore/pa/service/ProvisioningService;->se_:[B

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    :cond_0
    iget-object v1, p0, Lcom/gd/mobicore/pa/service/ProvisioningService;->mBinder:Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;

    return-object v1

    .line 531
    :catch_0
    move-exception v0

    .line 532
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "RootPA-J"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ProvisioningService something wrong in the given ip "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 537
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 538
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "RootPA-J"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ProvisioningService something wrong in the given logging level "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 512
    const-string v0, "RootPA-J"

    const-string v1, "Hello, ProvisioningService onCreate"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    invoke-super {p0}, Lcom/gd/mobicore/pa/service/BaseService;->onCreate()V

    .line 514
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 523
    invoke-super {p0}, Lcom/gd/mobicore/pa/service/BaseService;->onDestroy()V

    .line 524
    const-string v0, "RootPA-J"

    const-string v1, "ProvisioningService being destroyed"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    return-void
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 518
    const-string v0, "RootPA-J"

    const-string v1, "ProvisioningService onLowMemory"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    invoke-super {p0}, Lcom/gd/mobicore/pa/service/BaseService;->onLowMemory()V

    .line 520
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "i"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startid"    # I

    .prologue
    .line 547
    const-string v0, "RootPA-J"

    const-string v1, "ProvisioningService starting"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    const/4 v0, 0x1

    return v0
.end method
