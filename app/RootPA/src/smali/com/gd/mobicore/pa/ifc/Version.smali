.class public Lcom/gd/mobicore/pa/ifc/Version;
.super Ljava/lang/Object;
.source "Version.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/gd/mobicore/pa/ifc/Version;",
            ">;"
        }
    .end annotation
.end field

.field public static final VERSION_FIELD_CMP:Ljava/lang/String; = "CMP"

.field public static final VERSION_FIELD_CONT:Ljava/lang/String; = "CONT"

.field public static final VERSION_FIELD_DRAPI:Ljava/lang/String; = "DRAPI"

.field public static final VERSION_FIELD_MCCONF:Ljava/lang/String; = "MCCONF"

.field public static final VERSION_FIELD_MCI:Ljava/lang/String; = "MCI"

.field public static final VERSION_FIELD_MCLF:Ljava/lang/String; = "MCLF"

.field public static final VERSION_FIELD_SO:Ljava/lang/String; = "SO"

.field public static final VERSION_FIELD_TAG:Ljava/lang/String; = "TAG"

.field public static final VERSION_FIELD_TAG1ALL:Ljava/lang/String; = "TAG1ALL"

.field public static final VERSION_FIELD_TLAPI:Ljava/lang/String; = "TLAPI"


# instance fields
.field private productId_:Ljava/lang/String;

.field private version_:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/gd/mobicore/pa/ifc/Version$1;

    invoke-direct {v0}, Lcom/gd/mobicore/pa/ifc/Version$1;-><init>()V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/Version;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    invoke-virtual {p0, p1}, Lcom/gd/mobicore/pa/ifc/Version;->readFromParcel(Landroid/os/Parcel;)V

    .line 97
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/gd/mobicore/pa/ifc/Version$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/gd/mobicore/pa/ifc/Version$1;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/gd/mobicore/pa/ifc/Version;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "productId"    # Ljava/lang/String;
    .param p2, "version"    # Landroid/os/Bundle;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-virtual {p0, p2}, Lcom/gd/mobicore/pa/ifc/Version;->setVersion(Landroid/os/Bundle;)V

    .line 64
    invoke-virtual {p0, p1}, Lcom/gd/mobicore/pa/ifc/Version;->setProductId(Ljava/lang/String;)V

    .line 65
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return v0
.end method

.method public productId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/Version;->productId_:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gd/mobicore/pa/ifc/Version;->productId_:Ljava/lang/String;

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/gd/mobicore/pa/ifc/Version;->version_:Landroid/os/Bundle;

    .line 102
    return-void
.end method

.method public setProductId(Ljava/lang/String;)V
    .locals 0
    .param p1, "productId"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/gd/mobicore/pa/ifc/Version;->productId_:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public setVersion(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "version"    # Landroid/os/Bundle;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/gd/mobicore/pa/ifc/Version;->version_:Landroid/os/Bundle;

    .line 81
    return-void
.end method

.method public version()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/Version;->version_:Landroid/os/Bundle;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/Version;->productId_:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/Version;->productId_:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/Version;->version_:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/Version;->version_:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 117
    :cond_1
    return-void
.end method
