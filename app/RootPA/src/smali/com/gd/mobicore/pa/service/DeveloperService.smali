.class public Lcom/gd/mobicore/pa/service/DeveloperService;
.super Lcom/gd/mobicore/pa/service/BaseService;
.source "DeveloperService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;
    }
.end annotation


# static fields
.field private static final DEFAULT_PUKHASH:[B


# instance fields
.field private final mBinder:Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const/16 v0, 0x20

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/gd/mobicore/pa/service/DeveloperService;->DEFAULT_PUKHASH:[B

    return-void

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/gd/mobicore/pa/service/BaseService;-><init>()V

    .line 51
    new-instance v0, Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;

    invoke-direct {v0, p0}, Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;-><init>(Lcom/gd/mobicore/pa/service/DeveloperService;)V

    iput-object v0, p0, Lcom/gd/mobicore/pa/service/DeveloperService;->mBinder:Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub;

    .line 61
    return-void
.end method

.method static synthetic access$000()[B
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/gd/mobicore/pa/service/DeveloperService;->DEFAULT_PUKHASH:[B

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 198
    :try_start_0
    const-string v1, "SE"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    iput-object v1, p0, Lcom/gd/mobicore/pa/service/DeveloperService;->se_:[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    :goto_0
    :try_start_1
    const-string v1, "LOG"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Lcom/gd/mobicore/pa/service/Log;->setLoggingLevel(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 208
    :goto_1
    const-string v1, "RootPA-J"

    const-string v2, "DeveloperService binding, IfcVersion: 1.1"

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-object v1, p0, Lcom/gd/mobicore/pa/service/DeveloperService;->se_:[B

    if-eqz v1, :cond_0

    const-string v1, "RootPA-J"

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/gd/mobicore/pa/service/DeveloperService;->se_:[B

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    :cond_0
    iget-object v1, p0, Lcom/gd/mobicore/pa/service/DeveloperService;->mBinder:Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub;

    return-object v1

    .line 199
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "RootPA-J"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DeveloperService something wrong in the given ip "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 205
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 206
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "RootPA-J"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DeveloperService something wrong in the given logging level "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 180
    const-string v0, "RootPA-J"

    const-string v1, "Hello, DeveloperService onCreate"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    invoke-super {p0}, Lcom/gd/mobicore/pa/service/BaseService;->onCreate()V

    .line 182
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 191
    invoke-super {p0}, Lcom/gd/mobicore/pa/service/BaseService;->onDestroy()V

    .line 192
    const-string v0, "RootPA-J"

    const-string v1, "DeveloperService being destroyed"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    return-void
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 186
    const-string v0, "RootPA-J"

    const-string v1, "DeveloperService onLowMemory"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    invoke-super {p0}, Lcom/gd/mobicore/pa/service/BaseService;->onLowMemory()V

    .line 188
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "i"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startid"    # I

    .prologue
    .line 216
    const-string v0, "RootPA-J"

    const-string v1, "DeveloperService starting"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    const/4 v0, 0x1

    return v0
.end method
