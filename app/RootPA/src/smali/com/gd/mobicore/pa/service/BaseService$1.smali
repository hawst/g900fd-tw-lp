.class Lcom/gd/mobicore/pa/service/BaseService$1;
.super Ljava/util/TimerTask;
.source "BaseService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/gd/mobicore/pa/service/BaseService;->acquireLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/gd/mobicore/pa/service/BaseService;


# direct methods
.method constructor <init>(Lcom/gd/mobicore/pa/service/BaseService;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/gd/mobicore/pa/service/BaseService$1;->this$0:Lcom/gd/mobicore/pa/service/BaseService;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 111
    const-string v0, "RootPA-J"

    const-string v1, "Timer expired, releasing lock"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    # getter for: Lcom/gd/mobicore/pa/service/BaseService;->lock_:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {}, Lcom/gd/mobicore/pa/service/BaseService;->access$000()Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 113
    iget-object v0, p0, Lcom/gd/mobicore/pa/service/BaseService$1;->this$0:Lcom/gd/mobicore/pa/service/BaseService;

    # getter for: Lcom/gd/mobicore/pa/service/BaseService;->sessionOpened_:Z
    invoke-static {v0}, Lcom/gd/mobicore/pa/service/BaseService;->access$100(Lcom/gd/mobicore/pa/service/BaseService;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 114
    const-string v0, "RootPA-J"

    const-string v1, "BaseService.Timer.run, closingSession"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v0, p0, Lcom/gd/mobicore/pa/service/BaseService$1;->this$0:Lcom/gd/mobicore/pa/service/BaseService;

    invoke-virtual {v0}, Lcom/gd/mobicore/pa/service/BaseService;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->closeSession()V

    .line 116
    iget-object v0, p0, Lcom/gd/mobicore/pa/service/BaseService$1;->this$0:Lcom/gd/mobicore/pa/service/BaseService;

    # setter for: Lcom/gd/mobicore/pa/service/BaseService;->sessionOpened_:Z
    invoke-static {v0, v2}, Lcom/gd/mobicore/pa/service/BaseService;->access$102(Lcom/gd/mobicore/pa/service/BaseService;Z)Z

    .line 118
    :cond_0
    return-void
.end method
