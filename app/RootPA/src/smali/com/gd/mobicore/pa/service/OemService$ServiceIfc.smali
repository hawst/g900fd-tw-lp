.class Lcom/gd/mobicore/pa/service/OemService$ServiceIfc;
.super Lcom/gd/mobicore/pa/ifc/RootPAOemIfc$Stub;
.source "OemService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/gd/mobicore/pa/service/OemService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceIfc"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/gd/mobicore/pa/service/OemService;


# direct methods
.method public constructor <init>(Lcom/gd/mobicore/pa/service/OemService;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/gd/mobicore/pa/service/OemService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/OemService;

    .line 58
    invoke-direct {p0}, Lcom/gd/mobicore/pa/ifc/RootPAOemIfc$Stub;-><init>()V

    .line 59
    return-void
.end method

.method private commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/gd/mobicore/pa/service/OemService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/OemService;

    invoke-virtual {v0}, Lcom/gd/mobicore/pa/service/OemService;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public unregisterRootContainer()Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 66
    const-string v4, "RootPA-J"

    const-string v5, ">>RootPAServiceIfc.Stub.unregisterRootContainer"

    invoke-static {v4, v5}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const/high16 v4, 0x33330000

    new-instance v5, Ljava/util/Random;

    invoke-direct {v5}, Ljava/util/Random;-><init>()V

    invoke-virtual {v5}, Ljava/util/Random;->nextInt()I

    move-result v5

    add-int v3, v4, v5

    .line 70
    .local v3, "tmpSuid":I
    iget-object v4, p0, Lcom/gd/mobicore/pa/service/OemService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/OemService;

    invoke-virtual {v4, v3, v6}, Lcom/gd/mobicore/pa/service/OemService;->acquireLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v4

    if-nez v4, :cond_0

    .line 71
    new-instance v4, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/4 v5, 0x2

    invoke-direct {v4, v5}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    .line 96
    :goto_0
    return-object v4

    .line 74
    :cond_0
    iget-object v4, p0, Lcom/gd/mobicore/pa/service/OemService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/OemService;

    iput v3, v4, Lcom/gd/mobicore/pa/service/OemService;->doProvisioningLockSuid_:I

    .line 75
    const-string v4, "RootPA-J"

    const-string v5, "RootPAServiceIfc.Stub.unregisterRootContainer calling JNI"

    invoke-static {v4, v5}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    const/4 v2, 0x0

    .line 80
    .local v2, "ret":I
    :try_start_0
    iget-object v4, p0, Lcom/gd/mobicore/pa/service/OemService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/OemService;

    invoke-virtual {v4}, Lcom/gd/mobicore/pa/service/OemService;->setupProxy()V

    .line 81
    invoke-direct {p0}, Lcom/gd/mobicore/pa/service/OemService$ServiceIfc;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v4

    iget-object v5, p0, Lcom/gd/mobicore/pa/service/OemService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/OemService;

    iget-object v5, v5, Lcom/gd/mobicore/pa/service/OemService;->se_:[B

    invoke-virtual {v4, v5}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->unregisterRootContainer([B)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 87
    :goto_1
    iget-object v4, p0, Lcom/gd/mobicore/pa/service/OemService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/OemService;

    iget-object v5, p0, Lcom/gd/mobicore/pa/service/OemService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/OemService;

    iget v5, v5, Lcom/gd/mobicore/pa/service/OemService;->doProvisioningLockSuid_:I

    invoke-virtual {v4, v5, v6}, Lcom/gd/mobicore/pa/service/OemService;->releaseLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v1

    .line 88
    .local v1, "res":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {v1}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v4

    if-nez v4, :cond_1

    .line 89
    const-string v4, "RootPA-J"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "releasing lock failed, res: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/gd/mobicore/pa/ifc/CommandResult;->result()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :cond_1
    const-string v4, "RootPA-J"

    const-string v5, "<<RootPAServiceIfc.Stub.unregisterRootContainer"

    invoke-static {v4, v5}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    new-instance v4, Lcom/gd/mobicore/pa/ifc/CommandResult;

    invoke-direct {v4, v2}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto :goto_0

    .line 82
    .end local v1    # "res":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :catch_0
    move-exception v0

    .line 83
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "RootPA-J"

    const-string v5, "CommonPAWrapper().unregisterRootContainer exception: "

    invoke-static {v4, v5, v0}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 84
    const/4 v2, 0x7

    goto :goto_1
.end method
