.class public Lcom/gd/mobicore/pa/ifc/SPContainerStructure;
.super Ljava/lang/Object;
.source "SPContainerStructure.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/gd/mobicore/pa/ifc/SPContainerStructure;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private state_:Lcom/gd/mobicore/pa/ifc/SPContainerState;

.field private final tcList_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/gd/mobicore/pa/ifc/TrustletContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    new-instance v0, Lcom/gd/mobicore/pa/ifc/SPContainerStructure$1;

    invoke-direct {v0}, Lcom/gd/mobicore/pa/ifc/SPContainerStructure$1;-><init>()V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 55
    const/4 v0, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;-><init>(Lcom/gd/mobicore/pa/ifc/SPContainerState;Ljava/util/List;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Lcom/gd/mobicore/pa/ifc/SPContainerState;Ljava/util/List;)V
    .locals 0
    .param p1, "state"    # Lcom/gd/mobicore/pa/ifc/SPContainerState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/gd/mobicore/pa/ifc/SPContainerState;",
            "Ljava/util/List",
            "<",
            "Lcom/gd/mobicore/pa/ifc/TrustletContainer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p2, "tcList":Ljava/util/List;, "Ljava/util/List<Lcom/gd/mobicore/pa/ifc/TrustletContainer;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->state_:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 51
    iput-object p2, p0, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->tcList_:Ljava/util/List;

    .line 52
    return-void
.end method


# virtual methods
.method public add(Lcom/gd/mobicore/pa/ifc/TrustletContainer;)V
    .locals 1
    .param p1, "tc"    # Lcom/gd/mobicore/pa/ifc/TrustletContainer;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->tcList_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "state":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 111
    invoke-static {v0}, Lcom/gd/mobicore/pa/ifc/SPContainerState;->valueOf(Ljava/lang/String;)Lcom/gd/mobicore/pa/ifc/SPContainerState;

    move-result-object v1

    iput-object v1, p0, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->state_:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 112
    :cond_0
    iget-object v1, p0, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->tcList_:Ljava/util/List;

    sget-object v2, Lcom/gd/mobicore/pa/ifc/TrustletContainer;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 113
    return-void
.end method

.method public setState(Lcom/gd/mobicore/pa/ifc/SPContainerState;)V
    .locals 0
    .param p1, "state"    # Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->state_:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 72
    return-void
.end method

.method public state()Lcom/gd/mobicore/pa/ifc/SPContainerState;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->state_:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    return-object v0
.end method

.method public tcList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/gd/mobicore/pa/ifc/TrustletContainer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->tcList_:Ljava/util/List;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SPContainerStructure{state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->state_:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tcList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->tcList_:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 81
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->state_:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    if-nez v0, :cond_0

    .line 82
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 85
    :goto_0
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->tcList_:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 86
    return-void

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->state_:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    invoke-virtual {v0}, Lcom/gd/mobicore/pa/ifc/SPContainerState;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method
