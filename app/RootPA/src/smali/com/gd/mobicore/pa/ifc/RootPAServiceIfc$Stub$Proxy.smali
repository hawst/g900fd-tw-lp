.class Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub$Proxy;
.super Ljava/lang/Object;
.source "RootPAServiceIfc.java"

# interfaces
.implements Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 0
    .param p1, "remote"    # Landroid/os/IBinder;

    .prologue
    .line 319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 320
    iput-object p1, p0, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    .line 321
    return-void
.end method


# virtual methods
.method public acquireLock(I)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 6
    .param p1, "uid"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 482
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 483
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 486
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 487
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 488
    iget-object v3, p0, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x5

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 489
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 490
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 491
    sget-object v3, Lcom/gd/mobicore/pa/ifc/CommandResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/gd/mobicore/pa/ifc/CommandResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 498
    .local v2, "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 499
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 501
    return-object v2

    .line 494
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    goto :goto_0

    .line 498
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 499
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3
.end method

.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    return-object v0
.end method

.method public doProvisioning(ILcom/gd/mobicore/pa/ifc/SPID;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 6
    .param p1, "uid"    # I
    .param p2, "spid"    # Lcom/gd/mobicore/pa/ifc/SPID;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 613
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 614
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 617
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 618
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 619
    if-eqz p2, :cond_0

    .line 620
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 621
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/gd/mobicore/pa/ifc/SPID;->writeToParcel(Landroid/os/Parcel;I)V

    .line 626
    :goto_0
    iget-object v3, p0, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/16 v4, 0x8

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 627
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 628
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    .line 629
    sget-object v3, Lcom/gd/mobicore/pa/ifc/CommandResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/gd/mobicore/pa/ifc/CommandResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 636
    .local v2, "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 637
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 639
    return-object v2

    .line 624
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_0
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 636
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 637
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 632
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    goto :goto_1
.end method

.method public executeCmpCommands(ILjava/util/List;Ljava/util/List;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 6
    .param p1, "uid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/gd/mobicore/pa/ifc/CmpCommand;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/gd/mobicore/pa/ifc/CmpResponse;",
            ">;)",
            "Lcom/gd/mobicore/pa/ifc/CommandResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 549
    .local p2, "commands":Ljava/util/List;, "Ljava/util/List<Lcom/gd/mobicore/pa/ifc/CmpCommand;>;"
    .local p3, "responses":Ljava/util/List;, "Ljava/util/List<Lcom/gd/mobicore/pa/ifc/CmpResponse;>;"
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 550
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 553
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 554
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 555
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 556
    iget-object v3, p0, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x7

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 557
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 558
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 559
    sget-object v3, Lcom/gd/mobicore/pa/ifc/CommandResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/gd/mobicore/pa/ifc/CommandResult;

    .line 564
    .local v2, "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :goto_0
    sget-object v3, Lcom/gd/mobicore/pa/ifc/CmpResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {v1, p3, v3}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 567
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 568
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 570
    return-object v2

    .line 562
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    goto :goto_0

    .line 567
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 568
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3
.end method

.method public getDeviceId(Lcom/gd/mobicore/pa/ifc/SUID;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 6
    .param p1, "suid"    # Lcom/gd/mobicore/pa/ifc/SUID;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 447
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 448
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 451
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 452
    iget-object v3, p0, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 453
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 454
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    .line 455
    sget-object v3, Lcom/gd/mobicore/pa/ifc/CommandResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/gd/mobicore/pa/ifc/CommandResult;

    .line 460
    .local v2, "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 461
    invoke-virtual {p1, v1}, Lcom/gd/mobicore/pa/ifc/SUID;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 465
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 466
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 468
    return-object v2

    .line 458
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    goto :goto_0

    .line 465
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 466
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 328
    const-string v0, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    return-object v0
.end method

.method public getSPContainerState(Lcom/gd/mobicore/pa/ifc/SPID;Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 6
    .param p1, "spid"    # Lcom/gd/mobicore/pa/ifc/SPID;
    .param p2, "state"    # Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 693
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 694
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 697
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 698
    if-eqz p1, :cond_1

    .line 699
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 700
    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Lcom/gd/mobicore/pa/ifc/SPID;->writeToParcel(Landroid/os/Parcel;I)V

    .line 705
    :goto_0
    iget-object v3, p0, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/16 v4, 0xa

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 706
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 707
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_2

    .line 708
    sget-object v3, Lcom/gd/mobicore/pa/ifc/CommandResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/gd/mobicore/pa/ifc/CommandResult;

    .line 713
    .local v2, "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 714
    invoke-virtual {p2, v1}, Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 718
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 719
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 721
    return-object v2

    .line 703
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 718
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 719
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 711
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    goto :goto_1
.end method

.method public getSPContainerStructure(Lcom/gd/mobicore/pa/ifc/SPID;Lcom/gd/mobicore/pa/ifc/SPContainerStructure;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 6
    .param p1, "spid"    # Lcom/gd/mobicore/pa/ifc/SPID;
    .param p2, "cs"    # Lcom/gd/mobicore/pa/ifc/SPContainerStructure;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 652
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 653
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 656
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 657
    if-eqz p1, :cond_1

    .line 658
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 659
    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Lcom/gd/mobicore/pa/ifc/SPID;->writeToParcel(Landroid/os/Parcel;I)V

    .line 664
    :goto_0
    iget-object v3, p0, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/16 v4, 0x9

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 665
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 666
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_2

    .line 667
    sget-object v3, Lcom/gd/mobicore/pa/ifc/CommandResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/gd/mobicore/pa/ifc/CommandResult;

    .line 672
    .local v2, "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 673
    invoke-virtual {p2, v1}, Lcom/gd/mobicore/pa/ifc/SPContainerStructure;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 677
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 678
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 680
    return-object v2

    .line 662
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 677
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 678
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 670
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    goto :goto_1
.end method

.method public getVersion(Lcom/gd/mobicore/pa/ifc/Version;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 6
    .param p1, "version"    # Lcom/gd/mobicore/pa/ifc/Version;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 414
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 415
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 418
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 419
    iget-object v3, p0, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 420
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 421
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    .line 422
    sget-object v3, Lcom/gd/mobicore/pa/ifc/CommandResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/gd/mobicore/pa/ifc/CommandResult;

    .line 427
    .local v2, "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 428
    invoke-virtual {p1, v1}, Lcom/gd/mobicore/pa/ifc/Version;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 432
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 433
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 435
    return-object v2

    .line 425
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    goto :goto_0

    .line 432
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 433
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3
.end method

.method public isRootContainerRegistered(Lcom/gd/mobicore/pa/ifc/BooleanResult;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 6
    .param p1, "result"    # Lcom/gd/mobicore/pa/ifc/BooleanResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 340
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 341
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 344
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 345
    iget-object v3, p0, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 346
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 347
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    .line 348
    sget-object v3, Lcom/gd/mobicore/pa/ifc/CommandResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/gd/mobicore/pa/ifc/CommandResult;

    .line 353
    .local v2, "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 354
    invoke-virtual {p1, v1}, Lcom/gd/mobicore/pa/ifc/BooleanResult;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 358
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 359
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 361
    return-object v2

    .line 351
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    goto :goto_0

    .line 358
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 359
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3
.end method

.method public isSPContainerRegistered(Lcom/gd/mobicore/pa/ifc/SPID;Lcom/gd/mobicore/pa/ifc/BooleanResult;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 6
    .param p1, "spid"    # Lcom/gd/mobicore/pa/ifc/SPID;
    .param p2, "result"    # Lcom/gd/mobicore/pa/ifc/BooleanResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 374
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 375
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 378
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 379
    if-eqz p1, :cond_1

    .line 380
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 381
    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Lcom/gd/mobicore/pa/ifc/SPID;->writeToParcel(Landroid/os/Parcel;I)V

    .line 386
    :goto_0
    iget-object v3, p0, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 387
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 388
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_2

    .line 389
    sget-object v3, Lcom/gd/mobicore/pa/ifc/CommandResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/gd/mobicore/pa/ifc/CommandResult;

    .line 394
    .local v2, "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 395
    invoke-virtual {p2, v1}, Lcom/gd/mobicore/pa/ifc/BooleanResult;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 399
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 400
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 402
    return-object v2

    .line 384
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 399
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 400
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 392
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    goto :goto_1
.end method

.method public releaseLock(I)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 6
    .param p1, "uid"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 511
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 512
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 515
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 516
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 517
    iget-object v3, p0, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 518
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 519
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 520
    sget-object v3, Lcom/gd/mobicore/pa/ifc/CommandResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/gd/mobicore/pa/ifc/CommandResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 527
    .local v2, "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 528
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 530
    return-object v2

    .line 523
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    goto :goto_0

    .line 527
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 528
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3
.end method

.method public storeTA(Lcom/gd/mobicore/pa/ifc/SPID;[B[B)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 6
    .param p1, "spid"    # Lcom/gd/mobicore/pa/ifc/SPID;
    .param p2, "uuid"    # [B
    .param p3, "taBinary"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 733
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 734
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 737
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.gd.mobicore.pa.ifc.RootPAServiceIfc"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 738
    if-eqz p1, :cond_0

    .line 739
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 740
    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Lcom/gd/mobicore/pa/ifc/SPID;->writeToParcel(Landroid/os/Parcel;I)V

    .line 745
    :goto_0
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 746
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 747
    iget-object v3, p0, Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/16 v4, 0xb

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 748
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 749
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    .line 750
    sget-object v3, Lcom/gd/mobicore/pa/ifc/CommandResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/gd/mobicore/pa/ifc/CommandResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 757
    .local v2, "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 758
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 760
    return-object v2

    .line 743
    .end local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_0
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 757
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 758
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 753
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/gd/mobicore/pa/ifc/CommandResult;
    goto :goto_1
.end method
