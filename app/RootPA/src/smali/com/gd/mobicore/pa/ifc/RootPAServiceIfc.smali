.class public interface abstract Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc;
.super Ljava/lang/Object;
.source "RootPAServiceIfc.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/gd/mobicore/pa/ifc/RootPAServiceIfc$Stub;
    }
.end annotation


# virtual methods
.method public abstract acquireLock(I)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract doProvisioning(ILcom/gd/mobicore/pa/ifc/SPID;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract executeCmpCommands(ILjava/util/List;Ljava/util/List;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/gd/mobicore/pa/ifc/CmpCommand;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/gd/mobicore/pa/ifc/CmpResponse;",
            ">;)",
            "Lcom/gd/mobicore/pa/ifc/CommandResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getDeviceId(Lcom/gd/mobicore/pa/ifc/SUID;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getSPContainerState(Lcom/gd/mobicore/pa/ifc/SPID;Lcom/gd/mobicore/pa/ifc/SPContainerStateParcel;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getSPContainerStructure(Lcom/gd/mobicore/pa/ifc/SPID;Lcom/gd/mobicore/pa/ifc/SPContainerStructure;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getVersion(Lcom/gd/mobicore/pa/ifc/Version;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract isRootContainerRegistered(Lcom/gd/mobicore/pa/ifc/BooleanResult;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract isSPContainerRegistered(Lcom/gd/mobicore/pa/ifc/SPID;Lcom/gd/mobicore/pa/ifc/BooleanResult;)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract releaseLock(I)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract storeTA(Lcom/gd/mobicore/pa/ifc/SPID;[B[B)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
