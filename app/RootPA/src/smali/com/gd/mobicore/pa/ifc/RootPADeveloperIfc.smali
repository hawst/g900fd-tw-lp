.class public interface abstract Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc;
.super Ljava/lang/Object;
.source "RootPADeveloperIfc.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub;
    }
.end annotation


# virtual methods
.method public abstract installTrustlet(I[B[BI[BIII)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract installTrustletOrKey(I[B[B[BI[B)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
