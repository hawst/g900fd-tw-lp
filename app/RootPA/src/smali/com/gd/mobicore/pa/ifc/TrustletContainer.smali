.class public Lcom/gd/mobicore/pa/ifc/TrustletContainer;
.super Ljava/lang/Object;
.source "TrustletContainer.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/gd/mobicore/pa/ifc/TrustletContainer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private state_:Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;

.field private trustletId_:Ljava/util/UUID;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lcom/gd/mobicore/pa/ifc/TrustletContainer$1;

    invoke-direct {v0}, Lcom/gd/mobicore/pa/ifc/TrustletContainer$1;-><init>()V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/TrustletContainer;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method public constructor <init>(Ljava/util/UUID;Lcom/gd/mobicore/pa/ifc/TrustletContainerState;)V
    .locals 1
    .param p1, "trustletId"    # Ljava/util/UUID;
    .param p2, "state"    # Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/gd/mobicore/pa/ifc/TrustletContainer;->trustletId_:Ljava/util/UUID;

    .line 52
    new-instance v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;

    invoke-direct {v0, p2}, Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;-><init>(Lcom/gd/mobicore/pa/ifc/TrustletContainerState;)V

    iput-object v0, p0, Lcom/gd/mobicore/pa/ifc/TrustletContainer;->state_:Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;

    .line 53
    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/gd/mobicore/pa/ifc/TrustletContainer;
    .locals 2
    .param p0, "source"    # Landroid/os/Parcel;

    .prologue
    .line 70
    new-instance v0, Lcom/gd/mobicore/pa/ifc/TrustletContainer;

    invoke-direct {v0}, Lcom/gd/mobicore/pa/ifc/TrustletContainer;-><init>()V

    .line 71
    .local v0, "tc":Lcom/gd/mobicore/pa/ifc/TrustletContainer;
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/gd/mobicore/pa/ifc/TrustletContainer;->setTrustletId(Ljava/lang/String;)V

    .line 72
    sget-object v1, Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;

    invoke-virtual {v0, v1}, Lcom/gd/mobicore/pa/ifc/TrustletContainer;->setState(Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;)V

    .line 73
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public setState(Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;)V
    .locals 0
    .param p1, "state"    # Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/gd/mobicore/pa/ifc/TrustletContainer;->state_:Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;

    .line 109
    return-void
.end method

.method public setTrustletId(Ljava/lang/String;)V
    .locals 1
    .param p1, "trustletId"    # Ljava/lang/String;

    .prologue
    .line 97
    if-eqz p1, :cond_0

    .line 98
    invoke-static {p1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lcom/gd/mobicore/pa/ifc/TrustletContainer;->trustletId_:Ljava/util/UUID;

    .line 101
    :goto_0
    return-void

    .line 100
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/gd/mobicore/pa/ifc/TrustletContainer;->trustletId_:Ljava/util/UUID;

    goto :goto_0
.end method

.method public setTrustletId(Ljava/util/UUID;)V
    .locals 0
    .param p1, "trustletId"    # Ljava/util/UUID;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/gd/mobicore/pa/ifc/TrustletContainer;->trustletId_:Ljava/util/UUID;

    .line 94
    return-void
.end method

.method public state()Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/TrustletContainer;->state_:Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TrustletContainer{trustletId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/gd/mobicore/pa/ifc/TrustletContainer;->trustletId_:Ljava/util/UUID;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/gd/mobicore/pa/ifc/TrustletContainer;->state_:Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public trustletId()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/TrustletContainer;->trustletId_:Ljava/util/UUID;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 62
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/TrustletContainer;->trustletId_:Ljava/util/UUID;

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/TrustletContainer;->state_:Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;

    if-nez v0, :cond_0

    .line 64
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 67
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/gd/mobicore/pa/ifc/TrustletContainer;->state_:Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0
.end method
