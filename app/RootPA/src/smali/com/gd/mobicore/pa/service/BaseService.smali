.class public abstract Lcom/gd/mobicore/pa/service/BaseService;
.super Landroid/app/Service;
.source "BaseService.java"


# static fields
.field private static final lock_:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field protected final commonPaWrapper_:Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

.field protected doProvisioningLockSuid_:I

.field networkChangeReceiver_:Landroid/content/BroadcastReceiver;

.field protected se_:[B

.field private sessionOpened_:Z

.field private timerTask_:Ljava/util/TimerTask;

.field private timer_:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/gd/mobicore/pa/service/BaseService;->lock_:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 66
    iput-object v1, p0, Lcom/gd/mobicore/pa/service/BaseService;->timerTask_:Ljava/util/TimerTask;

    .line 67
    iput-object v1, p0, Lcom/gd/mobicore/pa/service/BaseService;->timer_:Ljava/util/Timer;

    .line 69
    iput v2, p0, Lcom/gd/mobicore/pa/service/BaseService;->doProvisioningLockSuid_:I

    .line 70
    iput-object v1, p0, Lcom/gd/mobicore/pa/service/BaseService;->se_:[B

    .line 82
    new-instance v0, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    invoke-direct {v0, p0}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;-><init>(Lcom/gd/mobicore/pa/service/BaseService;)V

    iput-object v0, p0, Lcom/gd/mobicore/pa/service/BaseService;->commonPaWrapper_:Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    .line 83
    iput-boolean v2, p0, Lcom/gd/mobicore/pa/service/BaseService;->sessionOpened_:Z

    .line 159
    iput-object v1, p0, Lcom/gd/mobicore/pa/service/BaseService;->networkChangeReceiver_:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000()Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/gd/mobicore/pa/service/BaseService;->lock_:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/gd/mobicore/pa/service/BaseService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/gd/mobicore/pa/service/BaseService;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/gd/mobicore/pa/service/BaseService;->sessionOpened_:Z

    return v0
.end method

.method static synthetic access$102(Lcom/gd/mobicore/pa/service/BaseService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/gd/mobicore/pa/service/BaseService;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/gd/mobicore/pa/service/BaseService;->sessionOpened_:Z

    return p1
.end method


# virtual methods
.method protected declared-synchronized acquireLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 6
    .param p1, "uid"    # I
    .param p2, "openSession"    # Z

    .prologue
    const/4 v4, 0x1

    .line 90
    monitor-enter p0

    :try_start_0
    const-string v1, "RootPA-J"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ">>BaseService.acquireLock "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/gd/mobicore/pa/service/BaseService;->lock_:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/gd/mobicore/pa/service/BaseService;->timer_:Ljava/util/Timer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    if-nez p1, :cond_0

    .line 92
    new-instance v1, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/16 v2, 0x8

    invoke-direct {v1, v2}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    :goto_0
    monitor-exit p0

    return-object v1

    .line 94
    :cond_0
    :try_start_1
    sget-object v1, Lcom/gd/mobicore/pa/service/BaseService;->lock_:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    .line 95
    .local v0, "result":Z
    if-eq v0, v4, :cond_1

    sget-object v1, Lcom/gd/mobicore/pa/service/BaseService;->lock_:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-ne v1, p1, :cond_4

    .line 97
    :cond_1
    if-ne v0, v4, :cond_2

    if-ne p2, v4, :cond_2

    iget-boolean v1, p0, Lcom/gd/mobicore/pa/service/BaseService;->sessionOpened_:Z

    if-nez v1, :cond_2

    .line 98
    const-string v1, "RootPA-J"

    const-string v2, "BaseService.acquireLock, openingSession"

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-virtual {p0}, Lcom/gd/mobicore/pa/service/BaseService;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->openSession()I

    .line 100
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/gd/mobicore/pa/service/BaseService;->sessionOpened_:Z

    .line 103
    :cond_2
    iget-object v1, p0, Lcom/gd/mobicore/pa/service/BaseService;->timer_:Ljava/util/Timer;

    if-eqz v1, :cond_3

    .line 104
    iget-object v1, p0, Lcom/gd/mobicore/pa/service/BaseService;->timerTask_:Ljava/util/TimerTask;

    invoke-virtual {v1}, Ljava/util/TimerTask;->cancel()Z

    .line 105
    iget-object v1, p0, Lcom/gd/mobicore/pa/service/BaseService;->timer_:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 108
    :cond_3
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/gd/mobicore/pa/service/BaseService;->timer_:Ljava/util/Timer;

    .line 109
    new-instance v1, Lcom/gd/mobicore/pa/service/BaseService$1;

    invoke-direct {v1, p0}, Lcom/gd/mobicore/pa/service/BaseService$1;-><init>(Lcom/gd/mobicore/pa/service/BaseService;)V

    iput-object v1, p0, Lcom/gd/mobicore/pa/service/BaseService;->timerTask_:Ljava/util/TimerTask;

    .line 120
    iget-object v1, p0, Lcom/gd/mobicore/pa/service/BaseService;->timer_:Ljava/util/Timer;

    iget-object v2, p0, Lcom/gd/mobicore/pa/service/BaseService;->timerTask_:Ljava/util/TimerTask;

    const-wide/32 v4, 0xea60

    invoke-virtual {v1, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 121
    const-string v1, "RootPA-J"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<<BaseService.acquireLock, successfull return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/gd/mobicore/pa/service/BaseService;->timer_:Ljava/util/Timer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    new-instance v1, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 90
    .end local v0    # "result":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 124
    .restart local v0    # "result":Z
    :cond_4
    :try_start_2
    new-instance v1, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method protected commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/gd/mobicore/pa/service/BaseService;->commonPaWrapper_:Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    return-object v0
.end method

.method public getFilesDirPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    invoke-virtual {p0}, Lcom/gd/mobicore/pa/service/BaseService;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected declared-synchronized locked(I)Z
    .locals 1
    .param p1, "uid"    # I

    .prologue
    .line 229
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/gd/mobicore/pa/service/BaseService;->lock_:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eq v0, p1, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 328
    invoke-super {p0, p1}, Landroid/app/Service;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 329
    const-string v0, "RootPA-J"

    const-string v1, "BaseService.onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    return-void
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 333
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 334
    const-string v0, "RootPA-J"

    const-string v1, "BaseService.onCreate"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 338
    iget-object v0, p0, Lcom/gd/mobicore/pa/service/BaseService;->networkChangeReceiver_:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/gd/mobicore/pa/service/BaseService;->networkChangeReceiver_:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/gd/mobicore/pa/service/BaseService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 340
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/gd/mobicore/pa/service/BaseService;->networkChangeReceiver_:Landroid/content/BroadcastReceiver;

    .line 342
    :cond_0
    const-string v0, "RootPA-J"

    const-string v1, "BaseService.onDestroy"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    return-void
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 346
    invoke-super {p0}, Landroid/app/Service;->onLowMemory()V

    .line 347
    const-string v0, "RootPA-J"

    const-string v1, "BaseService.onLowMemory"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 351
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    .line 352
    const-string v0, "RootPA-J"

    const-string v1, "BaseService.onRebind"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 356
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 357
    const-string v0, "RootPA-J"

    const-string v1, "BaseService.onStart"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 361
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    .line 362
    .local v0, "res":I
    const-string v1, "RootPA-J"

    const-string v2, "BaseService.onStartCommand"

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    return v0
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 367
    invoke-super {p0, p1}, Landroid/app/Service;->onTaskRemoved(Landroid/content/Intent;)V

    .line 368
    const-string v0, "RootPA-J"

    const-string v1, "BaseService.onTaskRemoved"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 373
    invoke-super {p0, p1}, Landroid/app/Service;->onTrimMemory(I)V

    .line 374
    const-string v0, "RootPA-J"

    const-string v1, "BaseService.onTrimMemory"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 378
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    .line 379
    .local v0, "res":Z
    const-string v1, "RootPA-J"

    const-string v2, "BaseService.onUnbind"

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    return v0
.end method

.method public provisioningStateCallback(II)V
    .locals 6
    .param p1, "state"    # I
    .param p2, "ret"    # I

    .prologue
    .line 255
    const-string v3, "RootPA-J"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ">>provisioningStateCallback "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    :try_start_0
    iget v3, p0, Lcom/gd/mobicore/pa/service/BaseService;->doProvisioningLockSuid_:I

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/gd/mobicore/pa/service/BaseService;->acquireLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v2

    .line 262
    .local v2, "res":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {v2}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v3

    if-nez v3, :cond_0

    .line 263
    const-string v3, "RootPA-J"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "provisioningStateCallback re-acquiring lock failed, res: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/gd/mobicore/pa/ifc/CommandResult;->result()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    .end local v2    # "res":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :cond_0
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.gd.mobicore.pa.service.PROVISIONING_PROGRESS_UPDATE"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 270
    .local v1, "intent":Landroid/content/Intent;
    sparse-switch p1, :sswitch_data_0

    .line 316
    const-string v3, "RootPA-J"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unknown state: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    const/4 v1, 0x0

    .line 320
    :goto_1
    if-eqz v1, :cond_1

    .line 321
    invoke-virtual {p0, v1}, Lcom/gd/mobicore/pa/service/BaseService;->sendBroadcast(Landroid/content/Intent;)V

    .line 324
    :cond_1
    const-string v3, "RootPA-J"

    const-string v4, "<<provisioningStateCallback "

    invoke-static {v3, v4}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    return-void

    .line 265
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 266
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "RootPA-J"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "provisioningStateCallback re-acquiring lock failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 272
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "intent":Landroid/content/Intent;
    :sswitch_0
    const-string v3, "com.gd.mobicore.pa.ifc.State"

    const/16 v4, 0x64

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 275
    :sswitch_1
    const-string v3, "com.gd.mobicore.pa.ifc.State"

    const/16 v4, 0xc8

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 278
    :sswitch_2
    const-string v3, "com.gd.mobicore.pa.ifc.State"

    const/16 v4, 0x12c

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 281
    :sswitch_3
    const-string v3, "com.gd.mobicore.pa.ifc.State"

    const/16 v4, 0x190

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 284
    :sswitch_4
    const-string v3, "com.gd.mobicore.pa.ifc.State"

    const/16 v4, 0x1f4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 287
    :sswitch_5
    const-string v3, "com.gd.mobicore.pa.ifc.State"

    const/16 v4, 0x3e8

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 290
    :sswitch_6
    const-string v3, "com.gd.mobicore.pa.ifc.State"

    const/16 v4, 0xbb8

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 293
    :sswitch_7
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    const-string v3, "com.gd.mobicore.pa.service.PROVISIONING_ERROR"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 295
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v3, "com.gd.mobicore.pa.ifc.Error"

    invoke-virtual {v1, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 300
    :sswitch_8
    :try_start_1
    iget v3, p0, Lcom/gd/mobicore/pa/service/BaseService;->doProvisioningLockSuid_:I

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/gd/mobicore/pa/service/BaseService;->releaseLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v2

    .line 301
    .restart local v2    # "res":Lcom/gd/mobicore/pa/ifc/CommandResult;
    invoke-virtual {v2}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v3

    if-nez v3, :cond_2

    .line 302
    const-string v3, "RootPA-J"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "provisioningStateCallback releasing lock failed, res: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/gd/mobicore/pa/ifc/CommandResult;->result()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    :cond_2
    const/4 v3, 0x0

    iput v3, p0, Lcom/gd/mobicore/pa/service/BaseService;->doProvisioningLockSuid_:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 305
    const/4 v1, 0x0

    .line 309
    .end local v2    # "res":Lcom/gd/mobicore/pa/ifc/CommandResult;
    :goto_2
    iget-object v3, p0, Lcom/gd/mobicore/pa/service/BaseService;->networkChangeReceiver_:Landroid/content/BroadcastReceiver;

    if-eqz v3, :cond_3

    .line 310
    iget-object v3, p0, Lcom/gd/mobicore/pa/service/BaseService;->networkChangeReceiver_:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3}, Lcom/gd/mobicore/pa/service/BaseService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 311
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/gd/mobicore/pa/service/BaseService;->networkChangeReceiver_:Landroid/content/BroadcastReceiver;

    .line 313
    :cond_3
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.gd.mobicore.pa.service.PROVISIONING_FINISHED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/gd/mobicore/pa/service/BaseService;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 306
    :catch_1
    move-exception v0

    .line 307
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v3, "RootPA-J"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "provisioningStateCallback releasing lock failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 270
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_7
        0x8 -> :sswitch_6
        0xdead -> :sswitch_8
    .end sparse-switch
.end method

.method protected declared-synchronized releaseLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 4
    .param p1, "uid"    # I
    .param p2, "closeSession"    # Z

    .prologue
    const/4 v3, 0x1

    .line 129
    monitor-enter p0

    :try_start_0
    const-string v0, "RootPA-J"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BaseService.releaseLock "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/gd/mobicore/pa/service/BaseService;->lock_:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/gd/mobicore/pa/service/BaseService;->timer_:Ljava/util/Timer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    if-nez p1, :cond_0

    .line 132
    new-instance v0, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    :goto_0
    monitor-exit p0

    return-object v0

    .line 135
    :cond_0
    :try_start_1
    sget-object v0, Lcom/gd/mobicore/pa/service/BaseService;->lock_:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/gd/mobicore/pa/service/BaseService;->lock_:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-ne v0, v3, :cond_4

    .line 137
    :cond_1
    if-ne p2, v3, :cond_2

    iget-boolean v0, p0, Lcom/gd/mobicore/pa/service/BaseService;->sessionOpened_:Z

    if-ne v0, v3, :cond_2

    .line 138
    const-string v0, "RootPA-J"

    const-string v1, "BaseService.releaseLock, closingSession"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-virtual {p0}, Lcom/gd/mobicore/pa/service/BaseService;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->closeSession()V

    .line 140
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/gd/mobicore/pa/service/BaseService;->sessionOpened_:Z

    .line 143
    :cond_2
    iget-object v0, p0, Lcom/gd/mobicore/pa/service/BaseService;->timer_:Ljava/util/Timer;

    if-eqz v0, :cond_3

    .line 144
    iget-object v0, p0, Lcom/gd/mobicore/pa/service/BaseService;->timerTask_:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 145
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/gd/mobicore/pa/service/BaseService;->timerTask_:Ljava/util/TimerTask;

    .line 146
    iget-object v0, p0, Lcom/gd/mobicore/pa/service/BaseService;->timer_:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 147
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/gd/mobicore/pa/service/BaseService;->timer_:Ljava/util/Timer;

    .line 149
    :cond_3
    new-instance v0, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 151
    :cond_4
    :try_start_2
    new-instance v0, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method protected setupProxy()V
    .locals 11

    .prologue
    .line 162
    const/4 v4, 0x0

    .line 163
    .local v4, "proxyAddress":[B
    invoke-static {}, Ljava/net/ProxySelector;->getDefault()Ljava/net/ProxySelector;

    move-result-object v0

    .line 165
    .local v0, "defaultProxySelector":Ljava/net/ProxySelector;
    if-eqz v0, :cond_0

    .line 166
    const/4 v6, 0x0

    .line 167
    .local v6, "uri":Ljava/net/URI;
    const/4 v5, 0x0

    .line 169
    .local v5, "proxyList":Ljava/util/List;, "Ljava/util/List<Ljava/net/Proxy;>;"
    :try_start_0
    iget-object v8, p0, Lcom/gd/mobicore/pa/service/BaseService;->se_:[B

    if-nez v8, :cond_2

    .line 170
    new-instance v7, Ljava/net/URI;

    const-string v8, "https://se.cgbe.trustonic.com"

    invoke-direct {v7, v8}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .end local v6    # "uri":Ljava/net/URI;
    .local v7, "uri":Ljava/net/URI;
    move-object v6, v7

    .line 174
    .end local v7    # "uri":Ljava/net/URI;
    .restart local v6    # "uri":Ljava/net/URI;
    :goto_0
    invoke-virtual {v0, v6}, Ljava/net/ProxySelector;->select(Ljava/net/URI;)Ljava/util/List;

    move-result-object v5

    .line 175
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_0

    .line 177
    const/4 v8, 0x0

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/Proxy;

    .line 178
    .local v3, "proxy":Ljava/net/Proxy;
    const-string v8, "RootPA-J"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "BaseService.setupProxy proxy "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    sget-object v8, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    if-eq v3, v8, :cond_0

    .line 180
    const-string v8, "RootPA-J"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "BaseService.setupProxy proxy.type "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    invoke-virtual {v3}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v8

    sget-object v9, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v8, v9, :cond_0

    .line 186
    invoke-virtual {v3}, Ljava/net/Proxy;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3}, Ljava/net/Proxy;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "@"

    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 196
    .end local v3    # "proxy":Ljava/net/Proxy;
    .end local v5    # "proxyList":Ljava/util/List;, "Ljava/util/List<Ljava/net/Proxy;>;"
    .end local v6    # "uri":Ljava/net/URI;
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/gd/mobicore/pa/service/BaseService;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v8

    const-string v9, "http_proxy"

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    invoke-virtual {v8, v9, v4}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->setEnvironmentVariable([B[B)V

    .line 197
    invoke-virtual {p0}, Lcom/gd/mobicore/pa/service/BaseService;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v8

    const-string v9, "https_proxy"

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    invoke-virtual {v8, v9, v4}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->setEnvironmentVariable([B[B)V

    .line 198
    const-string v9, "RootPA-J"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "BaseService.setupProxy just set the proxy to: "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-nez v4, :cond_3

    .end local v4    # "proxyAddress":[B
    :goto_2
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    iget-object v8, p0, Lcom/gd/mobicore/pa/service/BaseService;->networkChangeReceiver_:Landroid/content/BroadcastReceiver;

    if-nez v8, :cond_1

    .line 203
    new-instance v8, Lcom/gd/mobicore/pa/service/BaseService$2;

    invoke-direct {v8, p0}, Lcom/gd/mobicore/pa/service/BaseService$2;-><init>(Lcom/gd/mobicore/pa/service/BaseService;)V

    iput-object v8, p0, Lcom/gd/mobicore/pa/service/BaseService;->networkChangeReceiver_:Landroid/content/BroadcastReceiver;

    .line 223
    new-instance v2, Landroid/content/IntentFilter;

    const-string v8, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v2, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 224
    .local v2, "filter":Landroid/content/IntentFilter;
    iget-object v8, p0, Lcom/gd/mobicore/pa/service/BaseService;->networkChangeReceiver_:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v2}, Lcom/gd/mobicore/pa/service/BaseService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 226
    .end local v2    # "filter":Landroid/content/IntentFilter;
    :cond_1
    return-void

    .line 172
    .restart local v4    # "proxyAddress":[B
    .restart local v5    # "proxyList":Ljava/util/List;, "Ljava/util/List<Ljava/net/Proxy;>;"
    .restart local v6    # "uri":Ljava/net/URI;
    :cond_2
    :try_start_1
    new-instance v7, Ljava/net/URI;

    new-instance v8, Ljava/lang/String;

    iget-object v9, p0, Lcom/gd/mobicore/pa/service/BaseService;->se_:[B

    invoke-direct {v8, v9}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v7, v8}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .end local v6    # "uri":Ljava/net/URI;
    .restart local v7    # "uri":Ljava/net/URI;
    move-object v6, v7

    .end local v7    # "uri":Ljava/net/URI;
    .restart local v6    # "uri":Ljava/net/URI;
    goto/16 :goto_0

    .line 191
    :catch_0
    move-exception v1

    .line 192
    .local v1, "e":Ljava/lang/Exception;
    const-string v8, "RootPA-J"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "BaseService.setupProxy FAILURE in getting the proxy: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 198
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v5    # "proxyList":Ljava/util/List;, "Ljava/util/List<Ljava/net/Proxy;>;"
    .end local v6    # "uri":Ljava/net/URI;
    :cond_3
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v4}, Ljava/lang/String;-><init>([B)V

    move-object v4, v8

    goto :goto_2
.end method

.method public trustletInstallCallback([B)V
    .locals 3
    .param p1, "trustlet"    # [B

    .prologue
    .line 237
    const-string v1, "RootPA-J"

    const-string v2, ">>BaseService.trustletInstallCallback"

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.gd.mobicore.pa.service.INSTALL_TRUSTLET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 239
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.gd.mobicore.pa.ifc.Trustlet"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 240
    invoke-virtual {p0, v0}, Lcom/gd/mobicore/pa/service/BaseService;->sendBroadcast(Landroid/content/Intent;)V

    .line 241
    const-string v1, "RootPA-J"

    const-string v2, "<<BaseService.trustletInstallCallback"

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    return-void
.end method
