.class public final enum Lcom/gd/mobicore/pa/ifc/TrustletContainerState;
.super Ljava/lang/Enum;
.source "TrustletContainerState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/gd/mobicore/pa/ifc/TrustletContainerState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

.field public static final enum ACTIVATED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

.field public static final enum REGISTERED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

.field public static final enum SP_LOCKED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

.field public static final enum UNDEFINED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39
    new-instance v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v2}, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->UNDEFINED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    .line 41
    new-instance v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    const-string v1, "REGISTERED"

    invoke-direct {v0, v1, v3}, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->REGISTERED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    .line 43
    new-instance v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    const-string v1, "ACTIVATED"

    invoke-direct {v0, v1, v4}, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->ACTIVATED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    .line 45
    new-instance v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    const-string v1, "SP_LOCKED"

    invoke-direct {v0, v1, v5}, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->SP_LOCKED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    .line 37
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    sget-object v1, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->UNDEFINED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->REGISTERED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->ACTIVATED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->SP_LOCKED:Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->$VALUES:[Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/gd/mobicore/pa/ifc/TrustletContainerState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    const-class v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    return-object v0
.end method

.method public static values()[Lcom/gd/mobicore/pa/ifc/TrustletContainerState;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->$VALUES:[Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    invoke-virtual {v0}, [Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    return-object v0
.end method
