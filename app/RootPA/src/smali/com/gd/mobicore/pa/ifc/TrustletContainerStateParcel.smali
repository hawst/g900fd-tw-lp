.class public Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;
.super Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;
.source "TrustletContainerStateParcel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel",
        "<",
        "Lcom/gd/mobicore/pa/ifc/TrustletContainerState;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel$1;

    invoke-direct {v0}, Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel$1;-><init>()V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;-><init>()V

    .line 41
    return-void
.end method

.method public constructor <init>(Lcom/gd/mobicore/pa/ifc/TrustletContainerState;)V
    .locals 0
    .param p1, "enumeratedValue"    # Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/gd/mobicore/pa/ifc/AbstractEnumParcel;-><init>(Ljava/lang/Enum;)V

    .line 45
    return-void
.end method


# virtual methods
.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 55
    invoke-static {v0}, Lcom/gd/mobicore/pa/ifc/TrustletContainerState;->valueOf(Ljava/lang/String;)Lcom/gd/mobicore/pa/ifc/TrustletContainerState;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/gd/mobicore/pa/ifc/TrustletContainerStateParcel;->setEnumeratedValue(Ljava/lang/Enum;)V

    .line 56
    :cond_0
    return-void
.end method
