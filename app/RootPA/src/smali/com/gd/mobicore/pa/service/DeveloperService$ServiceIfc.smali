.class Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;
.super Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub;
.source "DeveloperService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/gd/mobicore/pa/service/DeveloperService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceIfc"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/gd/mobicore/pa/service/DeveloperService;


# direct methods
.method public constructor <init>(Lcom/gd/mobicore/pa/service/DeveloperService;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/DeveloperService;

    .line 63
    invoke-direct {p0}, Lcom/gd/mobicore/pa/ifc/RootPADeveloperIfc$Stub;-><init>()V

    .line 64
    return-void
.end method

.method private commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/DeveloperService;

    invoke-virtual {v0}, Lcom/gd/mobicore/pa/service/DeveloperService;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v0

    return-object v0
.end method

.method private uuidOk([B)Z
    .locals 2
    .param p1, "uuid"    # [B

    .prologue
    .line 76
    if-eqz p1, :cond_0

    array-length v0, p1

    const/16 v1, 0x10

    if-eq v0, v1, :cond_1

    .line 77
    :cond_0
    const-string v0, "RootPA-J"

    const-string v1, "DeveloperService.Stub.uuidOk NOK"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    const/4 v0, 0x0

    .line 81
    :goto_0
    return v0

    .line 80
    :cond_1
    const-string v0, "RootPA-J"

    const-string v1, "DeveloperService.Stub.uuidOk OK"

    invoke-static {v0, v1}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public installTrustlet(I[B[BI[BIII)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 15
    .param p1, "spid"    # I
    .param p2, "uuid"    # [B
    .param p3, "trustletBinary"    # [B
    .param p4, "minTltVersion"    # I
    .param p5, "tltPukHash"    # [B
    .param p6, "memoryType"    # I
    .param p7, "numberOfInstances"    # I
    .param p8, "flags"    # I

    .prologue
    .line 139
    const-string v1, "RootPA-J"

    const-string v2, ">>DeveloperService.Stub.installTrustlet"

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    if-nez p5, :cond_0

    .line 141
    # getter for: Lcom/gd/mobicore/pa/service/DeveloperService;->DEFAULT_PUKHASH:[B
    invoke-static {}, Lcom/gd/mobicore/pa/service/DeveloperService;->access$000()[B

    move-result-object p5

    .line 145
    :cond_0
    if-eqz p3, :cond_1

    if-eqz p1, :cond_1

    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;->uuidOk([B)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    move/from16 v0, p6

    if-le v0, v1, :cond_2

    .line 146
    :cond_1
    new-instance v1, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/16 v2, 0x8

    invoke-direct {v1, v2}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    .line 174
    :goto_0
    return-object v1

    .line 149
    :cond_2
    const/high16 v1, 0x22220000

    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    add-int v14, v1, v2

    .line 151
    .local v14, "tmpSuid":I
    iget-object v1, p0, Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/DeveloperService;

    const/4 v2, 0x0

    invoke-virtual {v1, v14, v2}, Lcom/gd/mobicore/pa/service/DeveloperService;->acquireLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v1

    if-nez v1, :cond_3

    .line 152
    new-instance v1, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto :goto_0

    .line 154
    :cond_3
    iget-object v1, p0, Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/DeveloperService;

    iput v14, v1, Lcom/gd/mobicore/pa/service/DeveloperService;->doProvisioningLockSuid_:I

    .line 155
    const/4 v13, 0x0

    .line 157
    .local v13, "err":I
    :try_start_0
    iget-object v1, p0, Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/DeveloperService;

    invoke-virtual {v1}, Lcom/gd/mobicore/pa/service/DeveloperService;->setupProxy()V

    .line 158
    invoke-direct {p0}, Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v1

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/DeveloperService;

    iget-object v11, v2, Lcom/gd/mobicore/pa/service/DeveloperService;->se_:[B

    move/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v5, p3

    move/from16 v6, p4

    move-object/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    invoke-virtual/range {v1 .. v11}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->installTrustlet(I[BI[BI[BIII[B)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v13

    .line 173
    :goto_1
    const-string v1, "RootPA-J"

    const-string v2, "<<DeveloperService.Stub.installTrustlet"

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    new-instance v1, Lcom/gd/mobicore/pa/ifc/CommandResult;

    invoke-direct {v1, v13}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto :goto_0

    .line 168
    :catch_0
    move-exception v12

    .line 169
    .local v12, "e":Ljava/lang/Exception;
    const-string v1, "RootPA-J"

    const-string v2, "CommonPAWrapper().installTrustlet exception: "

    invoke-static {v1, v2, v12}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 170
    const/4 v13, 0x7

    goto :goto_1
.end method

.method public installTrustletOrKey(I[B[B[BI[B)Lcom/gd/mobicore/pa/ifc/CommandResult;
    .locals 15
    .param p1, "spid"    # I
    .param p2, "uuid"    # [B
    .param p3, "trustletBinary"    # [B
    .param p4, "key"    # [B
    .param p5, "minTltVersion"    # I
    .param p6, "tltPukHash"    # [B

    .prologue
    .line 85
    const-string v1, "RootPA-J"

    const-string v2, ">>DeveloperService.Stub.installTrustletOrKey"

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    if-nez p6, :cond_0

    .line 87
    # getter for: Lcom/gd/mobicore/pa/service/DeveloperService;->DEFAULT_PUKHASH:[B
    invoke-static {}, Lcom/gd/mobicore/pa/service/DeveloperService;->access$000()[B

    move-result-object p6

    .line 90
    :cond_0
    if-nez p3, :cond_1

    if-eqz p4, :cond_3

    :cond_1
    if-eqz p3, :cond_2

    if-nez p4, :cond_3

    :cond_2
    if-eqz p1, :cond_3

    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;->uuidOk([B)Z

    move-result v1

    if-nez v1, :cond_4

    .line 91
    :cond_3
    new-instance v1, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/16 v2, 0x8

    invoke-direct {v1, v2}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    .line 128
    :goto_0
    return-object v1

    .line 94
    :cond_4
    const/high16 v1, 0x22220000

    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    add-int v14, v1, v2

    .line 96
    .local v14, "tmpSuid":I
    iget-object v1, p0, Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/DeveloperService;

    const/4 v2, 0x0

    invoke-virtual {v1, v14, v2}, Lcom/gd/mobicore/pa/service/DeveloperService;->acquireLock(IZ)Lcom/gd/mobicore/pa/ifc/CommandResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/gd/mobicore/pa/ifc/CommandResult;->isOk()Z

    move-result v1

    if-nez v1, :cond_5

    .line 97
    new-instance v1, Lcom/gd/mobicore/pa/ifc/CommandResult;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto :goto_0

    .line 99
    :cond_5
    iget-object v1, p0, Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/DeveloperService;

    iput v14, v1, Lcom/gd/mobicore/pa/service/DeveloperService;->doProvisioningLockSuid_:I

    .line 100
    const/4 v13, 0x0

    .line 101
    .local v13, "err":I
    const/4 v5, 0x0

    .line 104
    .local v5, "data":[B
    if-eqz p3, :cond_6

    .line 105
    move-object/from16 v5, p3

    .line 106
    const/4 v4, 0x1

    .line 111
    .local v4, "dataType":I
    :goto_1
    :try_start_0
    iget-object v1, p0, Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/DeveloperService;

    invoke-virtual {v1}, Lcom/gd/mobicore/pa/service/DeveloperService;->setupProxy()V

    .line 112
    invoke-direct {p0}, Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;->commonPAWrapper()Lcom/gd/mobicore/pa/jni/CommonPAWrapper;

    move-result-object v1

    const/4 v8, 0x2

    const/4 v9, 0x1

    const/4 v10, 0x0

    iget-object v2, p0, Lcom/gd/mobicore/pa/service/DeveloperService$ServiceIfc;->this$0:Lcom/gd/mobicore/pa/service/DeveloperService;

    iget-object v11, v2, Lcom/gd/mobicore/pa/service/DeveloperService;->se_:[B

    move/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v6, p5

    move-object/from16 v7, p6

    invoke-virtual/range {v1 .. v11}, Lcom/gd/mobicore/pa/jni/CommonPAWrapper;->installTrustlet(I[BI[BI[BIII[B)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v13

    .line 127
    :goto_2
    const-string v1, "RootPA-J"

    const-string v2, "<<DeveloperService.Stub.installTrustletOrKey"

    invoke-static {v1, v2}, Lcom/gd/mobicore/pa/service/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    new-instance v1, Lcom/gd/mobicore/pa/ifc/CommandResult;

    invoke-direct {v1, v13}, Lcom/gd/mobicore/pa/ifc/CommandResult;-><init>(I)V

    goto :goto_0

    .line 108
    .end local v4    # "dataType":I
    :cond_6
    move-object/from16 v5, p4

    .line 109
    const/4 v4, 0x2

    .restart local v4    # "dataType":I
    goto :goto_1

    .line 122
    :catch_0
    move-exception v12

    .line 123
    .local v12, "e":Ljava/lang/Exception;
    const-string v1, "RootPA-J"

    const-string v2, "CommonPAWrapper().installTrustletOrKey exception: "

    invoke-static {v1, v2, v12}, Lcom/gd/mobicore/pa/service/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 124
    const/4 v13, 0x7

    goto :goto_2
.end method
