.class public final enum Lcom/gd/mobicore/pa/ifc/SPContainerState;
.super Ljava/lang/Enum;
.source "SPContainerState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/gd/mobicore/pa/ifc/SPContainerState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/gd/mobicore/pa/ifc/SPContainerState;

.field public static final enum ACTIVATED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

.field public static final enum DOES_NOT_EXIST:Lcom/gd/mobicore/pa/ifc/SPContainerState;

.field public static final enum REGISTERED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

.field public static final enum ROOT_LOCKED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

.field public static final enum ROOT_SP_LOCKED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

.field public static final enum SP_LOCKED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

.field public static final enum UNDEFINED:Lcom/gd/mobicore/pa/ifc/SPContainerState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 39
    new-instance v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;

    const-string v1, "DOES_NOT_EXIST"

    invoke-direct {v0, v1, v3}, Lcom/gd/mobicore/pa/ifc/SPContainerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;->DOES_NOT_EXIST:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 41
    new-instance v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;

    const-string v1, "SP_LOCKED"

    invoke-direct {v0, v1, v4}, Lcom/gd/mobicore/pa/ifc/SPContainerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;->SP_LOCKED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 43
    new-instance v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;

    const-string v1, "ROOT_LOCKED"

    invoke-direct {v0, v1, v5}, Lcom/gd/mobicore/pa/ifc/SPContainerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;->ROOT_LOCKED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 45
    new-instance v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v6}, Lcom/gd/mobicore/pa/ifc/SPContainerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;->UNDEFINED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 47
    new-instance v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;

    const-string v1, "REGISTERED"

    invoke-direct {v0, v1, v7}, Lcom/gd/mobicore/pa/ifc/SPContainerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;->REGISTERED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 49
    new-instance v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;

    const-string v1, "ACTIVATED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/gd/mobicore/pa/ifc/SPContainerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;->ACTIVATED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 51
    new-instance v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;

    const-string v1, "ROOT_SP_LOCKED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/gd/mobicore/pa/ifc/SPContainerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;->ROOT_SP_LOCKED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    .line 37
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/gd/mobicore/pa/ifc/SPContainerState;

    sget-object v1, Lcom/gd/mobicore/pa/ifc/SPContainerState;->DOES_NOT_EXIST:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/gd/mobicore/pa/ifc/SPContainerState;->SP_LOCKED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/gd/mobicore/pa/ifc/SPContainerState;->ROOT_LOCKED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/gd/mobicore/pa/ifc/SPContainerState;->UNDEFINED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/gd/mobicore/pa/ifc/SPContainerState;->REGISTERED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/gd/mobicore/pa/ifc/SPContainerState;->ACTIVATED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/gd/mobicore/pa/ifc/SPContainerState;->ROOT_SP_LOCKED:Lcom/gd/mobicore/pa/ifc/SPContainerState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;->$VALUES:[Lcom/gd/mobicore/pa/ifc/SPContainerState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/gd/mobicore/pa/ifc/SPContainerState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    const-class v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;

    return-object v0
.end method

.method public static values()[Lcom/gd/mobicore/pa/ifc/SPContainerState;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/gd/mobicore/pa/ifc/SPContainerState;->$VALUES:[Lcom/gd/mobicore/pa/ifc/SPContainerState;

    invoke-virtual {v0}, [Lcom/gd/mobicore/pa/ifc/SPContainerState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/gd/mobicore/pa/ifc/SPContainerState;

    return-object v0
.end method
