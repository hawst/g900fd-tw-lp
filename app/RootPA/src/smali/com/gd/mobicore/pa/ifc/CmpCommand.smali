.class public Lcom/gd/mobicore/pa/ifc/CmpCommand;
.super Lcom/gd/mobicore/pa/ifc/CmpMsg;
.source "CmpCommand.java"


# static fields
.field public static final COMMAND_LENGTH_IDX:I = 0x4

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/gd/mobicore/pa/ifc/CmpCommand;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 114
    new-instance v0, Lcom/gd/mobicore/pa/ifc/CmpCommand$1;

    invoke-direct {v0}, Lcom/gd/mobicore/pa/ifc/CmpCommand$1;-><init>()V

    sput-object v0, Lcom/gd/mobicore/pa/ifc/CmpCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "commandId"    # I

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/gd/mobicore/pa/ifc/CmpMsg;-><init>()V

    .line 99
    invoke-virtual {p0, p1}, Lcom/gd/mobicore/pa/ifc/CmpCommand;->setCommandId(I)V

    .line 100
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/gd/mobicore/pa/ifc/CmpMsg;-><init>(Landroid/os/Parcel;)V

    .line 111
    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .param p1, "content"    # [B

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/gd/mobicore/pa/ifc/CmpMsg;-><init>([B)V

    .line 104
    return-void
.end method


# virtual methods
.method public commandId()I
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/gd/mobicore/pa/ifc/CmpCommand;->msgId()I

    move-result v0

    return v0
.end method

.method public ignoreError()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/gd/mobicore/pa/ifc/CmpCommand;->ignoreError_:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x1

    .line 131
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/gd/mobicore/pa/ifc/CmpCommand;->ignoreError_:Z

    .line 132
    invoke-super {p0, p1}, Lcom/gd/mobicore/pa/ifc/CmpMsg;->readFromParcel(Landroid/os/Parcel;)V

    .line 133
    return-void

    .line 131
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCommandId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 78
    invoke-virtual {p0, p1}, Lcom/gd/mobicore/pa/ifc/CmpCommand;->setMsgId(I)V

    .line 79
    return-void
.end method

.method public setIgnoreError(Z)V
    .locals 0
    .param p1, "ignore"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/gd/mobicore/pa/ifc/CmpCommand;->ignoreError_:Z

    .line 65
    return-void
.end method

.method public setLength(I)V
    .locals 3
    .param p1, "length"    # I

    .prologue
    const/4 v2, 0x4

    .line 84
    invoke-virtual {p0}, Lcom/gd/mobicore/pa/ifc/CmpCommand;->msgId()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/gd/mobicore/pa/ifc/CmpCommand;->msgId()I

    move-result v0

    if-eq v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/gd/mobicore/pa/ifc/CmpCommand;->msgId()I

    move-result v0

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_0

    if-le p1, v2, :cond_0

    .line 89
    invoke-virtual {p0, v2, p1}, Lcom/gd/mobicore/pa/ifc/CmpCommand;->setInt(II)V

    .line 91
    :cond_0
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v0, 0x1

    .line 126
    iget-boolean v1, p0, Lcom/gd/mobicore/pa/ifc/CmpCommand;->ignoreError_:Z

    if-ne v1, v0, :cond_0

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 127
    invoke-super {p0, p1, p2}, Lcom/gd/mobicore/pa/ifc/CmpMsg;->writeToParcel(Landroid/os/Parcel;I)V

    .line 128
    return-void

    .line 126
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
