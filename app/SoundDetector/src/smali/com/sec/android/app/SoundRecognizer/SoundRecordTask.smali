.class Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;
.super Ljava/lang/Object;
.source "SoundRecordTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field static final BYTE_BUFFER_SIZE:I = 0x8000

.field public static final BabyCrying:I = 0x0

.field static final SHORT_BUFFER_SIZE:I = 0x4000

.field static final SHORT_QUEUE_SIZE:I = 0xc000


# instance fields
.field private final AED_BABY_CRYING:I

.field private final AED_OTHER:I

.field private final AED_OTHER2:I

.field private final AED_OTHER3:I

.field private final AED_SPEECH:I

.field private final AED_SPEECH2:I

.field private AUDIO_RECORD_FOR_SOUND_DETECT:I

.field private EnergyLevel:[I

.field public TAG:Ljava/lang/String;

.field private aedflag1:I

.field private aedflag2:I

.field private aedflag3:I

.field private aedflag4:I

.field private aedflag5:I

.field private aedflag6:I

.field private final babyFileName:Ljava/lang/String;

.field public block_size:I

.field public buf:[B

.field private bufferCount:I

.field public dataInfo:[I

.field public done:Z

.field public f:Ljava/io/File;

.field public handler:Landroid/os/Handler;

.field private isAEDFlagOn:Z

.field public isMakePCM:Z

.field public isSoundDetectEnable:Z

.field private mAEDRecognizer:Lcom/samsung/android/app/AED;

.field private mAssetManager:Landroid/content/res/AssetManager;

.field public mCommandType:I

.field private mContext:Landroid/content/Context;

.field public mFileOutputStream:Ljava/io/FileOutputStream;

.field private mSamsungBCPPEngine:Lcom/sec/android/app/bcpp/SamsungBCPPEngine;

.field private m_listener:Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;

.field private numDivision:I

.field private final otherFileName:Ljava/lang/String;

.field private final otherFileName2:Ljava/lang/String;

.field private final otherFileName3:Ljava/lang/String;

.field private readLength:I

.field private readNshorts:I

.field public rec:Landroid/media/AudioRecord;

.field public resultInfo:[I

.field public resultIntInfo:[I

.field public rmshandler:Landroid/os/Handler;

.field public soundData:[S

.field public soundDataForBCPP:[S

.field private final speechFileName:Ljava/lang/String;

.field private final speechFileName2:Ljava/lang/String;

.field private tmp:[I

.field private totalReadCount:I


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;
    .param p3, "command"    # I

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->rec:Landroid/media/AudioRecord;

    .line 27
    iput v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->block_size:I

    .line 28
    iput-boolean v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->done:Z

    .line 30
    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->soundData:[S

    .line 31
    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->soundDataForBCPP:[S

    .line 33
    const-string v0, "SoundRecordTask"

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    .line 40
    iput-boolean v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->isMakePCM:Z

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->m_listener:Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;

    .line 43
    iput v3, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readNshorts:I

    .line 45
    iput v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mCommandType:I

    .line 47
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->AUDIO_RECORD_FOR_SOUND_DETECT:I

    .line 49
    iput v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->totalReadCount:I

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->f:Ljava/io/File;

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mFileOutputStream:Ljava/io/FileOutputStream;

    .line 54
    iput-boolean v4, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->isSoundDetectEnable:Z

    .line 56
    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAEDRecognizer:Lcom/samsung/android/app/AED;

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mSamsungBCPPEngine:Lcom/sec/android/app/bcpp/SamsungBCPPEngine;

    .line 59
    new-array v0, v5, [I

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->resultInfo:[I

    .line 60
    new-array v0, v5, [I

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->resultIntInfo:[I

    .line 61
    new-array v0, v5, [I

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->dataInfo:[I

    .line 63
    iput v3, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag1:I

    .line 64
    iput v3, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag2:I

    .line 65
    iput v3, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag3:I

    .line 66
    iput v3, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag4:I

    .line 67
    iput v3, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag5:I

    .line 68
    iput v3, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag6:I

    .line 70
    const-string v0, "Model/baby02.model"

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->babyFileName:Ljava/lang/String;

    .line 71
    const-string v0, "Model/speech1.model"

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->speechFileName:Ljava/lang/String;

    .line 72
    const-string v0, "Model/other1.model"

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->otherFileName:Ljava/lang/String;

    .line 73
    const-string v0, "Model/speech2.model"

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->speechFileName2:Ljava/lang/String;

    .line 74
    const-string v0, "Model/other2.model"

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->otherFileName2:Ljava/lang/String;

    .line 75
    const-string v0, "Model/other3.model"

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->otherFileName3:Ljava/lang/String;

    .line 77
    iput v4, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->AED_BABY_CRYING:I

    .line 78
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->AED_SPEECH:I

    .line 79
    iput v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->AED_SPEECH2:I

    .line 80
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->AED_OTHER:I

    .line 81
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->AED_OTHER2:I

    .line 82
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->AED_OTHER3:I

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mContext:Landroid/content/Context;

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAssetManager:Landroid/content/res/AssetManager;

    .line 88
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->EnergyLevel:[I

    .line 90
    iput-boolean v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->isAEDFlagOn:Z

    .line 92
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->tmp:[I

    .line 94
    iput v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->bufferCount:I

    .line 96
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->numDivision:I

    .line 97
    iput v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readLength:I

    .line 388
    new-instance v0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask$1;-><init>(Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;)V

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->handler:Landroid/os/Handler;

    .line 416
    new-instance v0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask$2;-><init>(Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;)V

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->rmshandler:Landroid/os/Handler;

    .line 101
    const v0, 0x8000

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->init(ILandroid/content/Context;Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;I)V

    .line 102
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;)Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->m_listener:Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;

    return-object v0
.end method

.method private getAudioRecord(I)Landroid/media/AudioRecord;
    .locals 13
    .param p1, "source"    # I

    .prologue
    const/4 v8, 0x0

    const v12, 0x28000

    const/16 v11, 0x3e80

    const/16 v10, 0x10

    const/4 v9, 0x2

    .line 451
    const/4 v7, 0x0

    .line 453
    .local v7, "retAudioRecord":Landroid/media/AudioRecord;
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v2, "getAudioRecord"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    :try_start_0
    new-instance v0, Landroid/media/AudioRecord;

    const/16 v2, 0x3e80

    .line 458
    const/16 v3, 0x10

    .line 459
    const/4 v4, 0x2

    const v5, 0x28000

    move v1, p1

    .line 457
    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 461
    .end local v7    # "retAudioRecord":Landroid/media/AudioRecord;
    .local v0, "retAudioRecord":Landroid/media/AudioRecord;
    :try_start_1
    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 463
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getAudioRecord for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=false, got !initialized"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    if-eqz v0, :cond_0

    .line 467
    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    :cond_0
    move-object v1, v8

    .line 495
    :goto_0
    return-object v1

    .line 473
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "got AudioRecord using source="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", also "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3e80

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 474
    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x28000

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 473
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 493
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getAudioRecord for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=true"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    .line 495
    goto :goto_0

    .line 476
    .end local v0    # "retAudioRecord":Landroid/media/AudioRecord;
    .restart local v7    # "retAudioRecord":Landroid/media/AudioRecord;
    :catch_0
    move-exception v6

    move-object v0, v7

    .line 478
    .end local v7    # "retAudioRecord":Landroid/media/AudioRecord;
    .restart local v0    # "retAudioRecord":Landroid/media/AudioRecord;
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getAudioRecord for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=false, IllegalArgumentException"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "got IllegalArgumentException using source="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", also "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 481
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 480
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    if-eqz v0, :cond_2

    .line 485
    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    :cond_2
    move-object v1, v8

    .line 487
    goto/16 :goto_0

    .line 476
    .end local v6    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v6

    goto :goto_1
.end method

.method public static twoBytesToShort(BB)S
    .locals 2
    .param p0, "b1"    # B
    .param p1, "b2"    # B

    .prologue
    .line 385
    and-int/lit16 v0, p0, 0xff

    shl-int/lit8 v1, p1, 0x8

    or-int/2addr v0, v1

    int-to-short v0, v0

    return v0
.end method


# virtual methods
.method public SendHandlerMessage(I[I)V
    .locals 3
    .param p1, "result"    # I
    .param p2, "resultInfo"    # [I

    .prologue
    .line 408
    iget-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 409
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 410
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "recognition_result"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 411
    const-string v2, "result_intInfo"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 412
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 413
    iget-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 414
    return-void
.end method

.method init(ILandroid/content/Context;Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;I)V
    .locals 10
    .param p1, "block_size"    # I
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "listener"    # Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;
    .param p4, "command"    # I

    .prologue
    const v9, 0x8000

    const/16 v8, 0x4000

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v2, "init()"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "command : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iput-boolean v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->done:Z

    .line 111
    iput p1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->block_size:I

    .line 112
    iput p4, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mCommandType:I

    .line 113
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->rec:Landroid/media/AudioRecord;

    .line 114
    iput-object p2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mContext:Landroid/content/Context;

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isSoundDetectEnable : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->isSoundDetectEnable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    iget v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->AUDIO_RECORD_FOR_SOUND_DETECT:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->getAudioRecord(I)Landroid/media/AudioRecord;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->rec:Landroid/media/AudioRecord;

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->rec:Landroid/media/AudioRecord;

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "new AudioRecord : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->AUDIO_RECORD_FOR_SOUND_DETECT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAEDRecognizer:Lcom/samsung/android/app/AED;

    if-nez v1, :cond_1

    .line 127
    invoke-static {}, Lcom/samsung/android/app/AEDWrapper;->getInstance()Lcom/samsung/android/app/AED;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAEDRecognizer:Lcom/samsung/android/app/AED;

    .line 130
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mSamsungBCPPEngine:Lcom/sec/android/app/bcpp/SamsungBCPPEngine;

    if-nez v1, :cond_2

    .line 132
    invoke-static {}, Lcom/sec/android/app/bcpp/SamsungBCPPEngineWrapper;->getInstance()Lcom/sec/android/app/bcpp/SamsungBCPPEngine;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mSamsungBCPPEngine:Lcom/sec/android/app/bcpp/SamsungBCPPEngine;

    .line 135
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAssetManager:Landroid/content/res/AssetManager;

    if-nez v1, :cond_3

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAssetManager:Landroid/content/res/AssetManager;

    .line 140
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v2, "AedInit start"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAEDRecognizer:Lcom/samsung/android/app/AED;

    if-eqz v1, :cond_4

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAEDRecognizer:Lcom/samsung/android/app/AED;

    iget-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAssetManager:Landroid/content/res/AssetManager;

    const-string v3, "Model/baby02.model"

    invoke-virtual {v1, v2, v3, v6}, Lcom/samsung/android/app/AED;->AedInit(Landroid/content/res/AssetManager;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag1:I

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAEDRecognizer:Lcom/samsung/android/app/AED;

    iget-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAssetManager:Landroid/content/res/AssetManager;

    const-string v3, "Model/speech1.model"

    invoke-virtual {v1, v2, v3, v7}, Lcom/samsung/android/app/AED;->AedInit(Landroid/content/res/AssetManager;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag2:I

    .line 146
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAEDRecognizer:Lcom/samsung/android/app/AED;

    iget-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAssetManager:Landroid/content/res/AssetManager;

    const-string v3, "Model/speech2.model"

    const/4 v4, 0x3

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/app/AED;->AedInit(Landroid/content/res/AssetManager;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag3:I

    .line 147
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAEDRecognizer:Lcom/samsung/android/app/AED;

    iget-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAssetManager:Landroid/content/res/AssetManager;

    const-string v3, "Model/other1.model"

    const/4 v4, 0x4

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/app/AED;->AedInit(Landroid/content/res/AssetManager;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag4:I

    .line 148
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAEDRecognizer:Lcom/samsung/android/app/AED;

    iget-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAssetManager:Landroid/content/res/AssetManager;

    const-string v3, "Model/other2.model"

    const/4 v4, 0x5

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/app/AED;->AedInit(Landroid/content/res/AssetManager;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag5:I

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAEDRecognizer:Lcom/samsung/android/app/AED;

    iget-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAssetManager:Landroid/content/res/AssetManager;

    const-string v3, "Model/other3.model"

    const/4 v4, 0x6

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/app/AED;->AedInit(Landroid/content/res/AssetManager;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag6:I

    .line 152
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AedInit FALG 1 : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AedInit FALG 2 : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AedInit FALG 3 : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag3:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AedInit FALG 4 : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag4:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AedInit FALG 5 : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag5:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AedInit FALG 6 : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->aedflag6:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v2, "AedInit end"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    iput-object p3, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->m_listener:Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;

    .line 164
    iput v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->totalReadCount:I

    .line 166
    iget-boolean v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->isMakePCM:Z

    if-eqz v1, :cond_5

    .line 168
    new-instance v1, Ljava/io/File;

    const-string v2, "/mnt/sdcard"

    const-string v3, "testPCM.pcm"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->f:Ljava/io/File;

    .line 172
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->f:Ljava/io/File;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mFileOutputStream:Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :cond_5
    :goto_0
    new-array v1, v9, [B

    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->buf:[B

    .line 181
    new-array v1, v8, [S

    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->soundData:[S

    .line 182
    const v1, 0xc000

    new-array v1, v1, [S

    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->soundDataForBCPP:[S

    .line 185
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->dataInfo:[I

    aput v8, v1, v5

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->dataInfo:[I

    aput v5, v1, v6

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->dataInfo:[I

    aput v5, v1, v7

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->resultInfo:[I

    aput p4, v1, v5

    .line 190
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->resultInfo:[I

    aput v5, v1, v6

    .line 191
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->resultInfo:[I

    aput v5, v1, v7

    .line 193
    iget v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->numDivision:I

    div-int v1, v9, v1

    iput v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readLength:I

    .line 194
    iput v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->bufferCount:I

    .line 196
    return-void

    .line 174
    :catch_0
    move-exception v0

    .line 176
    .local v0, "e1":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public isExistFile(Ljava/lang/String;)Z
    .locals 2
    .param p1, "mFilePath"    # Ljava/lang/String;

    .prologue
    .line 441
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 442
    .local v0, "mFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 443
    const/4 v1, 0x1

    .line 446
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method readByteBlock()I
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v5, -0x1

    .line 279
    iget-boolean v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->done:Z

    if-eqz v6, :cond_0

    .line 281
    iget-object v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v7, "readByteBlock return -1 : Section1"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    iput v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readNshorts:I

    .line 381
    :goto_0
    return v5

    .line 286
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->rec:Landroid/media/AudioRecord;

    if-eqz v6, :cond_1

    iget-boolean v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->done:Z

    if-nez v6, :cond_1

    .line 288
    iget-object v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->rec:Landroid/media/AudioRecord;

    iget-object v7, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->buf:[B

    iget v8, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->bufferCount:I

    iget v9, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readLength:I

    mul-int/2addr v8, v9

    iget v9, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readLength:I

    invoke-virtual {v6, v7, v8, v9}, Landroid/media/AudioRecord;->read([BII)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readNshorts:I

    .line 291
    :cond_1
    iget-boolean v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->done:Z

    if-eqz v6, :cond_2

    .line 293
    iget-object v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v7, "readByteBlock return -1 : Section2"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    iput v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readNshorts:I

    goto :goto_0

    .line 298
    :cond_2
    iget v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readNshorts:I

    iget v7, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readLength:I

    if-ge v6, v7, :cond_3

    .line 300
    iget-object v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "AudioRecord Read problem : nshorts = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readNshorts:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " command = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mCommandType:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    iput v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readNshorts:I

    goto :goto_0

    .line 306
    :cond_3
    iget v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->bufferCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->bufferCount:I

    .line 308
    iget v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->bufferCount:I

    iget v7, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->numDivision:I

    if-ne v6, v7, :cond_4

    .line 310
    iget-object v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "nshorts = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readNshorts:I

    iget v9, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->bufferCount:I

    mul-int/2addr v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " command = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mCommandType:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    :cond_4
    iget v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->totalReadCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->totalReadCount:I

    .line 316
    iget v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->bufferCount:I

    iget v7, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->numDivision:I

    if-ne v6, v7, :cond_8

    .line 318
    iput v11, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->bufferCount:I

    .line 320
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    const v6, 0x8000

    if-lt v3, v6, :cond_5

    .line 325
    iget-boolean v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->done:Z

    if-eqz v6, :cond_6

    .line 327
    iget-object v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v7, "readByteBlock return -1 : Section3"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    iput v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readNshorts:I

    goto/16 :goto_0

    .line 322
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->soundData:[S

    div-int/lit8 v7, v3, 0x2

    iget-object v8, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->buf:[B

    aget-byte v8, v8, v3

    iget-object v9, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->buf:[B

    add-int/lit8 v10, v3, 0x1

    aget-byte v9, v9, v10

    invoke-static {v8, v9}, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->twoBytesToShort(BB)S

    move-result v8

    aput-short v8, v6, v7

    .line 320
    add-int/lit8 v3, v3, 0x2

    goto :goto_1

    .line 332
    :cond_6
    iget v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readNshorts:I

    if-lez v5, :cond_8

    .line 334
    iget-boolean v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->isMakePCM:Z

    if-eqz v5, :cond_7

    .line 337
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mFileOutputStream:Ljava/io/FileOutputStream;

    iget-object v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->buf:[B

    invoke-virtual {v5, v6}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 344
    :cond_7
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v6, "Start AedExe"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    iget-object v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAEDRecognizer:Lcom/samsung/android/app/AED;

    iget-object v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->soundData:[S

    const/4 v7, 0x6

    iget-object v8, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->EnergyLevel:[I

    iget-object v9, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->tmp:[I

    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/samsung/android/app/AED;->AedExe([SI[I[I)I

    move-result v0

    .line 346
    .local v0, "aedResult":I
    iget-object v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v6, "End AedExe"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    iget-object v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mSamsungBCPPEngine:Lcom/sec/android/app/bcpp/SamsungBCPPEngine;

    iget-object v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->soundData:[S

    iget-object v7, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->dataInfo:[I

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/bcpp/SamsungBCPPEngine;->computeRMSEnergy([S[I)I

    move-result v4

    .line 350
    .local v4, "rmsResult":I
    invoke-virtual {p0, v4}, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->rmsSendHandlerMessage(I)V

    .line 352
    iget-object v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->resultInfo:[I

    iget v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mCommandType:I

    aput v6, v5, v11

    .line 354
    iget-object v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "aedResult : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    iget-object v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "rmsResult : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    if-ne v0, v12, :cond_a

    .line 360
    iget-object v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v6, "Start findStrongHarmonics"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    iget-object v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mSamsungBCPPEngine:Lcom/sec/android/app/bcpp/SamsungBCPPEngine;

    iget-object v6, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->soundData:[S

    iget-object v7, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->dataInfo:[I

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/bcpp/SamsungBCPPEngine;->findStrongHarmonics([S[I)I

    move-result v1

    .line 362
    .local v1, "bcppResult":I
    iget-object v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "End findStrongHarmonics bcppResult : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    if-ne v1, v12, :cond_9

    .line 366
    iget-object v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->resultInfo:[I

    invoke-virtual {p0, v0, v5}, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->SendHandlerMessage(I[I)V

    .line 381
    .end local v0    # "aedResult":I
    .end local v1    # "bcppResult":I
    .end local v3    # "i":I
    .end local v4    # "rmsResult":I
    :cond_8
    :goto_3
    iget v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readNshorts:I

    goto/16 :goto_0

    .line 338
    .restart local v3    # "i":I
    :catch_0
    move-exception v2

    .line 340
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 370
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v0    # "aedResult":I
    .restart local v1    # "bcppResult":I
    .restart local v4    # "rmsResult":I
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->resultInfo:[I

    invoke-virtual {p0, v11, v5}, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->SendHandlerMessage(I[I)V

    goto :goto_3

    .line 375
    .end local v1    # "bcppResult":I
    :cond_a
    iget-object v5, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->resultInfo:[I

    invoke-virtual {p0, v11, v5}, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->SendHandlerMessage(I[I)V

    goto :goto_3
.end method

.method public rmsSendHandlerMessage(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 433
    iget-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 434
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 435
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "rms_value"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 436
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 437
    iget-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->rmshandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 438
    return-void
.end method

.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "run start"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->rec:Landroid/media/AudioRecord;

    if-eqz v0, :cond_3

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "Call rec.startRecording start"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->rec:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "Call startRecording end"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->done:Z

    if-eqz v0, :cond_2

    .line 259
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->stopSoundDetectAudioRecord()V

    .line 262
    iput-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAEDRecognizer:Lcom/samsung/android/app/AED;

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "aMMUIRecognizer = null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    iput-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->m_listener:Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "m_listener = null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    iput-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mContext:Landroid/content/Context;

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "mContext = null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    iput-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->mAssetManager:Landroid/content/res/AssetManager;

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "mAssetManager = null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "run end"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    return-void

    .line 241
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readByteBlock()I

    .line 243
    iget-boolean v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->done:Z

    if-nez v0, :cond_1

    .line 248
    iget v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->readNshorts:I

    if-gtz v0, :cond_0

    goto :goto_0

    .line 256
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "SoundDetect fail to start"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "SoundRecordTask : stop start"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->done:Z

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "SoundRecordTask : stop end"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    return-void
.end method

.method public stopSoundDetectAudioRecord()V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "stopSoundDetectAudioRecord start"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->rec:Landroid/media/AudioRecord;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "Call rec.stop start"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->rec:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "Call rec.stop end"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "Call rec.release start"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->rec:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "Call rec.release end"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->rec:Landroid/media/AudioRecord;

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "rec = null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v1, "stopSoundDetectAudioRecord end"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    return-void
.end method
