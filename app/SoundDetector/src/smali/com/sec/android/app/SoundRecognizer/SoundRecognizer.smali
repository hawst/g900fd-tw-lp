.class public Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;
.super Ljava/lang/Object;
.source "SoundRecognizer.java"


# static fields
.field public static final STATE_READY:I = 0x0

.field public static final STATE_RUNNING:I = 0x1


# instance fields
.field private TAG:Ljava/lang/String;

.field private audio:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

.field private audio_thread:Ljava/lang/Thread;

.field private handler:Landroid/os/Handler;

.field public isEnableSoundDetect:Z

.field public mCommandType:I

.field private mContext:Landroid/content/Context;

.field private mListener:Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;

.field private mState:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;
    .param p3, "commandtype"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    .line 17
    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio_thread:Ljava/lang/Thread;

    .line 18
    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mListener:Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;

    .line 19
    iput v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mState:I

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->isEnableSoundDetect:Z

    .line 23
    const-string v0, "SoundRecognizer"

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    .line 24
    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->handler:Landroid/os/Handler;

    .line 25
    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mContext:Landroid/content/Context;

    .line 27
    iput v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mCommandType:I

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v1, "make new SoundRecognizer"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mContext:Landroid/content/Context;

    .line 33
    iput-object p2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mListener:Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;

    .line 34
    iput p3, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mCommandType:I

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Commandtype : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mCommandType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    return-void
.end method


# virtual methods
.method public SendHandlerMessage(I)V
    .locals 6
    .param p1, "type"    # I

    .prologue
    .line 67
    iget-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->handler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 70
    iget-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 71
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 72
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "commandType"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 73
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 74
    iget-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->handler:Landroid/os/Handler;

    const-wide/16 v4, 0x2bc

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 76
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public delayedStartSoundDetect(I)V
    .locals 3
    .param p1, "commandType"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v1, "delayedStartSoundDetect"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    monitor-enter p0

    .line 110
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v1, "SoundDetect is running. So Do nothing"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :goto_0
    monitor-exit p0

    .line 142
    return-void

    .line 116
    :cond_0
    new-instance v0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mListener:Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;

    invoke-direct {v0, v1, v2, p1}, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;-><init>(Landroid/content/Context;Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;I)V

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    iget-object v0, v0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->rec:Landroid/media/AudioRecord;

    if-eqz v0, :cond_1

    .line 120
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio_thread:Ljava/lang/Thread;

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio_thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 122
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mState:I

    goto :goto_0

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 126
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v1, "fail to running SoundDetect"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    if-eqz v0, :cond_2

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    invoke-virtual {v0}, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->stop()V

    .line 133
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio_thread:Ljava/lang/Thread;

    if-eqz v0, :cond_3

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v1, "why running empty audio_thread"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public destroySoundRecognizer()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v1, "destroySoundRecognizer"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    invoke-virtual {p0}, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->stopSoundDetect()V

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mListener:Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;

    if-eqz v0, :cond_0

    .line 206
    iput-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mListener:Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v1, "mListener = null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 212
    iput-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mContext:Landroid/content/Context;

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v1, "mContext = null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    :cond_1
    return-void
.end method

.method public getCommandtype()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mCommandType:I

    return v0
.end method

.method public getIsEnableSoundDetect()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->isEnableSoundDetect:Z

    return v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mState:I

    return v0
.end method

.method public isExistFile(Ljava/lang/String;)Z
    .locals 2
    .param p1, "mFilePath"    # Ljava/lang/String;

    .prologue
    .line 219
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 220
    .local v0, "mFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 222
    const/4 v1, 0x1

    .line 225
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v1, "setContext"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    iput-object p1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mContext:Landroid/content/Context;

    .line 48
    return-void
.end method

.method public setListener(Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v1, "setListener"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mListener:Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;

    .line 42
    return-void
.end method

.method public startSoundDetect(I)V
    .locals 3
    .param p1, "commandType"    # I

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v1, "startSoundDetect"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "commandType : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget-boolean v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->isEnableSoundDetect:Z

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->handler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 88
    new-instance v0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer$1;-><init>(Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;)V

    iput-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->handler:Landroid/os/Handler;

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v1, "handler create"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->SendHandlerMessage(I)V

    .line 102
    :cond_1
    return-void
.end method

.method public stopSoundDetect()V
    .locals 4

    .prologue
    .line 147
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v2, "stopSoundDetect Start"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    monitor-enter p0

    .line 151
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->isEnableSoundDetect:Z

    if-eqz v1, :cond_2

    .line 153
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->handler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->handler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 156
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->handler:Landroid/os/Handler;

    .line 158
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v2, "handler = null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    if-eqz v1, :cond_1

    .line 164
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    invoke-virtual {v1}, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->stop()V

    .line 166
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio_thread:Ljava/lang/Thread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_3

    .line 170
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v2, "wait for audio to stop: begin"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio_thread:Ljava/lang/Thread;

    const-wide/16 v2, 0x2bc

    invoke-virtual {v1, v2, v3}, Ljava/lang/Thread;->join(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 183
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v2, "wait for audio to stop: end"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v2, "audio = null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->audio_thread:Ljava/lang/Thread;

    .line 191
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->mState:I

    .line 149
    :cond_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 195
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v2, "stopSoundDetect End"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    return-void

    .line 174
    :catch_0
    move-exception v0

    .line 176
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 149
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    .line 181
    :cond_3
    :try_start_4
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->TAG:Ljava/lang/String;

    const-string v2, "audio_thread was not working"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method
