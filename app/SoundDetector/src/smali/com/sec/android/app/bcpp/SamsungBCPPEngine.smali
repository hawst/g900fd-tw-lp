.class public Lcom/sec/android/app/bcpp/SamsungBCPPEngine;
.super Ljava/lang/Object;
.source "SamsungBCPPEngine.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init()V
    .locals 3

    .prologue
    .line 14
    :try_start_0
    const-string v1, "SamsungBCPPEngine"

    const-string v2, "Trying to load libSamsungBCPP.so"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 15
    const-string v1, "SamsungBCPP"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 16
    const-string v1, "SamsungBCPPEngine"

    const-string v2, "Loading libSamsungBCPP.so done"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 18
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 20
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "SamsungBCPPEngine"

    const-string v2, "WARNING: Could not load libSamsungBCPP.so"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public native computeRMSEnergy([S[I)I
.end method

.method public native findStrongHarmonics([S[I)I
.end method
