.class Lcom/sec/android/app/SoundRecognizer/SoundRecordTask$1;
.super Landroid/os/Handler;
.source "SoundRecordTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;


# direct methods
.method constructor <init>(Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask$1;->this$0:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    .line 388
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 392
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask$1;->this$0:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    iget-object v1, v1, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->TAG:Ljava/lang/String;

    const-string v2, "handleMessage"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "recognition_result"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 395
    .local v0, "result":I
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask$1;->this$0:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "result_intInfo"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->resultIntInfo:[I

    .line 397
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask$1;->this$0:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    # getter for: Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->m_listener:Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;
    invoke-static {v1}, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->access$0(Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;)Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 399
    iget-object v1, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask$1;->this$0:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    # getter for: Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->m_listener:Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;
    invoke-static {v1}, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->access$0(Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;)Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask$1;->this$0:Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;

    iget-object v2, v2, Lcom/sec/android/app/SoundRecognizer/SoundRecordTask;->resultInfo:[I

    invoke-interface {v1, v0, v2}, Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;->onResults(I[I)V

    .line 401
    :cond_0
    return-void
.end method
