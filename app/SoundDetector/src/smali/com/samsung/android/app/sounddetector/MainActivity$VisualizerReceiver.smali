.class Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/sounddetector/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VisualizerReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/MainActivity;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/sounddetector/MainActivity;)V
    .locals 0

    .prologue
    .line 452
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/sounddetector/MainActivity;Lcom/samsung/android/app/sounddetector/MainActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/app/sounddetector/MainActivity;
    .param p2, "x1"    # Lcom/samsung/android/app/sounddetector/MainActivity$1;

    .prologue
    .line 452
    invoke-direct {p0, p1}, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;-><init>(Lcom/samsung/android/app/sounddetector/MainActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v6, 0x2e

    const/16 v5, 0x24

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 455
    const-string v1, "soundLevel"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 457
    .local v0, "soundLevel":I
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "visualization_detecting"

    if-ne v1, v2, :cond_3

    .line 458
    if-ge v0, v5, :cond_1

    .line 459
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_1:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$300(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 460
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_2:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$400(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 461
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_3:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$500(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 480
    :cond_0
    :goto_0
    return-void

    .line 462
    :cond_1
    if-gt v5, v0, :cond_2

    if-ge v0, v6, :cond_2

    .line 463
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_1:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$300(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 464
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_2:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$400(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 465
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_3:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$500(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 466
    :cond_2
    if-gt v6, v0, :cond_0

    .line 467
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_1:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$300(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 468
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_2:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$400(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 469
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_3:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$500(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 471
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "stop_sounddetect_service"

    if-ne v1, v2, :cond_0

    .line 472
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$600(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 473
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$600(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 474
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorPause_btn:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$700(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 475
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorIndication:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$800(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 476
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorDetecting:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$900(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 477
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/MainActivity;->main_layout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$1000(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    const-string v2, "#ffffff"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 478
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/MainActivity;->main_detect_layout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$1100(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto/16 :goto_0
.end method
