.class Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$1;
.super Ljava/lang/Object;
.source "RecentAlertActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$1;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$1;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;
    invoke-static {v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$000(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    move-result-object v0

    if-nez v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$1;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    new-instance v1, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$1;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;-><init>(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$1;)V

    # setter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;
    invoke-static {v0, v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$002(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;)Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    .line 100
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$1;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;
    invoke-static {v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$000(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->setModeFrom(I)V

    .line 101
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$1;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    invoke-virtual {v0, p3}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->onListItemSelect(I)V

    .line 102
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$1;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$1;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$000(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    .line 107
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$1;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    invoke-virtual {v0, p3}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->onListItemSelect(I)V

    .line 105
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$1;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;
    invoke-static {v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$000(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->notifyChanged()V

    goto :goto_0
.end method
