.class Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AlertPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/sounddetector/alert/AlertPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VisualizerReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)V
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;Lcom/samsung/android/app/sounddetector/alert/AlertPopup$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/app/sounddetector/alert/AlertPopup;
    .param p2, "x1"    # Lcom/samsung/android/app/sounddetector/alert/AlertPopup$1;

    .prologue
    .line 390
    invoke-direct {p0, p1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;-><init>(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v6, 0x2e

    const/16 v5, 0x24

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 393
    const-string v1, "soundLevel"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 395
    .local v0, "soundLevel":I
    const-string v1, "AlertPopup"

    const-string v2, "onReceive Alert visualizer"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "alert_visualization_baby_crying"

    if-ne v1, v2, :cond_0

    .line 397
    if-ge v0, v5, :cond_1

    .line 398
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    # getter for: Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_1:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->access$500(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 399
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    # getter for: Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_2:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->access$600(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 400
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    # getter for: Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_3:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->access$700(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 411
    :cond_0
    :goto_0
    return-void

    .line 401
    :cond_1
    if-gt v5, v0, :cond_2

    if-ge v0, v6, :cond_2

    .line 402
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    # getter for: Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_1:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->access$500(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 403
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    # getter for: Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_2:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->access$600(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 404
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    # getter for: Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_3:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->access$700(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 405
    :cond_2
    if-gt v6, v0, :cond_0

    .line 406
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    # getter for: Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_1:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->access$500(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 407
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    # getter for: Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_2:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->access$600(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 408
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    # getter for: Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_3:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->access$700(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
