.class Lcom/samsung/android/app/sounddetector/MainActivity$2;
.super Landroid/database/ContentObserver;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/sounddetector/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/MainActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/sounddetector/MainActivity;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$2;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 13
    .param p1, "selfChange"    # Z

    .prologue
    .line 91
    iget-object v10, p0, Lcom/samsung/android/app/sounddetector/MainActivity$2;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    invoke-virtual {v10}, Lcom/samsung/android/app/sounddetector/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "accessibility_enabled"

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_0

    const/4 v0, 0x1

    .line 93
    .local v0, "accessibilityEnabled":Z
    :goto_0
    iget-object v10, p0, Lcom/samsung/android/app/sounddetector/MainActivity$2;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    const-string v11, "accessibility"

    invoke-virtual {v10, v11}, Lcom/samsung/android/app/sounddetector/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityManager;

    .line 94
    .local v1, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstalledAccessibilityServiceList()Ljava/util/List;

    move-result-object v7

    .line 96
    .local v7, "installedServices":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    .local v3, "count":I
    :goto_1
    if-ge v5, v3, :cond_2

    .line 97
    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/accessibilityservice/AccessibilityServiceInfo;

    .line 98
    .local v6, "info":Landroid/accessibilityservice/AccessibilityServiceInfo;
    iget-object v10, p0, Lcom/samsung/android/app/sounddetector/MainActivity$2;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    invoke-static {v10}, Lcom/samsung/android/app/sounddetector/Utils;->getEnabledServicesFromSettings(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v4

    .line 99
    .local v4, "enabledServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    invoke-virtual {v6}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v10

    iget-object v9, v10, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 100
    .local v9, "serviceInfo":Landroid/content/pm/ServiceInfo;
    new-instance v2, Landroid/content/ComponentName;

    iget-object v10, v9, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v11, v9, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v10, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .local v2, "componentName":Landroid/content/ComponentName;
    if-eqz v0, :cond_1

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v8, 0x1

    .line 102
    .local v8, "serviceEnabled":Z
    :goto_2
    const-string v10, "MainActivity"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Talkback observer value is : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v10, p0, Lcom/samsung/android/app/sounddetector/MainActivity$2;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    invoke-virtual {v10, v8}, Lcom/samsung/android/app/sounddetector/MainActivity;->updateMoreOptions(Z)V

    .line 96
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 91
    .end local v0    # "accessibilityEnabled":Z
    .end local v1    # "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    .end local v2    # "componentName":Landroid/content/ComponentName;
    .end local v3    # "count":I
    .end local v4    # "enabledServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    .end local v5    # "i":I
    .end local v6    # "info":Landroid/accessibilityservice/AccessibilityServiceInfo;
    .end local v7    # "installedServices":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    .end local v8    # "serviceEnabled":Z
    .end local v9    # "serviceInfo":Landroid/content/pm/ServiceInfo;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 101
    .restart local v0    # "accessibilityEnabled":Z
    .restart local v1    # "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    .restart local v2    # "componentName":Landroid/content/ComponentName;
    .restart local v3    # "count":I
    .restart local v4    # "enabledServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    .restart local v5    # "i":I
    .restart local v6    # "info":Landroid/accessibilityservice/AccessibilityServiceInfo;
    .restart local v7    # "installedServices":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    .restart local v9    # "serviceInfo":Landroid/content/pm/ServiceInfo;
    :cond_1
    const/4 v8, 0x0

    goto :goto_2

    .line 105
    .end local v2    # "componentName":Landroid/content/ComponentName;
    .end local v4    # "enabledServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    .end local v6    # "info":Landroid/accessibilityservice/AccessibilityServiceInfo;
    .end local v9    # "serviceInfo":Landroid/content/pm/ServiceInfo;
    :cond_2
    return-void
.end method
