.class public Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;
.super Ljava/lang/Object;
.source "RecentAlertDB.java"


# instance fields
.field alertDate:Ljava/lang/String;

.field alertTime:Ljava/lang/String;

.field id:I

.field soundType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "type"    # I
    .param p3, "time"    # Ljava/lang/String;
    .param p4, "date"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->id:I

    .line 14
    iput p2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->soundType:I

    .line 15
    iput-object p3, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->alertTime:Ljava/lang/String;

    .line 16
    iput-object p4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->alertDate:Ljava/lang/String;

    .line 17
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "time"    # Ljava/lang/String;
    .param p3, "date"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->soundType:I

    .line 21
    iput-object p2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->alertTime:Ljava/lang/String;

    .line 22
    iput-object p3, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->alertDate:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public getAlertDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->alertDate:Ljava/lang/String;

    return-object v0
.end method

.method public getAlertTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->alertTime:Ljava/lang/String;

    return-object v0
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->id:I

    return v0
.end method

.method public getSoundType()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->soundType:I

    return v0
.end method

.method public setAlertDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->alertDate:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public setAlertTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->alertTime:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setID(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->id:I

    .line 31
    return-void
.end method

.method public setSoundType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->soundType:I

    .line 39
    return-void
.end method
