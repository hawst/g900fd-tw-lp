.class public Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;
.super Landroid/preference/SeekBarDialogPreference;
.source "VibrationIntensityPreference.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# static fields
.field private static final MAXIMUM_VIBRATION_INTENSITY:I = 0x5

.field private static final SEEKBAR_ID:[I

.field private static final TAG:Ljava/lang/String; = "VibrationFeedbackPreference"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mNotificationSeekBar:Landroid/widget/SeekBar;

.field private mNotificationVibrationIntensity:I

.field private mOldNotificationVibrationSlideLevel:I

.field mVibrator:Landroid/os/SystemVibrator;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f09002b

    aput v2, v0, v1

    sput-object v0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->SEEKBAR_ID:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/preference/SeekBarDialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    new-instance v0, Landroid/os/SystemVibrator;

    invoke-direct {v0}, Landroid/os/SystemVibrator;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mVibrator:Landroid/os/SystemVibrator;

    .line 35
    const v0, 0x7f030007

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->setDialogLayoutResource(I)V

    .line 36
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mContext:Landroid/content/Context;

    .line 37
    return-void
.end method

.method private StringToLongArray(Ljava/lang/String;)[J
    .locals 8
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 130
    if-nez p1, :cond_1

    move-object v2, v4

    .line 147
    :cond_0
    :goto_0
    return-object v2

    .line 133
    :cond_1
    const-string v5, ", "

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 135
    .local v3, "temp":[Ljava/lang/String;
    array-length v5, v3

    if-gtz v5, :cond_2

    move-object v2, v4

    .line 136
    goto :goto_0

    .line 139
    :cond_2
    array-length v5, v3

    new-array v2, v5, [J

    .line 141
    .local v2, "ret":[J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    :try_start_0
    array-length v5, v3

    if-ge v1, v5, :cond_0

    .line 142
    aget-object v5, v3, v1

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v2, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 141
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 144
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    move-object v2, v4

    .line 145
    goto :goto_0
.end method

.method private setNotificationVibrationIntensity(I)V
    .locals 3
    .param p1, "vibrationIntensityLevel"    # I

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "VIB_NOTIFICATION_MAGNITUDE"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 123
    const-string v0, "VibrationFeedbackPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setNotificationVibrationIntensity : Settings.System.VIB_NOTIFICATION_MAGNITUDE["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    return-void
.end method


# virtual methods
.method protected onBindDialogView(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x1

    .line 43
    sget-object v0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->SEEKBAR_ID:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mNotificationSeekBar:Landroid/widget/SeekBar;

    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mNotificationSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setSoundEffectsEnabled(Z)V

    .line 46
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mNotificationSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 47
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "VIB_NOTIFICATION_MAGNITUDE"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mOldNotificationVibrationSlideLevel:I

    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mNotificationSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mOldNotificationVibrationSlideLevel:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mNotificationSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 55
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 56
    invoke-virtual {p1, v2}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 57
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 58
    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 3
    .param p1, "positiveResult"    # Z

    .prologue
    .line 102
    invoke-super {p0, p1}, Landroid/preference/SeekBarDialogPreference;->onDialogClosed(Z)V

    .line 103
    if-eqz p1, :cond_0

    .line 104
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mNotificationSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mNotificationVibrationIntensity:I

    .line 106
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "VIB_NOTIFICATION_MAGNITUDE"

    iget v2, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mNotificationVibrationIntensity:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 109
    const-string v0, "VibrationFeedbackPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDialogClosed : Settings.System.VIB_NOTIFICATION_MAGNITUDE["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mNotificationVibrationIntensity:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v0}, Landroid/os/SystemVibrator;->cancel()V

    .line 117
    return-void

    .line 113
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mOldNotificationVibrationSlideLevel:I

    invoke-direct {p0, v0}, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->setNotificationVibrationIntensity(I)V

    goto :goto_0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    .line 151
    const/4 v0, 0x0

    return v0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 10
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromTouch"    # Z

    .prologue
    const/4 v2, 0x0

    .line 63
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mNotificationSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 64
    invoke-direct {p0, p2}, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->setNotificationVibrationIntensity(I)V

    .line 65
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "sound_detector_vibration_pattern"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 67
    .local v9, "temp":Ljava/lang/String;
    if-nez v9, :cond_0

    .line 68
    const-string v9, "content://com.android.settings.personalvibration.PersonalVibrationProvider/1"

    .line 71
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 73
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 74
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    const-string v0, "vibration_pattern"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 77
    .local v8, "patternStr":Ljava/lang/String;
    invoke-direct {p0, v8}, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->StringToLongArray(Ljava/lang/String;)[J

    move-result-object v7

    .line 78
    .local v7, "pattern":[J
    if-eqz v7, :cond_1

    .line 79
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/setting/VibrationIntensityPreference;->mVibrator:Landroid/os/SystemVibrator;

    const/4 v1, -0x1

    sget-object v2, Landroid/os/SystemVibrator$MagnitudeType;->NotificationMagnitude:Landroid/os/SystemVibrator$MagnitudeType;

    invoke-virtual {v0, v7, v1, v2}, Landroid/os/SystemVibrator;->vibrate([JILandroid/os/SystemVibrator$MagnitudeType;)V

    .line 85
    .end local v7    # "pattern":[J
    .end local v8    # "patternStr":Ljava/lang/String;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 88
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v9    # "temp":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 93
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 98
    return-void
.end method
