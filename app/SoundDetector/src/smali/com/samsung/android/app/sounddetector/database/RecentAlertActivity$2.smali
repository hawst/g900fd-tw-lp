.class Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$2;
.super Ljava/lang/Object;
.source "RecentAlertActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->ShowDialog(Landroid/util/SparseBooleanArray;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)V
    .locals 0

    .prologue
    .line 341
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$2;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 344
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$2;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->isClickUndoBtn:Z

    .line 345
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$2;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$2;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->getDataFromDataBase()Ljava/util/ArrayList;

    move-result-object v1

    # setter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertDataList:Ljava/util/ArrayList;
    invoke-static {v0, v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$202(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 346
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$2;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    new-instance v1, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$2;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$400(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$2;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertDataList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$200(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;I)V

    # setter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    invoke-static {v0, v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$302(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    .line 347
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$2;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$500(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$2;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$300(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 349
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$2;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mUndoPopup:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$600(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 350
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$2;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    invoke-virtual {v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->updateScreen()V

    .line 351
    return-void
.end method
