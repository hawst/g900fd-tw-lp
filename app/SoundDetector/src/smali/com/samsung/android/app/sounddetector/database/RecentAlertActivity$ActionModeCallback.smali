.class Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;
.super Ljava/lang/Object;
.source "RecentAlertActivity.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionModeCallback"
.end annotation


# static fields
.field public static final MODE_FROM_LONG_CLICK:I = 0x0

.field public static final MODE_FROM_MENU:I = 0x1


# instance fields
.field public deleteIcon:Landroid/widget/ImageButton;

.field public done:Landroid/widget/Button;

.field isSelectAllCheck:Z

.field private mActionModeFrom:I

.field private mDone:Landroid/view/MenuItem;

.field private mMode:Landroid/view/ActionMode;

.field private mSelectButton:Landroid/widget/CheckBox;

.field private mSelectPopup:Landroid/widget/ListPopupWindow;

.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

.field public viewicon:Landroid/view/View;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)V
    .locals 1

    .prologue
    .line 468
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 485
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->isSelectAllCheck:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;
    .param p2, "x1"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$1;

    .prologue
    .line 468
    invoke-direct {p0, p1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;-><init>(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;
    .param p1, "x1"    # Z

    .prologue
    .line 468
    invoke-direct {p0, p1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->toggleAllcheck(Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    .prologue
    .line 468
    invoke-direct {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->deleteSelectedItems()V

    return-void
.end method

.method private deleteSelectedItems()V
    .locals 2

    .prologue
    .line 693
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$300(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->getSelectedIds()Landroid/util/SparseBooleanArray;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->ShowDialog(Landroid/util/SparseBooleanArray;)V

    .line 694
    invoke-direct {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->stopActionMode()V

    .line 695
    return-void
.end method

.method private getCheckedCount()I
    .locals 2

    .prologue
    .line 628
    const/4 v0, 0x0

    .line 629
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$300(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->getSelectedCount()I

    move-result v0

    .line 630
    return v0
.end method

.method private stopActionMode()V
    .locals 1

    .prologue
    .line 680
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 681
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 683
    :cond_0
    return-void
.end method

.method private toggleAllcheck(Z)V
    .locals 4
    .param p1, "checked"    # Z

    .prologue
    .line 670
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->getDataFromDataBase()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 671
    .local v0, "mAllItemSize":I
    const-string v1, "RecentAlertActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AllItemSize : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    if-eqz p1, :cond_0

    .line 673
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$300(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->selectionAll(I)V

    .line 677
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->notifyChanged()V

    .line 678
    return-void

    .line 675
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$300(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->deSelectionAll(I)V

    goto :goto_0
.end method


# virtual methods
.method public getActionMode()Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 641
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method public getActionModeFrom()I
    .locals 1

    .prologue
    .line 637
    iget v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mActionModeFrom:I

    return v0
.end method

.method public notifyChanged()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 590
    invoke-direct {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->getCheckedCount()I

    move-result v0

    .line 591
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->done:Landroid/widget/Button;

    if-eqz v2, :cond_0

    .line 592
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->done:Landroid/widget/Button;

    if-nez v0, :cond_3

    move v2, v3

    :goto_0
    invoke-virtual {v5, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 595
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->deleteIcon:Landroid/widget/ImageButton;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->viewicon:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 596
    if-nez v0, :cond_4

    .line 597
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->deleteIcon:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 598
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->viewicon:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 604
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mDone:Landroid/view/MenuItem;

    if-eqz v2, :cond_2

    .line 605
    if-nez v0, :cond_5

    .line 606
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mDone:Landroid/view/MenuItem;

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 612
    :cond_2
    :goto_2
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    invoke-virtual {v2}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f060013

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 613
    .local v1, "format":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " ("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 614
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mSelectButton:Landroid/widget/CheckBox;

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 616
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$700(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->getRecentAlertCount()I

    move-result v2

    invoke-direct {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->getCheckedCount()I

    move-result v5

    if-ne v2, v5, :cond_6

    .line 617
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mSelectButton:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 618
    iput-boolean v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->isSelectAllCheck:Z

    .line 626
    :goto_3
    return-void

    .end local v1    # "format":Ljava/lang/String;
    :cond_3
    move v2, v4

    .line 592
    goto :goto_0

    .line 600
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->deleteIcon:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 601
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->viewicon:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 608
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mDone:Landroid/view/MenuItem;

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2

    .line 619
    .restart local v1    # "format":Ljava/lang/String;
    :cond_6
    invoke-direct {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->getCheckedCount()I

    move-result v2

    if-lez v2, :cond_7

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$700(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->getRecentAlertCount()I

    move-result v2

    invoke-direct {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->getCheckedCount()I

    move-result v4

    if-le v2, v4, :cond_7

    .line 620
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mSelectButton:Landroid/widget/CheckBox;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 621
    iput-boolean v3, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->isSelectAllCheck:Z

    goto :goto_3

    .line 623
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mSelectButton:Landroid/widget/CheckBox;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 624
    iput-boolean v3, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->isSelectAllCheck:Z

    goto :goto_3
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 658
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 665
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 660
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$300(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->getSelectedIds()Landroid/util/SparseBooleanArray;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->ShowDialog(Landroid/util/SparseBooleanArray;)V

    .line 661
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    .line 663
    const/4 v0, 0x1

    goto :goto_0

    .line 658
    nop

    :pswitch_data_0
    .packed-switch 0x7f090035
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 12
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const v11, 0x7f090007

    const v10, 0x7f090006

    const v9, 0x7f090005

    const/4 v8, 0x1

    const/16 v7, 0x8

    .line 490
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 491
    .local v3, "mSelect":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mMode:Landroid/view/ActionMode;

    .line 493
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    .line 494
    .local v1, "inflater":Landroid/view/MenuInflater;
    const/high16 v4, 0x7f080000

    invoke-virtual {v1, v4, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 496
    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    invoke-virtual {v4}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030001

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 499
    .local v0, "customView":Landroid/view/View;
    const v4, 0x7f090003

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mSelectButton:Landroid/widget/CheckBox;

    .line 501
    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mSelectButton:Landroid/widget/CheckBox;

    new-instance v5, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback$1;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback$1;-><init>(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;)V

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 514
    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;
    invoke-static {v4}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$000(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->getActionModeFrom()I

    move-result v4

    if-nez v4, :cond_0

    .line 515
    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 517
    .local v2, "layout":Landroid/widget/LinearLayout;
    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 518
    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->viewicon:Landroid/view/View;

    .line 519
    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->deleteIcon:Landroid/widget/ImageButton;

    .line 520
    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->deleteIcon:Landroid/widget/ImageButton;

    new-instance v5, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback$2;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback$2;-><init>(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 526
    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->viewicon:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 527
    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->deleteIcon:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 582
    .end local v2    # "layout":Landroid/widget/LinearLayout;
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    .line 583
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->notifyChanged()V

    .line 584
    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    invoke-static {v4}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$300(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    move-result-object v4

    invoke-virtual {v4, v8}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->setMode(I)V

    .line 585
    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    invoke-static {v4}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$300(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->notifyDataSetChanged()V

    .line 586
    return v8

    .line 546
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;
    invoke-static {v4}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$000(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->getActionModeFrom()I

    move-result v4

    if-ne v4, v8, :cond_1

    .line 547
    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 549
    .restart local v2    # "layout":Landroid/widget/LinearLayout;
    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 550
    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->viewicon:Landroid/view/View;

    .line 551
    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->deleteIcon:Landroid/widget/ImageButton;

    .line 552
    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->deleteIcon:Landroid/widget/ImageButton;

    new-instance v5, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback$3;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback$3;-><init>(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 558
    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->viewicon:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 559
    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->deleteIcon:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 579
    .end local v2    # "layout":Landroid/widget/LinearLayout;
    :cond_1
    const-string v4, "RecentAlertActivity"

    const-string v5, "unexpected routine"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    .line 686
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    invoke-static {v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$300(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->removeSelection()V

    .line 687
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    invoke-static {v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$300(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->setMode(I)V

    .line 688
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    invoke-static {v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$300(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->notifyDataSetChanged()V

    .line 689
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mSelectAllLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$1000(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 690
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;
    invoke-static {v0, v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$002(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;)Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    .line 691
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 4
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x0

    .line 646
    invoke-interface {p2, v3}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mDone:Landroid/view/MenuItem;

    .line 647
    invoke-direct {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->getCheckedCount()I

    move-result v0

    .line 648
    .local v0, "count":I
    if-nez v0, :cond_0

    .line 649
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mDone:Landroid/view/MenuItem;

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 653
    :goto_0
    return v3

    .line 651
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mDone:Landroid/view/MenuItem;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public setModeFrom(I)V
    .locals 0
    .param p1, "from"    # I

    .prologue
    .line 633
    iput p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->mActionModeFrom:I

    .line 634
    return-void
.end method
