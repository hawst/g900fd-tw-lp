.class Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$3;
.super Ljava/lang/Object;
.source "SoundDetectorEnabler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->ShowDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;

.field final synthetic val$do_not_show:Landroid/widget/CheckBox;

.field final synthetic val$edit:Landroid/content/SharedPreferences$Editor;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;Landroid/content/SharedPreferences$Editor;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$3;->this$0:Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;

    iput-object p2, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$3;->val$edit:Landroid/content/SharedPreferences$Editor;

    iput-object p3, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$3;->val$do_not_show:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 133
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$3;->val$edit:Landroid/content/SharedPreferences$Editor;

    const-string v2, "pref_do_not_show_again"

    iget-object v3, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$3;->val$do_not_show:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 134
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$3;->val$edit:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 135
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 137
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$3;->this$0:Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;

    # getter for: Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->access$100(Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "sound_detector"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 139
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.settings.action.sound_detector"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 140
    .local v0, "mIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$3;->this$0:Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;

    # getter for: Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->access$100(Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 141
    return-void
.end method
