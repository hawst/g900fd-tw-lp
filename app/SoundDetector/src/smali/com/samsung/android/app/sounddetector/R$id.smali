.class public final Lcom/samsung/android/app/sounddetector/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/sounddetector/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_delete:I = 0x7f090039

.field public static final action_recent_alert:I = 0x7f090036

.field public static final action_settings:I = 0x7f090037

.field public static final actionbar_cancel:I = 0x7f090008

.field public static final actionbar_delete_image_layout:I = 0x7f090004

.field public static final actionbar_delete_text_layout:I = 0x7f090007

.field public static final actionbar_done:I = 0x7f09000a

.field public static final actionbar_done_layout:I = 0x7f090009

.field public static final alert_bg:I = 0x7f09000b

.field public static final alert_detect_layout:I = 0x7f09000c

.field public static final alert_detect_level_1:I = 0x7f09000d

.field public static final alert_detect_level_2:I = 0x7f09000e

.field public static final alert_detect_level_3:I = 0x7f09000f

.field public static final alert_pause_btn:I = 0x7f090010

.field public static final alert_sound_detecting:I = 0x7f090011

.field public static final cover_alert_detect_layout:I = 0x7f090023

.field public static final cover_alert_detect_level_1:I = 0x7f090024

.field public static final cover_alert_detect_level_2:I = 0x7f090025

.field public static final cover_alert_detect_level_3:I = 0x7f090026

.field public static final cover_alert_pause_btn:I = 0x7f090027

.field public static final cover_alert_sound_detecting:I = 0x7f090028

.field public static final cover_color_bg:I = 0x7f090021

.field public static final cover_main_bg:I = 0x7f090022

.field public static final delete:I = 0x7f090035

.field public static final delete_icon:I = 0x7f090006

.field public static final disclaimer_text:I = 0x7f090029

.field public static final list_recent_alert_checkbox:I = 0x7f09002c

.field public static final list_recent_alert_header:I = 0x7f09002f

.field public static final list_recent_alert_time:I = 0x7f09002e

.field public static final list_recent_alert_title:I = 0x7f09002d

.field public static final main_app_description:I = 0x7f090019

.field public static final main_detect_layout:I = 0x7f090014

.field public static final main_detect_level_1:I = 0x7f090015

.field public static final main_detect_level_2:I = 0x7f090016

.field public static final main_detect_level_3:I = 0x7f090017

.field public static final main_layout:I = 0x7f090012

.field public static final main_pause_btn:I = 0x7f090018

.field public static final main_sound_detecting:I = 0x7f09001a

.field public static final main_state_btn:I = 0x7f090013

.field public static final navigate_up_layout:I = 0x7f090000

.field public static final navigation_bar:I = 0x7f090002

.field public static final never_show_dialog_again:I = 0x7f09002a

.field public static final notification_vibration_seekbar:I = 0x7f09002b

.field public static final recent_alert_listView:I = 0x7f09001e

.field public static final recent_no_alert_layout:I = 0x7f09001f

.field public static final recent_no_alert_text:I = 0x7f090020

.field public static final select_all:I = 0x7f090038

.field public static final select_all_checkbox:I = 0x7f09001c

.field public static final select_all_container:I = 0x7f09001b

.field public static final select_all_text:I = 0x7f09001d

.field public static final selected_text:I = 0x7f090001

.field public static final selection_menu:I = 0x7f090003

.field public static final separator:I = 0x7f090033

.field public static final undo_area:I = 0x7f090032

.field public static final undo_button_icon:I = 0x7f090034

.field public static final undo_container:I = 0x7f090030

.field public static final undo_popup_delete_text:I = 0x7f090031

.field public static final viewicon:I = 0x7f090005


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
