.class public Lcom/samsung/android/app/sounddetector/alert/AlertPopup;
.super Landroid/app/Activity;
.source "AlertPopup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;,
        Lcom/samsung/android/app/sounddetector/alert/AlertPopup$MyTimer;
    }
.end annotation


# static fields
.field private static final NOTIFLASH_LENGTH:I = 0x7d0

.field private static final SCOVER_WALLPAER_MODE_ON:I = 0x1

.field private static final START_SOUND_DETECT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "AlertPopup"

.field private static mNotificationFlashThread:Ljava/lang/Thread;


# instance fields
.field private WALLPAPER_IMAGE_NAME:Ljava/lang/String;

.field private cfmsService:Landroid/os/CustomFrequencyManager;

.field private flashNotification_state:I

.field private isRunning:Z

.field private mAlertDetectorLevel_1:Landroid/widget/ImageView;

.field private mAlertDetectorLevel_2:Landroid/widget/ImageView;

.field private mAlertDetectorLevel_3:Landroid/widget/ImageView;

.field private mAlertPause_btn:Landroid/widget/ImageButton;

.field private mContext:Landroid/content/Context;

.field private mCoverBgDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field private mCoverColorBG:Landroid/view/View;

.field private mCoverMainBG:Landroid/view/View;

.field private mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private mCoverOpen:Z

.field private mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field private mCoverWallPaper:I

.field private final mHandler:Landroid/os/Handler;

.field private mKey:Lcom/samsung/android/app/sounddetector/SoundDetectorKeyDispatcher;

.field private mScover:Lcom/samsung/android/sdk/cover/Scover;

.field mServiceConnection:Landroid/content/ServiceConnection;

.field private mSoundDetectService:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

.field private mVibrator:Landroid/os/SystemVibrator;

.field private mVisualizerReceiver:Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;

.field private timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 54
    const-string v0, "cover_wallpaper.jpg"

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->WALLPAPER_IMAGE_NAME:Ljava/lang/String;

    .line 55
    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverBgDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 81
    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->cfmsService:Landroid/os/CustomFrequencyManager;

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverOpen:Z

    .line 91
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverWallPaper:I

    .line 93
    new-instance v0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$1;-><init>(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 414
    new-instance v0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$3;-><init>(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private StringToLongArray(Ljava/lang/String;)[J
    .locals 8
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 366
    if-nez p1, :cond_1

    move-object v2, v4

    .line 387
    :cond_0
    :goto_0
    return-object v2

    .line 369
    :cond_1
    const-string v5, ", "

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 372
    .local v3, "temp":[Ljava/lang/String;
    array-length v5, v3

    if-gtz v5, :cond_2

    move-object v2, v4

    .line 374
    goto :goto_0

    .line 377
    :cond_2
    array-length v5, v3

    new-array v2, v5, [J

    .line 379
    .local v2, "ret":[J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    :try_start_0
    array-length v5, v3

    if-ge v1, v5, :cond_0

    .line 380
    aget-object v5, v3, v1

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v2, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 383
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    move-object v2, v4

    .line 385
    goto :goto_0
.end method

.method private TimerForAlert()V
    .locals 4

    .prologue
    .line 199
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->timer:Ljava/util/Timer;

    .line 200
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$MyTimer;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$MyTimer;-><init>(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;Lcom/samsung/android/app/sounddetector/alert/AlertPopup$1;)V

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 201
    return-void
.end method

.method private VibrationForSoundDetect()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 339
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "sound_detector_vibration_pattern"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 340
    .local v7, "mCurrentVibration":Ljava/lang/String;
    if-nez v7, :cond_0

    .line 341
    const-string v7, "content://com.android.settings.personalvibration.PersonalVibrationProvider/1"

    .line 342
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "sound_detector_vibration_pattern"

    const-string v3, "content://com.android.settings.personalvibration.PersonalVibrationProvider/1"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mVibrator:Landroid/os/SystemVibrator;

    if-eqz v0, :cond_1

    .line 347
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v0}, Landroid/os/SystemVibrator;->cancel()V

    .line 348
    iput-object v2, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mVibrator:Landroid/os/SystemVibrator;

    .line 351
    :cond_1
    new-instance v0, Landroid/os/SystemVibrator;

    invoke-direct {v0}, Landroid/os/SystemVibrator;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mVibrator:Landroid/os/SystemVibrator;

    .line 352
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 354
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_3

    .line 355
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 356
    const-string v0, "vibration_pattern"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 357
    .local v9, "patternStr":Ljava/lang/String;
    invoke-direct {p0, v9}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->StringToLongArray(Ljava/lang/String;)[J

    move-result-object v8

    .line 358
    .local v8, "pattern":[J
    if-eqz v8, :cond_2

    .line 359
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mVibrator:Landroid/os/SystemVibrator;

    const/4 v1, 0x0

    sget-object v2, Landroid/os/SystemVibrator$MagnitudeType;->NotificationMagnitude:Landroid/os/SystemVibrator$MagnitudeType;

    invoke-virtual {v0, v8, v1, v2}, Landroid/os/SystemVibrator;->vibrate([JILandroid/os/SystemVibrator$MagnitudeType;)V

    .line 361
    .end local v8    # "pattern":[J
    .end local v9    # "patternStr":Ljava/lang/String;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 363
    :cond_3
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Lcom/samsung/android/app/sounddetector/service/SoundDetectService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mSoundDetectService:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Lcom/samsung/android/app/sounddetector/service/SoundDetectService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/alert/AlertPopup;
    .param p1, "x1"    # Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mSoundDetectService:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_3:Landroid/widget/ImageView;

    return-object v0
.end method

.method private createBackgroundDrawable()V
    .locals 4

    .prologue
    .line 476
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "sview_color_wallpaper"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverWallPaper:I

    .line 478
    iget v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverWallPaper:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 479
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "sview_bg_wallpaper_path"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 481
    .local v0, "filePath":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 482
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->WALLPAPER_IMAGE_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 484
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 485
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverBgDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 488
    .end local v0    # "filePath":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private getBackgroundColor()I
    .locals 7

    .prologue
    .line 491
    const/4 v0, 0x0

    .line 492
    .local v0, "color":I
    const/4 v1, 0x0

    .line 494
    .local v1, "tempColor":I
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "s_vew_cover_background_color"

    const/16 v4, 0x8

    const/16 v5, 0x6b

    const/16 v6, 0x77

    invoke-static {v4, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 496
    const/16 v2, 0xef

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v3

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v4

    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 498
    return v0
.end method

.method private recycleView(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 502
    if-eqz p1, :cond_0

    .line 503
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 504
    .local v0, "bg":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 505
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 506
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "bg":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 507
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 510
    :cond_0
    return-void
.end method

.method private setupAlertView()V
    .locals 5

    .prologue
    const/16 v3, 0x400

    const/4 v2, 0x1

    .line 429
    iget-boolean v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverOpen:Z

    if-eqz v0, :cond_1

    .line 430
    const-string v0, "AlertPopup"

    const-string v1, "setupCoverViews bCoverOpen"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->requestWindowFeature(I)Z

    .line 433
    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->setContentView(I)V

    .line 435
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 436
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x680080

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 438
    const v0, 0x7f090010

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertPause_btn:Landroid/widget/ImageButton;

    .line 439
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertPause_btn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 441
    const v0, 0x7f09000d

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_1:Landroid/widget/ImageView;

    .line 442
    const v0, 0x7f09000e

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_2:Landroid/widget/ImageView;

    .line 443
    const v0, 0x7f09000f

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_3:Landroid/widget/ImageView;

    .line 473
    :cond_0
    :goto_0
    return-void

    .line 445
    :cond_1
    const-string v0, "AlertPopup"

    const-string v1, "setupCoverViews() bCoverClose"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->setRequestedOrientation(I)V

    .line 448
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->requestWindowFeature(I)Z

    .line 449
    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->setContentView(I)V

    .line 450
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x4280080

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 452
    const v0, 0x7f090027

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertPause_btn:Landroid/widget/ImageButton;

    .line 453
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertPause_btn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 455
    const v0, 0x7f090024

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_1:Landroid/widget/ImageView;

    .line 456
    const v0, 0x7f090025

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_2:Landroid/widget/ImageView;

    .line 457
    const v0, 0x7f090026

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_3:Landroid/widget/ImageView;

    .line 459
    const v0, 0x7f090021

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverColorBG:Landroid/view/View;

    .line 460
    const v0, 0x7f090022

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverMainBG:Landroid/view/View;

    .line 462
    invoke-direct {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->createBackgroundDrawable()V

    .line 464
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverColorBG:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 465
    iget v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverWallPaper:I

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverBgDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_2

    .line 466
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverMainBG:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverBgDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 468
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverMainBG:Landroid/view/View;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02001a

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 469
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverColorBG:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getBackgroundColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 313
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 336
    :goto_0
    return-void

    .line 315
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->finish()V

    .line 316
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mContext:Landroid/content/Context;

    const-class v4, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->stopService(Landroid/content/Intent;)Z

    .line 318
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.settings.action.sound_detector.stop"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 319
    .local v0, "mIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->sendBroadcast(Landroid/content/Intent;)V

    .line 321
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.accessibility.SOUND_DETECTOR"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 322
    .local v1, "popupIntent":Landroid/content/Intent;
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 323
    const/high16 v2, 0x20000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 324
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 327
    .end local v0    # "mIntent":Landroid/content/Intent;
    .end local v1    # "popupIntent":Landroid/content/Intent;
    :sswitch_1
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->finish()V

    .line 328
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mContext:Landroid/content/Context;

    const-class v4, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->stopService(Landroid/content/Intent;)Z

    .line 330
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.settings.action.sound_detector.stop"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 331
    .restart local v0    # "mIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 313
    :sswitch_data_0
    .sparse-switch
        0x7f090010 -> :sswitch_0
        0x7f090027 -> :sswitch_1
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 204
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 205
    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->setContentView(I)V

    .line 207
    const v0, 0x7f090010

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertPause_btn:Landroid/widget/ImageButton;

    .line 208
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertPause_btn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    const v0, 0x7f09000d

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_1:Landroid/widget/ImageView;

    .line 211
    const v0, 0x7f09000e

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_2:Landroid/widget/ImageView;

    .line 212
    const v0, 0x7f09000f

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mAlertDetectorLevel_3:Landroid/widget/ImageView;

    .line 213
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 110
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 111
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mContext:Landroid/content/Context;

    .line 113
    new-instance v4, Lcom/samsung/android/sdk/cover/Scover;

    invoke-direct {v4}, Lcom/samsung/android/sdk/cover/Scover;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    .line 116
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/cover/Scover;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/SsdkUnsupportedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 123
    :goto_0
    new-instance v4, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 125
    new-instance v4, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$2;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$2;-><init>(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)V

    iput-object v4, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 152
    new-instance v3, Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-direct {v3}, Lcom/samsung/android/sdk/cover/ScoverState;-><init>()V

    .line 153
    .local v3, "mScoverState":Lcom/samsung/android/sdk/cover/ScoverState;
    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v3

    .line 155
    if-eqz v3, :cond_0

    .line 156
    invoke-virtual {v3}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v4

    if-ne v4, v7, :cond_2

    .line 157
    iput-boolean v7, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverOpen:Z

    .line 161
    :goto_1
    const-string v4, "AlertPopup"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "..mCoverOpen.. = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverOpen:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->setupAlertView()V

    .line 166
    const-string v4, "CustomFrequencyManagerService"

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/CustomFrequencyManager;

    iput-object v4, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->cfmsService:Landroid/os/CustomFrequencyManager;

    .line 167
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "sound_detector_flash_noti"

    invoke-static {v4, v5, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->flashNotification_state:I

    .line 169
    iget v4, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->flashNotification_state:I

    if-ne v4, v7, :cond_1

    .line 170
    iput-boolean v7, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->isRunning:Z

    .line 171
    new-instance v4, Ljava/lang/Thread;

    invoke-direct {v4, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    sput-object v4, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mNotificationFlashThread:Ljava/lang/Thread;

    .line 172
    sget-object v4, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mNotificationFlashThread:Ljava/lang/Thread;

    invoke-virtual {v4, v7}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 173
    sget-object v4, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mNotificationFlashThread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 174
    const-string v4, "AlertPopup"

    const-string v5, "Thread start"

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->VibrationForSoundDetect()V

    .line 178
    invoke-direct {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->TimerForAlert()V

    .line 180
    new-instance v4, Lcom/samsung/android/app/sounddetector/SoundDetectorKeyDispatcher;

    invoke-direct {v4}, Lcom/samsung/android/app/sounddetector/SoundDetectorKeyDispatcher;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mKey:Lcom/samsung/android/app/sounddetector/SoundDetectorKeyDispatcher;

    .line 182
    new-instance v4, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;-><init>(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;Lcom/samsung/android/app/sounddetector/alert/AlertPopup$1;)V

    iput-object v4, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mVisualizerReceiver:Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;

    .line 183
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 184
    .local v2, "intentFilter":Landroid/content/IntentFilter;
    const-string v4, "alert_visualization_baby_crying"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 185
    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mVisualizerReceiver:Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;

    invoke-virtual {p0, v4, v2}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 187
    new-instance v0, Landroid/content/Intent;

    const-class v4, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-direct {v0, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 188
    .local v0, "detectionIntent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v4, v8}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 189
    return-void

    .line 117
    .end local v0    # "detectionIntent":Landroid/content/Intent;
    .end local v2    # "intentFilter":Landroid/content/IntentFilter;
    .end local v3    # "mScoverState":Lcom/samsung/android/sdk/cover/ScoverState;
    :catch_0
    move-exception v1

    .line 118
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_0

    .line 119
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 120
    .local v1, "e":Lcom/samsung/android/sdk/SsdkUnsupportedException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/SsdkUnsupportedException;->printStackTrace()V

    goto/16 :goto_0

    .line 159
    .end local v1    # "e":Lcom/samsung/android/sdk/SsdkUnsupportedException;
    .restart local v3    # "mScoverState":Lcom/samsung/android/sdk/cover/ScoverState;
    :cond_2
    iput-boolean v8, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverOpen:Z

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 217
    const-string v1, "AlertPopup"

    const-string v2, "onDestroy"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverMainBG:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 220
    const v1, 0x7f090022

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->recycleView(Landroid/view/View;)V

    .line 223
    :cond_0
    iget v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->flashNotification_state:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 224
    sget-object v1, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mNotificationFlashThread:Ljava/lang/Thread;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mNotificationFlashThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 225
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->isRunning:Z

    .line 226
    sget-object v1, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mNotificationFlashThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 227
    const-string v1, "AlertPopup"

    const-string v2, "Thread stop"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.ACTION_ASSISTIVE_OFF"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 229
    .local v0, "mIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->sendBroadcast(Landroid/content/Intent;)V

    .line 233
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->timer:Ljava/util/Timer;

    if-eqz v1, :cond_2

    .line 234
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->timer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 237
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mVibrator:Landroid/os/SystemVibrator;

    if-eqz v1, :cond_3

    .line 238
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v1}, Landroid/os/SystemVibrator;->cancel()V

    .line 239
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mVibrator:Landroid/os/SystemVibrator;

    .line 242
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v1, :cond_4

    .line 243
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 246
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mVisualizerReceiver:Lcom/samsung/android/app/sounddetector/alert/AlertPopup$VisualizerReceiver;

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 248
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mSoundDetectService:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    if-eqz v1, :cond_5

    .line 249
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mSoundDetectService:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->stopSoundDetect()V

    .line 252
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->unbindService(Landroid/content/ServiceConnection;)V

    .line 254
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 255
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 283
    const-string v1, "AlertPopup"

    const-string v2, "onKeyDown event"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    const/16 v1, 0xbb

    if-ne p1, v1, :cond_0

    .line 286
    const-string v1, "AlertPopup"

    const-string v2, "Back key, Recent app Key down is ingnored."

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    :goto_0
    return v0

    .line 288
    :cond_0
    const/4 v1, 0x4

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_1

    const/16 v1, 0x1a

    if-eq p1, v1, :cond_1

    const/16 v1, 0x18

    if-eq p1, v1, :cond_1

    const/16 v1, 0x19

    if-ne p1, v1, :cond_2

    .line 290
    :cond_1
    const-string v1, "AlertPopup"

    const-string v2, "Home key, Power key, Volume key down is activity finished."

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->finish()V

    goto :goto_0

    .line 295
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 272
    const-string v0, "AlertPopup"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mKey:Lcom/samsung/android/app/sounddetector/SoundDetectorKeyDispatcher;

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lcom/samsung/android/app/sounddetector/SoundDetectorKeyDispatcher;->requestSystemKeyEvent(IZLandroid/content/ComponentName;)Z

    .line 275
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mKey:Lcom/samsung/android/app/sounddetector/SoundDetectorKeyDispatcher;

    const/16 v1, 0x1a

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lcom/samsung/android/app/sounddetector/SoundDetectorKeyDispatcher;->requestSystemKeyEvent(IZLandroid/content/ComponentName;)Z

    .line 276
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mKey:Lcom/samsung/android/app/sounddetector/SoundDetectorKeyDispatcher;

    const/16 v1, 0xbb

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lcom/samsung/android/app/sounddetector/SoundDetectorKeyDispatcher;->requestSystemKeyEvent(IZLandroid/content/ComponentName;)Z

    .line 278
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 279
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 259
    const-string v0, "AlertPopup"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mKey:Lcom/samsung/android/app/sounddetector/SoundDetectorKeyDispatcher;

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lcom/samsung/android/app/sounddetector/SoundDetectorKeyDispatcher;->requestSystemKeyEvent(IZLandroid/content/ComponentName;)Z

    .line 262
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mKey:Lcom/samsung/android/app/sounddetector/SoundDetectorKeyDispatcher;

    const/16 v1, 0x1a

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lcom/samsung/android/app/sounddetector/SoundDetectorKeyDispatcher;->requestSystemKeyEvent(IZLandroid/content/ComponentName;)Z

    .line 263
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mKey:Lcom/samsung/android/app/sounddetector/SoundDetectorKeyDispatcher;

    const/16 v1, 0xbb

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lcom/samsung/android/app/sounddetector/SoundDetectorKeyDispatcher;->requestSystemKeyEvent(IZLandroid/content/ComponentName;)Z

    .line 265
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 267
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 268
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x7d0

    .line 300
    const-string v0, "AlertPopup"

    const-string v1, "NotificationFlash Thread Run"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->isRunning:Z

    if-eqz v0, :cond_1

    .line 302
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->cfmsService:Landroid/os/CustomFrequencyManager;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->cfmsService:Landroid/os/CustomFrequencyManager;

    const-string v1, "CLOCK_SET_TORCH_LIGHT"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/os/CustomFrequencyManager;->sendCommandToSSRM(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    invoke-static {v4, v5}, Landroid/os/SystemClock;->sleep(J)V

    .line 305
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->cfmsService:Landroid/os/CustomFrequencyManager;

    const-string v1, "CLOCK_SET_TORCH_LIGHT"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Landroid/os/CustomFrequencyManager;->sendCommandToSSRM(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    invoke-static {v4, v5}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_0

    .line 309
    :cond_1
    return-void
.end method
