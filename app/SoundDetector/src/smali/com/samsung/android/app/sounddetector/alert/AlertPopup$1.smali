.class Lcom/samsung/android/app/sounddetector/alert/AlertPopup$1;
.super Ljava/lang/Object;
.source "AlertPopup.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/sounddetector/alert/AlertPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$1;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$1;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    check-cast p2, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$DetectBinder;

    .end local p2    # "service":Landroid/os/IBinder;
    invoke-virtual {p2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$DetectBinder;->getService()Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    move-result-object v1

    # setter for: Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mSoundDetectService:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;
    invoke-static {v0, v1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->access$002(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    .line 97
    const-string v0, "AlertPopup"

    const-string v1, "Bind service connected"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$1;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    # getter for: Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->access$100(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 99
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$1;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mSoundDetectService:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;
    invoke-static {v0, v1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->access$002(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    .line 104
    const-string v0, "AlertPopup"

    const-string v1, "Bind service disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    return-void
.end method
