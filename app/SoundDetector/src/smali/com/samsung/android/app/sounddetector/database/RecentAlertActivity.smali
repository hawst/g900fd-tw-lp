.class public Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;
.super Landroid/app/Activity;
.source "RecentAlertActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;
    }
.end annotation


# static fields
.field private static final NORMAL_MODE:I = 0x0

.field private static final SELECT_MODE:I = 0x1


# instance fields
.field private final TAG:Ljava/lang/String;

.field private customView:Landroid/view/View;

.field isClickUndoBtn:Z

.field isShowingUndoPopup:Z

.field private listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

.field private mActionBar:Landroid/app/ActionBar;

.field private mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

.field private mAlertDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/sounddetector/database/AlertData;",
            ">;"
        }
    .end annotation
.end field

.field private mAlertListView:Landroid/widget/ListView;

.field private mContext:Landroid/content/Context;

.field private mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

.field private mNoAlertLayout:Landroid/widget/RelativeLayout;

.field private mOptionMenu:Landroid/view/Menu;

.field private mSelectAllCheckBox:Landroid/widget/CheckBox;

.field private mSelectAllLayout:Landroid/widget/RelativeLayout;

.field private mSelectNumText:Landroid/widget/TextView;

.field private mUndoPopup:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 49
    const-string v0, "RecentAlertActivity"

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->TAG:Ljava/lang/String;

    .line 73
    iput-boolean v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->isClickUndoBtn:Z

    .line 74
    iput-boolean v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->isShowingUndoPopup:Z

    .line 468
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;)Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;
    .param p1, "x1"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    return-object p1
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mSelectAllLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertDataList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertDataList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;
    .param p1, "x1"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mUndoPopup:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    return-object v0
.end method

.method static synthetic access$702(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;)Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;
    .param p1, "x1"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    return-object p1
.end method


# virtual methods
.method public ShowDialog(Landroid/util/SparseBooleanArray;)V
    .locals 12
    .param p1, "selectedAlert"    # Landroid/util/SparseBooleanArray;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 328
    move-object v2, p1

    .line 329
    .local v2, "mSelectedAlert":Landroid/util/SparseBooleanArray;
    iput-boolean v10, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->isClickUndoBtn:Z

    .line 331
    new-instance v7, Landroid/app/Dialog;

    iget-object v8, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mUndoPopup:Landroid/app/Dialog;

    .line 332
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f03000c

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 334
    .local v4, "mUndoPopupView":Landroid/view/View;
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mUndoPopup:Landroid/app/Dialog;

    invoke-virtual {v7, v11}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 335
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mUndoPopup:Landroid/app/Dialog;

    invoke-virtual {v7, v4}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 337
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mUndoPopup:Landroid/app/Dialog;

    const v8, 0x7f090031

    invoke-virtual {v7, v8}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 338
    .local v1, "mDeleteNum":Landroid/widget/TextView;
    const v7, 0x7f060015

    new-array v8, v11, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    invoke-virtual {v9}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->getSelectedCount()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {p0, v7, v8}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 340
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mUndoPopup:Landroid/app/Dialog;

    const v8, 0x7f090032

    invoke-virtual {v7, v8}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 341
    .local v3, "mUndoBtn":Landroid/widget/LinearLayout;
    new-instance v7, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$2;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$2;-><init>(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)V

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 354
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mUndoPopup:Landroid/app/Dialog;

    invoke-virtual {v7}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Landroid/view/Window;->clearFlags(I)V

    .line 355
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mUndoPopup:Landroid/app/Dialog;

    invoke-virtual {v7, v11}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 356
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mUndoPopup:Landroid/app/Dialog;

    invoke-virtual {v7}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    new-instance v8, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v8, v10}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v7, v8}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 357
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mUndoPopup:Landroid/app/Dialog;

    invoke-virtual {v7}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    const/16 v8, 0x50

    invoke-virtual {v7, v8}, Landroid/view/Window;->setGravity(I)V

    .line 358
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mUndoPopup:Landroid/app/Dialog;

    invoke-virtual {v7}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    .line 359
    .local v5, "params":Landroid/view/WindowManager$LayoutParams;
    const/4 v7, -0x1

    iput v7, v5, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 360
    const/high16 v7, 0x42340000    # 45.0f

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    invoke-static {v11, v7, v8}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v7

    float-to-int v7, v7

    iput v7, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 361
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mUndoPopup:Landroid/app/Dialog;

    invoke-virtual {v7}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 363
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mUndoPopup:Landroid/app/Dialog;

    new-instance v8, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;

    invoke-direct {v8, p0, v2}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;-><init>(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Landroid/util/SparseBooleanArray;)V

    invoke-virtual {v7, v8}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 392
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mUndoPopup:Landroid/app/Dialog;

    invoke-virtual {v7}, Landroid/app/Dialog;->show()V

    .line 393
    iput-boolean v11, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->isShowingUndoPopup:Z

    .line 394
    new-instance v7, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    .line 395
    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-virtual {v8}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->getRecentAlertCount()I

    move-result v8

    if-ne v7, v8, :cond_1

    .line 396
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mNoAlertLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 397
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertListView:Landroid/widget/ListView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setVisibility(I)V

    .line 398
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mOptionMenu:Landroid/view/Menu;

    if-eqz v7, :cond_0

    .line 399
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mOptionMenu:Landroid/view/Menu;

    invoke-interface {v7}, Landroid/view/Menu;->clear()V

    .line 413
    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-virtual {v7}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->close()V

    .line 414
    return-void

    .line 402
    :cond_1
    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v7

    add-int/lit8 v0, v7, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_3

    .line 403
    invoke-virtual {v2, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 404
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    invoke-virtual {v2, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/app/sounddetector/database/AlertData;

    .line 405
    .local v6, "selectedItem":Lcom/samsung/android/app/sounddetector/database/AlertData;
    const-string v7, "RecentAlertActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Index : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Lcom/samsung/android/app/sounddetector/database/AlertData;->getIndex()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertDataList:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 402
    .end local v6    # "selectedItem":Lcom/samsung/android/app/sounddetector/database/AlertData;
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 410
    :cond_3
    new-instance v7, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    iget-object v8, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertDataList:Ljava/util/ArrayList;

    invoke-direct {v7, v8, v9, v10}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;I)V

    iput-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    .line 411
    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertListView:Landroid/widget/ListView;

    iget-object v8, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method public changeSettingDateFormat(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p1, "mDate"    # Ljava/lang/String;

    .prologue
    .line 417
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 419
    .local v0, "cal":Ljava/util/Calendar;
    const-string v2, ""

    .line 420
    .local v2, "new_date":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "date_format"

    invoke-static {v10, v11}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 421
    .local v7, "setting_date":Ljava/lang/String;
    const-string v10, "%02d-%02d-%02d"

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-virtual {v0, v13}, Ljava/util/Calendar;->get(I)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    const/4 v13, 0x2

    invoke-virtual {v0, v13}, Ljava/util/Calendar;->get(I)I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    const/4 v13, 0x5

    invoke-virtual {v0, v13}, Ljava/util/Calendar;->get(I)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 423
    .local v8, "today_date":Ljava/lang/String;
    const/4 v10, 0x5

    const/4 v11, -0x1

    invoke-virtual {v0, v10, v11}, Ljava/util/Calendar;->add(II)V

    .line 424
    const-string v10, "%02d-%02d-%02d"

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-virtual {v0, v13}, Ljava/util/Calendar;->get(I)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    const/4 v13, 0x2

    invoke-virtual {v0, v13}, Ljava/util/Calendar;->get(I)I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    const/4 v13, 0x5

    invoke-virtual {v0, v13}, Ljava/util/Calendar;->get(I)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 426
    .local v9, "yesterday_date":Ljava/lang/String;
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v10, "yyyy-MM-dd"

    invoke-direct {v6, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 427
    .local v6, "original_format":Ljava/text/SimpleDateFormat;
    new-instance v4, Ljava/text/SimpleDateFormat;

    const/16 v10, 0x2d

    const/16 v11, 0x2f

    invoke-virtual {v7, v10, v11}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v4, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 429
    .local v4, "new_format":Ljava/text/SimpleDateFormat;
    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 430
    const v10, 0x7f060016

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 443
    .end local v2    # "new_date":Ljava/lang/String;
    .local v3, "new_date":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 432
    .end local v3    # "new_date":Ljava/lang/String;
    .restart local v2    # "new_date":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 433
    const v10, 0x7f060017

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 434
    .end local v2    # "new_date":Ljava/lang/String;
    .restart local v3    # "new_date":Ljava/lang/String;
    goto :goto_0

    .line 438
    .end local v3    # "new_date":Ljava/lang/String;
    .restart local v2    # "new_date":Ljava/lang/String;
    :cond_1
    :try_start_0
    invoke-virtual {v6, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v5

    .line 439
    .local v5, "original_date":Ljava/util/Date;
    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .end local v5    # "original_date":Ljava/util/Date;
    :goto_1
    move-object v3, v2

    .line 443
    .end local v2    # "new_date":Ljava/lang/String;
    .restart local v3    # "new_date":Ljava/lang/String;
    goto :goto_0

    .line 440
    .end local v3    # "new_date":Ljava/lang/String;
    .restart local v2    # "new_date":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 441
    .local v1, "e":Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_1
.end method

.method public changeSettingTimeFormat(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "mDate"    # Ljava/lang/String;

    .prologue
    .line 447
    const-string v2, ""

    .line 448
    .local v2, "new_time":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "time_12_24"

    invoke-static {v6, v7}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 450
    .local v5, "setting_time":Ljava/lang/String;
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v6, "HH:mm"

    invoke-direct {v4, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 451
    .local v4, "original_format":Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v6, "h:mm a"

    invoke-direct {v1, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 453
    .local v1, "new_format":Ljava/text/SimpleDateFormat;
    const-string v6, "12"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 455
    :try_start_0
    invoke-virtual {v4, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    .line 456
    .local v3, "original_date":Ljava/util/Date;
    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .end local v3    # "original_date":Ljava/util/Date;
    :goto_0
    move-object p1, v2

    .line 465
    .end local p1    # "mDate":Ljava/lang/String;
    :cond_0
    :goto_1
    return-object p1

    .line 457
    .restart local p1    # "mDate":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 458
    .local v0, "e":Ljava/text/ParseException;
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0

    .line 461
    .end local v0    # "e":Ljava/text/ParseException;
    :cond_1
    const-string v6, "24"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    goto :goto_1
.end method

.method public createModeData(IILjava/lang/String;Ljava/lang/String;)Lcom/samsung/android/app/sounddetector/database/AlertData;
    .locals 1
    .param p1, "idx"    # I
    .param p2, "type"    # I
    .param p3, "time"    # Ljava/lang/String;
    .param p4, "date"    # Ljava/lang/String;

    .prologue
    .line 241
    new-instance v0, Lcom/samsung/android/app/sounddetector/database/AlertData;

    invoke-direct {v0}, Lcom/samsung/android/app/sounddetector/database/AlertData;-><init>()V

    .line 242
    .local v0, "mode":Lcom/samsung/android/app/sounddetector/database/AlertData;
    invoke-virtual {v0, p1}, Lcom/samsung/android/app/sounddetector/database/AlertData;->setIndex(I)V

    .line 243
    invoke-virtual {v0, p2}, Lcom/samsung/android/app/sounddetector/database/AlertData;->setSoundType(I)V

    .line 244
    invoke-virtual {v0, p3}, Lcom/samsung/android/app/sounddetector/database/AlertData;->setAlertTime(Ljava/lang/String;)V

    .line 245
    invoke-virtual {v0, p4}, Lcom/samsung/android/app/sounddetector/database/AlertData;->setAlertDate(Ljava/lang/String;)V

    .line 247
    return-object v0
.end method

.method public getDataFromDataBase()Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/sounddetector/database/AlertData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 213
    new-instance v5, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    .line 216
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-virtual {v5}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->getAlertDataIndex()Ljava/util/ArrayList;

    move-result-object v2

    .line 217
    .local v2, "mAlertDataIdx":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 219
    .local v4, "modeDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/sounddetector/database/AlertData;>;"
    const-string v3, "Date"

    .line 221
    .local v3, "mCategoryDate":Ljava/lang/String;
    const-string v5, "RecentAlertActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "alert Data Num : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-virtual {v7}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->getRecentAlertCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    const-string v5, "RecentAlertActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "alert Data index size : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v0, v5, :cond_1

    .line 225
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v6, v5}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->getAlert(I)Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;

    move-result-object v1

    .line 226
    .local v1, "mAlert":Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;
    const-string v5, "RecentAlertActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "alert Data ID : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->getID()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->getAlertDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 229
    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->getAlertDate()Ljava/lang/String;

    move-result-object v3

    .line 230
    const/4 v5, 0x0

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->getAlertDate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->changeSettingDateFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v9, v9, v5, v6}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->createModeData(IILjava/lang/String;Ljava/lang/String;)Lcom/samsung/android/app/sounddetector/database/AlertData;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    :cond_0
    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->getID()I

    move-result v5

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->getSoundType()I

    move-result v6

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->getAlertTime()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->changeSettingTimeFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->getAlertDate()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v5, v6, v7, v8}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->createModeData(IILjava/lang/String;Ljava/lang/String;)Lcom/samsung/android/app/sounddetector/database/AlertData;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 236
    .end local v1    # "mAlert":Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-virtual {v5}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->close()V

    .line 237
    return-object v4
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 170
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 171
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    if-eqz v0, :cond_0

    .line 172
    const-string v0, "RecentAlertActivity"

    const-string v1, "Rotate"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 78
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 80
    iput-object p0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mContext:Landroid/content/Context;

    .line 82
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionBar:Landroid/app/ActionBar;

    .line 83
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 86
    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->setContentView(I)V

    .line 88
    const v0, 0x7f09001f

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mNoAlertLayout:Landroid/widget/RelativeLayout;

    .line 89
    const v0, 0x7f09001b

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mSelectAllLayout:Landroid/widget/RelativeLayout;

    .line 90
    const v0, 0x7f09001e

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertListView:Landroid/widget/ListView;

    .line 91
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->getDataFromDataBase()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertDataList:Ljava/util/ArrayList;

    .line 92
    new-instance v0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertDataList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1, v2}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;I)V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    .line 94
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 95
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 96
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertListView:Landroid/widget/ListView;

    new-instance v1, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$1;-><init>(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 112
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->updateScreen()V

    .line 113
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mOptionMenu:Landroid/view/Menu;

    .line 138
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->updateOption()V

    .line 139
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 117
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 118
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    if-eqz v0, :cond_0

    .line 163
    invoke-virtual {p0, p3}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->onListItemSelect(I)V

    .line 164
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    invoke-virtual {v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->notifyChanged()V

    .line 166
    :cond_0
    return-void
.end method

.method public onListItemSelect(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 251
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->toggleSelection(I)V

    .line 254
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 144
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 157
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 146
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->onBackPressed()V

    goto :goto_0

    .line 149
    :sswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    if-nez v0, :cond_0

    new-instance v0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;-><init>(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$1;)V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;->setModeFrom(I)V

    .line 151
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mActionMode:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$ActionModeCallback;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    goto :goto_0

    .line 144
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x102002c -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->isShowingUndoPopup:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mUndoPopup:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mUndoPopup:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 125
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 126
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 130
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 132
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->updateScreen()V

    .line 133
    return-void
.end method

.method public updateOption()V
    .locals 3

    .prologue
    .line 177
    new-instance v1, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    .line 178
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->getRecentAlertCount()I

    move-result v0

    .line 179
    .local v0, "mAlertNum":I
    if-nez v0, :cond_0

    .line 180
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mOptionMenu:Landroid/view/Menu;

    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    .line 184
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->close()V

    .line 185
    return-void

    .line 182
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mOptionMenu:Landroid/view/Menu;

    const v2, 0x7f06001c

    invoke-interface {v1, v2}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public updateScreen()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 188
    new-instance v1, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    .line 189
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->getRecentAlertCount()I

    move-result v0

    .line 191
    .local v0, "mAlertNum":I
    if-nez v0, :cond_1

    .line 192
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mNoAlertLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 194
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertListView:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 195
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mOptionMenu:Landroid/view/Menu;

    if-eqz v1, :cond_0

    .line 196
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mOptionMenu:Landroid/view/Menu;

    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    .line 198
    :cond_0
    const-string v1, "RecentAlertActivity"

    const-string v2, "No alerts"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->close()V

    .line 210
    return-void

    .line 200
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mNoAlertLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 201
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertListView:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 202
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mOptionMenu:Landroid/view/Menu;

    if-eqz v1, :cond_2

    .line 203
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mOptionMenu:Landroid/view/Menu;

    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    .line 204
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mOptionMenu:Landroid/view/Menu;

    const v2, 0x7f06001c

    invoke-interface {v1, v2}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    .line 206
    :cond_2
    const-string v1, "RecentAlertActivity"

    const-string v2, "not No alerts"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
