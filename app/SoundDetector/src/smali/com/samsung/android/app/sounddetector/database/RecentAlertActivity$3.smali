.class Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;
.super Ljava/lang/Object;
.source "RecentAlertActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->ShowDialog(Landroid/util/SparseBooleanArray;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

.field final synthetic val$mSelectedAlert:Landroid/util/SparseBooleanArray;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Landroid/util/SparseBooleanArray;)V
    .locals 0

    .prologue
    .line 363
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    iput-object p2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->val$mSelectedAlert:Landroid/util/SparseBooleanArray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 7
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v6, 0x0

    .line 366
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    iget-boolean v2, v2, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->isClickUndoBtn:Z

    if-nez v2, :cond_2

    .line 367
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    iget-object v3, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    invoke-virtual {v3}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->getDataFromDataBase()Ljava/util/ArrayList;

    move-result-object v3

    # setter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertDataList:Ljava/util/ArrayList;
    invoke-static {v2, v3}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$202(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 368
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    new-instance v3, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$400(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;
    invoke-static {v2, v3}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$702(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;)Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    .line 369
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    new-instance v3, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$400(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertDataList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$200(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {v3, v4, v5, v6}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;I)V

    # setter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    invoke-static {v2, v3}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$302(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    .line 371
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->val$mSelectedAlert:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 372
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->val$mSelectedAlert:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 373
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$300(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->val$mSelectedAlert:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/sounddetector/database/AlertData;

    .line 374
    .local v1, "selectedItem":Lcom/samsung/android/app/sounddetector/database/AlertData;
    const-string v2, "RecentAlertActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Index : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/AlertData;->getIndex()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$700(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    move-result-object v2

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/AlertData;->getIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->deleteAlert(I)V

    .line 371
    .end local v1    # "selectedItem":Lcom/samsung/android/app/sounddetector/database/AlertData;
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 379
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    iget-object v3, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    invoke-virtual {v3}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->getDataFromDataBase()Ljava/util/ArrayList;

    move-result-object v3

    # setter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertDataList:Ljava/util/ArrayList;
    invoke-static {v2, v3}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$202(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 380
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    new-instance v3, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$400(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertDataList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$200(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {v3, v4, v5, v6}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;I)V

    # setter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    invoke-static {v2, v3}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$302(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    .line 381
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mAlertListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$500(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Landroid/widget/ListView;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->listAdapater:Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
    invoke-static {v3}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$300(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 383
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->access$700(Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;)Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->close()V

    .line 385
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    invoke-virtual {v2}, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->updateScreen()V

    .line 388
    .end local v0    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    iput-boolean v6, v2, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;->isShowingUndoPopup:Z

    .line 389
    return-void
.end method
