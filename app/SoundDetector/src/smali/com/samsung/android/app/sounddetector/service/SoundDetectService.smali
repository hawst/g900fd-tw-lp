.class public Lcom/samsung/android/app/sounddetector/service/SoundDetectService;
.super Landroid/app/Service;
.source "SoundDetectService.java"

# interfaces
.implements Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;,
        Lcom/samsung/android/app/sounddetector/service/SoundDetectService$DetectBinder;
    }
.end annotation


# static fields
.field private static final FROM_CAMERA:Ljava/lang/String; = "FROM_CAMERA"

.field private static final TYPE_BABY_CRYING:I = 0x1

.field private static final TYPE_DETECTING:I


# instance fields
.field private final ALERT_SCREEN:Ljava/lang/String;

.field private final MAIN_SCREEN:Ljava/lang/String;

.field private final RECENT_ALERT_SCREEN:Ljava/lang/String;

.field private final SETTING_SCREEN:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private final VOICE_WAKE_UP:Ljava/lang/String;

.field private activityManager:Landroid/app/ActivityManager;

.field private allSoundOffOriginalState:I

.field private mAudioManager:Landroid/media/AudioManager;

.field private mCurrentSoundValue:I

.field private mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

.field mDetectBinder:Lcom/samsung/android/app/sounddetector/service/SoundDetectService$DetectBinder;

.field private mIsAppDetecting:Z

.field private mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPowerManager:Landroid/os/PowerManager;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mSoundRecognizer:Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private runningTaskInfo:Landroid/app/ActivityManager$RunningTaskInfo;

.field private timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 41
    const-string v0, "SoundDetectService"

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->TAG:Ljava/lang/String;

    .line 48
    const-string v0, "com.samsung.android.app.sounddetector.MainActivity"

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->MAIN_SCREEN:Ljava/lang/String;

    .line 49
    const-string v0, "com.samsung.android.app.sounddetector.setting.SettingsActivity"

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->SETTING_SCREEN:Ljava/lang/String;

    .line 50
    const-string v0, "com.samsung.android.app.sounddetector.database.RecentAlertActivity"

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->RECENT_ALERT_SCREEN:Ljava/lang/String;

    .line 51
    const-string v0, "com.samsung.android.app.sounddetector.alert.AlertPopup"

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->ALERT_SCREEN:Ljava/lang/String;

    .line 53
    const-string v0, "com.samsung.android.app.sounddetector.VOICE_WAKEUP"

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->VOICE_WAKE_UP:Ljava/lang/String;

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mSoundRecognizer:Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;

    .line 67
    iput v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mCurrentSoundValue:I

    .line 70
    iput v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->allSoundOffOriginalState:I

    .line 71
    iput-boolean v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mIsAppDetecting:Z

    .line 73
    new-instance v0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$DetectBinder;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$DetectBinder;-><init>(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mDetectBinder:Lcom/samsung/android/app/sounddetector/service/SoundDetectService$DetectBinder;

    .line 106
    new-instance v0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$1;-><init>(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 137
    new-instance v0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$2;-><init>(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 420
    new-instance v0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$3;-><init>(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Landroid/app/ActivityManager$RunningTaskInfo;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->runningTaskInfo:Landroid/app/ActivityManager$RunningTaskInfo;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;Landroid/app/ActivityManager$RunningTaskInfo;)Landroid/app/ActivityManager$RunningTaskInfo;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/service/SoundDetectService;
    .param p1, "x1"    # Landroid/app/ActivityManager$RunningTaskInfo;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->runningTaskInfo:Landroid/app/ActivityManager$RunningTaskInfo;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Landroid/app/ActivityManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->activityManager:Landroid/app/ActivityManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mIsAppDetecting:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/service/SoundDetectService;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mIsAppDetecting:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Landroid/os/PowerManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mPowerManager:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mSoundRecognizer:Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->timer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$702(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/service/SoundDetectService;
    .param p1, "x1"    # Ljava/util/Timer;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->timer:Ljava/util/Timer;

    return-object p1
.end method


# virtual methods
.method public alertVisualizer(II)V
    .locals 3
    .param p1, "soundType"    # I
    .param p2, "soundLevel"    # I

    .prologue
    .line 294
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 296
    .local v0, "intent":Landroid/content/Intent;
    packed-switch p1, :pswitch_data_0

    .line 304
    :goto_0
    const-string v1, "soundLevel"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 305
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->sendBroadcast(Landroid/content/Intent;)V

    .line 306
    return-void

    .line 298
    :pswitch_0
    const-string v1, "alert_visualization_baby_crying"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 299
    const-string v1, "SoundDetectService"

    const-string v2, "Crying BroadCast(Alert Visualizer)"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 296
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getCurrentDate()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 330
    const-string v1, ""

    .line 331
    .local v1, "date":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 333
    .local v0, "cal":Ljava/util/Calendar;
    const-string v2, "%02d-%02d-%02d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 335
    return-object v1
.end method

.method public getCurrentTime()Ljava/lang/String;
    .locals 10

    .prologue
    .line 309
    const-string v5, ""

    .line 310
    .local v5, "time":Ljava/lang/String;
    const-string v3, ""

    .line 311
    .local v3, "new_time":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 313
    .local v0, "cal":Ljava/util/Calendar;
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v6, "hh:mm"

    invoke-direct {v4, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 314
    .local v4, "original_format":Ljava/text/SimpleDateFormat;
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v6, "h:mm a"

    invoke-direct {v2, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 316
    .local v2, "new_format":Ljava/text/SimpleDateFormat;
    const-string v6, "%02d:%02d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/16 v9, 0xb

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const/16 v9, 0xc

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 319
    :try_start_0
    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 326
    :goto_0
    return-object v5

    .line 322
    :catch_0
    move-exception v1

    .line 323
    .local v1, "e":Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method public mainVisualizer(II)V
    .locals 3
    .param p1, "soundType"    # I
    .param p2, "soundLevel"    # I

    .prologue
    .line 277
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 279
    .local v0, "intent":Landroid/content/Intent;
    packed-switch p1, :pswitch_data_0

    .line 285
    const-string v1, "visualization_detecting"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 286
    const-string v1, "SoundDetectService"

    const-string v2, "Detecting BroadCast(Main Visualizer)"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    :goto_0
    const-string v1, "soundLevel"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 290
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->sendBroadcast(Landroid/content/Intent;)V

    .line 291
    return-void

    .line 281
    :pswitch_0
    const-string v1, "visualization_baby_crying"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    const-string v1, "SoundDetectService"

    const-string v2, "Crying BroadCast(Main Visualizer)"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 279
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public notiAlert()V
    .locals 9

    .prologue
    const v8, 0x7f060007

    const/high16 v7, 0x7f060000

    const/4 v6, 0x0

    .line 378
    const-string v4, "notification"

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 379
    .local v2, "notificationManager":Landroid/app/NotificationManager;
    new-instance v4, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v4, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v7}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const v5, 0x7f020019

    invoke-virtual {v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    invoke-virtual {p0, v8}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    .line 386
    .local v1, "notification":Landroid/app/Notification;
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.settings.accessibility.SOUND_DETECTOR"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 388
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const/high16 v5, 0x10000000

    invoke-static {v4, v6, v0, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 390
    .local v3, "pie":Landroid/app/PendingIntent;
    invoke-virtual {p0, v7}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v8}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, p0, v4, v5, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 391
    invoke-virtual {v2, v6, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 392
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mDetectBinder:Lcom/samsung/android/app/sounddetector/service/SoundDetectService$DetectBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 189
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 191
    const-string v1, "activity"

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->activityManager:Landroid/app/ActivityManager;

    .line 192
    new-instance v1, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    .line 193
    new-instance v1, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, p0, v3}, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;-><init>(Landroid/content/Context;Lcom/sec/android/app/SoundRecognizer/SoundRecognizerListener;I)V

    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mSoundRecognizer:Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;

    .line 194
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mAudioManager:Landroid/media/AudioManager;

    .line 195
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mPowerManager:Landroid/os/PowerManager;

    .line 196
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 198
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 199
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.android.app.sounddetector.VOICE_WAKEUP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 200
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 201
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 225
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 227
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mSoundRecognizer:Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;

    invoke-virtual {v1}, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->stopSoundDetect()V

    .line 228
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mSoundRecognizer:Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;

    invoke-virtual {v1}, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->destroySoundRecognizer()V

    .line 229
    const-string v1, "SoundDetectService"

    const-string v2, "Sound detect service stop"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 232
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 234
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->close()V

    .line 236
    iget v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->allSoundOffOriginalState:I

    if-nez v1, :cond_0

    .line 237
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.ALL_SOUND_MUTE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 238
    .local v0, "all_sound_off_intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "all_sound_off"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 239
    const-string v1, "mute"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 240
    sget-object v1, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 242
    .end local v0    # "all_sound_off_intent":Landroid/content/Intent;
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 243
    return-void
.end method

.method public onRMSValue(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 396
    iput p1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mCurrentSoundValue:I

    .line 397
    const-string v0, "SoundDetectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRMSValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    return-void
.end method

.method public onResults(I[I)V
    .locals 6
    .param p1, "soundDetectResult"    # I
    .param p2, "resultInfo"    # [I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 247
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->activityManager:Landroid/app/ActivityManager;

    invoke-virtual {v1, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningTaskInfo;

    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->runningTaskInfo:Landroid/app/ActivityManager$RunningTaskInfo;

    .line 248
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->runningTaskInfo:Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v1, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 249
    .local v0, "topActivity":Ljava/lang/String;
    const-string v1, "SoundDetectService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Top Activity : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    packed-switch p1, :pswitch_data_0

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 254
    :pswitch_0
    const-string v1, "SoundDetectService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sound Type (0) : not baby crying sound, Energy value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mCurrentSoundValue:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    const-string v1, "com.samsung.android.app.sounddetector.MainActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 257
    iget v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mCurrentSoundValue:I

    invoke-virtual {p0, v4, v1}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mainVisualizer(II)V

    goto :goto_0

    .line 261
    :pswitch_1
    const-string v1, "SoundDetectService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sound Type (1) : baby crying sound, Energy value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mCurrentSoundValue:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    const-string v1, "com.samsung.android.app.sounddetector.alert.AlertPopup"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 264
    iget v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mCurrentSoundValue:I

    invoke-virtual {p0, v5, v1}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->alertVisualizer(II)V

    goto :goto_0

    .line 266
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->popUpAlert()V

    .line 267
    const-string v1, "SoundDetectService"

    const-string v2, "Baby Crying Alert!!"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 251
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v4, 0x1

    .line 205
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 207
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v3, 0x20

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 208
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "all_sound_off"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->allSoundOffOriginalState:I

    .line 209
    iget v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->allSoundOffOriginalState:I

    if-nez v1, :cond_0

    .line 210
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.ALL_SOUND_MUTE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 211
    .local v0, "all_sound_off_intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "all_sound_off"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 212
    const-string v1, "mute"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 213
    sget-object v1, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 216
    .end local v0    # "all_sound_off_intent":Landroid/content/Intent;
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mSoundRecognizer:Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->startSoundDetect(I)V

    .line 217
    iput-boolean v4, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mIsAppDetecting:Z

    .line 218
    const-string v1, "SoundDetectService"

    const-string v2, "Sound detect service start"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    return v4
.end method

.method public popUpAlert()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 339
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "access_control_enabled"

    invoke-static {v5, v6, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-ne v5, v9, :cond_0

    .line 340
    const-string v5, "SoundDetectService"

    const-string v6, "not alert display because interaction control enabled."

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    :goto_0
    return-void

    .line 342
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-virtual {v5}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->getRecentAlertCount()I

    move-result v1

    .line 343
    .local v1, "mAlertNum":I
    const-string v5, "SoundDetectService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ALert Num : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    const/16 v5, 0x64

    if-ne v1, v5, :cond_2

    .line 346
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-virtual {v5}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->getOldRecentAlertId()I

    move-result v2

    .line 347
    .local v2, "mOldestAlertId":I
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-virtual {v5, v2}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->deleteAlert(I)V

    .line 348
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    new-instance v6, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getCurrentTime()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getCurrentDate()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v9, v7, v8}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->addAlert(Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;)V

    .line 353
    .end local v2    # "mOldestAlertId":I
    :goto_1
    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 354
    .local v4, "popupIntent":Landroid/content/Intent;
    const/high16 v5, 0x24800000

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 356
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v5, v10, v4, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 359
    .local v3, "pie":Landroid/app/PendingIntent;
    :try_start_0
    invoke-virtual {v3}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 364
    :goto_2
    const-string v5, "SoundDetectService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Is connected gear ? :  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "wmanager_connected"

    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    const-string v5, "1"

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "wmanager_connected"

    invoke-static {v6, v7}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 367
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->notiAlert()V

    .line 370
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    invoke-virtual {v5}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->close()V

    goto/16 :goto_0

    .line 350
    .end local v3    # "pie":Landroid/app/PendingIntent;
    .end local v4    # "popupIntent":Landroid/content/Intent;
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mDbOpenHelper:Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;

    new-instance v6, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getCurrentTime()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getCurrentDate()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v9, v7, v8}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->addAlert(Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;)V

    goto :goto_1

    .line 360
    .restart local v3    # "pie":Landroid/app/PendingIntent;
    .restart local v4    # "popupIntent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 361
    .local v0, "e":Landroid/app/PendingIntent$CanceledException;
    const-string v5, "SoundDetectService"

    const-string v6, "Pending intent cancled"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public startSoundDetect()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 401
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 405
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mIsAppDetecting:Z

    if-nez v0, :cond_1

    .line 406
    const-string v0, "SoundDetectService"

    const-string v1, "setParameters : voice_wakeup_babycry=off"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "voice_wakeup_babycry=off"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 409
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mSoundRecognizer:Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->startSoundDetect(I)V

    .line 410
    iput-boolean v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mIsAppDetecting:Z

    .line 411
    const-string v0, "SoundDetectService"

    const-string v1, "sound detect engine start detect"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    :cond_1
    return-void
.end method

.method public stopSoundDetect()V
    .locals 4

    .prologue
    .line 416
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->timer:Ljava/util/Timer;

    .line 417
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;-><init>(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;Lcom/samsung/android/app/sounddetector/service/SoundDetectService$1;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 418
    return-void
.end method
