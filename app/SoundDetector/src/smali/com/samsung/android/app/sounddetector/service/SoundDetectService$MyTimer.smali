.class Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;
.super Ljava/util/TimerTask;
.source "SoundDetectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/sounddetector/service/SoundDetectService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyTimer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;Lcom/samsung/android/app/sounddetector/service/SoundDetectService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/app/sounddetector/service/SoundDetectService;
    .param p2, "x1"    # Lcom/samsung/android/app/sounddetector/service/SoundDetectService$1;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;-><init>(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 84
    iget-object v3, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->activityManager:Landroid/app/ActivityManager;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$100(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Landroid/app/ActivityManager;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    # setter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->runningTaskInfo:Landroid/app/ActivityManager$RunningTaskInfo;
    invoke-static {v3, v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$002(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;Landroid/app/ActivityManager$RunningTaskInfo;)Landroid/app/ActivityManager$RunningTaskInfo;

    .line 85
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->runningTaskInfo:Landroid/app/ActivityManager$RunningTaskInfo;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$000(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    .line 86
    .local v1, "topActivity":Ljava/lang/String;
    const-string v2, "SoundDetectService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Top Activity : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mIsAppDetecting:Z
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$200(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "com.samsung.android.app.sounddetector.MainActivity"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mIsAppDetecting:Z
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$200(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "com.samsung.android.app.sounddetector.MainActivity"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mPowerManager:Landroid/os/PowerManager;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$300(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Landroid/os/PowerManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mIsAppDetecting:Z
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$200(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "com.samsung.android.app.sounddetector.alert.AlertPopup"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 91
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mSoundRecognizer:Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$400(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->stopSoundDetect()V

    .line 92
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # setter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mIsAppDetecting:Z
    invoke-static {v2, v5}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$202(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;Z)Z

    .line 93
    const-string v2, "SoundDetectService"

    const-string v3, "sound detect engine stop detect"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    const-string v2, "SoundDetectService"

    const-string v3, "setParameters : voice_wakeup_babycry=on"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$500(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Landroid/media/AudioManager;

    move-result-object v2

    const-string v3, "voice_wakeup_babycry=on"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 103
    :cond_3
    return-void

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
