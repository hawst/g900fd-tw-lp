.class public Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;
.super Ljava/lang/Object;
.source "SoundDetectorEnabler.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field public static final PREF_DO_NOT_SHOW_AGAIN:Ljava/lang/String; = "pref_do_not_show_again"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mAudioManager:Landroid/media/AudioManager;

.field private final mContext:Landroid/content/Context;

.field private mSharedPreferences:Landroid/content/SharedPreferences;

.field private mSwitch:Landroid/widget/Switch;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/Switch;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "switch_"    # Landroid/widget/Switch;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, "SoundDetectEnabler"

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->TAG:Ljava/lang/String;

    .line 33
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    .line 34
    iput-object p2, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    .line 36
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mAudioManager:Landroid/media/AudioManager;

    .line 37
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 38
    return-void
.end method

.method private ShowDialog()V
    .locals 9

    .prologue
    .line 120
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    const/high16 v7, 0x7f060000

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 122
    .local v5, "mDialogTitle":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 123
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f030006

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 124
    .local v3, "layout_dialog":Landroid/view/View;
    const v6, 0x7f09002a

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 125
    .local v0, "do_not_show":Landroid/widget/CheckBox;
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 127
    .local v1, "edit":Landroid/content/SharedPreferences$Editor;
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    const/4 v8, 0x5

    invoke-direct {v6, v7, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v6, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f06000e

    new-instance v8, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$3;

    invoke-direct {v8, p0, v1, v0}, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$3;-><init>(Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;Landroid/content/SharedPreferences$Editor;Landroid/widget/CheckBox;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f06000f

    new-instance v8, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$2;

    invoke-direct {v8, p0, v1, v0}, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$2;-><init>(Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;Landroid/content/SharedPreferences$Editor;Landroid/widget/CheckBox;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$1;

    invoke-direct {v7, p0, v1, v0}, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$1;-><init>(Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;Landroid/content/SharedPreferences$Editor;Landroid/widget/CheckBox;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v4

    .line 162
    .local v4, "mDialog":Landroid/app/AlertDialog;
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;)Landroid/widget/Switch;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 10
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 42
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "sound_detector"

    invoke-static {v6, v7, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 43
    .local v4, "state":I
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v6}, Landroid/media/AudioManager;->isRecordActive()Z

    move-result v2

    .line 44
    .local v2, "mic_state":Z
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v6}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v3

    .line 45
    .local v3, "music_state":Z
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/samsung/android/app/sounddetector/Utils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v5

    .line 47
    .local v5, "talkback_state":Z
    if-eqz p2, :cond_4

    if-nez v4, :cond_4

    .line 48
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v6, v8}, Landroid/widget/Switch;->setChecked(Z)V

    .line 49
    const-string v6, "SoundDetectEnabler"

    const-string v7, "switch is on"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    if-eqz v2, :cond_0

    .line 52
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    const v8, 0x7f060010

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 53
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v6, v9}, Landroid/widget/Switch;->setChecked(Z)V

    .line 80
    :goto_0
    return-void

    .line 54
    :cond_0
    if-nez v3, :cond_1

    if-eqz v5, :cond_2

    .line 55
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    const v8, 0x7f060011

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 56
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v6, v9}, Landroid/widget/Switch;->setChecked(Z)V

    goto :goto_0

    .line 58
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v7, "pref_do_not_show_again"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 59
    .local v1, "mNever_show_again":Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 60
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "sound_detector"

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 62
    new-instance v0, Landroid/content/Intent;

    const-string v6, "com.android.settings.action.sound_detector"

    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 63
    .local v0, "mIntent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 65
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->ShowDialog()V

    goto :goto_0

    .line 68
    .end local v1    # "mNever_show_again":Ljava/lang/Boolean;
    :cond_4
    if-nez p2, :cond_5

    if-ne v4, v8, :cond_5

    .line 69
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v6, v9}, Landroid/widget/Switch;->setChecked(Z)V

    .line 71
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "sound_detector"

    invoke-static {v6, v7, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 73
    new-instance v0, Landroid/content/Intent;

    const-string v6, "com.android.settings.action.sound_detector"

    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 74
    .restart local v0    # "mIntent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 76
    const-string v6, "SoundDetectEnabler"

    const-string v7, "switch is off"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 78
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_5
    const-string v6, "SoundDetectEnabler"

    const-string v7, "switch error"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 98
    return-void
.end method

.method public resume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 83
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "sound_detector"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 85
    .local v0, "soundDetector_state":I
    if-ne v0, v4, :cond_0

    .line 86
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v4}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 87
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v4}, Landroid/widget/Switch;->setChecked(Z)V

    .line 93
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 94
    return-void

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 90
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3}, Landroid/widget/Switch;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setSwitch(Landroid/widget/Switch;)V
    .locals 5
    .param p1, "switch_"    # Landroid/widget/Switch;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 101
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    :goto_0
    return-void

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 105
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    .line 106
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 108
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "sound_detector"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 110
    .local v0, "soundDetector_state":I
    if-ne v0, v4, :cond_1

    .line 111
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v4}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 112
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v4}, Landroid/widget/Switch;->setChecked(Z)V

    goto :goto_0

    .line 114
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 115
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3}, Landroid/widget/Switch;->setEnabled(Z)V

    goto :goto_0
.end method
