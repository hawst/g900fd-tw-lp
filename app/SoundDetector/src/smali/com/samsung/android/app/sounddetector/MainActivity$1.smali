.class Lcom/samsung/android/app/sounddetector/MainActivity$1;
.super Landroid/database/ContentObserver;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/sounddetector/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/MainActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/sounddetector/MainActivity;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$1;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 83
    const-string v0, "MainActivity"

    const-string v1, "SoundDetector Switch Observer changed"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity$1;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    invoke-virtual {v0}, Lcom/samsung/android/app/sounddetector/MainActivity;->updateSoundDetectorScreen()V

    .line 85
    return-void
.end method
