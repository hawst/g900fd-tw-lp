.class public Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "RecentAlertDbOpenHelper.java"


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "alertHistoryManager"

.field private static final DATABASE_VERSION:I = 0x1

.field private static final KEY_ALERT_DATE:Ljava/lang/String; = "alert_date"

.field private static final KEY_ALERT_TIME:Ljava/lang/String; = "alert_time"

.field private static final KEY_ID:Ljava/lang/String; = "id"

.field private static final KEY_SOUND_TYPE:Ljava/lang/String; = "sound_type"

.field private static final TABLE_NAME:Ljava/lang/String; = "recent_alert"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    const-string v0, "alertHistoryManager"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 25
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->mContext:Landroid/content/Context;

    .line 26
    return-void
.end method


# virtual methods
.method public addAlert(Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;)V
    .locals 4
    .param p1, "recentAlert"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 44
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 45
    .local v1, "value":Landroid/content/ContentValues;
    const-string v2, "sound_type"

    invoke-virtual {p1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->getSoundType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 46
    const-string v2, "alert_time"

    invoke-virtual {p1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->getAlertTime()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    const-string v2, "alert_date"

    invoke-virtual {p1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->getAlertDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v2, "recent_alert"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 50
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 51
    return-void
.end method

.method public deleteAlert(I)V
    .locals 4
    .param p1, "_id"    # I

    .prologue
    .line 133
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DELETE FROM recent_alert WHERE id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 135
    .local v0, "countQuery":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 136
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 137
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 138
    return-void
.end method

.method public getAlert(I)Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;
    .locals 11
    .param p1, "id"    # I

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 67
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "recent_alert"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "sound_type"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "alert_time"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "alert_date"

    aput-object v4, v2, v3

    const-string v3, "id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 70
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_0

    .line 71
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 74
    :cond_0
    new-instance v10, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x1

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v10, v1, v2, v3, v4}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    .line 75
    .local v10, "mAlert":Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 76
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 78
    return-object v10
.end method

.method public getAlertDataIndex()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    const-string v1, "SELECT id FROM recent_alert"

    .line 113
    .local v1, "countQuery":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 114
    .local v4, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v6, 0x0

    invoke-virtual {v4, v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 115
    .local v3, "cursor":Landroid/database/Cursor;
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 116
    .local v2, "count_size":I
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 118
    .local v0, "alertDataIdx":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v3, :cond_0

    .line 119
    invoke-interface {v3}, Landroid/database/Cursor;->moveToLast()Z

    .line 121
    :cond_0
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v2, :cond_1

    .line 122
    const/4 v6, 0x0

    invoke-interface {v3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    invoke-interface {v3}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 121
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 126
    :cond_1
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 127
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 129
    return-object v0
.end method

.method public getOldRecentAlertId()I
    .locals 5

    .prologue
    .line 94
    const-string v0, "SELECT * FROM recent_alert"

    .line 96
    .local v0, "countQuery":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 97
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v4, 0x0

    invoke-virtual {v2, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 99
    .local v1, "cursor":Landroid/database/Cursor;
    if-eqz v1, :cond_0

    .line 100
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 102
    :cond_0
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 104
    .local v3, "mOldestAlertId":I
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 105
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 107
    return v3
.end method

.method public getRecentAlertCount()I
    .locals 5

    .prologue
    .line 82
    const-string v1, "SELECT * FROM recent_alert"

    .line 84
    .local v1, "countQuery":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 85
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 86
    .local v2, "cursor":Landroid/database/Cursor;
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 87
    .local v0, "alert_count":I
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 88
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 90
    return v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 30
    const-string v0, "CREATE TABLE IF NOT EXISTS recent_alert(id INTEGER PRIMARY KEY,sound_type INTEGER,alert_time TEXT,alert_date TEXT)"

    .line 32
    .local v0, "CREATE_ENV_TABLE":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 37
    const-string v0, "DROP TABLE IF EXISTS recent_alert"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 38
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 39
    return-void
.end method

.method public updateAlert(Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;)I
    .locals 7
    .param p1, "recentAlert"    # Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDbOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 56
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 57
    .local v1, "value":Landroid/content/ContentValues;
    const-string v2, "sound_type"

    invoke-virtual {p1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->getSoundType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 58
    const-string v2, "alert_time"

    invoke-virtual {p1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->getAlertTime()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v2, "alert_date"

    invoke-virtual {p1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->getAlertDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const-string v2, "recent_alert"

    const-string v3, "id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertDB;->getID()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    return v2
.end method
