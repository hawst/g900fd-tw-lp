.class Lcom/samsung/android/app/sounddetector/service/SoundDetectService$2;
.super Landroid/telephony/PhoneStateListener;
.source "SoundDetectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/sounddetector/service/SoundDetectService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$2;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 6
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 139
    packed-switch p1, :pswitch_data_0

    .line 179
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 141
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$2;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mIsAppDetecting:Z
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$200(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 142
    iget-object v3, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$2;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$2;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->activityManager:Landroid/app/ActivityManager;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$100(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Landroid/app/ActivityManager;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    # setter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->runningTaskInfo:Landroid/app/ActivityManager$RunningTaskInfo;
    invoke-static {v3, v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$002(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;Landroid/app/ActivityManager$RunningTaskInfo;)Landroid/app/ActivityManager$RunningTaskInfo;

    .line 143
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$2;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->runningTaskInfo:Landroid/app/ActivityManager$RunningTaskInfo;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$000(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, "topActivity":Ljava/lang/String;
    const-string v2, "SoundDetectService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Top Activity : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    const-wide/16 v2, 0x7d0

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :goto_1
    const-string v2, "com.samsung.android.app.sounddetector.MainActivity"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$2;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mPowerManager:Landroid/os/PowerManager;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$300(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Landroid/os/PowerManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 153
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$2;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mIsAppDetecting:Z
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$200(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 154
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$2;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mSoundRecognizer:Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$400(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->startSoundDetect(I)V

    .line 155
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$2;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # setter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mIsAppDetecting:Z
    invoke-static {v2, v5}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$202(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;Z)Z

    .line 156
    const-string v2, "SoundDetectService"

    const-string v3, "call idle, sound detect engine start detect"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 159
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    const-string v2, "SoundDetectService"

    const-string v3, "call idle, change setParameters : voice_wakeup_babycry=on"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$2;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$500(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Landroid/media/AudioManager;

    move-result-object v2

    const-string v3, "voice_wakeup_babycry=on"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 167
    .end local v1    # "topActivity":Ljava/lang/String;
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$2;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mIsAppDetecting:Z
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$200(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 168
    const-string v2, "SoundDetectService"

    const-string v3, "call ringing, Stop sound detect"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$2;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mSoundRecognizer:Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$400(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/SoundRecognizer/SoundRecognizer;->stopSoundDetect()V

    .line 170
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$2;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # setter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mIsAppDetecting:Z
    invoke-static {v2, v4}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$202(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;Z)Z

    goto/16 :goto_0

    .line 172
    :cond_2
    const-string v2, "SoundDetectService"

    const-string v3, "call ringing, setParameters : voice_wakeup_babycry=off"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$2;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$500(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Landroid/media/AudioManager;

    move-result-object v2

    const-string v3, "voice_wakeup_babycry=off"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 139
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
