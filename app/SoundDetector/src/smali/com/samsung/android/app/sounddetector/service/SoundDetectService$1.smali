.class Lcom/samsung/android/app/sounddetector/service/SoundDetectService$1;
.super Ljava/lang/Object;
.source "SoundDetectService.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/sounddetector/service/SoundDetectService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$1;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 6
    .param p1, "focusChange"    # I

    .prologue
    const/4 v5, 0x0

    .line 109
    packed-switch p1, :pswitch_data_0

    .line 134
    :goto_0
    :pswitch_0
    return-void

    .line 111
    :pswitch_1
    const-string v2, "SoundDetectService"

    const-string v3, "Audio focus gain"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 114
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$1;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    iget-object v3, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$1;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    const v4, 0x7f060012

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 115
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$1;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-virtual {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "sound_detector"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 117
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.settings.action.sound_detector"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 118
    .local v0, "mIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$1;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-virtual {v2, v0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->sendBroadcast(Landroid/content/Intent;)V

    .line 120
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$1;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mIsAppDetecting:Z
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$200(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 121
    const-string v2, "SoundDetectService"

    const-string v3, "setParameters : voice_wakeup_babycry=off"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$1;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$500(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Landroid/media/AudioManager;

    move-result-object v2

    const-string v3, "voice_wakeup_babycry=off"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 125
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$1;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-virtual {v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->stopSelf()V

    .line 126
    new-instance v1, Landroid/content/Intent;

    const-string v2, "stop_sounddetect_service"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 127
    .local v1, "mStopIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$1;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->sendBroadcast(Landroid/content/Intent;)V

    .line 129
    const-string v2, "SoundDetectService"

    const-string v3, "Audio focus loss, Stop service"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 109
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
