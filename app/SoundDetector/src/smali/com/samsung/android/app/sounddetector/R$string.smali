.class public final Lcom/samsung/android/app/sounddetector/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/sounddetector/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final action_delete:I = 0x7f060004

.field public static final action_recent_alert:I = 0x7f060002

.field public static final action_select:I = 0x7f06001c

.field public static final action_settings:I = 0x7f060003

.field public static final app_name:I = 0x7f060000

.field public static final app_running:I = 0x7f060001

.field public static final baby_crying_detected:I = 0x7f060007

.field public static final cancel:I = 0x7f06000f

.field public static final delete:I = 0x7f06001f

.field public static final deleted:I = 0x7f060015

.field public static final deselect_all:I = 0x7f06001e

.field public static final disclaimer:I = 0x7f06001b

.field public static final do_not_show:I = 0x7f06001a

.field public static final done:I = 0x7f06001d

.field public static final flash_notification:I = 0x7f06000b

.field public static final flash_notification_summary:I = 0x7f06000c

.field public static final main_app_description:I = 0x7f060005

.field public static final no_alert:I = 0x7f060018

.field public static final notifications:I = 0x7f060009

.field public static final ok:I = 0x7f06000e

.field public static final select_all:I = 0x7f060013

.field public static final selected:I = 0x7f060014

.field public static final sent_alert:I = 0x7f06000d

.field public static final sound_detecting:I = 0x7f060006

.field public static final stop_detecting_warning:I = 0x7f060012

.field public static final today:I = 0x7f060016

.field public static final undo:I = 0x7f060019

.field public static final use_mic_warning:I = 0x7f060010

.field public static final use_playing_warning:I = 0x7f060011

.field public static final vibration_intensity:I = 0x7f060008

.field public static final vibrations:I = 0x7f06000a

.field public static final yesterday:I = 0x7f060017


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
