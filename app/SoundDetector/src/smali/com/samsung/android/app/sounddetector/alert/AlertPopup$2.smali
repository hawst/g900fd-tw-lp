.class Lcom/samsung/android/app/sounddetector/alert/AlertPopup$2;
.super Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
.source "AlertPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$2;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    invoke-direct {p0}, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 6
    .param p1, "state"    # Lcom/samsung/android/sdk/cover/ScoverState;

    .prologue
    .line 127
    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 128
    const-string v2, "AlertPopup"

    const-string v3, "mCoverStateListener cover is close -> open"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$2;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    invoke-virtual {v2}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->finish()V

    .line 131
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$2;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$2;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    # getter for: Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->access$200(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->stopService(Landroid/content/Intent;)Z

    .line 133
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.settings.action.sound_detector.stop"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 134
    .local v0, "mIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$2;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    invoke-virtual {v2, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->sendBroadcast(Landroid/content/Intent;)V

    .line 136
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.accessibility.SOUND_DETECTOR"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 137
    .local v1, "popupIntent":Landroid/content/Intent;
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 138
    const/high16 v2, 0x20000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 139
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$2;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->startActivity(Landroid/content/Intent;)V

    .line 149
    .end local v1    # "popupIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 141
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_0
    const-string v2, "AlertPopup"

    const-string v3, "mCoverStateListener cover is open -> close"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$2;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    invoke-virtual {v2}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->finish()V

    .line 144
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$2;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$2;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    # getter for: Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->access$200(Lcom/samsung/android/app/sounddetector/alert/AlertPopup;)Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->stopService(Landroid/content/Intent;)Z

    .line 146
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.settings.action.sound_detector.stop"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 147
    .restart local v0    # "mIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/alert/AlertPopup$2;->this$0:Lcom/samsung/android/app/sounddetector/alert/AlertPopup;

    invoke-virtual {v2, v0}, Lcom/samsung/android/app/sounddetector/alert/AlertPopup;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method
