.class public Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;
.super Landroid/preference/PreferenceFragment;
.source "SettingsPreferenceFragment.java"


# static fields
.field private static final EXTRA_FROM_CONTACT:Ljava/lang/String; = "android.intent.extra.pattern.FROM_CONTACT"

.field private static final EXTRA_FROM_SOUND_DETECTOR:Ljava/lang/String; = "android.intent.extra.pattern.FROM_SOUND_DETECTOR"

.field private static final EXTRA_PATTERN_EXISTING_URI:Ljava/lang/String; = "android.intent.extra.pattern.EXISTING_URI"

.field private static final KEY_FLASH_NOTIFICATION:Ljava/lang/String; = "flash_notification"

.field private static final KEY_VIBRATION_PATTERNS:Ljava/lang/String; = "vibration_patterns"

.field private static final REQUEST_VIBRATION_PATTERNS:I = 0x1


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mFlashNotification:Landroid/preference/CheckBoxPreference;

.field private mVibrationPatterns:Landroid/preference/PreferenceScreen;

.field private mVibrationPatternsType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 17
    const-string v0, "SettingsPreferenceFragment"

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public handleVibrationPicked(ILandroid/net/Uri;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "pickedUri"    # Landroid/net/Uri;

    .prologue
    .line 112
    move-object v1, p2

    .line 113
    .local v1, "mVibrationUri":Landroid/net/Uri;
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 115
    .local v0, "mReceivedVibration":Ljava/lang/String;
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 123
    :goto_1
    return-void

    .line 113
    .end local v0    # "mReceivedVibration":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 117
    .restart local v0    # "mReceivedVibration":Ljava/lang/String;
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "sound_detector_vibration_pattern"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_1

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 77
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 78
    packed-switch p1, :pswitch_data_0

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 81
    :pswitch_0
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 84
    if-eqz p3, :cond_0

    .line 85
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 86
    .local v0, "uri":Landroid/net/Uri;
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->handleVibrationPicked(ILandroid/net/Uri;)V

    goto :goto_0

    .line 78
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 35
    const v0, 0x7f040001

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    .line 37
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 39
    const-string v0, "vibration_patterns"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->mVibrationPatterns:Landroid/preference/PreferenceScreen;

    .line 40
    const-string v0, "flash_notification"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->mFlashNotification:Landroid/preference/CheckBoxPreference;

    .line 42
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->updateVibrationPattern()V

    .line 43
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 5
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 53
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->mVibrationPatterns:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 54
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "sound_detector_vibration_pattern"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->mVibrationPatternsType:Ljava/lang/String;

    .line 56
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 57
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.personalvibration.SelectPatternDialog"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    const-string v1, "android.intent.extra.pattern.FROM_CONTACT"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 60
    const-string v1, "android.intent.extra.pattern.FROM_SOUND_DETECTOR"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 61
    const-string v1, "android.intent.extra.pattern.EXISTING_URI"

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->mVibrationPatternsType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 62
    invoke-virtual {p0, v0, v3}, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 72
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return v4

    .line 63
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->mFlashNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->mFlashNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-ne v1, v3, :cond_2

    .line 65
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->mFlashNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 66
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "sound_detector_flash_noti"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 68
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->mFlashNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 69
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "sound_detector_flash_noti"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->updateVibrationPattern()V

    .line 48
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 49
    return-void
.end method

.method public updateState()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 126
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "sound_detector_flash_noti"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 128
    .local v0, "flashNotification_state":I
    if-nez v0, :cond_0

    .line 129
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->mFlashNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 133
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->mFlashNotification:Landroid/preference/CheckBoxPreference;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0
.end method

.method public updateVibrationPattern()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 93
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "sound_detector_vibration_pattern"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 96
    .local v7, "temp":Ljava/lang/String;
    if-nez v7, :cond_0

    .line 97
    const-string v7, "content://com.android.settings.personalvibration.PersonalVibrationProvider/1"

    .line 98
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "sound_detector_vibration_pattern"

    const-string v3, "content://com.android.settings.personalvibration.PersonalVibrationProvider/1"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 102
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 104
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 105
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;->mVibrationPatterns:Landroid/preference/PreferenceScreen;

    const-string v1, "vibration_name"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 107
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 109
    :cond_2
    return-void
.end method
