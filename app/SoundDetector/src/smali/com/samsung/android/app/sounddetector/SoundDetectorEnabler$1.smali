.class Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$1;
.super Ljava/lang/Object;
.source "SoundDetectorEnabler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->ShowDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;

.field final synthetic val$do_not_show:Landroid/widget/CheckBox;

.field final synthetic val$edit:Landroid/content/SharedPreferences$Editor;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;Landroid/content/SharedPreferences$Editor;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$1;->this$0:Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;

    iput-object p2, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$1;->val$edit:Landroid/content/SharedPreferences$Editor;

    iput-object p3, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$1;->val$do_not_show:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x0

    .line 153
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$1;->val$edit:Landroid/content/SharedPreferences$Editor;

    const-string v1, "pref_do_not_show_again"

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$1;->val$do_not_show:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 155
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$1;->val$edit:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 156
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 157
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$1;->this$0:Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;

    # getter for: Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;
    invoke-static {v0}, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->access$000(Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;)Landroid/widget/Switch;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 159
    :cond_0
    return v3
.end method
