.class public Lcom/samsung/android/app/sounddetector/MainActivity;
.super Landroid/app/Activity;
.source "MainActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;
    }
.end annotation


# static fields
.field private static final START_SOUND_DETECT:I = 0x1


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mActionBarLayout:Landroid/view/View;

.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mDetectorDetecting:Landroid/widget/TextView;

.field private mDetectorIndication:Landroid/widget/TextView;

.field private mDetectorLevel_1:Landroid/widget/ImageView;

.field private mDetectorLevel_2:Landroid/widget/ImageView;

.field private mDetectorLevel_3:Landroid/widget/ImageView;

.field private mDetectorPause_btn:Landroid/widget/ImageButton;

.field private mDetectorStart_btn:Landroid/widget/ImageButton;

.field private final mHandler:Landroid/os/Handler;

.field private mOptionMenu:Landroid/view/Menu;

.field mServiceConnection:Landroid/content/ServiceConnection;

.field private mSoundDetectEnbaler:Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;

.field private mSoundDetectService:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

.field private mSoundDetectorObserver:Landroid/database/ContentObserver;

.field private mTalkbackObserver:Landroid/database/ContentObserver;

.field private mVisualizerReceiver:Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;

.field private main_detect_layout:Landroid/widget/FrameLayout;

.field private main_layout:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 52
    const-string v0, "MainActivity"

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->TAG:Ljava/lang/String;

    .line 80
    new-instance v0, Lcom/samsung/android/app/sounddetector/MainActivity$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/sounddetector/MainActivity$1;-><init>(Lcom/samsung/android/app/sounddetector/MainActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mSoundDetectorObserver:Landroid/database/ContentObserver;

    .line 88
    new-instance v0, Lcom/samsung/android/app/sounddetector/MainActivity$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/sounddetector/MainActivity$2;-><init>(Lcom/samsung/android/app/sounddetector/MainActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mTalkbackObserver:Landroid/database/ContentObserver;

    .line 108
    new-instance v0, Lcom/samsung/android/app/sounddetector/MainActivity$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/sounddetector/MainActivity$3;-><init>(Lcom/samsung/android/app/sounddetector/MainActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 521
    new-instance v0, Lcom/samsung/android/app/sounddetector/MainActivity$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/sounddetector/MainActivity$4;-><init>(Lcom/samsung/android/app/sounddetector/MainActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/sounddetector/MainActivity;)Lcom/samsung/android/app/sounddetector/service/SoundDetectService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/MainActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mSoundDetectService:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/app/sounddetector/MainActivity;Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Lcom/samsung/android/app/sounddetector/service/SoundDetectService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/MainActivity;
    .param p1, "x1"    # Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mSoundDetectService:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/MainActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/MainActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_layout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/MainActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_detect_layout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/MainActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/MainActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/MainActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_3:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/MainActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/MainActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorPause_btn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/MainActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorIndication:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/sounddetector/MainActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorDetecting:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public makeStringWithImage(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)Landroid/text/SpannableString;
    .locals 6
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 435
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 436
    :cond_0
    const-string v3, "Utils"

    const-string v4, "makeStringWithImage - str or drawable is null"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    :goto_0
    return-object v2

    .line 438
    :cond_1
    const-string v3, "%s"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 439
    const-string v3, "Utils"

    const-string v4, "makeStringWithImage - %s is not in str"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 443
    :cond_2
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 444
    .local v2, "ss":Landroid/text/SpannableString;
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {p2, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 446
    new-instance v1, Landroid/text/style/ImageSpan;

    const/4 v3, 0x1

    invoke-direct {v1, p2, v3}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 447
    .local v1, "span":Landroid/text/style/ImageSpan;
    const-string v3, "%s"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 448
    .local v0, "iconIndex":I
    add-int/lit8 v3, v0, 0x2

    const/16 v4, 0x12

    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 387
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 432
    :goto_0
    return-void

    .line 389
    :sswitch_0
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v5}, Landroid/media/AudioManager;->isRecordActive()Z

    move-result v2

    .line 390
    .local v2, "mic_state":Z
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v5}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v3

    .line 391
    .local v3, "music_state":Z
    invoke-static {p0}, Lcom/samsung/android/app/sounddetector/Utils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v4

    .line 393
    .local v4, "talkback_state":Z
    if-eqz v2, :cond_0

    .line 394
    const v5, 0x7f060010

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/sounddetector/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 395
    :cond_0
    if-nez v3, :cond_1

    if-eqz v4, :cond_2

    .line 396
    :cond_1
    const v5, 0x7f060011

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/sounddetector/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 398
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;

    invoke-virtual {v5, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 399
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorPause_btn:Landroid/widget/ImageButton;

    invoke-virtual {v5, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 400
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorIndication:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 401
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorDetecting:Landroid/widget/TextView;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 402
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_layout:Landroid/widget/RelativeLayout;

    const-string v6, "#005471"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 403
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorPause_btn:Landroid/widget/ImageButton;

    const v6, 0x7f020016

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 404
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorDetecting:Landroid/widget/TextView;

    const v6, 0x7f060006

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/sounddetector/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 405
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_detect_layout:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 407
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/sounddetector/MainActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 408
    new-instance v0, Landroid/content/Intent;

    const-class v5, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-direct {v0, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 409
    .local v0, "detectionIntent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v5, v7}, Lcom/samsung/android/app/sounddetector/MainActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 411
    new-instance v1, Landroid/content/Intent;

    const-string v5, "com.android.settings.action.sound_detector.start"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 412
    .local v1, "mIntent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 417
    .end local v0    # "detectionIntent":Landroid/content/Intent;
    .end local v1    # "mIntent":Landroid/content/Intent;
    .end local v2    # "mic_state":Z
    .end local v3    # "music_state":Z
    .end local v4    # "talkback_state":Z
    :sswitch_1
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;

    invoke-virtual {v5, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 418
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorPause_btn:Landroid/widget/ImageButton;

    invoke-virtual {v5, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 419
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorIndication:Landroid/widget/TextView;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 420
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorDetecting:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 421
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_layout:Landroid/widget/RelativeLayout;

    const-string v6, "#ffffff"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 422
    iget-object v5, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_detect_layout:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 424
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/sounddetector/MainActivity;->stopService(Landroid/content/Intent;)Z

    .line 426
    new-instance v1, Landroid/content/Intent;

    const-string v5, "com.android.settings.action.sound_detector.stop"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 427
    .restart local v1    # "mIntent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 387
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f090013 -> :sswitch_0
        0x7f090018 -> :sswitch_1
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 7
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 125
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 127
    const v2, 0x7f030003

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/sounddetector/MainActivity;->setContentView(I)V

    .line 129
    const v2, 0x7f090012

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_layout:Landroid/widget/RelativeLayout;

    .line 130
    const v2, 0x7f090014

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_detect_layout:Landroid/widget/FrameLayout;

    .line 132
    const v2, 0x7f090013

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;

    .line 133
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    const v2, 0x7f090018

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorPause_btn:Landroid/widget/ImageButton;

    .line 136
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorPause_btn:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    const v2, 0x7f090019

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorIndication:Landroid/widget/TextView;

    .line 139
    const v2, 0x7f060005

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/sounddetector/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02000e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/app/sounddetector/MainActivity;->makeStringWithImage(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)Landroid/text/SpannableString;

    move-result-object v1

    .line 141
    .local v1, "mIndication":Landroid/text/SpannableString;
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorIndication:Landroid/widget/TextView;

    sget-object v3, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v2, v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 143
    const v2, 0x7f09001a

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorDetecting:Landroid/widget/TextView;

    .line 145
    const v2, 0x7f090015

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_1:Landroid/widget/ImageView;

    .line 146
    const v2, 0x7f090016

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_2:Landroid/widget/ImageView;

    .line 147
    const v2, 0x7f090017

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_3:Landroid/widget/ImageView;

    .line 150
    invoke-static {p0}, Lcom/samsung/android/app/sounddetector/Utils;->getServiceTaskName(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 151
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 152
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorPause_btn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 153
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorIndication:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 154
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorDetecting:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 155
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_layout:Landroid/widget/RelativeLayout;

    const-string v3, "#ffffff"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 156
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_detect_layout:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 170
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->updateSoundDetectorScreen()V

    .line 171
    return-void

    .line 158
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 159
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorPause_btn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 160
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorIndication:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 161
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorDetecting:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 162
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_layout:Landroid/widget/RelativeLayout;

    const-string v3, "#005471"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 163
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_detect_layout:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 165
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 166
    .local v0, "detectionIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v2, v5}, Lcom/samsung/android/app/sounddetector/MainActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 167
    const-string v2, "MainActivity"

    const-string v3, "onCrete bindService"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v9, 0x10

    const/4 v12, -0x2

    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 175
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 176
    const v6, 0x7f030003

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/sounddetector/MainActivity;->setContentView(I)V

    .line 179
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    if-eqz v6, :cond_0

    const-string v6, "android.intent.action.MAIN"

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 180
    const-string v6, "MainActivity"

    const-string v7, "android.intent.action.MAIN"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    invoke-static {p0}, Lcom/samsung/android/app/sounddetector/Utils;->getServiceTaskName(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 183
    new-instance v6, Landroid/content/Intent;

    const-class v7, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-direct {v6, p0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/sounddetector/MainActivity;->stopService(Landroid/content/Intent;)Z

    .line 185
    new-instance v4, Landroid/content/Intent;

    const-string v6, "com.android.settings.action.sound_detector.stop"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 186
    .local v4, "mIntent":Landroid/content/Intent;
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/sounddetector/MainActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 190
    .end local v4    # "mIntent":Landroid/content/Intent;
    :cond_0
    new-instance v6, Landroid/widget/Switch;

    invoke-direct {v6, p0}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mActionBarSwitch:Landroid/widget/Switch;

    .line 192
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "mNotify"

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->getExtra(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 193
    .local v2, "mFromNoti":Z
    if-eqz v2, :cond_1

    .line 194
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 199
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 201
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x7f050000

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 203
    .local v5, "padding":I
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v6, v10, v10, v5, v10}, Landroid/widget/Switch;->setPadding(IIII)V

    .line 204
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6, v9, v9}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 206
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v8, Landroid/app/ActionBar$LayoutParams;

    const/16 v9, 0x15

    invoke-direct {v8, v12, v12, v9}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v6, v7, v8}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 210
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mActionBarLayout:Landroid/view/View;

    .line 211
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v6}, Landroid/widget/Switch;->requestFocus()Z

    .line 213
    new-instance v6, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;

    iget-object v7, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-direct {v6, p0, v7}, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    iput-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mSoundDetectEnbaler:Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;

    .line 215
    const-string v6, "audio"

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/sounddetector/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/media/AudioManager;

    iput-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 217
    const v6, 0x7f090012

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    iput-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_layout:Landroid/widget/RelativeLayout;

    .line 218
    const v6, 0x7f090014

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout;

    iput-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_detect_layout:Landroid/widget/FrameLayout;

    .line 220
    const v6, 0x7f090013

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    iput-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;

    .line 221
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;

    invoke-virtual {v6, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 223
    const v6, 0x7f090018

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    iput-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorPause_btn:Landroid/widget/ImageButton;

    .line 224
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorPause_btn:Landroid/widget/ImageButton;

    invoke-virtual {v6, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    const v6, 0x7f090019

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorIndication:Landroid/widget/TextView;

    .line 227
    const v6, 0x7f060005

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/sounddetector/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f02000e

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lcom/samsung/android/app/sounddetector/MainActivity;->makeStringWithImage(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)Landroid/text/SpannableString;

    move-result-object v3

    .line 229
    .local v3, "mIndication":Landroid/text/SpannableString;
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorIndication:Landroid/widget/TextView;

    sget-object v7, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v6, v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 231
    const v6, 0x7f09001a

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorDetecting:Landroid/widget/TextView;

    .line 233
    const v6, 0x7f090015

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_1:Landroid/widget/ImageView;

    .line 234
    const v6, 0x7f090016

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_2:Landroid/widget/ImageView;

    .line 235
    const v6, 0x7f090017

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/sounddetector/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorLevel_3:Landroid/widget/ImageView;

    .line 238
    invoke-static {p0}, Lcom/samsung/android/app/sounddetector/Utils;->getServiceTaskName(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 239
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;

    invoke-virtual {v6, v10}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 240
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorPause_btn:Landroid/widget/ImageButton;

    invoke-virtual {v6, v11}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 241
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorIndication:Landroid/widget/TextView;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 242
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorDetecting:Landroid/widget/TextView;

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 243
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_layout:Landroid/widget/RelativeLayout;

    const-string v7, "#ffffff"

    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 244
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_detect_layout:Landroid/widget/FrameLayout;

    invoke-virtual {v6, v11}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 258
    :goto_1
    new-instance v6, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;

    const/4 v7, 0x0

    invoke-direct {v6, p0, v7}, Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;-><init>(Lcom/samsung/android/app/sounddetector/MainActivity;Lcom/samsung/android/app/sounddetector/MainActivity$1;)V

    iput-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mVisualizerReceiver:Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;

    .line 259
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 260
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    const-string v6, "visualization_detecting"

    invoke-virtual {v1, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 261
    const-string v6, "stop_sounddetect_service"

    invoke-virtual {v1, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 262
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mVisualizerReceiver:Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;

    invoke-virtual {p0, v6, v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 264
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->updateSoundDetectorScreen()V

    .line 266
    const-string v6, "MainActivity"

    const-string v7, "onCreate"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    return-void

    .line 196
    .end local v1    # "intentFilter":Landroid/content/IntentFilter;
    .end local v3    # "mIndication":Landroid/text/SpannableString;
    .end local v5    # "padding":I
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto/16 :goto_0

    .line 246
    .restart local v3    # "mIndication":Landroid/text/SpannableString;
    .restart local v5    # "padding":I
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;

    invoke-virtual {v6, v11}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 247
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorPause_btn:Landroid/widget/ImageButton;

    invoke-virtual {v6, v10}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 248
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorIndication:Landroid/widget/TextView;

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 249
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorDetecting:Landroid/widget/TextView;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 250
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_layout:Landroid/widget/RelativeLayout;

    const-string v7, "#005471"

    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 251
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_detect_layout:Landroid/widget/FrameLayout;

    invoke-virtual {v6, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 253
    new-instance v0, Landroid/content/Intent;

    const-class v6, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-direct {v0, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 254
    .local v0, "detectionIntent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v6, v10}, Lcom/samsung/android/app/sounddetector/MainActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 255
    const-string v6, "MainActivity"

    const-string v7, "onCrete bindService"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 349
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mOptionMenu:Landroid/view/Menu;

    .line 350
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->updateOption()V

    .line 351
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mSoundDetectService:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/MainActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 335
    const-string v0, "MainActivity"

    const-string v1, "onDestroy UnbindService"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mActionBarSwitch:Landroid/widget/Switch;

    .line 339
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mVisualizerReceiver:Lcom/samsung/android/app/sounddetector/MainActivity$VisualizerReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 341
    const-string v0, "MainActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 344
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 366
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 382
    :goto_0
    const/4 v1, 0x0

    return v1

    .line 368
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->onBackPressed()V

    goto :goto_0

    .line 371
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/app/sounddetector/database/RecentAlertActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 372
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 375
    .end local v0    # "i":Landroid/content/Intent;
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/app/sounddetector/setting/SettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 376
    .restart local v0    # "i":Landroid/content/Intent;
    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/samsung/android/app/sounddetector/setting/SettingsPreferenceFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 377
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/sounddetector/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 366
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_2
        0x102002c -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 315
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mActionBarSwitch:Landroid/widget/Switch;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 316
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mSoundDetectEnbaler:Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;

    invoke-virtual {v0}, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->pause()V

    .line 318
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mSoundDetectorObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 319
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mTalkbackObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 321
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mSoundDetectService:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mSoundDetectService:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-virtual {v0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->stopSoundDetect()V

    .line 323
    const-string v0, "MainActivity"

    const-string v1, "onPause Bind Call stopDetect"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    :cond_0
    const-string v0, "MainActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 329
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 271
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 272
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mSoundDetectEnbaler:Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;

    invoke-virtual {v0}, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->resume()V

    .line 273
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v4}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 276
    invoke-static {p0}, Lcom/samsung/android/app/sounddetector/Utils;->getServiceTaskName(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 277
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 278
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorPause_btn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 279
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorIndication:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 280
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorDetecting:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 281
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_layout:Landroid/widget/RelativeLayout;

    const-string v1, "#ffffff"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 282
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_detect_layout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 292
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mSoundDetectService:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mSoundDetectService:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-virtual {v0}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->startSoundDetect()V

    .line 294
    const-string v0, "MainActivity"

    const-string v1, "onResume Bind Call startDetect"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "sound_detector"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mSoundDetectorObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 301
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accessibility_enabled"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mTalkbackObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 304
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "enabled_accessibility_services"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mTalkbackObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 308
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->updateSoundDetectorScreen()V

    .line 310
    const-string v0, "MainActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    return-void

    .line 284
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 285
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorPause_btn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 286
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorIndication:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 287
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorDetecting:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 288
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_layout:Landroid/widget/RelativeLayout;

    const-string v1, "#005471"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 289
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_detect_layout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateMoreOptions(Z)V
    .locals 5
    .param p1, "isTalkback"    # Z

    .prologue
    const/4 v4, 0x0

    .line 506
    move v0, p1

    .line 508
    .local v0, "state":Z
    if-eqz v0, :cond_1

    .line 509
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mOptionMenu:Landroid/view/Menu;

    if-eqz v1, :cond_0

    .line 510
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mOptionMenu:Landroid/view/Menu;

    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    .line 519
    :cond_0
    :goto_0
    return-void

    .line 513
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mOptionMenu:Landroid/view/Menu;

    if-eqz v1, :cond_0

    .line 514
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mOptionMenu:Landroid/view/Menu;

    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    .line 515
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mOptionMenu:Landroid/view/Menu;

    const v2, 0x7f060002

    invoke-interface {v1, v4, v4, v4, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 516
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mOptionMenu:Landroid/view/Menu;

    const/4 v2, 0x1

    const v3, 0x7f060003

    invoke-interface {v1, v4, v2, v4, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public updateOption()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 355
    invoke-static {p0}, Lcom/samsung/android/app/sounddetector/Utils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v0

    .line 356
    .local v0, "talkback_state":Z
    if-eqz v0, :cond_0

    .line 357
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mOptionMenu:Landroid/view/Menu;

    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    .line 362
    :goto_0
    return-void

    .line 359
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mOptionMenu:Landroid/view/Menu;

    const v2, 0x7f060002

    invoke-interface {v1, v4, v4, v4, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 360
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mOptionMenu:Landroid/view/Menu;

    const/4 v2, 0x1

    const v3, 0x7f060003

    invoke-interface {v1, v4, v2, v4, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public updateSoundDetectorScreen()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 484
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "sound_detector"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 486
    .local v0, "state":I
    if-nez v0, :cond_1

    .line 487
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 488
    invoke-static {p0}, Lcom/samsung/android/app/sounddetector/Utils;->getServiceTaskName(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 489
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 490
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorPause_btn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 491
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorIndication:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 492
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorDetecting:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 493
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_layout:Landroid/widget/RelativeLayout;

    const-string v2, "#ffffff"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 494
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->main_detect_layout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 496
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->stopService(Landroid/content/Intent;)Z

    .line 498
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 503
    :goto_0
    return-void

    .line 500
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v5}, Landroid/widget/Switch;->setChecked(Z)V

    .line 501
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/MainActivity;->mDetectorStart_btn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method
