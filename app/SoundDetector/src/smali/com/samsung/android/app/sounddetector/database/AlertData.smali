.class public Lcom/samsung/android/app/sounddetector/database/AlertData;
.super Ljava/lang/Object;
.source "AlertData.java"


# instance fields
.field private alertDate:Ljava/lang/String;

.field private alertTime:Ljava/lang/String;

.field private mIndex:I

.field private soundType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "idx"    # I
    .param p2, "type"    # I
    .param p3, "time"    # Ljava/lang/String;
    .param p4, "date"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput p1, p0, Lcom/samsung/android/app/sounddetector/database/AlertData;->mIndex:I

    .line 14
    iput p2, p0, Lcom/samsung/android/app/sounddetector/database/AlertData;->soundType:I

    .line 15
    iput-object p3, p0, Lcom/samsung/android/app/sounddetector/database/AlertData;->alertTime:Ljava/lang/String;

    .line 16
    iput-object p4, p0, Lcom/samsung/android/app/sounddetector/database/AlertData;->alertDate:Ljava/lang/String;

    .line 17
    return-void
.end method


# virtual methods
.method public getAlertDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/AlertData;->alertDate:Ljava/lang/String;

    return-object v0
.end method

.method public getAlertTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/AlertData;->alertTime:Ljava/lang/String;

    return-object v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/samsung/android/app/sounddetector/database/AlertData;->mIndex:I

    return v0
.end method

.method public getSoundType()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/samsung/android/app/sounddetector/database/AlertData;->soundType:I

    return v0
.end method

.method public setAlertDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/database/AlertData;->alertDate:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setAlertTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/database/AlertData;->alertTime:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public setIndex(I)V
    .locals 0
    .param p1, "idx"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/samsung/android/app/sounddetector/database/AlertData;->mIndex:I

    .line 33
    return-void
.end method

.method public setSoundType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 20
    iput p1, p0, Lcom/samsung/android/app/sounddetector/database/AlertData;->soundType:I

    .line 21
    return-void
.end method
