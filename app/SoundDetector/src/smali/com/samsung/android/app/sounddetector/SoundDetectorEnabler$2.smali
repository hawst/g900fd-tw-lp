.class Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$2;
.super Ljava/lang/Object;
.source "SoundDetectorEnabler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->ShowDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;

.field final synthetic val$do_not_show:Landroid/widget/CheckBox;

.field final synthetic val$edit:Landroid/content/SharedPreferences$Editor;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;Landroid/content/SharedPreferences$Editor;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$2;->this$0:Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;

    iput-object p2, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$2;->val$edit:Landroid/content/SharedPreferences$Editor;

    iput-object p3, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$2;->val$do_not_show:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 145
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$2;->val$edit:Landroid/content/SharedPreferences$Editor;

    const-string v1, "pref_do_not_show_again"

    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$2;->val$do_not_show:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 146
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$2;->val$edit:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 147
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 148
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler$2;->this$0:Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;

    # getter for: Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->mSwitch:Landroid/widget/Switch;
    invoke-static {v0}, Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;->access$000(Lcom/samsung/android/app/sounddetector/SoundDetectorEnabler;)Landroid/widget/Switch;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 149
    return-void
.end method
