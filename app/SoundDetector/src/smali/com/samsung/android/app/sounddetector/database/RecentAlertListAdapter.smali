.class public Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "RecentAlertListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/samsung/android/app/sounddetector/database/AlertData;",
        ">;"
    }
.end annotation


# static fields
.field private static final NORMAL_MODE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "RecentAlertListAdapter"


# instance fields
.field private mAlertCheckBox:Landroid/widget/CheckBox;

.field private mAlertHeader:Landroid/widget/TextView;

.field private mAlertTime:Landroid/widget/TextView;

.field private mAlertTitle:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mMode:I

.field private mResource:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/sounddetector/database/AlertData;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedItemsIds:Landroid/util/SparseBooleanArray;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "mode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/sounddetector/database/AlertData;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p2, "resource":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/sounddetector/database/AlertData;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 37
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 39
    iput-object p2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mResource:Ljava/util/ArrayList;

    .line 40
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mContext:Landroid/content/Context;

    .line 41
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mSelectedItemsIds:Landroid/util/SparseBooleanArray;

    .line 42
    iput p3, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mMode:I

    .line 43
    return-void
.end method


# virtual methods
.method public deSelectionAll(I)V
    .locals 5
    .param p1, "size"    # I

    .prologue
    .line 148
    const/4 v0, 0x0

    .line 149
    .local v0, "data":Lcom/samsung/android/app/sounddetector/database/AlertData;
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->removeSelection()V

    .line 151
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_1

    .line 152
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "data":Lcom/samsung/android/app/sounddetector/database/AlertData;
    check-cast v0, Lcom/samsung/android/app/sounddetector/database/AlertData;

    .line 153
    .restart local v0    # "data":Lcom/samsung/android/app/sounddetector/database/AlertData;
    const-string v2, "RecentAlertListAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getItem() idx : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    invoke-virtual {v0}, Lcom/samsung/android/app/sounddetector/database/AlertData;->getAlertTime()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 155
    iget-object v2, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mSelectedItemsIds:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseBooleanArray;->delete(I)V

    .line 151
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 159
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->notifyDataSetChanged()V

    .line 160
    return-void
.end method

.method public getSelectedCount()I
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mSelectedItemsIds:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    return v0
.end method

.method public getSelectedIds()Landroid/util/SparseBooleanArray;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mSelectedItemsIds:Landroid/util/SparseBooleanArray;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v3, 0x0

    .line 48
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/sounddetector/database/AlertData;

    .line 50
    .local v0, "data":Lcom/samsung/android/app/sounddetector/database/AlertData;
    invoke-virtual {v0}, Lcom/samsung/android/app/sounddetector/database/AlertData;->getAlertTime()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 51
    iget v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mMode:I

    if-nez v1, :cond_1

    .line 52
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03000a

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 59
    :goto_0
    const v1, 0x7f09002d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mAlertTitle:Landroid/widget/TextView;

    .line 61
    const v1, 0x7f09002e

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mAlertTime:Landroid/widget/TextView;

    .line 62
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mAlertTime:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/samsung/android/app/sounddetector/database/AlertData;->getAlertTime()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mSelectedItemsIds:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mAlertCheckBox:Landroid/widget/CheckBox;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 74
    :cond_0
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 76
    return-object p2

    .line 54
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030009

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 56
    const v1, 0x7f09002c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mAlertCheckBox:Landroid/widget/CheckBox;

    goto :goto_0

    .line 64
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03000b

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 66
    const v1, 0x7f09002f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mAlertHeader:Landroid/widget/TextView;

    .line 67
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mAlertHeader:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/samsung/android/app/sounddetector/database/AlertData;->getAlertDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public isEnabled(I)Z
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/sounddetector/database/AlertData;

    .line 83
    .local v0, "data":Lcom/samsung/android/app/sounddetector/database/AlertData;
    const/4 v1, 0x0

    .line 85
    .local v1, "mIsEnabled":Z
    invoke-virtual {v0}, Lcom/samsung/android/app/sounddetector/database/AlertData;->getAlertTime()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 86
    const/4 v1, 0x1

    .line 89
    :cond_0
    return v1
.end method

.method public isHeader(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 110
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/sounddetector/database/AlertData;

    .line 112
    .local v0, "data":Lcom/samsung/android/app/sounddetector/database/AlertData;
    invoke-virtual {v0}, Lcom/samsung/android/app/sounddetector/database/AlertData;->getAlertTime()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 113
    const/4 v1, 0x0

    .line 115
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 94
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 95
    return-void
.end method

.method public removeSelection()V
    .locals 1

    .prologue
    .line 98
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mSelectedItemsIds:Landroid/util/SparseBooleanArray;

    .line 99
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->notifyDataSetChanged()V

    .line 100
    return-void
.end method

.method public selectView(IZ)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "value"    # Z

    .prologue
    .line 120
    if-eqz p2, :cond_0

    .line 121
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mSelectedItemsIds:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 125
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->notifyDataSetChanged()V

    .line 126
    return-void

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mSelectedItemsIds:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->delete(I)V

    goto :goto_0
.end method

.method public selectionAll(I)V
    .locals 7
    .param p1, "size"    # I

    .prologue
    .line 129
    const/4 v1, 0x0

    .line 130
    .local v1, "data":Lcom/samsung/android/app/sounddetector/database/AlertData;
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->removeSelection()V

    .line 133
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, p1, :cond_1

    .line 134
    :try_start_0
    invoke-virtual {p0, v3}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/samsung/android/app/sounddetector/database/AlertData;

    move-object v1, v0

    .line 135
    const-string v4, "RecentAlertListAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getItem() idx : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/database/AlertData;->getAlertTime()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 137
    iget-object v4, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mSelectedItemsIds:Landroid/util/SparseBooleanArray;

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 140
    :catch_0
    move-exception v2

    .line 141
    .local v2, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v4, "RecentAlertListAdapter"

    const-string v5, "IndexOutOfBoundsException has been occured!"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    .end local v2    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->notifyDataSetChanged()V

    .line 145
    return-void
.end method

.method public setMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 171
    iput p1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mMode:I

    .line 172
    return-void
.end method

.method public toggleSelection(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 103
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/sounddetector/database/AlertData;

    .line 104
    .local v0, "data":Lcom/samsung/android/app/sounddetector/database/AlertData;
    invoke-virtual {v0}, Lcom/samsung/android/app/sounddetector/database/AlertData;->getAlertTime()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 105
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->mSelectedItemsIds:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {p0, p1, v1}, Lcom/samsung/android/app/sounddetector/database/RecentAlertListAdapter;->selectView(IZ)V

    .line 107
    :cond_0
    return-void

    .line 105
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
