.class Lcom/samsung/android/app/sounddetector/MainActivity$3;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/sounddetector/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/MainActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/sounddetector/MainActivity;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/MainActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    check-cast p2, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$DetectBinder;

    .end local p2    # "service":Landroid/os/IBinder;
    invoke-virtual {p2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$DetectBinder;->getService()Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    move-result-object v1

    # setter for: Lcom/samsung/android/app/sounddetector/MainActivity;->mSoundDetectService:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;
    invoke-static {v0, v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$002(Lcom/samsung/android/app/sounddetector/MainActivity;Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    .line 112
    const-string v0, "MainActivity"

    const-string v1, "Bind service connected"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    # getter for: Lcom/samsung/android/app/sounddetector/MainActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$100(Lcom/samsung/android/app/sounddetector/MainActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 114
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/android/app/sounddetector/MainActivity$3;->this$0:Lcom/samsung/android/app/sounddetector/MainActivity;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/sounddetector/MainActivity;->mSoundDetectService:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;
    invoke-static {v0, v1}, Lcom/samsung/android/app/sounddetector/MainActivity;->access$002(Lcom/samsung/android/app/sounddetector/MainActivity;Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    .line 119
    const-string v0, "MainActivity"

    const-string v1, "Bind service disconnected"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    return-void
.end method
