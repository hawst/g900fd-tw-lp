.class public Lcom/samsung/android/app/AED;
.super Ljava/lang/Object;
.source "AED.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init()V
    .locals 3

    .prologue
    .line 14
    :try_start_0
    const-string v1, "AED"

    const-string v2, "Trying to load libaed.so"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 15
    const-string v1, "aed"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 16
    const-string v1, "AED"

    const-string v2, "Loading libaed.so"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 19
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 21
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "AED"

    const-string v2, "WARNING: Could not load libaed.so"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public native AedExe([SI[I[I)I
.end method

.method public native AedInit(Landroid/content/res/AssetManager;Ljava/lang/String;I)I
.end method
