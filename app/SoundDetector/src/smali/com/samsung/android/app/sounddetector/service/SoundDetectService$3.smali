.class Lcom/samsung/android/app/sounddetector/service/SoundDetectService$3;
.super Landroid/content/BroadcastReceiver;
.source "SoundDetectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/sounddetector/service/SoundDetectService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)V
    .locals 0

    .prologue
    .line 420
    iput-object p1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$3;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 423
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 425
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.samsung.android.app.sounddetector.VOICE_WAKEUP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 426
    const-string v1, "SoundDetectService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BroadcastReceive : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$3;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    invoke-virtual {v1}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->startSoundDetect()V

    .line 429
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$3;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    new-instance v2, Ljava/util/Timer;

    invoke-direct {v2}, Ljava/util/Timer;-><init>()V

    # setter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->timer:Ljava/util/Timer;
    invoke-static {v1, v2}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$702(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;Ljava/util/Timer;)Ljava/util/Timer;

    .line 430
    iget-object v1, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$3;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    # getter for: Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->timer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService;->access$700(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;)Ljava/util/Timer;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;

    iget-object v3, p0, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$3;->this$0:Lcom/samsung/android/app/sounddetector/service/SoundDetectService;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/app/sounddetector/service/SoundDetectService$MyTimer;-><init>(Lcom/samsung/android/app/sounddetector/service/SoundDetectService;Lcom/samsung/android/app/sounddetector/service/SoundDetectService$1;)V

    const-wide/16 v4, 0x3a98

    invoke-virtual {v1, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 432
    :cond_0
    return-void
.end method
