.class public Lcom/sec/android/app/video/VPTrimApp;
.super Landroid/app/AppGlobals;
.source "VPTrimApp.java"


# static fields
.field public static gContext:Landroid/content/Context;

.field public static gInflator:Landroid/view/LayoutInflater;

.field public static gResize:Lcom/samsung/app/video/resize/ResizeInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    new-instance v0, Lcom/samsung/app/video/resize/Resize;

    invoke-direct {v0}, Lcom/samsung/app/video/resize/Resize;-><init>()V

    sput-object v0, Lcom/sec/android/app/video/VPTrimApp;->gResize:Lcom/samsung/app/video/resize/ResizeInterface;

    .line 13
    invoke-static {}, Lcom/sec/android/app/video/VPTrimApp;->getInitialApplication()Landroid/app/Application;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/video/VPTrimApp;->gContext:Landroid/content/Context;

    .line 14
    sget-object v0, Lcom/sec/android/app/video/VPTrimApp;->gContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/video/VPTrimApp;->gInflator:Landroid/view/LayoutInflater;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/AppGlobals;-><init>()V

    return-void
.end method

.method public static getStringValue(I)Ljava/lang/String;
    .locals 1
    .param p0, "resId"    # I

    .prologue
    .line 18
    sget-object v0, Lcom/sec/android/app/video/VPTrimApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static showToast(IIII)V
    .locals 3
    .param p0, "aId"    # I
    .param p1, "gravity"    # I
    .param p2, "xOff"    # I
    .param p3, "yOff"    # I

    .prologue
    .line 23
    sget-object v1, Lcom/sec/android/app/video/VPTrimApp;->gContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2, p3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 24
    .local v0, "toast":Landroid/widget/Toast;
    invoke-virtual {v0, p0}, Landroid/widget/Toast;->setText(I)V

    .line 25
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 26
    return-void
.end method
