.class Lcom/sec/android/app/video/Utils$2;
.super Ljava/lang/Object;
.source "Utils.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/video/Utils;->getLengthFilterWithToast(Landroid/content/Context;II)Landroid/text/InputFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$length:I


# direct methods
.method constructor <init>(I)V
    .locals 0

    .prologue
    .line 1
    iput p1, p0, Lcom/sec/android/app/video/Utils$2;->val$length:I

    .line 240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 7
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 246
    :try_start_0
    iget v3, p0, Lcom/sec/android/app/video/Utils$2;->val$length:I

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v4

    sub-int v5, p6, p5

    sub-int/2addr v4, v5

    sub-int v1, v3, v4

    .line 247
    .local v1, "spaceAvailable":I
    if-gtz v1, :cond_1

    .line 248
    # invokes: Lcom/sec/android/app/video/Utils;->showToastMaxChars()V
    invoke-static {}, Lcom/sec/android/app/video/Utils;->access$0()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    const-string v2, ""

    .line 262
    .end local v1    # "spaceAvailable":I
    :cond_0
    :goto_0
    return-object v2

    .line 250
    .restart local v1    # "spaceAvailable":I
    :cond_1
    if-ne v1, v6, :cond_2

    :try_start_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-le v3, v6, :cond_2

    .line 251
    # invokes: Lcom/sec/android/app/video/Utils;->showToastMaxChars()V
    invoke-static {}, Lcom/sec/android/app/video/Utils;->access$0()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 260
    .end local v1    # "spaceAvailable":I
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 253
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "spaceAvailable":I
    :cond_2
    sub-int v3, p3, p2

    if-ge v1, v3, :cond_0

    .line 256
    add-int/2addr v1, p2

    .line 257
    :try_start_2
    # invokes: Lcom/sec/android/app/video/Utils;->showToastMaxChars()V
    invoke-static {}, Lcom/sec/android/app/video/Utils;->access$0()V

    .line 258
    invoke-interface {p1, p2, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v2

    goto :goto_0
.end method
