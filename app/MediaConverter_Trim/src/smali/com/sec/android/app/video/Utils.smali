.class public Lcom/sec/android/app/video/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field private static final INVALID_CHAR:[Ljava/lang/String;

.field private static final LOW_STORAGE_THRESHOLD:J = 0xa00000L

.field private static mMaxCharToastHandler:Landroid/os/Handler;

.field private static mToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/video/Utils;->mToast:Landroid/widget/Toast;

    .line 33
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 34
    const-string v2, "\\"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ":"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "*"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "?"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, ";"

    aput-object v2, v0, v1

    .line 33
    sput-object v0, Lcom/sec/android/app/video/Utils;->INVALID_CHAR:[Ljava/lang/String;

    .line 231
    new-instance v0, Lcom/sec/android/app/video/Utils$1;

    invoke-direct {v0}, Lcom/sec/android/app/video/Utils$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/video/Utils;->mMaxCharToastHandler:Landroid/os/Handler;

    .line 237
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()V
    .locals 0

    .prologue
    .line 224
    invoke-static {}, Lcom/sec/android/app/video/Utils;->showToastMaxChars()V

    return-void
.end method

.method static synthetic access$1()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/android/app/video/Utils;->INVALID_CHAR:[Ljava/lang/String;

    return-object v0
.end method

.method public static checkStorage()Z
    .locals 7

    .prologue
    .line 39
    new-instance v2, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    .line 40
    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    .line 39
    invoke-direct {v2, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 41
    .local v2, "stat":Landroid/os/StatFs;
    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v5

    int-to-long v5, v5

    mul-long v0, v3, v5

    .line 42
    .local v0, "bytesAvailable":J
    const-wide/32 v3, 0xa00000

    cmp-long v3, v0, v3

    if-gez v3, :cond_0

    .line 43
    const/4 v3, 0x1

    .line 45
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "length"    # I

    .prologue
    const/16 v1, 0x32

    .line 272
    const/4 v2, 0x3

    new-array v0, v2, [Landroid/text/InputFilter;

    .line 273
    .local v0, "FilterArray":[Landroid/text/InputFilter;
    if-le p1, v1, :cond_0

    .line 274
    :goto_0
    const/4 v1, 0x0

    const v2, 0x7f060006

    invoke-static {p0, p1, v2}, Lcom/sec/android/app/video/Utils;->getLengthFilterWithToast(Landroid/content/Context;II)Landroid/text/InputFilter;

    move-result-object v2

    aput-object v2, v0, v1

    .line 275
    const/4 v1, 0x1

    invoke-static {p0}, Lcom/sec/android/app/video/Utils;->getEmoticonFilterWithToast(Landroid/content/Context;)Landroid/text/InputFilter;

    move-result-object v2

    aput-object v2, v0, v1

    .line 276
    const/4 v1, 0x2

    new-instance v2, Lcom/sec/android/app/video/Utils$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/video/Utils$3;-><init>(Landroid/content/Context;)V

    aput-object v2, v0, v1

    .line 317
    return-object v0

    :cond_0
    move p1, v1

    .line 273
    goto :goto_0
.end method

.method private static getEmoticonFilterWithToast(Landroid/content/Context;)Landroid/text/InputFilter;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 320
    new-instance v0, Lcom/sec/android/app/video/Utils$4;

    invoke-direct {v0}, Lcom/sec/android/app/video/Utils$4;-><init>()V

    .line 451
    .local v0, "emoticonFilter":Landroid/text/InputFilter;
    return-object v0
.end method

.method public static getImageFilePathByUri(Landroid/net/Uri;)Ljava/lang/String;
    .locals 11
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 464
    const/4 v9, 0x0

    .line 465
    .local v9, "lFilepath":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 467
    .local v2, "cols":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 470
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/video/VEApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 472
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 473
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 475
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 482
    :cond_0
    if-eqz v6, :cond_1

    .line 483
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object v10, v9

    .line 487
    .end local v9    # "lFilepath":Ljava/lang/String;
    .local v10, "lFilepath":Ljava/lang/String;
    :goto_0
    return-object v10

    .line 477
    .end local v10    # "lFilepath":Ljava/lang/String;
    .restart local v9    # "lFilepath":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 482
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    if-eqz v6, :cond_2

    .line 483
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v10, v9

    .line 478
    .end local v9    # "lFilepath":Ljava/lang/String;
    .restart local v10    # "lFilepath":Ljava/lang/String;
    goto :goto_0

    .line 479
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v10    # "lFilepath":Ljava/lang/String;
    .restart local v9    # "lFilepath":Ljava/lang/String;
    :catch_1
    move-exception v8

    .line 482
    .local v8, "ex":Ljava/lang/UnsupportedOperationException;
    if-eqz v6, :cond_3

    .line 483
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v10, v9

    .line 480
    .end local v9    # "lFilepath":Ljava/lang/String;
    .restart local v10    # "lFilepath":Ljava/lang/String;
    goto :goto_0

    .line 481
    .end local v8    # "ex":Ljava/lang/UnsupportedOperationException;
    .end local v10    # "lFilepath":Ljava/lang/String;
    .restart local v9    # "lFilepath":Ljava/lang/String;
    :catchall_0
    move-exception v0

    .line 482
    if-eqz v6, :cond_4

    .line 483
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 485
    :cond_4
    throw v0
.end method

.method private static getLengthFilterWithToast(Landroid/content/Context;II)Landroid/text/InputFilter;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "length"    # I
    .param p2, "stringResource"    # I

    .prologue
    .line 240
    new-instance v0, Lcom/sec/android/app/video/Utils$2;

    invoke-direct {v0, p1}, Lcom/sec/android/app/video/Utils$2;-><init>(I)V

    .line 267
    .local v0, "inputFilter":Landroid/text/InputFilter;
    return-object v0
.end method

.method public static getNextAvailableName(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 513
    const/4 v1, 0x1

    .line 514
    .local v1, "index":I
    const/4 v2, 0x0

    .line 516
    .local v2, "mMatched":Z
    const/16 v4, 0x2f

    invoke-virtual {p0, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 517
    .local v3, "name":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x2e

    invoke-virtual {v3, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    invoke-virtual {v3, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%03d"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 519
    :goto_0
    new-instance v0, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v5, Lcom/samsung/app/video/trimactivity/TrimActivity;->OUTPUT_PATH:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".mp4"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 520
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v4, 0x3e7

    if-eq v1, v4, :cond_0

    .line 521
    add-int/lit8 v1, v1, 0x1

    .line 522
    const/4 v2, 0x1

    .line 524
    :cond_0
    if-nez v2, :cond_2

    .line 530
    invoke-static {v3}, Lcom/sec/android/app/video/Utils;->isSpecialCharactersPresent(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 531
    const-string v3, ""

    .line 532
    :cond_1
    return-object v3

    .line 527
    :cond_2
    const/4 v2, 0x0

    .line 528
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x5f

    invoke-virtual {v3, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v3, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "%03d"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 518
    goto :goto_0
.end method

.method public static getRetrieverForSource(Ljava/lang/String;)Landroid/media/MediaMetadataRetriever;
    .locals 5
    .param p0, "Uri"    # Ljava/lang/String;

    .prologue
    .line 134
    new-instance v3, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v3}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 135
    .local v3, "retriever":Landroid/media/MediaMetadataRetriever;
    const/4 v1, 0x0

    .line 137
    .local v1, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .local v2, "fis":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 142
    if-eqz v2, :cond_2

    .line 144
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v1, v2

    .line 150
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    :cond_0
    :goto_0
    return-object v3

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 142
    if-eqz v1, :cond_0

    .line 144
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 145
    :catch_1
    move-exception v0

    .line 146
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 141
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 142
    :goto_2
    if-eqz v1, :cond_1

    .line 144
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 149
    :cond_1
    :goto_3
    throw v4

    .line 145
    :catch_2
    move-exception v0

    .line 146
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 145
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :catch_3
    move-exception v0

    .line 146
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .line 141
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .line 139
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public static getVideoDuration(Landroid/media/MediaMetadataRetriever;Ljava/lang/String;)I
    .locals 6
    .param p0, "retriever"    # Landroid/media/MediaMetadataRetriever;
    .param p1, "Uri"    # Ljava/lang/String;

    .prologue
    .line 170
    const/4 v0, 0x0

    .line 171
    .local v0, "duration":I
    if-nez p0, :cond_0

    .line 172
    new-instance p0, Landroid/media/MediaMetadataRetriever;

    .end local p0    # "retriever":Landroid/media/MediaMetadataRetriever;
    invoke-direct {p0}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 173
    .restart local p0    # "retriever":Landroid/media/MediaMetadataRetriever;
    const/4 v3, 0x0

    .line 175
    .local v3, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v4, "fis":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v5

    invoke-virtual {p0, v5}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 180
    if-eqz v4, :cond_0

    .line 182
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 189
    .end local v4    # "fis":Ljava/io/FileInputStream;
    :cond_0
    :goto_0
    if-eqz p0, :cond_2

    .line 192
    const/16 v5, 0x9

    .line 191
    :try_start_3
    invoke-virtual {p0, v5}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    move-result v0

    move v1, v0

    .line 198
    .end local v0    # "duration":I
    .local v1, "duration":I
    :goto_1
    return v1

    .line 177
    .end local v1    # "duration":I
    .restart local v0    # "duration":I
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catch_0
    move-exception v2

    .line 178
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_4
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 180
    if-eqz v3, :cond_0

    .line 182
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    .line 183
    :catch_1
    move-exception v2

    .line 184
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 179
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 180
    :goto_3
    if-eqz v3, :cond_1

    .line 182
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 187
    :cond_1
    :goto_4
    throw v5

    .line 183
    :catch_2
    move-exception v2

    .line 184
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 183
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_3
    move-exception v2

    .line 184
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 194
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    :catch_4
    move-exception v2

    .line 195
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    move v1, v0

    .line 198
    .end local v0    # "duration":I
    .restart local v1    # "duration":I
    goto :goto_1

    .line 179
    .end local v1    # "duration":I
    .restart local v0    # "duration":I
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .line 177
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_5
    move-exception v2

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method public static getVideoDurationByPath(Ljava/lang/String;)J
    .locals 9
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 102
    const-wide/16 v2, 0x0

    .line 103
    .local v2, "id":J
    if-nez p0, :cond_0

    move-wide v4, v2

    .line 129
    .end local v2    # "id":J
    .local v4, "id":J
    :goto_0
    return-wide v4

    .line 107
    .end local v4    # "id":J
    .restart local v2    # "id":J
    :cond_0
    new-instance v6, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v6}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 108
    .local v6, "retriever":Landroid/media/MediaMetadataRetriever;
    const/4 v0, 0x0

    .line 111
    .local v0, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    .end local v0    # "fis":Ljava/io/FileInputStream;
    .local v1, "fis":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;)V

    .line 113
    const/16 v7, 0x9

    invoke-virtual {v6, v7}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v7

    int-to-long v2, v7

    .line 121
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 122
    invoke-virtual {v6}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 123
    const/4 v6, 0x0

    move-object v0, v1

    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v0    # "fis":Ljava/io/FileInputStream;
    :goto_1
    move-wide v4, v2

    .line 129
    .end local v2    # "id":J
    .restart local v4    # "id":J
    goto :goto_0

    .line 116
    .end local v4    # "id":J
    .restart local v2    # "id":J
    :catch_0
    move-exception v7

    .line 121
    :goto_2
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    .line 122
    invoke-virtual {v6}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 123
    const/4 v6, 0x0

    goto :goto_1

    .line 119
    :catchall_0
    move-exception v7

    .line 121
    :goto_3
    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    .line 122
    invoke-virtual {v6}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 123
    const/4 v6, 0x0

    .line 127
    :goto_4
    throw v7

    .line 124
    .end local v0    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v7

    move-object v0, v1

    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v0    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    :catch_2
    move-exception v8

    goto :goto_4

    .line 119
    .end local v0    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v7

    move-object v0, v1

    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v0    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .line 124
    :catch_3
    move-exception v7

    goto :goto_1

    .line 116
    .end local v0    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    :catch_4
    move-exception v7

    move-object v0, v1

    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v0    # "fis":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method public static getVideoFileInfoByUri(Landroid/net/Uri;Landroid/content/Context;)Landroid/database/Cursor;
    .locals 10
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 84
    const-string v1, "_data"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "duration"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "resolution"

    aput-object v1, v2, v0

    .line 87
    .local v2, "cols":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 90
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 91
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    move-object v7, v6

    .line 98
    .end local v6    # "c":Landroid/database/Cursor;
    .local v7, "c":Landroid/database/Cursor;
    :goto_0
    return-object v7

    .line 92
    .end local v7    # "c":Landroid/database/Cursor;
    .restart local v6    # "c":Landroid/database/Cursor;
    :catch_0
    move-exception v8

    .local v8, "e":Landroid/database/sqlite/SQLiteException;
    move-object v7, v6

    .line 93
    .end local v6    # "c":Landroid/database/Cursor;
    .restart local v7    # "c":Landroid/database/Cursor;
    goto :goto_0

    .line 94
    .end local v7    # "c":Landroid/database/Cursor;
    .end local v8    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v6    # "c":Landroid/database/Cursor;
    :catch_1
    move-exception v9

    .local v9, "ex":Ljava/lang/Exception;
    move-object v7, v6

    .line 95
    .end local v6    # "c":Landroid/database/Cursor;
    .restart local v7    # "c":Landroid/database/Cursor;
    goto :goto_0
.end method

.method public static getVideoFrame(Landroid/media/MediaMetadataRetriever;F)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "retriever"    # Landroid/media/MediaMetadataRetriever;
    .param p1, "time"    # F

    .prologue
    .line 155
    .line 156
    const/16 v4, 0x9

    .line 155
    :try_start_0
    invoke-virtual {p0, v4}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 158
    .local v0, "duration":I
    int-to-float v4, v0

    cmpl-float v4, p1, v4

    if-lez v4, :cond_0

    add-int/lit8 v4, v0, -0x64

    int-to-float p1, v4

    .line 159
    :cond_0
    const/high16 v4, 0x447a0000    # 1000.0f

    mul-float/2addr v4, p1

    float-to-long v2, v4

    .line 161
    .local v2, "microSecs":J
    invoke-virtual {p0, v2, v3}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 165
    .end local v0    # "duration":I
    .end local v2    # "microSecs":J
    :goto_0
    return-object v4

    .line 162
    :catch_0
    move-exception v1

    .line 163
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 165
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static getVideoResolutionFromURI(Ljava/lang/String;)Lcom/sec/android/app/video/ResolutionData;
    .locals 8
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 50
    new-instance v3, Lcom/sec/android/app/video/ResolutionData;

    invoke-direct {v3}, Lcom/sec/android/app/video/ResolutionData;-><init>()V

    .line 51
    .local v3, "res":Lcom/sec/android/app/video/ResolutionData;
    if-eqz p0, :cond_0

    .line 53
    new-instance v4, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v4}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 54
    .local v4, "retriever":Landroid/media/MediaMetadataRetriever;
    const/4 v0, 0x0

    .line 57
    .local v0, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    .end local v0    # "fis":Ljava/io/FileInputStream;
    .local v1, "fis":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;)V

    .line 59
    const/16 v6, 0x12

    invoke-virtual {v4, v6}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v5

    .line 60
    .local v5, "width":Ljava/lang/String;
    const/16 v6, 0x13

    invoke-virtual {v4, v6}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v2

    .line 61
    .local v2, "height":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v3, Lcom/sec/android/app/video/ResolutionData;->width:I

    .line 62
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v3, Lcom/sec/android/app/video/ResolutionData;->height:I

    .line 63
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/sec/android/app/video/ResolutionData;->resolution:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 70
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 71
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 72
    const/4 v4, 0x0

    .line 78
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v2    # "height":Ljava/lang/String;
    .end local v4    # "retriever":Landroid/media/MediaMetadataRetriever;
    .end local v5    # "width":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v3

    .line 65
    .restart local v0    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "retriever":Landroid/media/MediaMetadataRetriever;
    :catch_0
    move-exception v6

    .line 70
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    .line 71
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 72
    const/4 v4, 0x0

    goto :goto_0

    .line 68
    :catchall_0
    move-exception v6

    .line 70
    :goto_2
    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    .line 71
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 72
    const/4 v4, 0x0

    .line 76
    :goto_3
    throw v6

    .line 73
    .end local v0    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "height":Ljava/lang/String;
    .restart local v5    # "width":Ljava/lang/String;
    :catch_1
    move-exception v6

    goto :goto_0

    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v2    # "height":Ljava/lang/String;
    .end local v5    # "width":Ljava/lang/String;
    .restart local v0    # "fis":Ljava/io/FileInputStream;
    :catch_2
    move-exception v7

    goto :goto_3

    .line 68
    .end local v0    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v0    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .line 73
    :catch_3
    move-exception v6

    goto :goto_0

    .line 65
    .end local v0    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    :catch_4
    move-exception v6

    move-object v0, v1

    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v0    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public static is3DVideo(Landroid/media/MediaMetadataRetriever;)Z
    .locals 3
    .param p0, "retriever"    # Landroid/media/MediaMetadataRetriever;

    .prologue
    const/4 v1, 0x0

    .line 503
    const/16 v2, 0x2a

    invoke-virtual {p0, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    .line 504
    .local v0, "value":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 509
    :cond_0
    :goto_0
    return v1

    .line 506
    :cond_1
    const-string v2, "SBS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 507
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static isSpecialCharactersPresent(Ljava/lang/String;)Z
    .locals 3
    .param p0, "source"    # Ljava/lang/String;

    .prologue
    .line 455
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/video/Utils;->INVALID_CHAR:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 460
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 456
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/video/Utils;->INVALID_CHAR:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_1

    .line 457
    const/4 v1, 0x1

    goto :goto_1

    .line 455
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static isSupportedResolution(Lcom/sec/android/app/video/ResolutionData;)Z
    .locals 1
    .param p0, "mResolution"    # Lcom/sec/android/app/video/ResolutionData;

    .prologue
    .line 491
    const/4 v0, 0x1

    return v0
.end method

.method public static releaseRetriever(Landroid/media/MediaMetadataRetriever;)V
    .locals 1
    .param p0, "retriever"    # Landroid/media/MediaMetadataRetriever;

    .prologue
    .line 203
    if-nez p0, :cond_0

    .line 211
    :goto_0
    return-void

    .line 207
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 208
    :catch_0
    move-exception v0

    .line 209
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static showToast(Landroid/content/Context;IIII)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "aId"    # I
    .param p2, "gravity"    # I
    .param p3, "xOff"    # I
    .param p4, "yOff"    # I

    .prologue
    .line 214
    if-nez p0, :cond_0

    .line 222
    :goto_0
    return-void

    .line 217
    :cond_0
    sget-object v0, Lcom/sec/android/app/video/Utils;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_1

    if-eqz p0, :cond_1

    .line 218
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/video/Utils;->mToast:Landroid/widget/Toast;

    .line 220
    :cond_1
    sget-object v0, Lcom/sec/android/app/video/Utils;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    .line 221
    sget-object v0, Lcom/sec/android/app/video/Utils;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private static showToastMaxChars()V
    .locals 5

    .prologue
    const/16 v4, 0x7d1

    .line 226
    const/16 v0, 0x7d1

    .line 227
    .local v0, "MAX_TOAST_MESSAGE":I
    sget-object v1, Lcom/sec/android/app/video/Utils;->mMaxCharToastHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 228
    sget-object v1, Lcom/sec/android/app/video/Utils;->mMaxCharToastHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 229
    return-void
.end method
