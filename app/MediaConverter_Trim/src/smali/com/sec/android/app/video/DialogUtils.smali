.class public Lcom/sec/android/app/video/DialogUtils;
.super Ljava/lang/Object;
.source "DialogUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/video/DialogUtils$DialogListener;
    }
.end annotation


# static fields
.field public static final DIALOG_PLAY_CONFIRM:I = 0x322

.field public static final DIALOG_PROJECT_SAVE:I = 0x320

.field public static final DIALOG_RESIZE_PROGRESS:I = 0x324

.field public static final DIALOG_RESIZE_START:I = 0x325

.field public static final MAX_PROGRESS:I = 0x64


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createResizeProgressDialog(Landroid/content/Context;Lcom/sec/android/app/video/DialogUtils$DialogListener;)Landroid/app/AlertDialog;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "lstnr"    # Lcom/sec/android/app/video/DialogUtils$DialogListener;

    .prologue
    const/high16 v10, -0x1000000

    .line 127
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 128
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    sget-object v7, Lcom/sec/android/app/video/VPTrimApp;->gInflator:Landroid/view/LayoutInflater;

    const v8, 0x7f030002

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 129
    .local v6, "resize_progress":Landroid/view/View;
    sget-object v7, Lcom/sec/android/app/video/VPTrimApp;->gContext:Landroid/content/Context;

    const/high16 v8, 0x7f060000

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 130
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 132
    const v7, 0x7f0a0009

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 133
    .local v2, "pauseText":Landroid/widget/TextView;
    const v7, 0x7f0a000b

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 134
    .local v4, "progText":Landroid/widget/TextView;
    sget-object v7, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v8, "5"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 135
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 136
    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 138
    :cond_0
    const v7, 0x7f0a000a

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    .line 139
    .local v3, "prog":Landroid/widget/ProgressBar;
    const/16 v7, 0x64

    invoke-virtual {v3, v7}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 140
    const/4 v1, 0x0

    .line 141
    .local v1, "mProgress":I
    invoke-virtual {v3, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 143
    invoke-virtual {p1, v3, v4}, Lcom/sec/android/app/video/DialogUtils$DialogListener;->onResizeProgressCreated(Landroid/widget/ProgressBar;Landroid/widget/TextView;)V

    .line 146
    sget-object v7, Lcom/sec/android/app/video/VPTrimApp;->gContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f060009

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 147
    new-instance v8, Lcom/sec/android/app/video/DialogUtils$7;

    invoke-direct {v8, p1}, Lcom/sec/android/app/video/DialogUtils$7;-><init>(Lcom/sec/android/app/video/DialogUtils$DialogListener;)V

    .line 145
    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 153
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    .line 155
    .local v5, "progress":Landroid/app/AlertDialog;
    new-instance v7, Lcom/sec/android/app/video/DialogUtils$8;

    invoke-direct {v7, p1}, Lcom/sec/android/app/video/DialogUtils$8;-><init>(Lcom/sec/android/app/video/DialogUtils$DialogListener;)V

    invoke-virtual {v5, v7}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 165
    new-instance v7, Lcom/sec/android/app/video/DialogUtils$9;

    invoke-direct {v7, v2}, Lcom/sec/android/app/video/DialogUtils$9;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v5, v7}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 172
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 173
    return-object v5
.end method

.method public static createTrimDialog(Landroid/app/Activity;Ljava/lang/String;)Landroid/app/AlertDialog;
    .locals 9
    .param p0, "acvt"    # Landroid/app/Activity;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/high16 v8, -0x1000000

    .line 34
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f030001

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 35
    .local v4, "view":Landroid/view/View;
    const v5, 0x7f0a0008

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 36
    .local v0, "etext":Landroid/widget/EditText;
    const v5, 0x7f0a0007

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 37
    .local v3, "tv":Landroid/widget/TextView;
    sget-object v5, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v6, "5"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 38
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 39
    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setTextColor(I)V

    .line 41
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/video/Utils;->getNextAvailableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, "exportFilePath":Ljava/lang/String;
    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setMaxEms(I)V

    .line 43
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {p0, v5}, Lcom/sec/android/app/video/Utils;->getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 44
    invoke-virtual {v0}, Landroid/widget/EditText;->bringToFront()V

    .line 45
    new-instance v5, Lcom/sec/android/app/video/DialogUtils$1;

    invoke-direct {v5, p0, v0}, Lcom/sec/android/app/video/DialogUtils$1;-><init>(Landroid/app/Activity;Landroid/widget/EditText;)V

    .line 50
    const-wide/16 v6, 0x12c

    .line 45
    invoke-virtual {v0, v5, v6, v7}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 52
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-direct {v5, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 53
    const v6, 0x7f06000a

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 54
    invoke-virtual {v5, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 55
    const v6, 0x7f060008

    new-instance v7, Lcom/sec/android/app/video/DialogUtils$2;

    invoke-direct {v7, v0}, Lcom/sec/android/app/video/DialogUtils$2;-><init>(Landroid/widget/EditText;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 65
    const v6, 0x7f060009

    new-instance v7, Lcom/sec/android/app/video/DialogUtils$3;

    invoke-direct {v7, p0}, Lcom/sec/android/app/video/DialogUtils$3;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 70
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 71
    .local v2, "saveNewDialog":Landroid/app/AlertDialog;
    new-instance v5, Lcom/sec/android/app/video/DialogUtils$4;

    invoke-direct {v5, v2, v3}, Lcom/sec/android/app/video/DialogUtils$4;-><init>(Landroid/app/AlertDialog;Landroid/widget/TextView;)V

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 100
    new-instance v5, Lcom/sec/android/app/video/DialogUtils$5;

    invoke-direct {v5, v1, v2, v0}, Lcom/sec/android/app/video/DialogUtils$5;-><init>(Ljava/lang/String;Landroid/app/AlertDialog;Landroid/widget/EditText;)V

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 116
    new-instance v5, Lcom/sec/android/app/video/DialogUtils$6;

    invoke-direct {v5, p0}, Lcom/sec/android/app/video/DialogUtils$6;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 122
    return-object v2
.end method
