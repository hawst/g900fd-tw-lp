.class public Lcom/sec/android/app/video/ResolutionData;
.super Ljava/lang/Object;
.source "ResolutionData.java"


# instance fields
.field public audioCodecType:I

.field public height:I

.field public resolution:Ljava/lang/String;

.field public resolutionType:I

.field public videoCodecType:I

.field public width:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput v1, p0, Lcom/sec/android/app/video/ResolutionData;->height:I

    .line 5
    iput v1, p0, Lcom/sec/android/app/video/ResolutionData;->width:I

    .line 6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/video/ResolutionData;->resolution:Ljava/lang/String;

    .line 7
    iput v1, p0, Lcom/sec/android/app/video/ResolutionData;->resolutionType:I

    .line 8
    iput v1, p0, Lcom/sec/android/app/video/ResolutionData;->videoCodecType:I

    .line 9
    iput v1, p0, Lcom/sec/android/app/video/ResolutionData;->audioCodecType:I

    .line 3
    return-void
.end method
