.class Lcom/sec/android/app/video/Utils$3;
.super Ljava/lang/Object;
.source "Utils.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/video/Utils;->getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mToastInvalidChar:Landroid/widget/Toast;

.field private final synthetic val$ctx:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/video/Utils$3;->val$ctx:Landroid/content/Context;

    .line 276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 277
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/video/Utils$3;->mToastInvalidChar:Landroid/widget/Toast;

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 7
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const/4 v6, 0x0

    const v5, 0x7f060005

    .line 280
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    # getter for: Lcom/sec/android/app/video/Utils;->INVALID_CHAR:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/video/Utils;->access$1()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-lt v1, v3, :cond_1

    .line 291
    if-eqz p3, :cond_0

    .line 293
    const/4 v1, 0x0

    :goto_1
    if-lt v1, p3, :cond_4

    .line 314
    :cond_0
    :goto_2
    const/4 v3, 0x0

    :goto_3
    return-object v3

    .line 281
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    # getter for: Lcom/sec/android/app/video/Utils;->INVALID_CHAR:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/video/Utils;->access$1()[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_3

    .line 282
    iget-object v3, p0, Lcom/sec/android/app/video/Utils$3;->mToastInvalidChar:Landroid/widget/Toast;

    if-nez v3, :cond_2

    .line 283
    iget-object v3, p0, Lcom/sec/android/app/video/Utils$3;->val$ctx:Landroid/content/Context;

    invoke-static {v3, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/video/Utils$3;->mToastInvalidChar:Landroid/widget/Toast;

    .line 287
    :goto_4
    iget-object v3, p0, Lcom/sec/android/app/video/Utils$3;->mToastInvalidChar:Landroid/widget/Toast;

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 288
    const-string v3, ""

    goto :goto_3

    .line 285
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/video/Utils$3;->mToastInvalidChar:Landroid/widget/Toast;

    invoke-virtual {v3, v5}, Landroid/widget/Toast;->setText(I)V

    goto :goto_4

    .line 280
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 294
    :cond_4
    :try_start_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    .line 295
    .local v2, "value":I
    const v3, 0xfe000

    if-lt v2, v3, :cond_5

    .line 296
    const v3, 0xff000

    if-lt v2, v3, :cond_6

    .line 297
    :cond_5
    const v3, 0xe63e

    if-lt v2, v3, :cond_8

    .line 298
    const v3, 0xe757

    if-gt v2, v3, :cond_8

    .line 299
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/video/Utils$3;->mToastInvalidChar:Landroid/widget/Toast;

    if-nez v3, :cond_7

    .line 300
    iget-object v3, p0, Lcom/sec/android/app/video/Utils$3;->val$ctx:Landroid/content/Context;

    .line 301
    const v4, 0x7f060005

    .line 302
    const/4 v5, 0x0

    .line 300
    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/video/Utils$3;->mToastInvalidChar:Landroid/widget/Toast;

    .line 306
    :goto_5
    iget-object v3, p0, Lcom/sec/android/app/video/Utils$3;->mToastInvalidChar:Landroid/widget/Toast;

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 307
    const-string v3, ""

    goto :goto_3

    .line 304
    :cond_7
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/video/Utils$3;->mToastInvalidChar:Landroid/widget/Toast;

    const v4, 0x7f060005

    invoke-virtual {v3, v4}, Landroid/widget/Toast;->setText(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_5

    .line 310
    .end local v2    # "value":I
    :catch_0
    move-exception v0

    .line 311
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 293
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "value":I
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
