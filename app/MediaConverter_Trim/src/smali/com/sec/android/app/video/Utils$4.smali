.class Lcom/sec/android/app/video/Utils$4;
.super Ljava/lang/Object;
.source "Utils.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/video/Utils;->getEmoticonFilterWithToast(Landroid/content/Context;)Landroid/text/InputFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 7
    .param p1, "chars"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # Landroid/text/Spanned;
    .param p5, "arg4"    # I
    .param p6, "arg5"    # I

    .prologue
    .line 323
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 324
    .local v0, "count":I
    const/4 v2, 0x0

    .line 326
    .local v2, "hasEmoji":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_0

    .line 447
    .end local p1    # "chars":Ljava/lang/CharSequence;
    :goto_1
    return-object p1

    .line 327
    .restart local p1    # "chars":Ljava/lang/CharSequence;
    :cond_0
    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 328
    .local v1, "cu":I
    const v4, 0xd800

    if-lt v1, v4, :cond_1

    const v4, 0xdbff

    if-gt v1, v4, :cond_1

    .line 329
    const/4 v2, 0x1

    .line 442
    :goto_2
    if-eqz v2, :cond_2

    .line 443
    invoke-static {}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getInstance()Lcom/samsung/app/video/trimactivity/TrimActivity;

    move-result-object v4

    const v5, 0x7f060005

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 444
    const-string p1, ""

    goto :goto_1

    .line 331
    :cond_1
    sparse-switch v1, :sswitch_data_0

    goto :goto_2

    .line 436
    :sswitch_0
    const/4 v2, 0x1

    .line 437
    goto :goto_2

    .line 326
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 331
    nop

    :sswitch_data_0
    .sparse-switch
        0x2049 -> :sswitch_0
        0x2139 -> :sswitch_0
        0x2194 -> :sswitch_0
        0x2195 -> :sswitch_0
        0x2196 -> :sswitch_0
        0x2197 -> :sswitch_0
        0x2198 -> :sswitch_0
        0x2199 -> :sswitch_0
        0x219a -> :sswitch_0
        0x21aa -> :sswitch_0
        0x231b -> :sswitch_0
        0x23e9 -> :sswitch_0
        0x23ea -> :sswitch_0
        0x23eb -> :sswitch_0
        0x23ec -> :sswitch_0
        0x23f0 -> :sswitch_0
        0x23f3 -> :sswitch_0
        0x24c2 -> :sswitch_0
        0x25ab -> :sswitch_0
        0x25b6 -> :sswitch_0
        0x25c0 -> :sswitch_0
        0x25fb -> :sswitch_0
        0x25fc -> :sswitch_0
        0x25fd -> :sswitch_0
        0x25fe -> :sswitch_0
        0x2600 -> :sswitch_0
        0x2601 -> :sswitch_0
        0x2611 -> :sswitch_0
        0x2614 -> :sswitch_0
        0x2615 -> :sswitch_0
        0x261d -> :sswitch_0
        0x263a -> :sswitch_0
        0x2648 -> :sswitch_0
        0x2649 -> :sswitch_0
        0x264a -> :sswitch_0
        0x264b -> :sswitch_0
        0x264c -> :sswitch_0
        0x264d -> :sswitch_0
        0x264e -> :sswitch_0
        0x264f -> :sswitch_0
        0x2650 -> :sswitch_0
        0x2651 -> :sswitch_0
        0x2652 -> :sswitch_0
        0x2653 -> :sswitch_0
        0x2660 -> :sswitch_0
        0x2663 -> :sswitch_0
        0x2665 -> :sswitch_0
        0x2666 -> :sswitch_0
        0x2668 -> :sswitch_0
        0x267b -> :sswitch_0
        0x267f -> :sswitch_0
        0x26a0 -> :sswitch_0
        0x26a1 -> :sswitch_0
        0x26aa -> :sswitch_0
        0x26ab -> :sswitch_0
        0x26bd -> :sswitch_0
        0x26be -> :sswitch_0
        0x26c4 -> :sswitch_0
        0x26c5 -> :sswitch_0
        0x26ce -> :sswitch_0
        0x26d4 -> :sswitch_0
        0x26f3 -> :sswitch_0
        0x26f5 -> :sswitch_0
        0x26fa -> :sswitch_0
        0x26fd -> :sswitch_0
        0x2702 -> :sswitch_0
        0x2705 -> :sswitch_0
        0x2708 -> :sswitch_0
        0x270a -> :sswitch_0
        0x270b -> :sswitch_0
        0x270c -> :sswitch_0
        0x2712 -> :sswitch_0
        0x2714 -> :sswitch_0
        0x2716 -> :sswitch_0
        0x2728 -> :sswitch_0
        0x2733 -> :sswitch_0
        0x2734 -> :sswitch_0
        0x2744 -> :sswitch_0
        0x2747 -> :sswitch_0
        0x274c -> :sswitch_0
        0x274e -> :sswitch_0
        0x2753 -> :sswitch_0
        0x2754 -> :sswitch_0
        0x2755 -> :sswitch_0
        0x2757 -> :sswitch_0
        0x2764 -> :sswitch_0
        0x2795 -> :sswitch_0
        0x2796 -> :sswitch_0
        0x2797 -> :sswitch_0
        0x27b0 -> :sswitch_0
        0x27bf -> :sswitch_0
        0x2934 -> :sswitch_0
        0x2935 -> :sswitch_0
        0x2b05 -> :sswitch_0
        0x2b06 -> :sswitch_0
        0x2b07 -> :sswitch_0
        0x2b1b -> :sswitch_0
        0x2b1c -> :sswitch_0
        0x2b50 -> :sswitch_0
        0x2b55 -> :sswitch_0
        0x3030 -> :sswitch_0
        0x303d -> :sswitch_0
        0x3297 -> :sswitch_0
        0x3299 -> :sswitch_0
    .end sparse-switch
.end method
