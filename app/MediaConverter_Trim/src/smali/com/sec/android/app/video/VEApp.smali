.class public Lcom/sec/android/app/video/VEApp;
.super Landroid/app/AppGlobals;
.source "VEApp.java"


# static fields
.field public static gContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    invoke-static {}, Lcom/sec/android/app/video/VEApp;->getInitialApplication()Landroid/app/Application;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/video/VEApp;->gContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Landroid/app/AppGlobals;-><init>()V

    return-void
.end method

.method public static getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/video/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method public static getStringValue(I)Ljava/lang/String;
    .locals 1
    .param p0, "fileSaved"    # I

    .prologue
    .line 16
    sget-object v0, Lcom/sec/android/app/video/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
