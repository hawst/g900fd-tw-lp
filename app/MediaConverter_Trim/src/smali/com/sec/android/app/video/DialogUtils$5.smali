.class Lcom/sec/android/app/video/DialogUtils$5;
.super Ljava/lang/Object;
.source "DialogUtils.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/video/DialogUtils;->createTrimDialog(Landroid/app/Activity;Ljava/lang/String;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$etext:Landroid/widget/EditText;

.field private final synthetic val$exportFilePath:Ljava/lang/String;

.field private final synthetic val$saveNewDialog:Landroid/app/AlertDialog;


# direct methods
.method constructor <init>(Ljava/lang/String;Landroid/app/AlertDialog;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/video/DialogUtils$5;->val$exportFilePath:Ljava/lang/String;

    iput-object p2, p0, Lcom/sec/android/app/video/DialogUtils$5;->val$saveNewDialog:Landroid/app/AlertDialog;

    iput-object p3, p0, Lcom/sec/android/app/video/DialogUtils$5;->val$etext:Landroid/widget/EditText;

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v2, -0x1

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/video/DialogUtils$5;->val$exportFilePath:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/video/DialogUtils$5;->val$saveNewDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 108
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/video/DialogUtils$5;->val$etext:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/video/DialogUtils$5;->val$exportFilePath:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 113
    :cond_0
    :goto_1
    return-void

    .line 106
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/video/DialogUtils$5;->val$saveNewDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 110
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/video/DialogUtils$5;->val$etext:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/video/DialogUtils$5;->val$exportFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/video/DialogUtils$5;->val$etext:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/video/DialogUtils$5;->val$etext:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    goto :goto_1
.end method
