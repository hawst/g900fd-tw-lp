.class public abstract Lcom/samsung/app/video/resize/ResizeInterface;
.super Ljava/lang/Object;
.source "ResizeInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/app/video/resize/ResizeInterface$Adapter;
    }
.end annotation


# static fields
.field public static final MSG_RESIZE_COMPLETED:I = 0x1

.field public static final MSG_RESIZE_FAILED:I = 0x0

.field public static final MSG_RESIZE_STARTED:I = 0x2

.field public static final MSG_RESIZE_UPDATE:I = 0x3

.field public static final STATE_RESIZE_STARTED:I = 0x5

.field public static final STATE_RESIZE_STOPPED:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getDialogListener()Lcom/sec/android/app/video/DialogUtils$DialogListener;
.end method

.method public abstract getFileName()Ljava/lang/String;
.end method

.method public abstract getHandler()Landroid/os/Handler;
.end method

.method public abstract getResizeObject()Lcom/samsung/app/share/via/external/ShareviaObj;
.end method

.method public abstract isResizeRunning()Z
.end method

.method public abstract startResize(Landroid/app/Activity;Lcom/samsung/app/share/via/external/ShareviaObj;Lcom/samsung/app/video/resize/ResizeInterface$Adapter;)Z
.end method

.method public abstract stopResize()V
.end method
