.class Lcom/samsung/app/video/trimactivity/TrimActivity$5;
.super Ljava/lang/Object;
.source "TrimActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/trimactivity/TrimActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;


# direct methods
.method constructor <init>(Lcom/samsung/app/video/trimactivity/TrimActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 166
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    const/4 v2, -0x1

    invoke-static {v1, v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$3(Lcom/samsung/app/video/trimactivity/TrimActivity;I)V

    .line 167
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;
    invoke-static {v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$0(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;
    invoke-static {v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$0(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 168
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # invokes: Lcom/samsung/app/video/trimactivity/TrimActivity;->onPreviewPause(Z)V
    invoke-static {v1, v7}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$4(Lcom/samsung/app/video/trimactivity/TrimActivity;Z)V

    .line 169
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v0, v1

    .line 170
    .local v0, "dragXPos":I
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarWidth:I
    invoke-static {v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$5(Lcom/samsung/app/video/trimactivity/TrimActivity;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    # invokes: Lcom/samsung/app/video/trimactivity/TrimActivity;->getTimeFromPos(I)I
    invoke-static {v2, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$6(Lcom/samsung/app/video/trimactivity/TrimActivity;I)I

    move-result v2

    invoke-static {v1, v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$7(Lcom/samsung/app/video/trimactivity/TrimActivity;I)V

    .line 171
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPosition:I
    invoke-static {v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$1(Lcom/samsung/app/video/trimactivity/TrimActivity;)I

    move-result v1

    if-gez v1, :cond_1

    .line 172
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    invoke-static {v1, v7}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$7(Lcom/samsung/app/video/trimactivity/TrimActivity;I)V

    .line 173
    :cond_1
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPosition:I
    invoke-static {v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$1(Lcom/samsung/app/video/trimactivity/TrimActivity;)I

    move-result v1

    int-to-long v1, v1

    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J
    invoke-static {v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$8(Lcom/samsung/app/video/trimactivity/TrimActivity;)J

    move-result-wide v3

    iget-object v5, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarDuration:J
    invoke-static {v5}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$9(Lcom/samsung/app/video/trimactivity/TrimActivity;)J

    move-result-wide v5

    sub-long/2addr v3, v5

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    .line 174
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$8(Lcom/samsung/app/video/trimactivity/TrimActivity;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarDuration:J
    invoke-static {v4}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$9(Lcom/samsung/app/video/trimactivity/TrimActivity;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v2, v2

    invoke-static {v1, v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$7(Lcom/samsung/app/video/trimactivity/TrimActivity;I)V

    .line 176
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 194
    :goto_0
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;
    invoke-static {v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$0(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPosition:I
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$1(Lcom/samsung/app/video/trimactivity/TrimActivity;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->update(J)V

    .line 195
    return v8

    .line 178
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mLeftText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$10(Lcom/samsung/app/video/trimactivity/TrimActivity;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPosition:I
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$1(Lcom/samsung/app/video/trimactivity/TrimActivity;)I

    move-result v2

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getTimeInText(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mRightText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$11(Lcom/samsung/app/video/trimactivity/TrimActivity;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPosition:I
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$1(Lcom/samsung/app/video/trimactivity/TrimActivity;)I

    move-result v2

    int-to-long v2, v2

    iget-object v4, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarDuration:J
    invoke-static {v4}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$9(Lcom/samsung/app/video/trimactivity/TrimActivity;)J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getTimeInText(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    invoke-virtual {p1, v8}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 184
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # invokes: Lcom/samsung/app/video/trimactivity/TrimActivity;->dragSelectWindow(I)V
    invoke-static {v1, v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$12(Lcom/samsung/app/video/trimactivity/TrimActivity;I)V

    .line 185
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mLeftText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$10(Lcom/samsung/app/video/trimactivity/TrimActivity;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPosition:I
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$1(Lcom/samsung/app/video/trimactivity/TrimActivity;)I

    move-result v2

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getTimeInText(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mRightText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$11(Lcom/samsung/app/video/trimactivity/TrimActivity;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPosition:I
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$1(Lcom/samsung/app/video/trimactivity/TrimActivity;)I

    move-result v2

    int-to-long v2, v2

    iget-object v4, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarDuration:J
    invoke-static {v4}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$9(Lcom/samsung/app/video/trimactivity/TrimActivity;)J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getTimeInText(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 190
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # invokes: Lcom/samsung/app/video/trimactivity/TrimActivity;->dragSelectWindow(I)V
    invoke-static {v1, v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$12(Lcom/samsung/app/video/trimactivity/TrimActivity;I)V

    .line 191
    invoke-virtual {p1, v7}, Landroid/view/View;->setPressed(Z)V

    goto/16 :goto_0

    .line 176
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
