.class public abstract Lcom/samsung/app/video/resize/ResizeInterface$Adapter;
.super Ljava/lang/Object;
.source "ResizeInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/resize/ResizeInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Adapter"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getProgressDialog()Landroid/app/AlertDialog;
.end method

.method public onResizeCompleted(Landroid/net/Uri;)V
    .locals 0
    .param p1, "fileUri"    # Landroid/net/Uri;

    .prologue
    .line 33
    return-void
.end method

.method public onResizeFailed()V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public onResizeStarted()V
    .locals 0

    .prologue
    .line 31
    return-void
.end method
