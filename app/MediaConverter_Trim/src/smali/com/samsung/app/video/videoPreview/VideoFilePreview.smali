.class public Lcom/samsung/app/video/videoPreview/VideoFilePreview;
.super Landroid/widget/RelativeLayout;
.source "VideoFilePreview.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;,
        Lcom/samsung/app/video/videoPreview/VideoFilePreview$UnplugHeadsetReceiver;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;

.field private mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private mVideoCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mVideoErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

.field private receiver:Lcom/samsung/app/video/videoPreview/VideoFilePreview$UnplugHeadsetReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 39
    new-instance v0, Lcom/samsung/app/video/videoPreview/VideoFilePreview$1;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview$1;-><init>(Lcom/samsung/app/video/videoPreview/VideoFilePreview;)V

    iput-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 47
    new-instance v0, Lcom/samsung/app/video/videoPreview/VideoFilePreview$2;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview$2;-><init>(Lcom/samsung/app/video/videoPreview/VideoFilePreview;)V

    iput-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 102
    new-instance v0, Lcom/samsung/app/video/videoPreview/VideoFilePreview$3;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview$3;-><init>(Lcom/samsung/app/video/videoPreview/VideoFilePreview;)V

    iput-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 83
    iput-object p1, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mContext:Landroid/content/Context;

    .line 84
    invoke-direct {p0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->init()V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    new-instance v0, Lcom/samsung/app/video/videoPreview/VideoFilePreview$1;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview$1;-><init>(Lcom/samsung/app/video/videoPreview/VideoFilePreview;)V

    iput-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 47
    new-instance v0, Lcom/samsung/app/video/videoPreview/VideoFilePreview$2;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview$2;-><init>(Lcom/samsung/app/video/videoPreview/VideoFilePreview;)V

    iput-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 102
    new-instance v0, Lcom/samsung/app/video/videoPreview/VideoFilePreview$3;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview$3;-><init>(Lcom/samsung/app/video/videoPreview/VideoFilePreview;)V

    iput-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 77
    iput-object p1, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mContext:Landroid/content/Context;

    .line 78
    invoke-direct {p0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->init()V

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 88
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    new-instance v0, Lcom/samsung/app/video/videoPreview/VideoFilePreview$1;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview$1;-><init>(Lcom/samsung/app/video/videoPreview/VideoFilePreview;)V

    iput-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 47
    new-instance v0, Lcom/samsung/app/video/videoPreview/VideoFilePreview$2;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview$2;-><init>(Lcom/samsung/app/video/videoPreview/VideoFilePreview;)V

    iput-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 102
    new-instance v0, Lcom/samsung/app/video/videoPreview/VideoFilePreview$3;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview$3;-><init>(Lcom/samsung/app/video/videoPreview/VideoFilePreview;)V

    iput-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 89
    iput-object p1, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mContext:Landroid/content/Context;

    .line 90
    invoke-direct {p0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->init()V

    .line 91
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/app/video/videoPreview/VideoFilePreview;)Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mAdapter:Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;

    return-object v0
.end method

.method private init()V
    .locals 3

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 95
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030003

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 96
    const v1, 0x7f0a000c

    invoke-virtual {p0, v1}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    iput-object v1, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    .line 97
    iget-object v1, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    iget-object v2, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v1, v2}, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 98
    iget-object v1, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    iget-object v2, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v1, v2}, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 99
    iget-object v1, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mAudioManager:Landroid/media/AudioManager;

    .line 100
    return-void
.end method


# virtual methods
.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    invoke-virtual {v0}, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;->getCurrentPosition()I

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    invoke-virtual {v0}, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;->isPlaying()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->stop()V

    .line 186
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 188
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    invoke-virtual {v0}, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;->pause()V

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mAdapter:Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;

    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mAdapter:Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;

    invoke-virtual {v0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;->onStopped()V

    .line 149
    :cond_1
    return-void
.end method

.method public play(J)Z
    .locals 8
    .param p1, "pos"    # J

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 122
    iget-object v5, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mContext:Landroid/content/Context;

    const-string v6, "phone"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 123
    .local v1, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v5

    if-eqz v5, :cond_1

    .line 124
    iget-object v4, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mContext:Landroid/content/Context;

    const v5, 0x7f06000e

    invoke-static {v4, v5, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    .line 125
    .local v2, "toast":Landroid/widget/Toast;
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 140
    .end local v2    # "toast":Landroid/widget/Toast;
    :cond_0
    :goto_0
    return v3

    .line 129
    :cond_1
    iget-object v5, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mAudioManager:Landroid/media/AudioManager;

    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v7, 0x3

    invoke-virtual {v5, v6, v7, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 130
    .local v0, "lAudioFocus":I
    if-ne v0, v4, :cond_0

    .line 132
    iget-object v3, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    if-eqz v3, :cond_2

    .line 133
    iget-object v3, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    long-to-int v5, p1

    invoke-virtual {v3, v5}, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;->seekTo(I)V

    .line 134
    iget-object v3, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    invoke-virtual {v3}, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;->start()V

    .line 136
    :cond_2
    iget-object v3, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mAdapter:Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;

    if-eqz v3, :cond_3

    .line 137
    iget-object v3, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mAdapter:Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;

    invoke-virtual {v3}, Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;->onPlayed()V

    :cond_3
    move v3, v4

    .line 138
    goto :goto_0
.end method

.method public registerForHeadSetUnplugged()V
    .locals 3

    .prologue
    .line 61
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.media.AUDIO_BECOMING_NOISY"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 62
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    new-instance v1, Lcom/samsung/app/video/videoPreview/VideoFilePreview$UnplugHeadsetReceiver;

    invoke-direct {v1, p0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview$UnplugHeadsetReceiver;-><init>(Lcom/samsung/app/video/videoPreview/VideoFilePreview;)V

    iput-object v1, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->receiver:Lcom/samsung/app/video/videoPreview/VideoFilePreview$UnplugHeadsetReceiver;

    .line 63
    iget-object v1, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->receiver:Lcom/samsung/app/video/videoPreview/VideoFilePreview$UnplugHeadsetReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 64
    return-void
.end method

.method public resume(J)V
    .locals 1
    .param p1, "pos"    # J

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    invoke-virtual {v0}, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;->resume()V

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mAdapter:Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;

    if-eqz v0, :cond_1

    .line 156
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mAdapter:Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;

    invoke-virtual {v0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;->onPlayed()V

    .line 157
    :cond_1
    return-void
.end method

.method public seekTo(J)V
    .locals 2
    .param p1, "pos"    # J

    .prologue
    .line 169
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;->seekTo(I)V

    .line 172
    :cond_0
    return-void
.end method

.method public setAdapter(Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;)V
    .locals 0
    .param p1, "adap"    # Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;

    .prologue
    .line 176
    iput-object p1, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mAdapter:Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;

    .line 177
    return-void
.end method

.method public setDataUri(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    invoke-virtual {v0, p1}, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 117
    :cond_0
    return-void
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 1
    .param p1, "mPreviewPreparedListener"    # Landroid/media/MediaPlayer$OnPreparedListener;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    invoke-virtual {v0, p1}, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 202
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mVideoView:Lcom/samsung/app/video/videoPreview/ScaleableVideoView;

    invoke-virtual {v0}, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;->stopPlayback()V

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mAdapter:Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;

    if-eqz v0, :cond_1

    .line 164
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mAdapter:Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;

    invoke-virtual {v0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;->onStopped()V

    .line 165
    :cond_1
    return-void
.end method

.method public unRegisterForHeadSetUnplugged()V
    .locals 3

    .prologue
    .line 68
    :try_start_0
    iget-object v1, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->receiver:Lcom/samsung/app/video/videoPreview/VideoFilePreview$UnplugHeadsetReceiver;

    if-eqz v1, :cond_0

    .line 69
    iget-object v1, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->receiver:Lcom/samsung/app/video/videoPreview/VideoFilePreview$UnplugHeadsetReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 70
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public update(J)V
    .locals 0
    .param p1, "time"    # J

    .prologue
    .line 191
    invoke-virtual {p0, p1, p2}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->seekTo(J)V

    .line 192
    return-void
.end method
