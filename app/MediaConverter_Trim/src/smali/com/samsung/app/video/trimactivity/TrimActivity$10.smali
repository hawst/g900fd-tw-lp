.class Lcom/samsung/app/video/trimactivity/TrimActivity$10;
.super Ljava/lang/Object;
.source "TrimActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/app/video/trimactivity/TrimActivity;->startSeekbarUpdate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

.field private final synthetic val$wait_time:I


# direct methods
.method constructor <init>(Lcom/samsung/app/video/trimactivity/TrimActivity;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$10;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    iput p2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$10;->val$wait_time:I

    .line 613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 616
    const/4 v1, 0x0

    .line 617
    .local v1, "delayTime":I
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$10;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mIsPaused:Z
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$21(Lcom/samsung/app/video/trimactivity/TrimActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 634
    :goto_0
    return-void

    .line 620
    :cond_0
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$10;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$0(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->getCurrentPosition()I

    move-result v0

    .line 621
    .local v0, "currPos":I
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$10;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPosition:I
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$1(Lcom/samsung/app/video/trimactivity/TrimActivity;)I

    move-result v2

    int-to-long v2, v2

    iget-object v4, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$10;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarDuration:J
    invoke-static {v4}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$9(Lcom/samsung/app/video/trimactivity/TrimActivity;)J

    move-result-wide v4

    add-long/2addr v2, v4

    int-to-long v4, v0

    sub-long/2addr v2, v4

    long-to-int v1, v2

    .line 622
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$10;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mTrimBarMgr:Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$19(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;->setSeekBarPosition(I)V

    .line 623
    iget v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$10;->val$wait_time:I

    if-ge v1, v2, :cond_1

    .line 624
    :goto_1
    if-lez v1, :cond_2

    .line 625
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$10;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    invoke-static {v2, v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$3(Lcom/samsung/app/video/trimactivity/TrimActivity;I)V

    .line 626
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$10;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$0(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    move-result-object v2

    int-to-long v3, v1

    invoke-virtual {v2, p0, v3, v4}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 633
    :goto_2
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$10;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mLeftText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$10(Lcom/samsung/app/video/trimactivity/TrimActivity;)Landroid/widget/TextView;

    move-result-object v2

    int-to-long v3, v0

    invoke-static {v3, v4}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getTimeInText(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 623
    :cond_1
    iget v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$10;->val$wait_time:I

    goto :goto_1

    .line 628
    :cond_2
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$10;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    const/4 v3, -0x1

    invoke-static {v2, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$3(Lcom/samsung/app/video/trimactivity/TrimActivity;I)V

    .line 629
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$10;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    const/4 v3, 0x0

    # invokes: Lcom/samsung/app/video/trimactivity/TrimActivity;->onPreviewPause(Z)V
    invoke-static {v2, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$4(Lcom/samsung/app/video/trimactivity/TrimActivity;Z)V

    .line 630
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$10;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mTrimBarMgr:Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$19(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$10;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    iget-object v4, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$10;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mYellowBox:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$22(Lcom/samsung/app/video/trimactivity/TrimActivity;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLeft()I

    move-result v4

    # invokes: Lcom/samsung/app/video/trimactivity/TrimActivity;->getTimeFromPos(I)I
    invoke-static {v3, v4}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$6(Lcom/samsung/app/video/trimactivity/TrimActivity;I)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;->setSeekBarPosition(I)V

    goto :goto_2
.end method
