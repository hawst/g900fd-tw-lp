.class public Lcom/samsung/app/video/trimactivity/TrimActivity;
.super Landroid/app/Activity;
.source "TrimActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/app/video/trimactivity/TrimActivity$ResAdapter;
    }
.end annotation


# static fields
.field private static final DUMMY_START_POS:I = 0x5

.field private static final MSG_FINISH_ACTIVITY:I = 0xfe

.field private static final MSG_SHOW_TOAST:I = 0xff

.field public static final NOT_SUPPORTED_BY_ENGINE:I = 0x0

.field public static OUTPUT_PATH:Ljava/lang/String; = null

.field public static final SUPPORTED_BY_ENGINE:I = 0x1

.field private static final URI:Ljava/lang/String; = "uri"

.field private static _instance:Lcom/samsung/app/video/trimactivity/TrimActivity;


# instance fields
.field private dragListener:Landroid/view/View$OnTouchListener;

.field public final handler:Landroid/os/Handler;

.field private lastPlayedPos:I

.field private mActionBar:Landroid/app/ActionBar;

.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field private mDragHolder:Landroid/widget/ImageView;

.field private mDuration:J

.field private mGlobalBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mIsPaused:Z

.field private mLeftText:Landroid/widget/TextView;

.field private mOutputSize:J

.field private mPlayBtn:Landroid/widget/ImageButton;

.field private mPosition:I

.field private mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

.field private mPreviewListener:Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;

.field private mPreviewPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mPreviewTimeline:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

.field private mProgress:Landroid/app/AlertDialog;

.field private mResolution:Lcom/sec/android/app/video/ResolutionData;

.field private mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

.field private mRightText:Landroid/widget/TextView;

.field private mScover:Lcom/samsung/android/sdk/cover/Scover;

.field private mSelectBarDuration:J

.field private mSelectBarWidth:I

.field private mTrimBarMgr:Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;

.field private mVideoPath:Ljava/lang/String;

.field private mVideoUri:Landroid/net/Uri;

.field private mYellowBox:Landroid/widget/ImageView;

.field private resizeElem:Lcom/samsung/app/share/via/external/ShareviaObj;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const-wide/16 v3, -0x1

    const/4 v2, -0x1

    const/4 v0, 0x0

    .line 62
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 68
    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mActionBar:Landroid/app/ActionBar;

    .line 70
    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mLeftText:Landroid/widget/TextView;

    .line 71
    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mRightText:Landroid/widget/TextView;

    .line 72
    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPlayBtn:Landroid/widget/ImageButton;

    .line 75
    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mTrimBarMgr:Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;

    .line 76
    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    .line 77
    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->resizeElem:Lcom/samsung/app/share/via/external/ShareviaObj;

    .line 80
    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mVideoPath:Ljava/lang/String;

    .line 82
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mIsPaused:Z

    .line 83
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    .line 84
    iput-wide v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarDuration:J

    .line 85
    iput v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPosition:I

    .line 86
    iput v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarWidth:I

    .line 87
    iput-wide v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mOutputSize:J

    .line 90
    iput v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->lastPlayedPos:I

    .line 96
    new-instance v0, Lcom/samsung/app/video/trimactivity/TrimActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/trimactivity/TrimActivity$1;-><init>(Lcom/samsung/app/video/trimactivity/TrimActivity;)V

    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 107
    new-instance v0, Lcom/samsung/app/video/trimactivity/TrimActivity$2;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/trimactivity/TrimActivity$2;-><init>(Lcom/samsung/app/video/trimactivity/TrimActivity;)V

    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreviewPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 118
    new-instance v0, Lcom/samsung/app/video/trimactivity/TrimActivity$3;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/trimactivity/TrimActivity$3;-><init>(Lcom/samsung/app/video/trimactivity/TrimActivity;)V

    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mGlobalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 145
    new-instance v0, Lcom/samsung/app/video/trimactivity/TrimActivity$4;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/trimactivity/TrimActivity$4;-><init>(Lcom/samsung/app/video/trimactivity/TrimActivity;)V

    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->handler:Landroid/os/Handler;

    .line 163
    new-instance v0, Lcom/samsung/app/video/trimactivity/TrimActivity$5;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/trimactivity/TrimActivity$5;-><init>(Lcom/samsung/app/video/trimactivity/TrimActivity;)V

    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->dragListener:Landroid/view/View$OnTouchListener;

    .line 638
    new-instance v0, Lcom/samsung/app/video/trimactivity/TrimActivity$6;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/trimactivity/TrimActivity$6;-><init>(Lcom/samsung/app/video/trimactivity/TrimActivity;)V

    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreviewListener:Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;

    .line 689
    new-instance v0, Lcom/samsung/app/video/trimactivity/TrimActivity$7;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/trimactivity/TrimActivity$7;-><init>(Lcom/samsung/app/video/trimactivity/TrimActivity;)V

    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mClickListener:Landroid/view/View$OnClickListener;

    .line 62
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/video/videoPreview/VideoFilePreview;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/app/video/trimactivity/TrimActivity;)I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPosition:I

    return v0
.end method

.method static synthetic access$10(Lcom/samsung/app/video/trimactivity/TrimActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mLeftText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/app/video/trimactivity/TrimActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mRightText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$12(Lcom/samsung/app/video/trimactivity/TrimActivity;I)V
    .locals 0

    .prologue
    .line 206
    invoke-direct {p0, p1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->dragSelectWindow(I)V

    return-void
.end method

.method static synthetic access$13(Lcom/samsung/app/video/trimactivity/TrimActivity;)V
    .locals 0

    .prologue
    .line 596
    invoke-direct {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->showUnsupportedFormatDialog()V

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/app/video/trimactivity/TrimActivity;)V
    .locals 0

    .prologue
    .line 611
    invoke-direct {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->startSeekbarUpdate()V

    return-void
.end method

.method static synthetic access$15(Lcom/samsung/app/video/trimactivity/TrimActivity;)V
    .locals 0

    .prologue
    .line 530
    invoke-direct {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->initPausedScreen()V

    return-void
.end method

.method static synthetic access$16(Lcom/samsung/app/video/trimactivity/TrimActivity;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPlayBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$17(Lcom/samsung/app/video/trimactivity/TrimActivity;)V
    .locals 0

    .prologue
    .line 680
    invoke-direct {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->prepareVideo()V

    return-void
.end method

.method static synthetic access$18(Lcom/samsung/app/video/trimactivity/TrimActivity;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mProgress:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$19(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mTrimBarMgr:Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/share/via/external/ShareviaObj;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->resizeElem:Lcom/samsung/app/share/via/external/ShareviaObj;

    return-object v0
.end method

.method static synthetic access$20(Lcom/samsung/app/video/trimactivity/TrimActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mVideoPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$21(Lcom/samsung/app/video/trimactivity/TrimActivity;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mIsPaused:Z

    return v0
.end method

.method static synthetic access$22(Lcom/samsung/app/video/trimactivity/TrimActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mYellowBox:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/app/video/trimactivity/TrimActivity;I)V
    .locals 0

    .prologue
    .line 90
    iput p1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->lastPlayedPos:I

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/app/video/trimactivity/TrimActivity;Z)V
    .locals 0

    .prologue
    .line 536
    invoke-direct {p0, p1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->onPreviewPause(Z)V

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/app/video/trimactivity/TrimActivity;)I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarWidth:I

    return v0
.end method

.method static synthetic access$6(Lcom/samsung/app/video/trimactivity/TrimActivity;I)I
    .locals 1

    .prologue
    .line 199
    invoke-direct {p0, p1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getTimeFromPos(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$7(Lcom/samsung/app/video/trimactivity/TrimActivity;I)V
    .locals 0

    .prologue
    .line 85
    iput p1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPosition:I

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/app/video/trimactivity/TrimActivity;)J
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    return-wide v0
.end method

.method static synthetic access$9(Lcom/samsung/app/video/trimactivity/TrimActivity;)J
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarDuration:J

    return-wide v0
.end method

.method private cancel()V
    .locals 4

    .prologue
    .line 473
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->setResult(I)V

    .line 475
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->handler:Landroid/os/Handler;

    const/16 v1, 0xfe

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 476
    return-void
.end method

.method private checkVideoEditIntent(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "aIntent"    # Landroid/content/Intent;

    .prologue
    .line 708
    if-nez p1, :cond_0

    .line 709
    const/4 v0, 0x0

    .line 711
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private createTranscodeElementForMMS(Ljava/lang/String;)Lcom/samsung/app/share/via/external/ShareviaObj;
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 319
    new-instance v1, Lcom/samsung/app/share/via/external/ShareviaObj;

    invoke-direct {v1}, Lcom/samsung/app/share/via/external/ShareviaObj;-><init>()V

    .line 321
    .local v1, "resizeObj":Lcom/samsung/app/share/via/external/ShareviaObj;
    invoke-static {p1}, Lcom/sec/android/app/video/Utils;->getRetrieverForSource(Ljava/lang/String;)Landroid/media/MediaMetadataRetriever;

    move-result-object v2

    .line 322
    .local v2, "retriever":Landroid/media/MediaMetadataRetriever;
    invoke-static {v2, p1}, Lcom/sec/android/app/video/Utils;->getVideoDuration(Landroid/media/MediaMetadataRetriever;Ljava/lang/String;)I

    move-result v0

    .line 323
    .local v0, "lDuration":I
    invoke-static {v2}, Lcom/sec/android/app/video/Utils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    .line 324
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v3, v3, Lcom/sec/android/app/video/ResolutionData;->width:I

    invoke-virtual {v1, v3}, Lcom/samsung/app/share/via/external/ShareviaObj;->setResizeWidth(I)V

    .line 325
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v3, v3, Lcom/sec/android/app/video/ResolutionData;->height:I

    invoke-virtual {v1, v3}, Lcom/samsung/app/share/via/external/ShareviaObj;->setResizeHeight(I)V

    .line 326
    invoke-virtual {v1, p1}, Lcom/samsung/app/share/via/external/ShareviaObj;->setResizeInputFilename(Ljava/lang/String;)V

    .line 327
    invoke-static {p1}, Lcom/sec/android/app/video/Utils;->getNextAvailableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/samsung/app/share/via/external/ShareviaObj;->setResizeOutputFilename(Ljava/lang/String;)V

    .line 328
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v3, v3, Lcom/sec/android/app/video/ResolutionData;->resolutionType:I

    invoke-virtual {v1, v3}, Lcom/samsung/app/share/via/external/ShareviaObj;->setResizeOutputFileResolution(I)V

    .line 329
    invoke-virtual {v1, v5}, Lcom/samsung/app/share/via/external/ShareviaObj;->setResizeStartTime(I)V

    .line 330
    invoke-virtual {v1, v0}, Lcom/samsung/app/share/via/external/ShareviaObj;->setResizeEndTime(I)V

    .line 331
    iget-wide v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mOutputSize:J

    long-to-int v3, v3

    invoke-virtual {v1, v3}, Lcom/samsung/app/share/via/external/ShareviaObj;->setResizemaxSize(I)V

    .line 332
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v3, v3, Lcom/sec/android/app/video/ResolutionData;->videoCodecType:I

    invoke-virtual {v1, v3}, Lcom/samsung/app/share/via/external/ShareviaObj;->setResizeVideoCodec(I)V

    .line 333
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v3, v3, Lcom/sec/android/app/video/ResolutionData;->audioCodecType:I

    invoke-virtual {v1, v3}, Lcom/samsung/app/share/via/external/ShareviaObj;->setResizeAudioCodec(I)V

    .line 334
    invoke-virtual {v1, v5}, Lcom/samsung/app/share/via/external/ShareviaObj;->setResizeTranscodeMode(I)V

    .line 335
    return-object v1
.end method

.method private dragSelectWindow(I)V
    .locals 4
    .param p1, "dragXPos"    # I

    .prologue
    .line 207
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 208
    .local v0, "r":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreviewTimeline:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    invoke-virtual {v2, v0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 209
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05001e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 210
    .local v1, "thumb_offset":I
    iget v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarWidth:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, p1, v2

    iget v3, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v1

    if-le v2, v3, :cond_0

    iget v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarWidth:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, p1

    iget v3, v0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v1

    if-ge v2, v3, :cond_0

    .line 211
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDragHolder:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDragHolder:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int v3, p1, v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 212
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mYellowBox:Landroid/widget/ImageView;

    iget v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarWidth:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, p1, v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 213
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mTrimBarMgr:Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;

    iget v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarWidth:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, p1, v3

    invoke-interface {v2, v3}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;->setSelectBarPosition(I)V

    .line 215
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/samsung/app/video/trimactivity/TrimActivity;
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lcom/samsung/app/video/trimactivity/TrimActivity;->_instance:Lcom/samsung/app/video/trimactivity/TrimActivity;

    return-object v0
.end method

.method private getTimeFromPos(I)I
    .locals 4
    .param p1, "pos"    # I

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 201
    iget-wide v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    int-to-long v2, p1

    mul-long/2addr v0, v2

    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    .line 203
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    int-to-long v2, p1

    mul-long/2addr v0, v2

    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method

.method public static getTimeInText(J)Ljava/lang/String;
    .locals 9
    .param p0, "time"    # J

    .prologue
    const/4 v8, 0x0

    const-wide/16 v6, 0x3c

    .line 479
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v0

    .line 480
    .local v0, "numForm":Ljava/text/NumberFormat;
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/text/NumberFormat;->setMinimumIntegerDigits(I)V

    .line 481
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 483
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-wide/16 v2, 0x1f4

    add-long/2addr v2, p0

    const-wide/16 v4, 0x3e8

    div-long p0, v2, v4

    .line 485
    rem-long v2, p0, v6

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 486
    div-long/2addr p0, v6

    .line 488
    new-instance v2, Ljava/lang/StringBuilder;

    rem-long v3, p0, v6

    invoke-virtual {v0, v3, v4}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v8, v2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 489
    div-long/2addr p0, v6

    .line 491
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0, p1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v8, v2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 492
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getVEEditFilePath(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p1, "mediaUri"    # Landroid/net/Uri;

    .prologue
    .line 731
    const/4 v1, 0x0

    .line 733
    .local v1, "lEditPath":Ljava/lang/String;
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 734
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 736
    .local v2, "lFileName":Ljava/lang/String;
    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 737
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 736
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    .line 737
    if-nez v3, :cond_0

    .line 739
    sget-object v3, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 740
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 739
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    .line 740
    if-eqz v3, :cond_2

    .line 741
    :cond_0
    invoke-static {p1, p0}, Lcom/sec/android/app/video/Utils;->getVideoFileInfoByUri(Landroid/net/Uri;Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v0

    .line 743
    .local v0, "lCursor":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_1

    .line 744
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 746
    const-string v3, "_data"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 745
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 761
    .end local v0    # "lCursor":Landroid/database/Cursor;
    .end local v2    # "lFileName":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v1

    .line 749
    .restart local v2    # "lFileName":Ljava/lang/String;
    :cond_2
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 750
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 749
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    .line 750
    if-nez v3, :cond_3

    .line 752
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 753
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 752
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    .line 753
    if-eqz v3, :cond_4

    .line 754
    :cond_3
    invoke-static {p1}, Lcom/sec/android/app/video/Utils;->getImageFilePathByUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 755
    goto :goto_0

    :cond_4
    const-string v3, "file://"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 756
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 757
    goto :goto_0

    .line 758
    :cond_5
    move-object v1, v2

    goto :goto_0
.end method

.method private init(II)V
    .locals 8
    .param p1, "startPos"    # I
    .param p2, "endPos"    # I

    .prologue
    const-wide/16 v6, 0x2

    .line 497
    const v1, 0x7f0a0002

    invoke-virtual {p0, v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    .line 498
    .local v0, "trimBar":Lcom/samsung/app/video/videoPreview/PreviewTimelineView;
    iget v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarWidth:I

    iget-wide v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    long-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->init(II)Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mTrimBarMgr:Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;

    .line 499
    invoke-direct {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->prepareVideo()V

    .line 500
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mLeftText:Landroid/widget/TextView;

    iget-wide v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    iget-wide v4, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarDuration:J

    sub-long/2addr v2, v4

    div-long/2addr v2, v6

    invoke-static {v2, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getTimeInText(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 501
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mRightText:Landroid/widget/TextView;

    iget-wide v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    iget-wide v4, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarDuration:J

    add-long/2addr v2, v4

    div-long/2addr v2, v6

    invoke-static {v2, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getTimeInText(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 502
    new-instance v1, Lcom/samsung/app/video/trimactivity/TrimActivity$8;

    invoke-direct {v1, p0, v0}, Lcom/samsung/app/video/trimactivity/TrimActivity$8;-><init>(Lcom/samsung/app/video/trimactivity/TrimActivity;Lcom/samsung/app/video/videoPreview/PreviewTimelineView;)V

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->post(Ljava/lang/Runnable;)Z

    .line 528
    return-void
.end method

.method private initPausedScreen()V
    .locals 3

    .prologue
    .line 531
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mLeftText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    invoke-virtual {v1}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->getCurrentPosition()I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getTimeInText(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 532
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mRightText:Landroid/widget/TextView;

    iget-wide v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    invoke-static {v1, v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getTimeInText(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 533
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    invoke-virtual {v1}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->getCurrentPosition()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->seekTo(J)V

    .line 534
    return-void
.end method

.method private onPreviewPause(Z)V
    .locals 4
    .param p1, "preview"    # Z

    .prologue
    .line 537
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    if-eqz v1, :cond_1

    .line 538
    if-eqz p1, :cond_3

    .line 539
    const/4 v0, 0x1

    .line 540
    .local v0, "isPlayStarted":Z
    iget v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->lastPlayedPos:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 541
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    iget v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPosition:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->play(J)Z

    move-result v0

    .line 544
    :goto_0
    if-eqz v0, :cond_0

    .line 546
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mIsPaused:Z

    .line 547
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPlayBtn:Landroid/widget/ImageButton;

    const v2, 0x7f020002

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 554
    .end local v0    # "isPlayStarted":Z
    :cond_0
    :goto_1
    iget-boolean v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mIsPaused:Z

    if-eqz v1, :cond_1

    .line 555
    invoke-direct {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->initPausedScreen()V

    .line 558
    :cond_1
    return-void

    .line 543
    .restart local v0    # "isPlayStarted":Z
    :cond_2
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    iget v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->lastPlayedPos:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->play(J)Z

    move-result v0

    goto :goto_0

    .line 549
    .end local v0    # "isPlayStarted":Z
    :cond_3
    iget-boolean v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mIsPaused:Z

    if-nez v1, :cond_0

    .line 550
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    invoke-virtual {v1}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->pause()V

    .line 551
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPlayBtn:Landroid/widget/ImageButton;

    const v2, 0x7f020001

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 552
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mIsPaused:Z

    goto :goto_1
.end method

.method private prepareVideo()V
    .locals 2

    .prologue
    .line 682
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    invoke-virtual {v0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 683
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->setDataUri(Landroid/net/Uri;)V

    .line 684
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->setVisibility(I)V

    .line 687
    :cond_0
    return-void
.end method

.method private registerforSCover()V
    .locals 3

    .prologue
    .line 276
    new-instance v1, Lcom/samsung/android/sdk/cover/Scover;

    invoke-direct {v1}, Lcom/samsung/android/sdk/cover/Scover;-><init>()V

    iput-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    .line 278
    :try_start_0
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/cover/Scover;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/SsdkUnsupportedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 284
    :goto_0
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/cover/Scover;->isFeatureEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 285
    new-instance v1, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 286
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 288
    :cond_0
    return-void

    .line 279
    :catch_0
    move-exception v0

    .line 280
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 281
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 282
    .local v0, "e":Lcom/samsung/android/sdk/SsdkUnsupportedException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/SsdkUnsupportedException;->printStackTrace()V

    goto :goto_0
.end method

.method private setResolutionDataForResize()V
    .locals 7

    .prologue
    const/16 v6, 0x1e0

    const/4 v5, 0x5

    const/4 v4, 0x2

    const/4 v3, 0x4

    const/4 v2, 0x3

    .line 291
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v0, v0, Lcom/sec/android/app/video/ResolutionData;->width:I

    const/16 v1, 0xb0

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v0, v0, Lcom/sec/android/app/video/ResolutionData;->height:I

    const/16 v1, 0x90

    if-ne v0, v1, :cond_1

    .line 292
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    const/4 v1, 0x1

    iput v1, v0, Lcom/sec/android/app/video/ResolutionData;->resolutionType:I

    .line 293
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iput v4, v0, Lcom/sec/android/app/video/ResolutionData;->videoCodecType:I

    .line 294
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iput v5, v0, Lcom/sec/android/app/video/ResolutionData;->audioCodecType:I

    .line 316
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v0, v0, Lcom/sec/android/app/video/ResolutionData;->width:I

    const/16 v1, 0x140

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v0, v0, Lcom/sec/android/app/video/ResolutionData;->height:I

    const/16 v1, 0xf0

    if-ne v0, v1, :cond_2

    .line 296
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iput v4, v0, Lcom/sec/android/app/video/ResolutionData;->resolutionType:I

    .line 297
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iput v2, v0, Lcom/sec/android/app/video/ResolutionData;->videoCodecType:I

    .line 298
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iput v3, v0, Lcom/sec/android/app/video/ResolutionData;->audioCodecType:I

    goto :goto_0

    .line 299
    :cond_2
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v0, v0, Lcom/sec/android/app/video/ResolutionData;->width:I

    const/16 v1, 0x280

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v0, v0, Lcom/sec/android/app/video/ResolutionData;->height:I

    if-ne v0, v6, :cond_3

    .line 300
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iput v2, v0, Lcom/sec/android/app/video/ResolutionData;->resolutionType:I

    .line 301
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iput v2, v0, Lcom/sec/android/app/video/ResolutionData;->videoCodecType:I

    .line 302
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iput v3, v0, Lcom/sec/android/app/video/ResolutionData;->audioCodecType:I

    goto :goto_0

    .line 303
    :cond_3
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v0, v0, Lcom/sec/android/app/video/ResolutionData;->width:I

    const/16 v1, 0x2d0

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v0, v0, Lcom/sec/android/app/video/ResolutionData;->height:I

    if-ne v0, v6, :cond_4

    .line 304
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iput v3, v0, Lcom/sec/android/app/video/ResolutionData;->resolutionType:I

    .line 305
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iput v2, v0, Lcom/sec/android/app/video/ResolutionData;->videoCodecType:I

    .line 306
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iput v3, v0, Lcom/sec/android/app/video/ResolutionData;->audioCodecType:I

    goto :goto_0

    .line 307
    :cond_4
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v0, v0, Lcom/sec/android/app/video/ResolutionData;->width:I

    const/16 v1, 0x500

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v0, v0, Lcom/sec/android/app/video/ResolutionData;->height:I

    const/16 v1, 0x2d0

    if-ne v0, v1, :cond_5

    .line 308
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iput v5, v0, Lcom/sec/android/app/video/ResolutionData;->resolutionType:I

    .line 309
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iput v2, v0, Lcom/sec/android/app/video/ResolutionData;->videoCodecType:I

    .line 310
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iput v3, v0, Lcom/sec/android/app/video/ResolutionData;->audioCodecType:I

    goto :goto_0

    .line 311
    :cond_5
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v0, v0, Lcom/sec/android/app/video/ResolutionData;->width:I

    const/16 v1, 0x780

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v0, v0, Lcom/sec/android/app/video/ResolutionData;->height:I

    const/16 v1, 0x438

    if-ne v0, v1, :cond_0

    .line 312
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    const/4 v1, 0x6

    iput v1, v0, Lcom/sec/android/app/video/ResolutionData;->resolutionType:I

    .line 313
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iput v2, v0, Lcom/sec/android/app/video/ResolutionData;->videoCodecType:I

    .line 314
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iput v3, v0, Lcom/sec/android/app/video/ResolutionData;->audioCodecType:I

    goto/16 :goto_0
.end method

.method private showUnsupportedFormatDialog()V
    .locals 4

    .prologue
    .line 597
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 598
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 599
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060011

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 600
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060008

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/samsung/app/video/trimactivity/TrimActivity$9;

    invoke-direct {v3, p0}, Lcom/samsung/app/video/trimactivity/TrimActivity$9;-><init>(Lcom/samsung/app/video/trimactivity/TrimActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 607
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 608
    .local v1, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 609
    return-void
.end method

.method private startSeekbarUpdate()V
    .locals 7

    .prologue
    const-wide/16 v1, 0x64

    const-wide/16 v5, 0x14

    .line 612
    iget-wide v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    div-long/2addr v3, v5

    cmp-long v3, v3, v1

    if-gez v3, :cond_0

    iget-wide v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    div-long/2addr v1, v5

    :cond_0
    long-to-int v0, v1

    .line 613
    .local v0, "wait_time":I
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    new-instance v2, Lcom/samsung/app/video/trimactivity/TrimActivity$10;

    invoke-direct {v2, p0, v0}, Lcom/samsung/app/video/trimactivity/TrimActivity$10;-><init>(Lcom/samsung/app/video/trimactivity/TrimActivity;I)V

    invoke-virtual {v1, v2}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->post(Ljava/lang/Runnable;)Z

    .line 636
    return-void
.end method

.method private stopVideo()V
    .locals 1

    .prologue
    .line 672
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    if-eqz v0, :cond_0

    .line 674
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    invoke-virtual {v0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->stop()V

    .line 675
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mTrimBarMgr:Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;

    if-eqz v0, :cond_0

    .line 676
    invoke-direct {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->initPausedScreen()V

    .line 678
    :cond_0
    return-void
.end method

.method private validateVideoEditIntent(Landroid/content/Intent;)I
    .locals 3
    .param p1, "aIntent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 715
    if-nez p1, :cond_0

    move v1, v2

    .line 727
    :goto_0
    return v1

    .line 718
    :cond_0
    const-string v1, "uri"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    iput-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mVideoUri:Landroid/net/Uri;

    .line 720
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mVideoUri:Landroid/net/Uri;

    invoke-direct {p0, v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getVEEditFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 721
    .local v0, "filePath":Ljava/lang/String;
    if-nez v0, :cond_1

    move v1, v2

    .line 722
    goto :goto_0

    .line 724
    :cond_1
    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mVideoPath:Ljava/lang/String;

    .line 725
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mVideoPath:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/video/Utils;->getVideoDurationByPath(Ljava/lang/String;)J

    move-result-wide v1

    long-to-int v1, v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    .line 726
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mVideoPath:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/video/Utils;->getVideoResolutionFromURI(Ljava/lang/String;)Lcom/sec/android/app/video/ResolutionData;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolution:Lcom/sec/android/app/video/ResolutionData;

    .line 727
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private validateVideoEditIntent(ZLandroid/content/Intent;Z)I
    .locals 7
    .param p1, "aCreateNew"    # Z
    .param p2, "aIntent"    # Landroid/content/Intent;
    .param p3, "aRefreshStory"    # Z

    .prologue
    .line 766
    const/4 v1, 0x1

    .line 768
    .local v1, "ret":I
    :try_start_0
    invoke-direct {p0, p2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->validateVideoEditIntent(Landroid/content/Intent;)I

    move-result v3

    if-nez v3, :cond_0

    .line 769
    const/4 v1, 0x0

    move v2, v1

    .line 788
    .end local v1    # "ret":I
    .local v2, "ret":I
    :goto_0
    return v2

    .line 772
    .end local v2    # "ret":I
    .restart local v1    # "ret":I
    :cond_0
    iget-wide v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-gtz v3, :cond_1

    .line 773
    const/4 v1, 0x0

    move v2, v1

    .line 774
    .end local v1    # "ret":I
    .restart local v2    # "ret":I
    goto :goto_0

    .line 776
    .end local v2    # "ret":I
    .restart local v1    # "ret":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolution:Lcom/sec/android/app/video/ResolutionData;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolution:Lcom/sec/android/app/video/ResolutionData;

    invoke-static {v3}, Lcom/sec/android/app/video/Utils;->isSupportedResolution(Lcom/sec/android/app/video/ResolutionData;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 777
    const/4 v1, 0x0

    move v2, v1

    .line 778
    .end local v1    # "ret":I
    .restart local v2    # "ret":I
    goto :goto_0

    .line 781
    .end local v2    # "ret":I
    .restart local v1    # "ret":I
    :cond_2
    iget-wide v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-wide/16 v5, 0x3e8

    cmp-long v3, v3, v5

    if-gtz v3, :cond_3

    .line 782
    const/4 v1, 0x0

    move v2, v1

    .line 783
    .end local v1    # "ret":I
    .restart local v2    # "ret":I
    goto :goto_0

    .line 785
    .end local v2    # "ret":I
    .restart local v1    # "ret":I
    :catch_0
    move-exception v0

    .line 786
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    move v2, v1

    .line 788
    .end local v1    # "ret":I
    .restart local v2    # "ret":I
    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 219
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 220
    sput-object p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->_instance:Lcom/samsung/app/video/trimactivity/TrimActivity;

    .line 221
    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v4, "5"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 222
    const v3, 0x7f080001

    invoke-virtual {p0, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->setTheme(I)V

    .line 226
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mActionBar:Landroid/app/ActionBar;

    .line 227
    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v4, "4.4"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 228
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mActionBar:Landroid/app/ActionBar;

    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x106000d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-direct {v4, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 232
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 233
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "VIDEO_OUTPUT_SIZE"

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mOutputSize:J

    .line 234
    const-string v3, "VIDEO_OUTPUT_PATH"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/samsung/app/video/trimactivity/TrimActivity;->OUTPUT_PATH:Ljava/lang/String;

    .line 235
    iget-wide v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mOutputSize:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    .line 236
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/Videos/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/samsung/app/video/trimactivity/TrimActivity;->OUTPUT_PATH:Ljava/lang/String;

    .line 237
    new-instance v3, Lcom/sec/android/app/video/ResolutionData;

    invoke-direct {v3}, Lcom/sec/android/app/video/ResolutionData;-><init>()V

    iput-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    .line 239
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    const-string v4, "VIDEO_OUTPUT_WIDTH"

    const/16 v5, 0xb0

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, v3, Lcom/sec/android/app/video/ResolutionData;->width:I

    .line 240
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    const-string v4, "VIDEO_OUTPUT_HEIGHT"

    const/16 v5, 0x90

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, v3, Lcom/sec/android/app/video/ResolutionData;->height:I

    .line 241
    invoke-direct {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->setResolutionDataForResize()V

    .line 243
    :cond_0
    const/high16 v3, 0x7f030000

    invoke-virtual {p0, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->setContentView(I)V

    .line 244
    const/4 v2, 0x1

    .line 245
    .local v2, "ret":I
    invoke-direct {p0, v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->checkVideoEditIntent(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 246
    invoke-direct {p0, v8, v0, v7}, Lcom/samsung/app/video/trimactivity/TrimActivity;->validateVideoEditIntent(ZLandroid/content/Intent;Z)I

    move-result v2

    .line 247
    if-eq v2, v8, :cond_4

    .line 248
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f06000f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 249
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->finish()V

    .line 273
    :goto_2
    return-void

    .line 224
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "ret":I
    :cond_1
    const/high16 v3, 0x7f080000

    invoke-virtual {p0, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->setTheme(I)V

    goto/16 :goto_0

    .line 230
    :cond_2
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mActionBar:Landroid/app/ActionBar;

    const/high16 v4, 0x7f020000

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setIcon(I)V

    goto/16 :goto_1

    .line 253
    .restart local v0    # "intent":Landroid/content/Intent;
    .restart local v2    # "ret":I
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->finish()V

    goto :goto_2

    .line 256
    :cond_4
    const/high16 v3, 0x7f0a0000

    invoke-virtual {p0, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    iput-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    .line 257
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    iget-object v4, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreviewPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v3, v4}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 258
    const v3, 0x7f0a0001

    invoke-virtual {p0, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPlayBtn:Landroid/widget/ImageButton;

    .line 259
    const v3, 0x7f0a0002

    invoke-virtual {p0, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    iput-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreviewTimeline:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    .line 260
    const v3, 0x7f0a0003

    invoke-virtual {p0, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mYellowBox:Landroid/widget/ImageView;

    .line 261
    const v3, 0x7f0a0004

    invoke-virtual {p0, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDragHolder:Landroid/widget/ImageView;

    .line 262
    const v3, 0x7f0a0005

    invoke-virtual {p0, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mLeftText:Landroid/widget/TextView;

    .line 263
    const v3, 0x7f0a0006

    invoke-virtual {p0, v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mRightText:Landroid/widget/TextView;

    .line 265
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 266
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 267
    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 268
    const-string v3, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 269
    const-string v3, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 270
    const-string v3, "file"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 271
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mGlobalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 272
    invoke-direct {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->registerforSCover()V

    goto/16 :goto_2
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;

    .prologue
    .line 447
    const/4 v0, 0x0

    .line 449
    .local v0, "lDialog":Landroid/app/Dialog;
    sparse-switch p1, :sswitch_data_0

    .line 457
    .end local v0    # "lDialog":Landroid/app/Dialog;
    :goto_0
    return-object v0

    .line 451
    .restart local v0    # "lDialog":Landroid/app/Dialog;
    :sswitch_0
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mVideoPath:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/sec/android/app/video/DialogUtils;->createTrimDialog(Landroid/app/Activity;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 454
    :sswitch_1
    sget-object v1, Lcom/sec/android/app/video/VPTrimApp;->gResize:Lcom/samsung/app/video/resize/ResizeInterface;

    invoke-virtual {v1}, Lcom/samsung/app/video/resize/ResizeInterface;->getDialogListener()Lcom/sec/android/app/video/DialogUtils$DialogListener;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/video/DialogUtils;->createResizeProgressDialog(Landroid/content/Context;Lcom/sec/android/app/video/DialogUtils$DialogListener;)Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mProgress:Landroid/app/AlertDialog;

    .line 455
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mProgress:Landroid/app/AlertDialog;

    goto :goto_0

    .line 449
    nop

    :sswitch_data_0
    .sparse-switch
        0x320 -> :sswitch_0
        0x324 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 405
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 406
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 802
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->lastPlayedPos:I

    .line 803
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mGlobalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 805
    :try_start_0
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mGlobalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 811
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v1, :cond_1

    .line 812
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 814
    :cond_1
    sget-object v1, Lcom/sec/android/app/video/VPTrimApp;->gResize:Lcom/samsung/app/video/resize/ResizeInterface;

    invoke-virtual {v1}, Lcom/samsung/app/video/resize/ResizeInterface;->isResizeRunning()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 815
    sget-object v1, Lcom/sec/android/app/video/VPTrimApp;->gResize:Lcom/samsung/app/video/resize/ResizeInterface;

    invoke-virtual {v1}, Lcom/samsung/app/video/resize/ResizeInterface;->stopResize()V

    .line 817
    :cond_2
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 818
    return-void

    .line 807
    :catch_0
    move-exception v0

    .line 808
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 462
    packed-switch p1, :pswitch_data_0

    .line 468
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 464
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->releaseVideoResource()V

    .line 465
    invoke-direct {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->cancel()V

    .line 466
    const/4 v0, 0x1

    goto :goto_0

    .line 462
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 793
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 794
    invoke-direct {p0, p1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->checkVideoEditIntent(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 795
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->finish()V

    .line 798
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 425
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 442
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 427
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->releaseVideoResource()V

    .line 429
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->handler:Landroid/os/Handler;

    const/16 v1, 0xfe

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 433
    :sswitch_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->onPreviewPause(Z)V

    .line 434
    const/16 v0, 0x320

    invoke-virtual {p0, v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->showDialog(I)V

    goto :goto_0

    .line 438
    :sswitch_2
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->releaseVideoResource()V

    .line 439
    invoke-direct {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->cancel()V

    goto :goto_0

    .line 425
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0a000d -> :sswitch_2
        0x7f0a000e -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 390
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 391
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    if-eqz v0, :cond_0

    .line 392
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 393
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    invoke-virtual {v0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->stop()V

    .line 396
    :goto_0
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    invoke-virtual {v0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->unRegisterForHeadSetUnplugged()V

    .line 398
    :cond_0
    sget-object v0, Lcom/sec/android/app/video/VPTrimApp;->gResize:Lcom/samsung/app/video/resize/ResizeInterface;

    invoke-virtual {v0}, Lcom/samsung/app/video/resize/ResizeInterface;->isResizeRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 399
    sget-object v0, Lcom/sec/android/app/video/VPTrimApp;->gResize:Lcom/samsung/app/video/resize/ResizeInterface;

    invoke-virtual {v0}, Lcom/samsung/app/video/resize/ResizeInterface;->stopResize()V

    .line 401
    :cond_1
    return-void

    .line 395
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->onPreviewPause(Z)V

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x1

    .line 412
    :try_start_0
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mActionBar:Landroid/app/ActionBar;

    if-nez v1, :cond_0

    .line 413
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mActionBar:Landroid/app/ActionBar;

    .line 414
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mActionBar:Landroid/app/ActionBar;

    const v2, 0x7f060002

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(I)V

    .line 415
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 420
    :cond_0
    :goto_0
    return v3

    .line 417
    :catch_0
    move-exception v0

    .line 418
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 9

    .prologue
    const v8, 0x7f05001f

    const/4 v5, 0x5

    const/4 v4, 0x1

    .line 340
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 342
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mActionBar:Landroid/app/ActionBar;

    .line 343
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mActionBar:Landroid/app/ActionBar;

    const v1, 0x7f060002

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 344
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 345
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mActionBar:Landroid/app/ActionBar;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 346
    iget-wide v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 347
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Can not Trim. Output file duration is less than 1 sec."

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 348
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->finish()V

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    invoke-static {}, Lcom/samsung/app/share/via/external/NativeAccess;->getInstance()Lcom/samsung/app/share/via/external/NativeAccess;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mVideoPath:Ljava/lang/String;

    iget-wide v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mOutputSize:J

    long-to-int v2, v2

    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v3, v3, Lcom/sec/android/app/video/ResolutionData;->width:I

    iget-object v4, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mResolutionForResize:Lcom/sec/android/app/video/ResolutionData;

    iget v4, v4, Lcom/sec/android/app/video/ResolutionData;->height:I

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/app/share/via/external/NativeAccess;->getInputParamerterAnalysis(Ljava/lang/String;IIII)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarDuration:J

    .line 356
    iget-wide v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarDuration:J

    iget-wide v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 357
    iget-wide v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    iput-wide v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarDuration:J

    .line 359
    :cond_2
    const/4 v7, 0x0

    .line 360
    .local v7, "width":I
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 361
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v7, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 364
    :goto_1
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDragHolder:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->dragListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 365
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreviewListener:Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->setAdapter(Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;)V

    .line 366
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPlayBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 367
    iget-wide v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarDuration:J

    int-to-long v2, v7

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    div-long/2addr v0, v2

    long-to-int v0, v0

    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarWidth:I

    .line 368
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mYellowBox:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .line 369
    .local v6, "params":Landroid/view/ViewGroup$LayoutParams;
    iget v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarWidth:I

    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 370
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarWidth:I

    .line 372
    :cond_3
    iget v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarWidth:I

    iput v0, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 373
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mYellowBox:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 374
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->resizeElem:Lcom/samsung/app/share/via/external/ShareviaObj;

    if-nez v0, :cond_5

    .line 375
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mVideoPath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->createTranscodeElementForMMS(Ljava/lang/String;)Lcom/samsung/app/share/via/external/ShareviaObj;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->resizeElem:Lcom/samsung/app/share/via/external/ShareviaObj;

    .line 376
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->resizeElem:Lcom/samsung/app/share/via/external/ShareviaObj;

    iget-wide v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    invoke-virtual {v0, v1, v2}, Lcom/samsung/app/share/via/external/ShareviaObj;->setResizeDuration(J)V

    .line 377
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->resizeElem:Lcom/samsung/app/share/via/external/ShareviaObj;

    iget-wide v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mOutputSize:J

    invoke-virtual {v0, v1, v2}, Lcom/samsung/app/share/via/external/ShareviaObj;->setOutputFileSize(J)V

    .line 378
    iget v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarWidth:I

    sub-int v0, v7, v0

    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getTimeFromPos(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPosition:I

    .line 379
    iget-wide v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J

    long-to-int v0, v0

    invoke-direct {p0, v5, v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->init(II)V

    .line 383
    :goto_2
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    invoke-virtual {v0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->registerForHeadSetUnplugged()V

    goto/16 :goto_0

    .line 363
    .end local v6    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v7, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    goto/16 :goto_1

    .line 381
    .restart local v6    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_5
    invoke-direct {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->initPausedScreen()V

    goto :goto_2
.end method

.method public releaseVideoResource()V
    .locals 1

    .prologue
    .line 702
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    if-eqz v0, :cond_0

    .line 703
    invoke-direct {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->stopVideo()V

    .line 705
    :cond_0
    return-void
.end method

.method public save(Ljava/lang/String;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 561
    invoke-virtual {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->releaseVideoResource()V

    .line 562
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->resizeElem:Lcom/samsung/app/share/via/external/ShareviaObj;

    new-instance v2, Ljava/lang/StringBuilder;

    sget-object v3, Lcom/samsung/app/video/trimactivity/TrimActivity;->OUTPUT_PATH:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".mp4"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/app/share/via/external/ShareviaObj;->setResizeOutputFilename(Ljava/lang/String;)V

    .line 563
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->resizeElem:Lcom/samsung/app/share/via/external/ShareviaObj;

    iget v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPosition:I

    invoke-virtual {v1, v2}, Lcom/samsung/app/share/via/external/ShareviaObj;->setResizeStartTime(I)V

    .line 564
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->resizeElem:Lcom/samsung/app/share/via/external/ShareviaObj;

    iget v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mPosition:I

    int-to-long v2, v2

    iget-wide v4, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->mSelectBarDuration:J

    add-long/2addr v2, v4

    long-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/samsung/app/share/via/external/ShareviaObj;->setResizeEndTime(I)V

    .line 565
    const/16 v1, 0x320

    invoke-virtual {p0, v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->removeDialog(I)V

    .line 567
    :try_start_0
    sget-object v1, Lcom/sec/android/app/video/VPTrimApp;->gResize:Lcom/samsung/app/video/resize/ResizeInterface;

    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity;->resizeElem:Lcom/samsung/app/share/via/external/ShareviaObj;

    new-instance v3, Lcom/samsung/app/video/trimactivity/TrimActivity$ResAdapter;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/samsung/app/video/trimactivity/TrimActivity$ResAdapter;-><init>(Lcom/samsung/app/video/trimactivity/TrimActivity;Lcom/samsung/app/video/trimactivity/TrimActivity$ResAdapter;)V

    invoke-virtual {v1, p0, v2, v3}, Lcom/samsung/app/video/resize/ResizeInterface;->startResize(Landroid/app/Activity;Lcom/samsung/app/share/via/external/ShareviaObj;Lcom/samsung/app/video/resize/ResizeInterface$Adapter;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 572
    :goto_0
    return-void

    .line 568
    :catch_0
    move-exception v0

    .line 569
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->prepareVideo()V

    .line 570
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
