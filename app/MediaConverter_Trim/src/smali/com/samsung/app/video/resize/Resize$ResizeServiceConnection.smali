.class Lcom/samsung/app/video/resize/Resize$ResizeServiceConnection;
.super Ljava/lang/Object;
.source "Resize.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/resize/Resize;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ResizeServiceConnection"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/app/video/resize/Resize;


# direct methods
.method constructor <init>(Lcom/samsung/app/video/resize/Resize;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/samsung/app/video/resize/Resize$ResizeServiceConnection;->this$0:Lcom/samsung/app/video/resize/Resize;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "boundService"    # Landroid/os/IBinder;

    .prologue
    .line 169
    :try_start_0
    iget-object v1, p0, Lcom/samsung/app/video/resize/Resize$ResizeServiceConnection;->this$0:Lcom/samsung/app/video/resize/Resize;

    invoke-static {p2}, Lcom/samsung/app/video/resize/IResizeService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/app/video/resize/IResizeService;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/app/video/resize/Resize;->access$1(Lcom/samsung/app/video/resize/Resize;Lcom/samsung/app/video/resize/IResizeService;)V

    .line 170
    iget-object v1, p0, Lcom/samsung/app/video/resize/Resize$ResizeServiceConnection;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mService:Lcom/samsung/app/video/resize/IResizeService;
    invoke-static {v1}, Lcom/samsung/app/video/resize/Resize;->access$2(Lcom/samsung/app/video/resize/Resize;)Lcom/samsung/app/video/resize/IResizeService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/app/video/resize/IResizeService;->startResize()V

    .line 171
    iget-object v1, p0, Lcom/samsung/app/video/resize/Resize$ResizeServiceConnection;->this$0:Lcom/samsung/app/video/resize/Resize;

    const/4 v2, 0x5

    # invokes: Lcom/samsung/app/video/resize/Resize;->setResizeState(I)V
    invoke-static {v1, v2}, Lcom/samsung/app/video/resize/Resize;->access$3(Lcom/samsung/app/video/resize/Resize;I)V

    .line 172
    iget-object v1, p0, Lcom/samsung/app/video/resize/Resize$ResizeServiceConnection;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mHandler:Lcom/samsung/app/video/resize/Resize$ThreadHandler;
    invoke-static {v1}, Lcom/samsung/app/video/resize/Resize;->access$4(Lcom/samsung/app/video/resize/Resize;)Lcom/samsung/app/video/resize/Resize$ThreadHandler;

    move-result-object v1

    const/4 v2, 0x2

    const-wide/16 v3, 0xa

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 173
    iget-object v1, p0, Lcom/samsung/app/video/resize/Resize$ResizeServiceConnection;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mAdapter:Lcom/samsung/app/video/resize/ResizeInterface$Adapter;
    invoke-static {v1}, Lcom/samsung/app/video/resize/Resize;->access$5(Lcom/samsung/app/video/resize/Resize;)Lcom/samsung/app/video/resize/ResizeInterface$Adapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/app/video/resize/ResizeInterface$Adapter;->onResizeStarted()V

    .line 174
    iget-object v1, p0, Lcom/samsung/app/video/resize/Resize$ResizeServiceConnection;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/samsung/app/video/resize/Resize;->access$6(Lcom/samsung/app/video/resize/Resize;)Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x324

    invoke-virtual {v1, v2}, Landroid/app/Activity;->showDialog(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :goto_0
    return-void

    .line 175
    :catch_0
    move-exception v0

    .line 176
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ResizeServiceConnection;->this$0:Lcom/samsung/app/video/resize/Resize;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/app/video/resize/Resize;->access$1(Lcom/samsung/app/video/resize/Resize;Lcom/samsung/app/video/resize/IResizeService;)V

    .line 182
    return-void
.end method
