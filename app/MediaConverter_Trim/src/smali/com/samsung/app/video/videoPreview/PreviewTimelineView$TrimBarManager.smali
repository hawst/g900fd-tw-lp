.class public interface abstract Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;
.super Ljava/lang/Object;
.source "PreviewTimelineView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/videoPreview/PreviewTimelineView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TrimBarManager"
.end annotation


# virtual methods
.method public abstract getSeekBarPosition()I
.end method

.method public abstract getThumbnailCount()I
.end method

.method public abstract isThumbViewReady()Z
.end method

.method public abstract setSeekBarPosition(I)V
.end method

.method public abstract setSelectBarPosition(I)V
.end method

.method public abstract setThumbAt(ILandroid/graphics/Bitmap;)V
.end method
