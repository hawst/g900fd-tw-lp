.class public Lcom/samsung/app/video/resize/Resize;
.super Lcom/samsung/app/video/resize/ResizeInterface;
.source "Resize.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/app/video/resize/Resize$DialogListener;,
        Lcom/samsung/app/video/resize/Resize$ResizeServiceConnection;,
        Lcom/samsung/app/video/resize/Resize$ThreadHandler;
    }
.end annotation


# static fields
.field public static final ACTIVITY_PROJECT_EDIT:I = 0x1

.field public static final ACTIVITY_PROJECT_LIST:I


# instance fields
.field private ResizeState:I

.field private expFile:Ljava/io/File;

.field private lastProgress:I

.field private mActivity:Landroid/app/Activity;

.field private mAdapter:Lcom/samsung/app/video/resize/ResizeInterface$Adapter;

.field private final mConnection:Lcom/samsung/app/video/resize/Resize$ResizeServiceConnection;

.field private final mDlgLstnr:Lcom/samsung/app/video/resize/Resize$DialogListener;

.field private mFileName:Ljava/lang/String;

.field private final mHandler:Lcom/samsung/app/video/resize/Resize$ThreadHandler;

.field private mProgDlg:Landroid/app/AlertDialog;

.field private mProgText:Landroid/widget/TextView;

.field private mProgView:Landroid/widget/ProgressBar;

.field private mResObj:Lcom/samsung/app/share/via/external/ShareviaObj;

.field private mService:Lcom/samsung/app/video/resize/IResizeService;

.field private mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Lcom/samsung/app/video/resize/ResizeInterface;-><init>()V

    .line 33
    new-instance v0, Lcom/samsung/app/video/resize/Resize$DialogListener;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/resize/Resize$DialogListener;-><init>(Lcom/samsung/app/video/resize/Resize;)V

    iput-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mDlgLstnr:Lcom/samsung/app/video/resize/Resize$DialogListener;

    .line 34
    new-instance v0, Lcom/samsung/app/video/resize/Resize$ResizeServiceConnection;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/resize/Resize$ResizeServiceConnection;-><init>(Lcom/samsung/app/video/resize/Resize;)V

    iput-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mConnection:Lcom/samsung/app/video/resize/Resize$ResizeServiceConnection;

    .line 35
    new-instance v0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;

    invoke-direct {v0, p0}, Lcom/samsung/app/video/resize/Resize$ThreadHandler;-><init>(Lcom/samsung/app/video/resize/Resize;)V

    iput-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mHandler:Lcom/samsung/app/video/resize/Resize$ThreadHandler;

    .line 37
    iput-object v1, p0, Lcom/samsung/app/video/resize/Resize;->mAdapter:Lcom/samsung/app/video/resize/ResizeInterface$Adapter;

    .line 38
    iput-object v1, p0, Lcom/samsung/app/video/resize/Resize;->mResObj:Lcom/samsung/app/share/via/external/ShareviaObj;

    .line 39
    iput-object v1, p0, Lcom/samsung/app/video/resize/Resize;->mActivity:Landroid/app/Activity;

    .line 40
    iput-object v1, p0, Lcom/samsung/app/video/resize/Resize;->mFileName:Ljava/lang/String;

    .line 41
    iput-object v1, p0, Lcom/samsung/app/video/resize/Resize;->mService:Lcom/samsung/app/video/resize/IResizeService;

    .line 42
    iput-object v1, p0, Lcom/samsung/app/video/resize/Resize;->mProgDlg:Landroid/app/AlertDialog;

    .line 43
    iput-object v1, p0, Lcom/samsung/app/video/resize/Resize;->mProgView:Landroid/widget/ProgressBar;

    .line 44
    iput-object v1, p0, Lcom/samsung/app/video/resize/Resize;->mProgText:Landroid/widget/TextView;

    .line 45
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/app/video/resize/Resize;->ResizeState:I

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/app/video/resize/Resize;->lastProgress:I

    .line 31
    return-void
.end method

.method static synthetic access$1(Lcom/samsung/app/video/resize/Resize;Lcom/samsung/app/video/resize/IResizeService;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/app/video/resize/Resize;->mService:Lcom/samsung/app/video/resize/IResizeService;

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/app/video/resize/Resize;)V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Lcom/samsung/app/video/resize/Resize;->releaseService()V

    return-void
.end method

.method static synthetic access$11(Lcom/samsung/app/video/resize/Resize;I)V
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/samsung/app/video/resize/Resize;->lastProgress:I

    return-void
.end method

.method static synthetic access$12(Lcom/samsung/app/video/resize/Resize;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$13(Lcom/samsung/app/video/resize/Resize;Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/app/video/resize/Resize;->mProgDlg:Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/app/video/resize/Resize;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$15(Lcom/samsung/app/video/resize/Resize;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mProgDlg:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$16(Lcom/samsung/app/video/resize/Resize;)I
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/samsung/app/video/resize/Resize;->getResizeState()I

    move-result v0

    return v0
.end method

.method static synthetic access$17(Lcom/samsung/app/video/resize/Resize;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/samsung/app/video/resize/Resize;->lastProgress:I

    return v0
.end method

.method static synthetic access$18(Lcom/samsung/app/video/resize/Resize;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/samsung/app/video/resize/Resize;->mFileName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$19(Lcom/samsung/app/video/resize/Resize;)V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Lcom/samsung/app/video/resize/Resize;->initService()V

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/app/video/resize/Resize;)Lcom/samsung/app/video/resize/IResizeService;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mService:Lcom/samsung/app/video/resize/IResizeService;

    return-object v0
.end method

.method static synthetic access$20(Lcom/samsung/app/video/resize/Resize;Landroid/widget/ProgressBar;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/app/video/resize/Resize;->mProgView:Landroid/widget/ProgressBar;

    return-void
.end method

.method static synthetic access$21(Lcom/samsung/app/video/resize/Resize;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/app/video/resize/Resize;->mProgText:Landroid/widget/TextView;

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/app/video/resize/Resize;I)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/samsung/app/video/resize/Resize;->setResizeState(I)V

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/app/video/resize/Resize;)Lcom/samsung/app/video/resize/Resize$ThreadHandler;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mHandler:Lcom/samsung/app/video/resize/Resize$ThreadHandler;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/app/video/resize/Resize;)Lcom/samsung/app/video/resize/ResizeInterface$Adapter;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mAdapter:Lcom/samsung/app/video/resize/ResizeInterface$Adapter;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/app/video/resize/Resize;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/app/video/resize/Resize;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mProgView:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/app/video/resize/Resize;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mProgText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/app/video/resize/Resize;)V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/samsung/app/video/resize/Resize;->runMediaScanner()V

    return-void
.end method

.method private getResizeState()I
    .locals 2

    .prologue
    .line 109
    :try_start_0
    iget-object v1, p0, Lcom/samsung/app/video/resize/Resize;->mService:Lcom/samsung/app/video/resize/IResizeService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/app/video/resize/Resize;->mService:Lcom/samsung/app/video/resize/IResizeService;

    invoke-interface {v1}, Lcom/samsung/app/video/resize/IResizeService;->isEThreadAlive()Z

    move-result v1

    if-nez v1, :cond_1

    .line 110
    :cond_0
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/samsung/app/video/resize/Resize;->setResizeState(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :cond_1
    :goto_0
    iget v1, p0, Lcom/samsung/app/video/resize/Resize;->ResizeState:I

    return v1

    .line 111
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private initService()V
    .locals 4

    .prologue
    .line 125
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 126
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.samsung.app.video"

    const-string v2, "com.samsung.app.video.resize.ResizeService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    sget-object v1, Lcom/sec/android/app/video/VPTrimApp;->gContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/app/video/resize/Resize;->mConnection:Lcom/samsung/app/video/resize/Resize$ResizeServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 128
    return-void
.end method

.method private releaseService()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mService:Lcom/samsung/app/video/resize/IResizeService;

    if-eqz v0, :cond_0

    .line 132
    sget-object v0, Lcom/sec/android/app/video/VPTrimApp;->gContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/app/video/resize/Resize;->mConnection:Lcom/samsung/app/video/resize/Resize$ResizeServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 133
    :cond_0
    return-void
.end method

.method private reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 137
    iput-object v2, p0, Lcom/samsung/app/video/resize/Resize;->mResObj:Lcom/samsung/app/share/via/external/ShareviaObj;

    .line 138
    iput-object v2, p0, Lcom/samsung/app/video/resize/Resize;->mAdapter:Lcom/samsung/app/video/resize/ResizeInterface$Adapter;

    .line 139
    iput-object v2, p0, Lcom/samsung/app/video/resize/Resize;->mFileName:Ljava/lang/String;

    .line 140
    iput-object v2, p0, Lcom/samsung/app/video/resize/Resize;->mService:Lcom/samsung/app/video/resize/IResizeService;

    .line 141
    iput-object v2, p0, Lcom/samsung/app/video/resize/Resize;->mActivity:Landroid/app/Activity;

    .line 142
    sget-object v0, Lcom/samsung/app/video/trimactivity/TrimActivity;->OUTPUT_PATH:Ljava/lang/String;

    .line 143
    .local v0, "filepath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 144
    .local v1, "veDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_1

    .line 145
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 147
    :cond_1
    return-void
.end method

.method private runMediaScanner()V
    .locals 4

    .prologue
    .line 151
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    .line 152
    .local v0, "exPath":[Ljava/lang/String;
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/samsung/app/video/resize/Resize;->getFileName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 153
    iget-object v1, p0, Lcom/samsung/app/video/resize/Resize;->mActivity:Landroid/app/Activity;

    const/4 v2, 0x0

    new-instance v3, Lcom/samsung/app/video/resize/Resize$1;

    invoke-direct {v3, p0}, Lcom/samsung/app/video/resize/Resize$1;-><init>(Lcom/samsung/app/video/resize/Resize;)V

    invoke-static {v1, v0, v2, v3}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 159
    return-void
.end method

.method private setResizeState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 81
    iput p1, p0, Lcom/samsung/app/video/resize/Resize;->ResizeState:I

    .line 82
    return-void
.end method


# virtual methods
.method public getDialogListener()Lcom/samsung/app/video/resize/Resize$DialogListener;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mDlgLstnr:Lcom/samsung/app/video/resize/Resize$DialogListener;

    return-object v0
.end method

.method public bridge synthetic getDialogListener()Lcom/sec/android/app/video/DialogUtils$DialogListener;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/samsung/app/video/resize/Resize;->getDialogListener()Lcom/samsung/app/video/resize/Resize$DialogListener;

    move-result-object v0

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mHandler:Lcom/samsung/app/video/resize/Resize$ThreadHandler;

    return-object v0
.end method

.method public getResizeObject()Lcom/samsung/app/share/via/external/ShareviaObj;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mResObj:Lcom/samsung/app/share/via/external/ShareviaObj;

    return-object v0
.end method

.method public getResizeProgress()I
    .locals 6

    .prologue
    .line 276
    iget-object v2, p0, Lcom/samsung/app/video/resize/Resize;->expFile:Ljava/io/File;

    if-nez v2, :cond_0

    .line 277
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/samsung/app/video/resize/Resize;->mFileName:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/app/video/resize/Resize;->expFile:Ljava/io/File;

    .line 278
    :cond_0
    iget-object v2, p0, Lcom/samsung/app/video/resize/Resize;->expFile:Ljava/io/File;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/app/video/resize/Resize;->expFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/app/video/resize/Resize;->expFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v2

    if-nez v2, :cond_2

    .line 280
    :cond_1
    const/4 v2, 0x0

    .line 285
    :goto_0
    return v2

    .line 284
    :cond_2
    iget-object v2, p0, Lcom/samsung/app/video/resize/Resize;->expFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 285
    .local v0, "currentSize":J
    const-wide/16 v2, 0x400

    div-long v2, v0, v2

    const-wide/16 v4, 0x64

    mul-long/2addr v2, v4

    iget-object v4, p0, Lcom/samsung/app/video/resize/Resize;->mResObj:Lcom/samsung/app/share/via/external/ShareviaObj;

    invoke-virtual {v4}, Lcom/samsung/app/share/via/external/ShareviaObj;->getOutputFileSize()J

    move-result-wide v4

    div-long/2addr v2, v4

    long-to-int v2, v2

    goto :goto_0
.end method

.method public initResizeProgress(Ljava/lang/String;)V
    .locals 2
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 291
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/app/video/resize/Resize;->expFile:Ljava/io/File;

    .line 292
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize;->mHandler:Lcom/samsung/app/video/resize/Resize$ThreadHandler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->sendEmptyMessage(I)Z

    .line 293
    return-void
.end method

.method public isResizeRunning()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 98
    :try_start_0
    iget-object v2, p0, Lcom/samsung/app/video/resize/Resize;->mService:Lcom/samsung/app/video/resize/IResizeService;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/app/video/resize/Resize;->mService:Lcom/samsung/app/video/resize/IResizeService;

    invoke-interface {v2}, Lcom/samsung/app/video/resize/IResizeService;->isEThreadAlive()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    :goto_0
    return v1

    .line 98
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onAudioFocusChange(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 267
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 268
    const/4 v0, -0x2

    if-eq p1, v0, :cond_0

    const/4 v0, -0x3

    if-ne p1, v0, :cond_1

    .line 269
    :cond_0
    invoke-direct {p0}, Lcom/samsung/app/video/resize/Resize;->getResizeState()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 270
    invoke-virtual {p0}, Lcom/samsung/app/video/resize/Resize;->stopResize()V

    .line 272
    :cond_1
    return-void
.end method

.method public startResize(Landroid/app/Activity;Lcom/samsung/app/share/via/external/ShareviaObj;Lcom/samsung/app/video/resize/ResizeInterface$Adapter;)Z
    .locals 4
    .param p1, "actvt"    # Landroid/app/Activity;
    .param p2, "resObj"    # Lcom/samsung/app/share/via/external/ShareviaObj;
    .param p3, "adp"    # Lcom/samsung/app/video/resize/ResizeInterface$Adapter;

    .prologue
    const/4 v3, 0x0

    .line 54
    const/4 v0, 0x0

    .line 55
    .local v0, "res":Z
    invoke-virtual {p0}, Lcom/samsung/app/video/resize/Resize;->isResizeRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    .end local v0    # "res":Z
    :goto_0
    return v0

    .line 57
    .restart local v0    # "res":Z
    :cond_0
    invoke-static {}, Lcom/sec/android/app/video/Utils;->checkStorage()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 58
    const v1, 0x7f06000c

    const/4 v2, -0x1

    invoke-static {v1, v2, v3, v3}, Lcom/sec/android/app/video/VPTrimApp;->showToast(IIII)V

    goto :goto_0

    .line 61
    :cond_1
    invoke-direct {p0}, Lcom/samsung/app/video/resize/Resize;->reset()V

    .line 62
    iput-object p2, p0, Lcom/samsung/app/video/resize/Resize;->mResObj:Lcom/samsung/app/share/via/external/ShareviaObj;

    .line 63
    iput-object p3, p0, Lcom/samsung/app/video/resize/Resize;->mAdapter:Lcom/samsung/app/video/resize/ResizeInterface$Adapter;

    .line 64
    iput-object p1, p0, Lcom/samsung/app/video/resize/Resize;->mActivity:Landroid/app/Activity;

    .line 65
    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/samsung/app/video/resize/Resize;->mActivity:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/app/video/resize/Resize;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

    .line 66
    sget-object v1, Lcom/sec/android/app/video/VPTrimApp;->gResize:Lcom/samsung/app/video/resize/ResizeInterface;

    invoke-virtual {v1}, Lcom/samsung/app/video/resize/ResizeInterface;->getDialogListener()Lcom/sec/android/app/video/DialogUtils$DialogListener;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/app/share/via/external/ShareviaObj;->getResizeOutputFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/video/DialogUtils$DialogListener;->startResize(Ljava/lang/String;)V

    .line 67
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public stopResize()V
    .locals 2

    .prologue
    .line 73
    :try_start_0
    iget-object v1, p0, Lcom/samsung/app/video/resize/Resize;->mService:Lcom/samsung/app/video/resize/IResizeService;

    invoke-interface {v1}, Lcom/samsung/app/video/resize/IResizeService;->stopResize()V

    .line 74
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/samsung/app/video/resize/Resize;->setResizeState(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    :goto_0
    return-void

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
