.class Lcom/samsung/app/video/trimactivity/TrimActivity$ResAdapter;
.super Lcom/samsung/app/video/resize/ResizeInterface$Adapter;
.source "TrimActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/trimactivity/TrimActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ResAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;


# direct methods
.method private constructor <init>(Lcom/samsung/app/video/trimactivity/TrimActivity;)V
    .locals 0

    .prologue
    .line 574
    iput-object p1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$ResAdapter;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    invoke-direct {p0}, Lcom/samsung/app/video/resize/ResizeInterface$Adapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/app/video/trimactivity/TrimActivity;Lcom/samsung/app/video/trimactivity/TrimActivity$ResAdapter;)V
    .locals 0

    .prologue
    .line 574
    invoke-direct {p0, p1}, Lcom/samsung/app/video/trimactivity/TrimActivity$ResAdapter;-><init>(Lcom/samsung/app/video/trimactivity/TrimActivity;)V

    return-void
.end method


# virtual methods
.method public getProgressDialog()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$ResAdapter;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mProgress:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$18(Lcom/samsung/app/video/trimactivity/TrimActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onResizeCompleted(Landroid/net/Uri;)V
    .locals 5
    .param p1, "fileUri"    # Landroid/net/Uri;

    .prologue
    .line 582
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$ResAdapter;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    iget-object v1, v1, Lcom/samsung/app/video/trimactivity/TrimActivity;->handler:Landroid/os/Handler;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 583
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 584
    .local v0, "resIntent":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 585
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$ResAdapter;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->setResult(ILandroid/content/Intent;)V

    .line 587
    iget-object v1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$ResAdapter;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    iget-object v1, v1, Lcom/samsung/app/video/trimactivity/TrimActivity;->handler:Landroid/os/Handler;

    const/16 v2, 0xfe

    const-wide/16 v3, 0x1f4

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 588
    return-void
.end method

.method public onResizeFailed()V
    .locals 1

    .prologue
    .line 577
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$ResAdapter;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # invokes: Lcom/samsung/app/video/trimactivity/TrimActivity;->prepareVideo()V
    invoke-static {v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$17(Lcom/samsung/app/video/trimactivity/TrimActivity;)V

    .line 578
    return-void
.end method
