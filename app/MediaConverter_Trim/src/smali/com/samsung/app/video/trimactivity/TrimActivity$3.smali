.class Lcom/samsung/app/video/trimactivity/TrimActivity$3;
.super Landroid/content/BroadcastReceiver;
.source "TrimActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/trimactivity/TrimActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;


# direct methods
.method constructor <init>(Lcom/samsung/app/video/trimactivity/TrimActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$3;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    .line 118
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 121
    if-eqz p2, :cond_1

    .line 122
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 123
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 124
    const-string v2, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 125
    const-string v2, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 126
    :cond_0
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$3;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$0(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 127
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$3;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$0(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->stop()V

    .line 138
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 129
    .restart local v0    # "action":Ljava/lang/String;
    :cond_2
    const-string v2, "android.intent.action.PHONE_STATE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 130
    const-string v2, "state"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 131
    .local v1, "state":Ljava/lang/String;
    sget-object v2, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 132
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$3;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$0(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 133
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$3;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$0(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->stop()V

    goto :goto_0
.end method
