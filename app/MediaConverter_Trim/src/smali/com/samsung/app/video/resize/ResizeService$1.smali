.class Lcom/samsung/app/video/resize/ResizeService$1;
.super Lcom/samsung/app/video/resize/IResizeService$Stub;
.source "ResizeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/app/video/resize/ResizeService;->onBind(Landroid/content/Intent;)Lcom/samsung/app/video/resize/IResizeService$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/app/video/resize/ResizeService;


# direct methods
.method constructor <init>(Lcom/samsung/app/video/resize/ResizeService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/app/video/resize/ResizeService$1;->this$0:Lcom/samsung/app/video/resize/ResizeService;

    .line 18
    invoke-direct {p0}, Lcom/samsung/app/video/resize/IResizeService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public isEThreadAlive()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/app/video/resize/ResizeService$1;->this$0:Lcom/samsung/app/video/resize/ResizeService;

    # getter for: Lcom/samsung/app/video/resize/ResizeService;->eThread:Lcom/samsung/app/video/resize/ResizeThread;
    invoke-static {v0}, Lcom/samsung/app/video/resize/ResizeService;->access$2(Lcom/samsung/app/video/resize/ResizeService;)Lcom/samsung/app/video/resize/ResizeThread;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/app/video/resize/ResizeService$1;->this$0:Lcom/samsung/app/video/resize/ResizeService;

    # getter for: Lcom/samsung/app/video/resize/ResizeService;->eThread:Lcom/samsung/app/video/resize/ResizeThread;
    invoke-static {v0}, Lcom/samsung/app/video/resize/ResizeService;->access$2(Lcom/samsung/app/video/resize/ResizeService;)Lcom/samsung/app/video/resize/ResizeThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/resize/ResizeThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startResize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/app/video/resize/ResizeService$1;->this$0:Lcom/samsung/app/video/resize/ResizeService;

    new-instance v1, Lcom/samsung/app/video/resize/ResizeThread;

    invoke-direct {v1}, Lcom/samsung/app/video/resize/ResizeThread;-><init>()V

    invoke-static {v0, v1}, Lcom/samsung/app/video/resize/ResizeService;->access$1(Lcom/samsung/app/video/resize/ResizeService;Lcom/samsung/app/video/resize/ResizeThread;)V

    .line 21
    iget-object v0, p0, Lcom/samsung/app/video/resize/ResizeService$1;->this$0:Lcom/samsung/app/video/resize/ResizeService;

    # getter for: Lcom/samsung/app/video/resize/ResizeService;->eThread:Lcom/samsung/app/video/resize/ResizeThread;
    invoke-static {v0}, Lcom/samsung/app/video/resize/ResizeService;->access$2(Lcom/samsung/app/video/resize/ResizeService;)Lcom/samsung/app/video/resize/ResizeThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/resize/ResizeThread;->start()V

    .line 22
    return-void
.end method

.method public stopResize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/app/video/resize/ResizeService$1;->this$0:Lcom/samsung/app/video/resize/ResizeService;

    # getter for: Lcom/samsung/app/video/resize/ResizeService;->eThread:Lcom/samsung/app/video/resize/ResizeThread;
    invoke-static {v0}, Lcom/samsung/app/video/resize/ResizeService;->access$2(Lcom/samsung/app/video/resize/ResizeService;)Lcom/samsung/app/video/resize/ResizeThread;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/samsung/app/video/resize/ResizeService$1;->this$0:Lcom/samsung/app/video/resize/ResizeService;

    # getter for: Lcom/samsung/app/video/resize/ResizeService;->eThread:Lcom/samsung/app/video/resize/ResizeThread;
    invoke-static {v0}, Lcom/samsung/app/video/resize/ResizeService;->access$2(Lcom/samsung/app/video/resize/ResizeService;)Lcom/samsung/app/video/resize/ResizeThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/resize/ResizeThread;->stopResize()V

    .line 27
    :cond_0
    return-void
.end method
