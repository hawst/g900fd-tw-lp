.class public Lcom/samsung/app/video/resize/ResizeThread;
.super Ljava/lang/Thread;
.source "ResizeThread.java"


# instance fields
.field private fileName:Ljava/lang/String;

.field private mResizeObj:Lcom/samsung/app/share/via/external/ShareviaObj;

.field private wakelock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 19
    sget-object v0, Lcom/sec/android/app/video/VPTrimApp;->gResize:Lcom/samsung/app/video/resize/ResizeInterface;

    invoke-virtual {v0}, Lcom/samsung/app/video/resize/ResizeInterface;->getResizeObject()Lcom/samsung/app/share/via/external/ShareviaObj;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/app/video/resize/ResizeThread;->mResizeObj:Lcom/samsung/app/share/via/external/ShareviaObj;

    .line 20
    sget-object v0, Lcom/sec/android/app/video/VPTrimApp;->gResize:Lcom/samsung/app/video/resize/ResizeInterface;

    invoke-virtual {v0}, Lcom/samsung/app/video/resize/ResizeInterface;->getFileName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/app/video/resize/ResizeThread;->fileName:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public getFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 56
    iget-object v1, p0, Lcom/samsung/app/video/resize/ResizeThread;->fileName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/app/video/resize/ResizeThread;->fileName:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "fName":Ljava/lang/String;
    return-object v0
.end method

.method public releaseWakelock()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 52
    :cond_0
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 25
    :try_start_0
    sget-object v2, Lcom/sec/android/app/video/VPTrimApp;->gContext:Landroid/content/Context;

    const-string v3, "power"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    const/16 v3, 0xa

    const-string v4, "VE Export Thread"

    invoke-virtual {v2, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 26
    iget-object v2, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 27
    invoke-static {}, Lcom/samsung/app/share/via/external/NativeAccess;->getInstance()Lcom/samsung/app/share/via/external/NativeAccess;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/app/video/resize/ResizeThread;->mResizeObj:Lcom/samsung/app/share/via/external/ShareviaObj;

    invoke-virtual {v2, v3}, Lcom/samsung/app/share/via/external/NativeAccess;->startProcessing(Lcom/samsung/app/share/via/external/ShareviaObj;)I

    move-result v1

    .line 28
    .local v1, "returnVal":I
    if-ne v1, v5, :cond_1

    .line 29
    sget-object v2, Lcom/sec/android/app/video/VPTrimApp;->gResize:Lcom/samsung/app/video/resize/ResizeInterface;

    invoke-virtual {v2}, Lcom/samsung/app/video/resize/ResizeInterface;->getHandler()Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x1

    const-wide/16 v4, 0xa

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    iget-object v2, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 38
    iget-object v2, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 41
    .end local v1    # "returnVal":I
    :cond_0
    :goto_0
    return-void

    .line 32
    .restart local v1    # "returnVal":I
    :cond_1
    :try_start_1
    sget-object v2, Lcom/sec/android/app/video/VPTrimApp;->gResize:Lcom/samsung/app/video/resize/ResizeInterface;

    invoke-virtual {v2}, Lcom/samsung/app/video/resize/ResizeInterface;->getHandler()Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    const-wide/16 v4, 0xa

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 37
    iget-object v2, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 38
    iget-object v2, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    .line 34
    .end local v1    # "returnVal":I
    :catch_0
    move-exception v0

    .line 35
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 37
    iget-object v2, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 38
    iget-object v2, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    .line 36
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 37
    iget-object v3, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 38
    iget-object v3, p0, Lcom/samsung/app/video/resize/ResizeThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 39
    :cond_2
    throw v2
.end method

.method public stopResize()V
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Lcom/samsung/app/share/via/external/NativeAccess;->getInstance()Lcom/samsung/app/share/via/external/NativeAccess;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/share/via/external/NativeAccess;->stopProcessing()I

    .line 45
    return-void
.end method
