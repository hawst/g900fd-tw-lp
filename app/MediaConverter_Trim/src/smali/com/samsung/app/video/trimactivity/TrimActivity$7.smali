.class Lcom/samsung/app/video/trimactivity/TrimActivity$7;
.super Ljava/lang/Object;
.source "TrimActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/trimactivity/TrimActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;


# direct methods
.method constructor <init>(Lcom/samsung/app/video/trimactivity/TrimActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$7;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    .line 689
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 693
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$7;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPreview:Lcom/samsung/app/video/videoPreview/VideoFilePreview;
    invoke-static {v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$0(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/video/videoPreview/VideoFilePreview;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 694
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$7;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/app/video/trimactivity/TrimActivity;->onPreviewPause(Z)V
    invoke-static {v0, v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$4(Lcom/samsung/app/video/trimactivity/TrimActivity;Z)V

    .line 698
    :goto_0
    return-void

    .line 696
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$7;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    const/4 v1, 0x1

    # invokes: Lcom/samsung/app/video/trimactivity/TrimActivity;->onPreviewPause(Z)V
    invoke-static {v0, v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$4(Lcom/samsung/app/video/trimactivity/TrimActivity;Z)V

    goto :goto_0
.end method
