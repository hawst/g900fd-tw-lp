.class public Lcom/samsung/app/video/videoPreview/PreviewTimelineView;
.super Landroid/view/View;
.source "PreviewTimelineView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;,
        Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;
    }
.end annotation


# instance fields
.field private mBackgoundRect:Landroid/graphics/Rect;

.field private final mBarManager:Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;

.field private mBaseCanvas:Landroid/graphics/Canvas;

.field private mBitmapBackground:Landroid/graphics/Bitmap;

.field private mBitmapList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mEndPos:I

.field private mGreyBitmapRect:Landroid/graphics/Rect;

.field private mIsReady:Z

.field private mLength:I

.field private mPaintGrayscale:Landroid/graphics/Paint;

.field private mSeekBarPos:I

.field private mSelectBarPos:I

.field private mSelectBarWidth:I

.field private mStartPos:I

.field private mThumbBarWidth:I

.field private mThumbCount:I

.field private mThumbnailHeight:I

.field private mThumbnailWidth:I

.field private mVideoDuration:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 58
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 20
    new-instance v0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;

    invoke-direct {v0, p0, v2}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;-><init>(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;)V

    iput-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBarManager:Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;

    .line 21
    iput-object v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapBackground:Landroid/graphics/Bitmap;

    .line 22
    iput-object v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    .line 23
    iput-object v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mContext:Landroid/content/Context;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    .line 27
    iput-object v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBackgoundRect:Landroid/graphics/Rect;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mIsReady:Z

    .line 29
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mEndPos:I

    .line 30
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSelectBarPos:I

    .line 31
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mStartPos:I

    .line 32
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbBarWidth:I

    .line 33
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbCount:I

    .line 34
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailHeight:I

    .line 35
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    .line 36
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSelectBarWidth:I

    .line 37
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mVideoDuration:I

    .line 38
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mLength:I

    .line 39
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSeekBarPos:I

    .line 59
    invoke-virtual {p0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->create(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 50
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    new-instance v0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;

    invoke-direct {v0, p0, v2}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;-><init>(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;)V

    iput-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBarManager:Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;

    .line 21
    iput-object v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapBackground:Landroid/graphics/Bitmap;

    .line 22
    iput-object v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    .line 23
    iput-object v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mContext:Landroid/content/Context;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    .line 27
    iput-object v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBackgoundRect:Landroid/graphics/Rect;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mIsReady:Z

    .line 29
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mEndPos:I

    .line 30
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSelectBarPos:I

    .line 31
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mStartPos:I

    .line 32
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbBarWidth:I

    .line 33
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbCount:I

    .line 34
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailHeight:I

    .line 35
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    .line 36
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSelectBarWidth:I

    .line 37
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mVideoDuration:I

    .line 38
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mLength:I

    .line 39
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSeekBarPos:I

    .line 51
    invoke-virtual {p0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    :goto_0
    return-void

    .line 54
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->create(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    new-instance v0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;

    invoke-direct {v0, p0, v2}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;-><init>(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;)V

    iput-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBarManager:Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;

    .line 21
    iput-object v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapBackground:Landroid/graphics/Bitmap;

    .line 22
    iput-object v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    .line 23
    iput-object v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mContext:Landroid/content/Context;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    .line 27
    iput-object v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBackgoundRect:Landroid/graphics/Rect;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mIsReady:Z

    .line 29
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mEndPos:I

    .line 30
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSelectBarPos:I

    .line 31
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mStartPos:I

    .line 32
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbBarWidth:I

    .line 33
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbCount:I

    .line 34
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailHeight:I

    .line 35
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    .line 36
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSelectBarWidth:I

    .line 37
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mVideoDuration:I

    .line 38
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mLength:I

    .line 39
    iput v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSeekBarPos:I

    .line 43
    invoke-virtual {p0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    :goto_0
    return-void

    .line 46
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->create(Landroid/content/Context;)V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;I)I
    .locals 1

    .prologue
    .line 216
    invoke-direct {p0, p1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getSizeInPixels(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;I)V
    .locals 0

    .prologue
    .line 30
    iput p1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSelectBarPos:I

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;ILandroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 220
    invoke-direct {p0, p1, p2}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->setThumb(ILandroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;)Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mIsReady:Z

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;)I
    .locals 1

    .prologue
    .line 267
    invoke-direct {p0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getThumbCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;I)I
    .locals 1

    .prologue
    .line 327
    invoke-direct {p0, p1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getPosFromTime(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$6(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;I)V
    .locals 0

    .prologue
    .line 39
    iput p1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSeekBarPos:I

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSeekBarPos:I

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;I)I
    .locals 1

    .prologue
    .line 335
    invoke-direct {p0, p1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getTimeFromPos(I)I

    move-result v0

    return v0
.end method

.method private create(Landroid/content/Context;)V
    .locals 3
    .param p1, "cntxt"    # Landroid/content/Context;

    .prologue
    .line 272
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mPaintGrayscale:Landroid/graphics/Paint;

    .line 273
    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 274
    .local v0, "cm":Landroid/graphics/ColorMatrix;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 275
    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v1, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 276
    .local v1, "f":Landroid/graphics/ColorMatrixColorFilter;
    iget-object v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mPaintGrayscale:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 277
    iput-object p1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mContext:Landroid/content/Context;

    .line 278
    const v2, 0x7f050004

    invoke-direct {p0, v2}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getSizeInPixels(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbBarWidth:I

    .line 279
    const v2, 0x7f050005

    invoke-direct {p0, v2}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getSizeInPixels(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    .line 280
    const v2, 0x7f050006

    invoke-direct {p0, v2}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getSizeInPixels(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailHeight:I

    .line 281
    return-void
.end method

.method private drawBase(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 284
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapBackground:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBackgoundRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v2, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 285
    return-void
.end method

.method private drawBitmapWithGrey()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 85
    const/4 v2, 0x0

    .line 86
    .local v2, "greyWidth":I
    const/4 v5, 0x0

    .line 87
    .local v5, "plainWidth":I
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v6}, Landroid/graphics/Canvas;->save()I

    .line 88
    invoke-direct {p0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getThumbCount()I

    move-result v1

    .line 89
    .local v1, "count":I
    iget v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSelectBarPos:I

    .line 90
    iget v5, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSelectBarWidth:I

    .line 91
    iget v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbBarWidth:I

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    .line 92
    const/4 v3, 0x0

    .line 93
    .local v3, "i":I
    :cond_0
    :goto_0
    iget v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    if-gt v2, v6, :cond_5

    .line 105
    iget v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    sub-int v4, v6, v2

    .line 106
    .local v4, "leftOver":I
    if-lez v2, :cond_7

    if-lt v5, v4, :cond_7

    .line 107
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v3, v6, :cond_1

    .line 108
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v3, v6, -0x1

    .line 110
    :cond_1
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 111
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 112
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    iput v12, v6, Landroid/graphics/Rect;->left:I

    .line 113
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    iput v2, v6, Landroid/graphics/Rect;->right:I

    .line 114
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    iget-object v7, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    .line 115
    iget-object v8, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mPaintGrayscale:Landroid/graphics/Paint;

    .line 114
    invoke-virtual {v6, v0, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 116
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    iput v2, v6, Landroid/graphics/Rect;->left:I

    .line 117
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    iget v7, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    iput v7, v6, Landroid/graphics/Rect;->right:I

    .line 118
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    iget-object v7, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    .line 119
    iget-object v8, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    .line 118
    invoke-virtual {v6, v0, v7, v8, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 120
    sub-int/2addr v5, v4

    .line 121
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    iget v7, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    int-to-float v7, v7

    invoke-virtual {v6, v7, v10}, Landroid/graphics/Canvas;->translate(FF)V

    .line 122
    add-int/lit8 v3, v3, 0x1

    .line 144
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    :goto_1
    iget v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    if-gt v5, v6, :cond_9

    .line 156
    if-lez v5, :cond_4

    .line 157
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v3, v6, :cond_3

    .line 158
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v3, v6, -0x1

    .line 160
    :cond_3
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 161
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_4

    .line 162
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    iput v12, v6, Landroid/graphics/Rect;->left:I

    .line 163
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    iput v5, v6, Landroid/graphics/Rect;->right:I

    .line 164
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    iget-object v7, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    .line 165
    iget-object v8, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    .line 164
    invoke-virtual {v6, v0, v7, v8, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 166
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    iput v5, v6, Landroid/graphics/Rect;->left:I

    .line 167
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    iget v7, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    iput v7, v6, Landroid/graphics/Rect;->right:I

    .line 168
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    iget-object v7, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    .line 169
    iget-object v8, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mPaintGrayscale:Landroid/graphics/Paint;

    .line 168
    invoke-virtual {v6, v0, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 170
    add-int/lit8 v3, v3, 0x1

    .line 171
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    iget v7, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    int-to-float v7, v7

    invoke-virtual {v6, v7, v10}, Landroid/graphics/Canvas;->translate(FF)V

    .line 174
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_4
    :goto_2
    if-lt v3, v1, :cond_b

    .line 185
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v6}, Landroid/graphics/Canvas;->restore()V

    .line 186
    return-void

    .line 94
    .end local v4    # "leftOver":I
    :cond_5
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v3, v6, :cond_6

    .line 95
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v3, v6, -0x1

    .line 97
    :cond_6
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 98
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 99
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    iget-object v7, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mPaintGrayscale:Landroid/graphics/Paint;

    invoke-virtual {v6, v0, v10, v10, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 100
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    iget v7, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    int-to-float v7, v7

    invoke-virtual {v6, v7, v10}, Landroid/graphics/Canvas;->translate(FF)V

    .line 101
    add-int/lit8 v3, v3, 0x1

    .line 102
    iget v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    sub-int/2addr v2, v6

    goto/16 :goto_0

    .line 124
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v4    # "leftOver":I
    :cond_7
    if-lez v2, :cond_2

    if-ge v5, v4, :cond_2

    .line 125
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v3, v6, :cond_8

    .line 126
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v3, v6, -0x1

    .line 128
    :cond_8
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 129
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 130
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v6, v0, v10, v10, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 131
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    iput v12, v6, Landroid/graphics/Rect;->left:I

    .line 132
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    iput v2, v6, Landroid/graphics/Rect;->right:I

    .line 133
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    iget-object v7, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    .line 134
    iget-object v8, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mPaintGrayscale:Landroid/graphics/Paint;

    .line 133
    invoke-virtual {v6, v0, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 135
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    add-int v7, v2, v5

    iput v7, v6, Landroid/graphics/Rect;->left:I

    .line 136
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    iget v7, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    iput v7, v6, Landroid/graphics/Rect;->right:I

    .line 137
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    iget-object v7, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    .line 138
    iget-object v8, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mPaintGrayscale:Landroid/graphics/Paint;

    .line 137
    invoke-virtual {v6, v0, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 139
    const/4 v5, 0x0

    .line 140
    add-int/lit8 v3, v3, 0x1

    .line 141
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    iget v7, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    int-to-float v7, v7

    invoke-virtual {v6, v7, v10}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_1

    .line 145
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_9
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v3, v6, :cond_a

    .line 146
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v3, v6, -0x1

    .line 148
    :cond_a
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 149
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 150
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v6, v0, v10, v10, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 151
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    iget v7, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    int-to-float v7, v7

    invoke-virtual {v6, v7, v10}, Landroid/graphics/Canvas;->translate(FF)V

    .line 152
    add-int/lit8 v3, v3, 0x1

    .line 153
    iget v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    sub-int/2addr v5, v6

    goto/16 :goto_1

    .line 175
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_b
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v3, v6, :cond_c

    .line 176
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v3, v6, -0x1

    .line 178
    :cond_c
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 179
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_4

    .line 180
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    iget-object v7, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mPaintGrayscale:Landroid/graphics/Paint;

    invoke-virtual {v6, v0, v10, v10, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 181
    iget-object v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    iget v7, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    int-to-float v7, v7

    invoke-virtual {v6, v7, v10}, Landroid/graphics/Canvas;->translate(FF)V

    .line 182
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2
.end method

.method private getCenterSquare(Landroid/graphics/Bitmap;)Landroid/graphics/Rect;
    .locals 4
    .param p1, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v3, 0x0

    .line 189
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 190
    .local v1, "res":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 191
    .local v2, "wid":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 193
    .local v0, "hei":I
    if-lt v2, v0, :cond_0

    .line 194
    iput v3, v1, Landroid/graphics/Rect;->top:I

    .line 195
    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 196
    sub-int v3, v2, v0

    div-int/lit8 v3, v3, 0x2

    iput v3, v1, Landroid/graphics/Rect;->left:I

    .line 197
    iget v3, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v0

    iput v3, v1, Landroid/graphics/Rect;->right:I

    .line 204
    :goto_0
    return-object v1

    .line 199
    :cond_0
    iput v3, v1, Landroid/graphics/Rect;->left:I

    .line 200
    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 201
    sub-int v3, v0, v2

    div-int/lit8 v3, v3, 0x2

    iput v3, v1, Landroid/graphics/Rect;->top:I

    .line 202
    iget v3, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v2

    iput v3, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0
.end method

.method private getNthThumbRect(I)Landroid/graphics/Rect;
    .locals 3
    .param p1, "pos"    # I

    .prologue
    .line 208
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 209
    .local v0, "res":Landroid/graphics/Rect;
    iget v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    mul-int/2addr v1, p1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 210
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 211
    iget-object v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapBackground:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailHeight:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 212
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailHeight:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 213
    return-object v0
.end method

.method private getPosFromTime(I)I
    .locals 2
    .param p1, "time"    # I

    .prologue
    .line 328
    iget v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mVideoDuration:I

    if-nez v0, :cond_0

    .line 329
    const/4 v0, 0x0

    .line 331
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mLength:I

    mul-int/2addr v0, p1

    iget v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mVideoDuration:I

    div-int/2addr v0, v1

    goto :goto_0
.end method

.method private getSizeInPixels(I)I
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 217
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method private getThumbCount()I
    .locals 1

    .prologue
    .line 268
    iget v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbCount:I

    return v0
.end method

.method private getTimeFromPos(I)I
    .locals 4
    .param p1, "pos"    # I

    .prologue
    .line 336
    iget v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mVideoDuration:I

    int-to-long v0, v0

    int-to-long v2, p1

    mul-long/2addr v0, v2

    iget v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mLength:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private prepareUI(II)V
    .locals 9
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const v5, 0x7f050003

    const/4 v8, 0x0

    .line 288
    invoke-direct {p0, v5}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getSizeInPixels(I)I

    move-result v4

    sub-int/2addr p1, v4

    .line 289
    invoke-direct {p0, v5}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getSizeInPixels(I)I

    move-result v4

    iput v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mStartPos:I

    .line 290
    iput p1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mEndPos:I

    .line 291
    iget v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mEndPos:I

    iget v5, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mStartPos:I

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mLength:I

    .line 292
    invoke-virtual {p0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getWidth()I

    move-result v4

    iget v5, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSelectBarWidth:I

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSeekBarPos:I

    .line 293
    invoke-virtual {p0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getWidth()I

    move-result v4

    iget v5, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSelectBarWidth:I

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSelectBarPos:I

    .line 294
    iget v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    div-int v4, p1, v4

    iput v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbCount:I

    .line 295
    iget v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    rem-int v4, p1, v4

    if-eqz v4, :cond_0

    .line 296
    iget v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbCount:I

    .line 298
    :cond_0
    const v4, 0x7f050008

    invoke-direct {p0, v4}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getSizeInPixels(I)I

    move-result v0

    .line 299
    .local v0, "bgHeight":I
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 300
    .local v3, "opt":Landroid/graphics/BitmapFactory$Options;
    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v4, v3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 302
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBackgoundRect:Landroid/graphics/Rect;

    .line 303
    iget-object v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBackgoundRect:Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mStartPos:I

    iput v5, v4, Landroid/graphics/Rect;->left:I

    .line 304
    iget-object v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBackgoundRect:Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mEndPos:I

    iput v5, v4, Landroid/graphics/Rect;->right:I

    .line 305
    iget-object v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBackgoundRect:Landroid/graphics/Rect;

    const v5, 0x7f050009

    invoke-direct {p0, v5}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getSizeInPixels(I)I

    move-result v5

    iput v5, v4, Landroid/graphics/Rect;->top:I

    .line 306
    iget-object v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBackgoundRect:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBackgoundRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, v0

    iput v5, v4, Landroid/graphics/Rect;->bottom:I

    .line 308
    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, v0, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapBackground:Landroid/graphics/Bitmap;

    .line 309
    new-instance v4, Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapBackground:Landroid/graphics/Bitmap;

    invoke-direct {v4, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    .line 311
    iget-object v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020004

    invoke-static {v4, v5, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 313
    .local v2, "loading":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_1

    .line 314
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbCount:I

    if-lt v1, v4, :cond_2

    .line 316
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 318
    .end local v1    # "i":I
    :cond_1
    new-instance v4, Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    iget v6, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailHeight:I

    invoke-direct {v4, v8, v8, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mGreyBitmapRect:Landroid/graphics/Rect;

    .line 319
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mIsReady:Z

    .line 320
    return-void

    .line 315
    .restart local v1    # "i":I
    :cond_2
    iget-object v4, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    invoke-direct {p0, v2}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getCenterSquare(Landroid/graphics/Bitmap;)Landroid/graphics/Rect;

    move-result-object v5

    invoke-direct {p0, v1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getNthThumbRect(I)Landroid/graphics/Rect;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v2, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 314
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private resizeAndAddToList(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 227
    if-nez p1, :cond_0

    .line 233
    :goto_0
    return-void

    .line 229
    :cond_0
    iget v2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailWidth:I

    .line 230
    .local v2, "outWidth":I
    iget v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mThumbnailHeight:I

    .line 231
    .local v1, "outHeight":I
    invoke-virtual {p0, p1, v2, v1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getCenterCropBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 232
    .local v0, "finalBMP":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private setThumb(ILandroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "pos"    # I
    .param p2, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBaseCanvas:Landroid/graphics/Canvas;

    invoke-direct {p0, p2}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getCenterSquare(Landroid/graphics/Bitmap;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getNthThumbRect(I)Landroid/graphics/Rect;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 222
    invoke-direct {p0, p2}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->resizeAndAddToList(Landroid/graphics/Bitmap;)V

    .line 223
    invoke-virtual {p0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->invalidate()V

    .line 224
    return-void
.end method

.method private update(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 323
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 324
    invoke-direct {p0, p1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->drawBase(Landroid/graphics/Canvas;)V

    .line 325
    return-void
.end method


# virtual methods
.method public getCenterCropBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 16
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "outWidth"    # I
    .param p3, "outHeight"    # I

    .prologue
    .line 236
    if-nez p1, :cond_0

    .line 237
    const/4 v5, 0x0

    .line 265
    :goto_0
    return-object v5

    .line 239
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    int-to-float v13, v14

    .line 240
    .local v13, "width":F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    int-to-float v6, v14

    .line 241
    .local v6, "height":F
    const/high16 v9, 0x3f800000    # 1.0f

    .line 243
    .local v9, "ratio":F
    move/from16 v0, p2

    int-to-float v14, v0

    div-float v14, v13, v14

    move/from16 v0, p3

    int-to-float v15, v0

    div-float v15, v6, v15

    cmpg-float v14, v14, v15

    if-gez v14, :cond_1

    .line 244
    move/from16 v0, p2

    int-to-float v14, v0

    div-float v9, v13, v14

    .line 248
    :goto_1
    move/from16 v0, p2

    int-to-float v14, v0

    mul-float/2addr v14, v9

    sub-float v14, v13, v14

    float-to-int v14, v14

    div-int/lit8 v7, v14, 0x2

    .line 249
    .local v7, "left":I
    move/from16 v0, p3

    int-to-float v14, v0

    mul-float/2addr v14, v9

    sub-float v14, v6, v14

    float-to-int v14, v14

    div-int/lit8 v12, v14, 0x2

    .line 250
    .local v12, "top":I
    move/from16 v0, p2

    int-to-float v14, v0

    mul-float/2addr v14, v9

    float-to-int v14, v14

    add-int v10, v7, v14

    .line 251
    .local v10, "right":I
    move/from16 v0, p3

    int-to-float v14, v0

    mul-float/2addr v14, v9

    float-to-int v14, v14

    add-int v2, v12, v14

    .line 253
    .local v2, "bottom":I
    new-instance v11, Landroid/graphics/Rect;

    invoke-direct {v11, v7, v12, v10, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 254
    .local v11, "src":Landroid/graphics/Rect;
    new-instance v4, Landroid/graphics/Rect;

    const/4 v14, 0x0

    const/4 v15, 0x0

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-direct {v4, v14, v15, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 256
    .local v4, "dst":Landroid/graphics/Rect;
    sget-object v14, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v0, v1, v14}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 258
    .local v5, "finalBMP":Landroid/graphics/Bitmap;
    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    .line 259
    .local v8, "pt":Landroid/graphics/Paint;
    const/4 v14, 0x1

    invoke-virtual {v8, v14}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 261
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 262
    .local v3, "cnvs":Landroid/graphics/Canvas;
    const/high16 v14, -0x1000000

    invoke-virtual {v3, v14}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 263
    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v11, v4, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 246
    .end local v2    # "bottom":I
    .end local v3    # "cnvs":Landroid/graphics/Canvas;
    .end local v4    # "dst":Landroid/graphics/Rect;
    .end local v5    # "finalBMP":Landroid/graphics/Bitmap;
    .end local v7    # "left":I
    .end local v8    # "pt":Landroid/graphics/Paint;
    .end local v10    # "right":I
    .end local v11    # "src":Landroid/graphics/Rect;
    .end local v12    # "top":I
    :cond_1
    move/from16 v0, p3

    int-to-float v14, v0

    div-float v9, v6, v14

    goto :goto_1
.end method

.method public init(II)Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;
    .locals 1
    .param p1, "selectBarWidth"    # I
    .param p2, "duration"    # I

    .prologue
    .line 66
    const v0, 0x7f050002

    invoke-direct {p0, v0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getSizeInPixels(I)I

    move-result v0

    add-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSelectBarWidth:I

    .line 67
    iput p2, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mVideoDuration:I

    .line 68
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBarManager:Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 74
    invoke-virtual {p0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mIsReady:Z

    if-nez v0, :cond_2

    .line 78
    invoke-virtual {p0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->prepareUI(II)V

    .line 79
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->update(Landroid/graphics/Canvas;)V

    .line 80
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getThumbCount()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 81
    invoke-direct {p0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->drawBitmapWithGrey()V

    goto :goto_0
.end method
