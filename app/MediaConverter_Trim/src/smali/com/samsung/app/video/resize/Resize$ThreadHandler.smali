.class Lcom/samsung/app/video/resize/Resize$ThreadHandler;
.super Landroid/os/Handler;
.source "Resize.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/resize/Resize;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ThreadHandler"
.end annotation


# instance fields
.field mProgress:I

.field final synthetic this$0:Lcom/samsung/app/video/resize/Resize;


# direct methods
.method constructor <init>(Lcom/samsung/app/video/resize/Resize;)V
    .locals 1

    .prologue
    .line 185
    iput-object p1, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 187
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->mProgress:I

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v6, 0x324

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 191
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 194
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mProgView:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$7(Lcom/samsung/app/video/resize/Resize;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 195
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mProgText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$8(Lcom/samsung/app/video/resize/Resize;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "100%"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # invokes: Lcom/samsung/app/video/resize/Resize;->runMediaScanner()V
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$9(Lcom/samsung/app/video/resize/Resize;)V

    .line 197
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$6(Lcom/samsung/app/video/resize/Resize;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/Activity;->removeDialog(I)V

    .line 198
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # invokes: Lcom/samsung/app/video/resize/Resize;->releaseService()V
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$10(Lcom/samsung/app/video/resize/Resize;)V

    .line 199
    invoke-virtual {p0, v4}, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->removeMessages(I)V

    .line 200
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # invokes: Lcom/samsung/app/video/resize/Resize;->setResizeState(I)V
    invoke-static {v0, v5}, Lcom/samsung/app/video/resize/Resize;->access$3(Lcom/samsung/app/video/resize/Resize;I)V

    .line 201
    iput v2, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->mProgress:I

    .line 202
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    invoke-static {v0, v3}, Lcom/samsung/app/video/resize/Resize;->access$11(Lcom/samsung/app/video/resize/Resize;I)V

    goto :goto_0

    .line 206
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$6(Lcom/samsung/app/video/resize/Resize;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/Activity;->removeDialog(I)V

    .line 207
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # invokes: Lcom/samsung/app/video/resize/Resize;->releaseService()V
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$10(Lcom/samsung/app/video/resize/Resize;)V

    .line 208
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$6(Lcom/samsung/app/video/resize/Resize;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f060007

    invoke-static {v0, v1, v3, v2, v2}, Lcom/sec/android/app/video/Utils;->showToast(Landroid/content/Context;IIII)V

    .line 209
    invoke-virtual {p0, v4}, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->removeMessages(I)V

    .line 210
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # invokes: Lcom/samsung/app/video/resize/Resize;->setResizeState(I)V
    invoke-static {v0, v5}, Lcom/samsung/app/video/resize/Resize;->access$3(Lcom/samsung/app/video/resize/Resize;I)V

    .line 211
    iput v2, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->mProgress:I

    .line 212
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    invoke-static {v0, v3}, Lcom/samsung/app/video/resize/Resize;->access$11(Lcom/samsung/app/video/resize/Resize;I)V

    .line 213
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mAdapter:Lcom/samsung/app/video/resize/ResizeInterface$Adapter;
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$5(Lcom/samsung/app/video/resize/Resize;)Lcom/samsung/app/video/resize/ResizeInterface$Adapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/resize/ResizeInterface$Adapter;->onResizeFailed()V

    goto :goto_0

    .line 217
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$12(Lcom/samsung/app/video/resize/Resize;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 218
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$12(Lcom/samsung/app/video/resize/Resize;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 219
    :cond_1
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    iget-object v1, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mAdapter:Lcom/samsung/app/video/resize/ResizeInterface$Adapter;
    invoke-static {v1}, Lcom/samsung/app/video/resize/Resize;->access$5(Lcom/samsung/app/video/resize/Resize;)Lcom/samsung/app/video/resize/ResizeInterface$Adapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/app/video/resize/ResizeInterface$Adapter;->getProgressDialog()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/app/video/resize/Resize;->access$13(Lcom/samsung/app/video/resize/Resize;Landroid/app/AlertDialog;)V

    .line 220
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    iget-object v1, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mFileName:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/app/video/resize/Resize;->access$14(Lcom/samsung/app/video/resize/Resize;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/resize/Resize;->initResizeProgress(Ljava/lang/String;)V

    .line 221
    iput v2, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->mProgress:I

    .line 222
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    invoke-static {v0, v3}, Lcom/samsung/app/video/resize/Resize;->access$11(Lcom/samsung/app/video/resize/Resize;I)V

    goto/16 :goto_0

    .line 226
    :pswitch_3
    invoke-virtual {p0, v4}, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->removeMessages(I)V

    .line 227
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    invoke-virtual {v0}, Lcom/samsung/app/video/resize/Resize;->getResizeProgress()I

    move-result v0

    iput v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->mProgress:I

    .line 228
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mProgDlg:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$15(Lcom/samsung/app/video/resize/Resize;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->mProgress:I

    const/16 v1, 0x62

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # invokes: Lcom/samsung/app/video/resize/Resize;->getResizeState()I
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$16(Lcom/samsung/app/video/resize/Resize;)I

    move-result v0

    if-eq v0, v5, :cond_0

    .line 229
    invoke-virtual {p0, v4}, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->sendEmptyMessage(I)Z

    .line 230
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->lastProgress:I
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$17(Lcom/samsung/app/video/resize/Resize;)I

    move-result v0

    iget v1, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->mProgress:I

    if-eq v0, v1, :cond_0

    .line 232
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mProgView:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$7(Lcom/samsung/app/video/resize/Resize;)Landroid/widget/ProgressBar;

    move-result-object v0

    iget v1, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->mProgress:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 233
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mProgText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$8(Lcom/samsung/app/video/resize/Resize;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->mProgress:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->this$0:Lcom/samsung/app/video/resize/Resize;

    iget v1, p0, Lcom/samsung/app/video/resize/Resize$ThreadHandler;->mProgress:I

    invoke-static {v0, v1}, Lcom/samsung/app/video/resize/Resize;->access$11(Lcom/samsung/app/video/resize/Resize;I)V

    goto/16 :goto_0

    .line 191
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
