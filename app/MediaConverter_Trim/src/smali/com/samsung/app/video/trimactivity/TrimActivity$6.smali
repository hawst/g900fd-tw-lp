.class Lcom/samsung/app/video/trimactivity/TrimActivity$6;
.super Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;
.source "TrimActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/trimactivity/TrimActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;


# direct methods
.method constructor <init>(Lcom/samsung/app/video/trimactivity/TrimActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$6;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    .line 638
    invoke-direct {p0}, Lcom/samsung/app/video/videoPreview/VideoFilePreview$Adapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusLost()V
    .locals 2

    .prologue
    .line 666
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$6;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/app/video/trimactivity/TrimActivity;->onPreviewPause(Z)V
    invoke-static {v0, v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$4(Lcom/samsung/app/video/trimactivity/TrimActivity;Z)V

    .line 667
    return-void
.end method

.method public onCompleted()V
    .locals 2

    .prologue
    .line 654
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$6;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$3(Lcom/samsung/app/video/trimactivity/TrimActivity;I)V

    .line 655
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$6;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # invokes: Lcom/samsung/app/video/trimactivity/TrimActivity;->initPausedScreen()V
    invoke-static {v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$15(Lcom/samsung/app/video/trimactivity/TrimActivity;)V

    .line 656
    return-void
.end method

.method public onError()V
    .locals 2

    .prologue
    .line 642
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$6;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$3(Lcom/samsung/app/video/trimactivity/TrimActivity;I)V

    .line 643
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$6;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/app/video/trimactivity/TrimActivity;->onPreviewPause(Z)V
    invoke-static {v0, v1}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$4(Lcom/samsung/app/video/trimactivity/TrimActivity;Z)V

    .line 644
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$6;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # invokes: Lcom/samsung/app/video/trimactivity/TrimActivity;->showUnsupportedFormatDialog()V
    invoke-static {v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$13(Lcom/samsung/app/video/trimactivity/TrimActivity;)V

    .line 645
    return-void
.end method

.method public onPlayed()V
    .locals 1

    .prologue
    .line 649
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$6;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # invokes: Lcom/samsung/app/video/trimactivity/TrimActivity;->startSeekbarUpdate()V
    invoke-static {v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$14(Lcom/samsung/app/video/trimactivity/TrimActivity;)V

    .line 650
    return-void
.end method

.method public onStopped()V
    .locals 2

    .prologue
    .line 660
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$6;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mPlayBtn:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$16(Lcom/samsung/app/video/trimactivity/TrimActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f020001

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 661
    iget-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$6;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # invokes: Lcom/samsung/app/video/trimactivity/TrimActivity;->initPausedScreen()V
    invoke-static {v0}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$15(Lcom/samsung/app/video/trimactivity/TrimActivity;)V

    .line 662
    return-void
.end method
