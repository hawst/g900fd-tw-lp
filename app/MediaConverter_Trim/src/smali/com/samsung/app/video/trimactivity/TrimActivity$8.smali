.class Lcom/samsung/app/video/trimactivity/TrimActivity$8;
.super Ljava/lang/Object;
.source "TrimActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/app/video/trimactivity/TrimActivity;->init(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mPos:I

.field private mRetriever:Landroid/media/MediaMetadataRetriever;

.field final synthetic this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

.field private final synthetic val$trimBar:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;


# direct methods
.method constructor <init>(Lcom/samsung/app/video/trimactivity/TrimActivity;Lcom/samsung/app/video/videoPreview/PreviewTimelineView;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    iput-object p2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->val$trimBar:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    .line 502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 503
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->mPos:I

    .line 504
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->mRetriever:Landroid/media/MediaMetadataRetriever;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 507
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mTrimBarMgr:Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;
    invoke-static {v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$19(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;->isThumbViewReady()Z

    move-result v3

    if-nez v3, :cond_0

    .line 508
    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->mPos:I

    .line 509
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->val$trimBar:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    const-wide/16 v4, 0x64

    invoke-virtual {v3, p0, v4, v5}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 526
    :goto_0
    return-void

    .line 512
    :cond_0
    iget v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->mPos:I

    if-nez v3, :cond_1

    .line 513
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mVideoPath:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$20(Lcom/samsung/app/video/trimactivity/TrimActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/video/Utils;->getRetrieverForSource(Ljava/lang/String;)Landroid/media/MediaMetadataRetriever;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->mRetriever:Landroid/media/MediaMetadataRetriever;

    .line 514
    :cond_1
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mTrimBarMgr:Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;
    invoke-static {v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$19(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;->getThumbnailCount()I

    move-result v1

    .line 515
    .local v1, "count":I
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mDuration:J
    invoke-static {v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$8(Lcom/samsung/app/video/trimactivity/TrimActivity;)J

    move-result-wide v3

    add-int/lit8 v5, v1, -0x1

    int-to-long v5, v5

    div-long/2addr v3, v5

    long-to-int v2, v3

    .line 516
    .local v2, "gap":I
    iget v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->mPos:I

    if-ge v3, v1, :cond_3

    .line 517
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->mRetriever:Landroid/media/MediaMetadataRetriever;

    iget v4, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->mPos:I

    mul-int/2addr v4, v2

    div-int/lit8 v5, v2, 0x2

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-static {v3, v4}, Lcom/sec/android/app/video/Utils;->getVideoFrame(Landroid/media/MediaMetadataRetriever;F)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 518
    .local v0, "bmp":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 519
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->mTrimBarMgr:Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;
    invoke-static {v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$19(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;

    move-result-object v3

    iget v4, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->mPos:I

    invoke-interface {v3, v4, v0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;->setThumbAt(ILandroid/graphics/Bitmap;)V

    .line 521
    :cond_2
    iget v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->mPos:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->mPos:I

    .line 522
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->val$trimBar:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    invoke-virtual {v3, p0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 525
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :cond_3
    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$8;->mRetriever:Landroid/media/MediaMetadataRetriever;

    invoke-static {v3}, Lcom/sec/android/app/video/Utils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    goto :goto_0
.end method
