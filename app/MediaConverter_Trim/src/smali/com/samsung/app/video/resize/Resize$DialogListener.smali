.class Lcom/samsung/app/video/resize/Resize$DialogListener;
.super Lcom/sec/android/app/video/DialogUtils$DialogListener;
.source "Resize.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/resize/Resize;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DialogListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/app/video/resize/Resize;


# direct methods
.method constructor <init>(Lcom/samsung/app/video/resize/Resize;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/samsung/app/video/resize/Resize$DialogListener;->this$0:Lcom/samsung/app/video/resize/Resize;

    invoke-direct {p0}, Lcom/sec/android/app/video/DialogUtils$DialogListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onResizeProgressCancelled()V
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$DialogListener;->this$0:Lcom/samsung/app/video/resize/Resize;

    # getter for: Lcom/samsung/app/video/resize/Resize;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$6(Lcom/samsung/app/video/resize/Resize;)Landroid/app/Activity;

    move-result-object v0

    const/16 v1, 0x324

    invoke-virtual {v0, v1}, Landroid/app/Activity;->removeDialog(I)V

    .line 261
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$DialogListener;->this$0:Lcom/samsung/app/video/resize/Resize;

    invoke-virtual {v0}, Lcom/samsung/app/video/resize/Resize;->stopResize()V

    .line 262
    return-void
.end method

.method public onResizeProgressCreated(Landroid/widget/ProgressBar;Landroid/widget/TextView;)V
    .locals 1
    .param p1, "prog"    # Landroid/widget/ProgressBar;
    .param p2, "progText"    # Landroid/widget/TextView;

    .prologue
    .line 254
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$DialogListener;->this$0:Lcom/samsung/app/video/resize/Resize;

    invoke-static {v0, p1}, Lcom/samsung/app/video/resize/Resize;->access$20(Lcom/samsung/app/video/resize/Resize;Landroid/widget/ProgressBar;)V

    .line 255
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$DialogListener;->this$0:Lcom/samsung/app/video/resize/Resize;

    invoke-static {v0, p2}, Lcom/samsung/app/video/resize/Resize;->access$21(Lcom/samsung/app/video/resize/Resize;Landroid/widget/TextView;)V

    .line 256
    return-void
.end method

.method public startResize(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 248
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$DialogListener;->this$0:Lcom/samsung/app/video/resize/Resize;

    invoke-static {v0, p1}, Lcom/samsung/app/video/resize/Resize;->access$18(Lcom/samsung/app/video/resize/Resize;Ljava/lang/String;)V

    .line 249
    iget-object v0, p0, Lcom/samsung/app/video/resize/Resize$DialogListener;->this$0:Lcom/samsung/app/video/resize/Resize;

    # invokes: Lcom/samsung/app/video/resize/Resize;->initService()V
    invoke-static {v0}, Lcom/samsung/app/video/resize/Resize;->access$19(Lcom/samsung/app/video/resize/Resize;)V

    .line 250
    return-void
.end method
