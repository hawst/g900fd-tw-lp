.class Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;
.super Ljava/lang/Object;
.source "PreviewTimelineView.java"

# interfaces
.implements Lcom/samsung/app/video/videoPreview/PreviewTimelineView$TrimBarManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/videoPreview/PreviewTimelineView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Manager"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;


# direct methods
.method private constructor <init>(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;)V
    .locals 0

    .prologue
    .line 348
    iput-object p1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;->this$0:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;)V
    .locals 0

    .prologue
    .line 348
    invoke-direct {p0, p1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;-><init>(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;)V

    return-void
.end method


# virtual methods
.method public getSeekBarPosition()I
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;->this$0:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    iget-object v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;->this$0:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    # getter for: Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mSeekBarPos:I
    invoke-static {v1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->access$7(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;)I

    move-result v1

    # invokes: Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getTimeFromPos(I)I
    invoke-static {v0, v1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->access$8(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;I)I

    move-result v0

    return v0
.end method

.method public getThumbnailCount()I
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;->this$0:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    # invokes: Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getThumbCount()I
    invoke-static {v0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->access$4(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;)I

    move-result v0

    return v0
.end method

.method public isThumbViewReady()Z
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;->this$0:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    # getter for: Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->mIsReady:Z
    invoke-static {v0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->access$3(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;)Z

    move-result v0

    return v0
.end method

.method public setSeekBarPosition(I)V
    .locals 2
    .param p1, "time"    # I

    .prologue
    .line 375
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;->this$0:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    iget-object v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;->this$0:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    # invokes: Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getPosFromTime(I)I
    invoke-static {v1, p1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->access$5(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->access$6(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;I)V

    .line 376
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;->this$0:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    invoke-virtual {v0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->invalidate()V

    .line 377
    return-void
.end method

.method public setSelectBarPosition(I)V
    .locals 3
    .param p1, "dragXpos"    # I

    .prologue
    .line 351
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;->this$0:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    iget-object v1, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;->this$0:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    const v2, 0x7f050002

    # invokes: Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->getSizeInPixels(I)I
    invoke-static {v1, v2}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->access$0(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;I)I

    move-result v1

    sub-int v1, p1, v1

    invoke-static {v0, v1}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->access$1(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;I)V

    .line 352
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;->this$0:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    invoke-virtual {v0}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->invalidate()V

    .line 353
    return-void
.end method

.method public setThumbAt(ILandroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "pos"    # I
    .param p2, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 357
    if-nez p2, :cond_0

    .line 361
    :goto_0
    return-void

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/videoPreview/PreviewTimelineView$Manager;->this$0:Lcom/samsung/app/video/videoPreview/PreviewTimelineView;

    # invokes: Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->setThumb(ILandroid/graphics/Bitmap;)V
    invoke-static {v0, p1, p2}, Lcom/samsung/app/video/videoPreview/PreviewTimelineView;->access$2(Lcom/samsung/app/video/videoPreview/PreviewTimelineView;ILandroid/graphics/Bitmap;)V

    goto :goto_0
.end method
