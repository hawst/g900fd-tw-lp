.class public Lcom/samsung/app/video/videoPreview/ScaleableVideoView;
.super Landroid/widget/VideoView;
.source "ScaleableVideoView.java"


# instance fields
.field private mRespectLayoutSize:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v3, 0x0

    .line 25
    iget-boolean v2, p0, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;->mRespectLayoutSize:Z

    if-eqz v2, :cond_0

    .line 26
    invoke-static {v3, p1}, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;->getDefaultSize(II)I

    move-result v1

    .line 27
    .local v1, "width":I
    invoke-static {v3, p2}, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;->getDefaultSize(II)I

    move-result v0

    .line 29
    .local v0, "height":I
    invoke-virtual {p0, v1, v0}, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;->setMeasuredDimension(II)V

    .line 33
    .end local v0    # "height":I
    .end local v1    # "width":I
    :goto_0
    return-void

    .line 32
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/VideoView;->onMeasure(II)V

    goto :goto_0
.end method

.method public setFixedSizeFromLayout(Z)V
    .locals 0
    .param p1, "respectLayoutSize"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/samsung/app/video/videoPreview/ScaleableVideoView;->mRespectLayoutSize:Z

    .line 37
    return-void
.end method
