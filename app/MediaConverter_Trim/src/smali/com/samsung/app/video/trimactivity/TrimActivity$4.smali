.class Lcom/samsung/app/video/trimactivity/TrimActivity$4;
.super Landroid/os/Handler;
.source "TrimActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/trimactivity/TrimActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;


# direct methods
.method constructor <init>(Lcom/samsung/app/video/trimactivity/TrimActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$4;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    .line 145
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 147
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 160
    :goto_0
    return-void

    .line 149
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$4;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    invoke-virtual {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->finish()V

    goto :goto_0

    .line 152
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$4;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->resizeElem:Lcom/samsung/app/share/via/external/ShareviaObj;
    invoke-static {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$2(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/share/via/external/ShareviaObj;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/app/share/via/external/ShareviaObj;->getResizeOutputFilename()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$4;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    # getter for: Lcom/samsung/app/video/trimactivity/TrimActivity;->resizeElem:Lcom/samsung/app/share/via/external/ShareviaObj;
    invoke-static {v3}, Lcom/samsung/app/video/trimactivity/TrimActivity;->access$2(Lcom/samsung/app/video/trimactivity/TrimActivity;)Lcom/samsung/app/share/via/external/ShareviaObj;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/app/share/via/external/ShareviaObj;->getResizeOutputFilename()Ljava/lang/String;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 153
    .local v1, "str":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x4

    invoke-virtual {v1, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 154
    .local v0, "name":Ljava/lang/String;
    const v2, 0x7f06000d

    invoke-static {v2}, Lcom/sec/android/app/video/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v5

    const-string v4, "Videos"

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 155
    iget-object v2, p0, Lcom/samsung/app/video/trimactivity/TrimActivity$4;->this$0:Lcom/samsung/app/video/trimactivity/TrimActivity;

    invoke-virtual {v2}, Lcom/samsung/app/video/trimactivity/TrimActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 147
    :pswitch_data_0
    .packed-switch 0xfe
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
