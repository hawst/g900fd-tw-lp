.class public Lcom/samsung/app/share/via/external/NativeAccess;
.super Ljava/lang/Object;
.source "NativeAccess.java"


# static fields
.field private static nativeAccess:Lcom/samsung/app/share/via/external/NativeAccess;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/app/share/via/external/NativeAccess;->nativeAccess:Lcom/samsung/app/share/via/external/NativeAccess;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const-string v0, "mediaConverter"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 17
    const-string v0, "mediaConverter_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static getInstance()Lcom/samsung/app/share/via/external/NativeAccess;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/samsung/app/share/via/external/NativeAccess;->nativeAccess:Lcom/samsung/app/share/via/external/NativeAccess;

    if-nez v0, :cond_0

    .line 10
    new-instance v0, Lcom/samsung/app/share/via/external/NativeAccess;

    invoke-direct {v0}, Lcom/samsung/app/share/via/external/NativeAccess;-><init>()V

    sput-object v0, Lcom/samsung/app/share/via/external/NativeAccess;->nativeAccess:Lcom/samsung/app/share/via/external/NativeAccess;

    .line 12
    :cond_0
    sget-object v0, Lcom/samsung/app/share/via/external/NativeAccess;->nativeAccess:Lcom/samsung/app/share/via/external/NativeAccess;

    return-object v0
.end method


# virtual methods
.method public native getInputParamerterAnalysis(Ljava/lang/String;III)I
.end method

.method public native getInputParamerterAnalysis(Ljava/lang/String;IIII)I
.end method

.method public native getOutputFileSize(Lcom/samsung/app/share/via/external/ShareviaObj;)I
.end method

.method public native startProcessing(Lcom/samsung/app/share/via/external/ShareviaObj;)I
.end method

.method public native stopProcessing()I
.end method
