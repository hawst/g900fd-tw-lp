.class public Lcom/samsung/app/share/via/external/ShareviaObj;
.super Ljava/lang/Object;
.source "ShareviaObj.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/app/share/via/external/ShareviaObj$TranscodeMode;,
        Lcom/samsung/app/share/via/external/ShareviaObj$codecType;,
        Lcom/samsung/app/share/via/external/ShareviaObj$videoResType;
    }
.end annotation


# instance fields
.field OutFileResolution:I

.field assetmngr:Landroid/content/res/AssetManager;

.field audioCodecType:I

.field audioLength:I

.field audioOffset:I

.field endTime:I

.field iconFileName:Ljava/lang/String;

.field inputFileName:Ljava/lang/String;

.field maxOutFileDuration:I

.field maxOutFileSize:I

.field private outputDuration:J

.field outputFileName:Ljava/lang/String;

.field outputHeight:I

.field private outputSize:J

.field outputWidth:I

.field startTime:I

.field transcodeMode:I

.field version:I

.field videoCodecType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->version:I

    .line 54
    iput v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->version:I

    .line 55
    return-void
.end method


# virtual methods
.method public getOutputFileSize()J
    .locals 2

    .prologue
    .line 186
    iget-wide v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->outputSize:J

    return-wide v0
.end method

.method public getResizeAudioCodec()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->audioCodecType:I

    return v0
.end method

.method public getResizeAudioLength()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->audioLength:I

    return v0
.end method

.method public getResizeAudioOffset()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->audioOffset:I

    return v0
.end method

.method public getResizeDuration()J
    .locals 2

    .prologue
    .line 178
    iget-wide v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->outputDuration:J

    return-wide v0
.end method

.method public getResizeEndTime()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->endTime:I

    return v0
.end method

.method public getResizeHeight()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->outputHeight:I

    return v0
.end method

.method public getResizeInputFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->inputFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getResizeOutputFileResolution()I
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->OutFileResolution:I

    return v0
.end method

.method public getResizeOutputFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->outputFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getResizeStartTime()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->startTime:I

    return v0
.end method

.method public getResizeTranscodeMode()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->transcodeMode:I

    return v0
.end method

.method public getResizeVersion()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->version:I

    return v0
.end method

.method public getResizeVideoCodec()I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->videoCodecType:I

    return v0
.end method

.method public getResizeWidth()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->outputWidth:I

    return v0
.end method

.method public getResizemaxSize()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->maxOutFileSize:I

    return v0
.end method

.method public setOutputFileSize(J)V
    .locals 0
    .param p1, "output"    # J

    .prologue
    .line 190
    iput-wide p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->outputSize:J

    .line 191
    return-void
.end method

.method public setResizeAssetmngr(Landroid/content/res/AssetManager;)V
    .locals 0
    .param p1, "assetmngr"    # Landroid/content/res/AssetManager;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->assetmngr:Landroid/content/res/AssetManager;

    .line 79
    return-void
.end method

.method public setResizeAudioCodec(I)V
    .locals 0
    .param p1, "audiocodec"    # I

    .prologue
    .line 150
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->audioCodecType:I

    .line 151
    return-void
.end method

.method public setResizeAudioLength(I)V
    .locals 0
    .param p1, "audioLength"    # I

    .prologue
    .line 94
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->audioLength:I

    .line 95
    return-void
.end method

.method public setResizeAudioOffset(I)V
    .locals 0
    .param p1, "audioOffset"    # I

    .prologue
    .line 86
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->audioOffset:I

    .line 87
    return-void
.end method

.method public setResizeDuration(J)V
    .locals 0
    .param p1, "maxoutput"    # J

    .prologue
    .line 182
    iput-wide p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->outputDuration:J

    .line 183
    return-void
.end method

.method public setResizeEndTime(I)V
    .locals 0
    .param p1, "endtime"    # I

    .prologue
    .line 122
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->endTime:I

    .line 123
    return-void
.end method

.method public setResizeHeight(I)V
    .locals 0
    .param p1, "mHeight"    # I

    .prologue
    .line 74
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->outputHeight:I

    .line 75
    return-void
.end method

.method public setResizeIconFileName(Ljava/lang/String;)V
    .locals 0
    .param p1, "outfilename"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->iconFileName:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public setResizeInputFilename(Ljava/lang/String;)V
    .locals 0
    .param p1, "inputfilename"    # Ljava/lang/String;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->inputFileName:Ljava/lang/String;

    .line 159
    return-void
.end method

.method public setResizeOutputFileResolution(I)V
    .locals 0
    .param p1, "OutfileResolution"    # I

    .prologue
    .line 174
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->OutFileResolution:I

    .line 175
    return-void
.end method

.method public setResizeOutputFilename(Ljava/lang/String;)V
    .locals 0
    .param p1, "outfilename"    # Ljava/lang/String;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->outputFileName:Ljava/lang/String;

    .line 167
    return-void
.end method

.method public setResizeStartTime(I)V
    .locals 0
    .param p1, "starttime"    # I

    .prologue
    .line 114
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->startTime:I

    .line 115
    return-void
.end method

.method public setResizeTranscodeMode(I)V
    .locals 0
    .param p1, "transcodeMode"    # I

    .prologue
    .line 102
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->transcodeMode:I

    .line 103
    return-void
.end method

.method public setResizeVideoCodec(I)V
    .locals 0
    .param p1, "vtVideoCodec"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->videoCodecType:I

    .line 143
    return-void
.end method

.method public setResizeWidth(I)V
    .locals 0
    .param p1, "mWidth"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->outputWidth:I

    .line 67
    return-void
.end method

.method public setResizemaxDuration(I)V
    .locals 0
    .param p1, "maxduration"    # I

    .prologue
    .line 126
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->maxOutFileDuration:I

    .line 127
    return-void
.end method

.method public setResizemaxSize(I)V
    .locals 0
    .param p1, "maxsize"    # I

    .prologue
    .line 134
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->maxOutFileSize:I

    .line 135
    return-void
.end method
