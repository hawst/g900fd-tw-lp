.class interface abstract Landroid/support/v4/view/accessibility/AccessibilityRecordCompat$AccessibilityRecordImpl;
.super Ljava/lang/Object;
.source "AccessibilityRecordCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "AccessibilityRecordImpl"
.end annotation


# virtual methods
.method public abstract getCurrentItemIndex(Ljava/lang/Object;)I
.end method

.method public abstract getFromIndex(Ljava/lang/Object;)I
.end method

.method public abstract getMaxScrollY(Ljava/lang/Object;)I
.end method

.method public abstract getScrollX(Ljava/lang/Object;)I
.end method

.method public abstract getScrollY(Ljava/lang/Object;)I
.end method

.method public abstract getSource(Ljava/lang/Object;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
.end method

.method public abstract getText(Ljava/lang/Object;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getToIndex(Ljava/lang/Object;)I
.end method

.method public abstract isScrollable(Ljava/lang/Object;)Z
.end method

.method public abstract obtain(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public abstract recycle(Ljava/lang/Object;)V
.end method

.method public abstract setSource(Ljava/lang/Object;Landroid/view/View;I)V
.end method
