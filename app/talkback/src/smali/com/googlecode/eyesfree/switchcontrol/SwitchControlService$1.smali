.class Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService$1;
.super Ljava/lang/Object;
.source "SwitchControlService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;


# direct methods
.method constructor <init>(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 41
    iget-object v4, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    # invokes: Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->getMillisUntilSafeToProcessKeyEvents()J
    invoke-static {v4}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->access$000(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;)J

    move-result-wide v2

    .line 43
    .local v2, "timeToWait":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 45
    iget-object v4, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    # getter for: Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->access$200(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    # getter for: Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mRunnableToProcessHeldOffKeyEvents:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->access$100(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 46
    iget-object v4, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    # getter for: Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->access$200(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    # getter for: Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mRunnableToProcessHeldOffKeyEvents:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->access$100(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 59
    :goto_0
    return-void

    .line 54
    :cond_0
    iget-object v4, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    # getter for: Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mListOfHeldOffKeyEvents:Ljava/util/List;
    invoke-static {v4}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->access$300(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    .line 55
    .local v0, "event":Landroid/view/KeyEvent;
    iget-object v4, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    # invokes: Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->processGuarenteedSafeKeyEvent(Landroid/view/KeyEvent;)V
    invoke-static {v4, v0}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->access$400(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;Landroid/view/KeyEvent;)V

    goto :goto_1

    .line 58
    .end local v0    # "event":Landroid/view/KeyEvent;
    :cond_1
    iget-object v4, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    # getter for: Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mListOfHeldOffKeyEvents:Ljava/util/List;
    invoke-static {v4}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->access$300(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    goto :goto_0
.end method
