.class Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity$1;
.super Ljava/lang/Object;
.source "SwitchControlPreferenceActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity;


# direct methods
.method constructor <init>(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 10
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 52
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 53
    .local v1, "newValueString":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v4, v5

    .line 62
    :goto_0
    return v4

    .line 57
    :cond_0
    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 58
    .local v2, "value":D
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double v6, v2, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide v8, 0x3f847ae147ae147bL    # 0.01

    cmpg-double v6, v6, v8

    if-gez v6, :cond_1

    move v0, v4

    .line 60
    .local v0, "count":I
    :goto_1
    iget-object v6, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity;

    invoke-virtual {v6}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e0002

    new-array v8, v4, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-virtual {v6, v7, v0, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 58
    .end local v0    # "count":I
    :cond_1
    const/4 v0, 0x2

    goto :goto_1
.end method
