.class Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference$1;
.super Ljava/lang/Object;
.source "KeyComboPreference.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->onBindDialogView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;


# direct methods
.method constructor <init>(Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const-wide/16 v4, -0x1

    .line 120
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;

    # getter for: Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->access$000(Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 121
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iget-object v2, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 122
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;

    iget-object v2, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;

    # invokes: Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->describeExtendedKeyCode(J)Ljava/lang/String;
    invoke-static {v2, v4, v5}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->access$100(Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;

    # setter for: Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mKeyCombo:J
    invoke-static {v1, v4, v5}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->access$202(Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;J)J

    .line 124
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;

    # getter for: Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mCurrentKeyTextView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->access$300(Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;

    iget-object v3, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;

    # getter for: Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mKeyCombo:J
    invoke-static {v3}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->access$200(Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;)J

    move-result-wide v4

    # invokes: Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->describeExtendedKeyCode(J)Ljava/lang/String;
    invoke-static {v2, v4, v5}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->access$100(Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    return-void
.end method
