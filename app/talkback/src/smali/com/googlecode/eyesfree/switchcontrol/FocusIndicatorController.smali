.class public Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;
.super Ljava/lang/Object;
.source "FocusIndicatorController.java"


# instance fields
.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private final mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

.field private final mRelativeLayout:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Lcom/googlecode/eyesfree/widget/SimpleOverlay;)V
    .locals 3
    .param p1, "overlay"    # Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v1, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController$1;

    invoke-direct {v1, p0}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController$1;-><init>(Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;)V

    iput-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 44
    iput-object p1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    .line 45
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    const v2, 0x7f030009

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->setContentView(I)V

    .line 46
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    const v2, 0x7f0d0087

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mRelativeLayout:Landroid/widget/RelativeLayout;

    .line 47
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 48
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 49
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 50
    invoke-direct {p0}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->configureOverlay()V

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;)V
    .locals 0
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->configureOverlay()V

    return-void
.end method

.method static synthetic access$100(Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;)Lcom/googlecode/eyesfree/widget/SimpleOverlay;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    return-object v0
.end method

.method static synthetic access$200(Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mRelativeLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private configureOverlay()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 111
    iget-object v5, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    invoke-virtual {v5}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->getParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 112
    .local v1, "params":Landroid/view/WindowManager$LayoutParams;
    const/16 v5, 0x7d6

    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 113
    iget v5, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v5, v5, 0x10

    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 114
    iget v5, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v5, v5, 0x200

    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 117
    iget-object v5, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    invoke-virtual {v5}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "window"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    .line 119
    .local v4, "wm":Landroid/view/WindowManager;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 120
    .local v2, "size":Landroid/graphics/Point;
    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 121
    iget-object v5, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    invoke-virtual {v5}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/ScreenDimensionUtils;->getNavBarHeight(Landroid/content/Context;)I

    move-result v0

    .line 122
    .local v0, "navBarHeight":I
    iget-object v5, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    invoke-virtual {v5}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/ScreenDimensionUtils;->getStatusBarHeight(Landroid/content/Context;)I

    move-result v3

    .line 123
    .local v3, "statusBarHeight":I
    iget v5, v2, Landroid/graphics/Point;->y:I

    add-int/2addr v5, v3

    add-int/2addr v5, v0

    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 124
    iget v5, v2, Landroid/graphics/Point;->x:I

    add-int/2addr v5, v3

    add-int/2addr v5, v0

    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 125
    iput v7, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 126
    iput v7, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 128
    iget-object v5, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    invoke-virtual {v5, v1}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->setParams(Landroid/view/WindowManager$LayoutParams;)V

    .line 129
    return-void
.end method


# virtual methods
.method public clearFocus()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 98
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->hide()V

    .line 99
    return-void
.end method

.method public indicateFocusOnNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 1
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 59
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 60
    .local v0, "nodeBounds":Landroid/graphics/Rect;
    invoke-virtual {p1, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getBoundsInScreen(Landroid/graphics/Rect;)V

    .line 61
    invoke-virtual {p0, v0}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->indicateFocusOnRect(Landroid/graphics/Rect;)V

    .line 62
    return-void
.end method

.method public indicateFocusOnRect(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "boundingRect"    # Landroid/graphics/Rect;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->show()V

    .line 71
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController$2;

    invoke-direct {v1, p0, p1}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController$2;-><init>(Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;Landroid/graphics/Rect;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 91
    return-void
.end method

.method public shutdown()V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 106
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->hide()V

    .line 107
    return-void
.end method
