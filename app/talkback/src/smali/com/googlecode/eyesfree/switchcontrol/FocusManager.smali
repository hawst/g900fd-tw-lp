.class public Lcom/googlecode/eyesfree/switchcontrol/FocusManager;
.super Ljava/lang/Object;
.source "FocusManager.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

.field private final mCurrentRow:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentWindowIndex:I

.field private mDownClickKeyPressed:Z

.field private mFirstNodeInRow:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private final mFocusIndicatorController:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

.field private mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private final mRowCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;>;"
        }
    .end annotation
.end field

.field private mRowHighlighted:Z

.field private mWindowList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityWindowInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "focusIndicatorController"    # Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;
    .param p3, "contextMenuController"    # Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentRow:Ljava/util/Set;

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mRowCache:Ljava/util/Map;

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 68
    iput-object p1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mContext:Landroid/content/Context;

    .line 69
    iput-object p2, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mFocusIndicatorController:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    .line 70
    iput-object p3, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    .line 71
    return-void
.end method

.method private clearRowColumn()V
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mRowCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    .line 287
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mRowHighlighted:Z

    .line 288
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentRow:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 289
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mRowCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 290
    return-void
.end method

.method private crossWindowSearch(ILandroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 4
    .param p1, "focusDirection"    # I
    .param p2, "startingNode"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v2, 0x0

    .line 468
    invoke-static {p2, p1}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 470
    .local v0, "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 471
    invoke-direct {p0}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->clearRowColumn()V

    .line 472
    const/4 v1, 0x1

    if-ne p1, v1, :cond_3

    .line 473
    iget v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentWindowIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentWindowIndex:I

    .line 474
    iget v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentWindowIndex:I

    iget-object v3, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mWindowList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_2

    .line 475
    const/4 v1, 0x0

    iput v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentWindowIndex:I

    move-object v0, v2

    .line 491
    .end local v0    # "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_1
    :goto_1
    return-object v0

    .line 479
    .restart local v0    # "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_2
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mWindowList:Ljava/util/List;

    iget v3, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentWindowIndex:I

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-static {v1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->getRootForWindow(Landroid/view/accessibility/AccessibilityWindowInfo;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0

    .line 480
    :cond_3
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 481
    iget v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentWindowIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentWindowIndex:I

    .line 482
    iget v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentWindowIndex:I

    if-gez v1, :cond_4

    .line 483
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mWindowList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentWindowIndex:I

    move-object v0, v2

    .line 484
    goto :goto_1

    .line 487
    :cond_4
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mWindowList:Ljava/util/List;

    iget v3, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentWindowIndex:I

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-direct {p0, v1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->getLastNodeInWindow(Landroid/view/accessibility/AccessibilityWindowInfo;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0
.end method

.method private static getBoundingRectForRow(Ljava/util/List;)Landroid/graphics/Rect;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;)",
            "Landroid/graphics/Rect;"
        }
    .end annotation

    .prologue
    .line 220
    .local p0, "currentRow":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;>;"
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 221
    .local v0, "boundingRect":Landroid/graphics/Rect;
    const/4 v4, 0x0

    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getBoundsInScreen(Landroid/graphics/Rect;)V

    .line 222
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 223
    .local v1, "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 224
    .local v3, "nodeBounds":Landroid/graphics/Rect;
    invoke-virtual {v1, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getBoundsInScreen(Landroid/graphics/Rect;)V

    .line 225
    iget v4, v0, Landroid/graphics/Rect;->left:I

    iget v5, v3, Landroid/graphics/Rect;->left:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, v0, Landroid/graphics/Rect;->left:I

    .line 226
    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    iget v5, v3, Landroid/graphics/Rect;->bottom:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, v0, Landroid/graphics/Rect;->bottom:I

    .line 227
    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v5, v3, Landroid/graphics/Rect;->right:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, v0, Landroid/graphics/Rect;->right:I

    .line 228
    iget v4, v0, Landroid/graphics/Rect;->top:I

    iget v5, v3, Landroid/graphics/Rect;->top:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, v0, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 231
    .end local v1    # "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .end local v3    # "nodeBounds":Landroid/graphics/Rect;
    :cond_0
    return-object v0
.end method

.method private getCurrentRow()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;"
        }
    .end annotation

    .prologue
    .line 242
    iget-object v9, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mRowCache:Ljava/util/Map;

    iget-object v10, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-interface {v9, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 243
    iget-object v9, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mRowCache:Ljava/util/Map;

    iget-object v10, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    .line 279
    :goto_0
    return-object v9

    .line 247
    :cond_0
    new-instance v8, Landroid/util/SparseArray;

    invoke-direct {v8}, Landroid/util/SparseArray;-><init>()V

    .line 249
    .local v8, "rows":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;>;>;"
    iget-object v9, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mWindowList:Ljava/util/List;

    iget v10, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentWindowIndex:I

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-static {v9}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->getRootForWindow(Landroid/view/accessibility/AccessibilityWindowInfo;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 251
    .local v0, "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :goto_1
    if-eqz v0, :cond_3

    .line 252
    const/4 v9, 0x1

    invoke-static {v0, v9}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v6

    .line 254
    .local v6, "nextNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-direct {p0, v0}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->nodeShouldBeSelected(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 255
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 256
    .local v1, "currentNodeBounds":Landroid/graphics/Rect;
    invoke-virtual {v0, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getBoundsInScreen(Landroid/graphics/Rect;)V

    .line 257
    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    .line 258
    .local v5, "key":I
    invoke-virtual {v8, v5}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v9

    if-gez v9, :cond_1

    .line 259
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v8, v5, v9}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 262
    :cond_1
    invoke-virtual {v8, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    .end local v1    # "currentNodeBounds":Landroid/graphics/Rect;
    .end local v5    # "key":I
    :goto_2
    move-object v0, v6

    .line 269
    goto :goto_1

    .line 265
    :cond_2
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_2

    .line 272
    .end local v6    # "nextNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_3
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_3
    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v9

    if-ge v3, v9, :cond_5

    .line 273
    invoke-virtual {v8, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 274
    .local v2, "currentRow":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 275
    .local v7, "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    iget-object v9, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mRowCache:Ljava/util/Map;

    invoke-interface {v9, v7, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 272
    .end local v7    # "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 279
    .end local v2    # "currentRow":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_5
    iget-object v9, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mRowCache:Ljava/util/Map;

    iget-object v10, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    goto :goto_0
.end method

.method private getLastNodeInWindow(Landroid/view/accessibility/AccessibilityWindowInfo;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 3
    .param p1, "currentWindow"    # Landroid/view/accessibility/AccessibilityWindowInfo;

    .prologue
    .line 298
    invoke-static {p1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->getRootForWindow(Landroid/view/accessibility/AccessibilityWindowInfo;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 299
    .local v1, "root":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    move-result-object v0

    .line 300
    .local v0, "ref":Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;
    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->lastDescendant()Z

    .line 301
    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 302
    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->release()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    return-object v2
.end method

.method private static getRootForWindow(Landroid/view/accessibility/AccessibilityWindowInfo;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 2
    .param p0, "window"    # Landroid/view/accessibility/AccessibilityWindowInfo;

    .prologue
    const/4 v1, 0x0

    .line 334
    if-nez p0, :cond_1

    .line 343
    :cond_0
    :goto_0
    return-object v1

    .line 338
    :cond_1
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityWindowInfo;->getRoot()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 339
    .local v0, "rootInfo":Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v0, :cond_0

    .line 343
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {v1, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;-><init>(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private nodeShouldBeSelected(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p1, "currentNode"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 426
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getActions()I

    move-result v0

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_0

    .line 427
    const/4 v0, 0x0

    .line 430
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private removeSystemButtonsWindowFromWindowList()V
    .locals 10

    .prologue
    .line 385
    iget-object v8, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mContext:Landroid/content/Context;

    const-string v9, "window"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/WindowManager;

    .line 386
    .local v7, "wm":Landroid/view/WindowManager;
    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 387
    .local v0, "display":Landroid/view/Display;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 388
    .local v2, "screenSize":Landroid/graphics/Point;
    invoke-virtual {v0, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 390
    iget-object v8, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mWindowList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 391
    .local v6, "windowIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/view/accessibility/AccessibilityWindowInfo;>;"
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 392
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/accessibility/AccessibilityWindowInfo;

    .line 394
    .local v3, "window":Landroid/view/accessibility/AccessibilityWindowInfo;
    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityWindowInfo;->getType()I

    move-result v8

    const/4 v9, 0x3

    if-ne v8, v9, :cond_0

    .line 398
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 399
    .local v5, "windowBounds":Landroid/graphics/Rect;
    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityWindowInfo;->getBoundsInScreen(Landroid/graphics/Rect;)V

    .line 402
    iget v8, v5, Landroid/graphics/Rect;->top:I

    if-lez v8, :cond_1

    iget v8, v5, Landroid/graphics/Rect;->bottom:I

    iget v9, v2, Landroid/graphics/Point;->y:I

    if-ge v8, v9, :cond_1

    iget v8, v5, Landroid/graphics/Rect;->left:I

    if-lez v8, :cond_1

    iget v8, v5, Landroid/graphics/Rect;->right:I

    iget v9, v2, Landroid/graphics/Point;->x:I

    if-lt v8, v9, :cond_0

    .line 408
    :cond_1
    iget v8, v5, Landroid/graphics/Rect;->top:I

    if-gtz v8, :cond_2

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v8

    iget v9, v2, Landroid/graphics/Point;->x:I

    div-int/lit8 v9, v9, 0x2

    if-gt v8, v9, :cond_0

    .line 413
    :cond_2
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v9

    mul-int v4, v8, v9

    .line 414
    .local v4, "windowArea":I
    iget v8, v2, Landroid/graphics/Point;->x:I

    iget v9, v2, Landroid/graphics/Point;->y:I

    mul-int v1, v8, v9

    .line 415
    .local v1, "screenArea":I
    div-int/lit8 v8, v1, 0x2

    if-gt v4, v8, :cond_0

    .line 419
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 421
    .end local v1    # "screenArea":I
    .end local v3    # "window":Landroid/view/accessibility/AccessibilityWindowInfo;
    .end local v4    # "windowArea":I
    .end local v5    # "windowBounds":Landroid/graphics/Rect;
    :cond_3
    return-void
.end method

.method private setNodeWithCurrentFocus(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 1
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 434
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v0, :cond_0

    .line 435
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 438
    :cond_0
    iput-object p1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 439
    return-void
.end method

.method private static sortWindowListForTraversalOrder(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityWindowInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 347
    .local p0, "windowList":Ljava/util/List;, "Ljava/util/List<Landroid/view/accessibility/AccessibilityWindowInfo;>;"
    new-instance v0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager$1;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager$1;-><init>()V

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 379
    return-void
.end method


# virtual methods
.method public getNodeWithCurrentFocus()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 1

    .prologue
    .line 330
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mRowHighlighted:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    goto :goto_0
.end method

.method public resetFocusToCurrentWindowRoot()V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mWindowList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 209
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mRowHighlighted:Z

    .line 210
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mWindowList:Ljava/util/List;

    iget v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentWindowIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-static {v0}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->getRootForWindow(Landroid/view/accessibility/AccessibilityWindowInfo;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->setNodeWithCurrentFocus(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 211
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mFocusIndicatorController:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->clearFocus()V

    .line 213
    :cond_0
    return-void
.end method

.method public resetFocusWithNewWindowList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityWindowInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "newWindowList":Ljava/util/List;, "Ljava/util/List<Landroid/view/accessibility/AccessibilityWindowInfo;>;"
    const/4 v1, 0x0

    .line 310
    invoke-direct {p0, v1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->setNodeWithCurrentFocus(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 311
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mFocusIndicatorController:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->clearFocus()V

    .line 312
    invoke-direct {p0}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->clearRowColumn()V

    .line 313
    iput-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mWindowList:Ljava/util/List;

    .line 315
    if-nez p1, :cond_0

    .line 324
    :goto_0
    return-void

    .line 319
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mWindowList:Ljava/util/List;

    .line 321
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mWindowList:Ljava/util/List;

    invoke-static {v0}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->sortWindowListForTraversalOrder(Ljava/util/List;)V

    .line 323
    invoke-direct {p0}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->removeSystemButtonsWindowFromWindowList()V

    goto :goto_0
.end method

.method public updateFocus(I)V
    .locals 5
    .param p1, "focusDirection"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 142
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mWindowList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mWindowList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez v1, :cond_4

    .line 148
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mRowCache:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 149
    if-ne p1, v4, :cond_3

    .line 150
    iput v3, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentWindowIndex:I

    .line 151
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mWindowList:Ljava/util/List;

    iget v2, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentWindowIndex:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-static {v1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->getRootForWindow(Landroid/view/accessibility/AccessibilityWindowInfo;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    iput-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 162
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {p0, v1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->nodeShouldBeSelected(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 163
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {p0, p1, v1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->crossWindowSearch(ILandroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->setNodeWithCurrentFocus(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_1

    .line 152
    :cond_3
    const/4 v1, -0x1

    if-ne p1, v1, :cond_2

    .line 153
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mWindowList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentWindowIndex:I

    .line 154
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mWindowList:Ljava/util/List;

    iget v2, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentWindowIndex:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-direct {p0, v1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->getLastNodeInWindow(Landroid/view/accessibility/AccessibilityWindowInfo;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    iput-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    goto :goto_1

    .line 158
    :cond_4
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {p0, p1, v1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->crossWindowSearch(ILandroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->setNodeWithCurrentFocus(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_1

    .line 166
    :cond_5
    iput-boolean v3, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mRowHighlighted:Z

    .line 167
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez v1, :cond_7

    .line 169
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mFocusIndicatorController:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->clearFocus()V

    .line 170
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->isShowing()Z

    move-result v1

    if-nez v1, :cond_6

    .line 171
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->showGlobalContextMenu()Z

    goto :goto_0

    .line 173
    :cond_6
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->hide()V

    goto :goto_0

    .line 175
    :cond_7
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mWindowList:Ljava/util/List;

    iget v2, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentWindowIndex:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityWindowInfo;->getType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_8

    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentRow:Ljava/util/Set;

    iget-object v2, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 190
    invoke-direct {p0}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->getCurrentRow()Ljava/util/List;

    move-result-object v0

    .line 191
    .local v0, "currentRow":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;>;"
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentRow:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 192
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mCurrentRow:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 194
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    iput-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mFirstNodeInRow:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 195
    iput-boolean v4, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mRowHighlighted:Z

    .line 196
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-static {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->setNodeWithCurrentFocus(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 198
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mFocusIndicatorController:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    invoke-static {v0}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->getBoundingRectForRow(Ljava/util/List;)Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->indicateFocusOnRect(Landroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 200
    .end local v0    # "currentRow":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;>;"
    :cond_8
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mFocusIndicatorController:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    iget-object v2, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->indicateFocusOnNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto/16 :goto_0
.end method

.method public updateFocusForKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 102
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->willHandleKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 103
    invoke-static {p1}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->keyEventToExtendedKeyCode(Landroid/view/KeyEvent;)J

    move-result-wide v0

    .line 104
    .local v0, "extendedKeyCode":J
    iget-object v5, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mContext:Landroid/content/Context;

    const v6, 0x7f0601f8

    invoke-static {v5, v6}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->getKeyCodeForPreference(Landroid/content/Context;I)J

    move-result-wide v6

    cmp-long v5, v0, v6

    if-nez v5, :cond_1

    move v2, v3

    .line 106
    .local v2, "isClickEvent":Z
    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    if-nez v5, :cond_5

    .line 107
    iget-object v5, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mContext:Landroid/content/Context;

    const v6, 0x7f0601f6

    invoke-static {v5, v6}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->getKeyCodeForPreference(Landroid/content/Context;I)J

    move-result-wide v6

    cmp-long v5, v0, v6

    if-nez v5, :cond_2

    .line 109
    invoke-virtual {p0, v3}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->updateFocus(I)V

    .line 132
    .end local v0    # "extendedKeyCode":J
    .end local v2    # "isClickEvent":Z
    :cond_0
    :goto_1
    return v3

    .restart local v0    # "extendedKeyCode":J
    :cond_1
    move v2, v4

    .line 104
    goto :goto_0

    .line 110
    .restart local v2    # "isClickEvent":Z
    :cond_2
    iget-object v5, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mContext:Landroid/content/Context;

    const v6, 0x7f0601f7

    invoke-static {v5, v6}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->getKeyCodeForPreference(Landroid/content/Context;I)J

    move-result-wide v6

    cmp-long v5, v0, v6

    if-nez v5, :cond_4

    .line 112
    iget-boolean v4, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mRowHighlighted:Z

    if-eqz v4, :cond_3

    .line 114
    iget-object v4, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mFirstNodeInRow:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-static {v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->setNodeWithCurrentFocus(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 118
    :cond_3
    const/4 v4, -0x1

    invoke-virtual {p0, v4}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->updateFocus(I)V

    goto :goto_1

    .line 119
    :cond_4
    if-eqz v2, :cond_0

    .line 120
    iget-object v5, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mFirstNodeInRow:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-static {v5}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->setNodeWithCurrentFocus(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 121
    iput-boolean v4, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mRowHighlighted:Z

    .line 122
    iget-object v4, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mFocusIndicatorController:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    iget-object v5, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mNodeWithCurrentFocus:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4, v5}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->indicateFocusOnNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 123
    iput-boolean v3, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mDownClickKeyPressed:Z

    goto :goto_1

    .line 125
    :cond_5
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    if-ne v5, v3, :cond_0

    if-eqz v2, :cond_0

    .line 126
    iput-boolean v4, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mDownClickKeyPressed:Z

    goto :goto_1

    .end local v0    # "extendedKeyCode":J
    .end local v2    # "isClickEvent":Z
    :cond_6
    move v3, v4

    .line 132
    goto :goto_1
.end method

.method public willHandleKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 78
    invoke-static {p1}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->keyEventToExtendedKeyCode(Landroid/view/KeyEvent;)J

    move-result-wide v0

    .line 79
    .local v0, "extendedKeyCode":J
    iget-object v7, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mContext:Landroid/content/Context;

    const v8, 0x7f0601f6

    invoke-static {v7, v8}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->getKeyCodeForPreference(Landroid/content/Context;I)J

    move-result-wide v8

    cmp-long v7, v8, v0

    if-nez v7, :cond_4

    move v3, v5

    .line 85
    .local v3, "isNextKey":Z
    :goto_0
    iget-object v7, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mContext:Landroid/content/Context;

    const v8, 0x7f0601f8

    invoke-static {v7, v8}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->getKeyCodeForPreference(Landroid/content/Context;I)J

    move-result-wide v8

    cmp-long v7, v8, v0

    if-nez v7, :cond_5

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v7

    if-nez v7, :cond_0

    iget-boolean v7, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mRowHighlighted:Z

    if-nez v7, :cond_1

    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v7

    if-ne v7, v5, :cond_5

    iget-boolean v7, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mDownClickKeyPressed:Z

    if-eqz v7, :cond_5

    :cond_1
    move v2, v5

    .line 89
    .local v2, "isClickKey":Z
    :goto_1
    iget-object v7, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->mContext:Landroid/content/Context;

    const v8, 0x7f0601f7

    invoke-static {v7, v8}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->getKeyCodeForPreference(Landroid/content/Context;I)J

    move-result-wide v8

    cmp-long v7, v8, v0

    if-nez v7, :cond_6

    move v4, v5

    .line 91
    .local v4, "isPreviousKey":Z
    :goto_2
    if-nez v3, :cond_2

    if-nez v2, :cond_2

    if-eqz v4, :cond_3

    :cond_2
    move v6, v5

    :cond_3
    return v6

    .end local v2    # "isClickKey":Z
    .end local v3    # "isNextKey":Z
    .end local v4    # "isPreviousKey":Z
    :cond_4
    move v3, v6

    .line 79
    goto :goto_0

    .restart local v3    # "isNextKey":Z
    :cond_5
    move v2, v6

    .line 85
    goto :goto_1

    .restart local v2    # "isClickKey":Z
    :cond_6
    move v4, v6

    .line 89
    goto :goto_2
.end method
