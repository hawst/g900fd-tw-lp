.class public Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;
.super Landroid/widget/LinearLayout;
.source "ContextMenuView.java"


# instance fields
.field private mBackButton:Landroid/widget/Button;

.field private mHomeButton:Landroid/widget/Button;

.field private mNotificationsButton:Landroid/widget/Button;

.field private mQuickSettingsButton:Landroid/widget/Button;

.field private mRecentAppsButton:Landroid/widget/Button;

.field private mScrollBackwardButton:Landroid/widget/Button;

.field private mScrollForwardButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 27
    return-void
.end method


# virtual methods
.method public registerButtons(Landroid/view/View;)V
    .locals 1
    .param p1, "contextMenu"    # Landroid/view/View;

    .prologue
    .line 35
    const v0, 0x7f0d007d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mBackButton:Landroid/widget/Button;

    .line 36
    const v0, 0x7f0d007e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mHomeButton:Landroid/widget/Button;

    .line 37
    const v0, 0x7f0d007f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mRecentAppsButton:Landroid/widget/Button;

    .line 39
    const v0, 0x7f0d0080

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mNotificationsButton:Landroid/widget/Button;

    .line 41
    const v0, 0x7f0d0081

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mQuickSettingsButton:Landroid/widget/Button;

    .line 43
    const v0, 0x7f0d0082

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mScrollForwardButton:Landroid/widget/Button;

    .line 45
    const v0, 0x7f0d0083

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mScrollBackwardButton:Landroid/widget/Button;

    .line 47
    return-void
.end method

.method public showGlobalActionButtons()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 53
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mScrollForwardButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 54
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mScrollBackwardButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 56
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mBackButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 57
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mHomeButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 58
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mRecentAppsButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 59
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mNotificationsButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 60
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mQuickSettingsButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 61
    return-void
.end method

.method public showScrollableActionButtons()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 67
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mBackButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 68
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mHomeButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 69
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mRecentAppsButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 70
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mNotificationsButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 71
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mQuickSettingsButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 73
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mScrollForwardButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 74
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->mScrollBackwardButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 75
    return-void
.end method
