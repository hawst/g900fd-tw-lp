.class Lcom/googlecode/eyesfree/switchcontrol/AutoScanController$1;
.super Ljava/lang/Object;
.source "AutoScanController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;


# direct methods
.method constructor <init>(Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 43
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    # invokes: Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->isAutoScanEnabled()Z
    invoke-static {v0}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->access$000(Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    # invokes: Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->stopScan()V
    invoke-static {v0}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->access$100(Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;)V

    .line 55
    :goto_0
    return-void

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    # getter for: Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mFocusManager:Lcom/googlecode/eyesfree/switchcontrol/FocusManager;
    invoke-static {v0}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->access$200(Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;)Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->updateFocus(I)V

    .line 49
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    # getter for: Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mFocusManager:Lcom/googlecode/eyesfree/switchcontrol/FocusManager;
    invoke-static {v0}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->access$200(Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;)Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->getNodeWithCurrentFocus()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    # getter for: Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;
    invoke-static {v0}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->access$300(Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;)Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 51
    :cond_1
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    # getter for: Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->access$500(Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    # invokes: Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->getAutoScanDelay()I
    invoke-static {v1}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->access$400(Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 53
    :cond_2
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController$1;->this$0:Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    # invokes: Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->stopScan()V
    invoke-static {v0}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->access$100(Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;)V

    goto :goto_0
.end method
