.class public Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;
.super Landroid/preference/DialogPreference;
.source "KeyComboPreference.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mCurrentKeyTextView:Landroid/widget/TextView;

.field private mKeyCombo:J

.field private mResetButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 75
    const v0, 0x7f030008

    invoke-virtual {p0, v0}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->setDialogLayoutResource(I)V

    .line 76
    const v0, 0x104000a

    invoke-virtual {p0, v0}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->setPositiveButtonText(I)V

    .line 77
    const/high16 v0, 0x1040000

    invoke-virtual {p0, v0}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->setNegativeButtonText(I)V

    .line 78
    iput-object p1, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mContext:Landroid/content/Context;

    .line 79
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->setDialogIcon(Landroid/graphics/drawable/Drawable;)V

    .line 80
    return-void
.end method

.method static synthetic access$000(Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;J)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;
    .param p1, "x1"    # J

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->describeExtendedKeyCode(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;)J
    .locals 2
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;

    .prologue
    .line 22
    iget-wide v0, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mKeyCombo:J

    return-wide v0
.end method

.method static synthetic access$202(Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;J)J
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;
    .param p1, "x1"    # J

    .prologue
    .line 22
    iput-wide p1, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mKeyCombo:J

    return-wide p1
.end method

.method static synthetic access$300(Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mCurrentKeyTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method private describeExtendedKeyCode(J)Ljava/lang/String;
    .locals 7
    .param p1, "extendedKeyCode"    # J

    .prologue
    const-wide/16 v4, 0x0

    .line 151
    iget-object v2, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 152
    .local v1, "res":Landroid/content/res/Resources;
    const-wide/16 v2, -0x1

    cmp-long v2, p1, v2

    if-nez v2, :cond_0

    .line 153
    const v2, 0x7f060226

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 174
    :goto_0
    return-object v2

    .line 157
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 158
    .local v0, "keystrokeDescriptionBuilder":Ljava/lang/StringBuilder;
    const-wide v2, 0x100000000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 159
    const v2, 0x7f060223

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    :cond_1
    const-wide v2, 0x200000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 164
    const v2, 0x7f060225

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    :cond_2
    const-wide v2, 0x100000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 169
    const v2, 0x7f060224

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    :cond_3
    long-to-int v2, p1

    invoke-static {v2}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static getKeyCodeForPreference(Landroid/content/Context;I)J
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I

    .prologue
    .line 64
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 65
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    return-wide v2
.end method

.method public static keyEventToExtendedKeyCode(Landroid/view/KeyEvent;)J
    .locals 6
    .param p0, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    const-wide/16 v4, 0x0

    .line 49
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    int-to-long v0, v2

    .line 50
    .local v0, "returnValue":J
    invoke-virtual {p0}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v2

    if-eqz v2, :cond_1

    const-wide v2, 0x100000000L

    :goto_0
    or-long/2addr v0, v2

    .line 51
    invoke-virtual {p0}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v2

    if-eqz v2, :cond_2

    const-wide v2, 0x100000000000L

    :goto_1
    or-long/2addr v0, v2

    .line 52
    invoke-virtual {p0}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v2

    if-eqz v2, :cond_0

    const-wide v4, 0x200000000L

    :cond_0
    or-long/2addr v0, v4

    .line 53
    return-wide v0

    :cond_1
    move-wide v2, v4

    .line 50
    goto :goto_0

    :cond_2
    move-wide v2, v4

    .line 51
    goto :goto_1
.end method


# virtual methods
.method public getSummary()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 97
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->getPersistedLong(J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->describeExtendedKeyCode(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onBindDialogView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 111
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    .line 113
    const v0, 0x7f0d0085

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mCurrentKeyTextView:Landroid/widget/TextView;

    .line 115
    const v0, 0x7f0d0086

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mResetButton:Landroid/widget/Button;

    .line 116
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mResetButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    .line 117
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mResetButton:Landroid/widget/Button;

    new-instance v1, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference$1;

    invoke-direct {v1, p0}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference$1;-><init>(Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 2
    .param p1, "positiveResult"    # Z

    .prologue
    .line 102
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    .line 103
    if-eqz p1, :cond_0

    .line 104
    iget-wide v0, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mKeyCombo:J

    invoke-virtual {p0, v0, v1}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->persistLong(J)Z

    .line 105
    iget-wide v0, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mKeyCombo:J

    invoke-direct {p0, v0, v1}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->describeExtendedKeyCode(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 107
    :cond_0
    return-void
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v4, 0x1

    .line 85
    invoke-static {p2}, Landroid/view/KeyEvent;->isModifierKey(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    :goto_0
    return v4

    .line 89
    :cond_0
    invoke-static {p3}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->keyEventToExtendedKeyCode(Landroid/view/KeyEvent;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mKeyCombo:J

    .line 90
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mCurrentKeyTextView:Landroid/widget/TextView;

    iget-wide v2, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mKeyCombo:J

    invoke-direct {p0, v2, v3}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->describeExtendedKeyCode(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected showDialog(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "state"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 131
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->showDialog(Landroid/os/Bundle;)V

    .line 132
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    .line 135
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setFocusable(Z)V

    .line 136
    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setFocusable(Z)V

    .line 137
    const-wide/16 v2, -0x1

    invoke-virtual {p0, v2, v3}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->getPersistedLong(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mKeyCombo:J

    .line 138
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->mCurrentKeyTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 141
    return-void
.end method
