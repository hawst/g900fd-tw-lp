.class final Lcom/googlecode/eyesfree/switchcontrol/FocusManager$1;
.super Ljava/lang/Object;
.source "FocusManager.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->sortWindowListForTraversalOrder(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/view/accessibility/AccessibilityWindowInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Landroid/view/accessibility/AccessibilityWindowInfo;Landroid/view/accessibility/AccessibilityWindowInfo;)I
    .locals 7
    .param p1, "arg0"    # Landroid/view/accessibility/AccessibilityWindowInfo;
    .param p2, "arg1"    # Landroid/view/accessibility/AccessibilityWindowInfo;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 351
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityWindowInfo;->getType()I

    move-result v0

    .line 352
    .local v0, "type0":I
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityWindowInfo;->getType()I

    move-result v1

    .line 353
    .local v1, "type1":I
    if-ne v0, v1, :cond_1

    .line 376
    :cond_0
    :goto_0
    return v2

    .line 358
    :cond_1
    if-ne v0, v5, :cond_2

    move v2, v3

    .line 359
    goto :goto_0

    .line 362
    :cond_2
    if-ne v1, v5, :cond_3

    move v2, v4

    .line 363
    goto :goto_0

    .line 367
    :cond_3
    if-ne v0, v6, :cond_4

    move v2, v4

    .line 368
    goto :goto_0

    .line 371
    :cond_4
    if-ne v1, v6, :cond_0

    move v2, v3

    .line 372
    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 347
    check-cast p1, Landroid/view/accessibility/AccessibilityWindowInfo;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/view/accessibility/AccessibilityWindowInfo;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager$1;->compare(Landroid/view/accessibility/AccessibilityWindowInfo;Landroid/view/accessibility/AccessibilityWindowInfo;)I

    move-result v0

    return v0
.end method
