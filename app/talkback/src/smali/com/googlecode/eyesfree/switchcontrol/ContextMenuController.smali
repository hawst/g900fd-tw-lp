.class public Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;
.super Ljava/lang/Object;
.source "ContextMenuController.java"


# instance fields
.field private final mContextMenuView:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;

.field private mIsShowing:Z

.field private final mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

.field private mScrollableNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private final mSwitchControlService:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;


# direct methods
.method public constructor <init>(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;Lcom/googlecode/eyesfree/widget/SimpleOverlay;Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;)V
    .locals 3
    .param p1, "switchControlService"    # Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;
    .param p2, "overlay"    # Lcom/googlecode/eyesfree/widget/SimpleOverlay;
    .param p3, "contextMenuView"    # Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p2, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    .line 36
    iput-object p1, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mSwitchControlService:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    .line 37
    iput-object p3, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mContextMenuView:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;

    .line 38
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    iget-object v2, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mContextMenuView:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->setContentView(Landroid/view/View;)V

    .line 40
    const v1, 0x7f0d007d

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->setupGlobalMenuButton(II)V

    .line 42
    const v1, 0x7f0d007e

    const/4 v2, 0x2

    invoke-direct {p0, v1, v2}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->setupGlobalMenuButton(II)V

    .line 44
    const v1, 0x7f0d007f

    const/4 v2, 0x3

    invoke-direct {p0, v1, v2}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->setupGlobalMenuButton(II)V

    .line 46
    const v1, 0x7f0d0080

    const/4 v2, 0x4

    invoke-direct {p0, v1, v2}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->setupGlobalMenuButton(II)V

    .line 48
    const v1, 0x7f0d0081

    const/4 v2, 0x5

    invoke-direct {p0, v1, v2}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->setupGlobalMenuButton(II)V

    .line 51
    const v1, 0x7f0d0082

    const/16 v2, 0x1000

    invoke-direct {p0, v1, v2}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->setupScrollableMenuButton(II)V

    .line 53
    const v1, 0x7f0d0083

    const/16 v2, 0x2000

    invoke-direct {p0, v1, v2}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->setupScrollableMenuButton(II)V

    .line 57
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->getParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 58
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    const/16 v1, 0x7da

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 59
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    invoke-virtual {v1, v0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->setParams(Landroid/view/WindowManager$LayoutParams;)V

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;)Lcom/googlecode/eyesfree/widget/SimpleOverlay;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    return-object v0
.end method

.method static synthetic access$100(Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mScrollableNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    return-object v0
.end method

.method static synthetic access$200(Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;)Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mSwitchControlService:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    return-object v0
.end method

.method private setupGlobalMenuButton(II)V
    .locals 2
    .param p1, "resId"    # I
    .param p2, "action"    # I

    .prologue
    .line 146
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mContextMenuView:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;

    invoke-virtual {v1, p1}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 147
    .local v0, "menuButton":Landroid/widget/Button;
    new-instance v1, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController$2;

    invoke-direct {v1, p0, p2}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController$2;-><init>(Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    return-void
.end method

.method private setupScrollableMenuButton(II)V
    .locals 2
    .param p1, "resId"    # I
    .param p2, "action"    # I

    .prologue
    .line 126
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mContextMenuView:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;

    invoke-virtual {v1, p1}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 127
    .local v0, "menuButton":Landroid/widget/Button;
    new-instance v1, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController$1;

    invoke-direct {v1, p0, p2}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController$1;-><init>(Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mIsShowing:Z

    .line 103
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->hide()V

    .line 104
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mSwitchControlService:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->refreshWithNewWindows()V

    .line 105
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mScrollableNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mScrollableNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mScrollableNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 109
    :cond_0
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mIsShowing:Z

    return v0
.end method

.method public showGlobalContextMenu()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 69
    iget-boolean v1, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mIsShowing:Z

    if-nez v1, :cond_0

    .line 70
    iput-boolean v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mIsShowing:Z

    .line 71
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mContextMenuView:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->showGlobalActionButtons()V

    .line 72
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->show()V

    .line 76
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showScrollableContextMenu(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 2
    .param p1, "scrollableNode"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v0, 0x1

    .line 87
    iget-boolean v1, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mIsShowing:Z

    if-nez v1, :cond_0

    .line 88
    iput-boolean v0, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mIsShowing:Z

    .line 89
    iput-object p1, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mScrollableNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 90
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mContextMenuView:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->showScrollableActionButtons()V

    .line 91
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->show()V

    .line 95
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
