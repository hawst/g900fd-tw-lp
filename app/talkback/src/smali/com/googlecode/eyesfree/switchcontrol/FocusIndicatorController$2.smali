.class Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController$2;
.super Ljava/lang/Object;
.source "FocusIndicatorController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->indicateFocusOnRect(Landroid/graphics/Rect;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

.field final synthetic val$boundingRect:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController$2;->this$0:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    iput-object p2, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController$2;->val$boundingRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 74
    new-instance v0, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController$2;->this$0:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    # getter for: Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mOverlay:Lcom/googlecode/eyesfree/widget/SimpleOverlay;
    invoke-static {v3}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->access$100(Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;)Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    move-result-object v3

    invoke-virtual {v3}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 75
    .local v0, "imageView":Landroid/widget/ImageView;
    const v3, 0x7f020020

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 78
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController$2;->val$boundingRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget-object v4, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController$2;->val$boundingRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 80
    .local v2, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v3, 0x2

    new-array v1, v3, [I

    .line 81
    .local v1, "layoutCoordinates":[I
    iget-object v3, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController$2;->this$0:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    # getter for: Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->access$200(Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    .line 82
    iget-object v3, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController$2;->val$boundingRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    const/4 v4, 0x0

    aget v4, v1, v4

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 83
    iget-object v3, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController$2;->val$boundingRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    const/4 v4, 0x1

    aget v4, v1, v4

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 85
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 87
    iget-object v3, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController$2;->this$0:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    # getter for: Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->access$200(Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 88
    iget-object v3, p0, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController$2;->this$0:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    # getter for: Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->mRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->access$200(Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 89
    return-void
.end method
