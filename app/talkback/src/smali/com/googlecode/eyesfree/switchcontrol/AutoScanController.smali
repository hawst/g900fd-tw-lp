.class public Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;
.super Ljava/lang/Object;
.source "AutoScanController.java"


# instance fields
.field private final mAutoScan:Ljava/lang/Runnable;

.field private final mContext:Landroid/content/Context;

.field private final mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

.field private final mFocusIndicator:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

.field private final mFocusManager:Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

.field private final mHandler:Landroid/os/Handler;

.field private mIsScanInProgress:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/googlecode/eyesfree/switchcontrol/FocusManager;Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;Landroid/os/Handler;Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "focusManager"    # Lcom/googlecode/eyesfree/switchcontrol/FocusManager;
    .param p3, "focusIndicator"    # Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;
    .param p4, "handler"    # Landroid/os/Handler;
    .param p5, "contextMenuController"    # Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController$1;

    invoke-direct {v0, p0}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController$1;-><init>(Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mAutoScan:Ljava/lang/Runnable;

    .line 64
    iput-object p1, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mContext:Landroid/content/Context;

    .line 65
    iput-object p2, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mFocusManager:Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

    .line 66
    iput-object p3, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mFocusIndicator:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    .line 67
    iput-object p4, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mHandler:Landroid/os/Handler;

    .line 68
    iput-object p5, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->isAutoScanEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;)V
    .locals 0
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->stopScan()V

    return-void
.end method

.method static synthetic access$200(Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;)Lcom/googlecode/eyesfree/switchcontrol/FocusManager;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mFocusManager:Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;)Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    return-object v0
.end method

.method static synthetic access$400(Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;)I
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->getAutoScanDelay()I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getAutoScanDelay()I
    .locals 5

    .prologue
    .line 147
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 148
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const/high16 v1, 0x447a0000    # 1000.0f

    iget-object v2, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0601f3

    const v4, 0x7f060205

    invoke-static {v0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getFloatFromStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    return v1
.end method

.method private isAutoScanEnabled()Z
    .locals 4

    .prologue
    .line 138
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 139
    .local v0, "prefs":Landroid/content/SharedPreferences;
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mContext:Landroid/content/Context;

    const v2, 0x7f0601f2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mContext:Landroid/content/Context;

    const v3, 0x7f060204

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private startScan()V
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mIsScanInProgress:Z

    .line 122
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mAutoScan:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 123
    return-void
.end method

.method private stopScan()V
    .locals 2

    .prologue
    .line 129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mIsScanInProgress:Z

    .line 130
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mAutoScan:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 131
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mFocusIndicator:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->clearFocus()V

    .line 132
    return-void
.end method


# virtual methods
.method public onWindowStateChanged()V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->stopScan()V

    .line 115
    return-void
.end method

.method public performActionForKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 92
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->willHandleKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 93
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    .line 94
    iget-object v2, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mFocusManager:Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->getNodeWithCurrentFocus()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 95
    .local v0, "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    iget-boolean v2, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mIsScanInProgress:Z

    if-nez v2, :cond_1

    .line 96
    invoke-direct {p0}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->startScan()V

    .line 107
    .end local v0    # "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_0
    :goto_0
    return v1

    .line 97
    .restart local v0    # "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_1
    if-eqz v0, :cond_0

    .line 98
    iget-object v2, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    sget-object v4, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->CLICK:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    invoke-static {v2, v3, v0, v4, v1}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->performAction(Landroid/content/Context;Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;Z)Z

    goto :goto_0

    .line 107
    .end local v0    # "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public willHandleKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 77
    invoke-static {p1}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->keyEventToExtendedKeyCode(Landroid/view/KeyEvent;)J

    move-result-wide v0

    .line 78
    .local v0, "extendedKeyCode":J
    invoke-direct {p0}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->isAutoScanEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->mContext:Landroid/content/Context;

    const v3, 0x7f0601f4

    invoke-static {v2, v3}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->getKeyCodeForPreference(Landroid/content/Context;I)J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
