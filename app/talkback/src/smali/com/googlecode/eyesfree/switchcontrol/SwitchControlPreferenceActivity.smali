.class public Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity;
.super Landroid/preference/PreferenceActivity;
.source "SwitchControlPreferenceActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v6, 0x7f0601f3

    const/4 v4, 0x1

    .line 30
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    const v5, 0x7f040004

    invoke-virtual {p0, v5}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity;->addPreferencesFromResource(I)V

    .line 34
    invoke-virtual {p0, v6}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 36
    .local v0, "autoScanDelayPref":Landroid/preference/Preference;
    invoke-virtual {v0}, Landroid/preference/Preference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-virtual {p0, v6}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f060205

    invoke-virtual {p0, v7}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 40
    .local v2, "currentValue":D
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double v6, v2, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide v8, 0x3f847ae147ae147bL    # 0.01

    cmpg-double v5, v6, v8

    if-gez v5, :cond_0

    move v1, v4

    .line 42
    .local v1, "count":I
    :goto_0
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0002

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    aput-object v8, v4, v7

    invoke-virtual {v5, v6, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 45
    new-instance v4, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity$1;

    invoke-direct {v4, p0}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity$1;-><init>(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlPreferenceActivity;)V

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 65
    return-void

    .line 40
    .end local v1    # "count":I
    :cond_0
    const/4 v1, 0x2

    goto :goto_0
.end method
