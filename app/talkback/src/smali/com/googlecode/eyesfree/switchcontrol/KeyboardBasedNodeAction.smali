.class public final enum Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;
.super Ljava/lang/Enum;
.source "KeyboardBasedNodeAction.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

.field public static final enum CLICK:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

.field public static final enum LONG_CLICK:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

.field public static final enum SCROLL_BACK:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

.field public static final enum SCROLL_FORWARD:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;


# instance fields
.field private final mAction:I

.field private final mPreferenceKeyId:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 18
    new-instance v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    const-string v1, "CLICK"

    const v2, 0x7f0601f8

    const/16 v3, 0x10

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->CLICK:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    .line 19
    new-instance v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    const-string v1, "LONG_CLICK"

    const v2, 0x7f0601f9

    const/16 v3, 0x20

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->LONG_CLICK:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    .line 21
    new-instance v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    const-string v1, "SCROLL_FORWARD"

    const v2, 0x7f0601fa

    const/16 v3, 0x1000

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->SCROLL_FORWARD:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    .line 23
    new-instance v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    const-string v1, "SCROLL_BACK"

    const v2, 0x7f0601fb

    const/16 v3, 0x2000

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->SCROLL_BACK:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    .line 17
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    sget-object v1, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->CLICK:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->LONG_CLICK:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->SCROLL_FORWARD:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->SCROLL_BACK:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    aput-object v1, v0, v7

    sput-object v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->$VALUES:[Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "preferenceKeyId"    # I
    .param p4, "action"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput p3, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->mPreferenceKeyId:I

    .line 32
    iput p4, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->mAction:I

    .line 33
    return-void
.end method

.method public static getActionForKeyEvent(Landroid/content/Context;Landroid/view/KeyEvent;)Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 44
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v9

    .line 45
    .local v9, "prefs":Landroid/content/SharedPreferences;
    invoke-static {p1}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->keyEventToExtendedKeyCode(Landroid/view/KeyEvent;)J

    move-result-wide v2

    .line 46
    .local v2, "extendedKeyCode":J
    invoke-static {}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->values()[Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    move-result-object v1

    .local v1, "arr$":[Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v0, v1, v4

    .line 47
    .local v0, "action":Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;
    iget v10, v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->mPreferenceKeyId:I

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 48
    .local v8, "preferenceKey":Ljava/lang/String;
    const-wide/16 v10, -0x1

    invoke-interface {v9, v8, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 50
    .local v6, "keyCodeForAction":J
    cmp-long v10, v2, v6

    if-nez v10, :cond_0

    .line 55
    .end local v0    # "action":Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;
    .end local v6    # "keyCodeForAction":J
    .end local v8    # "preferenceKey":Ljava/lang/String;
    :goto_1
    return-object v0

    .line 46
    .restart local v0    # "action":Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;
    .restart local v6    # "keyCodeForAction":J
    .restart local v8    # "preferenceKey":Ljava/lang/String;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 55
    .end local v0    # "action":Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;
    .end local v6    # "keyCodeForAction":J
    .end local v8    # "preferenceKey":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static performAction(Landroid/content/Context;Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;Z)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contextMenuController"    # Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;
    .param p2, "nodeWithCurrentFocus"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3, "action"    # Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;
    .param p4, "isKeyDown"    # Z

    .prologue
    const/4 v2, 0x1

    .line 91
    if-nez p3, :cond_1

    .line 92
    const/4 v2, 0x0

    .line 118
    :cond_0
    :goto_0
    return v2

    .line 95
    :cond_1
    if-eqz p4, :cond_0

    if-eqz p2, :cond_0

    .line 96
    sget-object v1, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SCROLLABLE:Lcom/googlecode/eyesfree/utils/NodeFilter;

    .line 97
    .local v1, "scrollableFilter":Lcom/googlecode/eyesfree/utils/NodeFilter;
    invoke-virtual {v1, p0, p2}, Lcom/googlecode/eyesfree/utils/NodeFilter;->accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 98
    sget-object v3, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->CLICK:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    if-eq p3, v3, :cond_2

    sget-object v3, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->LONG_CLICK:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    if-ne p3, v3, :cond_3

    .line 99
    :cond_2
    invoke-static {p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->showScrollableContextMenu(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    goto :goto_0

    .line 104
    :cond_3
    iget v3, p3, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->mAction:I

    invoke-virtual {p2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    goto :goto_0

    .line 105
    :cond_4
    sget-object v3, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->SCROLL_FORWARD:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    if-eq p3, v3, :cond_5

    sget-object v3, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->SCROLL_BACK:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    if-ne p3, v3, :cond_6

    .line 106
    :cond_5
    invoke-static {p0, p2, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getSelfOrMatchingAncestor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 109
    .local v0, "scrollableAncestor":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v0, :cond_0

    .line 110
    iget v3, p3, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->mAction:I

    invoke-virtual {v0, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    .line 111
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    .line 114
    .end local v0    # "scrollableAncestor":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_6
    iget v3, p3, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->mAction:I

    invoke-virtual {p2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    goto :goto_0
.end method

.method public static performActionForKeyEvent(Landroid/content/Context;Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/KeyEvent;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contextMenuController"    # Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;
    .param p2, "nodeWithCurrentFocus"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 72
    invoke-static {p0, p3}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->getActionForKeyEvent(Landroid/content/Context;Landroid/view/KeyEvent;)Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    move-result-object v0

    .line 73
    .local v0, "action":Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 74
    .local v1, "isKeyDown":Z
    :goto_0
    invoke-static {p0, p1, p2, v0, v1}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->performAction(Landroid/content/Context;Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;Z)Z

    move-result v2

    return v2

    .line 73
    .end local v1    # "isKeyDown":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    return-object v0
.end method

.method public static values()[Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->$VALUES:[Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    invoke-virtual {v0}, [Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    return-object v0
.end method
