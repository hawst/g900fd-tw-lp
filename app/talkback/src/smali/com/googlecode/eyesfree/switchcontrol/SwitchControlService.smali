.class public Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;
.super Landroid/accessibilityservice/AccessibilityService;
.source "SwitchControlService.java"


# static fields
.field private static sInstance:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;


# instance fields
.field private mAutoScanController:Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

.field private mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

.field private mContextMenuView:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;

.field private mFocusIndicator:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

.field private mFocusManager:Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

.field private mHandler:Landroid/os/Handler;

.field private mLastWindowChangeTime:J

.field private mListOfHeldOffKeyEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/KeyEvent;",
            ">;"
        }
    .end annotation
.end field

.field private mRefreshRoot:Z

.field private final mRunnableToProcessHeldOffKeyEvents:Ljava/lang/Runnable;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    sput-object v0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->sInstance:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/accessibilityservice/AccessibilityService;-><init>()V

    .line 38
    new-instance v0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService$1;

    invoke-direct {v0, p0}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService$1;-><init>(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mRunnableToProcessHeldOffKeyEvents:Ljava/lang/Runnable;

    .line 77
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mLastWindowChangeTime:J

    return-void
.end method

.method static synthetic access$000(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;)J
    .locals 2
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->getMillisUntilSafeToProcessKeyEvents()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$100(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mRunnableToProcessHeldOffKeyEvents:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mListOfHeldOffKeyEvents:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$400(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;Landroid/view/KeyEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;
    .param p1, "x1"    # Landroid/view/KeyEvent;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->processGuarenteedSafeKeyEvent(Landroid/view/KeyEvent;)V

    return-void
.end method

.method private flagUserActivity()V
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 255
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 256
    return-void
.end method

.method private getMillisUntilSafeToProcessKeyEvents()J
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 259
    iget-wide v4, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mLastWindowChangeTime:J

    const-wide/16 v6, 0x1f4

    add-long/2addr v4, v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 261
    .local v0, "timeToWait":J
    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 262
    :goto_0
    return-wide v0

    :cond_0
    move-wide v0, v2

    .line 261
    goto :goto_0
.end method

.method private processGuarenteedSafeKeyEvent(Landroid/view/KeyEvent;)V
    .locals 2
    .param p1, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 231
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mRefreshRoot:Z

    if-eqz v0, :cond_0

    .line 232
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->refreshWithNewWindows()V

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mFocusManager:Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->updateFocusForKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 251
    :cond_1
    :goto_0
    return-void

    .line 239
    :cond_2
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mAutoScanController:Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->performActionForKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 243
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mFocusManager:Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->getNodeWithCurrentFocus()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    invoke-static {p0, v0, v1, p1}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->performActionForKeyEvent(Landroid/content/Context;Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 245
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mFocusManager:Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->resetFocusToCurrentWindowRoot()V

    goto :goto_0

    .line 249
    :cond_3
    invoke-static {p0, p0, p1}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->performActionForKeyEvent(Landroid/content/Context;Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;Landroid/view/KeyEvent;)Z

    .line 250
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mFocusManager:Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->resetFocusToCurrentWindowRoot()V

    goto :goto_0
.end method


# virtual methods
.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 13
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v10, 0x0

    const/4 v11, 0x1

    .line 112
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v3

    .line 113
    .local v3, "eventType":I
    const/16 v12, 0x20

    if-ne v3, v12, :cond_3

    move v8, v11

    .line 115
    .local v8, "willClearFocus":Z
    :goto_0
    const/16 v12, 0x800

    if-ne v3, v12, :cond_5

    .line 117
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getContentChangeTypes()I

    move-result v0

    .line 118
    .local v0, "changeTypes":I
    and-int/lit8 v0, v0, -0x3

    .line 119
    and-int/lit8 v0, v0, -0x5

    .line 120
    if-nez v8, :cond_0

    if-eqz v0, :cond_4

    :cond_0
    move v8, v11

    .line 152
    .end local v0    # "changeTypes":I
    :cond_1
    :goto_1
    if-eqz v8, :cond_2

    .line 153
    iget-object v10, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mFocusManager:Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

    const/4 v12, 0x0

    invoke-virtual {v10, v12}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->resetFocusWithNewWindowList(Ljava/util/List;)V

    .line 154
    iget-object v10, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    invoke-virtual {v10}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->hide()V

    .line 155
    iput-boolean v11, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mRefreshRoot:Z

    .line 156
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    iput-wide v10, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mLastWindowChangeTime:J

    .line 158
    :cond_2
    :goto_2
    return-void

    .end local v8    # "willClearFocus":Z
    :cond_3
    move v8, v10

    .line 113
    goto :goto_0

    .restart local v0    # "changeTypes":I
    .restart local v8    # "willClearFocus":Z
    :cond_4
    move v8, v10

    .line 120
    goto :goto_1

    .line 121
    .end local v0    # "changeTypes":I
    :cond_5
    const/high16 v12, 0x400000

    if-ne v3, v12, :cond_1

    .line 122
    iget-object v12, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    invoke-virtual {v12}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->isShowing()Z

    move-result v12

    if-eqz v12, :cond_8

    .line 123
    const v12, 0x7f060218

    invoke-virtual {p0, v12}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 125
    .local v2, "contextMenuDescription":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->getWindows()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/accessibility/AccessibilityWindowInfo;

    .line 127
    .local v9, "window":Landroid/view/accessibility/AccessibilityWindowInfo;
    new-instance v7, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v9}, Landroid/view/accessibility/AccessibilityWindowInfo;->getRoot()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v12

    invoke-direct {v7, v12}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;-><init>(Ljava/lang/Object;)V

    .line 129
    .local v7, "root":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {v7, v11}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v6

    .line 131
    .local v6, "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v6, :cond_7

    .line 132
    invoke-virtual {v6}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 134
    .local v5, "isContextMenu":Z
    const/4 v12, 0x2

    new-array v12, v12, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v6, v12, v10

    aput-object v7, v12, v11

    invoke-static {v12}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 135
    if-eqz v5, :cond_6

    .line 136
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 138
    .local v1, "contextMenu":Ljava/util/List;, "Ljava/util/List<Landroid/view/accessibility/AccessibilityWindowInfo;>;"
    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    iget-object v10, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mFocusManager:Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

    invoke-virtual {v10, v1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->resetFocusWithNewWindowList(Ljava/util/List;)V

    goto :goto_2

    .line 142
    .end local v1    # "contextMenu":Ljava/util/List;, "Ljava/util/List<Landroid/view/accessibility/AccessibilityWindowInfo;>;"
    .end local v5    # "isContextMenu":Z
    :cond_7
    if-eqz v7, :cond_6

    .line 143
    invoke-virtual {v7}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_3

    .line 149
    .end local v2    # "contextMenuDescription":Ljava/lang/String;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .end local v7    # "root":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .end local v9    # "window":Landroid/view/accessibility/AccessibilityWindowInfo;
    :cond_8
    const/4 v8, 0x1

    goto :goto_1
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 85
    invoke-super {p0}, Landroid/accessibilityservice/AccessibilityService;->onCreate()V

    .line 86
    sput-object p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->sInstance:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    .line 87
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mHandler:Landroid/os/Handler;

    .line 88
    new-instance v1, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    new-instance v2, Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    invoke-direct {v2, p0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;-><init>(Landroid/content/Context;)V

    invoke-direct {v1, v2}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;-><init>(Lcom/googlecode/eyesfree/widget/SimpleOverlay;)V

    iput-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mFocusIndicator:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    .line 89
    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 91
    .local v0, "inflater":Landroid/view/LayoutInflater;
    new-instance v1, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;

    invoke-direct {v1, p0}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mContextMenuView:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;

    .line 92
    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mContextMenuView:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;

    const v2, 0x7f030007

    iget-object v3, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mContextMenuView:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;->registerButtons(Landroid/view/View;)V

    .line 94
    new-instance v1, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    new-instance v2, Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    invoke-direct {v2, p0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mContextMenuView:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;

    invoke-direct {v1, p0, v2, v3}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;-><init>(Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;Lcom/googlecode/eyesfree/widget/SimpleOverlay;Lcom/googlecode/eyesfree/switchcontrol/ContextMenuView;)V

    iput-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    .line 96
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 100
    invoke-super {p0}, Landroid/accessibilityservice/AccessibilityService;->onDestroy()V

    .line 101
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mFocusIndicator:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mFocusIndicator:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->shutdown()V

    .line 105
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->sInstance:Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;

    .line 106
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mFocusIndicator:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;->clearFocus()V

    .line 107
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;->hide()V

    .line 108
    return-void
.end method

.method public onInterrupt()V
    .locals 0

    .prologue
    .line 163
    return-void
.end method

.method protected onKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 210
    invoke-static {p0, p1}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->getActionForKeyEvent(Landroid/content/Context;Landroid/view/KeyEvent;)Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;->getActionForKeyEvent(Landroid/content/Context;Landroid/view/KeyEvent;)Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedNodeAction;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mFocusManager:Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->willHandleKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mAutoScanController:Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->willHandleKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    const/4 v0, 0x0

    .line 227
    :goto_0
    return v0

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mListOfHeldOffKeyEvents:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mRunnableToProcessHeldOffKeyEvents:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->getMillisUntilSafeToProcessKeyEvents()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 226
    invoke-direct {p0}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->flagUserActivity()V

    .line 227
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onServiceConnected()V
    .locals 7

    .prologue
    .line 197
    new-instance v0, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

    iget-object v1, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mFocusIndicator:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    iget-object v2, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    invoke-direct {v0, p0, v1, v2}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;-><init>(Landroid/content/Context;Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mFocusManager:Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

    .line 198
    new-instance v0, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    iget-object v2, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mFocusManager:Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

    iget-object v3, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mFocusIndicator:Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;

    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    iget-object v5, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mContextMenuController:Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;-><init>(Landroid/content/Context;Lcom/googlecode/eyesfree/switchcontrol/FocusManager;Lcom/googlecode/eyesfree/switchcontrol/FocusIndicatorController;Landroid/os/Handler;Lcom/googlecode/eyesfree/switchcontrol/ContextMenuController;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mAutoScanController:Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    .line 200
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/PowerManager;

    .line 201
    .local v6, "powerManager":Landroid/os/PowerManager;
    const v0, 0x2000001a

    const-string v1, "SwitchControl"

    invoke-virtual {v6, v0, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 203
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mListOfHeldOffKeyEvents:Ljava/util/List;

    .line 204
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mRefreshRoot:Z

    .line 205
    return-void
.end method

.method public refreshWithNewWindows()V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mFocusManager:Lcom/googlecode/eyesfree/switchcontrol/FocusManager;

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->getWindows()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/switchcontrol/FocusManager;->resetFocusWithNewWindowList(Ljava/util/List;)V

    .line 186
    iget-object v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mAutoScanController:Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/switchcontrol/AutoScanController;->onWindowStateChanged()V

    .line 187
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->mRefreshRoot:Z

    .line 188
    return-void
.end method
