.class public final enum Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;
.super Ljava/lang/Enum;
.source "KeyboardBasedGlobalAction.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

.field public static final enum BACK:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

.field public static final enum HOME:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

.field public static final enum NOTIFICATIONS:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

.field public static final enum QUICK_SETTINGS:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

.field public static final enum RECENTS:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;


# instance fields
.field private final mAction:I

.field private final mPreferenceKeyId:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 15
    new-instance v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    const-string v1, "BACK"

    const v2, 0x7f0601fc

    invoke-direct {v0, v1, v8, v2, v4}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->BACK:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    .line 16
    new-instance v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    const-string v1, "HOME"

    const v2, 0x7f0601fd

    invoke-direct {v0, v1, v4, v2, v5}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->HOME:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    .line 17
    new-instance v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    const-string v1, "NOTIFICATIONS"

    const v2, 0x7f0601fe

    invoke-direct {v0, v1, v5, v2, v7}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->NOTIFICATIONS:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    .line 19
    new-instance v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    const-string v1, "QUICK_SETTINGS"

    const v2, 0x7f0601ff

    const/4 v3, 0x5

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->QUICK_SETTINGS:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    .line 21
    new-instance v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    const-string v1, "RECENTS"

    const v2, 0x7f060200

    invoke-direct {v0, v1, v7, v2, v6}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->RECENTS:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    .line 14
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    sget-object v1, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->BACK:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    aput-object v1, v0, v8

    sget-object v1, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->HOME:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->NOTIFICATIONS:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->QUICK_SETTINGS:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->RECENTS:Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    aput-object v1, v0, v7

    sput-object v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->$VALUES:[Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "preferenceKeyId"    # I
    .param p4, "action"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput p3, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->mPreferenceKeyId:I

    .line 29
    iput p4, p0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->mAction:I

    .line 30
    return-void
.end method

.method public static getActionForKeyEvent(Landroid/content/Context;Landroid/view/KeyEvent;)Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 41
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v9

    .line 42
    .local v9, "prefs":Landroid/content/SharedPreferences;
    invoke-static {p1}, Lcom/googlecode/eyesfree/switchcontrol/KeyComboPreference;->keyEventToExtendedKeyCode(Landroid/view/KeyEvent;)J

    move-result-wide v2

    .line 43
    .local v2, "extendedKeyCode":J
    invoke-static {}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->values()[Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    move-result-object v1

    .local v1, "arr$":[Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v0, v1, v4

    .line 44
    .local v0, "action":Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;
    iget v10, v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->mPreferenceKeyId:I

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 45
    .local v8, "preferenceKey":Ljava/lang/String;
    const-wide/16 v10, -0x1

    invoke-interface {v9, v8, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 47
    .local v6, "keyCodeForAction":J
    cmp-long v10, v2, v6

    if-nez v10, :cond_0

    .line 52
    .end local v0    # "action":Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;
    .end local v6    # "keyCodeForAction":J
    .end local v8    # "preferenceKey":Ljava/lang/String;
    :goto_1
    return-object v0

    .line 43
    .restart local v0    # "action":Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;
    .restart local v6    # "keyCodeForAction":J
    .restart local v8    # "preferenceKey":Ljava/lang/String;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 52
    .end local v0    # "action":Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;
    .end local v6    # "keyCodeForAction":J
    .end local v8    # "preferenceKey":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static performActionForKeyEvent(Landroid/content/Context;Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;Landroid/view/KeyEvent;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "switchControlService"    # Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;
    .param p2, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 67
    invoke-static {p0, p2}, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->getActionForKeyEvent(Landroid/content/Context;Landroid/view/KeyEvent;)Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    move-result-object v0

    .line 68
    .local v0, "action":Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;
    if-nez v0, :cond_0

    .line 69
    const/4 v1, 0x0

    .line 76
    :goto_0
    return v1

    .line 72
    :cond_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    .line 73
    iget v1, v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->mAction:I

    invoke-virtual {p1, v1}, Lcom/googlecode/eyesfree/switchcontrol/SwitchControlService;->performGlobalAction(I)Z

    .line 76
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    return-object v0
.end method

.method public static values()[Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->$VALUES:[Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    invoke-virtual {v0}, [Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/googlecode/eyesfree/switchcontrol/KeyboardBasedGlobalAction;

    return-object v0
.end method
