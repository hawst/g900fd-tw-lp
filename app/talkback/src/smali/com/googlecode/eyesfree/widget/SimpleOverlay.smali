.class public Lcom/googlecode/eyesfree/widget/SimpleOverlay;
.super Ljava/lang/Object;
.source "SimpleOverlay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/widget/SimpleOverlay$SilentFrameLayout;,
        Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;
    }
.end annotation


# instance fields
.field private final mContentView:Landroid/view/ViewGroup;

.field private final mContext:Landroid/content/Context;

.field private final mId:I

.field private mKeyListener:Landroid/view/View$OnKeyListener;

.field private mListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

.field private final mParams:Landroid/view/WindowManager$LayoutParams;

.field private mTouchListener:Landroid/view/View$OnTouchListener;

.field private mVisible:Z

.field private final mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;-><init>(Landroid/content/Context;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # I

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContext:Landroid/content/Context;

    .line 68
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mWindowManager:Landroid/view/WindowManager;

    .line 69
    new-instance v0, Lcom/googlecode/eyesfree/widget/SimpleOverlay$1;

    invoke-direct {v0, p0, p1}, Lcom/googlecode/eyesfree/widget/SimpleOverlay$1;-><init>(Lcom/googlecode/eyesfree/widget/SimpleOverlay;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContentView:Landroid/view/ViewGroup;

    .line 90
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mParams:Landroid/view/WindowManager$LayoutParams;

    .line 91
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x7d3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 92
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 93
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 95
    iput p2, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mId:I

    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mVisible:Z

    .line 98
    return-void
.end method

.method static synthetic access$000(Lcom/googlecode/eyesfree/widget/SimpleOverlay;)Landroid/view/View$OnKeyListener;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mKeyListener:Landroid/view/View$OnKeyListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/googlecode/eyesfree/widget/SimpleOverlay;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContentView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$200(Lcom/googlecode/eyesfree/widget/SimpleOverlay;)Landroid/view/View$OnTouchListener;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mTouchListener:Landroid/view/View$OnTouchListener;

    return-object v0
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 258
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mId:I

    return v0
.end method

.method public getParams()Landroid/view/WindowManager$LayoutParams;
    .locals 2

    .prologue
    .line 198
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 199
    .local v0, "copy":Landroid/view/WindowManager$LayoutParams;
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mParams:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    .line 200
    return-object v0
.end method

.method public hide()V
    .locals 2

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mVisible:Z

    if-nez v0, :cond_0

    .line 178
    :goto_0
    return-void

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContentView:Landroid/view/ViewGroup;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 171
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mVisible:Z

    .line 173
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

    invoke-interface {v0, p0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;->onHide(Lcom/googlecode/eyesfree/widget/SimpleOverlay;)V

    .line 177
    :cond_1
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->onHide()V

    goto :goto_0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mVisible:Z

    return v0
.end method

.method protected onHide()V
    .locals 0

    .prologue
    .line 192
    return-void
.end method

.method protected onShow()V
    .locals 0

    .prologue
    .line 185
    return-void
.end method

.method public setContentView(I)V
    .locals 2
    .param p1, "layoutResId"    # I

    .prologue
    .line 229
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 230
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 231
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1
    .param p1, "content"    # Landroid/view/View;

    .prologue
    .line 239
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContentView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 240
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 241
    return-void
.end method

.method public setListener(Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

    .line 140
    return-void
.end method

.method public setParams(Landroid/view/WindowManager$LayoutParams;)V
    .locals 3
    .param p1, "params"    # Landroid/view/WindowManager$LayoutParams;

    .prologue
    .line 209
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mParams:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v0, p1}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    .line 211
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mVisible:Z

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContentView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 214
    :cond_0
    return-void
.end method

.method public show()V
    .locals 3

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mVisible:Z

    if-eqz v0, :cond_0

    .line 159
    :goto_0
    return-void

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContentView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 152
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mVisible:Z

    .line 154
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

    if-eqz v0, :cond_1

    .line 155
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

    invoke-interface {v0, p0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;->onShow(Lcom/googlecode/eyesfree/widget/SimpleOverlay;)V

    .line 158
    :cond_1
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->onShow()V

    goto :goto_0
.end method
