.class public Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;
.super Lcom/googlecode/eyesfree/widget/SimpleOverlay;
.source "RadialMenuOverlay.java"

# interfaces
.implements Landroid/content/DialogInterface;


# instance fields
.field private final mMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

.field private final mMenuView:Lcom/googlecode/eyesfree/widget/RadialMenuView;


# direct methods
.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "menuId"    # I
    .param p3, "useNodeProvider"    # Z

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;-><init>(Landroid/content/Context;I)V

    .line 42
    new-instance v0, Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-direct {v0, p1, p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;-><init>(Landroid/content/Context;Landroid/content/DialogInterface;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    .line 43
    new-instance v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-direct {v0, p1, v1, p3}, Lcom/googlecode/eyesfree/widget/RadialMenuView;-><init>(Landroid/content/Context;Lcom/googlecode/eyesfree/widget/RadialMenu;Z)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenuView:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    .line 45
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenuView:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    invoke-virtual {p0, v0}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->setContentView(Landroid/view/View;)V

    .line 46
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 0

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->dismiss()V

    .line 73
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onDismiss()V

    .line 78
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->hide()V

    .line 79
    return-void
.end method

.method public getMenu()Lcom/googlecode/eyesfree/widget/RadialMenu;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    return-object v0
.end method

.method public getView()Lcom/googlecode/eyesfree/widget/RadialMenuView;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenuView:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    return-object v0
.end method

.method public showWithDot()V
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->show()V

    .line 51
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onShow()V

    .line 52
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenuView:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->displayDot()V

    .line 53
    return-void
.end method
