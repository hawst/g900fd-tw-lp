.class Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;
.super Ljava/lang/Object;
.source "RadialMenuView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/widget/RadialMenuView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TouchedMenuItem"
.end annotation


# instance fields
.field private isDirectTouch:Z

.field private item:Lcom/googlecode/eyesfree/widget/RadialMenuItem;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 996
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 997
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->item:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .line 998
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->isDirectTouch:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/googlecode/eyesfree/widget/RadialMenuView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/googlecode/eyesfree/widget/RadialMenuView$1;

    .prologue
    .line 996
    invoke-direct {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;

    .prologue
    .line 996
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->item:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    return-object v0
.end method

.method static synthetic access$102(Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 0
    .param p0, "x0"    # Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;
    .param p1, "x1"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .prologue
    .line 996
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->item:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    return-object p1
.end method

.method static synthetic access$200(Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;

    .prologue
    .line 996
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->isDirectTouch:Z

    return v0
.end method

.method static synthetic access$202(Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;
    .param p1, "x1"    # Z

    .prologue
    .line 996
    iput-boolean p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->isDirectTouch:Z

    return p1
.end method
