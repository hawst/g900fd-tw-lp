.class public Lcom/googlecode/eyesfree/widget/RadialMenuView;
.super Landroid/view/SurfaceView;
.source "RadialMenuView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;,
        Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;,
        Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;
    }
.end annotation


# instance fields
.field private final mCachedCornerBound:Landroid/graphics/RectF;

.field private final mCachedCornerPath:Landroid/graphics/Path;

.field private final mCachedCornerPathReverse:Landroid/graphics/Path;

.field private mCachedCornerPathWidth:F

.field private final mCachedExtremeBound:Landroid/graphics/RectF;

.field private mCachedMenuSize:I

.field private final mCachedOuterBound:Landroid/graphics/RectF;

.field private final mCachedOuterPath:Landroid/graphics/Path;

.field private final mCachedOuterPathReverse:Landroid/graphics/Path;

.field private mCachedOuterPathWidth:F

.field private mCenter:Landroid/graphics/PointF;

.field private final mCenterFillColor:I

.field private final mCenterTextFillColor:I

.field private final mCornerFillColor:I

.field private final mCornerRadius:I

.field private final mCornerTextFillColor:I

.field private mDisplayWedges:Z

.field private final mDotFillColor:I

.field private final mDotPathEffect:Landroid/graphics/DashPathEffect;

.field private final mDotStrokeColor:I

.field private final mDotStrokeWidth:F

.field private final mEntryPoint:Landroid/graphics/PointF;

.field private final mExtremeRadius:I

.field private final mExtremeRadiusSq:I

.field private mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

.field private mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

.field private final mHandler:Lcom/googlecode/eyesfree/widget/LongPressHandler;

.field private mHolder:Landroid/view/SurfaceHolder;

.field private final mInnerRadius:I

.field private final mInnerRadiusSq:I

.field private final mLayoutListener:Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;

.field private final mLongPressListener:Lcom/googlecode/eyesfree/widget/LongPressHandler$LongPressListener;

.field private mMaybeSingleTap:Z

.field private final mOuterFillColor:I

.field private final mOuterRadius:I

.field private final mPaint:Landroid/graphics/Paint;

.field private final mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

.field private mRootMenuOffset:F

.field private final mSelectionColor:I

.field private final mSelectionShadowColor:I

.field private final mSelectionTextFillColor:I

.field private final mShadowRadius:I

.field private final mSingleTapRadiusSq:I

.field private final mSpacing:I

.field private mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

.field private final mSubMenuFilter:Landroid/graphics/ColorFilter;

.field private mSubMenuMode:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

.field private mSubMenuOffset:F

.field private final mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

.field private final mTempMatrix:Landroid/graphics/Matrix;

.field private final mTextFillColor:I

.field private final mTextShadowColor:I

.field private final mTextShadowRadius:I

.field private final mTextSize:I

.field private mTouchExplorer:Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;

.field private final mUseNodeProvider:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/googlecode/eyesfree/widget/RadialMenu;Z)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "menu"    # Lcom/googlecode/eyesfree/widget/RadialMenu;
    .param p3, "useNodeProvider"    # Z

    .prologue
    .line 193
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 73
    new-instance v7, Landroid/graphics/PointF;

    invoke-direct {v7}, Landroid/graphics/PointF;-><init>()V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mEntryPoint:Landroid/graphics/PointF;

    .line 76
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    .line 79
    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7}, Landroid/graphics/RectF;-><init>()V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterBound:Landroid/graphics/RectF;

    .line 80
    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7}, Landroid/graphics/RectF;-><init>()V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerBound:Landroid/graphics/RectF;

    .line 81
    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7}, Landroid/graphics/RectF;-><init>()V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedExtremeBound:Landroid/graphics/RectF;

    .line 84
    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPath:Landroid/graphics/Path;

    .line 85
    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathReverse:Landroid/graphics/Path;

    .line 86
    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPath:Landroid/graphics/Path;

    .line 87
    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathReverse:Landroid/graphics/Path;

    .line 136
    new-instance v7, Landroid/graphics/DashPathEffect;

    const/4 v8, 0x2

    new-array v8, v8, [F

    fill-array-data v8, :array_0

    const/4 v9, 0x0

    invoke-direct {v7, v8, v9}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDotPathEffect:Landroid/graphics/DashPathEffect;

    .line 138
    const/high16 v7, 0x40a00000    # 5.0f

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDotStrokeWidth:F

    .line 150
    sget-object v7, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;->LIFT_TO_ACTIVATE:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuMode:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    .line 153
    const/4 v7, 0x0

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedMenuSize:I

    .line 174
    const/4 v7, 0x0

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenuOffset:F

    .line 177
    new-instance v7, Landroid/graphics/PointF;

    invoke-direct {v7}, Landroid/graphics/PointF;-><init>()V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    .line 965
    new-instance v7, Lcom/googlecode/eyesfree/widget/RadialMenuView$1;

    invoke-direct {v7, p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView$1;-><init>(Lcom/googlecode/eyesfree/widget/RadialMenuView;)V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    .line 982
    new-instance v7, Lcom/googlecode/eyesfree/widget/RadialMenuView$2;

    invoke-direct {v7, p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView$2;-><init>(Lcom/googlecode/eyesfree/widget/RadialMenuView;)V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mLongPressListener:Lcom/googlecode/eyesfree/widget/LongPressHandler$LongPressListener;

    .line 989
    new-instance v7, Lcom/googlecode/eyesfree/widget/RadialMenuView$3;

    invoke-direct {v7, p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView$3;-><init>(Lcom/googlecode/eyesfree/widget/RadialMenuView;)V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mLayoutListener:Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;

    .line 195
    iput-object p2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    .line 196
    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    iget-object v8, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mLayoutListener:Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;

    invoke-virtual {v7, v8}, Lcom/googlecode/eyesfree/widget/RadialMenu;->setLayoutListener(Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;)V

    .line 198
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    .line 199
    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 201
    new-instance v7, Lcom/googlecode/eyesfree/widget/LongPressHandler;

    invoke-direct {v7, p1}, Lcom/googlecode/eyesfree/widget/LongPressHandler;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mHandler:Lcom/googlecode/eyesfree/widget/LongPressHandler;

    .line 202
    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mHandler:Lcom/googlecode/eyesfree/widget/LongPressHandler;

    iget-object v8, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mLongPressListener:Lcom/googlecode/eyesfree/widget/LongPressHandler$LongPressListener;

    invoke-virtual {v7, v8}, Lcom/googlecode/eyesfree/widget/LongPressHandler;->setListener(Lcom/googlecode/eyesfree/widget/LongPressHandler$LongPressListener;)V

    .line 204
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v4

    .line 205
    .local v4, "holder":Landroid/view/SurfaceHolder;
    const/4 v7, -0x3

    invoke-interface {v4, v7}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 206
    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v4, v7}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 208
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 209
    .local v5, "res":Landroid/content/res/Resources;
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 211
    .local v1, "config":Landroid/view/ViewConfiguration;
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSingleTapRadiusSq:I

    .line 214
    const v7, 0x7f0c0009

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mInnerRadius:I

    .line 215
    const v7, 0x7f0c000a

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mOuterRadius:I

    .line 216
    const v7, 0x7f0c000b

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerRadius:I

    .line 217
    const v7, 0x7f0c000c

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    .line 218
    const v7, 0x7f0c000d

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSpacing:I

    .line 219
    const v7, 0x7f0c000e

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextSize:I

    .line 220
    const v7, 0x7f0c000f

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextShadowRadius:I

    .line 221
    const v7, 0x7f0c0010

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mShadowRadius:I

    .line 224
    const v7, 0x7f07001c

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mOuterFillColor:I

    .line 225
    const v7, 0x7f07001d

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextFillColor:I

    .line 226
    const v7, 0x7f07001e

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerFillColor:I

    .line 227
    const v7, 0x7f07001f

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerTextFillColor:I

    .line 228
    const v7, 0x7f070026

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDotFillColor:I

    .line 229
    const v7, 0x7f070027

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDotStrokeColor:I

    .line 230
    const v7, 0x7f070022

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSelectionColor:I

    .line 231
    const v7, 0x7f070023

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSelectionTextFillColor:I

    .line 232
    const v7, 0x7f070024

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSelectionShadowColor:I

    .line 233
    const v7, 0x7f07001a

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenterFillColor:I

    .line 234
    const v7, 0x7f07001b

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenterTextFillColor:I

    .line 235
    const v7, 0x7f070028

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextShadowColor:I

    .line 238
    const v7, 0x7f070020

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 239
    .local v2, "gradientInnerColor":I
    const v7, 0x7f070021

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 240
    .local v3, "gradientOuterColor":I
    const/4 v7, 0x2

    new-array v0, v7, [I

    const/4 v7, 0x0

    aput v2, v0, v7

    const/4 v7, 0x1

    aput v3, v0, v7

    .line 241
    .local v0, "colors":[I
    new-instance v7, Landroid/graphics/drawable/GradientDrawable;

    sget-object v8, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    invoke-direct {v7, v8, v0}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

    .line 242
    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/graphics/drawable/GradientDrawable;->setGradientType(I)V

    .line 243
    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

    iget v8, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    int-to-float v8, v8

    const/high16 v9, 0x40000000    # 2.0f

    mul-float/2addr v8, v9

    invoke-virtual {v7, v8}, Landroid/graphics/drawable/GradientDrawable;->setGradientRadius(F)V

    .line 245
    const v7, 0x7f070025

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 248
    .local v6, "subMenuOverlayColor":I
    new-instance v7, Landroid/graphics/PorterDuffColorFilter;

    sget-object v8, Landroid/graphics/PorterDuff$Mode;->SCREEN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v7, v6, v8}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuFilter:Landroid/graphics/ColorFilter;

    .line 250
    iget v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mInnerRadius:I

    iget v8, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mInnerRadius:I

    mul-int/2addr v7, v8

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mInnerRadiusSq:I

    .line 251
    iget v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    iget v8, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    mul-int/2addr v7, v8

    iput v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadiusSq:I

    .line 253
    iput-boolean p3, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mUseNodeProvider:Z

    .line 255
    iget-boolean v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mUseNodeProvider:Z

    if-eqz v7, :cond_0

    .line 256
    new-instance v7, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;

    invoke-direct {v7, p0, p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;-><init>(Lcom/googlecode/eyesfree/widget/RadialMenuView;Landroid/view/View;)V

    iput-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTouchExplorer:Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;

    .line 257
    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTouchExplorer:Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;

    invoke-static {p0, v7}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    .line 261
    :cond_0
    invoke-direct {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->initializeCachedShapes()V

    .line 262
    return-void

    .line 136
    :array_0
    .array-data 4
        0x41a00000    # 20.0f
        0x41a00000    # 20.0f
    .end array-data
.end method

.method static synthetic access$302(Lcom/googlecode/eyesfree/widget/RadialMenuView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0
    .param p0, "x0"    # Lcom/googlecode/eyesfree/widget/RadialMenuView;
    .param p1, "x1"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mHolder:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic access$400(Lcom/googlecode/eyesfree/widget/RadialMenuView;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/widget/RadialMenuView;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    return-object v0
.end method

.method static synthetic access$500(Lcom/googlecode/eyesfree/widget/RadialMenuView;Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/googlecode/eyesfree/widget/RadialMenuView;
    .param p1, "x1"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->onItemLongPressed(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V

    return-void
.end method

.method static synthetic access$600(Lcom/googlecode/eyesfree/widget/RadialMenuView;)Lcom/googlecode/eyesfree/widget/RadialMenu;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/widget/RadialMenuView;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    return-object v0
.end method

.method private static arcLength(FF)F
    .locals 2
    .param p0, "angle"    # F
    .param p1, "radius"    # F

    .prologue
    .line 962
    const v0, 0x40c90fdb

    mul-float/2addr v0, p1

    const/high16 v1, 0x43b40000    # 360.0f

    div-float v1, p0, v1

    mul-float/2addr v0, v1

    return v0
.end method

.method private static contractBounds(Landroid/graphics/RectF;F)V
    .locals 1
    .param p0, "rect"    # Landroid/graphics/RectF;
    .param p1, "amount"    # F

    .prologue
    .line 934
    iget v0, p0, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, p1

    iput v0, p0, Landroid/graphics/RectF;->left:F

    .line 935
    iget v0, p0, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, p1

    iput v0, p0, Landroid/graphics/RectF;->top:F

    .line 936
    iget v0, p0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, p1

    iput v0, p0, Landroid/graphics/RectF;->right:F

    .line 937
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, p1

    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    .line 938
    return-void
.end method

.method private static createBounds(Landroid/graphics/RectF;II)V
    .locals 5
    .param p0, "target"    # Landroid/graphics/RectF;
    .param p1, "diameter"    # I
    .param p2, "radius"    # I

    .prologue
    .line 926
    int-to-float v3, p1

    const/high16 v4, 0x40000000    # 2.0f

    div-float v0, v3, v4

    .line 927
    .local v0, "center":F
    int-to-float v3, p2

    sub-float v1, v0, v3

    .line 928
    .local v1, "left":F
    int-to-float v3, p2

    add-float v2, v0, v3

    .line 930
    .local v2, "right":F
    invoke-virtual {p0, v1, v1, v2, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 931
    return-void
.end method

.method private static distSq(Landroid/graphics/PointF;FF)F
    .locals 4
    .param p0, "p"    # Landroid/graphics/PointF;
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 949
    iget v2, p0, Landroid/graphics/PointF;->x:F

    sub-float v0, p1, v2

    .line 950
    .local v0, "dX":F
    iget v2, p0, Landroid/graphics/PointF;->y:F

    sub-float v1, p2, v2

    .line 951
    .local v1, "dY":F
    mul-float v2, v0, v0

    mul-float v3, v1, v1

    add-float/2addr v2, v3

    return v2
.end method

.method private drawCancel(Landroid/graphics/Canvas;IIF)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "center"    # F

    .prologue
    .line 474
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iget v6, v0, Landroid/graphics/PointF;->x:F

    .line 475
    .local v6, "centerX":F
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iget v7, v0, Landroid/graphics/PointF;->y:F

    .line 476
    .local v7, "centerY":F
    iget v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mInnerRadius:I

    int-to-float v10, v0

    .line 477
    .local v10, "radius":F
    const/high16 v0, 0x40800000    # 4.0f

    div-float v9, v10, v0

    .line 479
    .local v9, "iconRadius":F
    new-instance v8, Landroid/graphics/RectF;

    sub-float v0, v6, v10

    sub-float v1, v7, v10

    add-float v2, v6, v10

    add-float v3, v7, v10

    invoke-direct {v8, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 483
    .local v8, "dotBounds":Landroid/graphics/RectF;
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    if-nez v0, :cond_0

    const/4 v11, 0x1

    .line 485
    .local v11, "selected":Z
    :goto_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 486
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    if-eqz v11, :cond_1

    iget v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSelectionColor:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 487
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mShadowRadius:I

    int-to-float v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    if-eqz v11, :cond_2

    iget v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSelectionShadowColor:I

    :goto_2
    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 489
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v0}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 490
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 492
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 493
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    if-eqz v11, :cond_3

    iget v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSelectionTextFillColor:I

    :goto_3
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 494
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 495
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 496
    sub-float v1, v6, v9

    sub-float v2, v7, v9

    add-float v3, v6, v9

    add-float v4, v7, v9

    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 498
    add-float v1, v6, v9

    sub-float v2, v7, v9

    sub-float v3, v6, v9

    add-float v4, v7, v9

    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 500
    return-void

    .line 483
    .end local v11    # "selected":Z
    :cond_0
    const/4 v11, 0x0

    goto :goto_0

    .line 486
    .restart local v11    # "selected":Z
    :cond_1
    iget v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenterFillColor:I

    goto :goto_1

    .line 487
    :cond_2
    iget v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextShadowColor:I

    goto :goto_2

    .line 493
    :cond_3
    iget v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenterTextFillColor:I

    goto :goto_3
.end method

.method private drawCenterDot(Landroid/graphics/Canvas;II)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v9, 0x0

    .line 448
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 449
    .local v2, "context":Landroid/content/Context;
    int-to-float v5, p2

    div-float v0, v5, v6

    .line 450
    .local v0, "centerX":F
    int-to-float v5, p3

    div-float v1, v5, v6

    .line 451
    .local v1, "centerY":F
    iget v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mInnerRadius:I

    int-to-float v4, v5

    .line 452
    .local v4, "radius":F
    new-instance v3, Landroid/graphics/RectF;

    sub-float v5, v0, v4

    sub-float v6, v1, v4

    add-float v7, v0, v4

    add-float v8, v1, v4

    invoke-direct {v3, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 455
    .local v3, "dotBounds":Landroid/graphics/RectF;
    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 456
    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDotFillColor:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 457
    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextShadowRadius:I

    int-to-float v6, v6

    iget v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextShadowColor:I

    invoke-virtual {v5, v6, v9, v9, v7}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 458
    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v5}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 459
    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/4 v6, 0x0

    invoke-virtual {v5, v9, v9, v9, v6}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 462
    const/high16 v5, 0x40200000    # 2.5f

    invoke-static {v3, v5}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->contractBounds(Landroid/graphics/RectF;F)V

    .line 464
    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/high16 v6, 0x40a00000    # 5.0f

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 465
    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 466
    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDotStrokeColor:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 467
    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDotPathEffect:Landroid/graphics/DashPathEffect;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 468
    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v5}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 469
    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 471
    return-void
.end method

.method private drawCorner(Landroid/graphics/Canvas;IIFI)V
    .locals 15
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "center"    # F
    .param p5, "i"    # I

    .prologue
    .line 552
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getCorner(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v14

    .line 553
    .local v14, "wedge":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    if-nez v14, :cond_0

    .line 602
    :goto_0
    return-void

    .line 557
    :cond_0
    invoke-static/range {p5 .. p5}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getCornerRotation(I)F

    move-result v11

    .line 558
    .local v11, "rotation":F
    invoke-static/range {p5 .. p5}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getCornerLocation(I)Landroid/graphics/PointF;

    move-result-object v8

    .line 559
    .local v8, "cornerLocation":Landroid/graphics/PointF;
    iget v2, v8, Landroid/graphics/PointF;->x:F

    move/from16 v0, p2

    int-to-float v4, v0

    mul-float v9, v2, v4

    .line 560
    .local v9, "cornerX":F
    iget v2, v8, Landroid/graphics/PointF;->y:F

    move/from16 v0, p3

    int-to-float v4, v0

    mul-float v10, v2, v4

    .line 561
    .local v10, "cornerY":F
    invoke-virtual {v14}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    .line 562
    .local v13, "title":Ljava/lang/String;
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {v14, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v12

    .line 565
    .local v12, "selected":Z
    invoke-virtual {v14}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->hasSubMenu()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 566
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuFilter:Landroid/graphics/ColorFilter;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 571
    :goto_1
    iput v11, v14, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->offset:F

    .line 573
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 574
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    move/from16 v0, p4

    move/from16 v1, p4

    invoke-virtual {v2, v11, v0, v1}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 575
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    sub-float v4, v9, p4

    sub-float v5, v10, p4

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 576
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 578
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 579
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    if-eqz v12, :cond_4

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSelectionColor:I

    :goto_2
    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 580
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mShadowRadius:I

    int-to-float v5, v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    if-eqz v12, :cond_5

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSelectionShadowColor:I

    :goto_3
    invoke-virtual {v4, v5, v6, v7, v2}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 582
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPath:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 583
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 585
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 586
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    if-eqz v12, :cond_6

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSelectionTextFillColor:I

    :goto_4
    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 587
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 588
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextSize:I

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 589
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextShadowRadius:I

    int-to-float v4, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextShadowColor:I

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 591
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathWidth:F

    invoke-static {v2, v13, v4}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getEllipsizedText(Landroid/graphics/Paint;Ljava/lang/String;F)Ljava/lang/String;

    move-result-object v3

    .line 594
    .local v3, "renderText":Ljava/lang/String;
    const/high16 v2, 0x42b40000    # 90.0f

    cmpg-float v2, v11, v2

    if-gez v2, :cond_1

    const/high16 v2, -0x3d4c0000    # -90.0f

    cmpl-float v2, v11, v2

    if-gtz v2, :cond_2

    :cond_1
    const/high16 v2, 0x43870000    # 270.0f

    cmpl-float v2, v11, v2

    if-lez v2, :cond_7

    .line 595
    :cond_2
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathReverse:Landroid/graphics/Path;

    const/4 v5, 0x0

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextSize:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v6, v2

    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawTextOnPath(Ljava/lang/String;Landroid/graphics/Path;FFLandroid/graphics/Paint;)V

    .line 600
    :goto_5
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 601
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto/16 :goto_0

    .line 568
    .end local v3    # "renderText":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto/16 :goto_1

    .line 579
    :cond_4
    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerFillColor:I

    goto/16 :goto_2

    .line 580
    :cond_5
    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextShadowColor:I

    goto/16 :goto_3

    .line 586
    :cond_6
    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerTextFillColor:I

    goto :goto_4

    .line 597
    .restart local v3    # "renderText":Ljava/lang/String;
    :cond_7
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPath:Landroid/graphics/Path;

    const/4 v5, 0x0

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextSize:I

    neg-int v2, v2

    int-to-float v6, v2

    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawTextOnPath(Ljava/lang/String;Landroid/graphics/Path;FFLandroid/graphics/Paint;)V

    goto :goto_5
.end method

.method private drawWedge(Landroid/graphics/Canvas;IIFILcom/googlecode/eyesfree/widget/RadialMenu;F)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "center"    # F
    .param p5, "i"    # I
    .param p6, "menu"    # Lcom/googlecode/eyesfree/widget/RadialMenu;
    .param p7, "degrees"    # F

    .prologue
    .line 504
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    if-eqz v2, :cond_1

    iget v8, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuOffset:F

    .line 506
    .local v8, "offset":F
    :goto_0
    move-object/from16 v0, p6

    move/from16 v1, p5

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v12

    .line 507
    .local v12, "wedge":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-virtual {v12}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    .line 508
    .local v11, "title":Ljava/lang/String;
    move/from16 v0, p5

    int-to-float v2, v0

    mul-float v2, v2, p7

    add-float v9, v2, v8

    .line 509
    .local v9, "rotation":F
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {v12, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    .line 512
    .local v10, "selected":Z
    invoke-virtual {v12}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->hasSubMenu()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 513
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuFilter:Landroid/graphics/ColorFilter;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 518
    :goto_1
    iput v9, v12, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->offset:F

    .line 520
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 521
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    move/from16 v0, p4

    move/from16 v1, p4

    invoke-virtual {v2, v9, v0, v1}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 522
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float v4, v4, p4

    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sub-float v5, v5, p4

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 523
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 525
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 526
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    if-eqz v10, :cond_3

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSelectionColor:I

    :goto_2
    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 527
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mShadowRadius:I

    int-to-float v5, v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    if-eqz v10, :cond_4

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSelectionShadowColor:I

    :goto_3
    invoke-virtual {v4, v5, v6, v7, v2}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 529
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPath:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 530
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 532
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 533
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    if-eqz v10, :cond_5

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSelectionTextFillColor:I

    :goto_4
    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 534
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 535
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextSize:I

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 536
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextShadowRadius:I

    int-to-float v4, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextShadowColor:I

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 538
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathWidth:F

    invoke-static {v2, v11, v4}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getEllipsizedText(Landroid/graphics/Paint;Ljava/lang/String;F)Ljava/lang/String;

    move-result-object v3

    .line 541
    .local v3, "renderText":Ljava/lang/String;
    const/high16 v2, 0x42b40000    # 90.0f

    cmpg-float v2, v9, v2

    if-ltz v2, :cond_0

    const/high16 v2, 0x43870000    # 270.0f

    cmpl-float v2, v9, v2

    if-lez v2, :cond_6

    .line 542
    :cond_0
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathReverse:Landroid/graphics/Path;

    const/4 v5, 0x0

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextSize:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v6, v2

    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawTextOnPath(Ljava/lang/String;Landroid/graphics/Path;FFLandroid/graphics/Paint;)V

    .line 547
    :goto_5
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 548
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 549
    return-void

    .line 504
    .end local v3    # "renderText":Ljava/lang/String;
    .end local v8    # "offset":F
    .end local v9    # "rotation":F
    .end local v10    # "selected":Z
    .end local v11    # "title":Ljava/lang/String;
    .end local v12    # "wedge":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :cond_1
    iget v8, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenuOffset:F

    goto/16 :goto_0

    .line 515
    .restart local v8    # "offset":F
    .restart local v9    # "rotation":F
    .restart local v10    # "selected":Z
    .restart local v11    # "title":Ljava/lang/String;
    .restart local v12    # "wedge":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :cond_2
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto/16 :goto_1

    .line 526
    :cond_3
    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mOuterFillColor:I

    goto/16 :goto_2

    .line 527
    :cond_4
    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextShadowColor:I

    goto/16 :goto_3

    .line 533
    :cond_5
    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextFillColor:I

    goto :goto_4

    .line 544
    .restart local v3    # "renderText":Ljava/lang/String;
    :cond_6
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPath:Landroid/graphics/Path;

    const/4 v5, 0x0

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextSize:I

    neg-int v2, v2

    int-to-float v6, v2

    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawTextOnPath(Ljava/lang/String;Landroid/graphics/Path;FFLandroid/graphics/Paint;)V

    goto :goto_5
.end method

.method private getClosestTouchedCorner(FFLcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;)Z
    .locals 11
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "result"    # Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;

    .prologue
    const/4 v8, 0x1

    .line 685
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getWidth()I

    move-result v7

    .line 686
    .local v7, "width":I
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getHeight()I

    move-result v6

    .line 689
    .local v6, "height":I
    const/4 v5, 0x0

    .local v5, "groupId":I
    :goto_0
    const/4 v9, 0x4

    if-ge v5, v9, :cond_2

    .line 690
    iget-object v9, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-virtual {v9, v5}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getCorner(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    .line 691
    .local v0, "corner":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    if-nez v0, :cond_1

    .line 689
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 695
    :cond_1
    invoke-static {v5}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getCornerLocation(I)Landroid/graphics/PointF;

    move-result-object v3

    .line 696
    .local v3, "cornerLocation":Landroid/graphics/PointF;
    iget v9, v3, Landroid/graphics/PointF;->x:F

    int-to-float v10, v7

    mul-float/2addr v9, v10

    sub-float v1, p1, v9

    .line 697
    .local v1, "cornerDX":F
    iget v9, v3, Landroid/graphics/PointF;->y:F

    int-to-float v10, v6

    mul-float/2addr v9, v10

    sub-float v2, p2, v9

    .line 698
    .local v2, "cornerDY":F
    mul-float v9, v1, v1

    mul-float v10, v2, v2

    add-float v4, v9, v10

    .line 702
    .local v4, "cornerTouchDistSq":F
    iget v9, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadiusSq:I

    int-to-float v9, v9

    cmpg-float v9, v4, v9

    if-gez v9, :cond_0

    .line 703
    # setter for: Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->item:Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-static {p3, v0}, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->access$102(Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .line 704
    # setter for: Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->isDirectTouch:Z
    invoke-static {p3, v8}, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->access$202(Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;Z)Z

    .line 709
    .end local v0    # "corner":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .end local v1    # "cornerDX":F
    .end local v2    # "cornerDY":F
    .end local v3    # "cornerLocation":Landroid/graphics/PointF;
    .end local v4    # "cornerTouchDistSq":F
    :goto_1
    return v8

    :cond_2
    const/4 v8, 0x0

    goto :goto_1
.end method

.method private getClosestTouchedWedge(FFFLcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;)Z
    .locals 18
    .param p1, "dX"    # F
    .param p2, "dY"    # F
    .param p3, "touchDistSq"    # F
    .param p4, "result"    # Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;

    .prologue
    .line 714
    move-object/from16 v0, p0

    iget v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mInnerRadiusSq:I

    int-to-float v13, v13

    cmpg-float v13, p3, v13

    if-gtz v13, :cond_0

    .line 716
    const/4 v13, 0x0

    .line 741
    :goto_0
    return v13

    .line 719
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    if-eqz v13, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    .line 720
    .local v4, "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    if-eqz v13, :cond_4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuOffset:F

    .line 723
    .local v5, "offset":F
    :goto_2
    move/from16 v0, p1

    float-to-double v14, v0

    move/from16 v0, p2

    float-to-double v0, v0

    move-wide/from16 v16, v0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    .line 724
    .local v2, "angle":D
    const-wide v14, 0x4076800000000000L    # 360.0

    invoke-virtual {v4}, Lcom/googlecode/eyesfree/widget/RadialMenu;->size()I

    move-result v13

    int-to-double v0, v13

    move-wide/from16 v16, v0

    div-double v10, v14, v16

    .line 725
    .local v10, "wedgeArc":D
    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double v14, v10, v14

    float-to-double v0, v5

    move-wide/from16 v16, v0

    sub-double v6, v14, v16

    .line 727
    .local v6, "offsetArc":D
    const-wide v14, 0x4066800000000000L    # 180.0

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v16

    sub-double v14, v14, v16

    add-double/2addr v14, v6

    const-wide v16, 0x4076800000000000L    # 360.0

    rem-double v8, v14, v16

    .line 729
    .local v8, "touchArc":D
    const-wide/16 v14, 0x0

    cmpg-double v13, v8, v14

    if-gez v13, :cond_1

    .line 730
    const-wide v14, 0x4076800000000000L    # 360.0

    add-double/2addr v8, v14

    .line 733
    :cond_1
    div-double v14, v8, v10

    double-to-int v12, v14

    .line 734
    .local v12, "wedgeNum":I
    if-ltz v12, :cond_2

    invoke-virtual {v4}, Lcom/googlecode/eyesfree/widget/RadialMenu;->size()I

    move-result v13

    if-le v12, v13, :cond_5

    .line 735
    :cond_2
    const/4 v13, 0x6

    const-string v14, "Invalid wedge index: %d"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14, v15}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 736
    const/4 v13, 0x0

    goto :goto_0

    .line 719
    .end local v2    # "angle":D
    .end local v4    # "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    .end local v5    # "offset":F
    .end local v6    # "offsetArc":D
    .end local v8    # "touchArc":D
    .end local v10    # "wedgeArc":D
    .end local v12    # "wedgeNum":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    goto :goto_1

    .line 720
    .restart local v4    # "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    :cond_4
    move-object/from16 v0, p0

    iget v5, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenuOffset:F

    goto :goto_2

    .line 739
    .restart local v2    # "angle":D
    .restart local v5    # "offset":F
    .restart local v6    # "offsetArc":D
    .restart local v8    # "touchArc":D
    .restart local v10    # "wedgeArc":D
    .restart local v12    # "wedgeNum":I
    :cond_5
    invoke-virtual {v4, v12}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v13

    move-object/from16 v0, p4

    # setter for: Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->item:Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-static {v0, v13}, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->access$102(Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .line 740
    move-object/from16 v0, p0

    iget v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadiusSq:I

    int-to-float v13, v13

    cmpg-float v13, p3, v13

    if-gez v13, :cond_6

    const/4 v13, 0x1

    :goto_3
    move-object/from16 v0, p4

    # setter for: Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->isDirectTouch:Z
    invoke-static {v0, v13}, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->access$202(Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;Z)Z

    .line 741
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 740
    :cond_6
    const/4 v13, 0x0

    goto :goto_3
.end method

.method private static getEllipsizedText(Landroid/graphics/Paint;Ljava/lang/String;F)Ljava/lang/String;
    .locals 8
    .param p0, "paint"    # Landroid/graphics/Paint;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "maxWidth"    # F

    .prologue
    const/4 v7, 0x0

    .line 905
    invoke-virtual {p0, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    .line 906
    .local v3, "textWidth":F
    cmpg-float v4, v3, p2

    if-gtz v4, :cond_0

    .line 922
    .end local p1    # "title":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 911
    .restart local p1    # "title":Ljava/lang/String;
    :cond_0
    const-string v4, "\u2026"

    invoke-virtual {p0, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    .line 912
    .local v0, "ellipsisWidth":F
    const/4 v4, 0x1

    sub-float v5, p2, v0

    const/4 v6, 0x0

    invoke-virtual {p0, p1, v4, v5, v6}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v1

    .line 916
    .local v1, "length":I
    const/16 v4, 0x20

    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v2

    .line 917
    .local v2, "space":I
    if-lez v2, :cond_1

    .line 918
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\u2026"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 922
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\u2026"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private getTouchedMenuItem(FF)Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 670
    new-instance v2, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;

    const/4 v4, 0x0

    invoke-direct {v2, v4}, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;-><init>(Lcom/googlecode/eyesfree/widget/RadialMenuView$1;)V

    .line 671
    .local v2, "result":Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float v0, p1, v4

    .line 672
    .local v0, "dX":F
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float v1, p2, v4

    .line 673
    .local v1, "dY":F
    mul-float v4, v0, v0

    mul-float v5, v1, v1

    add-float v3, v4, v5

    .line 675
    .local v3, "touchDistSq":F
    invoke-direct {p0, p1, p2, v2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getClosestTouchedCorner(FFLcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 681
    :cond_0
    :goto_0
    return-object v2

    .line 677
    :cond_1
    iget-boolean v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDisplayWedges:Z

    if-eqz v4, :cond_0

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getClosestTouchedWedge(FFFLcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_0
.end method

.method private initializeCachedShapes()V
    .locals 13

    .prologue
    const/high16 v12, 0x42b40000    # 90.0f

    const/high16 v11, 0x42340000    # 45.0f

    const/high16 v10, -0x3cf90000    # -135.0f

    const/high16 v9, -0x3d4c0000    # -90.0f

    const/high16 v8, -0x3dcc0000    # -45.0f

    .line 349
    iget v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    mul-int/lit8 v5, v6, 0x2

    .line 351
    .local v5, "diameter":I
    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterBound:Landroid/graphics/RectF;

    iget v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mOuterRadius:I

    invoke-static {v6, v5, v7}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->createBounds(Landroid/graphics/RectF;II)V

    .line 352
    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerBound:Landroid/graphics/RectF;

    iget v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerRadius:I

    invoke-static {v6, v5, v7}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->createBounds(Landroid/graphics/RectF;II)V

    .line 353
    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedExtremeBound:Landroid/graphics/RectF;

    iget v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    invoke-static {v6, v5, v7}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->createBounds(Landroid/graphics/RectF;II)V

    .line 355
    const/high16 v4, 0x42b40000    # 90.0f

    .line 356
    .local v4, "cornerWedgeArc":F
    const/high16 v2, 0x43070000    # 135.0f

    .line 357
    .local v2, "cornerOffsetArc":F
    const/high16 v1, -0x3dcc0000    # -45.0f

    .line 358
    .local v1, "cornerLeft":F
    const/high16 v0, -0x3d4c0000    # -90.0f

    .line 359
    .local v0, "cornerCenter":F
    const/high16 v3, -0x3cf90000    # -135.0f

    .line 362
    .local v3, "cornerRight":F
    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPath:Landroid/graphics/Path;

    invoke-virtual {v6}, Landroid/graphics/Path;->rewind()V

    .line 363
    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPath:Landroid/graphics/Path;

    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerBound:Landroid/graphics/RectF;

    invoke-virtual {v6, v7, v9, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 364
    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPath:Landroid/graphics/Path;

    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedExtremeBound:Landroid/graphics/RectF;

    invoke-virtual {v6, v7, v8, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 365
    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPath:Landroid/graphics/Path;

    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerBound:Landroid/graphics/RectF;

    invoke-virtual {v6, v7, v10, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 366
    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPath:Landroid/graphics/Path;

    invoke-virtual {v6}, Landroid/graphics/Path;->close()V

    .line 368
    iget v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    int-to-float v6, v6

    invoke-static {v12, v6}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->arcLength(FF)F

    move-result v6

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathWidth:F

    .line 371
    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathReverse:Landroid/graphics/Path;

    invoke-virtual {v6}, Landroid/graphics/Path;->rewind()V

    .line 372
    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathReverse:Landroid/graphics/Path;

    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerBound:Landroid/graphics/RectF;

    invoke-virtual {v6, v7, v9, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 374
    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathReverse:Landroid/graphics/Path;

    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedExtremeBound:Landroid/graphics/RectF;

    invoke-virtual {v6, v7, v10, v12}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 376
    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathReverse:Landroid/graphics/Path;

    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerBound:Landroid/graphics/RectF;

    invoke-virtual {v6, v7, v8, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 377
    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathReverse:Landroid/graphics/Path;

    invoke-virtual {v6}, Landroid/graphics/Path;->close()V

    .line 378
    return-void
.end method

.method private invalidateCachedWedgeShapes()V
    .locals 18

    .prologue
    .line 306
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    .line 307
    .local v5, "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    :goto_0
    invoke-virtual {v5}, Lcom/googlecode/eyesfree/widget/RadialMenu;->size()I

    move-result v6

    .line 308
    .local v6, "menuSize":I
    if-gtz v6, :cond_1

    .line 339
    :goto_1
    return-void

    .line 306
    .end local v5    # "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    .end local v6    # "menuSize":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    goto :goto_0

    .line 312
    .restart local v5    # "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    .restart local v6    # "menuSize":I
    :cond_1
    const/high16 v13, 0x43b40000    # 360.0f

    int-to-float v14, v6

    div-float v12, v13, v14

    .line 313
    .local v12, "wedgeArc":F
    const/high16 v13, 0x40000000    # 2.0f

    div-float v13, v12, v13

    const/high16 v14, 0x42b40000    # 90.0f

    add-float v7, v13, v14

    .line 314
    .local v7, "offsetArc":F
    move-object/from16 v0, p0

    iget v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSpacing:I

    int-to-double v14, v13

    move-object/from16 v0, p0

    iget v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mOuterRadius:I

    int-to-double v0, v13

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->tan(D)D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v14

    double-to-float v10, v14

    .line 316
    .local v10, "spacingArc":F
    move-object/from16 v0, p0

    iget v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSpacing:I

    int-to-double v14, v13

    move-object/from16 v0, p0

    iget v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    int-to-double v0, v13

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->tan(D)D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v14

    double-to-float v11, v14

    .line 318
    .local v11, "spacingExtremeArc":F
    sub-float v13, v12, v10

    sub-float v3, v13, v7

    .line 319
    .local v3, "left":F
    sub-float v13, v12, v11

    sub-float v4, v13, v7

    .line 320
    .local v4, "leftExtreme":F
    const/high16 v13, 0x40000000    # 2.0f

    div-float v13, v12, v13

    sub-float v2, v13, v7

    .line 321
    .local v2, "center":F
    sub-float v8, v10, v7

    .line 322
    .local v8, "right":F
    sub-float v9, v11, v7

    .line 325
    .local v9, "rightExtreme":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPath:Landroid/graphics/Path;

    invoke-virtual {v13}, Landroid/graphics/Path;->rewind()V

    .line 326
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterBound:Landroid/graphics/RectF;

    sub-float v15, v3, v2

    invoke-virtual {v13, v14, v2, v15}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 327
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedExtremeBound:Landroid/graphics/RectF;

    sub-float v15, v8, v3

    invoke-virtual {v13, v14, v3, v15}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 328
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterBound:Landroid/graphics/RectF;

    sub-float v15, v2, v8

    invoke-virtual {v13, v14, v8, v15}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 329
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPath:Landroid/graphics/Path;

    invoke-virtual {v13}, Landroid/graphics/Path;->close()V

    .line 331
    sub-float v13, v3, v8

    move-object/from16 v0, p0

    iget v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    int-to-float v14, v14

    invoke-static {v13, v14}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->arcLength(FF)F

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathWidth:F

    .line 334
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathReverse:Landroid/graphics/Path;

    invoke-virtual {v13}, Landroid/graphics/Path;->rewind()V

    .line 335
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathReverse:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterBound:Landroid/graphics/RectF;

    sub-float v15, v8, v2

    invoke-virtual {v13, v14, v2, v15}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 336
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathReverse:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedExtremeBound:Landroid/graphics/RectF;

    sub-float v15, v3, v8

    invoke-virtual {v13, v14, v8, v15}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 337
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathReverse:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterBound:Landroid/graphics/RectF;

    sub-float v15, v2, v3

    invoke-virtual {v13, v14, v3, v15}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 338
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathReverse:Landroid/graphics/Path;

    invoke-virtual {v13}, Landroid/graphics/Path;->close()V

    goto/16 :goto_1
.end method

.method private onEnter(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 751
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mEntryPoint:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 752
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mMaybeSingleTap:Z

    .line 753
    return-void
.end method

.method private onItemFocused(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V
    .locals 3
    .param p1, "item"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .prologue
    const/4 v2, 0x0

    .line 824
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    if-ne v1, p1, :cond_0

    .line 844
    :goto_0
    return-void

    .line 828
    :cond_0
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    .line 830
    .local v0, "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    :goto_1
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .line 832
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->invalidate()V

    .line 834
    if-nez p1, :cond_2

    .line 835
    invoke-virtual {v0, v2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->clearSelection(I)Z

    .line 843
    :goto_2
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->sendAccessibilityEvent(I)V

    goto :goto_0

    .line 828
    .end local v0    # "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    :cond_1
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    goto :goto_1

    .line 836
    .restart local v0    # "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    :cond_2
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isCorner()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 838
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-virtual {v1, p1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->selectMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    goto :goto_2

    .line 840
    :cond_3
    invoke-virtual {v0, p1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->selectMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    goto :goto_2
.end method

.method private onItemLongPressed(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V
    .locals 2
    .param p1, "item"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .prologue
    .line 854
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuMode:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    sget-object v1, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;->LONG_PRESS:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    if-ne v0, v1, :cond_0

    .line 855
    if-eqz p1, :cond_1

    .line 856
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->hasSubMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 857
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getSubMenu()Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v0

    iget v1, p1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->offset:F

    invoke-direct {p0, v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->setSubMenu(Lcom/googlecode/eyesfree/widget/RadialSubMenu;F)V

    .line 864
    :cond_0
    :goto_0
    return-void

    .line 859
    :cond_1
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    if-eqz v0, :cond_0

    .line 861
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->setSubMenu(Lcom/googlecode/eyesfree/widget/RadialSubMenu;F)V

    goto :goto_0
.end method

.method private onItemSelected(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V
    .locals 4
    .param p1, "item"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 873
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    .line 875
    .local v0, "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    :goto_0
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .line 877
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->invalidate()V

    .line 879
    if-nez p1, :cond_1

    .line 880
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->performMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    .line 901
    :goto_1
    invoke-virtual {p0, v3}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->sendAccessibilityEvent(I)V

    .line 902
    return-void

    .line 873
    .end local v0    # "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    goto :goto_0

    .line 881
    .restart local v0    # "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    :cond_1
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->hasSubMenu()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 882
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getSubMenu()Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v1

    iget v2, p1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->offset:F

    invoke-direct {p0, v1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->setSubMenu(Lcom/googlecode/eyesfree/widget/RadialSubMenu;F)V

    .line 886
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isCorner()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 888
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-virtual {v1, p1, v3}, Lcom/googlecode/eyesfree/widget/RadialMenu;->performMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    goto :goto_1

    .line 890
    :cond_2
    invoke-virtual {v0, p1, v3}, Lcom/googlecode/eyesfree/widget/RadialMenu;->performMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    goto :goto_1

    .line 893
    :cond_3
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isCorner()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 895
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-virtual {v1, p1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->performMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    goto :goto_1

    .line 897
    :cond_4
    invoke-virtual {v0, p1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->performMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    goto :goto_1
.end method

.method private onMove(FF)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 762
    iget-boolean v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mMaybeSingleTap:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mEntryPoint:Landroid/graphics/PointF;

    invoke-static {v1, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->distSq(Landroid/graphics/PointF;FF)F

    move-result v1

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSingleTapRadiusSq:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 763
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mMaybeSingleTap:Z

    .line 766
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getTouchedMenuItem(FF)Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;

    move-result-object v0

    .line 770
    .local v0, "touchedItem":Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;
    iget-boolean v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mMaybeSingleTap:Z

    if-eqz v1, :cond_1

    # getter for: Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->item:Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-static {v0}, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->access$100(Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v1

    if-eqz v1, :cond_1

    # getter for: Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->isDirectTouch:Z
    invoke-static {v0}, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->access$200(Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 771
    :cond_1
    # getter for: Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->item:Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-static {v0}, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->access$100(Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->onItemFocused(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V

    .line 775
    :cond_2
    # getter for: Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->item:Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-static {v0}, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->access$100(Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v1

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDisplayWedges:Z

    if-nez v1, :cond_3

    .line 776
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDisplayWedges:Z

    .line 777
    invoke-virtual {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->displayAt(FF)V

    .line 779
    :cond_3
    return-void
.end method

.method private onUp(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 788
    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getTouchedMenuItem(FF)Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;

    move-result-object v0

    .line 792
    .local v0, "touchedItem":Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;
    iget-boolean v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mMaybeSingleTap:Z

    if-eqz v1, :cond_0

    # getter for: Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->item:Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-static {v0}, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->access$100(Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v1

    if-eqz v1, :cond_0

    # getter for: Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->isDirectTouch:Z
    invoke-static {v0}, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->access$200(Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 793
    :cond_0
    # getter for: Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->item:Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-static {v0}, Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;->access$100(Lcom/googlecode/eyesfree/widget/RadialMenuView$TouchedMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->onItemSelected(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V

    .line 795
    :cond_1
    return-void
.end method

.method private setSubMenu(Lcom/googlecode/eyesfree/widget/RadialSubMenu;F)V
    .locals 2
    .param p1, "subMenu"    # Lcom/googlecode/eyesfree/widget/RadialSubMenu;
    .param p2, "offset"    # F

    .prologue
    .line 804
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    .line 805
    iput p2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuOffset:F

    .line 807
    invoke-direct {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->invalidateCachedWedgeShapes()V

    .line 808
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->invalidate()V

    .line 809
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->onShow()V

    .line 811
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuMode:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    sget-object v1, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;->LONG_PRESS:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    if-ne v0, v1, :cond_0

    .line 812
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->getItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->onItemFocused(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V

    .line 814
    :cond_0
    return-void
.end method


# virtual methods
.method public displayAt(FF)V
    .locals 2
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F

    .prologue
    const/4 v1, 0x0

    .line 292
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iput p1, v0, Landroid/graphics/PointF;->x:F

    .line 293
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iput p2, v0, Landroid/graphics/PointF;->y:F

    .line 295
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDisplayWedges:Z

    .line 296
    iput-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    .line 297
    iput-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .line 299
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->invalidate()V

    .line 300
    return-void
.end method

.method public displayDot()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 278
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDisplayWedges:Z

    .line 279
    iput-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    .line 280
    iput-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .line 282
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->invalidate()V

    .line 283
    return-void
.end method

.method public invalidate()V
    .locals 14

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v13, 0x0

    .line 382
    invoke-super {p0}, Landroid/view/SurfaceView;->invalidate()V

    .line 384
    iget-object v8, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mHolder:Landroid/view/SurfaceHolder;

    .line 385
    .local v8, "holder":Landroid/view/SurfaceHolder;
    if-nez v8, :cond_1

    .line 445
    :cond_0
    :goto_0
    return-void

    .line 389
    :cond_1
    invoke-interface {v8}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v1

    .line 390
    .local v1, "canvas":Landroid/graphics/Canvas;
    if-eqz v1, :cond_0

    .line 395
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v13, v0}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 397
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    .line 398
    invoke-interface {v8, v1}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 402
    :cond_2
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getWidth()I

    move-result v2

    .line 403
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getHeight()I

    move-result v3

    .line 405
    .local v3, "height":I
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDisplayWedges:Z

    if-nez v0, :cond_3

    .line 406
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    int-to-float v10, v2

    div-float/2addr v10, v11

    iput v10, v0, Landroid/graphics/PointF;->x:F

    .line 407
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    int-to-float v10, v3

    div-float/2addr v10, v11

    iput v10, v0, Landroid/graphics/PointF;->y:F

    .line 411
    :cond_3
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

    iget-object v10, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    int-to-float v11, v2

    div-float/2addr v10, v11

    iget-object v11, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->y:F

    int-to-float v12, v3

    div-float/2addr v11, v12

    invoke-virtual {v0, v10, v11}, Landroid/graphics/drawable/GradientDrawable;->setGradientCenter(FF)V

    .line 412
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v13, v13, v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    .line 413
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 415
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    if-eqz v0, :cond_5

    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    .line 416
    .local v6, "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    :goto_1
    iget v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    int-to-float v4, v0

    .line 418
    .local v4, "center":F
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDisplayWedges:Z

    if-eqz v0, :cond_6

    .line 419
    invoke-virtual {v6}, Lcom/googlecode/eyesfree/widget/RadialMenu;->size()I

    move-result v9

    .line 420
    .local v9, "wedges":I
    const/high16 v0, 0x43b40000    # 360.0f

    int-to-float v10, v9

    div-float v7, v0, v10

    .line 423
    .local v7, "degrees":F
    iget v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedMenuSize:I

    invoke-virtual {v6}, Lcom/googlecode/eyesfree/widget/RadialMenu;->size()I

    move-result v10

    if-eq v0, v10, :cond_4

    .line 424
    invoke-direct {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->invalidateCachedWedgeShapes()V

    .line 428
    :cond_4
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->drawCancel(Landroid/graphics/Canvas;IIF)V

    .line 431
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    if-ge v5, v9, :cond_7

    move-object v0, p0

    .line 432
    invoke-direct/range {v0 .. v7}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->drawWedge(Landroid/graphics/Canvas;IIFILcom/googlecode/eyesfree/widget/RadialMenu;F)V

    .line 431
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 415
    .end local v4    # "center":F
    .end local v5    # "i":I
    .end local v6    # "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    .end local v7    # "degrees":F
    .end local v9    # "wedges":I
    :cond_5
    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    goto :goto_1

    .line 436
    .restart local v4    # "center":F
    .restart local v6    # "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    :cond_6
    invoke-direct {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->drawCenterDot(Landroid/graphics/Canvas;II)V

    .line 440
    :cond_7
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_3
    const/4 v0, 0x4

    if-ge v5, v0, :cond_8

    move-object v0, p0

    .line 441
    invoke-direct/range {v0 .. v5}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->drawCorner(Landroid/graphics/Canvas;IIFI)V

    .line 440
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 444
    :cond_8
    invoke-interface {v8, v1}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 629
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mUseNodeProvider:Z

    if-eqz v0, :cond_0

    .line 630
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 632
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 616
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 617
    .local v4, "widthMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 618
    .local v5, "widthSize":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 619
    .local v0, "heightMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 620
    .local v1, "heightSize":I
    if-nez v4, :cond_0

    const/16 v3, 0x140

    .line 621
    .local v3, "measuredWidth":I
    :goto_0
    if-nez v0, :cond_1

    const/16 v2, 0x1e0

    .line 623
    .local v2, "measuredHeight":I
    :goto_1
    invoke-virtual {p0, v3, v2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->setMeasuredDimension(II)V

    .line 624
    return-void

    .end local v2    # "measuredHeight":I
    .end local v3    # "measuredWidth":I
    :cond_0
    move v3, v5

    .line 620
    goto :goto_0

    .restart local v3    # "measuredWidth":I
    :cond_1
    move v2, v1

    .line 621
    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 638
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 653
    :pswitch_0
    const/4 v0, 0x0

    .line 658
    :goto_0
    return v0

    .line 642
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->onEnter(FF)V

    .line 645
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->onMove(FF)V

    .line 656
    :goto_1
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mHandler:Lcom/googlecode/eyesfree/widget/LongPressHandler;

    invoke-virtual {v0, p0, p1}, Lcom/googlecode/eyesfree/widget/LongPressHandler;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 658
    const/4 v0, 0x1

    goto :goto_0

    .line 649
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->onUp(FF)V

    goto :goto_1

    .line 638
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setSubMenuMode(Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;)V
    .locals 0
    .param p1, "subMenuMode"    # Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    .prologue
    .line 270
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuMode:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    .line 271
    return-void
.end method
