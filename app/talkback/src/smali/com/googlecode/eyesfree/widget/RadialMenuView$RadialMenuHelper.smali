.class Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;
.super Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper;
.source "RadialMenuView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/widget/RadialMenuView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RadialMenuHelper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper",
        "<",
        "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final mTempRect:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/googlecode/eyesfree/widget/RadialMenuView;


# direct methods
.method public constructor <init>(Lcom/googlecode/eyesfree/widget/RadialMenuView;Landroid/view/View;)V
    .locals 1
    .param p2, "parentView"    # Landroid/view/View;

    .prologue
    .line 1004
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->this$0:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    .line 1005
    invoke-direct {p0, p2}, Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper;-><init>(Landroid/view/View;)V

    .line 1002
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->mTempRect:Landroid/graphics/Rect;

    .line 1006
    return-void
.end method


# virtual methods
.method protected getItemForVirtualViewId(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 1044
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->this$0:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    # getter for: Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;
    invoke-static {v0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->access$600(Lcom/googlecode/eyesfree/widget/RadialMenuView;)Lcom/googlecode/eyesfree/widget/RadialMenu;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic getItemForVirtualViewId(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 1001
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->getItemForVirtualViewId(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method protected getVirtualViewIdForItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)I
    .locals 1
    .param p1, "item"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .prologue
    .line 1049
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->this$0:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    # getter for: Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;
    invoke-static {v0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->access$600(Lcom/googlecode/eyesfree/widget/RadialMenuView;)Lcom/googlecode/eyesfree/widget/RadialMenu;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->indexOf(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)I

    move-result v0

    return v0
.end method

.method protected bridge synthetic getVirtualViewIdForItem(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1001
    check-cast p1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->getVirtualViewIdForItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)I

    move-result v0

    return v0
.end method

.method protected getVisibleItems(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1059
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/widget/RadialMenuItem;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->this$0:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    # getter for: Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;
    invoke-static {v2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->access$600(Lcom/googlecode/eyesfree/widget/RadialMenuView;)Lcom/googlecode/eyesfree/widget/RadialMenu;

    move-result-object v2

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1060
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->this$0:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    # getter for: Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;
    invoke-static {v2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->access$600(Lcom/googlecode/eyesfree/widget/RadialMenuView;)Lcom/googlecode/eyesfree/widget/RadialMenu;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v1

    .line 1061
    .local v1, "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1059
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1063
    .end local v1    # "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :cond_0
    return-void
.end method

.method protected performActionForItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;ILandroid/os/Bundle;)Z
    .locals 1
    .param p1, "item"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .param p2, "action"    # I
    .param p3, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 1033
    packed-switch p2, :pswitch_data_0

    .line 1039
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1035
    :pswitch_0
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->onClickPerformed()Z

    .line 1036
    const/4 v0, 0x1

    goto :goto_0

    .line 1033
    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic performActionForItem(Ljava/lang/Object;ILandroid/os/Bundle;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # I
    .param p3, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 1001
    check-cast p1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3}, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->performActionForItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method protected populateEventForItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "item"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 1026
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1027
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isChecked()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    .line 1028
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isEnabled()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 1029
    return-void
.end method

.method protected bridge synthetic populateEventForItem(Ljava/lang/Object;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 1001
    check-cast p1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->populateEventForItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method protected populateNodeForItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 2
    .param p1, "item"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 1010
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1011
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isVisible()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setVisibleToUser(Z)V

    .line 1012
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isCheckable()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setCheckable(Z)V

    .line 1013
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isChecked()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setChecked(Z)V

    .line 1014
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isEnabled()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setEnabled(Z)V

    .line 1015
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setClickable(Z)V

    .line 1017
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->this$0:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1018
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInParent(Landroid/graphics/Rect;)V

    .line 1020
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->this$0:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1021
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInScreen(Landroid/graphics/Rect;)V

    .line 1022
    return-void
.end method

.method protected bridge synthetic populateNodeForItem(Ljava/lang/Object;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 1001
    check-cast p1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->populateNodeForItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    return-void
.end method
