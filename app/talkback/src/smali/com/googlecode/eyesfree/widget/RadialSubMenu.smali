.class public Lcom/googlecode/eyesfree/widget/RadialSubMenu;
.super Lcom/googlecode/eyesfree/widget/RadialMenu;
.source "RadialSubMenu.java"

# interfaces
.implements Landroid/view/SubMenu;


# instance fields
.field private final mMenuItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

.field private final mParentMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/content/DialogInterface;Lcom/googlecode/eyesfree/widget/RadialMenu;IIILjava/lang/CharSequence;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parent"    # Landroid/content/DialogInterface;
    .param p3, "parentMenu"    # Lcom/googlecode/eyesfree/widget/RadialMenu;
    .param p4, "groupId"    # I
    .param p5, "itemId"    # I
    .param p6, "order"    # I
    .param p7, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenu;-><init>(Landroid/content/Context;Landroid/content/DialogInterface;)V

    .line 47
    iput-object p3, p0, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->mParentMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    .line 48
    new-instance v0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-object v1, p1

    move v2, p4

    move v3, p5

    move v4, p6

    move-object v5, p7

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;Lcom/googlecode/eyesfree/widget/RadialSubMenu;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->mMenuItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .line 49
    return-void
.end method


# virtual methods
.method public clearHeader()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public bridge synthetic getItem()Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->getItem()Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public getItem()Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->mMenuItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    return-object v0
.end method

.method public selectMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z
    .locals 2
    .param p1, "item"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .param p2, "flags"    # I

    .prologue
    const/4 v0, 0x1

    .line 53
    invoke-super {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->selectMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 61
    :cond_0
    :goto_0
    return v0

    .line 57
    :cond_1
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->mParentMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->mParentMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-virtual {v1, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->selectMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 61
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setHeaderIcon(I)Landroid/view/SubMenu;
    .locals 1
    .param p1, "iconRes"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->mMenuItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 78
    return-object p0
.end method

.method public setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1
    .param p1, "icon"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->mMenuItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 85
    return-object p0
.end method

.method public setHeaderTitle(I)Landroid/view/SubMenu;
    .locals 1
    .param p1, "titleRes"    # I

    .prologue
    .line 90
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->mMenuItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 92
    return-object p0
.end method

.method public setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->mMenuItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 99
    return-object p0
.end method

.method public setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 104
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setIcon(I)Landroid/view/SubMenu;
    .locals 1
    .param p1, "iconRes"    # I

    .prologue
    .line 109
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->mMenuItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 111
    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1
    .param p1, "icon"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->mMenuItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 118
    return-object p0
.end method
