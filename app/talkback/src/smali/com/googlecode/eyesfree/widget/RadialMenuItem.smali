.class public Lcom/googlecode/eyesfree/widget/RadialMenuItem;
.super Ljava/lang/Object;
.source "RadialMenuItem.java"

# interfaces
.implements Landroid/view/MenuItem;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/widget/RadialMenuItem$1;,
        Lcom/googlecode/eyesfree/widget/RadialMenuItem$RadialContextMenuInfo;,
        Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;
    }
.end annotation


# instance fields
.field private mAlphaShortcut:C

.field private mCheckable:Z

.field private mChecked:Z

.field private final mContext:Landroid/content/Context;

.field private mCorner:Z

.field private mEnabled:Z

.field private mGroupId:I

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mIntent:Landroid/content/Intent;

.field private mItemId:I

.field private mListener:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private final mMenuInfo:Lcom/googlecode/eyesfree/widget/RadialMenuItem$RadialContextMenuInfo;

.field private mNumericShortcut:C

.field private mOrder:I

.field private mSelectionListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

.field private mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

.field private mTitle:Ljava/lang/CharSequence;

.field private mTitleCondensed:Ljava/lang/CharSequence;

.field private mVisible:Z

.field offset:F


# direct methods
.method public constructor <init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "groupId"    # I
    .param p3, "itemId"    # I
    .param p4, "order"    # I
    .param p5, "title"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mContext:Landroid/content/Context;

    .line 83
    new-instance v0, Lcom/googlecode/eyesfree/widget/RadialMenuItem$RadialContextMenuInfo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem$RadialContextMenuInfo;-><init>(Lcom/googlecode/eyesfree/widget/RadialMenuItem$1;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mMenuInfo:Lcom/googlecode/eyesfree/widget/RadialMenuItem$RadialContextMenuInfo;

    .line 84
    iput p2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mGroupId:I

    .line 85
    iput p3, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mItemId:I

    .line 86
    iput p4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mOrder:I

    .line 87
    iput-object p5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mTitle:Ljava/lang/CharSequence;

    .line 90
    iput-boolean v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mVisible:Z

    .line 91
    iput-boolean v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mCheckable:Z

    .line 92
    iput-boolean v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mChecked:Z

    .line 93
    iput-boolean v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mEnabled:Z

    .line 94
    return-void
.end method

.method constructor <init>(Landroid/content/Context;IIILjava/lang/CharSequence;Lcom/googlecode/eyesfree/widget/RadialSubMenu;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "groupId"    # I
    .param p3, "itemId"    # I
    .param p4, "order"    # I
    .param p5, "title"    # Ljava/lang/CharSequence;
    .param p6, "subMenu"    # Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    .prologue
    .line 67
    invoke-direct/range {p0 .. p5}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 69
    iput-object p6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    .line 70
    return-void
.end method


# virtual methods
.method public collapseActionView()Z
    .locals 1

    .prologue
    .line 342
    const/4 v0, 0x0

    return v0
.end method

.method public expandActionView()Z
    .locals 1

    .prologue
    .line 347
    const/4 v0, 0x0

    return v0
.end method

.method public getActionProvider()Landroid/view/ActionProvider;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 353
    const/4 v0, 0x0

    return-object v0
.end method

.method public getActionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 358
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAlphabeticShortcut()C
    .locals 1

    .prologue
    .line 98
    iget-char v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mAlphaShortcut:C

    return v0
.end method

.method public getGroupId()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mGroupId:I

    return v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getItemId()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mItemId:I

    return v0
.end method

.method public getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mMenuInfo:Lcom/googlecode/eyesfree/widget/RadialMenuItem$RadialContextMenuInfo;

    return-object v0
.end method

.method public getNumericShortcut()C
    .locals 1

    .prologue
    .line 128
    iget-char v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mNumericShortcut:C

    return v0
.end method

.method public getOrder()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mOrder:I

    return v0
.end method

.method public bridge synthetic getSubMenu()Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getSubMenu()Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v0

    return-object v0
.end method

.method public getSubMenu()Lcom/googlecode/eyesfree/widget/RadialSubMenu;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitleCondensed()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mTitleCondensed:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public hasSubMenu()Z
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActionViewExpanded()Z
    .locals 1

    .prologue
    .line 363
    const/4 v0, 0x0

    return v0
.end method

.method public isCheckable()Z
    .locals 1

    .prologue
    .line 158
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mCheckable:Z

    return v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mCheckable:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mChecked:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCorner()Z
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mCorner:Z

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 168
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mEnabled:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mVisible:Z

    return v0
.end method

.method public onClickPerformed()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 189
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mCheckable:Z

    if-eqz v0, :cond_0

    .line 190
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mChecked:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mChecked:Z

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-eqz v0, :cond_1

    .line 194
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-interface {v0, p0}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v1

    .line 197
    :cond_1
    return v1

    :cond_2
    move v0, v1

    .line 190
    goto :goto_0
.end method

.method public onSelectionPerformed()Z
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mSelectionListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mSelectionListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    invoke-interface {v0, p0}, Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;->onMenuItemSelection(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Z

    move-result v0

    .line 210
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .locals 1
    .param p1, "actionProvider"    # Landroid/view/ActionProvider;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 369
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setActionView(I)Landroid/view/MenuItem;
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 379
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 374
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 0
    .param p1, "alphaChar"    # C

    .prologue
    .line 215
    iput-char p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mAlphaShortcut:C

    .line 217
    return-object p0
.end method

.method public setCheckable(Z)Landroid/view/MenuItem;
    .locals 0
    .param p1, "checkable"    # Z

    .prologue
    .line 222
    iput-boolean p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mCheckable:Z

    .line 224
    return-object p0
.end method

.method public setChecked(Z)Landroid/view/MenuItem;
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 229
    iput-boolean p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mChecked:Z

    .line 231
    return-object p0
.end method

.method setCorner(Z)Landroid/view/MenuItem;
    .locals 1
    .param p1, "corner"    # Z

    .prologue
    .line 335
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mCorner:Z

    .line 337
    return-object p0
.end method

.method public setEnabled(Z)Landroid/view/MenuItem;
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 236
    iput-boolean p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mEnabled:Z

    .line 238
    return-object p0
.end method

.method public setIcon(I)Landroid/view/MenuItem;
    .locals 1
    .param p1, "iconRes"    # I

    .prologue
    .line 250
    if-nez p1, :cond_0

    .line 251
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 256
    :goto_0
    return-object p0

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mIcon:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 0
    .param p1, "icon"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 245
    return-object p0
.end method

.method public setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 261
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mIntent:Landroid/content/Intent;

    .line 263
    return-object p0
.end method

.method public setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 0
    .param p1, "numericChar"    # C

    .prologue
    .line 268
    iput-char p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mNumericShortcut:C

    .line 270
    return-object p0
.end method

.method public setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .locals 1
    .param p1, "listener"    # Landroid/view/MenuItem$OnActionExpandListener;

    .prologue
    .line 384
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 0
    .param p1, "menuItemClickListener"    # Landroid/view/MenuItem$OnMenuItemClickListener;

    .prologue
    .line 275
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 277
    return-object p0
.end method

.method public setOnMenuItemSelectionListener(Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;)Landroid/view/MenuItem;
    .locals 0
    .param p1, "menuItemSelectionListener"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    .prologue
    .line 288
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mSelectionListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    .line 290
    return-object p0
.end method

.method public setShortcut(CC)Landroid/view/MenuItem;
    .locals 0
    .param p1, "numericChar"    # C
    .param p2, "alphaChar"    # C

    .prologue
    .line 295
    iput-char p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mNumericShortcut:C

    .line 296
    iput-char p2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mAlphaShortcut:C

    .line 298
    return-object p0
.end method

.method public setShowAsAction(I)V
    .locals 1
    .param p1, "actionEnum"    # I

    .prologue
    .line 389
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 1
    .param p1, "actionEnum"    # I

    .prologue
    .line 394
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setTitle(I)Landroid/view/MenuItem;
    .locals 1
    .param p1, "titleRes"    # I

    .prologue
    .line 310
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mTitle:Ljava/lang/CharSequence;

    .line 312
    return-object p0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 303
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mTitle:Ljava/lang/CharSequence;

    .line 305
    return-object p0
.end method

.method public setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 0
    .param p1, "titleCondensed"    # Ljava/lang/CharSequence;

    .prologue
    .line 317
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mTitleCondensed:Ljava/lang/CharSequence;

    .line 318
    return-object p0
.end method

.method public setVisible(Z)Landroid/view/MenuItem;
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 323
    iput-boolean p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->mVisible:Z

    .line 325
    return-object p0
.end method
