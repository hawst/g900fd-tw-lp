.class public Lcom/googlecode/eyesfree/widget/RadialMenu;
.super Ljava/lang/Object;
.source "RadialMenu.java"

# interfaces
.implements Landroid/view/Menu;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;,
        Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mCorners:Lcom/googlecode/eyesfree/utils/SparseIterableArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/googlecode/eyesfree/utils/SparseIterableArray",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;"
        }
    .end annotation
.end field

.field private mLayoutListener:Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;

.field private mListener:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private final mParent:Landroid/content/DialogInterface;

.field private mQwertyMode:Z

.field private mSelectionListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

.field private mVisibilityListener:Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parent"    # Landroid/content/DialogInterface;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mContext:Landroid/content/Context;

    .line 74
    iput-object p2, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mParent:Landroid/content/DialogInterface;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    .line 76
    new-instance v0, Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/utils/SparseIterableArray;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mCorners:Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mQwertyMode:Z

    .line 80
    return-void
.end method

.method static getCornerLocation(I)Landroid/graphics/PointF;
    .locals 3
    .param p0, "groupId"    # I

    .prologue
    .line 374
    packed-switch p0, :pswitch_data_0

    .line 392
    const/4 v2, 0x0

    .line 395
    :goto_0
    return-object v2

    .line 376
    :pswitch_0
    const/4 v0, 0x0

    .line 377
    .local v0, "x":F
    const/4 v1, 0x0

    .line 395
    .local v1, "y":F
    :goto_1
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_0

    .line 380
    .end local v0    # "x":F
    .end local v1    # "y":F
    :pswitch_1
    const/high16 v0, 0x3f800000    # 1.0f

    .line 381
    .restart local v0    # "x":F
    const/4 v1, 0x0

    .line 382
    .restart local v1    # "y":F
    goto :goto_1

    .line 384
    .end local v0    # "x":F
    .end local v1    # "y":F
    :pswitch_2
    const/high16 v0, 0x3f800000    # 1.0f

    .line 385
    .restart local v0    # "x":F
    const/high16 v1, 0x3f800000    # 1.0f

    .line 386
    .restart local v1    # "y":F
    goto :goto_1

    .line 388
    .end local v0    # "x":F
    .end local v1    # "y":F
    :pswitch_3
    const/4 v0, 0x0

    .line 389
    .restart local v0    # "x":F
    const/high16 v1, 0x3f800000    # 1.0f

    .line 390
    .restart local v1    # "y":F
    goto :goto_1

    .line 374
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method static getCornerRotation(I)F
    .locals 1
    .param p0, "groupId"    # I

    .prologue
    .line 335
    packed-switch p0, :pswitch_data_0

    .line 349
    const/4 v0, 0x0

    .line 352
    .local v0, "rotation":F
    :goto_0
    return v0

    .line 337
    .end local v0    # "rotation":F
    :pswitch_0
    const/high16 v0, 0x43070000    # 135.0f

    .line 338
    .restart local v0    # "rotation":F
    goto :goto_0

    .line 340
    .end local v0    # "rotation":F
    :pswitch_1
    const/high16 v0, -0x3cf90000    # -135.0f

    .line 341
    .restart local v0    # "rotation":F
    goto :goto_0

    .line 343
    .end local v0    # "rotation":F
    :pswitch_2
    const/high16 v0, -0x3dcc0000    # -45.0f

    .line 344
    .restart local v0    # "rotation":F
    goto :goto_0

    .line 346
    .end local v0    # "rotation":F
    :pswitch_3
    const/high16 v0, 0x42340000    # 45.0f

    .line 347
    .restart local v0    # "rotation":F
    goto :goto_0

    .line 335
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private getItemForShortcut(ILandroid/view/KeyEvent;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 452
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .line 453
    .local v1, "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getAlphabeticShortcut()C

    move-result v2

    if-eq v2, p1, :cond_1

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getNumericShortcut()C

    move-result v2

    if-ne v2, p1, :cond_0

    .line 458
    .end local v1    # "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :cond_1
    :goto_0
    return-object v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic add(I)Landroid/view/MenuItem;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic add(IIII)Landroid/view/MenuItem;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # I
    .param p3, "x2"    # I
    .param p4, "x3"    # I

    .prologue
    .line 45
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(IIII)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # I
    .param p3, "x2"    # I
    .param p4, "x3"    # Ljava/lang/CharSequence;

    .prologue
    .line 45
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1
    .param p1, "x0"    # Ljava/lang/CharSequence;

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(Ljava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 1
    .param p1, "titleRes"    # I

    .prologue
    const/4 v0, 0x0

    .line 115
    invoke-virtual {p0, v0, v0, v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(IIII)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(IIII)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 2
    .param p1, "groupId"    # I
    .param p2, "itemId"    # I
    .param p3, "order"    # I
    .param p4, "titleRes"    # I

    .prologue
    .line 125
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 127
    .local v0, "title":Ljava/lang/CharSequence;
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v1

    return-object v1
.end method

.method public add(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 6
    .param p1, "groupId"    # I
    .param p2, "itemId"    # I
    .param p3, "order"    # I
    .param p4, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 132
    new-instance v0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mContext:Landroid/content/Context;

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 134
    .local v0, "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-virtual {p0, v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v1

    return-object v1
.end method

.method public add(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 2
    .param p1, "item"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .prologue
    .line 144
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->hasSubMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mSelectionListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    invoke-virtual {p1, v0}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setOnMenuItemSelectionListener(Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;)Landroid/view/MenuItem;

    .line 146
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-virtual {p1, v0}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 149
    :cond_0
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getGroupId()I

    move-result v0

    const v1, 0x7f0d0046

    if-ne v0, v1, :cond_1

    .line 150
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setCorner(Z)Landroid/view/MenuItem;

    .line 151
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mCorners:Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getOrder()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/googlecode/eyesfree/utils/SparseIterableArray;->put(ILjava/lang/Object;)V

    .line 156
    :goto_0
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onLayoutChanged()V

    .line 158
    return-object p1

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public add(Ljava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v0, 0x0

    .line 120
    invoke-virtual {p0, v0, v0, v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public addAll(Ljava/lang/Iterable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 167
    .local p1, "items":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lcom/googlecode/eyesfree/widget/RadialMenuItem;>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .line 168
    .local v1, "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-virtual {p0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    goto :goto_0

    .line 170
    .end local v1    # "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :cond_0
    return-void
.end method

.method public addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 13
    .param p1, "groupId"    # I
    .param p2, "itemId"    # I
    .param p3, "order"    # I
    .param p4, "caller"    # Landroid/content/ComponentName;
    .param p5, "specifics"    # [Landroid/content/Intent;
    .param p6, "intent"    # Landroid/content/Intent;
    .param p7, "flags"    # I
    .param p8, "outSpecificItems"    # [Landroid/view/MenuItem;

    .prologue
    .line 175
    iget-object v12, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    .line 176
    .local v10, "manager":Landroid/content/pm/PackageManager;
    const/4 v12, 0x0

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    invoke-virtual {v10, v0, v1, v2, v12}, Landroid/content/pm/PackageManager;->queryIntentActivityOptions(Landroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v8

    .line 179
    .local v8, "infoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    and-int/lit8 v12, p7, 0x1

    if-nez v12, :cond_0

    .line 180
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->removeGroup(I)V

    .line 183
    :cond_0
    const/4 v3, 0x0

    .line 185
    .local v3, "i":I
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    .line 186
    .local v7, "info":Landroid/content/pm/ResolveInfo;
    invoke-virtual {v7, v10}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 187
    .local v6, "icon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v7, v10}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v11

    .line 188
    .local v11, "title":Ljava/lang/CharSequence;
    move/from16 v0, p3

    invoke-virtual {p0, p1, p2, v0, v11}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v9

    .line 190
    .local v9, "item":Landroid/view/MenuItem;
    invoke-interface {v9, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 192
    move-object/from16 v0, p8

    array-length v12, v0

    if-ge v3, v12, :cond_1

    .line 193
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    aput-object v9, p8, v3

    move v3, v4

    .line 197
    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 195
    :cond_1
    new-instance v12, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v12}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v12

    .line 199
    .end local v6    # "icon":Landroid/graphics/drawable/Drawable;
    .end local v7    # "info":Landroid/content/pm/ResolveInfo;
    .end local v9    # "item":Landroid/view/MenuItem;
    .end local v11    # "title":Ljava/lang/CharSequence;
    :cond_2
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v12

    return v12
.end method

.method public bridge synthetic addSubMenu(I)Landroid/view/SubMenu;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->addSubMenu(I)Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # I
    .param p3, "x2"    # I
    .param p4, "x3"    # I

    .prologue
    .line 45
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/googlecode/eyesfree/widget/RadialMenu;->addSubMenu(IIII)Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # I
    .param p3, "x2"    # I
    .param p4, "x3"    # Ljava/lang/CharSequence;

    .prologue
    .line 45
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/googlecode/eyesfree/widget/RadialMenu;->addSubMenu(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1
    .param p1, "x0"    # Ljava/lang/CharSequence;

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->addSubMenu(Ljava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v0

    return-object v0
.end method

.method public addSubMenu(I)Lcom/googlecode/eyesfree/widget/RadialSubMenu;
    .locals 1
    .param p1, "titleRes"    # I

    .prologue
    const/4 v0, 0x0

    .line 204
    invoke-virtual {p0, v0, v0, v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->addSubMenu(IIII)Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v0

    return-object v0
.end method

.method public addSubMenu(IIII)Lcom/googlecode/eyesfree/widget/RadialSubMenu;
    .locals 2
    .param p1, "groupId"    # I
    .param p2, "itemId"    # I
    .param p3, "order"    # I
    .param p4, "titleRes"    # I

    .prologue
    .line 214
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 216
    .local v0, "title":Ljava/lang/CharSequence;
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->addSubMenu(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v1

    return-object v1
.end method

.method public addSubMenu(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialSubMenu;
    .locals 8
    .param p1, "groupId"    # I
    .param p2, "itemId"    # I
    .param p3, "order"    # I
    .param p4, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 221
    new-instance v0, Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mParent:Landroid/content/DialogInterface;

    move-object v3, p0

    move v4, p1

    move v5, p2

    move v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;-><init>(Landroid/content/Context;Landroid/content/DialogInterface;Lcom/googlecode/eyesfree/widget/RadialMenu;IIILjava/lang/CharSequence;)V

    .line 224
    .local v0, "submenu":Lcom/googlecode/eyesfree/widget/RadialSubMenu;
    invoke-virtual {v0}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->getItem()Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .line 226
    return-object v0
.end method

.method public addSubMenu(Ljava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialSubMenu;
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v0, 0x0

    .line 209
    invoke-virtual {p0, v0, v0, v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->addSubMenu(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 232
    return-void
.end method

.method public clearSelection(I)Z
    .locals 1
    .param p1, "flags"    # I

    .prologue
    .line 501
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->selectMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    move-result v0

    return v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 248
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onDismiss()V

    .line 249
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mParent:Landroid/content/DialogInterface;

    invoke-interface {v0}, Landroid/content/DialogInterface;->dismiss()V

    .line 250
    return-void
.end method

.method public bridge synthetic findItem(I)Landroid/view/MenuItem;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->findItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public findItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 4
    .param p1, "id"    # I

    .prologue
    const/4 v2, 0x0

    .line 254
    const/4 v3, -0x1

    if-ne p1, v3, :cond_0

    move-object v1, v2

    .line 270
    :goto_0
    return-object v1

    .line 258
    :cond_0
    iget-object v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .line 259
    .local v1, "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getItemId()I

    move-result v3

    if-ne v3, p1, :cond_1

    goto :goto_0

    .line 264
    .end local v1    # "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :cond_2
    iget-object v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mCorners:Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    invoke-virtual {v3}, Lcom/googlecode/eyesfree/utils/SparseIterableArray;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .line 265
    .restart local v1    # "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getItemId()I

    move-result v3

    if-ne v3, p1, :cond_3

    goto :goto_0

    .end local v1    # "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :cond_4
    move-object v1, v2

    .line 270
    goto :goto_0
.end method

.method public getCorner(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 1
    .param p1, "groupId"    # I

    .prologue
    .line 317
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mCorners:Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/utils/SparseIterableArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Landroid/view/MenuItem;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 275
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    return-object v0
.end method

.method public getItems(Z)Ljava/util/List;
    .locals 6
    .param p1, "includeCorners"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 286
    if-eqz p1, :cond_0

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 287
    .local v3, "listSize":I
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 289
    .local v2, "itemsCopy":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/widget/RadialMenuItem;>;"
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 291
    if-eqz p1, :cond_1

    .line 292
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mCorners:Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    invoke-virtual {v4}, Lcom/googlecode/eyesfree/utils/SparseIterableArray;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .line 293
    .local v1, "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 286
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .end local v2    # "itemsCopy":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/widget/RadialMenuItem;>;"
    .end local v3    # "listSize":I
    :cond_0
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mCorners:Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    invoke-virtual {v5}, Lcom/googlecode/eyesfree/utils/SparseIterableArray;->size()I

    move-result v5

    add-int v3, v4, v5

    goto :goto_0

    .line 297
    .restart local v2    # "itemsCopy":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/widget/RadialMenuItem;>;"
    .restart local v3    # "listSize":I
    :cond_1
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    return-object v4
.end method

.method public hasVisibleItems()Z
    .locals 3

    .prologue
    .line 435
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/MenuItem;

    .line 436
    .local v1, "item":Landroid/view/MenuItem;
    invoke-interface {v1}, Landroid/view/MenuItem;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 437
    const/4 v2, 0x1

    .line 441
    .end local v1    # "item":Landroid/view/MenuItem;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public indexOf(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)I
    .locals 1
    .param p1, "item"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .prologue
    .line 301
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 463
    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getItemForShortcut(ILandroid/view/KeyEvent;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    .line 465
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method onDismiss()V
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mVisibilityListener:Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mVisibilityListener:Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;

    invoke-interface {v0, p0}, Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;->onMenuDismissed(Lcom/googlecode/eyesfree/widget/RadialMenu;)V

    .line 244
    :cond_0
    return-void
.end method

.method protected onLayoutChanged()V
    .locals 1

    .prologue
    .line 614
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mLayoutListener:Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;

    if-eqz v0, :cond_0

    .line 615
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mLayoutListener:Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;

    invoke-interface {v0}, Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;->onLayoutChanged()V

    .line 617
    :cond_0
    return-void
.end method

.method onShow()V
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mVisibilityListener:Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mVisibilityListener:Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;

    invoke-interface {v0, p0}, Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;->onMenuShown(Lcom/googlecode/eyesfree/widget/RadialMenu;)V

    .line 238
    :cond_0
    return-void
.end method

.method public performIdentifierAction(II)Z
    .locals 2
    .param p1, "id"    # I
    .param p2, "flags"    # I

    .prologue
    .line 526
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->findItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    .line 528
    .local v0, "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-virtual {p0, v0, p2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->performMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    move-result v1

    return v1
.end method

.method public performMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z
    .locals 2
    .param p1, "item"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .param p2, "flags"    # I

    .prologue
    .line 513
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->onClickPerformed()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-interface {v1, p1}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    const/4 v0, 0x1

    .line 516
    .local v0, "performedAction":Z
    :goto_0
    if-eqz p1, :cond_1

    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_1

    and-int/lit8 v1, p2, 0x2

    if-eqz v1, :cond_2

    .line 518
    :cond_1
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->close()V

    .line 521
    :cond_2
    return v0

    .line 513
    .end local v0    # "performedAction":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;
    .param p3, "flags"    # I

    .prologue
    .line 533
    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getItemForShortcut(ILandroid/view/KeyEvent;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    .line 535
    .local v0, "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-virtual {p0, v0, p3}, Lcom/googlecode/eyesfree/widget/RadialMenu;->performMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    move-result v1

    return v1
.end method

.method public removeGroup(I)V
    .locals 4
    .param p1, "group"    # I

    .prologue
    .line 540
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 542
    .local v2, "removeItems":Ljava/util/List;, "Ljava/util/List<Landroid/view/MenuItem;>;"
    iget-object v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/MenuItem;

    .line 543
    .local v1, "item":Landroid/view/MenuItem;
    invoke-interface {v1}, Landroid/view/MenuItem;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 544
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 548
    .end local v1    # "item":Landroid/view/MenuItem;
    :cond_1
    iget-object v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 550
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 551
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onLayoutChanged()V

    .line 553
    :cond_2
    return-void
.end method

.method public removeItem(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 400
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->findItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    .line 401
    .local v0, "item":Landroid/view/MenuItem;
    if-nez v0, :cond_0

    .line 408
    :goto_0
    return-void

    .line 405
    :cond_0
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 407
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onLayoutChanged()V

    goto :goto_0
.end method

.method public selectMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z
    .locals 2
    .param p1, "item"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .param p2, "flags"    # I

    .prologue
    const/4 v0, 0x1

    .line 477
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->onSelectionPerformed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 485
    :cond_0
    :goto_0
    return v0

    .line 481
    :cond_1
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mSelectionListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mSelectionListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    invoke-interface {v1, p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;->onMenuItemSelection(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 485
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDefaultListener(Landroid/view/MenuItem$OnMenuItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/view/MenuItem$OnMenuItemClickListener;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 95
    return-void
.end method

.method public setDefaultSelectionListener(Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;)V
    .locals 0
    .param p1, "selectionListener"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mSelectionListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    .line 107
    return-void
.end method

.method public setGroupCheckable(IZZ)V
    .locals 4
    .param p1, "group"    # I
    .param p2, "checkable"    # Z
    .param p3, "exclusive"    # Z

    .prologue
    .line 557
    const/4 v2, 0x0

    .line 559
    .local v2, "layoutChanged":Z
    iget-object v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/MenuItem;

    .line 560
    .local v1, "item":Landroid/view/MenuItem;
    invoke-interface {v1}, Landroid/view/MenuItem;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 561
    invoke-interface {v1, p2}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    .line 562
    const/4 v2, 0x1

    goto :goto_0

    .line 566
    .end local v1    # "item":Landroid/view/MenuItem;
    :cond_1
    if-eqz v2, :cond_2

    .line 567
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onLayoutChanged()V

    .line 569
    :cond_2
    return-void
.end method

.method public setGroupEnabled(IZ)V
    .locals 4
    .param p1, "group"    # I
    .param p2, "enabled"    # Z

    .prologue
    .line 573
    const/4 v2, 0x0

    .line 575
    .local v2, "layoutChanged":Z
    iget-object v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/MenuItem;

    .line 576
    .local v1, "item":Landroid/view/MenuItem;
    invoke-interface {v1}, Landroid/view/MenuItem;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 577
    invoke-interface {v1, p2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 578
    const/4 v2, 0x1

    goto :goto_0

    .line 582
    .end local v1    # "item":Landroid/view/MenuItem;
    :cond_1
    if-eqz v2, :cond_2

    .line 583
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onLayoutChanged()V

    .line 585
    :cond_2
    return-void
.end method

.method public setGroupVisible(IZ)V
    .locals 4
    .param p1, "group"    # I
    .param p2, "visible"    # Z

    .prologue
    .line 589
    const/4 v2, 0x0

    .line 591
    .local v2, "layoutChanged":Z
    iget-object v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/MenuItem;

    .line 592
    .local v1, "item":Landroid/view/MenuItem;
    invoke-interface {v1}, Landroid/view/MenuItem;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 593
    invoke-interface {v1, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 594
    const/4 v2, 0x1

    goto :goto_0

    .line 598
    .end local v1    # "item":Landroid/view/MenuItem;
    :cond_1
    if-eqz v2, :cond_2

    .line 599
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onLayoutChanged()V

    .line 601
    :cond_2
    return-void
.end method

.method public setLayoutListener(Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;)V
    .locals 0
    .param p1, "layoutListener"    # Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mLayoutListener:Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;

    .line 84
    return-void
.end method

.method public setOnMenuVisibilityChangedListener(Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mVisibilityListener:Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;

    .line 111
    return-void
.end method

.method public setQwertyMode(Z)V
    .locals 0
    .param p1, "isQwerty"    # Z

    .prologue
    .line 605
    iput-boolean p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mQwertyMode:Z

    .line 606
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 610
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
