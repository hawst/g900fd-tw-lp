.class public Lcom/googlecode/eyesfree/utils/FocusFinder;
.super Ljava/lang/Object;
.source "FocusFinder.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# direct methods
.method public static getFocusedNode(Landroid/accessibilityservice/AccessibilityService;Z)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 6
    .param p0, "service"    # Landroid/accessibilityservice/AccessibilityService;
    .param p1, "fallbackOnRoot"    # Z

    .prologue
    .line 173
    invoke-virtual {p0}, Landroid/accessibilityservice/AccessibilityService;->getRootInActiveWindow()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v2

    .line 174
    .local v2, "root":Landroid/view/accessibility/AccessibilityNodeInfo;
    const/4 v0, 0x0

    .line 177
    .local v0, "focused":Landroid/view/accessibility/AccessibilityNodeInfo;
    const/4 v1, 0x0

    .line 178
    .local v1, "ret":Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v2, :cond_4

    .line 179
    const/4 v3, 0x2

    :try_start_0
    invoke-virtual {v2, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->findFocus(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 180
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->isVisibleToUser()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 181
    move-object v1, v0

    .line 182
    const/4 v0, 0x0

    .line 191
    :cond_0
    :goto_0
    if-eqz v1, :cond_7

    .line 192
    new-instance v3, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {v3, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;-><init>(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 195
    if-eqz v2, :cond_1

    .line 196
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->recycle()V

    .line 199
    :cond_1
    if-eqz v0, :cond_2

    .line 200
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->recycle()V

    .line 204
    :cond_2
    :goto_1
    return-object v3

    .line 183
    :cond_3
    if-eqz p1, :cond_0

    .line 184
    move-object v1, v2

    .line 185
    const/4 v2, 0x0

    goto :goto_0

    .line 188
    :cond_4
    const/4 v3, 0x6

    :try_start_1
    const-string v4, "No current window root"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p0, v3, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 195
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_5

    .line 196
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->recycle()V

    .line 199
    :cond_5
    if-eqz v0, :cond_6

    .line 200
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->recycle()V

    :cond_6
    throw v3

    .line 195
    :cond_7
    if-eqz v2, :cond_8

    .line 196
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->recycle()V

    .line 199
    :cond_8
    if-eqz v0, :cond_9

    .line 200
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->recycle()V

    .line 204
    :cond_9
    const/4 v3, 0x0

    goto :goto_1
.end method
