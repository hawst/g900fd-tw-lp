.class public abstract Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;
.super Landroid/support/v4/view/AccessibilityDelegateCompat;
.source "ExploreByTouchHelper.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private mFocusedVirtualViewId:I

.field private final mHost:Landroid/view/View;

.field private mHoveredVirtualViewId:I

.field private final mManager:Landroid/view/accessibility/AccessibilityManager;

.field private mNodeProvider:Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;

.field private final mTempGlobalRect:[I

.field private final mTempParentRect:Landroid/graphics/Rect;

.field private final mTempScreenRect:Landroid/graphics/Rect;

.field private final mTempVisibleRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1, "host"    # Landroid/view/View;

    .prologue
    const/high16 v1, -0x80000000

    .line 88
    invoke-direct {p0}, Landroid/support/v4/view/AccessibilityDelegateCompat;-><init>()V

    .line 65
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempScreenRect:Landroid/graphics/Rect;

    .line 66
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempParentRect:Landroid/graphics/Rect;

    .line 67
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempVisibleRect:Landroid/graphics/Rect;

    .line 68
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempGlobalRect:[I

    .line 77
    iput v1, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mFocusedVirtualViewId:I

    .line 80
    iput v1, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHoveredVirtualViewId:I

    .line 463
    new-instance v0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper$1;

    invoke-direct {v0, p0}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper$1;-><init>(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mNodeProvider:Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;

    .line 89
    iput-object p1, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    .line 90
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mManager:Landroid/view/accessibility/AccessibilityManager;

    .line 92
    return-void
.end method

.method static synthetic access$000(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->getNodeForHost()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->getNodeForRoot()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;
    .param p1, "x1"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->getNodeForVirtualViewId(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    .prologue
    .line 53
    iget v0, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mFocusedVirtualViewId:I

    return v0
.end method

.method static synthetic access$302(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;I)I
    .locals 0
    .param p0, "x0"    # Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;
    .param p1, "x1"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mFocusedVirtualViewId:I

    return p1
.end method

.method static synthetic access$400(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    return-object v0
.end method

.method private getEventForRoot(I)Landroid/view/accessibility/AccessibilityEvent;
    .locals 4
    .param p1, "eventType"    # I

    .prologue
    .line 231
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 232
    .local v0, "event":Landroid/view/accessibility/AccessibilityEvent;
    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 234
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v1, v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 235
    .local v1, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    const v3, -0x7fffffff

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->setSource(Landroid/view/View;I)V

    .line 237
    return-object v0
.end method

.method private getEventForVirtualViewId(II)Landroid/view/accessibility/AccessibilityEvent;
    .locals 4
    .param p1, "virtualViewId"    # I
    .param p2, "eventType"    # I

    .prologue
    .line 251
    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 254
    .local v0, "event":Landroid/view/accessibility/AccessibilityEvent;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 255
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "$VirtualView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 258
    invoke-virtual {p0, p1, v0}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->populateEventForVirtualViewId(ILandroid/view/accessibility/AccessibilityEvent;)V

    .line 260
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 261
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "You must add text or a content description in populateEventForItem()"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 266
    :cond_0
    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 269
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v1, v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 270
    .local v1, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-virtual {v1, v2, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->setSource(Landroid/view/View;I)V

    .line 272
    return-object v0
.end method

.method private getNodeForHost()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 5

    .prologue
    .line 284
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-static {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/view/View;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 285
    .local v1, "result":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-static {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/view/View;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    .line 286
    .local v2, "source":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-static {v3, v2}, Landroid/support/v4/view/ViewCompat;->onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 289
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempParentRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getBoundsInParent(Landroid/graphics/Rect;)V

    .line 290
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getBoundsInScreen(Landroid/graphics/Rect;)V

    .line 291
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempParentRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInParent(Landroid/graphics/Rect;)V

    .line 292
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInScreen(Landroid/graphics/Rect;)V

    .line 295
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-static {v3}, Landroid/support/v4/view/ViewCompat;->getParentForAccessibility(Landroid/view/View;)Landroid/view/ViewParent;

    move-result-object v0

    .line 296
    .local v0, "parent":Landroid/view/ViewParent;
    instance-of v3, v0, Landroid/view/View;

    if-eqz v3, :cond_0

    .line 297
    check-cast v0, Landroid/view/View;

    .end local v0    # "parent":Landroid/view/ViewParent;
    invoke-virtual {v1, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setParent(Landroid/view/View;)V

    .line 301
    :cond_0
    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isVisibleToUser()Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setVisibleToUser(Z)V

    .line 302
    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setPackageName(Ljava/lang/CharSequence;)V

    .line 303
    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getClassName()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setClassName(Ljava/lang/CharSequence;)V

    .line 306
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    const v4, -0x7fffffff

    invoke-virtual {v1, v3, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addChild(Landroid/view/View;I)V

    .line 308
    return-object v1
.end method

.method private getNodeForRoot()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 6

    .prologue
    .line 320
    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-static {v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/view/View;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 321
    .local v1, "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-static {v4, v1}, Landroid/support/v4/view/ViewCompat;->onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 324
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 325
    .local v3, "virtualViewIds":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Integer;>;"
    invoke-virtual {p0, v3}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->getVisibleVirtualViewIds(Ljava/util/List;)V

    .line 327
    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 328
    .local v2, "virtualViewId":Ljava/lang/Integer;
    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addChild(Landroid/view/View;I)V

    goto :goto_0

    .line 332
    .end local v2    # "virtualViewId":Ljava/lang/Integer;
    :cond_0
    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setParent(Landroid/view/View;)V

    .line 333
    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    const v5, -0x7fffffff

    invoke-virtual {v1, v4, v5}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setSource(Landroid/view/View;I)V

    .line 335
    return-object v1
.end method

.method private getNodeForVirtualViewId(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 7
    .param p1, "virtualViewId"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 367
    invoke-static {}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 370
    .local v0, "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-virtual {v0, v5}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setEnabled(Z)V

    .line 371
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "$VirtualView"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setClassName(Ljava/lang/CharSequence;)V

    .line 374
    invoke-virtual {p0, p1, v0}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->populateNodeForVirtualViewId(ILandroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 376
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 377
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "You must add text or a content description in populateNodeForVirtualViewId()"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 382
    :cond_0
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setPackageName(Ljava/lang/CharSequence;)V

    .line 383
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    const v4, -0x7fffffff

    invoke-virtual {v0, v3, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setParent(Landroid/view/View;I)V

    .line 384
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-virtual {v0, v3, p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setSource(Landroid/view/View;I)V

    .line 387
    iget v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mFocusedVirtualViewId:I

    if-ne v3, p1, :cond_1

    .line 388
    invoke-virtual {v0, v5}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setAccessibilityFocused(Z)V

    .line 389
    const/16 v3, 0x80

    invoke-virtual {v0, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    .line 395
    :goto_0
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempParentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getBoundsInParent(Landroid/graphics/Rect;)V

    .line 396
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempParentRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 397
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "You must set parent bounds in populateNodeForVirtualViewId()"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 391
    :cond_1
    invoke-virtual {v0, v6}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setAccessibilityFocused(Z)V

    .line 392
    const/16 v3, 0x40

    invoke-virtual {v0, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    goto :goto_0

    .line 402
    :cond_2
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempParentRect:Landroid/graphics/Rect;

    invoke-direct {p0, v3}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->intersectVisibleToUser(Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 403
    invoke-virtual {v0, v5}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setVisibleToUser(Z)V

    .line 404
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempParentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInParent(Landroid/graphics/Rect;)V

    .line 408
    :cond_3
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempGlobalRect:[I

    invoke-virtual {v3, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 409
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempGlobalRect:[I

    aget v1, v3, v6

    .line 410
    .local v1, "offsetX":I
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempGlobalRect:[I

    aget v2, v3, v5

    .line 411
    .local v2, "offsetY":I
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempScreenRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempParentRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 412
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 413
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInScreen(Landroid/graphics/Rect;)V

    .line 415
    return-object v0
.end method

.method private intersectVisibleToUser(Landroid/graphics/Rect;)Z
    .locals 5
    .param p1, "localRect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v2, 0x0

    .line 428
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 456
    :cond_0
    :goto_0
    return v2

    .line 433
    :cond_1
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWindowVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 439
    move-object v0, p0

    .line 440
    :goto_1
    instance-of v3, v0, Landroid/view/View;

    if-eqz v3, :cond_2

    move-object v1, v0

    .line 441
    check-cast v1, Landroid/view/View;

    .line 444
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getAlpha()F

    move-result v3

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-lez v3, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 447
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 448
    .local v0, "current":Landroid/view/ViewParent;
    goto :goto_1

    .line 451
    .end local v0    # "current":Landroid/view/ViewParent;
    .end local v1    # "view":Landroid/view/View;
    :cond_2
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempVisibleRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 456
    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mTempVisibleRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v2}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v2

    goto :goto_0
.end method


# virtual methods
.method public getAccessibilityNodeProvider(Landroid/view/View;)Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;
    .locals 1
    .param p1, "host"    # Landroid/view/View;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mNodeProvider:Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;

    return-object v0
.end method

.method protected abstract getVisibleVirtualViewIds(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method protected abstract performActionForVirtualViewId(IILandroid/os/Bundle;)Z
.end method

.method protected abstract populateEventForVirtualViewId(ILandroid/view/accessibility/AccessibilityEvent;)V
.end method

.method protected abstract populateNodeForVirtualViewId(ILandroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
.end method

.method public sendEventForVirtualViewId(II)Z
    .locals 4
    .param p1, "virtualViewId"    # I
    .param p2, "eventType"    # I

    .prologue
    const/4 v2, 0x0

    .line 165
    const/high16 v3, -0x80000000

    if-eq p1, v3, :cond_0

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 181
    :cond_0
    :goto_0
    return v2

    .line 169
    :cond_1
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 170
    .local v1, "group":Landroid/view/ViewGroup;
    if-eqz v1, :cond_0

    .line 175
    const v2, -0x7fffffff

    if-ne p1, v2, :cond_2

    .line 176
    invoke-direct {p0, p2}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->getEventForRoot(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 181
    .local v0, "event":Landroid/view/accessibility/AccessibilityEvent;
    :goto_1
    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v2

    goto :goto_0

    .line 178
    .end local v0    # "event":Landroid/view/accessibility/AccessibilityEvent;
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->getEventForVirtualViewId(II)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .restart local v0    # "event":Landroid/view/accessibility/AccessibilityEvent;
    goto :goto_1
.end method
