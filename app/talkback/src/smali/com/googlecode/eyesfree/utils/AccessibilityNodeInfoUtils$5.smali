.class final Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$5;
.super Lcom/googlecode/eyesfree/utils/NodeFilter;
.source "AccessibilityNodeInfoUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1066
    invoke-direct {p0}, Lcom/googlecode/eyesfree/utils/NodeFilter;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 1069
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Landroid/widget/AbsListView;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Landroid/widget/AbsSpinner;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Landroid/widget/ScrollView;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Landroid/widget/HorizontalScrollView;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    # getter for: Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->CLASS_TOUCHWIZ_TWABSLISTVIEW:Ljava/lang/Class;
    invoke-static {}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->access$100()Ljava/lang/Class;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p1, p2, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesAnyClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method
