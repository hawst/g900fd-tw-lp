.class public Lcom/googlecode/eyesfree/utils/NodeSearch;
.super Ljava/lang/Object;
.source "NodeSearch.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;,
        Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;,
        Lcom/googlecode/eyesfree/utils/NodeSearch$SearchResultFilter;,
        Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;
    }
.end annotation


# instance fields
.field private final mAccessibilityService:Landroid/accessibilityservice/AccessibilityService;

.field private mActive:Z

.field private final mFilters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/utils/NodeSearch$SearchResultFilter;",
            ">;"
        }
    .end annotation
.end field

.field private final mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

.field private final mMatchedNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

.field private final mQueryText:Ljava/lang/StringBuilder;

.field private final mSearchOverlay:Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;


# direct methods
.method public constructor <init>(Landroid/accessibilityservice/AccessibilityService;Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;)V
    .locals 2
    .param p1, "accessibilityService"    # Landroid/accessibilityservice/AccessibilityService;
    .param p2, "labelManager"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    .param p3, "textFormatter"    # Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mQueryText:Ljava/lang/StringBuilder;

    .line 92
    new-instance v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mMatchedNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mFilters:Ljava/util/List;

    .line 119
    iput-object p1, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mAccessibilityService:Landroid/accessibilityservice/AccessibilityService;

    .line 120
    iput-object p2, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    .line 121
    new-instance v0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mQueryText:Ljava/lang/StringBuilder;

    invoke-direct {v0, p1, v1, p3}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;-><init>(Landroid/content/Context;Ljava/lang/StringBuilder;Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mSearchOverlay:Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;

    .line 122
    return-void
.end method

.method private evaluateSearch()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 314
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mMatchedNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->get()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/googlecode/eyesfree/utils/NodeSearch;->nodeMatchesQuery(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 319
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/googlecode/eyesfree/utils/NodeSearch;->nextResult(I)Z

    move-result v0

    goto :goto_0
.end method

.method private nodeMatchesQuery(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 6
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v4, 0x0

    .line 331
    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mQueryText:Ljava/lang/StringBuilder;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 332
    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mAccessibilityService:Landroid/accessibilityservice/AccessibilityService;

    invoke-static {v4, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v4

    .line 351
    :cond_0
    :goto_0
    return v4

    .line 335
    :cond_1
    if-eqz p1, :cond_0

    .line 339
    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mFilters:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchResultFilter;

    .line 340
    .local v0, "filter":Lcom/googlecode/eyesfree/utils/NodeSearch$SearchResultFilter;
    invoke-interface {v0, p1}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchResultFilter;->shouldFilter(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_0

    .line 345
    .end local v0    # "filter":Lcom/googlecode/eyesfree/utils/NodeSearch$SearchResultFilter;
    :cond_3
    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    invoke-static {p1, v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getNodeText(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 346
    .local v2, "nodeText":Ljava/lang/CharSequence;
    if-eqz v2, :cond_0

    .line 350
    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mQueryText:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 351
    .local v3, "queryText":Ljava/lang/String;
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    goto :goto_0
.end method


# virtual methods
.method public backspaceQueryText()Z
    .locals 3

    .prologue
    .line 193
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mQueryText:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 194
    .local v0, "length":I
    if-lez v0, :cond_0

    .line 195
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mQueryText:Ljava/lang/StringBuilder;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 196
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mSearchOverlay:Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;->refreshOverlay()V

    .line 197
    const/4 v1, 0x1

    .line 200
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCurrentNode()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mMatchedNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->isNull(Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mAccessibilityService:Landroid/accessibilityservice/AccessibilityService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/googlecode/eyesfree/utils/FocusFinder;->getFocusedNode(Landroid/accessibilityservice/AccessibilityService;Z)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mMatchedNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->get()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0
.end method

.method public getCurrentQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mQueryText:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasMatch()Z
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mMatchedNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->isNull(Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mActive:Z

    return v0
.end method

.method public nextResult(I)Z
    .locals 4
    .param p1, "direction"    # I

    .prologue
    .line 241
    new-instance v1, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    invoke-direct {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;-><init>()V

    .line 242
    .local v1, "next":Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/NodeSearch;->getCurrentNode()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->reset(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 244
    const/4 v0, 0x0

    .line 246
    .local v0, "focusableNext":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->get()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 247
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->get()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/googlecode/eyesfree/utils/NodeSearch;->nodeMatchesQuery(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 250
    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mAccessibilityService:Landroid/accessibilityservice/AccessibilityService;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->get()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->findFocusFromHover(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 254
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isAccessibilityFocused()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_3

    .line 265
    :cond_1
    if-nez v0, :cond_4

    .line 266
    const/4 v2, 0x0

    .line 272
    if-eqz v0, :cond_2

    .line 273
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 275
    :cond_2
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->recycle()V

    :goto_1
    return v2

    .line 258
    :cond_3
    :try_start_1
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->get()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->reset(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 259
    if-eqz v0, :cond_0

    .line 260
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 261
    const/4 v0, 0x0

    goto :goto_0

    .line 269
    :cond_4
    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mMatchedNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    invoke-virtual {v2, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->reset(Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;)V

    .line 270
    const/16 v2, 0x40

    invoke-virtual {v0, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    .line 272
    if-eqz v0, :cond_5

    .line 273
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 275
    :cond_5
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->recycle()V

    goto :goto_1

    .line 272
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_6

    .line 273
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 275
    :cond_6
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->recycle()V

    throw v2
.end method

.method public reEvaluateSearch()V
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mMatchedNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mMatchedNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->get()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->refreshNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->reset(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 284
    invoke-direct {p0}, Lcom/googlecode/eyesfree/utils/NodeSearch;->evaluateSearch()Z

    .line 285
    return-void
.end method

.method public startSearch()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mSearchOverlay:Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;->show()V

    .line 145
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mActive:Z

    .line 146
    return-void
.end method

.method public stopSearch()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 152
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mMatchedNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->clear()V

    .line 153
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mQueryText:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 154
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mSearchOverlay:Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;->hide()V

    .line 155
    iput-boolean v1, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mActive:Z

    .line 156
    return-void
.end method

.method public tryAddQueryText(Ljava/lang/CharSequence;)Z
    .locals 3
    .param p1, "newText"    # Ljava/lang/CharSequence;

    .prologue
    .line 175
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mQueryText:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 176
    .local v0, "initLength":I
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mQueryText:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 177
    invoke-direct {p0}, Lcom/googlecode/eyesfree/utils/NodeSearch;->evaluateSearch()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mSearchOverlay:Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;->refreshOverlay()V

    .line 179
    const/4 v1, 0x1

    .line 183
    :goto_0
    return v1

    .line 182
    :cond_0
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mQueryText:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/NodeSearch;->mQueryText:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 183
    const/4 v1, 0x0

    goto :goto_0
.end method
