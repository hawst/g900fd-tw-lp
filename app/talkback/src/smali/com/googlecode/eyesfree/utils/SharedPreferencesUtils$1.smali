.class final Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils$1;
.super Ljava/lang/Object;
.source "SharedPreferencesUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->storeBooleanAsync(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$key:Ljava/lang/String;

.field final synthetic val$prefs:Landroid/content/SharedPreferences;

.field final synthetic val$value:Z


# direct methods
.method constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils$1;->val$prefs:Landroid/content/SharedPreferences;

    iput-object p2, p0, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils$1;->val$key:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils$1;->val$value:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 128
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils$1;->val$prefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 129
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils$1;->val$key:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils$1;->val$value:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 130
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 131
    return-void
.end method
