.class public Lcom/googlecode/eyesfree/utils/StringBuilderUtils;
.super Ljava/lang/Object;
.source "StringBuilderUtils.java"


# static fields
.field private static final HEX_ALPHABET:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "0123456789abcdef"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->HEX_ALPHABET:[C

    return-void
.end method

.method public static varargs appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    .locals 5
    .param p0, "builder"    # Landroid/text/SpannableStringBuilder;
    .param p1, "args"    # [Ljava/lang/CharSequence;

    .prologue
    .line 78
    if-nez p0, :cond_0

    .line 79
    new-instance p0, Landroid/text/SpannableStringBuilder;

    .end local p0    # "builder":Landroid/text/SpannableStringBuilder;
    invoke-direct {p0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 82
    .restart local p0    # "builder":Landroid/text/SpannableStringBuilder;
    :cond_0
    move-object v1, p1

    .local v1, "arr$":[Ljava/lang/CharSequence;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_5

    aget-object v0, v1, v2

    .line 83
    .local v0, "arg":Ljava/lang/CharSequence;
    if-nez v0, :cond_2

    .line 82
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 87
    :cond_2
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    .line 91
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_3

    .line 92
    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->needsBreakingSeparator(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 93
    const-string v4, ", "

    invoke-virtual {p0, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 99
    :cond_3
    :goto_2
    invoke-virtual {p0, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_1

    .line 95
    :cond_4
    const-string v4, " "

    invoke-virtual {p0, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_2

    .line 102
    .end local v0    # "arg":Ljava/lang/CharSequence;
    :cond_5
    return-object p0
.end method

.method public static bytesToHexString([B)Ljava/lang/String;
    .locals 8
    .param p0, "bytes"    # [B

    .prologue
    .line 128
    if-nez p0, :cond_0

    .line 129
    const/4 v7, 0x0

    .line 141
    :goto_0
    return-object v7

    .line 132
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    array-length v7, p0

    mul-int/lit8 v7, v7, 0x2

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 134
    .local v2, "hex":Ljava/lang/StringBuilder;
    move-object v0, p0

    .local v0, "arr$":[B
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_1

    aget-byte v1, v0, v3

    .line 135
    .local v1, "b":B
    ushr-int/lit8 v7, v1, 0x4

    and-int/lit8 v5, v7, 0xf

    .line 136
    .local v5, "nibble1":I
    and-int/lit8 v6, v1, 0xf

    .line 137
    .local v6, "nibble2":I
    sget-object v7, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->HEX_ALPHABET:[C

    aget-char v7, v7, v5

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 138
    sget-object v7, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->HEX_ALPHABET:[C

    aget-char v7, v7, v6

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 134
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 141
    .end local v1    # "b":B
    .end local v5    # "nibble1":I
    .end local v6    # "nibble2":I
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method public static getAggregateText(Ljava/util/List;)Ljava/lang/CharSequence;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "textList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 53
    :cond_0
    const/4 v0, 0x0

    .line 63
    .local v0, "aggregateText":Ljava/lang/CharSequence;
    :goto_0
    return-object v0

    .line 55
    .end local v0    # "aggregateText":Ljava/lang/CharSequence;
    :cond_1
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 56
    .local v1, "builder":Landroid/text/SpannableStringBuilder;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 57
    .local v3, "text":Ljava/lang/CharSequence;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/CharSequence;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    invoke-static {v1, v4}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_1

    .line 60
    .end local v3    # "text":Ljava/lang/CharSequence;
    :cond_2
    move-object v0, v1

    .restart local v0    # "aggregateText":Ljava/lang/CharSequence;
    goto :goto_0
.end method

.method private static needsBreakingSeparator(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p0, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 113
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    const/4 v0, 0x0

    .line 117
    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v0

    goto :goto_0
.end method
