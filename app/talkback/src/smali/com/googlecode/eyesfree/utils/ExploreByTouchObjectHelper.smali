.class public abstract Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper;
.super Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;
.source "ExploreByTouchObjectHelper.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0
    .param p1, "parentView"    # Landroid/view/View;

    .prologue
    .line 53
    .local p0, "this":Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper;, "Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper<TT;>;"
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;-><init>(Landroid/view/View;)V

    .line 54
    return-void
.end method


# virtual methods
.method protected abstract getItemForVirtualViewId(I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation
.end method

.method protected abstract getVirtualViewIdForItem(Ljava/lang/Object;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation
.end method

.method protected abstract getVisibleItems(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method protected final getVisibleVirtualViewIds(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 127
    .local p0, "this":Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper;, "Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper<TT;>;"
    .local p1, "virtualViewIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 128
    .local v2, "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-virtual {p0, v2}, Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper;->getVisibleItems(Ljava/util/List;)V

    .line 130
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 131
    .local v1, "item":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, v1}, Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper;->getVirtualViewIdForItem(Ljava/lang/Object;)I

    move-result v3

    .line 132
    .local v3, "virtualViewId":I
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 134
    .end local v1    # "item":Ljava/lang/Object;, "TT;"
    .end local v3    # "virtualViewId":I
    :cond_0
    return-void
.end method

.method protected abstract performActionForItem(Ljava/lang/Object;ILandroid/os/Bundle;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I",
            "Landroid/os/Bundle;",
            ")Z"
        }
    .end annotation
.end method

.method protected final performActionForVirtualViewId(IILandroid/os/Bundle;)Z
    .locals 2
    .param p1, "virtualViewId"    # I
    .param p2, "action"    # I
    .param p3, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 94
    .local p0, "this":Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper;, "Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper<TT;>;"
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper;->getItemForVirtualViewId(I)Ljava/lang/Object;

    move-result-object v0

    .line 95
    .local v0, "item":Ljava/lang/Object;, "TT;"
    if-nez v0, :cond_0

    .line 96
    const/4 v1, 0x0

    .line 99
    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0, v0, p2, p3}, Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper;->performActionForItem(Ljava/lang/Object;ILandroid/os/Bundle;)Z

    move-result v1

    goto :goto_0
.end method

.method protected abstract populateEventForItem(Ljava/lang/Object;Landroid/view/accessibility/AccessibilityEvent;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/view/accessibility/AccessibilityEvent;",
            ")V"
        }
    .end annotation
.end method

.method protected final populateEventForVirtualViewId(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1, "virtualViewId"    # I
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 104
    .local p0, "this":Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper;, "Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper<TT;>;"
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper;->getItemForVirtualViewId(I)Ljava/lang/Object;

    move-result-object v0

    .line 105
    .local v0, "item":Ljava/lang/Object;, "TT;"
    if-nez v0, :cond_0

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_0
    invoke-virtual {p0, v0, p2}, Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper;->populateEventForItem(Ljava/lang/Object;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 110
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected abstract populateNodeForItem(Ljava/lang/Object;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ")V"
        }
    .end annotation
.end method

.method protected final populateNodeForVirtualViewId(ILandroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 2
    .param p1, "virtualViewId"    # I
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 116
    .local p0, "this":Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper;, "Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper<TT;>;"
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper;->getItemForVirtualViewId(I)Ljava/lang/Object;

    move-result-object v0

    .line 117
    .local v0, "item":Ljava/lang/Object;, "TT;"
    if-nez v0, :cond_0

    .line 123
    :goto_0
    return-void

    .line 121
    :cond_0
    invoke-virtual {p0, v0, p2}, Lcom/googlecode/eyesfree/utils/ExploreByTouchObjectHelper;->populateNodeForItem(Ljava/lang/Object;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 122
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setClassName(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
