.class public abstract Lcom/googlecode/eyesfree/utils/NodeFilter;
.super Ljava/lang/Object;
.source "NodeFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/utils/NodeFilter$NodeFilterAnd;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    return-void
.end method


# virtual methods
.method public abstract accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
.end method

.method public and(Lcom/googlecode/eyesfree/utils/NodeFilter;)Lcom/googlecode/eyesfree/utils/NodeFilter;
    .locals 1
    .param p1, "filter"    # Lcom/googlecode/eyesfree/utils/NodeFilter;

    .prologue
    .line 29
    if-nez p1, :cond_0

    .line 33
    .end local p0    # "this":Lcom/googlecode/eyesfree/utils/NodeFilter;
    :goto_0
    return-object p0

    .restart local p0    # "this":Lcom/googlecode/eyesfree/utils/NodeFilter;
    :cond_0
    new-instance v0, Lcom/googlecode/eyesfree/utils/NodeFilter$NodeFilterAnd;

    invoke-direct {v0, p0, p1}, Lcom/googlecode/eyesfree/utils/NodeFilter$NodeFilterAnd;-><init>(Lcom/googlecode/eyesfree/utils/NodeFilter;Lcom/googlecode/eyesfree/utils/NodeFilter;)V

    move-object p0, v0

    goto :goto_0
.end method
