.class public Lcom/googlecode/eyesfree/utils/ClassLoadingManager;
.super Landroid/content/BroadcastReceiver;
.source "ClassLoadingManager.java"


# static fields
.field private static mContext:Landroid/content/Context;

.field private static sInstance:Lcom/googlecode/eyesfree/utils/ClassLoadingManager;


# instance fields
.field private final mCachedClasses:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mCachedClasses:Ljava/util/HashMap;

    .line 52
    return-void
.end method

.method public static getInstance()Lcom/googlecode/eyesfree/utils/ClassLoadingManager;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->sInstance:Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;-><init>()V

    sput-object v0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->sInstance:Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    .line 63
    :cond_0
    sget-object v0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->sInstance:Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    return-object v0
.end method


# virtual methods
.method public checkInstanceOf(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "targetClassName"    # Ljava/lang/CharSequence;
    .param p3, "loaderPackage"    # Ljava/lang/CharSequence;
    .param p4, "referenceClassName"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v1, 0x0

    .line 189
    if-eqz p2, :cond_0

    if-nez p4, :cond_1

    .line 204
    :cond_0
    :goto_0
    return v1

    .line 194
    :cond_1
    invoke-static {p2, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 195
    const/4 v1, 0x1

    goto :goto_0

    .line 198
    :cond_2
    invoke-virtual {p0, p1, p4, p3}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->loadOrGetCachedClass(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Class;

    move-result-object v0

    .line 200
    .local v0, "referenceClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-eqz v0, :cond_0

    .line 204
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->checkInstanceOf(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Class;)Z

    move-result v1

    goto :goto_0
.end method

.method public checkInstanceOf(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Class;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "targetClassName"    # Ljava/lang/CharSequence;
    .param p3, "loaderPackage"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .local p4, "referenceClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v1, 0x0

    .line 216
    if-eqz p2, :cond_0

    if-nez p4, :cond_1

    .line 225
    :cond_0
    :goto_0
    return v1

    .line 220
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->loadOrGetCachedClass(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Class;

    move-result-object v0

    .line 221
    .local v0, "targetClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-eqz v0, :cond_0

    .line 225
    invoke-virtual {p4, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    goto :goto_0
.end method

.method public init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    if-nez p1, :cond_0

    .line 73
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "context cannot be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 76
    :cond_0
    sget-object v1, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_1

    .line 77
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "cannot call init twice"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 80
    :cond_1
    sput-object p1, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mContext:Landroid/content/Context;

    .line 81
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 82
    .local v0, "packageFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 83
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 84
    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 85
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 86
    sget-object v1, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 87
    return-void
.end method

.method public loadOrGetCachedClass(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Class;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "className"    # Ljava/lang/CharSequence;
    .param p3, "packageName"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 119
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 120
    const/4 v8, 0x3

    const-string v9, "Missing class name. Failed to load class."

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {p0, v8, v9, v10}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 121
    const/4 v8, 0x0

    .line 177
    :goto_0
    return-object v8

    .line 125
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 126
    const/16 v8, 0x2e

    invoke-static {p2, v8}, Landroid/text/TextUtils;->lastIndexOf(Ljava/lang/CharSequence;C)I

    move-result v4

    .line 128
    .local v4, "lastDotIndex":I
    if-gez v4, :cond_1

    .line 129
    const/4 v8, 0x3

    const-string v9, "Missing package name. Failed to load class: %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p2, v10, v11

    invoke-static {p0, v8, v9, v10}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 131
    const/4 v8, 0x0

    goto :goto_0

    .line 134
    :cond_1
    const/4 v8, 0x0

    invoke-static {p2, v8, v4}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object p3

    .line 137
    .end local v4    # "lastDotIndex":I
    :cond_2
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 138
    .local v0, "classNameStr":Ljava/lang/String;
    invoke-interface {p3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    .line 141
    .local v7, "packageNameStr":Ljava/lang/String;
    iget-object v8, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mCachedClasses:Ljava/util/HashMap;

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 142
    iget-object v8, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mCachedClasses:Ljava/util/HashMap;

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Class;

    goto :goto_0

    .line 147
    :cond_3
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 148
    .local v3, "insideClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-eqz v3, :cond_4

    .line 149
    iget-object v8, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mCachedClasses:Ljava/util/HashMap;

    invoke-virtual {v8, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v8, v3

    .line 150
    goto :goto_0

    .line 152
    .end local v3    # "insideClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :catch_0
    move-exception v8

    .line 157
    :cond_4
    if-nez p1, :cond_5

    .line 158
    const/4 v8, 0x0

    goto :goto_0

    .line 163
    :cond_5
    const/4 v2, 0x3

    .line 164
    .local v2, "flags":I
    const/4 v8, 0x3

    :try_start_1
    invoke-virtual {p1, v7, v8}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v6

    .line 165
    .local v6, "packageContext":Landroid/content/Context;
    invoke-virtual {v6}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    .line 167
    .local v5, "outsideClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-eqz v5, :cond_6

    .line 168
    iget-object v8, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mCachedClasses:Ljava/util/HashMap;

    invoke-virtual {v8, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v8, v5

    .line 169
    goto :goto_0

    .line 171
    .end local v5    # "outsideClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v6    # "packageContext":Landroid/content/Context;
    :catch_1
    move-exception v1

    .line 172
    .local v1, "e":Ljava/lang/Exception;
    const/4 v8, 0x6

    const-string v9, "Failed to load external class: %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v0, v10, v11

    invoke-static {p0, v8, v9, v10}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 175
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_6
    const/4 v8, 0x3

    const-string v9, "Failed to load class: %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v0, v10, v11

    invoke-static {v8, v9, v10}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 176
    iget-object v8, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mCachedClasses:Ljava/util/HashMap;

    const/4 v9, 0x0

    invoke-virtual {v8, v0, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    const/4 v8, 0x0

    goto/16 :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 231
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mCachedClasses:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 232
    return-void
.end method

.method public shutdown()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mCachedClasses:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 95
    sget-object v0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 96
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call init before shutdown"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 99
    :cond_0
    sget-object v0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 100
    const/4 v0, 0x0

    sput-object v0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mContext:Landroid/content/Context;

    .line 101
    return-void
.end method
