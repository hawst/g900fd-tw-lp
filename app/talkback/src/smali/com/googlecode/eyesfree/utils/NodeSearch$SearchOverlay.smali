.class Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;
.super Lcom/googlecode/eyesfree/widget/SimpleOverlay;
.source "NodeSearch.java"

# interfaces
.implements Landroid/content/DialogInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/utils/NodeSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchOverlay"
.end annotation


# instance fields
.field private final mSearchView:Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/StringBuilder;Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "queryText"    # Ljava/lang/StringBuilder;
    .param p3, "textFormatter"    # Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;

    .prologue
    .line 373
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;-><init>(Landroid/content/Context;)V

    .line 375
    new-instance v1, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;

    invoke-direct {v1, p1, p2, p3}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;-><init>(Landroid/content/Context;Ljava/lang/StringBuilder;Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;)V

    iput-object v1, p0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;->mSearchView:Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;

    .line 378
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;->getParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 379
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    const/16 v1, 0x7da

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 380
    invoke-virtual {p0, v0}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;->setParams(Landroid/view/WindowManager$LayoutParams;)V

    .line 382
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;->mSearchView:Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;

    invoke-virtual {p0, v1}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;->setContentView(Landroid/view/View;)V

    .line 383
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 0

    .prologue
    .line 402
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;->dismiss()V

    .line 403
    return-void
.end method

.method public dismiss()V
    .locals 0

    .prologue
    .line 408
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;->hide()V

    .line 409
    return-void
.end method

.method protected onShow()V
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;->mSearchView:Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->show()V

    .line 391
    return-void
.end method

.method public refreshOverlay()V
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchOverlay;->mSearchView:Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->invalidate()V

    .line 398
    return-void
.end method
