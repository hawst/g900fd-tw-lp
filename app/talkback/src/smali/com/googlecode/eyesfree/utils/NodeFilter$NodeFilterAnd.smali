.class Lcom/googlecode/eyesfree/utils/NodeFilter$NodeFilterAnd;
.super Lcom/googlecode/eyesfree/utils/NodeFilter;
.source "NodeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/utils/NodeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NodeFilterAnd"
.end annotation


# instance fields
.field private final mFilters:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/googlecode/eyesfree/utils/NodeFilter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/googlecode/eyesfree/utils/NodeFilter;Lcom/googlecode/eyesfree/utils/NodeFilter;)V
    .locals 1
    .param p1, "lhs"    # Lcom/googlecode/eyesfree/utils/NodeFilter;
    .param p2, "rhs"    # Lcom/googlecode/eyesfree/utils/NodeFilter;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/googlecode/eyesfree/utils/NodeFilter;-><init>()V

    .line 52
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeFilter$NodeFilterAnd;->mFilters:Ljava/util/LinkedList;

    .line 55
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeFilter$NodeFilterAnd;->mFilters:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 56
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeFilter$NodeFilterAnd;->mFilters:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 57
    return-void
.end method


# virtual methods
.method public accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 61
    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/NodeFilter$NodeFilterAnd;->mFilters:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/eyesfree/utils/NodeFilter;

    .line 62
    .local v0, "filter":Lcom/googlecode/eyesfree/utils/NodeFilter;
    invoke-virtual {v0, p1, p2}, Lcom/googlecode/eyesfree/utils/NodeFilter;->accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 63
    const/4 v2, 0x0

    .line 67
    .end local v0    # "filter":Lcom/googlecode/eyesfree/utils/NodeFilter;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public and(Lcom/googlecode/eyesfree/utils/NodeFilter;)Lcom/googlecode/eyesfree/utils/NodeFilter;
    .locals 1
    .param p1, "filter"    # Lcom/googlecode/eyesfree/utils/NodeFilter;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/NodeFilter$NodeFilterAnd;->mFilters:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 74
    return-object p0
.end method
