.class public Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;
.super Ljava/lang/Object;
.source "AccessibilityNodeInfoUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;,
        Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;
    }
.end annotation


# static fields
.field private static final CLASS_TOUCHWIZ_TWABSLISTVIEW:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final CLASS_TOUCHWIZ_TWADAPTERVIEW:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final FILTER_ACCESSIBILITY_FOCUSABLE:Lcom/googlecode/eyesfree/utils/NodeFilter;

.field private static final FILTER_AUTO_SCROLL:Lcom/googlecode/eyesfree/utils/NodeFilter;

.field public static final FILTER_SCROLLABLE:Lcom/googlecode/eyesfree/utils/NodeFilter;

.field private static final FILTER_SCROLL_BACKWARD:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;

.field private static final FILTER_SCROLL_FORWARD:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;

.field public static final FILTER_SHOULD_FOCUS:Lcom/googlecode/eyesfree/utils/NodeFilter;

.field private static final SUPPORTS_VISIBILITY:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->SUPPORTS_VISIBILITY:Z

    .line 53
    const-string v0, "com.sec.android.touchwiz.widget.TwAdapterView"

    invoke-static {v0}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->CLASS_TOUCHWIZ_TWADAPTERVIEW:Ljava/lang/Class;

    .line 60
    const-string v0, "com.sec.android.touchwiz.widget.TwAbsListView"

    invoke-static {v0}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->CLASS_TOUCHWIZ_TWABSLISTVIEW:Ljava/lang/Class;

    .line 1023
    new-instance v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$2;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$2;-><init>()V

    sput-object v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SCROLLABLE:Lcom/googlecode/eyesfree/utils/NodeFilter;

    .line 1030
    new-instance v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$3;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$3;-><init>()V

    sput-object v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_ACCESSIBILITY_FOCUSABLE:Lcom/googlecode/eyesfree/utils/NodeFilter;

    .line 1041
    new-instance v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$4;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$4;-><init>()V

    sput-object v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SHOULD_FOCUS:Lcom/googlecode/eyesfree/utils/NodeFilter;

    .line 1066
    new-instance v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$5;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$5;-><init>()V

    sput-object v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_AUTO_SCROLL:Lcom/googlecode/eyesfree/utils/NodeFilter;

    .line 1076
    new-instance v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;-><init>(I)V

    sput-object v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SCROLL_FORWARD:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;

    .line 1079
    new-instance v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;

    const/16 v1, 0x2000

    invoke-direct {v0, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;-><init>(I)V

    sput-object v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SCROLL_BACKWARD:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;

    return-void

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    return-void
.end method

.method static synthetic access$000(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 45
    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isScrollable(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->CLASS_TOUCHWIZ_TWABSLISTVIEW:Ljava/lang/Class;

    return-object v0
.end method

.method public static findFocusFromHover(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "touched"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 247
    sget-object v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SHOULD_FOCUS:Lcom/googlecode/eyesfree/utils/NodeFilter;

    invoke-static {p0, p1, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getSelfOrMatchingAncestor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    return-object v0
.end method

.method private static getMatchingAncestor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "filter"    # Lcom/googlecode/eyesfree/utils/NodeFilter;

    .prologue
    const/4 v1, 0x0

    .line 475
    if-nez p1, :cond_0

    .line 504
    :goto_0
    return-object v1

    .line 479
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 483
    .local v0, "ancestors":Ljava/util/HashSet;, "Ljava/util/HashSet<Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;>;"
    :try_start_0
    invoke-static {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 484
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getParent()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object p1

    .line 486
    :goto_1
    if-eqz p1, :cond_3

    .line 487
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 489
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 501
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    goto :goto_0

    .line 493
    :cond_1
    :try_start_1
    invoke-virtual {p2, p0, p1}, Lcom/googlecode/eyesfree/utils/NodeFilter;->accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 495
    invoke-static {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 501
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    goto :goto_0

    .line 498
    :cond_2
    :try_start_2
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getParent()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object p1

    goto :goto_1

    .line 501
    :cond_3
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    throw v1
.end method

.method public static getNodeText(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;
    .locals 4
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v2, 0x0

    .line 75
    if-nez p0, :cond_1

    move-object v0, v2

    .line 93
    :cond_0
    :goto_0
    return-object v0

    .line 81
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    .line 82
    .local v0, "contentDescription":Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v3

    if-gtz v3, :cond_0

    .line 87
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 88
    .local v1, "text":Ljava/lang/CharSequence;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {v1}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v3

    if-lez v3, :cond_3

    move-object v0, v1

    .line 90
    goto :goto_0

    :cond_3
    move-object v0, v2

    .line 93
    goto :goto_0
.end method

.method public static getNodeText(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)Ljava/lang/CharSequence;
    .locals 3
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p1, "labelManager"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    .prologue
    .line 107
    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getNodeText(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 108
    .local v1, "text":Ljava/lang/CharSequence;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 119
    .end local v1    # "text":Ljava/lang/CharSequence;
    :goto_0
    return-object v1

    .line 112
    .restart local v1    # "text":Ljava/lang/CharSequence;
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 113
    invoke-virtual {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getViewIdResourceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->getLabelForViewIdFromCache(Ljava/lang/String;)Lcom/googlecode/eyesfree/labeling/Label;

    move-result-object v0

    .line 115
    .local v0, "label":Lcom/googlecode/eyesfree/labeling/Label;
    if-eqz v0, :cond_1

    .line 116
    invoke-virtual {v0}, Lcom/googlecode/eyesfree/labeling/Label;->getText()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 119
    .end local v0    # "label":Lcom/googlecode/eyesfree/labeling/Label;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getRoot(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 2
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 126
    if-nez p0, :cond_0

    .line 127
    const/4 v0, 0x0

    .line 138
    :goto_0
    return-object v0

    .line 130
    :cond_0
    const/4 v0, 0x0

    .line 131
    .local v0, "current":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 134
    .local v1, "parent":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_1
    move-object v0, v1

    .line 135
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getParent()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 136
    if-nez v1, :cond_1

    goto :goto_0
.end method

.method public static getSelfOrMatchingAncestor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "filter"    # Lcom/googlecode/eyesfree/utils/NodeFilter;

    .prologue
    .line 458
    if-nez p1, :cond_0

    .line 459
    const/4 v0, 0x0

    .line 466
    :goto_0
    return-object v0

    .line 462
    :cond_0
    invoke-virtual {p2, p0, p1}, Lcom/googlecode/eyesfree/utils/NodeFilter;->accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 463
    invoke-static {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0

    .line 466
    :cond_1
    invoke-static {p0, p1, p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getMatchingAncestor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0
.end method

.method private static hasMatchingAncestor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "filter"    # Lcom/googlecode/eyesfree/utils/NodeFilter;

    .prologue
    const/4 v1, 0x0

    .line 439
    if-nez p1, :cond_1

    .line 449
    :cond_0
    :goto_0
    return v1

    .line 443
    :cond_1
    invoke-static {p0, p1, p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getMatchingAncestor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 444
    .local v0, "result":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v0, :cond_0

    .line 448
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 449
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static hasNonActionableSpeakingChildren(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v11, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 284
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v1

    .line 286
    .local v1, "childCount":I
    const/4 v0, 0x0

    .line 289
    .local v0, "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_4

    .line 291
    :try_start_0
    invoke-virtual {p1, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 293
    if-nez v0, :cond_0

    .line 294
    const-class v5, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const/4 v6, 0x2

    const-string v7, "Child %d is null, skipping it"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v5, v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 321
    new-array v5, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v5, v4

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 289
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 300
    :cond_0
    :try_start_1
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isVisibleOrLegacy(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 301
    const-class v5, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const/4 v6, 0x2

    const-string v7, "Child %d is invisible, skipping it"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v5, v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 321
    new-array v5, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v5, v4

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_1

    .line 307
    :cond_1
    :try_start_2
    sget-object v5, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_ACCESSIBILITY_FOCUSABLE:Lcom/googlecode/eyesfree/utils/NodeFilter;

    invoke-virtual {v5, p0, v0}, Lcom/googlecode/eyesfree/utils/NodeFilter;->accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 308
    const-class v5, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const/4 v6, 0x2

    const-string v7, "Child %d is focusable, skipping it"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v5, v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 321
    new-array v5, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v5, v4

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_1

    .line 315
    :cond_2
    :try_start_3
    invoke-static {p0, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isSpeakingNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 316
    const-class v5, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const/4 v6, 0x2

    const-string v7, "Does have actionable speaking children (child %d)"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v5, v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 321
    new-array v5, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v5, v4

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 327
    :goto_2
    return v3

    .line 321
    :cond_3
    new-array v5, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v5, v4

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_1

    :catchall_0
    move-exception v5

    new-array v3, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v4

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v5

    .line 325
    :cond_4
    const-class v3, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v5, "Does not have non-actionable speaking children"

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v3, v11, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    move v3, v4

    .line 327
    goto :goto_2
.end method

.method private static hasText(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 2
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v0, 0x0

    .line 530
    if-nez p0, :cond_1

    .line 534
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isAccessibilityFocusable(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 156
    if-nez p1, :cond_1

    .line 187
    :cond_0
    :goto_0
    return v0

    .line 161
    :cond_1
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isVisibleOrLegacy(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 166
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isActionableForAccessibility(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 167
    goto :goto_0

    .line 170
    :cond_2
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_3

    .line 173
    invoke-static {p0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isTopLevelScrollItem(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 174
    goto :goto_0

    .line 179
    :cond_3
    invoke-static {p0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isTopLevelScrollItem(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isSpeakingNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {p0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->hasNonActionableSpeakingChildren(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    .line 182
    goto :goto_0
.end method

.method public static isActionableForAccessibility(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 3
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 345
    if-nez p0, :cond_0

    .line 362
    :goto_0
    return v0

    .line 350
    :cond_0
    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isClickable(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isLongClickable(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    .line 351
    goto :goto_0

    .line 354
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    .line 355
    goto :goto_0

    .line 358
    :cond_3
    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasNativeWebContent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 359
    new-array v2, v1, [I

    aput v1, v2, v0

    invoke-static {p0, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v0

    goto :goto_0

    .line 362
    :cond_4
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {p0, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v0

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x1
        0x400
        0x800
    .end array-data
.end method

.method public static isAutoScrollEdgeListItem(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "direction"    # I

    .prologue
    .line 644
    sget-object v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_AUTO_SCROLL:Lcom/googlecode/eyesfree/utils/NodeFilter;

    invoke-static {p0, p1, p2, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isEdgeListItem(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ILcom/googlecode/eyesfree/utils/NodeFilter;)Z

    move-result v0

    return v0
.end method

.method public static isClickable(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 3
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 397
    if-nez p0, :cond_0

    .line 405
    :goto_0
    return v0

    .line 401
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isClickable()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 402
    goto :goto_0

    .line 405
    :cond_1
    new-array v1, v1, [I

    const/16 v2, 0x10

    aput v2, v1, v0

    invoke-static {p0, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v0

    goto :goto_0
.end method

.method public static isEdgeListItem(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 590
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isEdgeListItem(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ILcom/googlecode/eyesfree/utils/NodeFilter;)Z

    move-result v0

    return v0
.end method

.method public static isEdgeListItem(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ILcom/googlecode/eyesfree/utils/NodeFilter;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "direction"    # I
    .param p3, "filter"    # Lcom/googlecode/eyesfree/utils/NodeFilter;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 610
    if-nez p1, :cond_1

    .line 624
    :cond_0
    :goto_0
    return v0

    .line 614
    :cond_1
    if-gtz p2, :cond_2

    const/4 v2, -0x1

    sget-object v3, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SCROLL_BACKWARD:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;

    invoke-virtual {v3, p3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;->and(Lcom/googlecode/eyesfree/utils/NodeFilter;)Lcom/googlecode/eyesfree/utils/NodeFilter;

    move-result-object v3

    invoke-static {p0, p1, v2, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isMatchingEdgeListItem(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ILcom/googlecode/eyesfree/utils/NodeFilter;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 616
    goto :goto_0

    .line 619
    :cond_2
    if-ltz p2, :cond_0

    sget-object v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SCROLL_FORWARD:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;

    invoke-virtual {v2, p3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;->and(Lcom/googlecode/eyesfree/utils/NodeFilter;)Lcom/googlecode/eyesfree/utils/NodeFilter;

    move-result-object v2

    invoke-static {p0, p1, v1, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isMatchingEdgeListItem(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ILcom/googlecode/eyesfree/utils/NodeFilter;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 621
    goto :goto_0
.end method

.method public static isLongClickable(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 3
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 420
    if-nez p0, :cond_0

    .line 428
    :goto_0
    return v0

    .line 424
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isLongClickable()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 425
    goto :goto_0

    .line 428
    :cond_1
    new-array v1, v1, [I

    const/16 v2, 0x20

    aput v2, v1, v0

    invoke-static {p0, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v0

    goto :goto_0
.end method

.method private static isMatchingEdgeListItem(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ILcom/googlecode/eyesfree/utils/NodeFilter;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "direction"    # I
    .param p3, "filter"    # Lcom/googlecode/eyesfree/utils/NodeFilter;

    .prologue
    const/4 v7, 0x3

    const/4 v8, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 659
    const/4 v0, 0x0

    .line 660
    .local v0, "ancestor":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v1, 0x0

    .line 661
    .local v1, "searched":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v2, 0x0

    .line 664
    .local v2, "searchedAncestor":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v6, 0x0

    :try_start_0
    invoke-static {v6, p1, p3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getMatchingAncestor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 665
    if-nez v0, :cond_0

    .line 691
    new-array v6, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v6, v4

    aput-object v1, v6, v5

    aput-object v2, v6, v8

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 694
    :goto_0
    return v4

    .line 672
    :cond_0
    :try_start_1
    invoke-static {p1, p2}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 674
    :goto_1
    if-eqz v1, :cond_1

    invoke-static {p0, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 675
    move-object v3, v1

    .line 676
    .local v3, "temp":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {v3, p2}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 677
    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 691
    .end local v3    # "temp":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :catchall_0
    move-exception v6

    new-array v7, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v7, v4

    aput-object v1, v7, v5

    aput-object v2, v7, v8

    invoke-static {v7}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v6

    .line 680
    :cond_1
    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v1, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v6

    if-eqz v6, :cond_3

    .line 691
    :cond_2
    new-array v6, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v6, v4

    aput-object v1, v6, v5

    aput-object v2, v6, v8

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v4, v5

    goto :goto_0

    .line 685
    :cond_3
    const/4 v6, 0x0

    :try_start_3
    invoke-static {v6, v1, p3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getMatchingAncestor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    .line 686
    invoke-virtual {v0, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v6

    if-nez v6, :cond_4

    .line 691
    new-array v6, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v6, v4

    aput-object v1, v6, v5

    aput-object v2, v6, v8

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v4, v5

    goto :goto_0

    :cond_4
    new-array v6, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v6, v4

    aput-object v1, v6, v5

    aput-object v2, v6, v8

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0
.end method

.method private static isScrollable(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 514
    invoke-virtual {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isScrollable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 515
    const/4 v0, 0x1

    .line 518
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {p0, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v0

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x1000
        0x2000
    .end array-data
.end method

.method public static isSelfOrAncestorFocused(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 369
    if-nez p1, :cond_0

    .line 370
    const/4 v0, 0x0

    .line 376
    :goto_0
    return v0

    .line 373
    :cond_0
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isAccessibilityFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 374
    const/4 v0, 0x1

    goto :goto_0

    .line 376
    :cond_1
    new-instance v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$1;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$1;-><init>()V

    invoke-static {p0, p1, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->hasMatchingAncestor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Z

    move-result v0

    goto :goto_0
.end method

.method private static isSpeakingNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 252
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->hasText(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 253
    const-class v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v3, "Speaking, has text"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v4, v3, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 279
    :goto_0
    return v0

    .line 259
    :cond_0
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isCheckable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 260
    const-class v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v3, "Speaking, is checkable"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v4, v3, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 266
    :cond_1
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->supportsWebActions(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 267
    const-class v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v3, "Speaking, has web content"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v4, v3, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 273
    :cond_2
    invoke-static {p0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->hasNonActionableSpeakingChildren(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 274
    const-class v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v3, "Speaking, has non-actionable speaking children"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v4, v3, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 279
    goto :goto_0
.end method

.method public static isTopLevelScrollItem(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 546
    if-nez p1, :cond_0

    .line 577
    :goto_0
    return v1

    .line 550
    :cond_0
    const/4 v0, 0x0

    .line 553
    .local v0, "parent":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_start_0
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getParent()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 554
    if-nez v0, :cond_1

    .line 577
    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v1

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 559
    :cond_1
    :try_start_1
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isScrollable(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_2

    .line 577
    new-array v3, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v1

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v1, v2

    goto :goto_0

    .line 568
    :cond_2
    const/4 v3, 0x4

    :try_start_2
    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/widget/AdapterView;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-class v5, Landroid/widget/ScrollView;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-class v5, Landroid/widget/HorizontalScrollView;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    sget-object v5, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->CLASS_TOUCHWIZ_TWADAPTERVIEW:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-static {p0, v0, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesAnyClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/widget/Spinner;

    aput-object v5, v3, v4

    invoke-static {p0, v0, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesAnyClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[Ljava/lang/Class;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    if-nez v3, :cond_3

    .line 577
    new-array v3, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v1

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v1, v2

    goto :goto_0

    :cond_3
    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v1

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    :catchall_0
    move-exception v3

    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v1

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v3
.end method

.method public static isVisibleOrLegacy(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 1009
    sget-boolean v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->SUPPORTS_VISIBILITY:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isVisibleToUser()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static varargs nodeMatchesAnyClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[Ljava/lang/Class;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            "[",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 741
    .local p2, "referenceClasses":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    move-object v0, p2

    .local v0, "arr$":[Ljava/lang/Class;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 742
    .local v3, "referenceClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p0, p1, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 743
    const/4 v4, 0x1

    .line 746
    .end local v3    # "referenceClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_1
    return v4

    .line 741
    .restart local v3    # "referenceClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 746
    .end local v3    # "referenceClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static nodeMatchesClassByName(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/CharSequence;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "referenceClassName"    # Ljava/lang/CharSequence;

    .prologue
    .line 761
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 762
    :cond_0
    const/4 v3, 0x0

    .line 773
    :goto_0
    return v3

    .line 766
    :cond_1
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getClassName()Ljava/lang/CharSequence;

    move-result-object v2

    .line 767
    .local v2, "nodeClassName":Ljava/lang/CharSequence;
    invoke-static {v2, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 768
    const/4 v3, 0x1

    goto :goto_0

    .line 771
    :cond_2
    invoke-static {}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->getInstance()Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    move-result-object v1

    .line 772
    .local v1, "loader":Lcom/googlecode/eyesfree/utils/ClassLoadingManager;
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v0

    .line 773
    .local v0, "appPackage":Ljava/lang/CharSequence;
    invoke-virtual {v1, p0, v2, v0, p2}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->checkInstanceOf(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    goto :goto_0
.end method

.method public static nodeMatchesClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 711
    .local p2, "referenceClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 712
    :cond_0
    const/4 v3, 0x0

    .line 723
    :goto_0
    return v3

    .line 716
    :cond_1
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getClassName()Ljava/lang/CharSequence;

    move-result-object v2

    .line 717
    .local v2, "nodeClassName":Ljava/lang/CharSequence;
    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 718
    const/4 v3, 0x1

    goto :goto_0

    .line 721
    :cond_2
    invoke-static {}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->getInstance()Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    move-result-object v1

    .line 722
    .local v1, "loader":Lcom/googlecode/eyesfree/utils/ClassLoadingManager;
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v0

    .line 723
    .local v0, "appPackage":Ljava/lang/CharSequence;
    invoke-virtual {v1, p0, v2, v0, p2}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->checkInstanceOf(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Class;)Z

    move-result v3

    goto :goto_0
.end method

.method public static recycleNodes(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 782
    .local p0, "nodes":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;>;"
    if-nez p0, :cond_0

    .line 793
    :goto_0
    return-void

    .line 786
    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 787
    .local v1, "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v1, :cond_1

    .line 788
    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_1

    .line 792
    .end local v1    # "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_2
    invoke-interface {p0}, Ljava/util/Collection;->clear()V

    goto :goto_0
.end method

.method public static varargs recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 4
    .param p0, "nodes"    # [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 801
    if-nez p0, :cond_1

    .line 810
    :cond_0
    return-void

    .line 805
    :cond_1
    move-object v0, p0

    .local v0, "arr$":[Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 806
    .local v3, "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v3, :cond_2

    .line 807
    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 805
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static refreshFromChild(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 4
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v3, 0x0

    .line 969
    invoke-virtual {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 970
    invoke-virtual {p0, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 971
    .local v0, "firstChild":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v0, :cond_1

    .line 972
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getParent()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 973
    .local v1, "parent":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 974
    invoke-virtual {p0, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 981
    .end local v0    # "firstChild":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .end local v1    # "parent":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :goto_0
    return-object v1

    .line 977
    .restart local v0    # "firstChild":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .restart local v1    # "parent":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_0
    const/4 v2, 0x1

    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v2, v3

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 981
    .end local v0    # "firstChild":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .end local v1    # "parent":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static refreshFromParent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 6
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 986
    invoke-virtual {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getParent()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v3

    .line 987
    .local v3, "parent":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v3, :cond_2

    .line 989
    :try_start_0
    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v1

    .line 990
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 991
    invoke-virtual {v3, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 992
    .local v0, "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-virtual {p0, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_0

    .line 998
    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 1001
    .end local v0    # "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .end local v1    # "childCount":I
    .end local v2    # "i":I
    :goto_1
    return-object v0

    .line 995
    .restart local v0    # "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .restart local v1    # "childCount":I
    .restart local v2    # "i":I
    :cond_0
    const/4 v4, 0x1

    :try_start_1
    new-array v4, v4, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 990
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 998
    .end local v0    # "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_1
    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 1001
    .end local v1    # "childCount":I
    .end local v2    # "i":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 998
    :catchall_0
    move-exception v4

    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    throw v4
.end method

.method public static refreshNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 1
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 957
    if-nez p0, :cond_1

    .line 958
    const/4 v0, 0x0

    .line 964
    :cond_0
    :goto_0
    return-object v0

    .line 960
    :cond_1
    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->refreshFromChild(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 961
    .local v0, "result":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-nez v0, :cond_0

    .line 962
    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->refreshFromParent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0
.end method

.method public static searchFromBfs(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "filter"    # Lcom/googlecode/eyesfree/utils/NodeFilter;

    .prologue
    const/4 v5, 0x0

    .line 847
    if-nez p1, :cond_1

    .line 874
    :cond_0
    :goto_0
    return-object v5

    .line 851
    :cond_1
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 854
    .local v4, "queue":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;>;"
    invoke-static {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 856
    :cond_2
    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 857
    invoke-virtual {v4}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 859
    .local v3, "item":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-virtual {p2, p0, v3}, Lcom/googlecode/eyesfree/utils/NodeFilter;->accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 860
    invoke-static {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v5

    goto :goto_0

    .line 863
    :cond_3
    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v1

    .line 865
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v1, :cond_2

    .line 866
    invoke-virtual {v3, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 868
    .local v0, "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v0, :cond_4

    .line 869
    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 865
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static searchFromInOrderTraversal(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "root"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "filter"    # Lcom/googlecode/eyesfree/utils/NodeFilter;
    .param p3, "direction"    # I

    .prologue
    .line 933
    invoke-static {p1, p3}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 935
    .local v0, "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 939
    .local v1, "seenNodes":Ljava/util/HashSet;, "Ljava/util/HashSet<Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;>;"
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p2, p0, v0}, Lcom/googlecode/eyesfree/utils/NodeFilter;->accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 940
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 941
    invoke-static {v0, p3}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0

    .line 945
    :cond_0
    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    .line 947
    return-object v0
.end method

.method public static shouldFocusNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x2

    const/4 v0, 0x0

    .line 198
    if-nez p1, :cond_0

    .line 237
    :goto_0
    return v0

    .line 202
    :cond_0
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isVisibleOrLegacy(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 203
    const-class v1, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v2, "Don\'t focus, node is not visible"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v4, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 208
    :cond_1
    sget-object v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_ACCESSIBILITY_FOCUSABLE:Lcom/googlecode/eyesfree/utils/NodeFilter;

    invoke-virtual {v2, p0, p1}, Lcom/googlecode/eyesfree/utils/NodeFilter;->accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 211
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v2

    if-gtz v2, :cond_2

    .line 212
    const-class v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v3, "Focus, node is focusable and has no children"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v4, v3, v0}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 214
    goto :goto_0

    .line 215
    :cond_2
    invoke-static {p0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isSpeakingNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 216
    const-class v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v3, "Focus, node is focusable and has something to speak"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v4, v3, v0}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 218
    goto :goto_0

    .line 220
    :cond_3
    const-class v1, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v2, "Don\'t focus, node is focusable but has nothing to speak"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v4, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 228
    :cond_4
    sget-object v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_ACCESSIBILITY_FOCUSABLE:Lcom/googlecode/eyesfree/utils/NodeFilter;

    invoke-static {p0, p1, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->hasMatchingAncestor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->hasText(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 230
    const-class v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v3, "Focus, node has text and no focusable ancestors"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v4, v3, v0}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 232
    goto :goto_0

    .line 235
    :cond_5
    const-class v1, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v2, "Don\'t focus, failed all focusability tests"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v4, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static varargs supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z
    .locals 6
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p1, "actions"    # [I

    .prologue
    .line 823
    if-eqz p0, :cond_1

    .line 824
    invoke-virtual {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getActions()I

    move-result v4

    .line 826
    .local v4, "supportedActions":I
    move-object v1, p1

    .local v1, "arr$":[I
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget v0, v1, v2

    .line 827
    .local v0, "action":I
    and-int v5, v4, v0

    if-ne v5, v0, :cond_0

    .line 828
    const/4 v5, 0x1

    .line 833
    .end local v0    # "action":I
    .end local v1    # "arr$":[I
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "supportedActions":I
    :goto_1
    return v5

    .line 826
    .restart local v0    # "action":I
    .restart local v1    # "arr$":[I
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v4    # "supportedActions":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 833
    .end local v0    # "action":I
    .end local v1    # "arr$":[I
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "supportedActions":I
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method
