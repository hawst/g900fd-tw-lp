.class Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper$1;
.super Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;
.source "ExploreByTouchHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;


# direct methods
.method constructor <init>(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;)V
    .locals 0

    .prologue
    .line 463
    iput-object p1, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper$1;->this$0:Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    invoke-direct {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;-><init>()V

    return-void
.end method


# virtual methods
.method public createAccessibilityNodeInfo(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 1
    .param p1, "virtualViewId"    # I

    .prologue
    .line 466
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper$1;->this$0:Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    # invokes: Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->getNodeForHost()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->access$000(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 472
    :goto_0
    return-object v0

    .line 468
    :cond_0
    const v0, -0x7fffffff

    if-ne p1, v0, :cond_1

    .line 469
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper$1;->this$0:Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    # invokes: Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->getNodeForRoot()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->access$100(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0

    .line 472
    :cond_1
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper$1;->this$0:Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    # invokes: Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->getNodeForVirtualViewId(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {v0, p1}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->access$200(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0
.end method

.method public performAction(IILandroid/os/Bundle;)Z
    .locals 3
    .param p1, "virtualViewId"    # I
    .param p2, "action"    # I
    .param p3, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 477
    const/4 v0, 0x0

    .line 479
    .local v0, "handled":Z
    sparse-switch p2, :sswitch_data_0

    .line 507
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 508
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper$1;->this$0:Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    # getter for: Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;
    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->access$400(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;)Landroid/view/View;

    move-result-object v1

    invoke-static {v1, p2, p3}, Landroid/support/v4/view/ViewCompat;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v1

    .line 516
    :goto_0
    return v1

    .line 483
    :sswitch_0
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper$1;->this$0:Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    # getter for: Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mFocusedVirtualViewId:I
    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->access$300(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;)I

    move-result v1

    if-eq v1, p1, :cond_0

    .line 484
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper$1;->this$0:Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    # setter for: Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mFocusedVirtualViewId:I
    invoke-static {v1, p1}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->access$302(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;I)I

    .line 485
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper$1;->this$0:Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    # getter for: Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;
    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->access$400(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 486
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper$1;->this$0:Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    const v2, 0x8000

    invoke-virtual {v1, p1, v2}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->sendEventForVirtualViewId(II)Z

    .line 488
    const/4 v0, 0x1

    .line 514
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper$1;->this$0:Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    invoke-virtual {v1, p1, p2, p3}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->performActionForVirtualViewId(IILandroid/os/Bundle;)Z

    move-result v1

    or-int/2addr v0, v1

    move v1, v0

    .line 516
    goto :goto_0

    .line 492
    :sswitch_1
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper$1;->this$0:Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    # getter for: Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mFocusedVirtualViewId:I
    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->access$300(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;)I

    move-result v1

    if-ne v1, p1, :cond_1

    .line 493
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper$1;->this$0:Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    const/high16 v2, -0x80000000

    # setter for: Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mFocusedVirtualViewId:I
    invoke-static {v1, v2}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->access$302(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;I)I

    .line 499
    :cond_1
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper$1;->this$0:Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    # getter for: Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->mHost:Landroid/view/View;
    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->access$400(Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 500
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper$1;->this$0:Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;

    const/high16 v2, 0x10000

    invoke-virtual {v1, p1, v2}, Lcom/googlecode/eyesfree/utils/ExploreByTouchHelper;->sendEventForVirtualViewId(II)Z

    .line 502
    const/4 v0, 0x1

    .line 503
    goto :goto_1

    .line 479
    nop

    :sswitch_data_0
    .sparse-switch
        0x40 -> :sswitch_0
        0x80 -> :sswitch_1
    .end sparse-switch
.end method
