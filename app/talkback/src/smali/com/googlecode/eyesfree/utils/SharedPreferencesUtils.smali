.class public Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;
.super Ljava/lang/Object;
.source "SharedPreferencesUtils.java"


# direct methods
.method public static getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z
    .locals 2
    .param p0, "prefs"    # Landroid/content/SharedPreferences;
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "keyResId"    # I
    .param p3, "defaultResId"    # I

    .prologue
    .line 91
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getFloatFromStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)F
    .locals 3
    .param p0, "prefs"    # Landroid/content/SharedPreferences;
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "keyResId"    # I
    .param p3, "defaultResId"    # I

    .prologue
    .line 60
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "strPref":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    return v1
.end method

.method public static getIntFromStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)I
    .locals 3
    .param p0, "prefs"    # Landroid/content/SharedPreferences;
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "keyResId"    # I
    .param p3, "defaultResId"    # I

    .prologue
    .line 41
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "strPref":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public static getStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Ljava/lang/String;
    .locals 2
    .param p0, "prefs"    # Landroid/content/SharedPreferences;
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "keyResId"    # I
    .param p3, "defaultResId"    # I

    .prologue
    .line 76
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-nez p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static putBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;IZ)V
    .locals 2
    .param p0, "prefs"    # Landroid/content/SharedPreferences;
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "keyResId"    # I
    .param p3, "value"    # Z

    .prologue
    .line 104
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 105
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 106
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 107
    return-void
.end method

.method public static storeBooleanAsync(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V
    .locals 3
    .param p0, "prefs"    # Landroid/content/SharedPreferences;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 120
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_0

    .line 121
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 122
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 123
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 134
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :goto_0
    return-void

    .line 125
    :cond_0
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils$1;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
