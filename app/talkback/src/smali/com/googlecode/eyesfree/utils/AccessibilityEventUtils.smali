.class public Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;
.super Ljava/lang/Object;
.source "AccessibilityEventUtils.java"


# direct methods
.method public static eventEquals(Landroid/view/accessibility/AccessibilityEvent;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 6
    .param p0, "first"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p1, "second"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v0, 0x0

    .line 131
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 203
    :cond_0
    :goto_0
    return v0

    .line 134
    :cond_1
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 137
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_6

    .line 138
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_0

    .line 144
    :cond_2
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getClassName()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_7

    .line 145
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getClassName()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_0

    .line 151
    :cond_3
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_8

    .line 156
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_0

    .line 162
    :cond_4
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getBeforeText()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_9

    .line 163
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getBeforeText()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_0

    .line 169
    :cond_5
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getParcelableData()Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_0

    .line 173
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getAddedCount()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getAddedCount()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 176
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->isChecked()Z

    move-result v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->isChecked()Z

    move-result v2

    if-ne v1, v2, :cond_0

    .line 179
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->isEnabled()Z

    move-result v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->isEnabled()Z

    move-result v2

    if-ne v1, v2, :cond_0

    .line 182
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 185
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->isFullScreen()Z

    move-result v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->isFullScreen()Z

    move-result v2

    if-ne v1, v2, :cond_0

    .line 188
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getCurrentItemIndex()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getCurrentItemIndex()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 191
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getItemCount()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getItemCount()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 194
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->isPassword()Z

    move-result v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->isPassword()Z

    move-result v2

    if-ne v1, v2, :cond_0

    .line 197
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getRemovedCount()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getRemovedCount()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 200
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 203
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 141
    :cond_6
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_0

    .line 148
    :cond_7
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getClassName()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getClassName()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    .line 159
    :cond_8
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    .line 166
    :cond_9
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getBeforeText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getBeforeText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0
.end method

.method public static eventMatchesAnyType(Landroid/view/accessibility/AccessibilityEvent;I)Z
    .locals 2
    .param p0, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p1, "typeMask"    # I

    .prologue
    const/4 v0, 0x0

    .line 74
    if-nez p0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v1

    and-int/2addr v1, p1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static eventMatchesClass(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/String;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "referenceClassName"    # Ljava/lang/String;

    .prologue
    .line 52
    if-nez p1, :cond_0

    .line 53
    const/4 v3, 0x0

    .line 60
    :goto_0
    return v3

    .line 56
    :cond_0
    invoke-static {}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->getInstance()Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    move-result-object v2

    .line 57
    .local v2, "loader":Lcom/googlecode/eyesfree/utils/ClassLoadingManager;
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getClassName()Ljava/lang/CharSequence;

    move-result-object v1

    .line 58
    .local v1, "eventClassName":Ljava/lang/CharSequence;
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v0

    .line 60
    .local v0, "appPackage":Ljava/lang/CharSequence;
    invoke-virtual {v2, p0, v1, v0, p2}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->checkInstanceOf(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    goto :goto_0
.end method

.method public static getEventAggregateText(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 6
    .param p0, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 111
    if-nez p0, :cond_1

    .line 112
    const/4 v0, 0x0

    .line 123
    :cond_0
    return-object v0

    .line 115
    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 116
    .local v0, "aggregator":Landroid/text/SpannableStringBuilder;
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    .line 117
    .local v1, "eventText":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 119
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/CharSequence;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 120
    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/CharSequence;

    const/4 v5, 0x0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    aput-object v3, v4, v5

    invoke-static {v0, v4}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method public static getEventTextOrDescription(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 2
    .param p0, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 90
    if-nez p0, :cond_1

    .line 91
    const/4 v0, 0x0

    .line 100
    :cond_0
    :goto_0
    return-object v0

    .line 94
    :cond_1
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    .line 96
    .local v0, "contentDescription":Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->getEventAggregateText(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method
