.class public Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;
.super Ljava/lang/Object;
.source "WebInterfaceUtils.java"


# direct methods
.method public static hasLegacyWebContent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 3
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v1, 0x0

    .line 309
    if-nez p0, :cond_1

    .line 333
    :cond_0
    :goto_0
    return v1

    .line 313
    :cond_1
    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->supportsWebActions(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 319
    invoke-virtual {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getParent()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 320
    .local v0, "parent":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->supportsWebActions(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 321
    if-eqz v0, :cond_0

    .line 322
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    .line 328
    :cond_2
    if-eqz v0, :cond_3

    .line 329
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 333
    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static hasNativeWebContent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 4
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 276
    if-nez p0, :cond_1

    .line 299
    :cond_0
    :goto_0
    return v2

    .line 280
    :cond_1
    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->supportsWebActions(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 286
    invoke-virtual {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getParent()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 287
    .local v0, "parent":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->supportsWebActions(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 288
    if-eqz v0, :cond_2

    .line 289
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    :cond_2
    move v2, v1

    .line 291
    goto :goto_0

    .line 294
    :cond_3
    if-eqz v0, :cond_4

    .line 295
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 299
    :cond_4
    invoke-virtual {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v3

    if-lez v3, :cond_5

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_5
    move v1, v2

    goto :goto_1
.end method

.method public static hasNavigableWebContent(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 356
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->supportsWebActions(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->isScriptInjectionEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasNativeWebContent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isScriptInjectionEnabled(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 341
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "accessibility_script_injection"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 343
    .local v0, "injectionSetting":I
    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public static performNavigationAtGranularityAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;II)Z
    .locals 3
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p1, "direction"    # I
    .param p2, "granularity"    # I

    .prologue
    .line 180
    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    const/16 v0, 0x100

    .line 183
    .local v0, "action":I
    :goto_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 184
    .local v1, "args":Landroid/os/Bundle;
    const-string v2, "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 186
    invoke-virtual {p0, v0, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(ILandroid/os/Bundle;)Z

    move-result v2

    return v2

    .line 180
    .end local v0    # "action":I
    .end local v1    # "args":Landroid/os/Bundle;
    :cond_0
    const/16 v0, 0x200

    goto :goto_0
.end method

.method public static performNavigationToHtmlElementAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ILjava/lang/String;)Z
    .locals 3
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p1, "direction"    # I
    .param p2, "htmlElement"    # Ljava/lang/String;

    .prologue
    .line 135
    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    const/16 v0, 0x400

    .line 138
    .local v0, "action":I
    :goto_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 139
    .local v1, "args":Landroid/os/Bundle;
    const-string v2, "ACTION_ARGUMENT_HTML_ELEMENT_STRING"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-virtual {p0, v0, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(ILandroid/os/Bundle;)Z

    move-result v2

    return v2

    .line 135
    .end local v0    # "action":I
    .end local v1    # "args":Landroid/os/Bundle;
    :cond_0
    const/16 v0, 0x800

    goto :goto_0
.end method

.method public static performSpecialAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Z
    .locals 1
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p1, "action"    # I

    .prologue
    .line 205
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performSpecialAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;II)Z

    move-result v0

    return v0
.end method

.method public static performSpecialAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;II)Z
    .locals 1
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p1, "action"    # I
    .param p2, "direction"    # I

    .prologue
    .line 235
    invoke-static {p0, p2, p1}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performNavigationAtGranularityAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;II)Z

    move-result v0

    return v0
.end method

.method public static setSpecialContentModeEnabled(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)Z
    .locals 2
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p1, "enabled"    # Z

    .prologue
    .line 253
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 254
    .local v0, "direction":I
    :goto_0
    const/4 v1, -0x4

    invoke-static {p0, v1, v0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performSpecialAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;II)Z

    move-result v1

    return v1

    .line 253
    .end local v0    # "direction":I
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static supportsWebActions(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 264
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {p0, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v0

    return v0

    nop

    :array_0
    .array-data 4
        0x400
        0x800
    .end array-data
.end method
