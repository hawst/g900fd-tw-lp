.class public Lcom/googlecode/eyesfree/utils/SparseIterableArray;
.super Landroid/support/v4/util/SparseArrayCompat;
.source "SparseIterableArray.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/utils/SparseIterableArray$1;,
        Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/support/v4/util/SparseArrayCompat",
        "<TT;>;",
        "Ljava/lang/Iterable",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    .local p0, "this":Lcom/googlecode/eyesfree/utils/SparseIterableArray;, "Lcom/googlecode/eyesfree/utils/SparseIterableArray<TT;>;"
    invoke-direct {p0}, Landroid/support/v4/util/SparseArrayCompat;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "this":Lcom/googlecode/eyesfree/utils/SparseIterableArray;, "Lcom/googlecode/eyesfree/utils/SparseIterableArray<TT;>;"
    new-instance v0, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;-><init>(Lcom/googlecode/eyesfree/utils/SparseIterableArray;Lcom/googlecode/eyesfree/utils/SparseIterableArray$1;)V

    return-object v0
.end method
