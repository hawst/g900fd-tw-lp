.class Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;
.super Landroid/view/SurfaceView;
.source "NodeSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/utils/NodeSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchView"
.end annotation


# instance fields
.field private final mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

.field private mHolder:Landroid/view/SurfaceHolder;

.field private final mQueryText:Ljava/lang/StringBuilder;

.field private final mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

.field private final mTextFormatter:Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/StringBuilder;Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "queryText"    # Ljava/lang/StringBuilder;
    .param p3, "textFormatter"    # Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;

    .prologue
    .line 455
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 435
    new-instance v2, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView$1;

    invoke-direct {v2, p0}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView$1;-><init>(Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;)V

    iput-object v2, p0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    .line 457
    iput-object p2, p0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->mQueryText:Ljava/lang/StringBuilder;

    .line 458
    iput-object p3, p0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->mTextFormatter:Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;

    .line 460
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    .line 461
    .local v1, "holder":Landroid/view/SurfaceHolder;
    const/4 v2, -0x3

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 462
    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 465
    const/4 v2, 0x2

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    .line 466
    .local v0, "colors":[I
    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    invoke-direct {v2, v3, v0}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    iput-object v2, p0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

    .line 467
    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setGradientType(I)V

    .line 468
    return-void

    .line 465
    nop

    :array_0
    .array-data 4
        0x77000000
        0x77000000
    .end array-data
.end method

.method static synthetic access$002(Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0
    .param p0, "x0"    # Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;
    .param p1, "x1"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 415
    iput-object p1, p0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->mHolder:Landroid/view/SurfaceHolder;

    return-object p1
.end method


# virtual methods
.method public invalidate()V
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 476
    invoke-super {p0}, Landroid/view/SurfaceView;->invalidate()V

    .line 478
    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->mHolder:Landroid/view/SurfaceHolder;

    .line 479
    .local v2, "holder":Landroid/view/SurfaceHolder;
    if-nez v2, :cond_1

    .line 515
    :cond_0
    :goto_0
    return-void

    .line 483
    :cond_1
    invoke-interface {v2}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 484
    .local v0, "canvas":Landroid/graphics/Canvas;
    if-eqz v0, :cond_0

    .line 489
    sget-object v5, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v6, v5}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 491
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->getVisibility()I

    move-result v5

    if-eqz v5, :cond_2

    .line 492
    invoke-interface {v2, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 496
    :cond_2
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->getWidth()I

    move-result v4

    .line 497
    .local v4, "width":I
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->getHeight()I

    move-result v1

    .line 500
    .local v1, "height":I
    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v5, v6, v6, v4, v1}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    .line 501
    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v5, v0}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 503
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 504
    .local v3, "paint":Landroid/graphics/Paint;
    const/4 v5, -0x1

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 505
    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 506
    sget-object v5, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 507
    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->mTextFormatter:Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;

    invoke-interface {v5}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;->getTextSize()F

    move-result v5

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 508
    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->mTextFormatter:Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;

    iget-object v6, p0, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->mQueryText:Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;->getDisplayText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    int-to-float v6, v4

    div-float/2addr v6, v8

    int-to-float v7, v1

    div-float/2addr v7, v8

    invoke-virtual {v0, v5, v6, v7, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 514
    invoke-interface {v2, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public show()V
    .locals 0

    .prologue
    .line 471
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/NodeSearch$SearchView;->invalidate()V

    .line 472
    return-void
.end method
