.class final enum Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;
.super Ljava/lang/Enum;
.source "ThreeDSensorTapDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "SensorDetectorState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

.field public static final enum DEFINITE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

.field public static final enum NO_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

.field public static final enum POSSIBLE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

.field public static final enum PROCESSING_CANDIDATE_DEFINITE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

.field public static final enum PROCESSING_CANDIDATE_POSSIBLE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

.field public static final enum TOO_NOISY:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 355
    new-instance v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    const-string v1, "NO_TAP"

    invoke-direct {v0, v1, v3}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->NO_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    .line 358
    new-instance v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    const-string v1, "DEFINITE_TAP"

    invoke-direct {v0, v1, v4}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->DEFINITE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    .line 361
    new-instance v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    const-string v1, "POSSIBLE_TAP"

    invoke-direct {v0, v1, v5}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->POSSIBLE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    .line 364
    new-instance v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    const-string v1, "TOO_NOISY"

    invoke-direct {v0, v1, v6}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->TOO_NOISY:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    .line 367
    new-instance v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    const-string v1, "PROCESSING_CANDIDATE_POSSIBLE_TAP"

    invoke-direct {v0, v1, v7}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->PROCESSING_CANDIDATE_POSSIBLE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    .line 370
    new-instance v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    const-string v1, "PROCESSING_CANDIDATE_DEFINITE_TAP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->PROCESSING_CANDIDATE_DEFINITE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    .line 353
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    sget-object v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->NO_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->DEFINITE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->POSSIBLE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->TOO_NOISY:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->PROCESSING_CANDIDATE_POSSIBLE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->PROCESSING_CANDIDATE_DEFINITE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->$VALUES:[Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 353
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 353
    const-class v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    return-object v0
.end method

.method public static values()[Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;
    .locals 1

    .prologue
    .line 353
    sget-object v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->$VALUES:[Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    invoke-virtual {v0}, [Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    return-object v0
.end method
