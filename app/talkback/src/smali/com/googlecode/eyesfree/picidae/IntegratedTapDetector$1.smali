.class Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$1;
.super Ljava/lang/Object;
.source "IntegratedTapDetector.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->sendDoubleTapToListeners(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

.field final synthetic val$finalListener:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;

.field final synthetic val$finalTimestamp:J


# direct methods
.method constructor <init>(Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;J)V
    .locals 1

    .prologue
    .line 456
    iput-object p1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$1;->this$0:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

    iput-object p2, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$1;->val$finalListener:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;

    iput-wide p3, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$1;->val$finalTimestamp:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 459
    iget-object v0, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$1;->val$finalListener:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;

    iget-wide v2, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$1;->val$finalTimestamp:J

    invoke-interface {v0, v2, v3}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;->onDoubleTap(J)V

    .line 460
    return-void
.end method
