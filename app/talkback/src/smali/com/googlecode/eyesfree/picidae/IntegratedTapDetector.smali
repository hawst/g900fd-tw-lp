.class public Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;
.super Ljava/lang/Object;
.source "IntegratedTapDetector.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$TapListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$3;,
        Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;,
        Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;,
        Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;,
        Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    }
.end annotation


# instance fields
.field private final mAccelTapDetector:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;

.field private final mAccelTapEventQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

.field private final mGyroTapDetector:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;

.field private final mGyroTapEventQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field private mHaveReportedAtLeastOneTap:Z

.field private final mIntegratedTapEventQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;",
            ">;"
        }
    .end annotation
.end field

.field private mLastReportedTapTime:J

.field private final mListenerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field private mMaxDoubleTapSpacingNanos:J

.field private mMinDoubleTapQuality:D

.field private mMinTapQuality:D

.field private mMinTapSpacingNanos:J

.field private mPostDelayTime:J

.field private mRawDataOutputStream:Lcom/google/common/io/LittleEndianDataOutputStream;

.field private final mSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method public constructor <init>(Landroid/hardware/SensorManager;)V
    .locals 1
    .param p1, "sensorManager"    # Landroid/hardware/SensorManager;

    .prologue
    const/4 v0, 0x0

    .line 168
    invoke-direct {p0, p1, v0, v0}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;-><init>(Landroid/hardware/SensorManager;Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;)V

    .line 169
    return-void
.end method

.method public constructor <init>(Landroid/hardware/SensorManager;Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;)V
    .locals 10
    .param p1, "sensorManager"    # Landroid/hardware/SensorManager;
    .param p2, "accelTapDetector"    # Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;
    .param p3, "gyroTapDetector"    # Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    iput-object v4, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mAccelTapEventQueue:Ljava/util/Queue;

    .line 121
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    iput-object v4, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mGyroTapEventQueue:Ljava/util/Queue;

    .line 124
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    iput-object v4, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mIntegratedTapEventQueue:Ljava/util/Queue;

    .line 139
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mListenerMap:Ljava/util/Map;

    .line 143
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mRawDataOutputStream:Lcom/google/common/io/LittleEndianDataOutputStream;

    .line 146
    sget-object v4, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;->INTEGRATED_TAP_DETECTOR:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    iput-object v4, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mCurrentTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    .line 152
    iput-boolean v6, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mHaveReportedAtLeastOneTap:Z

    .line 155
    const-wide/32 v4, 0x5f5e100

    iput-wide v4, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mMinTapSpacingNanos:J

    .line 158
    iput-wide v8, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mMaxDoubleTapSpacingNanos:J

    .line 161
    iput-wide v8, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mPostDelayTime:J

    .line 174
    iput-object p1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mSensorManager:Landroid/hardware/SensorManager;

    .line 176
    new-instance v3, Landroid/os/HandlerThread;

    const-string v4, "AccelGyroAudioTapDetector"

    const/16 v5, -0x14

    invoke-direct {v3, v4, v5}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 177
    .local v3, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v3}, Landroid/os/HandlerThread;->start()V

    .line 178
    new-instance v4, Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v4, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mHandler:Landroid/os/Handler;

    .line 179
    iput-boolean v6, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mHaveReportedAtLeastOneTap:Z

    .line 180
    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    iput-wide v4, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mMinTapQuality:D

    .line 181
    const-wide v4, 0x3fc3333333333333L    # 0.15

    iput-wide v4, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mMinDoubleTapQuality:D

    .line 183
    if-nez p2, :cond_1

    .line 184
    iget-object v4, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    .line 185
    .local v0, "accelerometer":Landroid/hardware/Sensor;
    const/high16 v2, 0x41f00000    # 30.0f

    .line 186
    .local v2, "maxRange":F
    if-eqz v0, :cond_0

    .line 187
    invoke-virtual {v0}, Landroid/hardware/Sensor;->getMaximumRange()F

    move-result v2

    .line 190
    :cond_0
    new-instance p2, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;

    .end local p2    # "accelTapDetector":Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;
    sget-object v4, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->ACCELEROMETER:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    invoke-direct {p2, p0, v2, v4}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;-><init>(Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$TapListener;FLcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;)V

    .line 194
    .end local v0    # "accelerometer":Landroid/hardware/Sensor;
    .end local v2    # "maxRange":F
    .restart local p2    # "accelTapDetector":Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;
    :cond_1
    if-nez p3, :cond_3

    .line 195
    iget-object v4, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    .line 196
    .local v1, "gyroscope":Landroid/hardware/Sensor;
    const/high16 v2, 0x41000000    # 8.0f

    .line 197
    .restart local v2    # "maxRange":F
    if-eqz v1, :cond_2

    .line 198
    invoke-virtual {v1}, Landroid/hardware/Sensor;->getMaximumRange()F

    move-result v2

    .line 201
    :cond_2
    new-instance p3, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;

    .end local p3    # "gyroTapDetector":Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;
    sget-object v4, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->GYROSCOPE:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    invoke-direct {p3, p0, v2, v4}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;-><init>(Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$TapListener;FLcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;)V

    .line 205
    .end local v1    # "gyroscope":Landroid/hardware/Sensor;
    .end local v2    # "maxRange":F
    .restart local p3    # "gyroTapDetector":Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;
    :cond_3
    iput-object p2, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mAccelTapDetector:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;

    .line 206
    iput-object p3, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mGyroTapDetector:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;

    .line 207
    return-void
.end method

.method private emitTapsFromQueues(J)V
    .locals 1
    .param p1, "timestamp"    # J

    .prologue
    .line 498
    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->integrateAccelAndGyroQueues(J)V

    .line 500
    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->processIntegratedQueueAsSingleAndDoubleTaps(J)V

    .line 501
    return-void
.end method

.method private integrateAccelAndGyroQueues(J)V
    .locals 13
    .param p1, "timestamp"    # J

    .prologue
    .line 514
    :goto_0
    iget-object v6, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mGyroTapEventQueue:Ljava/util/Queue;

    invoke-interface {v6}, Ljava/util/Queue;->size()I

    move-result v6

    if-lez v6, :cond_2

    iget-object v6, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mAccelTapEventQueue:Ljava/util/Queue;

    invoke-interface {v6}, Ljava/util/Queue;->size()I

    move-result v6

    if-lez v6, :cond_2

    .line 515
    iget-object v6, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mGyroTapEventQueue:Ljava/util/Queue;

    invoke-interface {v6}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;

    iget-wide v8, v6, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->nanos:J

    iget-object v6, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mAccelTapEventQueue:Ljava/util/Queue;

    invoke-interface {v6}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;

    iget-wide v6, v6, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->nanos:J

    const-wide/32 v10, 0x5f5e100

    add-long/2addr v6, v10

    cmp-long v6, v8, v6

    if-lez v6, :cond_0

    .line 517
    iget-object v6, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mIntegratedTapEventQueue:Ljava/util/Queue;

    iget-object v7, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mAccelTapEventQueue:Ljava/util/Queue;

    invoke-interface {v7}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 521
    :cond_0
    iget-object v6, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mAccelTapEventQueue:Ljava/util/Queue;

    invoke-interface {v6}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;

    iget-wide v8, v6, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->nanos:J

    iget-object v6, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mGyroTapEventQueue:Ljava/util/Queue;

    invoke-interface {v6}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;

    iget-wide v6, v6, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->nanos:J

    const-wide/32 v10, 0x5f5e100

    add-long/2addr v6, v10

    cmp-long v6, v8, v6

    if-lez v6, :cond_1

    .line 523
    iget-object v6, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mIntegratedTapEventQueue:Ljava/util/Queue;

    iget-object v7, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mGyroTapEventQueue:Ljava/util/Queue;

    invoke-interface {v7}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 531
    :cond_1
    iget-object v6, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mAccelTapEventQueue:Ljava/util/Queue;

    invoke-interface {v6}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;

    .line 532
    .local v0, "accelTap":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;
    iget-object v6, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mGyroTapEventQueue:Ljava/util/Queue;

    invoke-interface {v6}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;

    .line 533
    .local v1, "gyroTap":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;
    new-instance v2, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;

    iget-wide v6, v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->quality:D

    iget-wide v8, v1, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->quality:D

    add-double/2addr v6, v8

    iget-wide v8, v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->nanos:J

    iget-wide v10, v1, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->nanos:J

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    invoke-direct {v2, v6, v7, v8, v9}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;-><init>(DJ)V

    .line 535
    .local v2, "integratedTap":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;
    iget-object v6, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mIntegratedTapEventQueue:Ljava/util/Queue;

    invoke-interface {v6, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 542
    .end local v0    # "accelTap":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;
    .end local v1    # "gyroTap":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;
    .end local v2    # "integratedTap":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;
    :cond_2
    iget-object v6, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mGyroTapEventQueue:Ljava/util/Queue;

    invoke-interface {v6}, Ljava/util/Queue;->size()I

    move-result v6

    if-lez v6, :cond_4

    iget-object v3, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mGyroTapEventQueue:Ljava/util/Queue;

    .line 544
    .local v3, "nonEmptyTapQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;>;"
    :goto_1
    invoke-interface {v3}, Ljava/util/Queue;->size()I

    move-result v6

    if-lez v6, :cond_3

    .line 545
    const-wide/32 v6, 0x5f5e100

    sub-long v6, p1, v6

    const-wide/32 v8, 0x5f5e100

    sub-long v4, v6, v8

    .line 547
    .local v4, "latestTimeThatCantBeADoubleTap":J
    invoke-interface {v3}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;

    iget-wide v6, v6, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->nanos:J

    cmp-long v6, v6, v4

    if-lez v6, :cond_5

    .line 559
    .end local v4    # "latestTimeThatCantBeADoubleTap":J
    :cond_3
    return-void

    .line 542
    .end local v3    # "nonEmptyTapQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;>;"
    :cond_4
    iget-object v3, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mAccelTapEventQueue:Ljava/util/Queue;

    goto :goto_1

    .line 557
    .restart local v3    # "nonEmptyTapQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;>;"
    .restart local v4    # "latestTimeThatCantBeADoubleTap":J
    :cond_5
    iget-object v6, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mIntegratedTapEventQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private processIntegratedQueueAsSingleAndDoubleTaps(J)V
    .locals 15
    .param p1, "timestamp"    # J

    .prologue
    .line 567
    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mIntegratedTapEventQueue:Ljava/util/Queue;

    invoke-interface {v7}, Ljava/util/Queue;->size()I

    move-result v7

    const/4 v8, 0x2

    if-lt v7, v8, :cond_5

    .line 568
    iget-object v7, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mIntegratedTapEventQueue:Ljava/util/Queue;

    invoke-interface {v7}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;

    .line 569
    .local v3, "olderTap":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;
    iget-wide v8, v3, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->nanos:J

    invoke-direct {p0, v8, v9}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->tapAllowedAt(J)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 577
    iget-object v7, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mIntegratedTapEventQueue:Ljava/util/Queue;

    invoke-interface {v7}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;

    .line 578
    .local v2, "newerTap":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;
    iget-wide v8, v2, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->nanos:J

    iget-wide v10, v3, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->nanos:J

    iget-wide v12, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mMaxDoubleTapSpacingNanos:J

    add-long/2addr v10, v12

    cmp-long v7, v8, v10

    if-gez v7, :cond_4

    .line 583
    iget-wide v8, v2, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->quality:D

    iget-wide v10, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mMinTapQuality:D

    cmpl-double v7, v8, v10

    if-ltz v7, :cond_2

    iget-wide v8, v3, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->quality:D

    iget-wide v10, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mMinDoubleTapQuality:D

    cmpl-double v7, v8, v10

    if-ltz v7, :cond_2

    const/4 v4, 0x1

    .line 585
    .local v4, "qualityGoodEnough1":Z
    :goto_1
    iget-wide v8, v2, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->quality:D

    iget-wide v10, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mMinDoubleTapQuality:D

    cmpl-double v7, v8, v10

    if-ltz v7, :cond_3

    iget-wide v8, v3, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->quality:D

    iget-wide v10, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mMinTapQuality:D

    cmpl-double v7, v8, v10

    if-ltz v7, :cond_3

    const/4 v5, 0x1

    .line 587
    .local v5, "qualityGoodEnough2":Z
    :goto_2
    if-nez v4, :cond_1

    if-eqz v5, :cond_4

    .line 588
    :cond_1
    iget-wide v8, v3, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->nanos:J

    invoke-virtual {p0, v8, v9}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->sendDoubleTapToListeners(J)V

    .line 589
    iget-object v7, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mIntegratedTapEventQueue:Ljava/util/Queue;

    invoke-interface {v7}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    goto :goto_0

    .line 583
    .end local v4    # "qualityGoodEnough1":Z
    .end local v5    # "qualityGoodEnough2":Z
    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    .line 585
    .restart local v4    # "qualityGoodEnough1":Z
    :cond_3
    const/4 v5, 0x0

    goto :goto_2

    .line 595
    .end local v4    # "qualityGoodEnough1":Z
    :cond_4
    iget-wide v8, v3, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->quality:D

    iget-wide v10, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mMinTapQuality:D

    cmpl-double v7, v8, v10

    if-ltz v7, :cond_0

    .line 596
    iget-wide v8, v3, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->nanos:J

    invoke-virtual {p0, v8, v9}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->sendSingleTapToListeners(J)V

    goto :goto_0

    .line 603
    .end local v2    # "newerTap":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;
    .end local v3    # "olderTap":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;
    :cond_5
    iget-object v7, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mIntegratedTapEventQueue:Ljava/util/Queue;

    invoke-interface {v7}, Ljava/util/Queue;->size()I

    move-result v7

    if-nez v7, :cond_7

    .line 620
    :cond_6
    :goto_3
    return-void

    .line 608
    :cond_7
    iget-wide v8, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mMaxDoubleTapSpacingNanos:J

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-lez v7, :cond_8

    const-wide/32 v0, 0xbebc200

    .line 610
    .local v0, "maxOffsetToConsider":J
    :goto_4
    iget-object v7, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mIntegratedTapEventQueue:Ljava/util/Queue;

    invoke-interface {v7}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;

    iget-wide v8, v7, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->nanos:J

    iget-wide v10, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mMaxDoubleTapSpacingNanos:J

    sub-long v10, p1, v10

    sub-long/2addr v10, v0

    cmp-long v7, v8, v10

    if-gtz v7, :cond_6

    .line 612
    iget-object v7, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mIntegratedTapEventQueue:Ljava/util/Queue;

    invoke-interface {v7}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;

    .line 613
    .local v6, "tap":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;
    iget-wide v8, v6, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->quality:D

    iget-wide v10, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mMinTapQuality:D

    cmpl-double v7, v8, v10

    if-ltz v7, :cond_6

    iget-wide v8, v6, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->nanos:J

    invoke-direct {p0, v8, v9}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->tapAllowedAt(J)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 614
    iget-wide v8, v6, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->nanos:J

    invoke-virtual {p0, v8, v9}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->sendSingleTapToListeners(J)V

    goto :goto_3

    .line 608
    .end local v0    # "maxOffsetToConsider":J
    .end local v6    # "tap":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;
    :cond_8
    const-wide/16 v0, 0x0

    goto :goto_4
.end method

.method private tapAllowedAt(J)Z
    .locals 5
    .param p1, "timestamp"    # J

    .prologue
    .line 624
    iget-wide v0, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mLastReportedTapTime:J

    sub-long v0, p1, v0

    iget-wide v2, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mMinTapSpacingNanos:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-boolean v0, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mHaveReportedAtLeastOneTap:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;

    .prologue
    .line 383
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-virtual {p0, p1, v0}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->addListener(Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;Landroid/os/Handler;)V

    .line 384
    return-void
.end method

.method public addListener(Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;Landroid/os/Handler;)V
    .locals 2
    .param p1, "listener"    # Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 374
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mListenerMap:Ljava/util/Map;

    monitor-enter v1

    .line 375
    :try_start_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mListenerMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    monitor-exit v1

    .line 377
    return-void

    .line 376
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public flushPendingTaps()V
    .locals 2

    .prologue
    .line 437
    const-wide v0, 0x7fffffffffffffffL

    invoke-direct {p0, v0, v1}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->emitTapsFromQueues(J)V

    .line 438
    return-void
.end method

.method public onAccelerometerChanged(J[F)V
    .locals 3
    .param p1, "timestamp"    # J
    .param p3, "values"    # [F

    .prologue
    .line 305
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mRawDataOutputStream:Lcom/google/common/io/LittleEndianDataOutputStream;

    if-eqz v1, :cond_0

    .line 307
    :try_start_0
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mRawDataOutputStream:Lcom/google/common/io/LittleEndianDataOutputStream;

    invoke-virtual {v1, p1, p2}, Lcom/google/common/io/LittleEndianDataOutputStream;->writeLong(J)V

    .line 308
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mRawDataOutputStream:Lcom/google/common/io/LittleEndianDataOutputStream;

    sget-object v2, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;->ACCELEROMETER:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/common/io/LittleEndianDataOutputStream;->writeInt(I)V

    .line 309
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mRawDataOutputStream:Lcom/google/common/io/LittleEndianDataOutputStream;

    const/4 v2, 0x0

    aget v2, p3, v2

    invoke-virtual {v1, v2}, Lcom/google/common/io/LittleEndianDataOutputStream;->writeFloat(F)V

    .line 310
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mRawDataOutputStream:Lcom/google/common/io/LittleEndianDataOutputStream;

    const/4 v2, 0x1

    aget v2, p3, v2

    invoke-virtual {v1, v2}, Lcom/google/common/io/LittleEndianDataOutputStream;->writeFloat(F)V

    .line 311
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mRawDataOutputStream:Lcom/google/common/io/LittleEndianDataOutputStream;

    const/4 v2, 0x2

    aget v2, p3, v2

    invoke-virtual {v1, v2}, Lcom/google/common/io/LittleEndianDataOutputStream;->writeFloat(F)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 317
    :cond_0
    :goto_0
    sget-object v1, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$3;->$SwitchMap$com$googlecode$eyesfree$picidae$IntegratedTapDetector$TapDetector:[I

    iget-object v2, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mCurrentTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 325
    :goto_1
    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->emitTapsFromQueues(J)V

    .line 326
    return-void

    .line 312
    :catch_0
    move-exception v0

    .line 313
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 320
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_0
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mAccelTapDetector:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;

    invoke-virtual {v1, p1, p2, p3}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->onSensorChanged(J[F)V

    goto :goto_1

    .line 317
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 398
    return-void
.end method

.method public onGyroscopeChanged(J[F)V
    .locals 3
    .param p1, "timestamp"    # J
    .param p3, "values"    # [F

    .prologue
    .line 332
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mRawDataOutputStream:Lcom/google/common/io/LittleEndianDataOutputStream;

    if-eqz v1, :cond_0

    .line 334
    :try_start_0
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mRawDataOutputStream:Lcom/google/common/io/LittleEndianDataOutputStream;

    invoke-virtual {v1, p1, p2}, Lcom/google/common/io/LittleEndianDataOutputStream;->writeLong(J)V

    .line 335
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mRawDataOutputStream:Lcom/google/common/io/LittleEndianDataOutputStream;

    sget-object v2, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;->GYROSCOPE:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/common/io/LittleEndianDataOutputStream;->writeInt(I)V

    .line 336
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mRawDataOutputStream:Lcom/google/common/io/LittleEndianDataOutputStream;

    const/4 v2, 0x0

    aget v2, p3, v2

    invoke-virtual {v1, v2}, Lcom/google/common/io/LittleEndianDataOutputStream;->writeFloat(F)V

    .line 337
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mRawDataOutputStream:Lcom/google/common/io/LittleEndianDataOutputStream;

    const/4 v2, 0x1

    aget v2, p3, v2

    invoke-virtual {v1, v2}, Lcom/google/common/io/LittleEndianDataOutputStream;->writeFloat(F)V

    .line 338
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mRawDataOutputStream:Lcom/google/common/io/LittleEndianDataOutputStream;

    const/4 v2, 0x2

    aget v2, p3, v2

    invoke-virtual {v1, v2}, Lcom/google/common/io/LittleEndianDataOutputStream;->writeFloat(F)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 343
    :cond_0
    :goto_0
    sget-object v1, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$3;->$SwitchMap$com$googlecode$eyesfree$picidae$IntegratedTapDetector$TapDetector:[I

    iget-object v2, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mCurrentTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 351
    :goto_1
    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->emitTapsFromQueues(J)V

    .line 352
    return-void

    .line 339
    :catch_0
    move-exception v0

    .line 340
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 346
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_0
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mGyroTapDetector:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;

    invoke-virtual {v1, p1, p2, p3}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->onSensorChanged(J[F)V

    goto :goto_1

    .line 343
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 402
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 411
    :goto_0
    :pswitch_0
    return-void

    .line 404
    :pswitch_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->onAccelerometerChanged(J[F)V

    goto :goto_0

    .line 407
    :pswitch_2
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->onGyroscopeChanged(J[F)V

    goto :goto_0

    .line 402
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public sendDoubleTapToListeners(J)V
    .locals 19
    .param p1, "timestampNanos"    # J

    .prologue
    .line 446
    move-wide/from16 v8, p1

    .line 447
    .local v8, "finalTimestamp":J
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mLastReportedTapTime:J

    .line 448
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mHaveReportedAtLeastOneTap:Z

    .line 451
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mListenerMap:Ljava/util/Map;

    monitor-enter v12

    .line 452
    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mListenerMap:Ljava/util/Map;

    invoke-interface {v11}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;

    .line 453
    .local v10, "listener":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    move-object v6, v10

    .line 454
    .local v6, "finalListener":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mPostDelayTime:J

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v16

    sub-long v16, v16, p1

    sub-long v4, v14, v16

    .line 455
    .local v4, "delay":J
    const-wide/16 v14, 0x0

    cmp-long v11, v4, v14

    if-lez v11, :cond_0

    .line 456
    :goto_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mListenerMap:Ljava/util/Map;

    invoke-interface {v11, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/os/Handler;

    new-instance v13, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v6, v8, v9}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$1;-><init>(Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;J)V

    invoke-virtual {v11, v13, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 463
    .end local v4    # "delay":J
    .end local v6    # "finalListener":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v10    # "listener":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    :catchall_0
    move-exception v11

    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v11

    .line 455
    .restart local v4    # "delay":J
    .restart local v6    # "finalListener":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    .restart local v7    # "i$":Ljava/util/Iterator;
    .restart local v10    # "listener":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    :cond_0
    const-wide/16 v4, 0x0

    goto :goto_1

    .line 463
    .end local v4    # "delay":J
    .end local v6    # "finalListener":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    .end local v10    # "listener":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    :cond_1
    :try_start_1
    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 464
    return-void
.end method

.method public sendSingleTapToListeners(J)V
    .locals 19
    .param p1, "timestampNanos"    # J

    .prologue
    .line 471
    move-wide/from16 v8, p1

    .line 472
    .local v8, "finalTimestamp":J
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mLastReportedTapTime:J

    .line 473
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mHaveReportedAtLeastOneTap:Z

    .line 476
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mListenerMap:Ljava/util/Map;

    monitor-enter v12

    .line 477
    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mListenerMap:Ljava/util/Map;

    invoke-interface {v11}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;

    .line 478
    .local v10, "listener":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    move-object v6, v10

    .line 479
    .local v6, "finalListener":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mPostDelayTime:J

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v16

    sub-long v16, v16, p1

    sub-long v4, v14, v16

    .line 480
    .local v4, "delay":J
    const-wide/16 v14, 0x0

    cmp-long v11, v4, v14

    if-lez v11, :cond_0

    .line 481
    :goto_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mListenerMap:Ljava/util/Map;

    invoke-interface {v11, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/os/Handler;

    new-instance v13, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$2;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v6, v8, v9}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$2;-><init>(Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;J)V

    invoke-virtual {v11, v13, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 488
    .end local v4    # "delay":J
    .end local v6    # "finalListener":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v10    # "listener":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    :catchall_0
    move-exception v11

    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v11

    .line 480
    .restart local v4    # "delay":J
    .restart local v6    # "finalListener":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    .restart local v7    # "i$":Ljava/util/Iterator;
    .restart local v10    # "listener":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    :cond_0
    const-wide/16 v4, 0x0

    goto :goto_1

    .line 488
    .end local v4    # "delay":J
    .end local v6    # "finalListener":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    .end local v10    # "listener":Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
    :cond_1
    :try_start_1
    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 489
    return-void
.end method

.method public setDoubleTapDetectionQuality(D)V
    .locals 1
    .param p1, "tq"    # D

    .prologue
    .line 252
    iput-wide p1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mMinDoubleTapQuality:D

    .line 253
    return-void
.end method

.method public setLastTapNanoTime(J)V
    .locals 1
    .param p1, "newTime"    # J

    .prologue
    .line 429
    iput-wide p1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mLastReportedTapTime:J

    .line 430
    return-void
.end method

.method public setMaxDoubleTapSpacingNanos(J)V
    .locals 1
    .param p1, "maxDTapSpacing"    # J

    .prologue
    .line 233
    iput-wide p1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mMaxDoubleTapSpacingNanos:J

    .line 234
    return-void
.end method

.method public setPostDelayTimeMillis(J)V
    .locals 1
    .param p1, "millisToDelayPosts"    # J

    .prologue
    .line 262
    iput-wide p1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mPostDelayTime:J

    .line 263
    return-void
.end method

.method public setTapDetectionQuality(D)V
    .locals 1
    .param p1, "tq"    # D

    .prologue
    .line 242
    iput-wide p1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mMinTapQuality:D

    .line 243
    return-void
.end method

.method public start()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 283
    iget-object v2, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    .line 284
    .local v0, "accelerometer":Landroid/hardware/Sensor;
    iget-object v2, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, p0, v0, v4, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    .line 287
    iget-object v2, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    .line 288
    .local v1, "gyroscope":Landroid/hardware/Sensor;
    iget-object v2, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, p0, v1, v4, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    .line 290
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 299
    return-void
.end method

.method public threeDSensorTapDetected(Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;JD)V
    .locals 2
    .param p1, "threeDSensorTapDetector"    # Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;
    .param p2, "timestamp"    # J
    .param p4, "tapConfidence"    # D

    .prologue
    .line 416
    iget-object v0, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mAccelTapDetector:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;

    if-ne p1, v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mAccelTapEventQueue:Ljava/util/Queue;

    new-instance v1, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;

    invoke-direct {v1, p4, p5, p2, p3}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;-><init>(DJ)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mGyroTapDetector:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;

    if-ne p1, v0, :cond_1

    .line 421
    iget-object v0, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->mGyroTapEventQueue:Ljava/util/Queue;

    new-instance v1, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;

    invoke-direct {v1, p4, p5, p2, p3}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;-><init>(DJ)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 424
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->emitTapsFromQueues(J)V

    .line 425
    return-void
.end method
