.class public final enum Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;
.super Ljava/lang/Enum;
.source "ThreeDSensorTapDetectorType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

.field public static final enum ACCELEROMETER:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

.field public static final enum GYROSCOPE:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;


# instance fields
.field public final definiteTapsFallTimeNanos:J

.field public final definiteTapsHighAmplitudeTimeNanos:J

.field public final definiteTapsLowLevel:F

.field public final definiteTapsLowTimeNanos:J

.field public final energyPerSampleNoiseLimit:F

.field public final filterDen:[F

.field public final filterNum:[F

.field public final multipleOfNoiseForPossibleTap:F

.field public final possibleTapsFallTimeNanos:J

.field public final possibleTapsHighAmplitudeTimeNanos:J

.field public final possibleTapsLowLevel:F

.field public final possibleTapsLowTimeNanos:J

.field public final thresholdForDefiniteTap:F

.field public final thresholdForPossibleTap:F


# direct methods
.method static constructor <clinit>()V
    .locals 22

    .prologue
    .line 25
    new-instance v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    const-string v2, "ACCELEROMETER"

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v4, v0, [F

    fill-array-data v4, :array_0

    const/4 v0, 0x2

    new-array v5, v0, [F

    fill-array-data v5, :array_1

    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v7, 0x40a00000    # 5.0f

    const-wide/32 v8, 0x1c9c380

    const-wide/32 v10, 0x2625a00

    const-wide/32 v12, 0x42c1d80

    const-wide/32 v14, 0x3938700

    const/high16 v16, 0x40400000    # 3.0f

    const/high16 v17, 0x40a00000    # 5.0f

    const-wide/16 v18, 0x0

    const-wide/16 v20, 0x0

    invoke-direct/range {v1 .. v21}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;-><init>(Ljava/lang/String;I[F[FFFJJJJFFJJ)V

    sput-object v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->ACCELEROMETER:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    .line 38
    new-instance v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    const-string v2, "GYROSCOPE"

    const/4 v3, 0x1

    const/4 v0, 0x2

    new-array v4, v0, [F

    fill-array-data v4, :array_2

    const/4 v0, 0x2

    new-array v5, v0, [F

    fill-array-data v5, :array_3

    const v6, 0x3d4ccccd    # 0.05f

    const/high16 v7, 0x40800000    # 4.0f

    const-wide/32 v8, 0x1c9c380

    const-wide/32 v10, 0x2625a00

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const/high16 v16, 0x3e800000    # 0.25f

    const/high16 v17, 0x3f000000    # 0.5f

    const-wide/32 v18, 0x42c1d80

    const-wide/32 v20, 0x3938700

    invoke-direct/range {v1 .. v21}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;-><init>(Ljava/lang/String;I[F[FFFJJJJFFJJ)V

    sput-object v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->GYROSCOPE:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    .line 24
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    const/4 v1, 0x0

    sget-object v2, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->ACCELEROMETER:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->GYROSCOPE:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->$VALUES:[Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    return-void

    .line 25
    nop

    :array_0
    .array-data 4
        0x3f4ccccd    # 0.8f
        -0x40b33333    # -0.8f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        -0x40b33333    # -0.8f
    .end array-data

    .line 38
    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
    .end array-data

    :array_3
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;I[F[FFFJJJJFFJJ)V
    .locals 5
    .param p3, "filterNumInit"    # [F
    .param p4, "filterDenInit"    # [F
    .param p5, "energyPerSampleNoiseLimitInit"    # F
    .param p6, "multipleOfNoiseForPossibleTapInit"    # F
    .param p7, "definiteTapsHighAmplitudeTimeNanosInit"    # J
    .param p9, "possibleTapsHighAmplitudeTimeNanosInit"    # J
    .param p11, "definiteTapsFallTimeNanosInit"    # J
    .param p13, "possibleTapsFallTimeNanosInit"    # J
    .param p15, "definiteTapsLowLevelInit"    # F
    .param p16, "possibleTapsLowLevelInit"    # F
    .param p17, "definiteTapsLowTimeNanosInit"    # J
    .param p19, "possibleTapsLowTimeNanosInit"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([F[FFFJJJJFFJJ)V"
        }
    .end annotation

    .prologue
    .line 133
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 134
    iput-object p3, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->filterNum:[F

    .line 135
    iput-object p4, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->filterDen:[F

    .line 136
    iput p5, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->energyPerSampleNoiseLimit:F

    .line 137
    iput p6, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->multipleOfNoiseForPossibleTap:F

    .line 138
    mul-float v2, p5, p6

    iput v2, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->thresholdForPossibleTap:F

    .line 140
    const/high16 v2, 0x40000000    # 2.0f

    iget v3, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->thresholdForPossibleTap:F

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->thresholdForDefiniteTap:F

    .line 141
    iput-wide p7, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->definiteTapsHighAmplitudeTimeNanos:J

    .line 142
    iput-wide p9, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->possibleTapsHighAmplitudeTimeNanos:J

    .line 143
    move-wide/from16 v0, p11

    iput-wide v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->definiteTapsFallTimeNanos:J

    .line 144
    move-wide/from16 v0, p13

    iput-wide v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->possibleTapsFallTimeNanos:J

    .line 145
    move/from16 v0, p15

    iput v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->definiteTapsLowLevel:F

    .line 146
    move/from16 v0, p16

    iput v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->possibleTapsLowLevel:F

    .line 147
    move-wide/from16 v0, p17

    iput-wide v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->definiteTapsLowTimeNanos:J

    .line 148
    move-wide/from16 v0, p19

    iput-wide v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->possibleTapsLowTimeNanos:J

    .line 149
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    return-object v0
.end method

.method public static values()[Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->$VALUES:[Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    invoke-virtual {v0}, [Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    return-object v0
.end method
