.class public final enum Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;
.super Ljava/lang/Enum;
.source "IntegratedTapDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SensorTag"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

.field public static final enum ACCELEROMETER:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

.field public static final enum AUDIO:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

.field public static final enum GYROSCOPE:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

.field public static final enum INVALID:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 657
    new-instance v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v2}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;->INVALID:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    new-instance v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    const-string v1, "ACCELEROMETER"

    invoke-direct {v0, v1, v3}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;->ACCELEROMETER:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    new-instance v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    const-string v1, "GYROSCOPE"

    invoke-direct {v0, v1, v4}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;->GYROSCOPE:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    new-instance v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    const-string v1, "AUDIO"

    invoke-direct {v0, v1, v5}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;->AUDIO:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    .line 656
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    sget-object v1, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;->INVALID:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    aput-object v1, v0, v2

    sget-object v1, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;->ACCELEROMETER:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    aput-object v1, v0, v3

    sget-object v1, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;->GYROSCOPE:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    aput-object v1, v0, v4

    sget-object v1, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;->AUDIO:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    aput-object v1, v0, v5

    sput-object v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;->$VALUES:[Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 656
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 656
    const-class v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    return-object v0
.end method

.method public static values()[Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;
    .locals 1

    .prologue
    .line 656
    sget-object v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;->$VALUES:[Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    invoke-virtual {v0}, [Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$SensorTag;

    return-object v0
.end method
