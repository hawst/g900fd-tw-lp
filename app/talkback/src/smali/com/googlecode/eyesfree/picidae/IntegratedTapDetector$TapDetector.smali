.class public final enum Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;
.super Ljava/lang/Enum;
.source "IntegratedTapDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TapDetector"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

.field public static final enum ACCEL_ONLY_DETECTOR:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

.field public static final enum GYRO_ONLY_DETECTOR:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

.field public static final enum INTEGRATED_TAP_DETECTOR:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 644
    new-instance v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    const-string v1, "INTEGRATED_TAP_DETECTOR"

    invoke-direct {v0, v1, v2}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;->INTEGRATED_TAP_DETECTOR:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    .line 647
    new-instance v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    const-string v1, "ACCEL_ONLY_DETECTOR"

    invoke-direct {v0, v1, v3}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;->ACCEL_ONLY_DETECTOR:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    .line 650
    new-instance v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    const-string v1, "GYRO_ONLY_DETECTOR"

    invoke-direct {v0, v1, v4}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;->GYRO_ONLY_DETECTOR:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    .line 642
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    sget-object v1, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;->INTEGRATED_TAP_DETECTOR:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    aput-object v1, v0, v2

    sget-object v1, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;->ACCEL_ONLY_DETECTOR:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    aput-object v1, v0, v3

    sget-object v1, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;->GYRO_ONLY_DETECTOR:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    aput-object v1, v0, v4

    sput-object v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;->$VALUES:[Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 642
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 642
    const-class v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    return-object v0
.end method

.method public static values()[Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;
    .locals 1

    .prologue
    .line 642
    sget-object v0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;->$VALUES:[Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    invoke-virtual {v0}, [Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapDetector;

    return-object v0
.end method
