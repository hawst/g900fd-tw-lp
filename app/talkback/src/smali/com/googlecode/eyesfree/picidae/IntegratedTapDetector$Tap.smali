.class Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;
.super Ljava/lang/Object;
.source "IntegratedTapDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Tap"
.end annotation


# instance fields
.field public final nanos:J

.field public final quality:D


# direct methods
.method public constructor <init>(DJ)V
    .locals 1
    .param p1, "qualityInit"    # D
    .param p3, "nanosInit"    # J

    .prologue
    .line 675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 676
    iput-wide p1, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->quality:D

    .line 677
    iput-wide p3, p0, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$Tap;->nanos:J

    .line 678
    return-void
.end method
