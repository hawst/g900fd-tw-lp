.class public Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;
.super Ljava/lang/Object;
.source "ThreeDSensorTapDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$1;,
        Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$EnergySamplePair;,
        Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;,
        Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$TapListener;
    }
.end annotation


# instance fields
.field private mCandidateTapStart:J

.field private mConditionedSignalEnergy:F

.field private mCurrentState:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

.field private mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

.field private final mEnergySamplesList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$EnergySamplePair;",
            ">;"
        }
    .end annotation
.end field

.field private final mLargestMagSq:F

.field private mLastConditionedMagnitudeSq:F

.field private mLastFilterOutput:[F

.field private mLastInput:[F

.field private mLastTimestamp:J

.field private final mTapListener:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$TapListener;


# direct methods
.method public constructor <init>(Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$TapListener;FLcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;)V
    .locals 4
    .param p1, "tapListener"    # Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$TapListener;
    .param p2, "sensorMaxScale"    # F
    .param p3, "type"    # Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x3

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mConditionedSignalEnergy:F

    .line 76
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastInput:[F

    .line 79
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastFilterOutput:[F

    .line 97
    const/high16 v0, 0x40400000    # 3.0f

    mul-float/2addr v0, p2

    mul-float/2addr v0, p2

    iput v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLargestMagSq:F

    .line 98
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mEnergySamplesList:Ljava/util/LinkedList;

    .line 99
    iput-object p1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mTapListener:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$TapListener;

    .line 100
    iput-object p3, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    .line 101
    iput-wide v2, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastTimestamp:J

    .line 102
    sget-object v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->TOO_NOISY:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    invoke-direct {p0, v2, v3, v0}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->changeToNewCurrentState(JLcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;)V

    .line 103
    return-void

    .line 76
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    .line 79
    :array_1
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private changeToNewCurrentState(JLcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;)V
    .locals 7
    .param p1, "timestamp"    # J
    .param p3, "newState"    # Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    .prologue
    .line 313
    iput-object p3, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mCurrentState:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    .line 319
    sget-object v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->POSSIBLE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    if-ne p3, v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mTapListener:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$TapListener;

    const-wide v4, 0x3fc3333333333333L    # 0.15

    move-object v1, p0

    move-wide v2, p1

    invoke-interface/range {v0 .. v5}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$TapListener;->threeDSensorTapDetected(Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;JD)V

    .line 323
    :cond_0
    sget-object v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->DEFINITE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    if-ne p3, v0, :cond_1

    .line 324
    iget-object v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mTapListener:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$TapListener;

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    move-object v1, p0

    move-wide v2, p1

    invoke-interface/range {v0 .. v5}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$TapListener;->threeDSensorTapDetected(Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;JD)V

    .line 327
    :cond_1
    return-void
.end method

.method private clearEnergySamplesList()V
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mEnergySamplesList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 331
    const/4 v0, 0x0

    iput v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mConditionedSignalEnergy:F

    .line 332
    return-void
.end method

.method private stateMachineNoTap(J)V
    .locals 3
    .param p1, "timestamp"    # J

    .prologue
    .line 192
    iget v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastConditionedMagnitudeSq:F

    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget v1, v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->thresholdForDefiniteTap:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 193
    sget-object v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->PROCESSING_CANDIDATE_DEFINITE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    invoke-direct {p0, p1, p2, v0}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->changeToNewCurrentState(JLcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;)V

    .line 195
    iput-wide p1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mCandidateTapStart:J

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    iget v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastConditionedMagnitudeSq:F

    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget v1, v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->thresholdForPossibleTap:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 197
    sget-object v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->PROCESSING_CANDIDATE_POSSIBLE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    invoke-direct {p0, p1, p2, v0}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->changeToNewCurrentState(JLcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;)V

    .line 199
    iput-wide p1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mCandidateTapStart:J

    goto :goto_0

    .line 200
    :cond_2
    iget v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mConditionedSignalEnergy:F

    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mEnergySamplesList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget v2, v2, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->energyPerSampleNoiseLimit:F

    mul-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 202
    sget-object v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->TOO_NOISY:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    invoke-direct {p0, p1, p2, v0}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->changeToNewCurrentState(JLcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;)V

    goto :goto_0
.end method

.method private stateMachineProcessingDefiniteTap(J)V
    .locals 19
    .param p1, "timestamp"    # J

    .prologue
    .line 219
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mCandidateTapStart:J

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget-wide v0, v13, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->definiteTapsHighAmplitudeTimeNanos:J

    move-wide/from16 v16, v0

    add-long v8, v14, v16

    .line 220
    .local v8, "x1":J
    move-object/from16 v0, p0

    iget v5, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLargestMagSq:F

    .line 221
    .local v5, "y1":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget-wide v14, v13, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->definiteTapsFallTimeNanos:J

    add-long v10, v8, v14

    .line 222
    .local v10, "x2":J
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget v12, v13, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->definiteTapsLowLevel:F

    .line 223
    .local v12, "y2":F
    move-wide/from16 v6, p1

    .line 224
    .local v6, "x":J
    sub-float v13, v12, v5

    long-to-float v14, v6

    long-to-float v15, v8

    sub-float/2addr v14, v15

    mul-float/2addr v13, v14

    long-to-float v14, v10

    long-to-float v15, v8

    sub-float/2addr v14, v15

    div-float/2addr v13, v14

    add-float v4, v5, v13

    .line 227
    .local v4, "envelope":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget v13, v13, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->definiteTapsLowLevel:F

    invoke-static {v4, v13}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 229
    move-object/from16 v0, p0

    iget v13, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastConditionedMagnitudeSq:F

    cmpl-float v13, v13, v4

    if-lez v13, :cond_1

    .line 237
    sget-object v13, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->PROCESSING_CANDIDATE_POSSIBLE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v0, v1, v2, v13}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->changeToNewCurrentState(JLcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;)V

    .line 239
    invoke-direct/range {p0 .. p2}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->stateMachineProcessingPossibleTap(J)V

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget-wide v14, v13, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->definiteTapsLowTimeNanos:J

    add-long/2addr v14, v10

    cmp-long v13, p1, v14

    if-lez v13, :cond_0

    .line 241
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mCandidateTapStart:J

    sget-object v13, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->DEFINITE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15, v13}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->changeToNewCurrentState(JLcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;)V

    goto :goto_0
.end method

.method private stateMachineProcessingPossibleTap(J)V
    .locals 19
    .param p1, "timestamp"    # J

    .prologue
    .line 256
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mCandidateTapStart:J

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget-wide v0, v13, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->possibleTapsHighAmplitudeTimeNanos:J

    move-wide/from16 v16, v0

    add-long v8, v14, v16

    .line 257
    .local v8, "x1":J
    move-object/from16 v0, p0

    iget v5, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLargestMagSq:F

    .line 258
    .local v5, "y1":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget-wide v14, v13, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->possibleTapsFallTimeNanos:J

    add-long v10, v8, v14

    .line 259
    .local v10, "x2":J
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget v12, v13, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->possibleTapsLowLevel:F

    .line 260
    .local v12, "y2":F
    move-wide/from16 v6, p1

    .line 261
    .local v6, "x":J
    sub-float v13, v12, v5

    long-to-float v14, v6

    long-to-float v15, v8

    sub-float/2addr v14, v15

    mul-float/2addr v13, v14

    long-to-float v14, v10

    long-to-float v15, v8

    sub-float/2addr v14, v15

    div-float/2addr v13, v14

    add-float v4, v5, v13

    .line 264
    .local v4, "envelope":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget v13, v13, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->possibleTapsLowLevel:F

    invoke-static {v4, v13}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 266
    move-object/from16 v0, p0

    iget v13, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastConditionedMagnitudeSq:F

    cmpl-float v13, v13, v4

    if-lez v13, :cond_1

    .line 273
    sget-object v13, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->TOO_NOISY:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v0, v1, v2, v13}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->changeToNewCurrentState(JLcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;)V

    .line 278
    :cond_0
    :goto_0
    return-void

    .line 274
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget-wide v14, v13, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->possibleTapsLowTimeNanos:J

    add-long/2addr v14, v10

    cmp-long v13, p1, v14

    if-lez v13, :cond_0

    .line 275
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mCandidateTapStart:J

    sget-object v13, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->POSSIBLE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15, v13}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->changeToNewCurrentState(JLcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;)V

    goto :goto_0
.end method

.method private stateMachineTooNoisy(J)V
    .locals 7
    .param p1, "timestamp"    # J

    .prologue
    .line 290
    iget-object v2, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mEnergySamplesList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$EnergySamplePair;

    iget-wide v4, v2, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$EnergySamplePair;->mTime:J

    iget-object v2, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mEnergySamplesList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$EnergySamplePair;

    iget-wide v2, v2, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$EnergySamplePair;->mTime:J

    sub-long v0, v4, v2

    .line 292
    .local v0, "timeSpanInHistoryNanos":J
    const-wide/32 v2, 0x4c4b400

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 310
    :cond_0
    :goto_0
    return-void

    .line 297
    :cond_1
    iget v2, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastConditionedMagnitudeSq:F

    iget-object v3, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mEnergySamplesList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget v3, v3, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->multipleOfNoiseForPossibleTap:F

    iget v4, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mConditionedSignalEnergy:F

    mul-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    .line 299
    sget-object v2, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->PROCESSING_CANDIDATE_POSSIBLE_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    invoke-direct {p0, p1, p2, v2}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->changeToNewCurrentState(JLcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;)V

    .line 301
    iput-wide p1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mCandidateTapStart:J

    goto :goto_0

    .line 306
    :cond_2
    iget v2, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mConditionedSignalEnergy:F

    iget-object v3, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mEnergySamplesList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget v4, v4, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->energyPerSampleNoiseLimit:F

    mul-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 308
    sget-object v2, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->NO_TAP:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    invoke-direct {p0, p1, p2, v2}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->changeToNewCurrentState(JLcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;)V

    goto :goto_0
.end method


# virtual methods
.method getConditionedSignalEnergy()F
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mConditionedSignalEnergy:F

    return v0
.end method

.method getLastConditionedMagnitudeSq()F
    .locals 1

    .prologue
    .line 182
    iget v0, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastConditionedMagnitudeSq:F

    return v0
.end method

.method public onSensorChanged(J[F)V
    .locals 9
    .param p1, "timestamp"    # J
    .param p3, "values"    # [F

    .prologue
    const-wide/32 v6, 0x5f5e100

    const/4 v5, 0x1

    .line 107
    iget-wide v2, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastTimestamp:J

    sub-long v2, p1, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    cmp-long v1, v2, v6

    if-lez v1, :cond_0

    .line 108
    invoke-direct {p0}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->clearEnergySamplesList()V

    .line 112
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mCurrentState:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    sget-object v2, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->TOO_NOISY:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    if-eq v1, v2, :cond_0

    .line 113
    sget-object v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->TOO_NOISY:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    invoke-direct {p0, p1, p2, v1}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->changeToNewCurrentState(JLcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;)V

    .line 116
    :cond_0
    iput-wide p1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastTimestamp:J

    .line 119
    const/4 v1, 0x0

    iput v1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastConditionedMagnitudeSq:F

    .line 120
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 121
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastFilterOutput:[F

    iget-object v2, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget-object v2, v2, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->filterNum:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    aget v3, p3, v0

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget-object v3, v3, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->filterNum:[F

    aget v3, v3, v5

    iget-object v4, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastInput:[F

    aget v4, v4, v0

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mDetectorType:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;

    iget-object v3, v3, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetectorType;->filterDen:[F

    aget v3, v3, v5

    iget-object v4, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastFilterOutput:[F

    aget v4, v4, v0

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    aput v2, v1, v0

    .line 124
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastInput:[F

    aget v2, p3, v0

    aput v2, v1, v0

    .line 125
    iget v1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastConditionedMagnitudeSq:F

    iget-object v2, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastFilterOutput:[F

    aget v2, v2, v0

    iget-object v3, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastFilterOutput:[F

    aget v3, v3, v0

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastConditionedMagnitudeSq:F

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 132
    :cond_1
    iget v1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mConditionedSignalEnergy:F

    iget v2, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastConditionedMagnitudeSq:F

    add-float/2addr v1, v2

    iput v1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mConditionedSignalEnergy:F

    .line 133
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mEnergySamplesList:Ljava/util/LinkedList;

    new-instance v2, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$EnergySamplePair;

    iget v3, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mLastConditionedMagnitudeSq:F

    invoke-direct {v2, p0, p1, p2, v3}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$EnergySamplePair;-><init>(Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;JF)V

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 134
    :goto_1
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mEnergySamplesList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$EnergySamplePair;

    iget-wide v2, v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$EnergySamplePair;->mTime:J

    sub-long v4, p1, v6

    cmp-long v1, v2, v4

    if-gtz v1, :cond_2

    .line 135
    iget v2, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mConditionedSignalEnergy:F

    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mEnergySamplesList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$EnergySamplePair;

    iget v1, v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$EnergySamplePair;->mValue:F

    sub-float v1, v2, v1

    iput v1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mConditionedSignalEnergy:F

    .line 136
    iget-object v1, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mEnergySamplesList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    goto :goto_1

    .line 148
    :cond_2
    sget-object v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$1;->$SwitchMap$com$googlecode$eyesfree$picidae$ThreeDSensorTapDetector$SensorDetectorState:[I

    iget-object v2, p0, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->mCurrentState:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 173
    :goto_2
    return-void

    .line 150
    :pswitch_0
    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->stateMachineNoTap(J)V

    goto :goto_2

    .line 154
    :pswitch_1
    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->stateMachineProcessingDefiniteTap(J)V

    goto :goto_2

    .line 158
    :pswitch_2
    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->stateMachineProcessingPossibleTap(J)V

    goto :goto_2

    .line 162
    :pswitch_3
    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->stateMachineTooNoisy(J)V

    goto :goto_2

    .line 168
    :pswitch_4
    sget-object v1, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;->TOO_NOISY:Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;

    invoke-direct {p0, p1, p2, v1}, Lcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector;->changeToNewCurrentState(JLcom/googlecode/eyesfree/picidae/ThreeDSensorTapDetector$SensorDetectorState;)V

    goto :goto_2

    .line 148
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
