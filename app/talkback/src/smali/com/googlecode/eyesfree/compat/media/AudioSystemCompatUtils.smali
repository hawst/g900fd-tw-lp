.class public Lcom/googlecode/eyesfree/compat/media/AudioSystemCompatUtils;
.super Ljava/lang/Object;
.source "AudioSystemCompatUtils.java"


# static fields
.field private static final CLASS_AudioSystem:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final METHOD_LEGACY_isStreamActive:Ljava/lang/reflect/Method;

.field private static final METHOD_isSourceActive:Ljava/lang/reflect/Method;

.field private static final METHOD_isStreamActive:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 26
    const-string v0, "android.media.AudioSystem"

    invoke-static {v0}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/media/AudioSystemCompatUtils;->CLASS_AudioSystem:Ljava/lang/Class;

    .line 28
    sget-object v0, Lcom/googlecode/eyesfree/compat/media/AudioSystemCompatUtils;->CLASS_AudioSystem:Ljava/lang/Class;

    const-string v1, "isStreamActive"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v4

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/media/AudioSystemCompatUtils;->METHOD_isStreamActive:Ljava/lang/reflect/Method;

    .line 30
    sget-object v0, Lcom/googlecode/eyesfree/compat/media/AudioSystemCompatUtils;->CLASS_AudioSystem:Ljava/lang/Class;

    const-string v1, "isStreamActive"

    new-array v2, v5, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/media/AudioSystemCompatUtils;->METHOD_LEGACY_isStreamActive:Ljava/lang/reflect/Method;

    .line 32
    sget-object v0, Lcom/googlecode/eyesfree/compat/media/AudioSystemCompatUtils;->CLASS_AudioSystem:Ljava/lang/Class;

    const-string v1, "isSourceActive"

    new-array v2, v5, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/media/AudioSystemCompatUtils;->METHOD_isSourceActive:Ljava/lang/reflect/Method;

    return-void
.end method

.method public static isSourceActive(I)Z
    .locals 6
    .param p0, "source"    # I

    .prologue
    const/4 v5, 0x0

    .line 66
    const/4 v0, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sget-object v2, Lcom/googlecode/eyesfree/compat/media/AudioSystemCompatUtils;->METHOD_isSourceActive:Ljava/lang/reflect/Method;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v0, v1, v2, v3}, Lcom/googlecode/eyesfree/compat/CompatUtils;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
