.class public Lcom/googlecode/eyesfree/compat/util/SparseLongArray;
.super Ljava/lang/Object;
.source "SparseLongArray.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private mKeys:[I

.field private mSize:I

.field private mValues:[J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;-><init>(I)V

    .line 38
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "initialCapacity"    # I

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {p1}, Lcom/googlecode/eyesfree/compat/util/ArrayUtils;->idealLongArraySize(I)I

    move-result p1

    .line 48
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mKeys:[I

    .line 49
    new-array v0, p1, [J

    iput-object v0, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mValues:[J

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mSize:I

    .line 51
    return-void
.end method

.method private static binarySearch([IIIJ)I
    .locals 7
    .param p0, "a"    # [I
    .param p1, "start"    # I
    .param p2, "len"    # I
    .param p3, "key"    # J

    .prologue
    .line 228
    add-int v1, p1, p2

    .local v1, "high":I
    add-int/lit8 v2, p1, -0x1

    .line 230
    .local v2, "low":I
    :goto_0
    sub-int v3, v1, v2

    const/4 v4, 0x1

    if-le v3, v4, :cond_1

    .line 231
    add-int v3, v1, v2

    div-int/lit8 v0, v3, 0x2

    .line 233
    .local v0, "guess":I
    aget v3, p0, v0

    int-to-long v4, v3

    cmp-long v3, v4, p3

    if-gez v3, :cond_0

    .line 234
    move v2, v0

    goto :goto_0

    .line 236
    :cond_0
    move v1, v0

    goto :goto_0

    .line 239
    .end local v0    # "guess":I
    :cond_1
    add-int v3, p1, p2

    if-ne v1, v3, :cond_3

    .line 240
    add-int v3, p1, p2

    xor-int/lit8 v1, v3, -0x1

    .line 244
    .end local v1    # "high":I
    :cond_2
    :goto_1
    return v1

    .line 241
    .restart local v1    # "high":I
    :cond_3
    aget v3, p0, v1

    int-to-long v4, v3

    cmp-long v3, v4, p3

    if-eqz v3, :cond_2

    .line 244
    xor-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method

.method private growKeyAndValueArrays(I)V
    .locals 6
    .param p1, "minNeededSize"    # I

    .prologue
    const/4 v5, 0x0

    .line 215
    invoke-static {p1}, Lcom/googlecode/eyesfree/compat/util/ArrayUtils;->idealLongArraySize(I)I

    move-result v0

    .line 217
    .local v0, "n":I
    new-array v1, v0, [I

    .line 218
    .local v1, "nkeys":[I
    new-array v2, v0, [J

    .line 220
    .local v2, "nvalues":[J
    iget-object v3, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mKeys:[I

    iget-object v4, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mKeys:[I

    array-length v4, v4

    invoke-static {v3, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 221
    iget-object v3, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mValues:[J

    iget-object v4, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mValues:[J

    array-length v4, v4

    invoke-static {v3, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 223
    iput-object v1, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mKeys:[I

    .line 224
    iput-object v2, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mValues:[J

    .line 225
    return-void
.end method


# virtual methods
.method public clone()Lcom/googlecode/eyesfree/compat/util/SparseLongArray;
    .locals 3

    .prologue
    .line 55
    const/4 v1, 0x0

    .line 57
    .local v1, "clone":Lcom/googlecode/eyesfree/compat/util/SparseLongArray;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;

    move-object v1, v0

    .line 58
    iget-object v2, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mKeys:[I

    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    iput-object v2, v1, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mKeys:[I

    .line 59
    iget-object v2, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mValues:[J

    invoke-virtual {v2}, [J->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [J

    iput-object v2, v1, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mValues:[J
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :goto_0
    return-object v1

    .line 60
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->clone()Lcom/googlecode/eyesfree/compat/util/SparseLongArray;

    move-result-object v0

    return-object v0
.end method

.method public get(I)J
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 71
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->get(IJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public get(IJ)J
    .locals 6
    .param p1, "key"    # I
    .param p2, "valueIfKeyNotFound"    # J

    .prologue
    .line 79
    iget-object v1, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mKeys:[I

    const/4 v2, 0x0

    iget v3, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mSize:I

    int-to-long v4, p1

    invoke-static {v1, v2, v3, v4, v5}, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->binarySearch([IIIJ)I

    move-result v0

    .line 81
    .local v0, "i":I
    if-gez v0, :cond_0

    .line 84
    .end local p2    # "valueIfKeyNotFound":J
    :goto_0
    return-wide p2

    .restart local p2    # "valueIfKeyNotFound":J
    :cond_0
    iget-object v1, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mValues:[J

    aget-wide p2, v1, v0

    goto :goto_0
.end method

.method public put(IJ)V
    .locals 6
    .param p1, "key"    # I
    .param p2, "value"    # J

    .prologue
    .line 114
    iget-object v1, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mKeys:[I

    const/4 v2, 0x0

    iget v3, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mSize:I

    int-to-long v4, p1

    invoke-static {v1, v2, v3, v4, v5}, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->binarySearch([IIIJ)I

    move-result v0

    .line 116
    .local v0, "i":I
    if-ltz v0, :cond_0

    .line 117
    iget-object v1, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mValues:[J

    aput-wide p2, v1, v0

    .line 134
    :goto_0
    return-void

    .line 119
    :cond_0
    xor-int/lit8 v0, v0, -0x1

    .line 121
    iget v1, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mSize:I

    iget-object v2, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mKeys:[I

    array-length v2, v2

    if-lt v1, v2, :cond_1

    .line 122
    iget v1, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mSize:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->growKeyAndValueArrays(I)V

    .line 125
    :cond_1
    iget v1, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mSize:I

    sub-int/2addr v1, v0

    if-eqz v1, :cond_2

    .line 126
    iget-object v1, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mKeys:[I

    iget-object v2, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mKeys:[I

    add-int/lit8 v3, v0, 0x1

    iget v4, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mSize:I

    sub-int/2addr v4, v0

    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 127
    iget-object v1, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mValues:[J

    iget-object v2, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mValues:[J

    add-int/lit8 v3, v0, 0x1

    iget v4, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mSize:I

    sub-int/2addr v4, v0

    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 130
    :cond_2
    iget-object v1, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mKeys:[I

    aput p1, v1, v0

    .line 131
    iget-object v1, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mValues:[J

    aput-wide p2, v1, v0

    .line 132
    iget v1, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mSize:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->mSize:I

    goto :goto_0
.end method
