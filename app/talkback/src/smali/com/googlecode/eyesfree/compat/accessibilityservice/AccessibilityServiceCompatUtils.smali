.class public Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;
.super Ljava/lang/Object;
.source "AccessibilityServiceCompatUtils.java"


# static fields
.field private static final METHOD_getRootInActiveWindow:Ljava/lang/reflect/Method;

.field private static final METHOD_getServiceInfo:Ljava/lang/reflect/Method;

.field private static final METHOD_performGlobalAction:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 28
    const-class v0, Landroid/accessibilityservice/AccessibilityService;

    const-string v1, "performGlobalAction"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->METHOD_performGlobalAction:Ljava/lang/reflect/Method;

    .line 30
    const-class v0, Landroid/accessibilityservice/AccessibilityService;

    const-string v1, "getServiceInfo"

    new-array v2, v4, [Ljava/lang/Class;

    invoke-static {v0, v1, v2}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->METHOD_getServiceInfo:Ljava/lang/reflect/Method;

    .line 32
    const-class v0, Landroid/accessibilityservice/AccessibilityService;

    const-string v1, "getRootInActiveWindow"

    new-array v2, v4, [Ljava/lang/Class;

    invoke-static {v0, v1, v2}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->METHOD_getRootInActiveWindow:Ljava/lang/reflect/Method;

    return-void
.end method

.method public static getRootInActiveWindow(Landroid/accessibilityservice/AccessibilityService;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 4
    .param p0, "service"    # Landroid/accessibilityservice/AccessibilityService;

    .prologue
    const/4 v1, 0x0

    .line 45
    sget-object v2, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->METHOD_getRootInActiveWindow:Ljava/lang/reflect/Method;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/compat/CompatUtils;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 46
    .local v0, "root":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 47
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {v1, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;-><init>(Ljava/lang/Object;)V

    .line 49
    :cond_0
    return-object v1
.end method

.method public static performGlobalAction(Landroid/accessibilityservice/AccessibilityService;I)Z
    .locals 5
    .param p0, "service"    # Landroid/accessibilityservice/AccessibilityService;
    .param p1, "action"    # I

    .prologue
    const/4 v4, 0x0

    .line 36
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sget-object v1, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->METHOD_performGlobalAction:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/compat/CompatUtils;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
