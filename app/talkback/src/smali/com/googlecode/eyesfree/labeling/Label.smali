.class public Lcom/googlecode/eyesfree/labeling/Label;
.super Ljava/lang/Object;
.source "Label.java"


# instance fields
.field private mId:J

.field private mLocale:Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;

.field private mPackageSignature:Ljava/lang/String;

.field private mPackageVersion:I

.field private mScreenshotPath:Ljava/lang/String;

.field private mText:Ljava/lang/String;

.field private mTimestampMillis:J

.field private mViewName:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 0
    .param p1, "labelId"    # J
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "packageSignature"    # Ljava/lang/String;
    .param p5, "viewName"    # Ljava/lang/String;
    .param p6, "text"    # Ljava/lang/String;
    .param p7, "locale"    # Ljava/lang/String;
    .param p8, "packageVersion"    # I
    .param p9, "screenshotPath"    # Ljava/lang/String;
    .param p10, "timestampMillis"    # J

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-wide p1, p0, Lcom/googlecode/eyesfree/labeling/Label;->mId:J

    .line 64
    iput-object p3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageName:Ljava/lang/String;

    .line 65
    iput-object p4, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageSignature:Ljava/lang/String;

    .line 66
    iput-object p5, p0, Lcom/googlecode/eyesfree/labeling/Label;->mViewName:Ljava/lang/String;

    .line 67
    iput-object p6, p0, Lcom/googlecode/eyesfree/labeling/Label;->mText:Ljava/lang/String;

    .line 68
    iput-object p7, p0, Lcom/googlecode/eyesfree/labeling/Label;->mLocale:Ljava/lang/String;

    .line 69
    iput p8, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageVersion:I

    .line 70
    iput-object p9, p0, Lcom/googlecode/eyesfree/labeling/Label;->mScreenshotPath:Ljava/lang/String;

    .line 71
    iput-wide p10, p0, Lcom/googlecode/eyesfree/labeling/Label;->mTimestampMillis:J

    .line 72
    return-void
.end method

.method public constructor <init>(Lcom/googlecode/eyesfree/labeling/Label;J)V
    .locals 4
    .param p1, "labelWithoutId"    # Lcom/googlecode/eyesfree/labeling/Label;
    .param p2, "labelId"    # J

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/labeling/Label;->getId()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 104
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Label to copy cannot have an ID already assigned."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_0
    iput-wide p2, p0, Lcom/googlecode/eyesfree/labeling/Label;->mId:J

    .line 108
    iget-object v0, p1, Lcom/googlecode/eyesfree/labeling/Label;->mPackageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageName:Ljava/lang/String;

    .line 109
    iget-object v0, p1, Lcom/googlecode/eyesfree/labeling/Label;->mPackageSignature:Ljava/lang/String;

    iput-object v0, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageSignature:Ljava/lang/String;

    .line 110
    iget-object v0, p1, Lcom/googlecode/eyesfree/labeling/Label;->mViewName:Ljava/lang/String;

    iput-object v0, p0, Lcom/googlecode/eyesfree/labeling/Label;->mViewName:Ljava/lang/String;

    .line 111
    iget-object v0, p1, Lcom/googlecode/eyesfree/labeling/Label;->mText:Ljava/lang/String;

    iput-object v0, p0, Lcom/googlecode/eyesfree/labeling/Label;->mText:Ljava/lang/String;

    .line 112
    iget-object v0, p1, Lcom/googlecode/eyesfree/labeling/Label;->mLocale:Ljava/lang/String;

    iput-object v0, p0, Lcom/googlecode/eyesfree/labeling/Label;->mLocale:Ljava/lang/String;

    .line 113
    iget v0, p1, Lcom/googlecode/eyesfree/labeling/Label;->mPackageVersion:I

    iput v0, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageVersion:I

    .line 114
    iget-object v0, p1, Lcom/googlecode/eyesfree/labeling/Label;->mScreenshotPath:Ljava/lang/String;

    iput-object v0, p0, Lcom/googlecode/eyesfree/labeling/Label;->mScreenshotPath:Ljava/lang/String;

    .line 115
    iget-wide v0, p1, Lcom/googlecode/eyesfree/labeling/Label;->mTimestampMillis:J

    iput-wide v0, p0, Lcom/googlecode/eyesfree/labeling/Label;->mTimestampMillis:J

    .line 116
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 12
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "packageSignature"    # Ljava/lang/String;
    .param p3, "viewName"    # Ljava/lang/String;
    .param p4, "text"    # Ljava/lang/String;
    .param p5, "locale"    # Ljava/lang/String;
    .param p6, "packageVersion"    # I
    .param p7, "screenshotPath"    # Ljava/lang/String;
    .param p8, "timestamp"    # J

    .prologue
    .line 89
    const-wide/16 v1, -0x1

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    move-wide/from16 v10, p8

    invoke-direct/range {v0 .. v11}, Lcom/googlecode/eyesfree/labeling/Label;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;J)V

    .line 91
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 260
    if-ne p0, p1, :cond_1

    .line 330
    :cond_0
    :goto_0
    return v1

    .line 264
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 265
    goto :goto_0

    .line 268
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 269
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 272
    check-cast v0, Lcom/googlecode/eyesfree/labeling/Label;

    .line 274
    .local v0, "other":Lcom/googlecode/eyesfree/labeling/Label;
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mLocale:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 275
    iget-object v3, v0, Lcom/googlecode/eyesfree/labeling/Label;->mLocale:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    .line 276
    goto :goto_0

    .line 278
    :cond_4
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mLocale:Ljava/lang/String;

    iget-object v4, v0, Lcom/googlecode/eyesfree/labeling/Label;->mLocale:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 279
    goto :goto_0

    .line 282
    :cond_5
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageName:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 283
    iget-object v3, v0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageName:Ljava/lang/String;

    if-eqz v3, :cond_7

    move v1, v2

    .line 284
    goto :goto_0

    .line 286
    :cond_6
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageName:Ljava/lang/String;

    iget-object v4, v0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 287
    goto :goto_0

    .line 290
    :cond_7
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageSignature:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 291
    iget-object v3, v0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageSignature:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v1, v2

    .line 292
    goto :goto_0

    .line 294
    :cond_8
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageSignature:Ljava/lang/String;

    iget-object v4, v0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageSignature:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 295
    goto :goto_0

    .line 298
    :cond_9
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mScreenshotPath:Ljava/lang/String;

    if-nez v3, :cond_a

    .line 299
    iget-object v3, v0, Lcom/googlecode/eyesfree/labeling/Label;->mScreenshotPath:Ljava/lang/String;

    if-eqz v3, :cond_b

    move v1, v2

    .line 300
    goto :goto_0

    .line 302
    :cond_a
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mScreenshotPath:Ljava/lang/String;

    iget-object v4, v0, Lcom/googlecode/eyesfree/labeling/Label;->mScreenshotPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    move v1, v2

    .line 303
    goto :goto_0

    .line 306
    :cond_b
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mText:Ljava/lang/String;

    if-nez v3, :cond_c

    .line 307
    iget-object v3, v0, Lcom/googlecode/eyesfree/labeling/Label;->mText:Ljava/lang/String;

    if-eqz v3, :cond_d

    move v1, v2

    .line 308
    goto :goto_0

    .line 310
    :cond_c
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mText:Ljava/lang/String;

    iget-object v4, v0, Lcom/googlecode/eyesfree/labeling/Label;->mText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    move v1, v2

    .line 311
    goto/16 :goto_0

    .line 314
    :cond_d
    iget-wide v4, p0, Lcom/googlecode/eyesfree/labeling/Label;->mTimestampMillis:J

    iget-wide v6, v0, Lcom/googlecode/eyesfree/labeling/Label;->mTimestampMillis:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_e

    move v1, v2

    .line 315
    goto/16 :goto_0

    .line 318
    :cond_e
    iget v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageVersion:I

    iget v4, v0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageVersion:I

    if-eq v3, v4, :cond_f

    move v1, v2

    .line 319
    goto/16 :goto_0

    .line 322
    :cond_f
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mViewName:Ljava/lang/String;

    if-nez v3, :cond_10

    .line 323
    iget-object v3, v0, Lcom/googlecode/eyesfree/labeling/Label;->mViewName:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 324
    goto/16 :goto_0

    .line 326
    :cond_10
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mViewName:Ljava/lang/String;

    iget-object v4, v0, Lcom/googlecode/eyesfree/labeling/Label;->mViewName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 327
    goto/16 :goto_0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 123
    iget-wide v0, p0, Lcom/googlecode/eyesfree/labeling/Label;->mId:J

    return-wide v0
.end method

.method public getLocale()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/Label;->mLocale:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageSignature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageSignature:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageVersion()I
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageVersion:I

    return v0
.end method

.method public getScreenshotPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/Label;->mScreenshotPath:Ljava/lang/String;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/Label;->mText:Ljava/lang/String;

    return-object v0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 232
    iget-wide v0, p0, Lcom/googlecode/eyesfree/labeling/Label;->mTimestampMillis:J

    return-wide v0
.end method

.method public getViewName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/Label;->mViewName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 245
    const/16 v0, 0x1f

    .line 246
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 247
    .local v1, "result":I
    iget-object v2, p0, Lcom/googlecode/eyesfree/labeling/Label;->mLocale:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 248
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageName:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 249
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageSignature:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 250
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/googlecode/eyesfree/labeling/Label;->mScreenshotPath:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 251
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/googlecode/eyesfree/labeling/Label;->mText:Ljava/lang/String;

    if-nez v2, :cond_4

    move v2, v3

    :goto_4
    add-int v1, v4, v2

    .line 252
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/googlecode/eyesfree/labeling/Label;->mTimestampMillis:J

    iget-wide v6, p0, Lcom/googlecode/eyesfree/labeling/Label;->mTimestampMillis:J

    const/16 v8, 0x20

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int v1, v2, v4

    .line 253
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageVersion:I

    add-int v1, v2, v4

    .line 254
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/googlecode/eyesfree/labeling/Label;->mViewName:Ljava/lang/String;

    if-nez v4, :cond_5

    :goto_5
    add-int v1, v2, v3

    .line 255
    return v1

    .line 247
    :cond_0
    iget-object v2, p0, Lcom/googlecode/eyesfree/labeling/Label;->mLocale:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 248
    :cond_1
    iget-object v2, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 249
    :cond_2
    iget-object v2, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageSignature:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 250
    :cond_3
    iget-object v2, p0, Lcom/googlecode/eyesfree/labeling/Label;->mScreenshotPath:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    .line 251
    :cond_4
    iget-object v2, p0, Lcom/googlecode/eyesfree/labeling/Label;->mText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4

    .line 254
    :cond_5
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mViewName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_5
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 182
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/Label;->mText:Ljava/lang/String;

    .line 183
    return-void
.end method

.method public setTimestamp(J)V
    .locals 1
    .param p1, "timestampMillis"    # J

    .prologue
    .line 240
    iput-wide p1, p0, Lcom/googlecode/eyesfree/labeling/Label;->mTimestampMillis:J

    .line 241
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 338
    const-string v0, "%s[id=%d, packageName=%s, packageSignature=%s, viewName=%s, text=%s, locale=%s, packageVersion=%d, screenshotPath=%s, timestamp=%d]"

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/googlecode/eyesfree/labeling/Label;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageSignature:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mViewName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mText:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mLocale:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mPackageVersion:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/Label;->mScreenshotPath:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    iget-wide v4, p0, Lcom/googlecode/eyesfree/labeling/Label;->mTimestampMillis:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
