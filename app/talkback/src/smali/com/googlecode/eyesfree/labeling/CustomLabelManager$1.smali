.class Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;
.super Ljava/lang/Object;
.source "CustomLabelManager.java"

# interfaces
.implements Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->prefetchLabelsForPackage(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 594
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    iput-object p2, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;->val$packageName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLabelsFetched(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 597
    .local p1, "results":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/googlecode/eyesfree/labeling/Label;>;"
    if-eqz p1, :cond_0

    .line 598
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    # getter for: Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mLabelCache:Ljava/util/Map;
    invoke-static {v0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->access$800(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;->val$packageName:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 600
    :cond_0
    return-void
.end method
