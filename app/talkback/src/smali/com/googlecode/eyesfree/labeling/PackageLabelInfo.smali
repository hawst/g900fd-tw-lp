.class public Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;
.super Ljava/lang/Object;
.source "PackageLabelInfo.java"


# instance fields
.field private mLabelCount:I

.field private mPackageName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "labelCount"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;->mPackageName:Ljava/lang/String;

    .line 36
    iput p2, p0, Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;->mLabelCount:I

    .line 37
    return-void
.end method


# virtual methods
.method public getLabelCount()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;->mLabelCount:I

    return v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method
