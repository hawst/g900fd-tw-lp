.class Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver$1;
.super Ljava/lang/Object;
.source "PackageRemovalReceiver.java"

# interfaces
.implements Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;

.field final synthetic val$labelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;


# direct methods
.method constructor <init>(Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver$1;->this$0:Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;

    iput-object p2, p0, Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver$1;->val$labelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLabelsFetched(Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "results":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/googlecode/eyesfree/labeling/Label;>;"
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 46
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 47
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    .line 48
    .local v2, "labels":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/googlecode/eyesfree/labeling/Label;>;"
    const/4 v3, 0x2

    const-string v4, "Removing %d labels."

    new-array v5, v8, [Ljava/lang/Object;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {p0, v3, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 49
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/labeling/Label;

    .line 50
    .local v1, "l":Lcom/googlecode/eyesfree/labeling/Label;
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver$1;->val$labelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    new-array v4, v8, [Lcom/googlecode/eyesfree/labeling/Label;

    aput-object v1, v4, v7

    invoke-virtual {v3, v4}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->removeLabel([Lcom/googlecode/eyesfree/labeling/Label;)V

    goto :goto_0

    .line 54
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "l":Lcom/googlecode/eyesfree/labeling/Label;
    .end local v2    # "labels":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/googlecode/eyesfree/labeling/Label;>;"
    :cond_0
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver$1;->val$labelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    invoke-virtual {v3}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->shutdown()V

    .line 55
    return-void
.end method
