.class public Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PackageRemovalReceiver.java"


# static fields
.field private static final INTENT_FILTER:Landroid/content/IntentFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;->INTENT_FILTER:Landroid/content/IntentFilter;

    .line 27
    sget-object v0, Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;->INTENT_FILTER:Landroid/content/IntentFilter;

    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 28
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public getFilter()Landroid/content/IntentFilter;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;->INTENT_FILTER:Landroid/content/IntentFilter;

    return-object v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    .line 32
    const-string v4, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 33
    const-string v4, "android.intent.extra.REPLACING"

    invoke-virtual {p2, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 34
    .local v1, "isUpgrade":Z
    if-nez v1, :cond_0

    .line 35
    new-instance v2, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    invoke-direct {v2, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;-><init>(Landroid/content/Context;)V

    .line 36
    .local v2, "labelManager":Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 37
    .local v3, "packageName":Ljava/lang/String;
    const/4 v4, 0x2

    const-string v5, "Package %s removed.  Discarding associated labels."

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v3, v6, v7

    invoke-static {p0, v4, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 41
    new-instance v0, Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver$1;

    invoke-direct {v0, p0, v2}, Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver$1;-><init>(Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V

    .line 58
    .local v0, "callback":Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;
    invoke-virtual {v2, v3, v0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->getLabelsForPackageFromDatabase(Ljava/lang/String;Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;)V

    .line 61
    .end local v0    # "callback":Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;
    .end local v1    # "isUpgrade":Z
    .end local v2    # "labelManager":Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_0
    return-void
.end method
