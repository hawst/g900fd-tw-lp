.class Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DirectLabelFetchTask;
.super Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;
.source "CustomLabelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DirectLabelFetchTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask",
        "<",
        "Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;",
        "Ljava/lang/Void;",
        "Lcom/googlecode/eyesfree/labeling/Label;",
        ">;"
    }
.end annotation


# instance fields
.field private mRequest:Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;

.field final synthetic this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;


# direct methods
.method private constructor <init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V
    .locals 1

    .prologue
    .line 681
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DirectLabelFetchTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    .param p2, "x1"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;

    .prologue
    .line 681
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DirectLabelFetchTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;)Lcom/googlecode/eyesfree/labeling/Label;
    .locals 7
    .param p1, "requests"    # [Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 688
    if-eqz p1, :cond_0

    array-length v2, p1

    if-eq v2, v4, :cond_1

    .line 689
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Direct label fetch task supports only single requests."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 693
    :cond_1
    aget-object v2, p1, v6

    iput-object v2, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DirectLabelFetchTask;->mRequest:Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;

    .line 694
    const/4 v2, 0x2

    const-string v3, "Spawning new DirectLabelFetchTask(%d)"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 696
    iget-object v2, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DirectLabelFetchTask;->mRequest:Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;->getLabelId()J

    move-result-wide v0

    .line 697
    .local v0, "labelId":J
    iget-object v2, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DirectLabelFetchTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    # getter for: Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;
    invoke-static {v2}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->access$1200(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->getLabelById(J)Lcom/googlecode/eyesfree/labeling/Label;

    move-result-object v2

    return-object v2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 681
    check-cast p1, [Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DirectLabelFetchTask;->doInBackground([Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;)Lcom/googlecode/eyesfree/labeling/Label;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/googlecode/eyesfree/labeling/Label;)V
    .locals 5
    .param p1, "result"    # Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    const/4 v4, 0x2

    .line 702
    const-string v0, "DirectLabelFetchTask(%d) complete.  Obtained label %s"

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {p0, v4, v0, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 705
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DirectLabelFetchTask;->mRequest:Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;->invokeCallback(Lcom/googlecode/eyesfree/labeling/Label;)V

    .line 706
    invoke-super {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 707
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 681
    check-cast p1, Lcom/googlecode/eyesfree/labeling/Label;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DirectLabelFetchTask;->onPostExecute(Lcom/googlecode/eyesfree/labeling/Label;)V

    return-void
.end method
