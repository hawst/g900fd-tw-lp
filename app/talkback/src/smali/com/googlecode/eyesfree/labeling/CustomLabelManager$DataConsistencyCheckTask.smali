.class Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DataConsistencyCheckTask;
.super Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;
.source "CustomLabelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataConsistencyCheckTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/googlecode/eyesfree/labeling/Label;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;


# direct methods
.method private constructor <init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V
    .locals 1

    .prologue
    .line 884
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DataConsistencyCheckTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    .param p2, "x1"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;

    .prologue
    .line 884
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DataConsistencyCheckTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 884
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DataConsistencyCheckTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 15
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;"
        }
    .end annotation

    .prologue
    .line 888
    iget-object v10, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DataConsistencyCheckTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    # getter for: Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;
    invoke-static {v10}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->access$1200(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    move-result-object v10

    invoke-virtual {v10}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->getAllLabels()Ljava/util/List;

    move-result-object v1

    .line 890
    .local v1, "allLabels":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/labeling/Label;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 891
    :cond_0
    const/4 v2, 0x0

    .line 939
    :cond_1
    return-object v2

    .line 894
    :cond_2
    iget-object v10, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DataConsistencyCheckTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    # getter for: Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->access$1500(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    .line 896
    .local v9, "pm":Landroid/content/pm/PackageManager;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 897
    .local v2, "candidates":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/labeling/Label;>;"
    invoke-interface {v2}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v5

    .line 901
    .local v5, "i":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/googlecode/eyesfree/labeling/Label;>;"
    :goto_0
    invoke-interface {v5}, Ljava/util/ListIterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 902
    invoke-interface {v5}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/googlecode/eyesfree/labeling/Label;

    .line 905
    .local v6, "l":Lcom/googlecode/eyesfree/labeling/Label;
    invoke-virtual {v6}, Lcom/googlecode/eyesfree/labeling/Label;->getPackageName()Ljava/lang/String;

    move-result-object v8

    .line 906
    .local v8, "packageName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 908
    .local v7, "packageInfo":Landroid/content/pm/PackageInfo;
    const/16 v10, 0x40

    :try_start_0
    invoke-virtual {v9, v8, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 920
    invoke-virtual {v6}, Lcom/googlecode/eyesfree/labeling/Label;->getPackageSignature()Ljava/lang/String;

    move-result-object v4

    .line 921
    .local v4, "expectedHash":Ljava/lang/String;
    # invokes: Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->computePackageSignatureHash(Landroid/content/pm/PackageInfo;)Ljava/lang/String;
    invoke-static {v7}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->access$1600(Landroid/content/pm/PackageInfo;)Ljava/lang/String;

    move-result-object v0

    .line 922
    .local v0, "actualHash":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 927
    :cond_3
    const-class v10, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    const/4 v11, 0x5

    const-string v12, "Consistency check removing label due to signature mismatch for package %s."

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v8, v13, v14

    invoke-static {v10, v11, v12, v13}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 909
    .end local v0    # "actualHash":Ljava/lang/String;
    .end local v4    # "expectedHash":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 912
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-class v10, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    const/4 v11, 0x2

    const-string v12, "Consistency check removing label for unknown package %s."

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v8, v13, v14

    invoke-static {v10, v11, v12, v13}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 936
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0    # "actualHash":Ljava/lang/String;
    .restart local v4    # "expectedHash":Ljava/lang/String;
    :cond_4
    invoke-interface {v5}, Ljava/util/ListIterator;->remove()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 884
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DataConsistencyCheckTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "labelsToRemove":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/labeling/Label;>;"
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 944
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 955
    :cond_0
    :goto_0
    return-void

    .line 948
    :cond_1
    const/4 v2, 0x2

    const-string v3, "Found %d labels to remove during consistency check"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 950
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/labeling/Label;

    .line 951
    .local v1, "l":Lcom/googlecode/eyesfree/labeling/Label;
    iget-object v2, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DataConsistencyCheckTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    new-array v3, v7, [Lcom/googlecode/eyesfree/labeling/Label;

    aput-object v1, v3, v6

    invoke-virtual {v2, v3}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->removeLabel([Lcom/googlecode/eyesfree/labeling/Label;)V

    goto :goto_1

    .line 954
    .end local v1    # "l":Lcom/googlecode/eyesfree/labeling/Label;
    :cond_2
    invoke-super {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    goto :goto_0
.end method
