.class Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelRemoveTask;
.super Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;
.source "CustomLabelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LabelRemoveTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask",
        "<",
        "Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private mRequest:Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest;

.field final synthetic this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;


# direct methods
.method private constructor <init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V
    .locals 1

    .prologue
    .line 847
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelRemoveTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    .param p2, "x1"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;

    .prologue
    .line 847
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelRemoveTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest;)Ljava/lang/Boolean;
    .locals 7
    .param p1, "requests"    # [Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 853
    if-eqz p1, :cond_0

    array-length v1, p1

    if-eq v1, v4, :cond_1

    .line 854
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Remove task supports only single Label removals."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 858
    :cond_1
    aget-object v1, p1, v6

    iput-object v1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelRemoveTask;->mRequest:Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest;

    .line 860
    const-string v1, "Spawning new LabelRemoveTask(%d) for label: %s"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelRemoveTask;->mRequest:Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest;

    invoke-virtual {v3}, Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest;->getLabel()Lcom/googlecode/eyesfree/labeling/Label;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p0, v5, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 863
    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelRemoveTask;->mRequest:Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest;->getLabel()Lcom/googlecode/eyesfree/labeling/Label;

    move-result-object v0

    .line 864
    .local v0, "label":Lcom/googlecode/eyesfree/labeling/Label;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/labeling/Label;->getId()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 865
    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelRemoveTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    # getter for: Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;
    invoke-static {v1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->access$1200(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->deleteLabel(Lcom/googlecode/eyesfree/labeling/Label;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 868
    :goto_0
    return-object v1

    :cond_2
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 847
    check-cast p1, [Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelRemoveTask;->doInBackground([Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 873
    const-string v0, "LabelRemoveTask(%d) complete.  Removed: %s."

    new-array v1, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object p1, v1, v4

    invoke-static {p0, v5, v0, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 876
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 877
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelRemoveTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    new-array v1, v4, [Ljava/lang/String;

    iget-object v2, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelRemoveTask;->mRequest:Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest;

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest;->getLabel()Lcom/googlecode/eyesfree/labeling/Label;

    move-result-object v2

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/labeling/Label;->getPackageName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    # invokes: Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->sendCacheRefreshIntent([Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->access$1400(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;[Ljava/lang/String;)V

    .line 880
    :cond_0
    invoke-super {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 881
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 847
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelRemoveTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
