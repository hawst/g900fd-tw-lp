.class public Lcom/googlecode/eyesfree/labeling/LabelOperationUtils;
.super Ljava/lang/Object;
.source "LabelOperationUtils.java"


# direct methods
.method public static startActivityAddLabelForNode(Landroid/content/Context;Landroid/view/accessibility/AccessibilityNodeInfo;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    const/4 v3, 0x0

    .line 54
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v3

    .line 58
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.google.android.marvin.talkback.labeling.ADD_LABEL"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 59
    .local v0, "addIntent":Landroid/content/Intent;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 60
    .local v2, "extras":Landroid/os/Bundle;
    const-string v4, "EXTRA_STRING_RESOURCE_NAME"

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getViewIdResourceName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const/high16 v4, 0x10000000

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 62
    const/high16 v4, 0x4000000

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 63
    invoke-virtual {v0, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 66
    :try_start_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    const/4 v3, 0x1

    goto :goto_0

    .line 68
    :catch_0
    move-exception v1

    .line 69
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static startActivityEditLabel(Landroid/content/Context;Lcom/googlecode/eyesfree/labeling/Label;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "label"    # Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    const/4 v3, 0x0

    .line 75
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 91
    :cond_0
    :goto_0
    return v3

    .line 79
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.google.android.marvin.talkback.labeling.EDIT_LABEL"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 80
    .local v1, "editIntent":Landroid/content/Intent;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 81
    .local v2, "extras":Landroid/os/Bundle;
    const-string v4, "EXTRA_LONG_LABEL_ID"

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/labeling/Label;->getId()J

    move-result-wide v6

    invoke-virtual {v2, v4, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 82
    const/high16 v4, 0x10000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 83
    const/high16 v4, 0x4000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 84
    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 87
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    const/4 v3, 0x1

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static startActivityRemoveLabel(Landroid/content/Context;Lcom/googlecode/eyesfree/labeling/Label;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "label"    # Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    const/4 v3, 0x0

    .line 96
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 112
    :cond_0
    :goto_0
    return v3

    .line 100
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.google.android.marvin.talkback.labeling.REMOVE_LABEL"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 101
    .local v2, "removeIntent":Landroid/content/Intent;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 102
    .local v1, "extras":Landroid/os/Bundle;
    const-string v4, "EXTRA_LONG_LABEL_ID"

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/labeling/Label;->getId()J

    move-result-wide v6

    invoke-virtual {v1, v4, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 103
    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 104
    const/high16 v4, 0x4000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 105
    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 108
    :try_start_0
    invoke-virtual {p0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    const/4 v3, 0x1

    goto :goto_0

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
