.class abstract Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;
.super Landroid/os/AsyncTask;
.source "CustomLabelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "TrackedAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Params:",
        "Ljava/lang/Object;",
        "Progress:",
        "Ljava/lang/Object;",
        "Result:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/AsyncTask",
        "<TParams;TProgress;TResult;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;


# direct methods
.method private constructor <init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V
    .locals 0

    .prologue
    .line 651
    .local p0, "this":Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;, "Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask<TParams;TProgress;TResult;>;"
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    .param p2, "x1"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;

    .prologue
    .line 651
    .local p0, "this":Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;, "Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask<TParams;TProgress;TResult;>;"
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V

    return-void
.end method


# virtual methods
.method protected varargs abstract doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)TResult;"
        }
    .end annotation
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .prologue
    .line 670
    .local p0, "this":Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;, "Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask<TParams;TProgress;TResult;>;"
    .local p1, "result":Ljava/lang/Object;, "TResult;"
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    # invokes: Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->taskEnding(Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;)V
    invoke-static {v0, p0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->access$1000(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;)V

    .line 671
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 672
    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 656
    .local p0, "this":Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;, "Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask<TParams;TProgress;TResult;>;"
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    # invokes: Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->taskStarting(Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;)V
    invoke-static {v0, p0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->access$900(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;)V

    .line 657
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 658
    return-void
.end method
