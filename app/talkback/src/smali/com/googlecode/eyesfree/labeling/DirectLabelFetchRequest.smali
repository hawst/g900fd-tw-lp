.class public Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;
.super Ljava/lang/Object;
.source "DirectLabelFetchRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest$OnLabelFetchedListener;
    }
.end annotation


# instance fields
.field private final mLabelId:J

.field private final mListener:Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest$OnLabelFetchedListener;


# direct methods
.method public constructor <init>(JLcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest$OnLabelFetchedListener;)V
    .locals 1
    .param p1, "labelId"    # J
    .param p3, "listener"    # Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest$OnLabelFetchedListener;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-wide p1, p0, Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;->mLabelId:J

    .line 26
    iput-object p3, p0, Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;->mListener:Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest$OnLabelFetchedListener;

    .line 27
    return-void
.end method


# virtual methods
.method public getLabelId()J
    .locals 2

    .prologue
    .line 30
    iget-wide v0, p0, Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;->mLabelId:J

    return-wide v0
.end method

.method protected invokeCallback(Lcom/googlecode/eyesfree/labeling/Label;)V
    .locals 1
    .param p1, "result"    # Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;->mListener:Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest$OnLabelFetchedListener;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;->mListener:Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest$OnLabelFetchedListener;

    invoke-interface {v0, p1}, Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest$OnLabelFetchedListener;->onLabelFetched(Lcom/googlecode/eyesfree/labeling/Label;)V

    .line 37
    :cond_0
    return-void
.end method
