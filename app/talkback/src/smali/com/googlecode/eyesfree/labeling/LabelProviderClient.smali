.class public Lcom/googlecode/eyesfree/labeling/LabelProviderClient;
.super Ljava/lang/Object;
.source "LabelProviderClient.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field private static final GET_LABELS_FOR_APPLICATION_QUERY_WHERE:Ljava/lang/String;

.field private static final GET_LABEL_QUERY_WHERE:Ljava/lang/String;

.field private static final PACKAGE_SUMMARY_QUERY_WHERE:Ljava/lang/String;


# instance fields
.field private mClient:Landroid/content/ContentProviderClient;

.field private final mLabelsContentUri:Landroid/net/Uri;

.field private final mPackageSummaryContentUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "packageName"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "locale"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "packageVersion"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " <= ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->GET_LABELS_FOR_APPLICATION_QUERY_WHERE:Ljava/lang/String;

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "packageName"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "viewName"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "locale"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "packageVersion"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " <= ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->GET_LABEL_QUERY_WHERE:Ljava/lang/String;

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "locale"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->PACKAGE_SUMMARY_QUERY_WHERE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "authority"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "content"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "labels"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mLabelsContentUri:Landroid/net/Uri;

    .line 85
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "content"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "packageSummary"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mPackageSummaryContentUri:Landroid/net/Uri;

    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 92
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mLabelsContentUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v1

    iput-object v1, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mClient:Landroid/content/ContentProviderClient;

    .line 94
    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mClient:Landroid/content/ContentProviderClient;

    if-nez v1, :cond_0

    .line 95
    const/4 v1, 0x5

    const-string v2, "Failed to acquire content provider client."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 97
    :cond_0
    return-void
.end method

.method private static buildContentValuesForLabel(Lcom/googlecode/eyesfree/labeling/Label;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "label"    # Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    .line 444
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 446
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "packageName"

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/labeling/Label;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    const-string v1, "packageSignature"

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/labeling/Label;->getPackageSignature()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    const-string v1, "viewName"

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/labeling/Label;->getViewName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    const-string v1, "text"

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/labeling/Label;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    const-string v1, "locale"

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/labeling/Label;->getLocale()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    const-string v1, "packageVersion"

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/labeling/Label;->getPackageVersion()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 452
    const-string v1, "screenshotPath"

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/labeling/Label;->getScreenshotPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    const-string v1, "timestamp"

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/labeling/Label;->getTimestamp()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 455
    return-object v0
.end method

.method private checkClient()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 635
    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mClient:Landroid/content/ContentProviderClient;

    if-nez v1, :cond_0

    .line 636
    const/4 v1, 0x5

    const-string v2, "Aborting operation: the client failed to initialize or already shut down."

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 641
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getLabelFromCursor(Landroid/database/Cursor;)Lcom/googlecode/eyesfree/labeling/Label;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 515
    if-nez p1, :cond_0

    .line 516
    const/4 v0, 0x0

    .line 524
    :goto_0
    return-object v0

    .line 519
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 520
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->getLabelFromCursorAtCurrentPosition(Landroid/database/Cursor;)Lcom/googlecode/eyesfree/labeling/Label;

    move-result-object v0

    .line 522
    .local v0, "result":Lcom/googlecode/eyesfree/labeling/Label;
    invoke-direct {p0, v0}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->logResult(Lcom/googlecode/eyesfree/labeling/Label;)V

    goto :goto_0
.end method

.method private getLabelFromCursorAtCurrentPosition(Landroid/database/Cursor;)Lcom/googlecode/eyesfree/labeling/Label;
    .locals 14
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v13, 0x5

    const/4 v12, 0x0

    .line 467
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 468
    :cond_0
    const-string v0, "Failed to get label from cursor."

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {p0, v13, v0, v12}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 469
    const/4 v0, 0x0

    .line 482
    :goto_0
    return-object v0

    .line 472
    :cond_1
    invoke-interface {p1, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 473
    .local v1, "labelId":J
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 474
    .local v3, "packageName":Ljava/lang/String;
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 475
    .local v4, "packageSignature":Ljava/lang/String;
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 476
    .local v5, "viewName":Ljava/lang/String;
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 477
    .local v6, "text":Ljava/lang/String;
    invoke-interface {p1, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 478
    .local v7, "locale":Ljava/lang/String;
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 479
    .local v8, "packageVersion":I
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 480
    .local v9, "screenshotPath":Ljava/lang/String;
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 482
    .local v10, "timestamp":J
    new-instance v0, Lcom/googlecode/eyesfree/labeling/Label;

    invoke-direct/range {v0 .. v11}, Lcom/googlecode/eyesfree/labeling/Label;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;J)V

    goto :goto_0
.end method

.method private getLabelListFromCursor(Landroid/database/Cursor;)Ljava/util/List;
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;"
        }
    .end annotation

    .prologue
    .line 534
    if-nez p1, :cond_0

    .line 535
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 548
    :goto_0
    return-object v2

    .line 538
    :cond_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 539
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/labeling/Label;>;"
    :cond_1
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 540
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->getLabelFromCursorAtCurrentPosition(Landroid/database/Cursor;)Lcom/googlecode/eyesfree/labeling/Label;

    move-result-object v0

    .line 541
    .local v0, "label":Lcom/googlecode/eyesfree/labeling/Label;
    if-eqz v0, :cond_1

    .line 542
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 546
    .end local v0    # "label":Lcom/googlecode/eyesfree/labeling/Label;
    :cond_2
    invoke-direct {p0, v1}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->logResult(Ljava/lang/Iterable;)V

    .line 548
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    goto :goto_0
.end method

.method private getLabelMapFromCursor(Landroid/database/Cursor;)Ljava/util/Map;
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;"
        }
    .end annotation

    .prologue
    .line 582
    if-nez p1, :cond_0

    .line 583
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v4

    .line 599
    :goto_0
    return-object v4

    .line 586
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 587
    .local v2, "labelCount":I
    const/4 v4, 0x0

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 589
    .local v0, "initialCapacity":I
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 590
    .local v3, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/googlecode/eyesfree/labeling/Label;>;"
    :cond_1
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 591
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->getLabelFromCursorAtCurrentPosition(Landroid/database/Cursor;)Lcom/googlecode/eyesfree/labeling/Label;

    move-result-object v1

    .line 592
    .local v1, "label":Lcom/googlecode/eyesfree/labeling/Label;
    if-eqz v1, :cond_1

    .line 593
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/labeling/Label;->getViewName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 597
    .end local v1    # "label":Lcom/googlecode/eyesfree/labeling/Label;
    :cond_2
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->logResult(Ljava/lang/Iterable;)V

    .line 599
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v4

    goto :goto_0
.end method

.method private getPackageLabelInfoFromCursor(Landroid/database/Cursor;)Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v4, 0x0

    .line 495
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 496
    :cond_0
    const/4 v2, 0x5

    const-string v3, "Failed to get PackageLabelInfo from cursor."

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 497
    const/4 v2, 0x0

    .line 504
    :goto_0
    return-object v2

    .line 500
    :cond_1
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 501
    .local v1, "packageName":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 504
    .local v0, "labelCount":I
    new-instance v2, Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;

    invoke-direct {v2, v1, v0}, Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;-><init>(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private getPackageSummaryFromCursor(Landroid/database/Cursor;)Ljava/util/List;
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 559
    if-nez p1, :cond_0

    .line 560
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 571
    :goto_0
    return-object v2

    .line 563
    :cond_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 564
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;>;"
    :cond_1
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 565
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->getPackageLabelInfoFromCursor(Landroid/database/Cursor;)Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;

    move-result-object v0

    .line 566
    .local v0, "packageLabelInfo":Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;
    if-eqz v0, :cond_1

    .line 567
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 571
    .end local v0    # "packageLabelInfo":Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;
    :cond_2
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    goto :goto_0
.end method

.method private logResult(Lcom/googlecode/eyesfree/labeling/Label;)V
    .locals 4
    .param p1, "label"    # Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    .line 608
    const/4 v0, 0x2

    const-string v1, "Query result: %s."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 609
    return-void
.end method

.method private logResult(Ljava/lang/Iterable;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/googlecode/eyesfree/labeling/Label;>;"
    const/4 v5, 0x2

    .line 617
    sget v3, Lcom/googlecode/eyesfree/utils/LogUtils;->LOG_LEVEL:I

    if-lt v3, v5, :cond_1

    .line 618
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Query result: ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 619
    .local v2, "logMessageBuilder":Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/labeling/Label;

    .line 620
    .local v1, "label":Lcom/googlecode/eyesfree/labeling/Label;
    const-string v3, "\n  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 621
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 623
    .end local v1    # "label":Lcom/googlecode/eyesfree/labeling/Label;
    :cond_0
    const-string v3, "]."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 625
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0, v5, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 627
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "logMessageBuilder":Ljava/lang/StringBuilder;
    :cond_1
    return-void
.end method


# virtual methods
.method public deleteLabel(Lcom/googlecode/eyesfree/labeling/Label;)Z
    .locals 10
    .param p1, "label"    # Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 390
    const/4 v7, 0x3

    const-string v8, "Deleting label: %s."

    new-array v9, v5, [Ljava/lang/Object;

    aput-object p1, v9, v6

    invoke-static {p0, v7, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 392
    if-nez p1, :cond_1

    .line 415
    :cond_0
    :goto_0
    return v6

    .line 396
    :cond_1
    invoke-direct {p0}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->checkClient()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 400
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/labeling/Label;->getId()J

    move-result-wide v2

    .line 402
    .local v2, "labelId":J
    const-wide/16 v8, -0x1

    cmp-long v7, v2, v8

    if-nez v7, :cond_2

    .line 403
    const/4 v5, 0x5

    const-string v7, "Cannot delete label with no ID."

    new-array v8, v6, [Ljava/lang/Object;

    invoke-static {p0, v5, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 407
    :cond_2
    iget-object v7, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mLabelsContentUri:Landroid/net/Uri;

    invoke-static {v7, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 410
    .local v4, "uri":Landroid/net/Uri;
    :try_start_0
    iget-object v7, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mClient:Landroid/content/ContentProviderClient;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v4, v8, v9}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 412
    .local v1, "rowsAffected":I
    if-lez v1, :cond_3

    :goto_1
    move v6, v5

    goto :goto_0

    :cond_3
    move v5, v6

    goto :goto_1

    .line 413
    .end local v1    # "rowsAffected":I
    :catch_0
    move-exception v0

    .line 414
    .local v0, "e":Landroid/os/RemoteException;
    const/4 v5, 0x6

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v8, v6, [Ljava/lang/Object;

    invoke-static {p0, v5, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getAllLabels()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 155
    const/4 v0, 0x3

    const-string v1, "Querying all labels."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 157
    invoke-direct {p0}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->checkClient()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v8

    .line 172
    :cond_0
    :goto_0
    return-object v0

    .line 161
    :cond_1
    const/4 v6, 0x0

    .line 163
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mClient:Landroid/content/ContentProviderClient;

    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mLabelsContentUri:Landroid/net/Uri;

    sget-object v2, Lcom/googlecode/eyesfree/labeling/LabelsTable;->ALL_COLUMNS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 166
    invoke-direct {p0, v6}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->getLabelListFromCursor(Landroid/database/Cursor;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 171
    if-eqz v6, :cond_0

    .line 172
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 167
    :catch_0
    move-exception v7

    .line 168
    .local v7, "e":Landroid/os/RemoteException;
    const/4 v0, 0x6

    :try_start_1
    invoke-virtual {v7}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 171
    if-eqz v6, :cond_2

    .line 172
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v8

    goto :goto_0

    .line 171
    .end local v7    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 172
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public getLabelById(J)Lcom/googlecode/eyesfree/labeling/Label;
    .locals 9
    .param p1, "id"    # J

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 319
    const/4 v0, 0x3

    const-string v2, "Querying single label: id=%d."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {p0, v0, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 321
    invoke-direct {p0}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->checkClient()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v8

    .line 337
    :cond_0
    :goto_0
    return-object v0

    .line 325
    :cond_1
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mLabelsContentUri:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 326
    .local v1, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 328
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mClient:Landroid/content/ContentProviderClient;

    sget-object v2, Lcom/googlecode/eyesfree/labeling/LabelsTable;->ALL_COLUMNS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 331
    invoke-direct {p0, v6}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->getLabelFromCursor(Landroid/database/Cursor;)Lcom/googlecode/eyesfree/labeling/Label;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 336
    if-eqz v6, :cond_0

    .line 337
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 332
    :catch_0
    move-exception v7

    .line 333
    .local v7, "e":Landroid/os/RemoteException;
    const/4 v0, 0x6

    :try_start_1
    invoke-virtual {v7}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 336
    if-eqz v6, :cond_2

    .line 337
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v8

    goto :goto_0

    .line 336
    .end local v7    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 337
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public getLabelsForPackage(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "locale"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;"
        }
    .end annotation

    .prologue
    .line 266
    const v0, 0x7fffffff

    invoke-virtual {p0, p1, p2, v0}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->getLabelsForPackage(Ljava/lang/String;Ljava/lang/String;I)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getLabelsForPackage(Ljava/lang/String;Ljava/lang/String;I)Ljava/util/Map;
    .locals 11
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "locale"    # Ljava/lang/String;
    .param p3, "maxPackageVersion"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x3

    const/4 v3, 0x0

    .line 225
    const-string v0, "Querying labels for package: packageName=%s, locale=%s, maxPackageVersion=%s."

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p1, v1, v3

    aput-object p2, v1, v9

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-static {p0, v5, v0, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 229
    invoke-direct {p0}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->checkClient()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v8

    .line 248
    :cond_0
    :goto_0
    return-object v0

    .line 233
    :cond_1
    new-array v4, v5, [Ljava/lang/String;

    aput-object p1, v4, v3

    aput-object p2, v4, v9

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v10

    .line 236
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 238
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mClient:Landroid/content/ContentProviderClient;

    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mLabelsContentUri:Landroid/net/Uri;

    sget-object v2, Lcom/googlecode/eyesfree/labeling/LabelsTable;->ALL_COLUMNS:[Ljava/lang/String;

    sget-object v3, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->GET_LABELS_FOR_APPLICATION_QUERY_WHERE:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 242
    invoke-direct {p0, v6}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->getLabelMapFromCursor(Landroid/database/Cursor;)Ljava/util/Map;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 247
    if-eqz v6, :cond_0

    .line 248
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 243
    :catch_0
    move-exception v7

    .line 244
    .local v7, "e":Landroid/os/RemoteException;
    const/4 v0, 0x6

    :try_start_1
    invoke-virtual {v7}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 247
    if-eqz v6, :cond_2

    .line 248
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v8

    goto :goto_0

    .line 247
    .end local v7    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 248
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public getPackageSummary(Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .param p1, "locale"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 187
    const/4 v0, 0x3

    const-string v1, "Querying package summary."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 189
    invoke-direct {p0}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->checkClient()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v8

    .line 206
    :cond_0
    :goto_0
    return-object v0

    .line 193
    :cond_1
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    aput-object p1, v4, v3

    .line 195
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 197
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mClient:Landroid/content/ContentProviderClient;

    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mPackageSummaryContentUri:Landroid/net/Uri;

    const/4 v2, 0x0

    sget-object v3, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->PACKAGE_SUMMARY_QUERY_WHERE:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 200
    invoke-direct {p0, v6}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->getPackageSummaryFromCursor(Landroid/database/Cursor;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 205
    if-eqz v6, :cond_0

    .line 206
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 201
    :catch_0
    move-exception v7

    .line 202
    .local v7, "e":Landroid/os/RemoteException;
    const/4 v0, 0x6

    :try_start_1
    invoke-virtual {v7}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 205
    if-eqz v6, :cond_2

    .line 206
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v8

    goto :goto_0

    .line 205
    .end local v7    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 206
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public insertLabel(Lcom/googlecode/eyesfree/labeling/Label;)Lcom/googlecode/eyesfree/labeling/Label;
    .locals 12
    .param p1, "label"    # Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    .line 110
    const/4 v7, 0x3

    const-string v8, "Inserting label: %s."

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p1, v9, v10

    invoke-static {p0, v7, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 112
    if-nez p1, :cond_0

    .line 113
    const/4 v7, 0x0

    .line 142
    :goto_0
    return-object v7

    .line 116
    :cond_0
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/labeling/Label;->getId()J

    move-result-wide v2

    .line 117
    .local v2, "labelId":J
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/labeling/Label;->getId()J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-eqz v7, :cond_1

    .line 118
    const/4 v7, 0x5

    const-string v8, "Cannot insert label with existing ID (id=%d)."

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {p0, v7, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 119
    const/4 v7, 0x0

    goto :goto_0

    .line 122
    :cond_1
    invoke-direct {p0}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->checkClient()Z

    move-result v7

    if-nez v7, :cond_2

    .line 123
    const/4 v7, 0x0

    goto :goto_0

    .line 126
    :cond_2
    invoke-static {p1}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->buildContentValuesForLabel(Lcom/googlecode/eyesfree/labeling/Label;)Landroid/content/ContentValues;

    move-result-object v6

    .line 130
    .local v6, "values":Landroid/content/ContentValues;
    :try_start_0
    iget-object v7, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mClient:Landroid/content/ContentProviderClient;

    iget-object v8, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mLabelsContentUri:Landroid/net/Uri;

    invoke-virtual {v7, v8, v6}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 136
    .local v1, "resultUri":Landroid/net/Uri;
    if-nez v1, :cond_3

    .line 137
    const/4 v7, 0x5

    const-string v8, "Failed to insert label."

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {p0, v7, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 138
    const/4 v7, 0x0

    goto :goto_0

    .line 131
    .end local v1    # "resultUri":Landroid/net/Uri;
    :catch_0
    move-exception v0

    .line 132
    .local v0, "e":Landroid/os/RemoteException;
    const/4 v7, 0x6

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {p0, v7, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 133
    const/4 v7, 0x0

    goto :goto_0

    .line 141
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v1    # "resultUri":Landroid/net/Uri;
    :cond_3
    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 142
    .local v4, "newLabelId":J
    new-instance v7, Lcom/googlecode/eyesfree/labeling/Label;

    invoke-direct {v7, p1, v4, v5}, Lcom/googlecode/eyesfree/labeling/Label;-><init>(Lcom/googlecode/eyesfree/labeling/Label;J)V

    goto :goto_0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mClient:Landroid/content/ContentProviderClient;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 423
    invoke-direct {p0}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->checkClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mClient:Landroid/content/ContentProviderClient;

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    .line 425
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mClient:Landroid/content/ContentProviderClient;

    .line 427
    :cond_0
    return-void
.end method

.method public updateLabel(Lcom/googlecode/eyesfree/labeling/Label;)Z
    .locals 11
    .param p1, "label"    # Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 351
    const/4 v8, 0x3

    const-string v9, "Updating label: %s."

    new-array v10, v6, [Ljava/lang/Object;

    aput-object p1, v10, v7

    invoke-static {p0, v8, v9, v10}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 353
    if-nez p1, :cond_1

    .line 377
    :cond_0
    :goto_0
    return v7

    .line 357
    :cond_1
    invoke-direct {p0}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->checkClient()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 361
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/labeling/Label;->getId()J

    move-result-wide v2

    .line 363
    .local v2, "labelId":J
    const-wide/16 v8, -0x1

    cmp-long v8, v2, v8

    if-nez v8, :cond_2

    .line 364
    const/4 v6, 0x5

    const-string v8, "Cannot update label with no ID."

    new-array v9, v7, [Ljava/lang/Object;

    invoke-static {p0, v6, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 368
    :cond_2
    iget-object v8, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mLabelsContentUri:Landroid/net/Uri;

    invoke-static {v8, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 369
    .local v4, "uri":Landroid/net/Uri;
    invoke-static {p1}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->buildContentValuesForLabel(Lcom/googlecode/eyesfree/labeling/Label;)Landroid/content/ContentValues;

    move-result-object v5

    .line 372
    .local v5, "values":Landroid/content/ContentValues;
    :try_start_0
    iget-object v8, p0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->mClient:Landroid/content/ContentProviderClient;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v4, v5, v9, v10}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 374
    .local v1, "rowsAffected":I
    if-lez v1, :cond_3

    :goto_1
    move v7, v6

    goto :goto_0

    :cond_3
    move v6, v7

    goto :goto_1

    .line 375
    .end local v1    # "rowsAffected":I
    :catch_0
    move-exception v0

    .line 376
    .local v0, "e":Landroid/os/RemoteException;
    const/4 v6, 0x6

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v8

    new-array v9, v7, [Ljava/lang/Object;

    invoke-static {p0, v6, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
