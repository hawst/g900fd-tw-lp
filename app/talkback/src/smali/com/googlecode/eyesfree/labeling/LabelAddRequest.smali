.class public Lcom/googlecode/eyesfree/labeling/LabelAddRequest;
.super Ljava/lang/Object;
.source "LabelAddRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/labeling/LabelAddRequest$OnLabelAddedListener;
    }
.end annotation


# instance fields
.field private final mLabel:Lcom/googlecode/eyesfree/labeling/Label;

.field private final mListener:Lcom/googlecode/eyesfree/labeling/LabelAddRequest$OnLabelAddedListener;


# direct methods
.method public constructor <init>(Lcom/googlecode/eyesfree/labeling/Label;Lcom/googlecode/eyesfree/labeling/LabelAddRequest$OnLabelAddedListener;)V
    .locals 0
    .param p1, "label"    # Lcom/googlecode/eyesfree/labeling/Label;
    .param p2, "listener"    # Lcom/googlecode/eyesfree/labeling/LabelAddRequest$OnLabelAddedListener;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/LabelAddRequest;->mLabel:Lcom/googlecode/eyesfree/labeling/Label;

    .line 26
    iput-object p2, p0, Lcom/googlecode/eyesfree/labeling/LabelAddRequest;->mListener:Lcom/googlecode/eyesfree/labeling/LabelAddRequest$OnLabelAddedListener;

    .line 27
    return-void
.end method


# virtual methods
.method public getLabel()Lcom/googlecode/eyesfree/labeling/Label;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/LabelAddRequest;->mLabel:Lcom/googlecode/eyesfree/labeling/Label;

    return-object v0
.end method

.method protected invokeCallback(Lcom/googlecode/eyesfree/labeling/Label;)V
    .locals 1
    .param p1, "result"    # Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/LabelAddRequest;->mListener:Lcom/googlecode/eyesfree/labeling/LabelAddRequest$OnLabelAddedListener;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/LabelAddRequest;->mListener:Lcom/googlecode/eyesfree/labeling/LabelAddRequest$OnLabelAddedListener;

    invoke-interface {v0, p1}, Lcom/googlecode/eyesfree/labeling/LabelAddRequest$OnLabelAddedListener;->onLabelAdded(Lcom/googlecode/eyesfree/labeling/Label;)V

    .line 37
    :cond_0
    return-void
.end method
