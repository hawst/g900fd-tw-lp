.class public Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
.super Ljava/lang/Object;
.source "CustomLabelManager.java"

# interfaces
.implements Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DataConsistencyCheckTask;,
        Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelRemoveTask;,
        Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelUpdateTask;,
        Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelAddTask;,
        Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;,
        Lcom/googlecode/eyesfree/labeling/CustomLabelManager$AllLabelsFetchTask;,
        Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DirectLabelFetchTask;,
        Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;,
        Lcom/googlecode/eyesfree/labeling/CustomLabelManager$CacheRefreshReceiver;
    }
.end annotation


# static fields
.field private static final REFRESH_INTENT_FILTER:Landroid/content/IntentFilter;

.field private static final RESOURCE_NAME_SPLIT_PATTERN:Ljava/util/regex/Pattern;


# instance fields
.field private final mClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

.field private final mContext:Landroid/content/Context;

.field private final mLabelCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;>;"
        }
    .end annotation
.end field

.field private mLastLocale:Ljava/util/Locale;

.field private final mLock:Ljava/lang/Object;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mRefreshReceiver:Lcom/googlecode/eyesfree/labeling/CustomLabelManager$CacheRefreshReceiver;

.field private mRunningTasks:I

.field private mShouldShutdownClient:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 91
    const-string v0, ":id/"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->RESOURCE_NAME_SPLIT_PATTERN:Ljava/util/regex/Pattern;

    .line 93
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.marvin.talkback.labeling.REFRESH_LABEL_CACHE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->REFRESH_INTENT_FILTER:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    new-instance v0, Lcom/googlecode/eyesfree/labeling/LruCache;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lcom/googlecode/eyesfree/labeling/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mLabelCache:Ljava/util/Map;

    .line 99
    new-instance v0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$CacheRefreshReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$CacheRefreshReceiver;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mRefreshReceiver:Lcom/googlecode/eyesfree/labeling/CustomLabelManager$CacheRefreshReceiver;

    .line 113
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mContext:Landroid/content/Context;

    .line 114
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 115
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mLastLocale:Ljava/util/Locale;

    .line 116
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mLock:Ljava/lang/Object;

    .line 117
    iput-boolean v2, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mShouldShutdownClient:Z

    .line 118
    iput v2, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mRunningTasks:I

    .line 119
    new-instance v0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    const-string v1, "com.google.android.marvin.talkback.providers.LabelProvider"

    invoke-direct {v0, p1, v1}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    .line 120
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mRefreshReceiver:Lcom/googlecode/eyesfree/labeling/CustomLabelManager$CacheRefreshReceiver;

    sget-object v2, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->REFRESH_INTENT_FILTER:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 121
    return-void
.end method

.method static synthetic access$1000(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    .param p1, "x1"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->taskEnding(Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)Lcom/googlecode/eyesfree/labeling/LabelProviderClient;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)Landroid/content/pm/PackageManager;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mPackageManager:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;[Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->sendCacheRefreshIntent([Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1600(Landroid/content/pm/PackageInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Landroid/content/pm/PackageInfo;

    .prologue
    .line 77
    invoke-static {p0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->computePackageSignatureHash(Landroid/content/pm/PackageInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mLabelCache:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$900(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    .param p1, "x1"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->taskStarting(Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;)V

    return-void
.end method

.method private static computePackageSignatureHash(Landroid/content/pm/PackageInfo;)Ljava/lang/String;
    .locals 12
    .param p0, "packageInfo"    # Landroid/content/pm/PackageInfo;

    .prologue
    .line 608
    const-string v6, ""

    .line 610
    .local v6, "signatureHash":Ljava/lang/String;
    iget-object v7, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 612
    .local v7, "sigs":[Landroid/content/pm/Signature;
    :try_start_0
    const-string v8, "SHA-1"

    invoke-static {v8}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v4

    .line 613
    .local v4, "messageDigest":Ljava/security/MessageDigest;
    move-object v0, v7

    .local v0, "arr$":[Landroid/content/pm/Signature;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v5, v0, v2

    .line 614
    .local v5, "s":Landroid/content/pm/Signature;
    invoke-virtual {v5}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/security/MessageDigest;->update([B)V

    .line 613
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 617
    .end local v5    # "s":Landroid/content/pm/Signature;
    :cond_0
    invoke-virtual {v4}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v8

    invoke-static {v8}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->bytesToHexString([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 623
    .end local v0    # "arr$":[Landroid/content/pm/Signature;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "messageDigest":Ljava/security/MessageDigest;
    :goto_1
    return-object v6

    .line 618
    :catch_0
    move-exception v1

    .line 619
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    const-class v8, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    const/4 v9, 0x5

    const-string v10, "Unable to create SHA-1 MessageDigest"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v8, v9, v10, v11}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private maybeShutdownClient()V
    .locals 4

    .prologue
    .line 499
    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 500
    :try_start_0
    iget v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mRunningTasks:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mShouldShutdownClient:Z

    if-eqz v0, :cond_0

    .line 501
    const/4 v0, 0x2

    const-string v2, "All tasks completed and shutdown requested.  Releasing database."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 503
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->shutdown()V

    .line 505
    :cond_0
    monitor-exit v1

    .line 506
    return-void

    .line 505
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private prefetchLabelsForPackage(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 585
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 605
    :cond_0
    :goto_0
    return-void

    .line 593
    :cond_1
    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mLabelCache:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 594
    new-instance v0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;

    invoke-direct {v0, p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Ljava/lang/String;)V

    .line 603
    .local v0, "callback":Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;
    invoke-virtual {p0, p1, v0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->getLabelsForPackageFromDatabase(Ljava/lang/String;Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;)V

    goto :goto_0
.end method

.method private prefetchLabelsFromEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 12
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 538
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v10

    const/16 v11, 0x800

    if-ne v10, v11, :cond_4

    .line 542
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getSource()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v9

    .line 543
    .local v9, "source":Landroid/view/accessibility/AccessibilityNodeInfo;
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 544
    .local v6, "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/CharSequence;>;"
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    .line 546
    .local v8, "seenNodes":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    invoke-virtual {v8, v9}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 549
    :cond_0
    :goto_0
    invoke-virtual {v8}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_3

    .line 550
    invoke-virtual {v8}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .line 551
    .local v1, "currentNode":Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v1, :cond_0

    .line 555
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getViewIdResourceName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->splitResourceName(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v7

    .line 557
    .local v7, "resId":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v7, :cond_1

    .line 558
    iget-object v10, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v6, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 561
    :cond_1
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getChildCount()I

    move-result v0

    .line 562
    .local v0, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_2

    .line 563
    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getChild(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 562
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 566
    :cond_2
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->recycle()V

    goto :goto_0

    .line 569
    .end local v0    # "childCount":I
    .end local v1    # "currentNode":Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v2    # "i":I
    .end local v7    # "resId":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_3
    invoke-virtual {v6}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    .line 570
    .local v5, "packageName":Ljava/lang/CharSequence;
    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->prefetchLabelsForPackage(Ljava/lang/String;)V

    goto :goto_2

    .line 574
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "packageName":Ljava/lang/CharSequence;
    .end local v6    # "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/CharSequence;>;"
    .end local v8    # "seenNodes":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    .end local v9    # "source":Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_4
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getSource()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v4

    .line 575
    .local v4, "node":Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v4, :cond_5

    .line 576
    invoke-virtual {v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->getViewIdResourceName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->splitResourceName(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v7

    .line 577
    .restart local v7    # "resId":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v7, :cond_5

    .line 578
    iget-object v10, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Ljava/lang/String;

    invoke-direct {p0, v10}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->prefetchLabelsForPackage(Ljava/lang/String;)V

    .line 582
    .end local v4    # "node":Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v7    # "resId":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_5
    return-void
.end method

.method private refreshCacheInternal(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 442
    .local p1, "packageNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    if-nez v3, :cond_3

    .line 447
    :cond_0
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mLabelCache:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    .line 448
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mLabelCache:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 458
    :cond_1
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 459
    .local v2, "packageName":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 460
    invoke-direct {p0, v2}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->prefetchLabelsForPackage(Ljava/lang/String;)V

    goto :goto_0

    .line 452
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_3
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 453
    .local v1, "p":Ljava/lang/String;
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mLabelCache:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 463
    .end local v1    # "p":Ljava/lang/String;
    :cond_4
    return-void
.end method

.method private varargs sendCacheRefreshIntent([Ljava/lang/String;)V
    .locals 2
    .param p1, "packageNames"    # [Ljava/lang/String;

    .prologue
    .line 627
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.marvin.talkback.labeling.REFRESH_LABEL_CACHE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 629
    .local v0, "refreshIntent":Landroid/content/Intent;
    const-string v1, "EXTRA_STRING_ARRAY_PACKAGES"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 630
    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 631
    return-void
.end method

.method public static splitResourceName(Ljava/lang/String;)Landroid/util/Pair;
    .locals 7
    .param p0, "resourceName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 426
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 438
    :goto_0
    return-object v1

    .line 430
    :cond_0
    sget-object v2, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->RESOURCE_NAME_SPLIT_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p0, v3}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;I)[Ljava/lang/String;

    move-result-object v0

    .line 431
    .local v0, "splitId":[Ljava/lang/String;
    array-length v2, v0

    if-ne v2, v3, :cond_1

    aget-object v2, v0, v6

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    aget-object v2, v0, v5

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 433
    :cond_1
    const-class v2, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    const/4 v3, 0x5

    const-string v4, "Failed to parse resource: %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v6

    invoke-static {v2, v3, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 438
    :cond_2
    new-instance v1, Landroid/util/Pair;

    aget-object v2, v0, v6

    aget-object v3, v0, v5

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private taskEnding(Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask",
            "<***>;)V"
        }
    .end annotation

    .prologue
    .line 529
    .local p1, "task":Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;, "Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask<***>;"
    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 530
    const/4 v0, 0x2

    :try_start_0
    const-string v2, "Task %s ending."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {p0, v0, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 531
    iget v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mRunningTasks:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mRunningTasks:I

    .line 532
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 534
    invoke-direct {p0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->maybeShutdownClient()V

    .line 535
    return-void

    .line 532
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private taskStarting(Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask",
            "<***>;)V"
        }
    .end annotation

    .prologue
    .line 515
    .local p1, "task":Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;, "Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask<***>;"
    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 516
    const/4 v0, 0x2

    :try_start_0
    const-string v2, "Task %s starting."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {p0, v0, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 517
    iget v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mRunningTasks:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mRunningTasks:I

    .line 518
    monitor-exit v1

    .line 519
    return-void

    .line 518
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public addLabel(Ljava/lang/String;Ljava/lang/String;)V
    .locals 24
    .param p1, "resourceName"    # Ljava/lang/String;
    .param p2, "userLabel"    # Ljava/lang/String;

    .prologue
    .line 269
    invoke-virtual/range {p0 .. p0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_0

    .line 326
    :goto_0
    return-void

    .line 274
    :cond_0
    if-nez p2, :cond_1

    .line 275
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v5, "Attempted to add a label with a null userLabel value"

    invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 278
    :cond_1
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 279
    .local v6, "finalLabel":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 280
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v5, "Attempted to add a label with an empty userLabel value"

    invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 285
    :cond_2
    invoke-static/range {p1 .. p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->splitResourceName(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v18

    .line 286
    .local v18, "parsedId":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v18, :cond_3

    .line 287
    const/4 v3, 0x5

    const-string v5, "Attempted to add a label with an invalid or poorly formed view ID."

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v3, v5, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 294
    :cond_3
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mPackageManager:Landroid/content/pm/PackageManager;

    move-object/from16 v0, v18

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    const/16 v9, 0x40

    invoke-virtual {v5, v3, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v17

    .line 301
    .local v17, "packageInfo":Landroid/content/pm/PackageInfo;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v7

    .line 302
    .local v7, "locale":Ljava/lang/String;
    move-object/from16 v0, v17

    iget v8, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 303
    .local v8, "version":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 304
    .local v10, "timestamp":J
    const-string v4, ""

    .line 306
    .local v4, "signatureHash":Ljava/lang/String;
    move-object/from16 v0, v17

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    move-object/from16 v22, v0

    .line 308
    .local v22, "sigs":[Landroid/content/pm/Signature;
    :try_start_1
    const-string v3, "SHA-1"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v16

    .line 309
    .local v16, "messageDigest":Ljava/security/MessageDigest;
    move-object/from16 v12, v22

    .local v12, "arr$":[Landroid/content/pm/Signature;
    array-length v15, v12

    .local v15, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_1
    if-ge v14, v15, :cond_4

    aget-object v20, v12, v14

    .line 310
    .local v20, "s":Landroid/content/pm/Signature;
    invoke-virtual/range {v20 .. v20}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/security/MessageDigest;->update([B)V
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    .line 309
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 296
    .end local v4    # "signatureHash":Ljava/lang/String;
    .end local v7    # "locale":Ljava/lang/String;
    .end local v8    # "version":I
    .end local v10    # "timestamp":J
    .end local v12    # "arr$":[Landroid/content/pm/Signature;
    .end local v14    # "i$":I
    .end local v15    # "len$":I
    .end local v16    # "messageDigest":Ljava/security/MessageDigest;
    .end local v17    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v20    # "s":Landroid/content/pm/Signature;
    .end local v22    # "sigs":[Landroid/content/pm/Signature;
    :catch_0
    move-exception v13

    .line 297
    .local v13, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v3, 0x5

    const-string v5, "Attempted to add a label for an unknown package."

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v3, v5, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 313
    .end local v13    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v4    # "signatureHash":Ljava/lang/String;
    .restart local v7    # "locale":Ljava/lang/String;
    .restart local v8    # "version":I
    .restart local v10    # "timestamp":J
    .restart local v12    # "arr$":[Landroid/content/pm/Signature;
    .restart local v14    # "i$":I
    .restart local v15    # "len$":I
    .restart local v16    # "messageDigest":Ljava/security/MessageDigest;
    .restart local v17    # "packageInfo":Landroid/content/pm/PackageInfo;
    .restart local v22    # "sigs":[Landroid/content/pm/Signature;
    :cond_4
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v3

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->bytesToHexString([B)Ljava/lang/String;
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v4

    .line 319
    .end local v12    # "arr$":[Landroid/content/pm/Signature;
    .end local v14    # "i$":I
    .end local v15    # "len$":I
    .end local v16    # "messageDigest":Ljava/security/MessageDigest;
    :goto_2
    const-string v21, ""

    .line 321
    .local v21, "screenshotPath":Ljava/lang/String;
    new-instance v2, Lcom/googlecode/eyesfree/labeling/Label;

    move-object/from16 v0, v18

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v5, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    const-string v9, ""

    invoke-direct/range {v2 .. v11}, Lcom/googlecode/eyesfree/labeling/Label;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;J)V

    .line 323
    .local v2, "label":Lcom/googlecode/eyesfree/labeling/Label;
    new-instance v19, Lcom/googlecode/eyesfree/labeling/LabelAddRequest;

    const/4 v3, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v2, v3}, Lcom/googlecode/eyesfree/labeling/LabelAddRequest;-><init>(Lcom/googlecode/eyesfree/labeling/Label;Lcom/googlecode/eyesfree/labeling/LabelAddRequest$OnLabelAddedListener;)V

    .line 324
    .local v19, "request":Lcom/googlecode/eyesfree/labeling/LabelAddRequest;
    new-instance v23, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelAddTask;

    const/4 v3, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelAddTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V

    .line 325
    .local v23, "task":Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelAddTask;
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/googlecode/eyesfree/labeling/LabelAddRequest;

    const/4 v5, 0x0

    aput-object v19, v3, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelAddTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 314
    .end local v2    # "label":Lcom/googlecode/eyesfree/labeling/Label;
    .end local v19    # "request":Lcom/googlecode/eyesfree/labeling/LabelAddRequest;
    .end local v21    # "screenshotPath":Ljava/lang/String;
    .end local v23    # "task":Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelAddTask;
    :catch_1
    move-exception v13

    .line 315
    .local v13, "e":Ljava/security/NoSuchAlgorithmException;
    const/4 v3, 0x5

    const-string v5, "Unable to create SHA-1 MessageDigest"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v3, v5, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public ensureDataConsistency()V
    .locals 2

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    :goto_0
    return-void

    .line 157
    :cond_0
    new-instance v0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DataConsistencyCheckTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DataConsistencyCheckTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DataConsistencyCheckTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public getAllLabelsFromDatabase(Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest$OnAllLabelsFetchedListener;)V
    .locals 4
    .param p1, "callback"    # Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest$OnAllLabelsFetchedListener;

    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    .line 238
    :goto_0
    return-void

    .line 235
    :cond_0
    new-instance v0, Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;

    invoke-direct {v0, p1}, Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;-><init>(Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest$OnAllLabelsFetchedListener;)V

    .line 236
    .local v0, "request":Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;
    new-instance v1, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$AllLabelsFetchTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$AllLabelsFetchTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V

    .line 237
    .local v1, "task":Lcom/googlecode/eyesfree/labeling/CustomLabelManager$AllLabelsFetchTask;
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$AllLabelsFetchTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public getLabelForLabelIdFromDatabase(Ljava/lang/Long;Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest$OnLabelFetchedListener;)V
    .locals 4
    .param p1, "labelId"    # Ljava/lang/Long;
    .param p2, "callback"    # Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest$OnLabelFetchedListener;

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    .line 207
    :goto_0
    return-void

    .line 204
    :cond_0
    new-instance v0, Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3, p2}, Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;-><init>(JLcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest$OnLabelFetchedListener;)V

    .line 205
    .local v0, "request":Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;
    new-instance v1, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DirectLabelFetchTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DirectLabelFetchTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V

    .line 206
    .local v1, "task":Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DirectLabelFetchTask;
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$DirectLabelFetchTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public getLabelForViewIdFromCache(Ljava/lang/String;)Lcom/googlecode/eyesfree/labeling/Label;
    .locals 5
    .param p1, "resourceName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 172
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-object v2

    .line 176
    :cond_1
    invoke-static {p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->splitResourceName(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 177
    .local v1, "parsedId":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    .line 181
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mLabelCache:Ljava/util/Map;

    iget-object v4, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 186
    iget-object v2, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mLabelCache:Ljava/util/Map;

    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 187
    .local v0, "packageLabels":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/googlecode/eyesfree/labeling/Label;>;"
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/googlecode/eyesfree/labeling/Label;

    goto :goto_0
.end method

.method public getLabelsForPackageFromDatabase(Ljava/lang/String;Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    .line 228
    :goto_0
    return-void

    .line 224
    :cond_0
    new-instance v0, Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;

    invoke-direct {v0, p1, p2}, Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;-><init>(Ljava/lang/String;Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;)V

    .line 226
    .local v0, "request":Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;
    new-instance v1, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V

    .line 227
    .local v1, "task":Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 125
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 126
    .local v0, "currentLocale":Ljava/util/Locale;
    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mLastLocale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 128
    iput-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mLastLocale:Ljava/util/Locale;

    .line 129
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->refreshCacheInternal(Ljava/util/Set;)V

    .line 132
    :cond_0
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 138
    :goto_0
    return-void

    .line 136
    :sswitch_0
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->prefetchLabelsFromEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0

    .line 132
    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0x800 -> :sswitch_0
        0x8000 -> :sswitch_0
    .end sparse-switch
.end method

.method public varargs refreshCache([Ljava/lang/String;)V
    .locals 6
    .param p1, "packageNames"    # [Ljava/lang/String;

    .prologue
    .line 403
    const/4 v4, 0x0

    .line 404
    .local v4, "packageSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    array-length v5, p1

    if-lez v5, :cond_0

    .line 406
    new-instance v4, Ljava/util/HashSet;

    .end local v4    # "packageSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 407
    .restart local v4    # "packageSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 408
    .local v3, "p":Ljava/lang/String;
    invoke-virtual {v4, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 407
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 412
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "p":Ljava/lang/String;
    :cond_0
    invoke-direct {p0, v4}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->refreshCacheInternal(Ljava/util/Set;)V

    .line 413
    return-void
.end method

.method public varargs removeLabel([Lcom/googlecode/eyesfree/labeling/Label;)V
    .locals 9
    .param p1, "labels"    # [Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 379
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->isInitialized()Z

    move-result v6

    if-nez v6, :cond_1

    .line 393
    :cond_0
    :goto_0
    return-void

    .line 383
    :cond_1
    if-eqz p1, :cond_2

    array-length v6, p1

    if-nez v6, :cond_3

    .line 384
    :cond_2
    const/4 v6, 0x5

    const-string v7, "Attempted to delete a null or empty array of labels."

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {p0, v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 388
    :cond_3
    move-object v0, p1

    .local v0, "arr$":[Lcom/googlecode/eyesfree/labeling/Label;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 389
    .local v2, "l":Lcom/googlecode/eyesfree/labeling/Label;
    new-instance v4, Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest;

    invoke-direct {v4, v2, v7}, Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest;-><init>(Lcom/googlecode/eyesfree/labeling/Label;Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest$OnLabelRemovedListener;)V

    .line 390
    .local v4, "request":Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest;
    new-instance v5, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelRemoveTask;

    invoke-direct {v5, p0, v7}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelRemoveTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V

    .line 391
    .local v5, "task":Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelRemoveTask;
    const/4 v6, 0x1

    new-array v6, v6, [Lcom/googlecode/eyesfree/labeling/LabelRemoveRequest;

    aput-object v4, v6, v8

    invoke-virtual {v5, v6}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelRemoveTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 388
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public shutdown()V
    .locals 3

    .prologue
    .line 469
    const/4 v0, 0x2

    const-string v1, "Shutdown requested."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 473
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mRefreshReceiver:Lcom/googlecode/eyesfree/labeling/CustomLabelManager$CacheRefreshReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 478
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mShouldShutdownClient:Z

    .line 479
    invoke-direct {p0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->maybeShutdownClient()V

    .line 480
    return-void
.end method

.method public varargs updateLabel([Lcom/googlecode/eyesfree/labeling/Label;)V
    .locals 9
    .param p1, "labels"    # [Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 341
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->isInitialized()Z

    move-result v6

    if-nez v6, :cond_1

    .line 364
    :cond_0
    :goto_0
    return-void

    .line 345
    :cond_1
    if-eqz p1, :cond_2

    array-length v6, p1

    if-nez v6, :cond_3

    .line 346
    :cond_2
    const/4 v6, 0x5

    const-string v7, "Attempted to update a null or empty array of labels."

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {p0, v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 350
    :cond_3
    move-object v0, p1

    .local v0, "arr$":[Lcom/googlecode/eyesfree/labeling/Label;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 351
    .local v2, "l":Lcom/googlecode/eyesfree/labeling/Label;
    if-nez v2, :cond_4

    .line 352
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Attempted to update a null label."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 355
    :cond_4
    invoke-virtual {v2}, Lcom/googlecode/eyesfree/labeling/Label;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 356
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Attempted to update a label with an empty text value"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 360
    :cond_5
    new-instance v4, Lcom/googlecode/eyesfree/labeling/LabelUpdateRequest;

    invoke-direct {v4, v2, v7}, Lcom/googlecode/eyesfree/labeling/LabelUpdateRequest;-><init>(Lcom/googlecode/eyesfree/labeling/Label;Lcom/googlecode/eyesfree/labeling/LabelUpdateRequest$OnLabelUpdatedListener;)V

    .line 361
    .local v4, "request":Lcom/googlecode/eyesfree/labeling/LabelUpdateRequest;
    new-instance v5, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelUpdateTask;

    invoke-direct {v5, p0, v7}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelUpdateTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V

    .line 362
    .local v5, "task":Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelUpdateTask;
    const/4 v6, 0x1

    new-array v6, v6, [Lcom/googlecode/eyesfree/labeling/LabelUpdateRequest;

    aput-object v4, v6, v8

    invoke-virtual {v5, v6}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelUpdateTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 350
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
