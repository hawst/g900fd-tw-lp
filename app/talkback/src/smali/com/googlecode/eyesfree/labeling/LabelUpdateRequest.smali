.class public Lcom/googlecode/eyesfree/labeling/LabelUpdateRequest;
.super Ljava/lang/Object;
.source "LabelUpdateRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/labeling/LabelUpdateRequest$OnLabelUpdatedListener;
    }
.end annotation


# instance fields
.field private final mLabel:Lcom/googlecode/eyesfree/labeling/Label;

.field private final mListener:Lcom/googlecode/eyesfree/labeling/LabelUpdateRequest$OnLabelUpdatedListener;


# direct methods
.method public constructor <init>(Lcom/googlecode/eyesfree/labeling/Label;Lcom/googlecode/eyesfree/labeling/LabelUpdateRequest$OnLabelUpdatedListener;)V
    .locals 0
    .param p1, "label"    # Lcom/googlecode/eyesfree/labeling/Label;
    .param p2, "listener"    # Lcom/googlecode/eyesfree/labeling/LabelUpdateRequest$OnLabelUpdatedListener;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/LabelUpdateRequest;->mLabel:Lcom/googlecode/eyesfree/labeling/Label;

    .line 26
    iput-object p2, p0, Lcom/googlecode/eyesfree/labeling/LabelUpdateRequest;->mListener:Lcom/googlecode/eyesfree/labeling/LabelUpdateRequest$OnLabelUpdatedListener;

    .line 27
    return-void
.end method


# virtual methods
.method public getLabel()Lcom/googlecode/eyesfree/labeling/Label;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/LabelUpdateRequest;->mLabel:Lcom/googlecode/eyesfree/labeling/Label;

    return-object v0
.end method
