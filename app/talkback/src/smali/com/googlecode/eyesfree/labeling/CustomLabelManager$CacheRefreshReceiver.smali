.class Lcom/googlecode/eyesfree/labeling/CustomLabelManager$CacheRefreshReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CustomLabelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CacheRefreshReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;


# direct methods
.method private constructor <init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V
    .locals 0

    .prologue
    .line 633
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$CacheRefreshReceiver;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    .param p2, "x1"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;

    .prologue
    .line 633
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$CacheRefreshReceiver;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 637
    const/4 v0, 0x0

    .line 638
    .local v0, "packages":[Ljava/lang/String;
    const-string v1, "EXTRA_STRING_ARRAY_PACKAGES"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 639
    const-string v1, "EXTRA_STRING_ARRAY_PACKAGES"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 643
    :cond_0
    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$CacheRefreshReceiver;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    invoke-virtual {v1, v0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->refreshCache([Ljava/lang/String;)V

    .line 644
    return-void
.end method
