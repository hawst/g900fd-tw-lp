.class Lcom/googlecode/eyesfree/labeling/CustomLabelManager$AllLabelsFetchTask;
.super Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;
.source "CustomLabelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AllLabelsFetchTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask",
        "<",
        "Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/googlecode/eyesfree/labeling/Label;",
        ">;>;"
    }
.end annotation


# instance fields
.field private mRequest:Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;

.field final synthetic this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;


# direct methods
.method private constructor <init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V
    .locals 1

    .prologue
    .line 710
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$AllLabelsFetchTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    .param p2, "x1"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;

    .prologue
    .line 710
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$AllLabelsFetchTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 710
    check-cast p1, [Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$AllLabelsFetchTask;->doInBackground([Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;)Ljava/util/List;
    .locals 6
    .param p1, "requests"    # [Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 717
    if-eqz p1, :cond_0

    array-length v0, p1

    if-eq v0, v4, :cond_1

    .line 718
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Fetch all labels task supports only single requests."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 722
    :cond_1
    aget-object v0, p1, v3

    iput-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$AllLabelsFetchTask;->mRequest:Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;

    .line 724
    const-string v0, "Spawning new AllLabelsFetchTask(%d) for %s"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$AllLabelsFetchTask;->mRequest:Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;

    aput-object v2, v1, v4

    invoke-static {p0, v5, v0, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 727
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$AllLabelsFetchTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    # getter for: Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;
    invoke-static {v0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->access$1200(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->getAllLabels()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 710
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$AllLabelsFetchTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 732
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/labeling/Label;>;"
    const/4 v0, 0x2

    const-string v1, "AllLabelsFetchTask(%d) complete"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 733
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$AllLabelsFetchTask;->mRequest:Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;->invokeCallback(Ljava/util/List;)V

    .line 734
    invoke-super {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 735
    return-void
.end method
