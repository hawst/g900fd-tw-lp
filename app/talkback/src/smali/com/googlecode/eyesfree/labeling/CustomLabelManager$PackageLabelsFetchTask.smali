.class Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;
.super Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;
.source "CustomLabelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PackageLabelsFetchTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask",
        "<",
        "Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;",
        "Ljava/lang/Void;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Lcom/googlecode/eyesfree/labeling/Label;",
        ">;>;"
    }
.end annotation


# instance fields
.field private mRequest:Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;

.field final synthetic this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;


# direct methods
.method private constructor <init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V
    .locals 1

    .prologue
    .line 738
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    .param p2, "x1"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;

    .prologue
    .line 738
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 738
    check-cast p1, [Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;->doInBackground([Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;)Ljava/util/Map;
    .locals 9
    .param p1, "requests"    # [Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 745
    if-eqz p1, :cond_0

    array-length v3, p1

    if-eq v3, v6, :cond_1

    .line 746
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Fetch labels for package task supports only single package lookups."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 750
    :cond_1
    aget-object v3, p1, v7

    iput-object v3, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;->mRequest:Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;

    .line 752
    const-string v3, "Spawning new PackageLabelsFetchTask(%d) for %s"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    iget-object v5, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;->mRequest:Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;

    aput-object v5, v4, v6

    invoke-static {p0, v8, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 755
    const v2, 0x7fffffff

    .line 757
    .local v2, "versionCode":I
    :try_start_0
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    # getter for: Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mPackageManager:Landroid/content/pm/PackageManager;
    invoke-static {v3}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->access$1300(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;->mRequest:Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;

    invoke-virtual {v4}, Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 759
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 766
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    # getter for: Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;
    invoke-static {v3}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->access$1200(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    move-result-object v3

    iget-object v4, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;->mRequest:Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;

    invoke-virtual {v4}, Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v2}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->getLabelsForPackage(Ljava/lang/String;Ljava/lang/String;I)Ljava/util/Map;

    move-result-object v3

    return-object v3

    .line 760
    :catch_0
    move-exception v0

    .line 761
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v3, 0x5

    const-string v4, "Unable to resolve package info during prefetch for %s"

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;->mRequest:Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;

    invoke-virtual {v6}, Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;->getPackageName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {p0, v3, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 738
    check-cast p1, Ljava/util/Map;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;->onPostExecute(Ljava/util/Map;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 772
    .local p1, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/googlecode/eyesfree/labeling/Label;>;"
    const/4 v0, 0x2

    const-string v1, "LabelPrefetchTask(%d) complete"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 773
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$PackageLabelsFetchTask;->mRequest:Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;->invokeCallback(Ljava/util/Map;)V

    .line 774
    invoke-super {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 775
    return-void
.end method
