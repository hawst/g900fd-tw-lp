.class public Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;
.super Ljava/lang/Object;
.source "PackageLabelsFetchRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;
    }
.end annotation


# instance fields
.field private final mListener:Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;

.field private final mPackageName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "onLabelsFetchedListener"    # Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;->mPackageName:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;->mListener:Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;

    .line 30
    return-void
.end method


# virtual methods
.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method protected invokeCallback(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/googlecode/eyesfree/labeling/Label;>;"
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;->mListener:Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;->mListener:Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;

    invoke-interface {v0, p1}, Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;->onLabelsFetched(Ljava/util/Map;)V

    .line 40
    :cond_0
    return-void
.end method
