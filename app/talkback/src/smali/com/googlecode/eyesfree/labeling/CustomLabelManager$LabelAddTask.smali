.class Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelAddTask;
.super Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;
.source "CustomLabelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LabelAddTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask",
        "<",
        "Lcom/googlecode/eyesfree/labeling/LabelAddRequest;",
        "Ljava/lang/Void;",
        "Lcom/googlecode/eyesfree/labeling/Label;",
        ">;"
    }
.end annotation


# instance fields
.field private mRequest:Lcom/googlecode/eyesfree/labeling/LabelAddRequest;

.field final synthetic this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;


# direct methods
.method private constructor <init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V
    .locals 1

    .prologue
    .line 778
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelAddTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    .param p2, "x1"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager$1;

    .prologue
    .line 778
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelAddTask;-><init>(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/googlecode/eyesfree/labeling/LabelAddRequest;)Lcom/googlecode/eyesfree/labeling/Label;
    .locals 6
    .param p1, "requests"    # [Lcom/googlecode/eyesfree/labeling/LabelAddRequest;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 784
    if-eqz p1, :cond_0

    array-length v0, p1

    if-eq v0, v4, :cond_1

    .line 785
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Add task supports only single Label additions."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 789
    :cond_1
    aget-object v0, p1, v3

    iput-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelAddTask;->mRequest:Lcom/googlecode/eyesfree/labeling/LabelAddRequest;

    .line 790
    const-string v0, "Spawning new LabelAddTask(%d) for %s"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelAddTask;->mRequest:Lcom/googlecode/eyesfree/labeling/LabelAddRequest;

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/labeling/LabelAddRequest;->getLabel()Lcom/googlecode/eyesfree/labeling/Label;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p0, v5, v0, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 793
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelAddTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    # getter for: Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->mClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;
    invoke-static {v0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->access$1200(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    move-result-object v0

    iget-object v1, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelAddTask;->mRequest:Lcom/googlecode/eyesfree/labeling/LabelAddRequest;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/labeling/LabelAddRequest;->getLabel()Lcom/googlecode/eyesfree/labeling/Label;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->insertLabel(Lcom/googlecode/eyesfree/labeling/Label;)Lcom/googlecode/eyesfree/labeling/Label;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 778
    check-cast p1, [Lcom/googlecode/eyesfree/labeling/LabelAddRequest;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelAddTask;->doInBackground([Lcom/googlecode/eyesfree/labeling/LabelAddRequest;)Lcom/googlecode/eyesfree/labeling/Label;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/googlecode/eyesfree/labeling/Label;)V
    .locals 6
    .param p1, "result"    # Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 798
    const-string v0, "LabelAddTask(%d) complete, stored as %s"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object p1, v1, v4

    invoke-static {p0, v5, v0, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 800
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelAddTask;->mRequest:Lcom/googlecode/eyesfree/labeling/LabelAddRequest;

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/labeling/LabelAddRequest;->invokeCallback(Lcom/googlecode/eyesfree/labeling/Label;)V

    .line 802
    if-eqz p1, :cond_0

    .line 803
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelAddTask;->this$0:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    new-array v1, v4, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/labeling/Label;->getPackageName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    # invokes: Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->sendCacheRefreshIntent([Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->access$1400(Lcom/googlecode/eyesfree/labeling/CustomLabelManager;[Ljava/lang/String;)V

    .line 806
    :cond_0
    invoke-super {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$TrackedAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 807
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 778
    check-cast p1, Lcom/googlecode/eyesfree/labeling/Label;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager$LabelAddTask;->onPostExecute(Lcom/googlecode/eyesfree/labeling/Label;)V

    return-void
.end method
