.class public Lcom/googlecode/eyesfree/labeling/LabelsTable;
.super Ljava/lang/Object;
.source "LabelsTable.java"


# static fields
.field public static final ALL_COLUMNS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 52
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "packageName"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "packageSignature"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "viewName"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "text"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "locale"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "packageVersion"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "screenshotPath"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    sput-object v0, Lcom/googlecode/eyesfree/labeling/LabelsTable;->ALL_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8
    .param p0, "database"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x3

    .line 57
    const-class v0, Lcom/googlecode/eyesfree/labeling/LabelsTable;

    const/4 v1, 0x4

    const-string v2, "Creating table: %s."

    new-array v3, v7, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "labels"

    aput-object v5, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 59
    new-instance v0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;

    const-string v1, "labels"

    invoke-direct {v0, p0, v1}, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1, v7, v7}, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->addColumn(Ljava/lang/String;IZ)Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;

    move-result-object v0

    const-string v1, "packageName"

    invoke-virtual {v0, v1, v6}, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->addColumn(Ljava/lang/String;I)Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;

    move-result-object v0

    const-string v1, "packageSignature"

    invoke-virtual {v0, v1, v6}, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->addColumn(Ljava/lang/String;I)Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;

    move-result-object v0

    const-string v1, "viewName"

    invoke-virtual {v0, v1, v6}, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->addColumn(Ljava/lang/String;I)Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;

    move-result-object v0

    const-string v1, "text"

    invoke-virtual {v0, v1, v6}, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->addColumn(Ljava/lang/String;I)Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;

    move-result-object v0

    const-string v1, "locale"

    invoke-virtual {v0, v1, v6}, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->addColumn(Ljava/lang/String;I)Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;

    move-result-object v0

    const-string v1, "packageVersion"

    invoke-virtual {v0, v1, v7}, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->addColumn(Ljava/lang/String;I)Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;

    move-result-object v0

    const-string v1, "screenshotPath"

    invoke-virtual {v0, v1, v6}, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->addColumn(Ljava/lang/String;I)Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;

    move-result-object v0

    const-string v1, "timestamp"

    invoke-virtual {v0, v1, v7}, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->addColumn(Ljava/lang/String;I)Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->createTable()V

    .line 70
    return-void
.end method

.method public static onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 8
    .param p0, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "oldVersion"    # I
    .param p2, "newVersion"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 75
    if-ge p1, v7, :cond_0

    .line 76
    const-class v0, Lcom/googlecode/eyesfree/labeling/LabelsTable;

    const/4 v1, 0x4

    const-string v2, "Dropping table %s to upgrade from version %d to version %d."

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "labels"

    aput-object v4, v3, v5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 79
    const-string v0, "DROP TABLE IF EXISTS %s"

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "labels"

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 82
    invoke-static {p0}, Lcom/googlecode/eyesfree/labeling/LabelsTable;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 87
    return-void

    .line 84
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Attempted database upgrade from unsupported database version."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
