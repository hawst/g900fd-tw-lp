.class public interface abstract Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest$OnAllLabelsFetchedListener;
.super Ljava/lang/Object;
.source "AllLabelsFetchRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnAllLabelsFetchedListener"
.end annotation


# virtual methods
.method public abstract onAllLabelsFetched(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;)V"
        }
    .end annotation
.end method
