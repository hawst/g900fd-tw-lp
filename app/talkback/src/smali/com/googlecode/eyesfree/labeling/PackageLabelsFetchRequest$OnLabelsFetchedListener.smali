.class public interface abstract Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest$OnLabelsFetchedListener;
.super Ljava/lang/Object;
.source "PackageLabelsFetchRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/labeling/PackageLabelsFetchRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnLabelsFetchedListener"
.end annotation


# virtual methods
.method public abstract onLabelsFetched(Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;)V"
        }
    .end annotation
.end method
