.class public Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;
.super Ljava/lang/Object;
.source "SQLiteTableBuilder.java"


# instance fields
.field private mCreated:Z

.field private mDatabase:Landroid/database/sqlite/SQLiteDatabase;

.field private mHasColumns:Z

.field private mStringBuilder:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 2
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-boolean v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mHasColumns:Z

    .line 54
    iput-boolean v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mCreated:Z

    .line 64
    if-nez p1, :cond_0

    .line 65
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Database cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Table name cannot be empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_1
    const-string v0, "^[a-zA-Z_][a-zA-Z0-9_]*$"

    invoke-virtual {p2, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 69
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid table name."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_2
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    .line 75
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    const-string v1, "CREATE TABLE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    return-void
.end method

.method private appendType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 132
    packed-switch p1, :pswitch_data_0

    .line 146
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unrecognized data type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134
    :pswitch_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    const-string v1, " INTEGER"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    :goto_0
    return-void

    .line 137
    :pswitch_1
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    const-string v1, " REAL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 140
    :pswitch_2
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    const-string v1, " TEXT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 143
    :pswitch_3
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    const-string v1, " BLOB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 132
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public addColumn(Ljava/lang/String;I)Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;
    .locals 1
    .param p1, "columnName"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 93
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->addColumn(Ljava/lang/String;IZ)Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;

    move-result-object v0

    return-object v0
.end method

.method public addColumn(Ljava/lang/String;IZ)Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "primaryKey"    # Z

    .prologue
    .line 110
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Column name cannot be empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_0
    const-string v0, "^[a-zA-Z_][a-zA-Z0-9_]*$"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 113
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid column name."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :cond_1
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mHasColumns:Z

    if-eqz v0, :cond_2

    .line 117
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    :cond_2
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    invoke-direct {p0, p2}, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->appendType(I)V

    .line 122
    if-eqz p3, :cond_3

    .line 123
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    const-string v1, " PRIMARY KEY"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mHasColumns:Z

    .line 128
    return-object p0
.end method

.method public buildQueryString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 154
    const-string v0, "%s%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, ")"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public createTable()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mCreated:Z

    if-eqz v0, :cond_0

    .line 164
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "createTable was already called on this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->buildQueryString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 168
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/labeling/SQLiteTableBuilder;->mCreated:Z

    .line 169
    return-void
.end method
