.class public Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;
.super Ljava/lang/Object;
.source "AllLabelsFetchRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest$OnAllLabelsFetchedListener;
    }
.end annotation


# instance fields
.field private final mListener:Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest$OnAllLabelsFetchedListener;


# direct methods
.method public constructor <init>(Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest$OnAllLabelsFetchedListener;)V
    .locals 0
    .param p1, "onAllLabelsFetchedListener"    # Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest$OnAllLabelsFetchedListener;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;->mListener:Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest$OnAllLabelsFetchedListener;

    .line 27
    return-void
.end method


# virtual methods
.method protected invokeCallback(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/labeling/Label;>;"
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;->mListener:Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest$OnAllLabelsFetchedListener;

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest;->mListener:Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest$OnAllLabelsFetchedListener;

    invoke-interface {v0, p1}, Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest$OnAllLabelsFetchedListener;->onAllLabelsFetched(Ljava/util/List;)V

    .line 33
    :cond_0
    return-void
.end method
