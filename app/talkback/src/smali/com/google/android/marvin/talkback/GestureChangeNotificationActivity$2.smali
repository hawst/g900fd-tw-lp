.class Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;
.super Ljava/lang/Object;
.source "GestureChangeNotificationActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;->this$0:Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 72
    iget-object v1, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;->this$0:Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    # invokes: Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->clearPreviouslyConfiguredMappings()V
    invoke-static {v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->access$000(Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;)V

    .line 73
    iget-object v1, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;->this$0:Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    # invokes: Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->dismissNotification()V
    invoke-static {v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->access$100(Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;)V

    .line 74
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 75
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;->this$0:Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 78
    .local v0, "shortcutsIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;->this$0:Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->startActivity(Landroid/content/Intent;)V

    .line 79
    iget-object v1, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;->this$0:Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->finish()V

    .line 80
    return-void
.end method
