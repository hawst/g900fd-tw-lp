.class public Lcom/google/android/marvin/talkback/speechrules/RuleSwitch;
.super Lcom/google/android/marvin/talkback/speechrules/RuleDefault;
.source "RuleSwitch.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/speechrules/RuleDefault;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 40
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Landroid/widget/Switch;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Landroid/widget/ToggleButton;

    aput-object v2, v0, v1

    invoke-static {p1, p2, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesAnyClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public format(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 47
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 48
    .local v1, "output":Landroid/text/SpannableStringBuilder;
    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    .line 50
    .local v4, "text":Ljava/lang/CharSequence;
    :goto_0
    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    .line 53
    .local v0, "contentDescription":Ljava/lang/CharSequence;
    new-array v5, v9, [Ljava/lang/CharSequence;

    aput-object v0, v5, v8

    invoke-static {v1, v5}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 56
    const v6, 0x7f0600be

    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    move-object v5, v4

    :goto_1
    aput-object v5, v7, v8

    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 58
    .local v3, "switchDescription":Ljava/lang/CharSequence;
    new-array v5, v9, [Ljava/lang/CharSequence;

    aput-object v3, v5, v8

    invoke-static {v1, v5}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 61
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    const-class v5, Landroid/widget/ToggleButton;

    invoke-static {p1, p2, v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 63
    :cond_0
    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_4

    const v5, 0x7f0600c7

    :goto_2
    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 65
    .local v2, "state":Ljava/lang/CharSequence;
    new-array v5, v9, [Ljava/lang/CharSequence;

    aput-object v2, v5, v8

    invoke-static {v1, v5}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 68
    .end local v2    # "state":Ljava/lang/CharSequence;
    :cond_1
    return-object v1

    .line 48
    .end local v0    # "contentDescription":Ljava/lang/CharSequence;
    .end local v3    # "switchDescription":Ljava/lang/CharSequence;
    .end local v4    # "text":Ljava/lang/CharSequence;
    :cond_2
    invoke-static {p3}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->getEventAggregateText(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v4

    goto :goto_0

    .line 56
    .restart local v0    # "contentDescription":Ljava/lang/CharSequence;
    .restart local v4    # "text":Ljava/lang/CharSequence;
    :cond_3
    const-string v5, ""

    goto :goto_1

    .line 63
    .restart local v3    # "switchDescription":Ljava/lang/CharSequence;
    :cond_4
    const v5, 0x7f0600c8

    goto :goto_2
.end method

.method public bridge synthetic getHintText(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 35
    invoke-super {p0, p1, p2}, Lcom/google/android/marvin/talkback/speechrules/RuleDefault;->getHintText(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
