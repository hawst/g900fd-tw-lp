.class public Lcom/google/android/marvin/talkback/CursorController;
.super Ljava/lang/Object;
.source "CursorController.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/KeyComboManager$KeyComboListener;
.implements Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;,
        Lcom/google/android/marvin/talkback/CursorController$ScrollListener;
    }
.end annotation


# instance fields
.field private final mGranularityListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

.field private final mNavigateSeenNodes:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;"
        }
    .end annotation
.end field

.field private mReachedEdge:Z

.field private final mScrollListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/marvin/talkback/CursorController$ScrollListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 1
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mNavigateSeenNodes:Ljava/util/HashSet;

    .line 71
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityListeners:Ljava/util/Set;

    .line 74
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mScrollListeners:Ljava/util/Set;

    .line 84
    iput-object p1, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 85
    new-instance v0, Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-direct {v0, p1}, Lcom/google/android/marvin/talkback/CursorGranularityManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    .line 86
    return-void
.end method

.method private adjustGranularity(I)Z
    .locals 6
    .param p1, "direction"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 462
    const/4 v0, 0x0

    .line 465
    .local v0, "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 466
    if-nez v0, :cond_0

    .line 478
    new-array v3, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v2

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v1, v2

    :goto_0
    return v1

    .line 470
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v3, v0, p1}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->adjustGranularityAt(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Z

    move-result v1

    .line 472
    .local v1, "wasAdjusted":Z
    if-eqz v1, :cond_1

    .line 473
    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->getRequestedGranularity()Lcom/google/android/marvin/talkback/CursorGranularity;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/google/android/marvin/talkback/CursorController;->granularityUpdated(Lcom/google/android/marvin/talkback/CursorGranularity;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 478
    :cond_1
    new-array v3, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v2

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .end local v1    # "wasAdjusted":Z
    :catchall_0
    move-exception v3

    new-array v4, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v4, v2

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v3
.end method

.method private static attemptHtmlNavigation(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Z
    .locals 2
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p1, "direction"    # I

    .prologue
    .line 760
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    const/16 v0, 0x400

    .line 763
    .local v0, "action":I
    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    move-result v1

    return v1

    .line 760
    .end local v0    # "action":I
    :cond_0
    const/16 v0, 0x800

    goto :goto_0
.end method

.method private attemptScrollAction(I)Z
    .locals 10
    .param p1, "action"    # I

    .prologue
    const/4 v8, 0x2

    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 398
    const/4 v0, 0x0

    .line 399
    .local v0, "cursor":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v5, 0x0

    .line 402
    .local v5, "scrollableNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 403
    if-nez v0, :cond_0

    .line 422
    new-array v7, v8, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v7, v6

    aput-object v5, v7, v9

    invoke-static {v7}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v4, v6

    :goto_0
    return v4

    .line 407
    :cond_0
    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/CursorController;->getBestScrollableNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 408
    if-nez v5, :cond_1

    .line 422
    new-array v7, v8, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v7, v6

    aput-object v5, v7, v9

    invoke-static {v7}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v4, v6

    goto :goto_0

    .line 412
    :cond_1
    :try_start_2
    invoke-virtual {v5, p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    move-result v4

    .line 413
    .local v4, "performedAction":Z
    if-eqz v4, :cond_2

    .line 414
    new-instance v3, Ljava/util/HashSet;

    iget-object v7, p0, Lcom/google/android/marvin/talkback/CursorController;->mScrollListeners:Ljava/util/Set;

    invoke-direct {v3, v7}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 415
    .local v3, "listeners":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/marvin/talkback/CursorController$ScrollListener;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/marvin/talkback/CursorController$ScrollListener;

    .line 416
    .local v2, "listener":Lcom/google/android/marvin/talkback/CursorController$ScrollListener;
    invoke-interface {v2, p1}, Lcom/google/android/marvin/talkback/CursorController$ScrollListener;->onScroll(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 422
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/google/android/marvin/talkback/CursorController$ScrollListener;
    .end local v3    # "listeners":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/marvin/talkback/CursorController$ScrollListener;>;"
    .end local v4    # "performedAction":Z
    :catchall_0
    move-exception v7

    new-array v8, v8, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v8, v6

    aput-object v5, v8, v9

    invoke-static {v8}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v7

    .restart local v4    # "performedAction":Z
    :cond_2
    new-array v7, v8, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v7, v6

    aput-object v5, v7, v9

    invoke-static {v7}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0
.end method

.method private getBestScrollableNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 5
    .param p1, "cursor"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 428
    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    sget-object v4, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SCROLLABLE:Lcom/googlecode/eyesfree/utils/NodeFilter;

    invoke-static {v3, p1, v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getSelfOrMatchingAncestor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 432
    .local v0, "predecessor":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v0, :cond_0

    .line 449
    .end local v0    # "predecessor":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :goto_0
    return-object v0

    .line 441
    .restart local v0    # "predecessor":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_0
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getRoot(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 442
    .local v1, "root":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    sget-object v4, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SCROLLABLE:Lcom/googlecode/eyesfree/utils/NodeFilter;

    invoke-static {v3, v1, v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->searchFromBfs(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    .line 445
    .local v2, "searched":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v2, :cond_1

    move-object v0, v2

    .line 446
    goto :goto_0

    .line 449
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getLastNodeFrom(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 2
    .param p0, "root"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 618
    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    move-result-object v0

    .line 619
    .local v0, "ref":Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;
    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->lastDescendant()Z

    .line 620
    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->release()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    return-object v1
.end method

.method private granularityUpdated(Lcom/google/android/marvin/talkback/CursorGranularity;Z)V
    .locals 7
    .param p1, "granularity"    # Lcom/google/android/marvin/talkback/CursorGranularity;
    .param p2, "fromUser"    # Z

    .prologue
    const/4 v6, 0x0

    .line 673
    new-instance v2, Ljava/util/HashSet;

    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityListeners:Ljava/util/Set;

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 676
    .local v2, "localListeners":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;

    .line 677
    .local v1, "listener":Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;
    invoke-interface {v1, p1}, Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;->onGranularityChanged(Lcom/google/android/marvin/talkback/CursorGranularity;)V

    goto :goto_0

    .line 680
    .end local v1    # "listener":Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;
    :cond_0
    if-eqz p2, :cond_1

    .line 681
    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    iget v5, p1, Lcom/google/android/marvin/talkback/CursorGranularity;->resourceId:I

    invoke-virtual {v4, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v6, v6, v5}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    .line 684
    :cond_1
    return-void
.end method

.method private navigateFrom(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 6
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "direction"    # I

    .prologue
    .line 638
    if-nez p1, :cond_0

    .line 639
    const/4 v0, 0x0

    .line 668
    :goto_0
    return-object v0

    .line 642
    :cond_0
    const/4 v0, 0x0

    .line 646
    .local v0, "next":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_start_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorController;->mNavigateSeenNodes:Ljava/util/HashSet;

    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    .line 648
    invoke-static {p1, p2}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 650
    :goto_1
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v1, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 651
    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorController;->mNavigateSeenNodes:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 652
    const/4 v1, 0x6

    const-string v2, "Found duplicate during traversal: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getInfo()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 668
    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorController;->mNavigateSeenNodes:Ljava/util/HashSet;

    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    goto :goto_0

    .line 658
    :cond_2
    const/4 v1, 0x2

    :try_start_1
    const-string v2, "Search strategy rejected node: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getInfo()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 661
    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorController;->mNavigateSeenNodes:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 663
    invoke-static {v0, p2}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 668
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/CursorController;->mNavigateSeenNodes:Ljava/util/HashSet;

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    throw v1
.end method

.method private navigateSelfOrFrom(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 1
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "direction"    # I

    .prologue
    .line 625
    if-nez p1, :cond_0

    .line 626
    const/4 v0, 0x0

    .line 633
    :goto_0
    return-object v0

    .line 629
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 630
    invoke-static {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0

    .line 633
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/marvin/talkback/CursorController;->navigateFrom(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0
.end method

.method private navigateWithGranularity(IZZ)Z
    .locals 11
    .param p1, "direction"    # I
    .param p2, "shouldWrap"    # Z
    .param p3, "shouldScroll"    # Z

    .prologue
    .line 510
    const/4 v8, 0x1

    if-ne p1, v8, :cond_0

    .line 511
    const/16 v3, 0x100

    .line 512
    .local v3, "navigationAction":I
    const/16 v6, 0x1000

    .line 513
    .local v6, "scrollDirection":I
    const/4 v2, 0x1

    .line 514
    .local v2, "focusSearchDirection":I
    const/4 v1, 0x1

    .line 522
    .local v1, "edgeDirection":I
    :goto_0
    const/4 v0, 0x0

    .line 523
    .local v0, "current":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v7, 0x0

    .line 526
    .local v7, "target":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 527
    if-nez v0, :cond_1

    .line 528
    const/4 v8, 0x0

    .line 579
    const/4 v9, 0x2

    new-array v9, v9, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    const/4 v10, 0x1

    aput-object v7, v9, v10

    invoke-static {v9}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    :goto_1
    return v8

    .line 516
    .end local v0    # "current":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .end local v1    # "edgeDirection":I
    .end local v2    # "focusSearchDirection":I
    .end local v3    # "navigationAction":I
    .end local v6    # "scrollDirection":I
    .end local v7    # "target":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_0
    const/16 v3, 0x200

    .line 517
    .restart local v3    # "navigationAction":I
    const/16 v6, 0x2000

    .line 518
    .restart local v6    # "scrollDirection":I
    const/4 v2, -0x1

    .line 519
    .restart local v2    # "focusSearchDirection":I
    const/4 v1, -0x1

    .restart local v1    # "edgeDirection":I
    goto :goto_0

    .line 533
    .restart local v0    # "current":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .restart local v7    # "target":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_1
    :try_start_1
    iget-object v8, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v8, v0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->isLockedTo(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 534
    iget-object v8, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v8, v3}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->navigate(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    .line 535
    .local v5, "result":I
    const/4 v8, 0x1

    if-ne v5, v8, :cond_2

    const/4 v8, 0x1

    .line 579
    :goto_2
    const/4 v9, 0x2

    new-array v9, v9, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    const/4 v10, 0x1

    aput-object v7, v9, v10

    invoke-static {v9}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_1

    .line 535
    :cond_2
    const/4 v8, 0x0

    goto :goto_2

    .line 539
    .end local v5    # "result":I
    :cond_3
    :try_start_2
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->supportsWebActions(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-static {v0, p1}, Lcom/google/android/marvin/talkback/CursorController;->attemptHtmlNavigation(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v8

    if-eqz v8, :cond_4

    .line 541
    const/4 v8, 0x1

    .line 579
    const/4 v9, 0x2

    new-array v9, v9, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    const/4 v10, 0x1

    aput-object v7, v9, v10

    invoke-static {v9}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_1

    .line 546
    :cond_4
    if-eqz p3, :cond_5

    .line 547
    :try_start_3
    iget-object v8, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v8}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 549
    .local v4, "prefs":Landroid/content/SharedPreferences;
    iget-object v8, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v8}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f060037

    const v10, 0x7f0b000c

    invoke-static {v4, v8, v9, v10}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result p3

    .line 556
    .end local v4    # "prefs":Landroid/content/SharedPreferences;
    :cond_5
    if-eqz p3, :cond_6

    iget-object v8, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v8, v0, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isAutoScrollEdgeListItem(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-direct {p0, v6}, Lcom/google/android/marvin/talkback/CursorController;->attemptScrollAction(I)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v8

    if-eqz v8, :cond_6

    .line 559
    const/4 v8, 0x1

    .line 579
    const/4 v9, 0x2

    new-array v9, v9, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    const/4 v10, 0x1

    aput-object v7, v9, v10

    invoke-static {v9}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_1

    .line 563
    :cond_6
    :try_start_4
    invoke-direct {p0, v0, v2}, Lcom/google/android/marvin/talkback/CursorController;->navigateFrom(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v7

    .line 564
    if-eqz v7, :cond_7

    invoke-virtual {p0, v7}, Lcom/google/android/marvin/talkback/CursorController;->setCursor(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 565
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/marvin/talkback/CursorController;->mReachedEdge:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 566
    const/4 v8, 0x1

    .line 579
    const/4 v9, 0x2

    new-array v9, v9, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    const/4 v10, 0x1

    aput-object v7, v9, v10

    invoke-static {v9}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto/16 :goto_1

    .line 569
    :cond_7
    :try_start_5
    iget-boolean v8, p0, Lcom/google/android/marvin/talkback/CursorController;->mReachedEdge:Z

    if-eqz v8, :cond_8

    if-eqz p2, :cond_8

    .line 570
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/marvin/talkback/CursorController;->mReachedEdge:Z

    .line 571
    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/CursorController;->navigateWrapAround(I)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v8

    .line 579
    const/4 v9, 0x2

    new-array v9, v9, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    const/4 v10, 0x1

    aput-object v7, v9, v10

    invoke-static {v9}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto/16 :goto_1

    .line 576
    :cond_8
    const/4 v8, 0x1

    :try_start_6
    iput-boolean v8, p0, Lcom/google/android/marvin/talkback/CursorController;->mReachedEdge:Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 577
    const/4 v8, 0x0

    .line 579
    const/4 v9, 0x2

    new-array v9, v9, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    const/4 v10, 0x1

    aput-object v7, v9, v10

    invoke-static {v9}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto/16 :goto_1

    :catchall_0
    move-exception v8

    const/4 v9, 0x2

    new-array v9, v9, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    const/4 v10, 0x1

    aput-object v7, v9, v10

    invoke-static {v9}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v8
.end method

.method private navigateWrapAround(I)Z
    .locals 12
    .param p1, "direction"    # I

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 584
    iget-object v6, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getRootInActiveWindow()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v2

    .line 585
    .local v2, "root":Landroid/view/accessibility/AccessibilityNodeInfo;
    if-nez v2, :cond_0

    .line 613
    :goto_0
    return v5

    .line 589
    :cond_0
    const/4 v0, 0x0

    .line 590
    .local v0, "compatRoot":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v3, 0x0

    .line 591
    .local v3, "tempNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v4, 0x0

    .line 594
    .local v4, "wrapNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_start_0
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {v1, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;-><init>(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 596
    .end local v0    # "compatRoot":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .local v1, "compatRoot":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    packed-switch p1, :pswitch_data_0

    .line 606
    :goto_1
    :pswitch_0
    if-nez v4, :cond_1

    .line 607
    const/4 v6, 0x6

    :try_start_1
    const-string v7, "Failed to wrap navigation"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {p0, v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 613
    new-array v6, v11, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v6, v5

    aput-object v3, v6, v9

    aput-object v4, v6, v10

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 598
    :pswitch_1
    :try_start_2
    invoke-direct {p0, v1, p1}, Lcom/google/android/marvin/talkback/CursorController;->navigateSelfOrFrom(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v4

    .line 599
    goto :goto_1

    .line 601
    :pswitch_2
    invoke-static {v1}, Lcom/google/android/marvin/talkback/CursorController;->getLastNodeFrom(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v3

    .line 602
    invoke-direct {p0, v3, p1}, Lcom/google/android/marvin/talkback/CursorController;->navigateSelfOrFrom(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v4

    goto :goto_1

    .line 611
    :cond_1
    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/CursorController;->setCursor(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v6

    .line 613
    new-array v7, v11, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v7, v5

    aput-object v3, v7, v9

    aput-object v4, v7, v10

    invoke-static {v7}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v5, v6

    goto :goto_0

    .end local v1    # "compatRoot":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .restart local v0    # "compatRoot":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :catchall_0
    move-exception v6

    :goto_2
    new-array v7, v11, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v7, v5

    aput-object v3, v7, v9

    aput-object v4, v7, v10

    invoke-static {v7}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v6

    .end local v0    # "compatRoot":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .restart local v1    # "compatRoot":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "compatRoot":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .restart local v0    # "compatRoot":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    goto :goto_2

    .line 596
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private performAction(I)Z
    .locals 4
    .param p1, "action"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 693
    const/4 v0, 0x0

    .line 696
    .local v0, "current":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 697
    if-nez v0, :cond_0

    .line 703
    new-array v2, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v1

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    :goto_0
    return v1

    .line 701
    :cond_0
    :try_start_1
    invoke-virtual {v0, p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    .line 703
    new-array v3, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v1

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v1, v2

    goto :goto_0

    :catchall_0
    move-exception v2

    new-array v3, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v1

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v2
.end method


# virtual methods
.method public addGranularityListener(Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;

    .prologue
    .line 94
    if-nez p1, :cond_0

    .line 95
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 99
    return-void
.end method

.method public addScrollListener(Lcom/google/android/marvin/talkback/CursorController$ScrollListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/marvin/talkback/CursorController$ScrollListener;

    .prologue
    .line 110
    if-nez p1, :cond_0

    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mScrollListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 115
    return-void
.end method

.method public clearCursor()V
    .locals 3

    .prologue
    .line 324
    iget-object v2, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getRootInActiveWindow()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v1

    .line 325
    .local v1, "root":Landroid/view/accessibility/AccessibilityNodeInfo;
    if-nez v1, :cond_1

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->findFocus(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 331
    .local v0, "focused":Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v0, :cond_0

    .line 335
    const/16 v2, 0x80

    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->performAction(I)Z

    goto :goto_0
.end method

.method public clickCurrent()Z
    .locals 1

    .prologue
    .line 223
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/CursorController;->performAction(I)Z

    move-result v0

    return v0
.end method

.method public getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 4

    .prologue
    .line 348
    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getRootInActiveWindow()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 349
    .local v0, "activeRoot":Landroid/view/accessibility/AccessibilityNodeInfo;
    if-nez v0, :cond_1

    .line 350
    const/4 v1, 0x0

    .line 371
    :cond_0
    :goto_0
    return-object v1

    .line 353
    :cond_1
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {v1, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;-><init>(Ljava/lang/Object;)V

    .line 354
    .local v1, "compatRoot":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->findFocus(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    .line 359
    .local v2, "focusedNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v2, :cond_0

    .line 366
    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isVisibleOrLegacy(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 367
    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    :cond_2
    move-object v1, v2

    .line 371
    goto :goto_0
.end method

.method public getGranularityAt(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lcom/google/android/marvin/talkback/CursorGranularity;
    .locals 1
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v0, p1}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->isLockedTo(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->getRequestedGranularity()Lcom/google/android/marvin/talkback/CursorGranularity;

    move-result-object v0

    .line 388
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/marvin/talkback/CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularity;

    goto :goto_0
.end method

.method public isSelectionModeActive()Z
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->isSelectionModeActive()Z

    move-result v0

    return v0
.end method

.method public jumpToBottom()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 194
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->clearCursor()V

    .line 195
    iput-boolean v1, p0, Lcom/google/android/marvin/talkback/CursorController;->mReachedEdge:Z

    .line 196
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/marvin/talkback/CursorController;->previous(ZZ)Z

    move-result v0

    return v0
.end method

.method public jumpToTop()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 183
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->clearCursor()V

    .line 184
    iput-boolean v1, p0, Lcom/google/android/marvin/talkback/CursorController;->mReachedEdge:Z

    .line 185
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/marvin/talkback/CursorController;->next(ZZ)Z

    move-result v0

    return v0
.end method

.method public less()Z
    .locals 1

    .prologue
    .line 214
    const/16 v0, 0x2000

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/CursorController;->attemptScrollAction(I)Z

    move-result v0

    return v0
.end method

.method public more()Z
    .locals 1

    .prologue
    .line 205
    const/16 v0, 0x1000

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/CursorController;->attemptScrollAction(I)Z

    move-result v0

    return v0
.end method

.method public next(ZZ)Z
    .locals 1
    .param p1, "shouldWrap"    # Z
    .param p2, "shouldScroll"    # Z

    .prologue
    .line 160
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/marvin/talkback/CursorController;->navigateWithGranularity(IZZ)Z

    move-result v0

    return v0
.end method

.method public nextGranularity()Z
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/CursorController;->adjustGranularity(I)Z

    move-result v0

    return v0
.end method

.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 731
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v2

    const v3, 0x8000

    if-ne v2, v3, :cond_0

    .line 732
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getSource()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 733
    .local v0, "node":Landroid/view/accessibility/AccessibilityNodeInfo;
    if-nez v0, :cond_1

    .line 734
    const/4 v2, 0x5

    const-string v3, "TYPE_VIEW_ACCESSIBILITY_FOCUSED event without a source."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 746
    .end local v0    # "node":Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_0
    :goto_0
    return-void

    .line 742
    .restart local v0    # "node":Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_1
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {v1, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;-><init>(Ljava/lang/Object;)V

    .line 743
    .local v1, "nodeCompat":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    iget-object v2, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v2, v1}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->onNodeFocused(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 744
    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0
.end method

.method public onComboPerformed(I)Z
    .locals 2
    .param p1, "id"    # I

    .prologue
    const/4 v0, 0x1

    .line 709
    const v1, 0x7f0d0017

    if-ne p1, v1, :cond_0

    .line 710
    invoke-virtual {p0, v0, v0}, Lcom/google/android/marvin/talkback/CursorController;->next(ZZ)Z

    .line 726
    :goto_0
    return v0

    .line 712
    :cond_0
    const v1, 0x7f0d0016

    if-ne p1, v1, :cond_1

    .line 713
    invoke-virtual {p0, v0, v0}, Lcom/google/android/marvin/talkback/CursorController;->previous(ZZ)Z

    goto :goto_0

    .line 715
    :cond_1
    const v1, 0x7f0d0014

    if-ne p1, v1, :cond_2

    .line 716
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->jumpToTop()Z

    goto :goto_0

    .line 718
    :cond_2
    const v1, 0x7f0d0015

    if-ne p1, v1, :cond_3

    .line 719
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->jumpToBottom()Z

    goto :goto_0

    .line 721
    :cond_3
    const v1, 0x7f0d0013

    if-ne p1, v1, :cond_4

    .line 722
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->clickCurrent()Z

    goto :goto_0

    .line 726
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public previous(ZZ)Z
    .locals 1
    .param p1, "shouldWrap"    # Z
    .param p2, "shouldScroll"    # Z

    .prologue
    .line 174
    const/4 v0, -0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/marvin/talkback/CursorController;->navigateWithGranularity(IZZ)Z

    move-result v0

    return v0
.end method

.method public previousGranularity()Z
    .locals 1

    .prologue
    .line 250
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/CursorController;->adjustGranularity(I)Z

    move-result v0

    return v0
.end method

.method public refocus()Z
    .locals 2

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 139
    .local v0, "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-nez v0, :cond_0

    .line 140
    const/4 v1, 0x0

    .line 146
    :goto_0
    return v1

    .line 143
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->clearCursor()V

    .line 144
    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/CursorController;->setCursor(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    .line 145
    .local v1, "result":Z
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0
.end method

.method public removeGranularityListener(Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;

    .prologue
    .line 102
    if-nez p1, :cond_0

    .line 103
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 107
    return-void
.end method

.method public setCursor(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 290
    const/16 v0, 0x40

    invoke-virtual {p1, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    move-result v0

    return v0
.end method

.method public setGranularity(Lcom/google/android/marvin/talkback/CursorGranularity;Z)Z
    .locals 4
    .param p1, "granularity"    # Lcom/google/android/marvin/talkback/CursorGranularity;
    .param p2, "fromUser"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 263
    const/4 v0, 0x0

    .line 266
    .local v0, "current":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 267
    if-nez v0, :cond_0

    .line 277
    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v1

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 280
    :goto_0
    return v1

    .line 271
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v3, v0, p1}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->setGranularityAt(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/google/android/marvin/talkback/CursorGranularity;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    .line 277
    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v1

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 275
    :cond_1
    :try_start_2
    invoke-direct {p0, p1, p2}, Lcom/google/android/marvin/talkback/CursorController;->granularityUpdated(Lcom/google/android/marvin/talkback/CursorGranularity;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 277
    new-array v3, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v1

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v1, v2

    .line 280
    goto :goto_0

    .line 277
    :catchall_0
    move-exception v3

    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v1

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v3
.end method

.method public setSelectionModeActive(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)V
    .locals 2
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "active"    # Z

    .prologue
    .line 305
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v0, p1}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->isLockedTo(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 306
    sget-object v0, Lcom/google/android/marvin/talkback/CursorGranularity;->CHARACTER:Lcom/google/android/marvin/talkback/CursorGranularity;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/marvin/talkback/CursorController;->setGranularity(Lcom/google/android/marvin/talkback/CursorGranularity;Z)Z

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v0, p2}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->setSelectionModeActive(Z)V

    .line 310
    return-void
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->shutdown()V

    .line 130
    return-void
.end method
