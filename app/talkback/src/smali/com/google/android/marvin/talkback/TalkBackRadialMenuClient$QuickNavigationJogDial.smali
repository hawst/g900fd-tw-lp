.class Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;
.super Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;
.source "TalkBackRadialMenuClient.java"

# interfaces
.implements Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;
.implements Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QuickNavigationJogDial"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mCursorController:Lcom/google/android/marvin/talkback/CursorController;

.field private final mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

.field private final mHandler:Landroid/os/Handler;

.field private final mHintRunnable:Ljava/lang/Runnable;

.field private final mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 1
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 187
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;-><init>(I)V

    .line 179
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mHandler:Landroid/os/Handler;

    .line 240
    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial$1;-><init>(Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mHintRunnable:Ljava/lang/Runnable;

    .line 189
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mContext:Landroid/content/Context;

    .line 190
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    .line 191
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    .line 192
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/CachedFeedbackController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    .line 193
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;)Lcom/google/android/marvin/talkback/SpeechController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    return-object v0
.end method


# virtual methods
.method public onFirstTouch()V
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/CursorController;->refocus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/marvin/talkback/CursorController;->next(ZZ)Z

    .line 200
    :cond_0
    return-void
.end method

.method public onMenuDismissed(Lcom/googlecode/eyesfree/widget/RadialMenu;)V
    .locals 2
    .param p1, "menu"    # Lcom/googlecode/eyesfree/widget/RadialMenu;

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mHintRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 237
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/CursorController;->refocus()Z

    .line 238
    return-void
.end method

.method public onMenuItemSelection(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Z
    .locals 2
    .param p1, "item"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mHintRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 220
    if-nez p1, :cond_0

    .line 222
    const/4 v0, 0x0

    .line 226
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onMenuShown(Lcom/googlecode/eyesfree/widget/RadialMenu;)V
    .locals 4
    .param p1, "menu"    # Lcom/googlecode/eyesfree/widget/RadialMenu;

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mHintRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 232
    return-void
.end method

.method public onNext()V
    .locals 3

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/marvin/talkback/CursorController;->next(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    const v1, 0x7f050003

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(I)Z

    .line 214
    :cond_0
    return-void
.end method

.method public onPrevious()V
    .locals 3

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/marvin/talkback/CursorController;->previous(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    const v1, 0x7f050003

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(I)Z

    .line 207
    :cond_0
    return-void
.end method
