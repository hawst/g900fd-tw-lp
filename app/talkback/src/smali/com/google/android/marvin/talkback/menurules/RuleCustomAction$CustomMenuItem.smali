.class Lcom/google/android/marvin/talkback/menurules/RuleCustomAction$CustomMenuItem;
.super Ljava/lang/Object;
.source "RuleCustomAction.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/menurules/RuleCustomAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CustomMenuItem"
.end annotation


# instance fields
.field final mId:I

.field final mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;


# direct methods
.method constructor <init>(ILandroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput p1, p0, Lcom/google/android/marvin/talkback/menurules/RuleCustomAction$CustomMenuItem;->mId:I

    .line 90
    iput-object p2, p0, Lcom/google/android/marvin/talkback/menurules/RuleCustomAction$CustomMenuItem;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 91
    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 95
    iget-object v1, p0, Lcom/google/android/marvin/talkback/menurules/RuleCustomAction$CustomMenuItem;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    iget v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleCustomAction$CustomMenuItem;->mId:I

    invoke-virtual {v1, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    move-result v0

    .line 96
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/google/android/marvin/talkback/menurules/RuleCustomAction$CustomMenuItem;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 97
    return v0
.end method
