.class public Lcom/google/android/marvin/talkback/formatter/CheckableClickedFormatter;
.super Ljava/lang/Object;
.source "CheckableClickedFormatter.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 24
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v4

    if-eq v4, v3, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v2

    .line 28
    :cond_1
    new-instance v0, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v0, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 29
    .local v0, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 32
    .local v1, "source":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isCheckable()Z

    move-result v4

    if-eqz v4, :cond_2

    move v2, v3

    .line 33
    goto :goto_0

    .line 34
    :cond_2
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-ge v4, v5, :cond_0

    .line 39
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_3

    move v2, v3

    .line 40
    goto :goto_0

    .line 45
    :cond_3
    const-string v4, "android.widget.CompoundButton"

    invoke-static {p2, p1, v4}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->eventMatchesClass(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    .line 47
    goto :goto_0
.end method

.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    .line 55
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v1, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 56
    .local v1, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    .line 58
    .local v2, "source":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->getEventTextOrDescription(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 59
    .local v0, "eventText":Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 60
    invoke-virtual {p3, v0}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 65
    :cond_0
    const-class v3, Landroid/widget/ToggleButton;

    invoke-static {p2, v2, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "android.widget.Switch.class"

    invoke-static {p2, v2, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByName(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 70
    :cond_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v3, v4, :cond_2

    .line 74
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 75
    const v3, 0x7f0600c7

    invoke-virtual {p2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 82
    :cond_2
    :goto_0
    const v3, 0x7f050020

    invoke-virtual {p3, v3}, Lcom/google/android/marvin/talkback/Utterance;->addAuditory(I)V

    .line 83
    const v3, 0x7f0a001b

    invoke-virtual {p3, v3}, Lcom/google/android/marvin/talkback/Utterance;->addHaptic(I)V

    .line 85
    const/4 v3, 0x1

    return v3

    .line 77
    :cond_3
    const v3, 0x7f0600c8

    invoke-virtual {p2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
