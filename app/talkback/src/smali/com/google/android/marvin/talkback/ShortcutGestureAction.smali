.class public final enum Lcom/google/android/marvin/talkback/ShortcutGestureAction;
.super Ljava/lang/Enum;
.source "ShortcutGestureAction.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/marvin/talkback/ShortcutGestureAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/marvin/talkback/ShortcutGestureAction;

.field public static final enum BACK:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

.field public static final enum HOME:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

.field public static final enum LOCAL_BREAKOUT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

.field public static final enum NOTIFICATIONS:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

.field public static final enum PRINT_NODE_TREE:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

.field public static final enum READ_FROM_CURRENT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

.field public static final enum READ_FROM_TOP:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

.field public static final enum RECENTS:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

.field public static final enum TALKBACK_BREAKOUT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

.field public static final enum UNASSIGNED:Lcom/google/android/marvin/talkback/ShortcutGestureAction;


# instance fields
.field private final mLabelResId:I

.field private final mValueResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 27
    new-instance v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    const-string v1, "UNASSIGNED"

    const v2, 0x7f060058

    const v3, 0x7f06011c

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->UNASSIGNED:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    .line 28
    new-instance v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    const-string v1, "BACK"

    const v2, 0x7f060059

    const v3, 0x7f06011d

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->BACK:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    .line 29
    new-instance v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    const-string v1, "HOME"

    const v2, 0x7f06005a

    const v3, 0x7f06011e

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->HOME:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    .line 30
    new-instance v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    const-string v1, "RECENTS"

    const v2, 0x7f06005b

    const v3, 0x7f06011f

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->RECENTS:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    .line 31
    new-instance v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    const-string v1, "NOTIFICATIONS"

    const v2, 0x7f06005c

    const v3, 0x7f060120

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->NOTIFICATIONS:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    .line 32
    new-instance v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    const-string v1, "TALKBACK_BREAKOUT"

    const/4 v2, 0x5

    const v3, 0x7f06005d

    const v4, 0x7f060121

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->TALKBACK_BREAKOUT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    .line 34
    new-instance v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    const-string v1, "LOCAL_BREAKOUT"

    const/4 v2, 0x6

    const v3, 0x7f06005e

    const v4, 0x7f060122

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->LOCAL_BREAKOUT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    .line 35
    new-instance v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    const-string v1, "READ_FROM_TOP"

    const/4 v2, 0x7

    const v3, 0x7f06005f

    const v4, 0x7f060126

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->READ_FROM_TOP:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    .line 36
    new-instance v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    const-string v1, "READ_FROM_CURRENT"

    const/16 v2, 0x8

    const v3, 0x7f060060

    const v4, 0x7f060127

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->READ_FROM_CURRENT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    .line 38
    new-instance v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    const-string v1, "PRINT_NODE_TREE"

    const/16 v2, 0x9

    const v3, 0x7f060061

    const v4, 0x7f060128

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->PRINT_NODE_TREE:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    .line 26
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    sget-object v1, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->UNASSIGNED:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->BACK:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->HOME:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->RECENTS:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->NOTIFICATIONS:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->TALKBACK_BREAKOUT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->LOCAL_BREAKOUT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->READ_FROM_TOP:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->READ_FROM_CURRENT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->PRINT_NODE_TREE:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->$VALUES:[Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "valueResId"    # I
    .param p4, "labelResId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    iput p3, p0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->mValueResId:I

    .line 48
    iput p4, p0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->mLabelResId:I

    .line 49
    return-void
.end method

.method public static safeValueOf(Ljava/lang/String;)Lcom/google/android/marvin/talkback/ShortcutGestureAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 82
    if-nez p0, :cond_0

    .line 83
    sget-object v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->UNASSIGNED:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    .line 92
    :goto_0
    return-object v0

    .line 87
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->valueOf(Ljava/lang/String;)Lcom/google/android/marvin/talkback/ShortcutGestureAction;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 88
    :catch_0
    move-exception v0

    .line 92
    sget-object v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->UNASSIGNED:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/marvin/talkback/ShortcutGestureAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/marvin/talkback/ShortcutGestureAction;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->$VALUES:[Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    invoke-virtual {v0}, [Lcom/google/android/marvin/talkback/ShortcutGestureAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    return-object v0
.end method


# virtual methods
.method public getLabel(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    iget v0, p0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->mLabelResId:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
