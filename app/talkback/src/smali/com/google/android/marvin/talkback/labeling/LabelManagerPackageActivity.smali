.class public Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;
.super Landroid/app/Activity;
.source "LabelManagerPackageActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$1;,
        Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LoadScreenshotTask;,
        Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;,
        Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;
    }
.end annotation


# instance fields
.field private mApplicationLabel:Ljava/lang/CharSequence;

.field private mLabelAdapter:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;

.field private mLabelList:Landroid/widget/ListView;

.field private mLabelProviderClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

.field private mPackageIcon:Landroid/graphics/drawable/Drawable;

.field private mPackageName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 227
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;)Lcom/googlecode/eyesfree/labeling/LabelProviderClient;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mLabelProviderClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;)Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mLabelAdapter:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;)Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;
    .param p1, "x1"    # Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mLabelAdapter:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mLabelList:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 75
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    const v5, 0x7f030003

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->setContentView(I)V

    .line 79
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 80
    .local v2, "intent":Landroid/content/Intent;
    const-string v5, "packageName"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 81
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Intent missing package name extra."

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 84
    :cond_0
    const-string v5, "packageName"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mPackageName:Ljava/lang/String;

    .line 86
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 88
    .local v4, "packageManager":Landroid/content/pm/PackageManager;
    :try_start_0
    iget-object v5, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mPackageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mPackageIcon:Landroid/graphics/drawable/Drawable;

    .line 89
    iget-object v5, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mPackageName:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 90
    .local v3, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mApplicationLabel:Ljava/lang/CharSequence;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    .end local v3    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    const v5, 0x7f060088

    new-array v6, v10, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mApplicationLabel:Ljava/lang/CharSequence;

    aput-object v7, v6, v9

    invoke-virtual {p0, v5, v6}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 102
    .local v0, "actionBar":Landroid/app/ActionBar;
    iget-object v5, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mPackageIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 103
    invoke-virtual {v0, v10}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 105
    const v5, 0x7f0d0074

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mLabelList:Landroid/widget/ListView;

    .line 106
    new-instance v5, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    const-string v6, "com.google.android.marvin.talkback.providers.LabelProvider"

    invoke-direct {v5, p0, v6}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mLabelProviderClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    .line 107
    return-void

    .line 91
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    :catch_0
    move-exception v1

    .line 92
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v5, 0x4

    const-string v6, "Could not load package info for package %s."

    new-array v7, v10, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mPackageName:Ljava/lang/String;

    aput-object v8, v7, v9

    invoke-static {p0, v5, v6, v7}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 95
    invoke-virtual {v4}, Landroid/content/pm/PackageManager;->getDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mPackageIcon:Landroid/graphics/drawable/Drawable;

    .line 96
    iget-object v5, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mPackageName:Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mApplicationLabel:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 118
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 120
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mLabelProviderClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->shutdown()V

    .line 121
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 128
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 133
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 130
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->finish()V

    .line 131
    const/4 v0, 0x1

    goto :goto_0

    .line 128
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 113
    new-instance v0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;-><init>(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 114
    return-void
.end method
