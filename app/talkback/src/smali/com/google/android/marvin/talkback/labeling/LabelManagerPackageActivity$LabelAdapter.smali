.class Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;
.super Landroid/widget/ArrayAdapter;
.source "LabelManagerPackageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LabelAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/googlecode/eyesfree/labeling/Label;",
        ">;"
    }
.end annotation


# instance fields
.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 145
    .local p4, "items":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/labeling/Label;>;"
    iput-object p1, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    .line 146
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 148
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 149
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v11, 0x0

    .line 154
    if-nez p2, :cond_0

    .line 155
    iget-object v7, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v8, 0x7f030002

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 158
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/googlecode/eyesfree/labeling/Label;

    .line 159
    .local v3, "label":Lcom/googlecode/eyesfree/labeling/Label;
    if-nez v3, :cond_1

    .line 186
    :goto_0
    return-object p2

    .line 163
    :cond_1
    const v7, 0x7f0d0072

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 164
    .local v4, "textView":Landroid/widget/TextView;
    invoke-virtual {v3}, Lcom/googlecode/eyesfree/labeling/Label;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    const v7, 0x7f0d0073

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 167
    .local v6, "timestampView":Landroid/widget/TextView;
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1}, Ljava/text/SimpleDateFormat;-><init>()V

    .line 168
    .local v1, "dateFormat":Ljava/text/DateFormat;
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v3}, Lcom/googlecode/eyesfree/labeling/Label;->getTimestamp()J

    move-result-wide v8

    invoke-direct {v0, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 169
    .local v0, "date":Ljava/util/Date;
    iget-object v7, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    const v8, 0x7f06008a

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {v7, v8, v9}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 171
    .local v5, "timestampMessage":Ljava/lang/String;
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    const v7, 0x7f0d0071

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 174
    .local v2, "iconImage":Landroid/widget/ImageView;
    new-instance v7, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LoadScreenshotTask;

    iget-object v8, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    invoke-direct {v7, v8, v3, v2}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LoadScreenshotTask;-><init>(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;Lcom/googlecode/eyesfree/labeling/Label;Landroid/widget/ImageView;)V

    new-array v8, v11, [Ljava/lang/Void;

    invoke-virtual {v7, v8}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LoadScreenshotTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 176
    new-instance v7, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter$1;

    invoke-direct {v7, p0, v3}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter$1;-><init>(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;Lcom/googlecode/eyesfree/labeling/Label;)V

    invoke-virtual {p2, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
