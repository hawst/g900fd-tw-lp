.class public Lcom/google/android/marvin/talkback/KeyboardSearchManager;
.super Ljava/lang/Object;
.source "KeyboardSearchManager.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/KeyComboManager$KeyComboListener;
.implements Lcom/google/android/marvin/talkback/TalkBackService$KeyEventListener;
.implements Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field private final mContext:Lcom/google/android/marvin/talkback/TalkBackService;

.field private mHandler:Landroid/os/Handler;

.field private mHasNavigated:Z

.field private final mHint:Ljava/lang/Runnable;

.field private final mInitialNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

.field private final mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

.field private final mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

.field private final mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V
    .locals 2
    .param p1, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p2, "labelManager"    # Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHandler:Landroid/os/Handler;

    .line 69
    new-instance v1, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    invoke-direct {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;-><init>()V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mInitialNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    .line 365
    new-instance v1, Lcom/google/android/marvin/talkback/KeyboardSearchManager$2;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager$2;-><init>(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHint:Ljava/lang/Runnable;

    .line 76
    iput-object p1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 77
    iput-object p2, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    .line 79
    new-instance v0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager$1;-><init>(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)V

    .line 92
    .local v0, "formatter":Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;
    new-instance v1, Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-direct {v1, p1, p2, v0}, Lcom/googlecode/eyesfree/utils/NodeSearch;-><init>(Landroid/accessibilityservice/AccessibilityService;Lcom/googlecode/eyesfree/labeling/CustomLabelManager;Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    .line 93
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    .line 94
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/google/android/marvin/talkback/TalkBackService;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/googlecode/eyesfree/utils/NodeSearch;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHasNavigated:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/google/android/marvin/talkback/SpeechController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    return-object v0
.end method

.method private cancelSearch()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 337
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHint:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 338
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/NodeSearch;->stopSearch()V

    .line 340
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f06008d

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v4, v4, v3}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    .line 343
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/googlecode/eyesfree/utils/FocusFinder;->getFocusedNode(Landroid/accessibilityservice/AccessibilityService;Z)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 344
    .local v0, "focused":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-nez v0, :cond_0

    .line 362
    :goto_0
    return-void

    .line 349
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mInitialNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mInitialNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->get()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->refreshNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->reset(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 350
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mInitialNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->isNull(Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 351
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mInitialNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->get()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isAccessibilityFocused()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 360
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    .line 354
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mInitialNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->get()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 360
    :goto_1
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    .line 356
    :cond_2
    const/16 v1, 0x80

    :try_start_2
    invoke-virtual {v0, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 360
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    throw v1
.end method

.method private finishSearch()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 327
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHint:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 328
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/NodeSearch;->stopSearch()V

    .line 329
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f06008e

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    .line 331
    return-void
.end method

.method private moveToEnd(I)Z
    .locals 2
    .param p1, "direction"    # I

    .prologue
    .line 287
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->resetHintTime()V

    .line 289
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/NodeSearch;->hasMatch()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 290
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v1, p1}, Lcom/googlecode/eyesfree/utils/NodeSearch;->nextResult(I)Z

    move-result v0

    .line 291
    .local v0, "result":Z
    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v1, p1}, Lcom/googlecode/eyesfree/utils/NodeSearch;->nextResult(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 298
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHasNavigated:Z

    .line 299
    return v0

    .line 292
    .end local v0    # "result":Z
    :cond_1
    const/4 v1, -0x1

    if-ne p1, v1, :cond_2

    .line 293
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/CursorController;->jumpToTop()Z

    move-result v0

    .restart local v0    # "result":Z
    goto :goto_0

    .line 295
    .end local v0    # "result":Z
    :cond_2
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/CursorController;->jumpToBottom()Z

    move-result v0

    .restart local v0    # "result":Z
    goto :goto_0
.end method

.method private moveToNext(I)Z
    .locals 4
    .param p1, "direction"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 262
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->resetHintTime()V

    .line 264
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/NodeSearch;->hasMatch()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 265
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v1, p1}, Lcom/googlecode/eyesfree/utils/NodeSearch;->nextResult(I)Z

    move-result v0

    .line 274
    .local v0, "result":Z
    :goto_0
    iput-boolean v2, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHasNavigated:Z

    .line 275
    return v0

    .line 266
    .end local v0    # "result":Z
    :cond_0
    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    .line 267
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v1

    invoke-virtual {v1, v3, v2}, Lcom/google/android/marvin/talkback/CursorController;->previous(ZZ)Z

    move-result v0

    .restart local v0    # "result":Z
    goto :goto_0

    .line 270
    .end local v0    # "result":Z
    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v1

    invoke-virtual {v1, v3, v2}, Lcom/google/android/marvin/talkback/CursorController;->next(ZZ)Z

    move-result v0

    .restart local v0    # "result":Z
    goto :goto_0
.end method

.method private resetHintTime()V
    .locals 4

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHint:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 307
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHint:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 308
    return-void
.end method

.method private startSearch()V
    .locals 6

    .prologue
    .line 314
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/googlecode/eyesfree/utils/FocusFinder;->getFocusedNode(Landroid/accessibilityservice/AccessibilityService;Z)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 315
    .local v0, "focused":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mInitialNode:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    invoke-virtual {v1, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->reset(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 316
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHasNavigated:Z

    .line 317
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/NodeSearch;->startSearch()V

    .line 318
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f06008c

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    .line 320
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHint:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 321
    return-void
.end method


# virtual methods
.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/NodeSearch;->isActive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 239
    :cond_1
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 241
    :sswitch_0
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->cancelSearch()V

    goto :goto_0

    .line 244
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/NodeSearch;->hasMatch()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/NodeSearch;->reEvaluateSearch()V

    goto :goto_0

    .line 239
    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0x800 -> :sswitch_1
    .end sparse-switch
.end method

.method public onComboPerformed(I)Z
    .locals 4
    .param p1, "id"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    const/4 v0, 0x1

    .line 198
    const v2, 0x7f0d001b

    if-ne p1, v2, :cond_0

    .line 199
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->toggleSearch()V

    .line 230
    :goto_0
    return v0

    .line 204
    :cond_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/utils/NodeSearch;->isActive()Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 205
    goto :goto_0

    .line 208
    :cond_1
    const v2, 0x7f0d0016

    if-ne p1, v2, :cond_2

    .line 209
    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->moveToNext(I)Z

    goto :goto_0

    .line 211
    :cond_2
    const v2, 0x7f0d0017

    if-ne p1, v2, :cond_3

    .line 212
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->moveToNext(I)Z

    goto :goto_0

    .line 214
    :cond_3
    const v2, 0x7f0d0014

    if-ne p1, v2, :cond_4

    .line 215
    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->moveToEnd(I)Z

    goto :goto_0

    .line 217
    :cond_4
    const v2, 0x7f0d0015

    if-ne p1, v2, :cond_5

    .line 218
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->moveToEnd(I)Z

    goto :goto_0

    .line 220
    :cond_5
    const v2, 0x7f0d0013

    if-ne p1, v2, :cond_8

    .line 221
    iget-boolean v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHasNavigated:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/NodeSearch;->hasMatch()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 222
    :cond_6
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->finishSearch()V

    .line 223
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/CursorController;->clickCurrent()Z

    goto :goto_0

    .line 225
    :cond_7
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->cancelSearch()V

    goto :goto_0

    :cond_8
    move v0, v1

    .line 230
    goto :goto_0
.end method

.method public onGesture(I)Z
    .locals 1
    .param p1, "gestureId"    # I

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/NodeSearch;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->cancelSearch()V

    .line 117
    const/4 v0, 0x1

    .line 120
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 126
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getModifiers()I

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v5}, Lcom/googlecode/eyesfree/utils/NodeSearch;->isActive()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    move v3, v4

    .line 193
    :goto_0
    return v3

    .line 130
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    if-nez v5, :cond_7

    .line 131
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 177
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isPrintingKey()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 178
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->resetHintTime()V

    .line 179
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDisplayLabel()C

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    .line 180
    .local v0, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v4, v0}, Lcom/googlecode/eyesfree/utils/NodeSearch;->tryAddQueryText(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 181
    iget-object v4, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v10, v9, v11}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    goto :goto_0

    .line 133
    .end local v0    # "key":Ljava/lang/String;
    :sswitch_0
    iget-boolean v4, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHasNavigated:Z

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v4}, Lcom/googlecode/eyesfree/utils/NodeSearch;->hasMatch()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 134
    :cond_2
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->finishSearch()V

    .line 135
    iget-object v4, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/CursorController;->clickCurrent()Z

    goto :goto_0

    .line 137
    :cond_3
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->cancelSearch()V

    goto :goto_0

    .line 141
    :sswitch_1
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->resetHintTime()V

    .line 142
    iget-object v5, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v5}, Lcom/googlecode/eyesfree/utils/NodeSearch;->getCurrentQuery()Ljava/lang/String;

    move-result-object v2

    .line 143
    .local v2, "queryText":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 144
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->cancelSearch()V

    goto :goto_0

    .line 146
    :cond_4
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 147
    .local v1, "lastChar":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v5}, Lcom/googlecode/eyesfree/utils/NodeSearch;->backspaceQueryText()Z

    .line 148
    iget-object v5, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const v7, 0x7f0600a9

    new-array v8, v3, [Ljava/lang/Object;

    aput-object v1, v8, v4

    invoke-virtual {v6, v7, v8}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4, v10, v9, v11}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 155
    .end local v1    # "lastChar":Ljava/lang/String;
    .end local v2    # "queryText":Ljava/lang/String;
    :sswitch_2
    const/4 v4, -0x1

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->moveToEnd(I)Z

    goto/16 :goto_0

    .line 158
    :sswitch_3
    const/4 v4, -0x1

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->moveToNext(I)Z

    goto/16 :goto_0

    .line 161
    :sswitch_4
    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->moveToEnd(I)Z

    goto/16 :goto_0

    .line 164
    :sswitch_5
    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->moveToNext(I)Z

    goto/16 :goto_0

    .line 167
    :sswitch_6
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->resetHintTime()V

    .line 168
    iget-object v4, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    const-string v5, " "

    invoke-virtual {v4, v5}, Lcom/googlecode/eyesfree/utils/NodeSearch;->tryAddQueryText(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 169
    iget-object v4, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const v6, 0x7f060251

    invoke-virtual {v5, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v10, v9, v11}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 173
    :cond_5
    iget-object v4, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/CachedFeedbackController;

    move-result-object v4

    const v5, 0x7f050003

    invoke-virtual {v4, v5}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(I)Z

    goto/16 :goto_0

    .line 185
    .restart local v0    # "key":Ljava/lang/String;
    :cond_6
    iget-object v4, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/CachedFeedbackController;

    move-result-object v4

    const v5, 0x7f050003

    invoke-virtual {v4, v5}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(I)Z

    goto/16 :goto_0

    .end local v0    # "key":Ljava/lang/String;
    :cond_7
    move v3, v4

    .line 193
    goto/16 :goto_0

    .line 131
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_2
        0x14 -> :sswitch_4
        0x15 -> :sswitch_3
        0x16 -> :sswitch_5
        0x3e -> :sswitch_6
        0x42 -> :sswitch_0
        0x43 -> :sswitch_1
    .end sparse-switch
.end method

.method public toggleSearch()V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/NodeSearch;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->cancelSearch()V

    .line 105
    :goto_0
    return-void

    .line 103
    :cond_0
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->startSearch()V

    goto :goto_0
.end method
