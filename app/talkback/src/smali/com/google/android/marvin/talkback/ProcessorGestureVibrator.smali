.class Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;
.super Ljava/lang/Object;
.source "ProcessorGestureVibrator.java"

# interfaces
.implements Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# instance fields
.field private final mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

.field private final mFeedbackRunnable:Ljava/lang/Runnable;

.field private final mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mHandler:Landroid/os/Handler;

    .line 65
    new-instance v0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator$1;-><init>(Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mFeedbackRunnable:Ljava/lang/Runnable;

    .line 47
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getInstance()Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/CachedFeedbackController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;)Lcom/google/android/marvin/talkback/CachedFeedbackController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    return-object v0
.end method


# virtual methods
.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 52
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 61
    :goto_0
    return-void

    .line 54
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mFeedbackRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x46

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 57
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mFeedbackRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->interrupt()V

    goto :goto_0

    .line 52
    nop

    :sswitch_data_0
    .sparse-switch
        0x40000 -> :sswitch_0
        0x80000 -> :sswitch_1
    .end sparse-switch
.end method
