.class Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;
.super Lcom/google/android/marvin/talkback/tutorial/TutorialModule;
.source "TouchTutorialModule2.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

.field private final mAppsAdapter:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

.field private final mListItemFocusDelegate:Landroid/view/View$AccessibilityDelegate;

.field private final mScrollBackwardListViewListener:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView$ListViewListener;

.field private final mScrollFowardListViewListener:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView$ListViewListener;

.field private final mSecondListItemFocusDelegate:Landroid/view/View$AccessibilityDelegate;

.field private final mThirdListItemFocusDelegate:Landroid/view/View$AccessibilityDelegate;

.field private final mViewScrolledDelegate:Landroid/view/View$AccessibilityDelegate;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V
    .locals 6
    .param p1, "parentTutorial"    # Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 160
    const v0, 0x7f03000e

    const v1, 0x7f0601c6

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;II)V

    .line 47
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mListItemFocusDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 68
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$2;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$2;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mViewScrolledDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 89
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$3;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$3;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mSecondListItemFocusDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 107
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$4;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$4;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mScrollFowardListViewListener:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView$ListViewListener;

    .line 126
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$5;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$5;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mThirdListItemFocusDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 144
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$6;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$6;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mScrollBackwardListViewListener:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView$ListViewListener;

    .line 163
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$7;

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x1090003

    const v3, 0x1020014

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$7;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAppsAdapter:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    .line 172
    const v0, 0x7f0d0094

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    .line 173
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAppsAdapter:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 175
    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->setSkipVisible(Z)V

    .line 176
    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->setBackVisible(Z)V

    .line 177
    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->setNextVisible(Z)V

    .line 178
    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->setFinishVisible(Z)V

    .line 179
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->onTrigger1()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->onTrigger2()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->onTrigger3()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->onTrigger4()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->onTrigger5()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->onTrigger6()V

    return-void
.end method

.method private onTrigger0()V
    .locals 3

    .prologue
    .line 211
    const v0, 0x7f0601c7

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 214
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mListItemFocusDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 215
    return-void
.end method

.method private onTrigger1()V
    .locals 3

    .prologue
    .line 218
    const v0, 0x7f0601c8

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 221
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mViewScrolledDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 222
    return-void
.end method

.method private onTrigger2()V
    .locals 3

    .prologue
    .line 225
    const v0, 0x7f0601c9

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 228
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mSecondListItemFocusDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 229
    return-void
.end method

.method private onTrigger3()V
    .locals 3

    .prologue
    .line 232
    const v0, 0x7f0601ca

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 235
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mScrollFowardListViewListener:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView$ListViewListener;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;->setInstrumentation(Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView$ListViewListener;)V

    .line 236
    return-void
.end method

.method private onTrigger4()V
    .locals 3

    .prologue
    .line 239
    const v0, 0x7f0601cb

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 242
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mThirdListItemFocusDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 243
    return-void
.end method

.method private onTrigger5()V
    .locals 3

    .prologue
    .line 246
    const v0, 0x7f0601cc

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mScrollBackwardListViewListener:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView$ListViewListener;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;->setInstrumentation(Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView$ListViewListener;)V

    .line 250
    return-void
.end method

.method private onTrigger6()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 254
    const v0, 0x7f0601cd

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0601b9

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v5, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 256
    return-void
.end method


# virtual methods
.method public onPause()V
    .locals 0

    .prologue
    .line 193
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 198
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 183
    invoke-super {p0}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->onStart()V

    .line 185
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->setAllowContextMenus(Z)V

    .line 187
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->onTrigger0()V

    .line 188
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 202
    invoke-super {p0}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->onStop()V

    .line 204
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 205
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;->setInstrumentation(Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView$ListViewListener;)V

    .line 207
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->setAllowContextMenus(Z)V

    .line 208
    return-void
.end method
