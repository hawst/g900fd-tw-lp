.class Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;
.super Ljava/lang/Object;
.source "RuleGranularity.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/menurules/RuleGranularity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GranularityMenuItemClickListener"
.end annotation


# instance fields
.field private final mCursorController:Lcom/google/android/marvin/talkback/CursorController;

.field private final mHasWebContent:Z

.field private final mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)V
    .locals 1
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3, "hasWebContent"    # Z

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    .line 115
    invoke-static {p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 116
    iput-boolean p3, p0, Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;->mHasWebContent:Z

    .line 117
    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 122
    if-nez p1, :cond_0

    .line 153
    iget-object v3, p0, Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    :goto_0
    return v2

    .line 126
    :cond_0
    :try_start_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 128
    .local v1, "itemId":I
    const v4, 0x7f0d0025

    if-ne v1, v4, :cond_1

    .line 133
    iget-object v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    sget-object v4, Lcom/google/android/marvin/talkback/CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularity;

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/google/android/marvin/talkback/CursorController;->setGranularity(Lcom/google/android/marvin/talkback/CursorGranularity;Z)Z

    .line 135
    iget-object v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v4, 0x1

    invoke-static {v2, v4}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->setSpecialContentModeEnabled(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    iget-object v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    move v2, v3

    goto :goto_0

    .line 139
    :cond_1
    :try_start_1
    invoke-static {v1}, Lcom/google/android/marvin/talkback/CursorGranularity;->fromResourceId(I)Lcom/google/android/marvin/talkback/CursorGranularity;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 140
    .local v0, "granularity":Lcom/google/android/marvin/talkback/CursorGranularity;
    if-nez v0, :cond_2

    .line 153
    iget-object v3, p0, Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    .line 142
    :cond_2
    :try_start_2
    iget-boolean v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;->mHasWebContent:Z

    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/android/marvin/talkback/CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularity;

    if-ne v0, v2, :cond_3

    .line 148
    iget-object v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->setSpecialContentModeEnabled(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)Z

    .line 151
    :cond_3
    iget-object v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lcom/google/android/marvin/talkback/CursorController;->setGranularity(Lcom/google/android/marvin/talkback/CursorGranularity;Z)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    .line 153
    iget-object v3, p0, Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    .end local v0    # "granularity":Lcom/google/android/marvin/talkback/CursorGranularity;
    .end local v1    # "itemId":I
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    throw v2
.end method
