.class Lcom/google/android/marvin/talkback/TalkBackService$8;
.super Landroid/database/ContentObserver;
.source "TalkBackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/TalkBackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackService;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 1618
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackService$8;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 4
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1622
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackService$8;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1623
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v3, "touch_exploration_enabled"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_0

    .line 1625
    .local v1, "touchExplorationEnabled":Z
    :goto_0
    if-nez v1, :cond_1

    .line 1632
    :goto_1
    return-void

    .end local v1    # "touchExplorationEnabled":Z
    :cond_0
    move v1, v2

    .line 1623
    goto :goto_0

    .line 1629
    .restart local v1    # "touchExplorationEnabled":Z
    :cond_1
    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1631
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService$8;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # invokes: Lcom/google/android/marvin/talkback/TalkBackService;->onTouchExplorationEnabled()V
    invoke-static {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->access$600(Lcom/google/android/marvin/talkback/TalkBackService;)V

    goto :goto_1
.end method
