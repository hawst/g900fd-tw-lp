.class Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$4;
.super Ljava/lang/Object;
.source "TalkBackPreferencesActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->createDisableExploreByTouchDialog()Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V
    .locals 0

    .prologue
    .line 648
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$4;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v3, 0x0

    .line 651
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$4;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mExploreByTouchDialog:Landroid/app/AlertDialog;
    invoke-static {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->access$002(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 652
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$4;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    # invokes: Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->setTouchExplorationRequested(Z)Z
    invoke-static {v1, v3}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->access$100(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 655
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$4;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    const v2, 0x7f060036

    # invokes: Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;
    invoke-static {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->access$200(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 658
    .local v0, "prefTouchExploration":Landroid/preference/CheckBoxPreference;
    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 660
    .end local v0    # "prefTouchExploration":Landroid/preference/CheckBoxPreference;
    :cond_0
    return-void
.end method
