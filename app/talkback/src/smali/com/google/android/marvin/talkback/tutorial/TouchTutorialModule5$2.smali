.class Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$2;
.super Ljava/lang/Object;
.source "TouchTutorialModule5.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$2;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 76
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$2;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->access$000(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)Landroid/widget/EditText;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$2;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->access$100(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$2;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->access$000(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$2;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$2$1;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$2$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$2;)V

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->installTriggerDelayedWithFeedback(Ljava/lang/Runnable;)V

    .line 87
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
