.class public Lcom/google/android/marvin/talkback/menurules/RuleViewPager;
.super Ljava/lang/Object;
.source "RuleViewPager.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/menurules/NodeMenuRule;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/menurules/RuleViewPager$ViewPagerItemClickListener;
    }
.end annotation


# static fields
.field private static final FILTER_PAGED:Lcom/googlecode/eyesfree/utils/NodeFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/marvin/talkback/menurules/RuleViewPager$1;

    invoke-direct {v0}, Lcom/google/android/marvin/talkback/menurules/RuleViewPager$1;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/menurules/RuleViewPager;->FILTER_PAGED:Lcom/googlecode/eyesfree/utils/NodeFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    return-void
.end method


# virtual methods
.method public accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36
    const/4 v1, 0x0

    .line 37
    .local v1, "rootNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v0, 0x0

    .line 40
    .local v0, "pagerNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_start_0
    invoke-static {p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getRoot(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 41
    if-nez v1, :cond_0

    .line 52
    new-array v4, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v4, v2

    aput-object v0, v4, v3

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    :goto_0
    return v2

    .line 45
    :cond_0
    :try_start_1
    sget-object v4, Lcom/google/android/marvin/talkback/menurules/RuleViewPager;->FILTER_PAGED:Lcom/googlecode/eyesfree/utils/NodeFilter;

    invoke-static {p1, v1, v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->searchFromBfs(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 46
    if-nez v0, :cond_1

    .line 52
    new-array v4, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v4, v2

    aput-object v0, v4, v3

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    :cond_1
    new-array v4, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v4, v2

    aput-object v0, v4, v3

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v2, v3

    goto :goto_0

    :catchall_0
    move-exception v4

    new-array v5, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v5, v2

    aput-object v0, v5, v3

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v4
.end method

.method public canCollapseMenu()Z
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    return v0
.end method

.method public getMenuItemsForNode(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/List;
    .locals 14
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/marvin/talkback/TalkBackService;",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    new-instance v10, Ljava/util/LinkedList;

    invoke-direct {v10}, Ljava/util/LinkedList;-><init>()V

    .line 61
    .local v10, "items":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/googlecode/eyesfree/widget/RadialMenuItem;>;"
    const/4 v13, 0x0

    .line 62
    .local v13, "rootNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v11, 0x0

    .line 65
    .local v11, "pagerNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_start_0
    invoke-static/range {p2 .. p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getRoot(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    .line 66
    if-nez v13, :cond_0

    .line 105
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v3, 0x0

    aput-object v13, v2, v3

    const/4 v3, 0x1

    aput-object v11, v2, v3

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    :goto_0
    return-object v10

    .line 70
    :cond_0
    :try_start_1
    sget-object v2, Lcom/google/android/marvin/talkback/menurules/RuleViewPager;->FILTER_PAGED:Lcom/googlecode/eyesfree/utils/NodeFilter;

    invoke-static {p1, v13, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->searchFromBfs(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v11

    .line 71
    if-nez v11, :cond_1

    .line 105
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v3, 0x0

    aput-object v13, v2, v3

    const/4 v3, 0x1

    aput-object v11, v2, v3

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 75
    :cond_1
    const/4 v2, 0x1

    :try_start_2
    new-array v2, v2, [I

    const/4 v3, 0x0

    const/16 v4, 0x2000

    aput v4, v2, v3

    invoke-static {v11, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 77
    new-instance v0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v2, 0x0

    const v3, 0x7f0d0002

    const/4 v4, 0x0

    const v5, 0x7f06013e

    invoke-virtual {p1, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 80
    .local v0, "prevPage":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-virtual {v10, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 83
    .end local v0    # "prevPage":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :cond_2
    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/16 v4, 0x1000

    aput v4, v2, v3

    invoke-static {v11, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 85
    new-instance v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v3, 0x0

    const v4, 0x7f0d0003

    const/4 v5, 0x0

    const v2, 0x7f06013d

    invoke-virtual {p1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 88
    .local v1, "nextPage":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-virtual {v10, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 91
    .end local v1    # "nextPage":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :cond_3
    invoke-virtual {v10}, Ljava/util/LinkedList;->isEmpty()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    if-eqz v2, :cond_4

    .line 105
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v3, 0x0

    aput-object v13, v2, v3

    const/4 v3, 0x1

    aput-object v11, v2, v3

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 95
    :cond_4
    :try_start_3
    invoke-static {v11}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v12

    .line 97
    .local v12, "pagerNodeClone":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    new-instance v9, Lcom/google/android/marvin/talkback/menurules/RuleViewPager$ViewPagerItemClickListener;

    invoke-direct {v9, v12}, Lcom/google/android/marvin/talkback/menurules/RuleViewPager$ViewPagerItemClickListener;-><init>(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 99
    .local v9, "itemClickListener":Lcom/google/android/marvin/talkback/menurules/RuleViewPager$ViewPagerItemClickListener;
    invoke-virtual {v10}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/MenuItem;

    .line 100
    .local v8, "item":Landroid/view/MenuItem;
    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 105
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "item":Landroid/view/MenuItem;
    .end local v9    # "itemClickListener":Lcom/google/android/marvin/talkback/menurules/RuleViewPager$ViewPagerItemClickListener;
    .end local v12    # "pagerNodeClone":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :catchall_0
    move-exception v2

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v4, 0x0

    aput-object v13, v3, v4

    const/4 v4, 0x1

    aput-object v11, v3, v4

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v2

    .restart local v7    # "i$":Ljava/util/Iterator;
    .restart local v9    # "itemClickListener":Lcom/google/android/marvin/talkback/menurules/RuleViewPager$ViewPagerItemClickListener;
    .restart local v12    # "pagerNodeClone":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_5
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v3, 0x0

    aput-object v13, v2, v3

    const/4 v3, 0x1

    aput-object v11, v2, v3

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto/16 :goto_0
.end method

.method public getUserFriendlyMenuName(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 111
    const v0, 0x7f06013c

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
