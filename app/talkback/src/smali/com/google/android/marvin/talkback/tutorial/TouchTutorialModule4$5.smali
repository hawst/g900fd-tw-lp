.class Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$5;
.super Ljava/lang/Object;
.source "TouchTutorialModule4.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$5;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGranularityChanged(Lcom/google/android/marvin/talkback/CursorGranularity;)V
    .locals 2
    .param p1, "granularity"    # Lcom/google/android/marvin/talkback/CursorGranularity;

    .prologue
    .line 151
    sget-object v0, Lcom/google/android/marvin/talkback/CursorGranularity;->CHARACTER:Lcom/google/android/marvin/talkback/CursorGranularity;

    invoke-virtual {v0, p1}, Lcom/google/android/marvin/talkback/CursorGranularity;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$5;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mContextMenuMonitor:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->access$700(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->setListener(Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$5;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    # invokes: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->removeAllGranularityListeners()V
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->access$600(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$5;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$5$1;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$5$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$5;)V

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->installTriggerDelayedWithFeedback(Ljava/lang/Runnable;)V

    .line 161
    :cond_0
    return-void
.end method
