.class Lcom/google/android/marvin/talkback/TalkBackAnalytics$1;
.super Ljava/lang/Object;
.source "TalkBackAnalytics.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/marvin/talkback/TalkBackAnalytics;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackAnalytics;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackAnalytics;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics$1;->this$0:Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGranularityChanged(Lcom/google/android/marvin/talkback/CursorGranularity;)V
    .locals 1
    .param p1, "granularity"    # Lcom/google/android/marvin/talkback/CursorGranularity;

    .prologue
    .line 106
    sget-object v0, Lcom/google/android/marvin/talkback/CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularity;

    if-eq p1, v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics$1;->this$0:Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    # operator++ for: Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mNumGranularityChanges:I
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->access$008(Lcom/google/android/marvin/talkback/TalkBackAnalytics;)I

    .line 109
    :cond_0
    return-void
.end method
