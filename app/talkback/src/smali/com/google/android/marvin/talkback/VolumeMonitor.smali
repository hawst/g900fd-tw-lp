.class public Lcom/google/android/marvin/talkback/VolumeMonitor;
.super Landroid/content/BroadcastReceiver;
.source "VolumeMonitor.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;
    }
.end annotation


# static fields
.field private static final STREAM_NAMES:Landroid/util/SparseIntArray;


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private mCurrentStream:I

.field private final mHandler:Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;

.field private final mReleaseControl:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

.field private final mSelfAdjustments:Landroid/util/SparseIntArray;

.field private mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 47
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/VolumeMonitor;->STREAM_NAMES:Landroid/util/SparseIntArray;

    .line 50
    sget-object v0, Lcom/google/android/marvin/talkback/VolumeMonitor;->STREAM_NAMES:Landroid/util/SparseIntArray;

    const/16 v1, -0x64

    const v2, 0x7f0601a8

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 51
    sget-object v0, Lcom/google/android/marvin/talkback/VolumeMonitor;->STREAM_NAMES:Landroid/util/SparseIntArray;

    const/4 v1, 0x0

    const v2, 0x7f0601a9

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 52
    sget-object v0, Lcom/google/android/marvin/talkback/VolumeMonitor;->STREAM_NAMES:Landroid/util/SparseIntArray;

    const/4 v1, 0x1

    const v2, 0x7f0601aa

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 53
    sget-object v0, Lcom/google/android/marvin/talkback/VolumeMonitor;->STREAM_NAMES:Landroid/util/SparseIntArray;

    const/4 v1, 0x2

    const v2, 0x7f0601ab

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 54
    sget-object v0, Lcom/google/android/marvin/talkback/VolumeMonitor;->STREAM_NAMES:Landroid/util/SparseIntArray;

    const/4 v1, 0x3

    const v2, 0x7f0601ac

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 55
    sget-object v0, Lcom/google/android/marvin/talkback/VolumeMonitor;->STREAM_NAMES:Landroid/util/SparseIntArray;

    const/4 v1, 0x4

    const v2, 0x7f0601ad

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 56
    sget-object v0, Lcom/google/android/marvin/talkback/VolumeMonitor;->STREAM_NAMES:Landroid/util/SparseIntArray;

    const/4 v1, 0x5

    const v2, 0x7f0601ae

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 57
    sget-object v0, Lcom/google/android/marvin/talkback/VolumeMonitor;->STREAM_NAMES:Landroid/util/SparseIntArray;

    const/16 v1, 0x8

    const v2, 0x7f0601af

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 58
    return-void
.end method

.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 2
    .param p1, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 76
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 61
    new-instance v0, Landroid/util/SparseIntArray;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mSelfAdjustments:Landroid/util/SparseIntArray;

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mCurrentStream:I

    .line 313
    new-instance v0, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;-><init>(Lcom/google/android/marvin/talkback/VolumeMonitor;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mHandler:Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;

    .line 319
    new-instance v0, Lcom/google/android/marvin/talkback/VolumeMonitor$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/VolumeMonitor$1;-><init>(Lcom/google/android/marvin/talkback/VolumeMonitor;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mReleaseControl:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .line 77
    iput-object p1, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mContext:Landroid/content/Context;

    .line 78
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    .line 79
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mAudioManager:Landroid/media/AudioManager;

    .line 80
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/VolumeMonitor;III)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/VolumeMonitor;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/VolumeMonitor;->internalOnVolumeChanged(III)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/VolumeMonitor;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/VolumeMonitor;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/VolumeMonitor;->internalOnControlAcquired(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/VolumeMonitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/VolumeMonitor;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/VolumeMonitor;->internalOnReleaseControl()V

    return-void
.end method

.method private getAnnouncementForStreamType(II)Ljava/lang/String;
    .locals 6
    .param p1, "templateResId"    # I
    .param p2, "streamType"    # I

    .prologue
    const/4 v3, 0x2

    .line 155
    if-ne p2, v3, :cond_0

    .line 156
    iget-object v2, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 164
    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/marvin/talkback/VolumeMonitor;->getStreamName(I)Ljava/lang/String;

    move-result-object v0

    .line 165
    .local v0, "streamName":Ljava/lang/String;
    invoke-direct {p0, p2}, Lcom/google/android/marvin/talkback/VolumeMonitor;->getStreamVolume(I)I

    move-result v1

    .line 167
    .local v1, "volume":I
    iget-object v2, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mContext:Landroid/content/Context;

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, p1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .end local v0    # "streamName":Ljava/lang/String;
    .end local v1    # "volume":I
    :goto_0
    return-object v2

    .line 158
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mContext:Landroid/content/Context;

    const v3, 0x7f0600d6

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 160
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mContext:Landroid/content/Context;

    const v3, 0x7f0600d7

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 156
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getStreamName(I)Ljava/lang/String;
    .locals 2
    .param p1, "streamType"    # I

    .prologue
    .line 259
    sget-object v1, Lcom/google/android/marvin/talkback/VolumeMonitor;->STREAM_NAMES:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 260
    .local v0, "resId":I
    if-gtz v0, :cond_0

    .line 261
    const-string v1, ""

    .line 264
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getStreamVolume(I)I
    .locals 8
    .param p1, "streamType"    # I

    .prologue
    .line 306
    iget-object v3, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3, p1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    .line 307
    .local v0, "currentVolume":I
    iget-object v3, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3, p1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    .line 308
    .local v1, "maxVolume":I
    mul-int/lit8 v3, v0, 0x14

    div-int/2addr v3, v1

    int-to-double v4, v3

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    add-double/2addr v4, v6

    double-to-int v3, v4

    mul-int/lit8 v2, v3, 0x5

    .line 310
    .local v2, "volumePercent":I
    return v2
.end method

.method private internalOnControlAcquired(I)V
    .locals 5
    .param p1, "streamType"    # I

    .prologue
    .line 142
    const/4 v0, 0x2

    const-string v1, "Acquired control of stream %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 144
    iget-object v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mHandler:Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->releaseControlDelayed()V

    .line 145
    return-void
.end method

.method private internalOnReleaseControl()V
    .locals 7

    .prologue
    .line 176
    iget-object v2, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mHandler:Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->clearReleaseControl()V

    .line 178
    iget v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mCurrentStream:I

    .line 179
    .local v0, "streamType":I
    if-gez v0, :cond_0

    .line 196
    :goto_0
    return-void

    .line 184
    :cond_0
    const/4 v2, 0x2

    const-string v3, "Released control of stream %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mCurrentStream:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 186
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/VolumeMonitor;->shouldAnnounceStream(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 187
    iget-object v2, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mHandler:Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;

    new-instance v3, Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;

    iget-object v4, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mReleaseControl:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    const/4 v5, 0x3

    invoke-direct {v3, v4, v5}, Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;-><init>(Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;I)V

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 192
    :cond_1
    const v2, 0x7f0601a7

    invoke-direct {p0, v2, v0}, Lcom/google/android/marvin/talkback/VolumeMonitor;->getAnnouncementForStreamType(II)Ljava/lang/String;

    move-result-object v1

    .line 195
    .local v1, "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mReleaseControl:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    invoke-direct {p0, v1, v2}, Lcom/google/android/marvin/talkback/VolumeMonitor;->speakWithCompletion(Ljava/lang/String;Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    goto :goto_0
.end method

.method private internalOnVolumeChanged(III)V
    .locals 2
    .param p1, "streamType"    # I
    .param p2, "volume"    # I
    .param p3, "prevVolume"    # I

    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Lcom/google/android/marvin/talkback/VolumeMonitor;->isSelfAdjusted(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    iget v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mCurrentStream:I

    if-gez v0, :cond_2

    .line 121
    iput p1, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mCurrentStream:I

    .line 122
    iget-object v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mAudioManager:Landroid/media/AudioManager;

    iget v1, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mCurrentStream:I

    invoke-static {v0, v1}, Lcom/googlecode/eyesfree/compat/media/AudioManagerCompatUtils;->forceVolumeControlStream(Landroid/media/AudioManager;I)V

    .line 123
    iget-object v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mHandler:Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;

    invoke-virtual {v0, p1}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->onControlAcquired(I)V

    goto :goto_0

    .line 127
    :cond_2
    if-eq p2, p3, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mHandler:Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->releaseControlDelayed()V

    goto :goto_0
.end method

.method private isSelfAdjusted(II)Z
    .locals 2
    .param p1, "streamType"    # I
    .param p2, "volume"    # I

    .prologue
    const/4 v0, 0x0

    .line 95
    iget-object v1, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mSelfAdjustments:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v1

    if-gez v1, :cond_1

    .line 102
    :cond_0
    :goto_0
    return v0

    .line 97
    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mSelfAdjustments:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    if-ne v1, p2, :cond_0

    .line 98
    iget-object v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mSelfAdjustments:Landroid/util/SparseIntArray;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 99
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private shouldAnnounceStream(I)Z
    .locals 3
    .param p1, "streamType"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 213
    packed-switch p1, :pswitch_data_0

    .line 226
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 216
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 221
    goto :goto_0

    .line 213
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private speakWithCompletion(Ljava/lang/String;Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V
    .locals 9
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "completedAction"    # Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .prologue
    const/4 v2, 0x0

    .line 239
    iget-object v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mHandler:Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;

    new-instance v1, Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;

    const/4 v2, 0x3

    invoke-direct {v1, p2, v2}, Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;-><init>(Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->post(Ljava/lang/Runnable;)Z

    .line 250
    :goto_0
    return-void

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v1, p1

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, p2

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;Ljava/util/Set;Ljava/util/Set;IILandroid/os/Bundle;Landroid/os/Bundle;Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    goto :goto_0
.end method


# virtual methods
.method public getFilter()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 84
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 85
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 86
    const-string v1, "android.media.MASTER_VOLUME_CHANGED_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 87
    return-object v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, -0x1

    .line 269
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 271
    .local v0, "action":Ljava/lang/String;
    const-string v4, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 272
    const-string v4, "android.media.EXTRA_VOLUME_STREAM_TYPE"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 274
    .local v2, "type":I
    const-string v4, "android.media.EXTRA_VOLUME_STREAM_VALUE"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 276
    .local v3, "value":I
    const-string v4, "android.media.EXTRA_PREV_VOLUME_STREAM_VALUE"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 279
    .local v1, "prevValue":I
    if-ltz v2, :cond_0

    if-ltz v3, :cond_0

    if-gez v1, :cond_1

    .line 296
    .end local v1    # "prevValue":I
    .end local v2    # "type":I
    .end local v3    # "value":I
    :cond_0
    :goto_0
    return-void

    .line 283
    .restart local v1    # "prevValue":I
    .restart local v2    # "type":I
    .restart local v3    # "value":I
    :cond_1
    iget-object v4, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mHandler:Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;

    invoke-virtual {v4, v2, v3, v1}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->onVolumeChanged(III)V

    goto :goto_0

    .line 284
    .end local v1    # "prevValue":I
    .end local v2    # "type":I
    .end local v3    # "value":I
    :cond_2
    const-string v4, "android.media.MASTER_VOLUME_CHANGED_ACTION"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 285
    const-string v4, "android.media.EXTRA_MASTER_VOLUME_VALUE"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 287
    .restart local v3    # "value":I
    const-string v4, "android.media.EXTRA_PREV_MASTER_VOLUME_VALUE"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 290
    .restart local v1    # "prevValue":I
    if-ltz v3, :cond_0

    if-ltz v1, :cond_0

    .line 294
    iget-object v4, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mHandler:Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;

    const/16 v5, -0x64

    invoke-virtual {v4, v5, v3, v1}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->onVolumeChanged(III)V

    goto :goto_0
.end method

.method public releaseControl()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 202
    iput v1, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mCurrentStream:I

    .line 203
    iget-object v0, p0, Lcom/google/android/marvin/talkback/VolumeMonitor;->mAudioManager:Landroid/media/AudioManager;

    invoke-static {v0, v1}, Lcom/googlecode/eyesfree/compat/media/AudioManagerCompatUtils;->forceVolumeControlStream(Landroid/media/AudioManager;I)V

    .line 204
    return-void
.end method
