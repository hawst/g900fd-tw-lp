.class public Lcom/google/android/marvin/utils/FailoverTextToSpeech;
.super Ljava/lang/Object;
.source "FailoverTextToSpeech.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;,
        Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;,
        Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;
    }
.end annotation


# static fields
.field private static final FORCE_TTS_LOCALE_CHANGES:Z

.field private static final PREFERRED_FALLBACK_LOCALE:Ljava/util/Locale;

.field private static final USE_GOOGLE_TTS_WORKAROUNDS:Z


# instance fields
.field private final mComponentCallbacks:Landroid/content/ComponentCallbacks;

.field private final mContext:Landroid/content/Context;

.field private mCurrentPitch:F

.field private mCurrentRate:F

.field private mDefaultLocale:Ljava/util/Locale;

.field private mDefaultPitch:F

.field private mDefaultRate:F

.field private mDefaultTtsEngine:Ljava/lang/String;

.field private final mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

.field private mHasSetLocale:Z

.field private final mInstalledTtsEngines:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mLocaleObserver:Landroid/database/ContentObserver;

.field private final mMediaStateMonitor:Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;

.field private final mPitchObserver:Landroid/database/ContentObserver;

.field private final mRateObserver:Landroid/database/ContentObserver;

.field private final mResolver:Landroid/content/ContentResolver;

.field private final mSynthObserver:Landroid/database/ContentObserver;

.field private mSystemLocale:Ljava/util/Locale;

.field private mSystemTtsEngine:Ljava/lang/String;

.field private mTempTts:Landroid/speech/tts/TextToSpeech;

.field private mTempTtsEngine:Ljava/lang/String;

.field private mTts:Landroid/speech/tts/TextToSpeech;

.field private final mTtsChangeListener:Landroid/speech/tts/TextToSpeech$OnInitListener;

.field private mTtsEngine:Ljava/lang/String;

.field private mTtsFailures:I

.field private final mTtsListener:Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;

.field private mUsingFallbackLocale:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 538
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xf

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->USE_GOOGLE_TTS_WORKAROUNDS:Z

    .line 542
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->FORCE_TTS_LOCALE_CHANGES:Z

    .line 550
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    sput-object v0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->PREFERRED_FALLBACK_LOCALE:Ljava/util/Locale;

    return-void

    :cond_0
    move v0, v2

    .line 538
    goto :goto_0

    :cond_1
    move v1, v2

    .line 542
    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v3, Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;

    invoke-direct {v3, p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;-><init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mMediaStateMonitor:Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;

    .line 81
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mInstalledTtsEngines:Ljava/util/LinkedList;

    .line 114
    iput v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mCurrentRate:F

    .line 117
    iput v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mCurrentPitch:F

    .line 119
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mListeners:Ljava/util/List;

    .line 557
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSystemLocale:Ljava/util/Locale;

    .line 565
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultLocale:Ljava/util/Locale;

    .line 830
    new-instance v3, Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    invoke-direct {v3, p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;-><init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    .line 835
    new-instance v3, Lcom/google/android/marvin/utils/FailoverTextToSpeech$1;

    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    invoke-direct {v3, p0, v4}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$1;-><init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;Landroid/os/Handler;)V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSynthObserver:Landroid/database/ContentObserver;

    .line 842
    new-instance v3, Lcom/google/android/marvin/utils/FailoverTextToSpeech$2;

    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    invoke-direct {v3, p0, v4}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$2;-><init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;Landroid/os/Handler;)V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mPitchObserver:Landroid/database/ContentObserver;

    .line 849
    new-instance v3, Lcom/google/android/marvin/utils/FailoverTextToSpeech$3;

    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    invoke-direct {v3, p0, v4}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$3;-><init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;Landroid/os/Handler;)V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mRateObserver:Landroid/database/ContentObserver;

    .line 861
    new-instance v3, Lcom/google/android/marvin/utils/FailoverTextToSpeech$4;

    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    invoke-direct {v3, p0, v4}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$4;-><init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;Landroid/os/Handler;)V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mLocaleObserver:Landroid/database/ContentObserver;

    .line 869
    new-instance v3, Lcom/google/android/marvin/utils/FailoverTextToSpeech$5;

    invoke-direct {v3, p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$5;-><init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsListener:Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;

    .line 882
    new-instance v3, Lcom/google/android/marvin/utils/FailoverTextToSpeech$6;

    invoke-direct {v3, p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$6;-><init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsChangeListener:Landroid/speech/tts/TextToSpeech$OnInitListener;

    .line 894
    new-instance v3, Lcom/google/android/marvin/utils/FailoverTextToSpeech$7;

    invoke-direct {v3, p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$7;-><init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mComponentCallbacks:Landroid/content/ComponentCallbacks;

    .line 122
    iput-object p1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mContext:Landroid/content/Context;

    .line 123
    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mMediaStateMonitor:Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;

    iget-object v5, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mMediaStateMonitor:Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;

    invoke-virtual {v5}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;->getFilter()Landroid/content/IntentFilter;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 125
    const-string v3, "tts_default_synth"

    invoke-static {v3}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 126
    .local v2, "defaultSynth":Landroid/net/Uri;
    const-string v3, "tts_default_pitch"

    invoke-static {v3}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 127
    .local v0, "defaultPitch":Landroid/net/Uri;
    const-string v3, "tts_default_rate"

    invoke-static {v3}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 129
    .local v1, "defaultRate":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    .line 130
    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSynthObserver:Landroid/database/ContentObserver;

    invoke-virtual {v3, v2, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 131
    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mPitchObserver:Landroid/database/ContentObserver;

    invoke-virtual {v3, v0, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 132
    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mRateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v3, v1, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 134
    sget-boolean v3, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->USE_GOOGLE_TTS_WORKAROUNDS:Z

    if-eqz v3, :cond_0

    .line 135
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->registerGoogleTtsFixCallbacks()V

    .line 138
    :cond_0
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->updateDefaultPitch()V

    .line 139
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->updateDefaultRate()V

    .line 143
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->updateDefaultEngine()V

    .line 144
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->updateDefaultEngine()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->updateDefaultPitch()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->updateDefaultRate()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->updateDefaultLocale()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/marvin/utils/FailoverTextToSpeech;Landroid/content/res/Configuration;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;
    .param p1, "x1"    # Landroid/content/res/Configuration;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/marvin/utils/FailoverTextToSpeech;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;
    .param p1, "x1"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->handleTtsInitialized(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/marvin/utils/FailoverTextToSpeech;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->handleUtteranceCompleted(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/marvin/utils/FailoverTextToSpeech;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->handleMediaStateChanged(Ljava/lang/String;)V

    return-void
.end method

.method private attemptRestorePreferredLocale()V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 711
    iget-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    if-nez v2, :cond_0

    .line 726
    :goto_0
    return-void

    .line 715
    :cond_0
    iget-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultLocale:Ljava/util/Locale;

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultLocale:Ljava/util/Locale;

    .line 716
    .local v0, "preferredLocale":Ljava/util/Locale;
    :goto_1
    iget-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v2, v0}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    move-result v1

    .line 717
    .local v1, "status":I
    invoke-static {v1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->isAvailableStatus(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 718
    const/4 v2, 0x6

    const-string v3, "Failed to restore TTS locale to %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 715
    .end local v0    # "preferredLocale":Ljava/util/Locale;
    .end local v1    # "status":I
    :cond_1
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSystemLocale:Ljava/util/Locale;

    goto :goto_1

    .line 722
    .restart local v0    # "preferredLocale":Ljava/util/Locale;
    .restart local v1    # "status":I
    :cond_2
    const/4 v2, 0x4

    const-string v3, "Restored TTS locale to %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 724
    iput-boolean v5, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mUsingFallbackLocale:Z

    .line 725
    iput-boolean v6, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHasSetLocale:Z

    goto :goto_0
.end method

.method private attemptSetFallbackLanguage()V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .prologue
    const/4 v4, 0x6

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 639
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->getBestAvailableLocale()Ljava/util/Locale;

    move-result-object v0

    .line 640
    .local v0, "fallbackLocale":Ljava/util/Locale;
    if-nez v0, :cond_0

    .line 641
    const-string v2, "Failed to find fallback locale"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p0, v4, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 660
    :goto_0
    return-void

    .line 645
    :cond_0
    iget-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    if-nez v2, :cond_1

    .line 646
    const-string v2, "mTts null when setting fallback locale."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p0, v4, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 650
    :cond_1
    iget-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v2, v0}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    move-result v1

    .line 651
    .local v1, "status":I
    invoke-static {v1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->isAvailableStatus(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 652
    const-string v2, "Failed to set fallback locale to %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {p0, v4, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 656
    :cond_2
    const/4 v2, 0x2

    const-string v3, "Set fallback locale to %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 658
    iput-boolean v6, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mUsingFallbackLocale:Z

    .line 659
    iput-boolean v6, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHasSetLocale:Z

    goto :goto_0
.end method

.method private attemptTtsFailover(Ljava/lang/String;)V
    .locals 7
    .param p1, "failedEngine"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 378
    const-class v1, Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v2, 0x6

    const-string v3, "Attempting TTS failover from %s"

    new-array v4, v5, [Ljava/lang/Object;

    aput-object p1, v4, v6

    invoke-static {v1, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 381
    iget v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsFailures:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsFailures:I

    .line 385
    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mInstalledTtsEngines:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-le v1, v5, :cond_0

    iget v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsFailures:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_1

    .line 386
    :cond_0
    invoke-direct {p0, p1, v6}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->setTtsEngine(Ljava/lang/String;Z)V

    .line 400
    :goto_0
    return-void

    .line 391
    :cond_1
    if-eqz p1, :cond_2

    .line 392
    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mInstalledTtsEngines:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 393
    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mInstalledTtsEngines:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 397
    :cond_2
    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mInstalledTtsEngines:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 399
    .local v0, "nextEngine":Ljava/lang/String;
    invoke-direct {p0, v0, v5}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->setTtsEngine(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private static compareLocales(Ljava/util/Locale;Ljava/util/Locale;)I
    .locals 4
    .param p0, "primary"    # Ljava/util/Locale;
    .param p1, "other"    # Ljava/util/Locale;

    .prologue
    .line 797
    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 798
    .local v1, "lang":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 799
    :cond_0
    const/4 v3, 0x0

    .line 812
    :goto_0
    return v3

    .line 802
    :cond_1
    invoke-virtual {p0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 803
    .local v0, "country":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 804
    :cond_2
    const/4 v3, 0x1

    goto :goto_0

    .line 807
    :cond_3
    invoke-virtual {p0}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v2

    .line 808
    .local v2, "variant":Ljava/lang/String;
    if-eqz v2, :cond_4

    invoke-virtual {p1}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 809
    :cond_4
    const/4 v3, 0x2

    goto :goto_0

    .line 812
    :cond_5
    const/4 v3, 0x3

    goto :goto_0
.end method

.method private ensureSupportedLocale()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .prologue
    .line 595
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->needsFallbackLocale()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 596
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->attemptSetFallbackLanguage()V

    .line 604
    :cond_0
    :goto_0
    return-void

    .line 597
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mUsingFallbackLocale:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHasSetLocale:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->FORCE_TTS_LOCALE_CHANGES:Z

    if-eqz v0, :cond_0

    .line 602
    :cond_2
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->attemptRestorePreferredLocale()V

    goto :goto_0
.end method

.method private getBestAvailableLocale()Ljava/util/Locale;
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .prologue
    .line 672
    iget-object v9, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    if-nez v9, :cond_1

    .line 673
    const/4 v1, 0x0

    .line 700
    :cond_0
    :goto_0
    return-object v1

    .line 677
    :cond_1
    iget-object v9, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    sget-object v10, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->PREFERRED_FALLBACK_LOCALE:Ljava/util/Locale;

    invoke-virtual {v9, v10}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v9

    if-ltz v9, :cond_2

    .line 678
    sget-object v1, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->PREFERRED_FALLBACK_LOCALE:Ljava/util/Locale;

    goto :goto_0

    .line 683
    :cond_2
    const/4 v1, 0x0

    .line 684
    .local v1, "bestLocale":Ljava/util/Locale;
    const/4 v2, -0x1

    .line 686
    .local v2, "bestScore":I
    invoke-static {}, Ljava/util/Locale;->getAvailableLocales()[Ljava/util/Locale;

    move-result-object v6

    .line 687
    .local v6, "locales":[Ljava/util/Locale;
    move-object v0, v6

    .local v0, "arr$":[Ljava/util/Locale;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    .line 688
    .local v5, "locale":Ljava/util/Locale;
    iget-object v9, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9, v5}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v8

    .line 689
    .local v8, "status":I
    invoke-static {v8}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->isAvailableStatus(I)Z

    move-result v9

    if-nez v9, :cond_4

    .line 687
    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 693
    :cond_4
    iget-object v9, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSystemLocale:Ljava/util/Locale;

    invoke-static {v9, v5}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->compareLocales(Ljava/util/Locale;Ljava/util/Locale;)I

    move-result v7

    .line 694
    .local v7, "score":I
    if-le v7, v2, :cond_3

    .line 695
    move-object v1, v5

    .line 696
    move v2, v7

    goto :goto_2
.end method

.method private handleMediaStateChanged(Ljava/lang/String;)V
    .locals 5
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 488
    const-string v0, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSystemTtsEngine:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsEngine:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 491
    const-string v0, "Saw media unmount"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v4, v0, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 492
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSystemTtsEngine:Ljava/lang/String;

    invoke-direct {p0, v0, v3}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->setTtsEngine(Ljava/lang/String;Z)V

    .line 496
    :cond_0
    const-string v0, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 497
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultTtsEngine:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsEngine:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 499
    const-string v0, "Saw media mount"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v4, v0, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 500
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultTtsEngine:Ljava/lang/String;

    invoke-direct {p0, v0, v3}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->setTtsEngine(Ljava/lang/String;Z)V

    .line 503
    :cond_1
    return-void
.end method

.method private handleTtsInitialized(I)V
    .locals 10
    .param p1, "status"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 409
    iget-object v7, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTts:Landroid/speech/tts/TextToSpeech;

    if-nez v7, :cond_1

    .line 410
    const/4 v5, 0x6

    const-string v7, "Attempted to initialize TTS more than once!"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p0, v5, v7, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 453
    :cond_0
    :goto_0
    return-void

    .line 414
    :cond_1
    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTts:Landroid/speech/tts/TextToSpeech;

    .line 415
    .local v3, "tempTts":Landroid/speech/tts/TextToSpeech;
    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTtsEngine:Ljava/lang/String;

    .line 417
    .local v4, "tempTtsEngine":Ljava/lang/String;
    iput-object v8, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTts:Landroid/speech/tts/TextToSpeech;

    .line 418
    iput-object v8, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTtsEngine:Ljava/lang/String;

    .line 420
    if-eqz p1, :cond_2

    .line 421
    invoke-direct {p0, v4}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->attemptTtsFailover(Ljava/lang/String;)V

    goto :goto_0

    .line 425
    :cond_2
    iget-object v7, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v7, :cond_6

    move v1, v5

    .line 427
    .local v1, "isSwitchingEngines":Z
    :goto_1
    if-eqz v1, :cond_3

    .line 428
    iget-object v7, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-static {v7}, Lcom/google/android/marvin/utils/TextToSpeechUtils;->attemptTtsShutdown(Landroid/speech/tts/TextToSpeech;)V

    .line 431
    :cond_3
    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    .line 432
    iget-object v7, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    iget-object v8, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsListener:Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;

    invoke-virtual {v7, v8}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceCompletedListener(Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;)I

    .line 434
    if-nez v4, :cond_7

    .line 435
    iget-object v7, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-static {v7}, Lcom/googlecode/eyesfree/compat/speech/tts/TextToSpeechCompatUtils;->getCurrentEngine(Landroid/speech/tts/TextToSpeech;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsEngine:Ljava/lang/String;

    .line 440
    :goto_2
    sget-boolean v7, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->USE_GOOGLE_TTS_WORKAROUNDS:Z

    if-eqz v7, :cond_4

    .line 441
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->updateDefaultLocale()V

    .line 444
    :cond_4
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x14

    if-le v7, v8, :cond_5

    .line 445
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->setAudioAttributesApi21()V

    .line 448
    :cond_5
    const-class v7, Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v8, 0x4

    const-string v9, "Switched to TTS engine: %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v4, v5, v6

    invoke-static {v7, v8, v9, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 450
    iget-object v5, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mListeners:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

    .line 451
    .local v2, "mListener":Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;
    invoke-interface {v2, v1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;->onTtsInitialized(Z)V

    goto :goto_3

    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "isSwitchingEngines":Z
    .end local v2    # "mListener":Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;
    :cond_6
    move v1, v6

    .line 425
    goto :goto_1

    .line 437
    .restart local v1    # "isSwitchingEngines":Z
    :cond_7
    iput-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsEngine:Ljava/lang/String;

    goto :goto_2
.end method

.method private handleUtteranceCompleted(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "utteranceId"    # Ljava/lang/String;
    .param p2, "success"    # Z

    .prologue
    .line 473
    if-eqz p2, :cond_0

    .line 474
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsFailures:I

    .line 477
    :cond_0
    iget-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

    .line 478
    .local v1, "mListener":Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;
    invoke-interface {v1, p1, p2}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;->onUtteranceCompleted(Ljava/lang/String;Z)V

    goto :goto_0

    .line 480
    .end local v1    # "mListener":Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;
    :cond_1
    return-void
.end method

.method private static isAvailableStatus(I)Z
    .locals 2
    .param p0, "status"    # I

    .prologue
    const/4 v0, 0x1

    .line 825
    if-eqz p0, :cond_0

    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private needsFallbackLocale()Z
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 615
    const-string v2, "com.google.android.tts"

    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsEngine:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultLocale:Ljava/util/Locale;

    if-eqz v2, :cond_1

    .line 627
    :cond_0
    :goto_0
    return v1

    .line 619
    :cond_1
    iget-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v2, :cond_0

    .line 626
    iget-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSystemLocale:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Landroid/speech/tts/TextToSpeech;->getFeatures(Ljava/util/Locale;)Ljava/util/Set;

    move-result-object v0

    .line 627
    .local v0, "features":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v0, :cond_2

    const-string v2, "embeddedTts"

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSystemLocale:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->isAvailableStatus(I)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .prologue
    .line 751
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 752
    .local v0, "newLocale":Ljava/util/Locale;
    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSystemLocale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 761
    :goto_0
    return-void

    .line 756
    :cond_0
    iput-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSystemLocale:Ljava/util/Locale;

    .line 760
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->ensureSupportedLocale()V

    goto :goto_0
.end method

.method private registerGoogleTtsFixCallbacks()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .prologue
    .line 770
    const-string v1, "tts_default_locale"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 771
    .local v0, "defaultLocaleUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mLocaleObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 772
    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mComponentCallbacks:Landroid/content/ComponentCallbacks;

    invoke-virtual {v1, v2}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 773
    return-void
.end method

.method private setAudioAttributesApi21()V
    .locals 3

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    new-instance v1, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v1}, Landroid/media/AudioAttributes$Builder;-><init>()V

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Landroid/media/AudioAttributes$Builder;->setUsage(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setAudioAttributes(Landroid/media/AudioAttributes;)I

    .line 459
    return-void
.end method

.method private setEngineByPackageName_GB_HC(Ljava/lang/String;)V
    .locals 3
    .param p1, "engine"    # Ljava/lang/String;

    .prologue
    .line 364
    iput-object p1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTtsEngine:Ljava/lang/String;

    .line 365
    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    iput-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTts:Landroid/speech/tts/TextToSpeech;

    .line 367
    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    iget-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTtsEngine:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/googlecode/eyesfree/compat/speech/tts/TextToSpeechCompatUtils;->setEngineByPackageName(Landroid/speech/tts/TextToSpeech;Ljava/lang/String;)I

    move-result v0

    .line 368
    .local v0, "status":I
    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsChangeListener:Landroid/speech/tts/TextToSpeech$OnInitListener;

    invoke-interface {v1, v0}, Landroid/speech/tts/TextToSpeech$OnInitListener;->onInit(I)V

    .line 369
    return-void
.end method

.method private setTtsEngine(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "engine"    # Ljava/lang/String;
    .param p2, "resetFailures"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 332
    if-eqz p2, :cond_0

    .line 333
    iput v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsFailures:I

    .line 337
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-static {v0}, Lcom/google/android/marvin/utils/TextToSpeechUtils;->attemptTtsShutdown(Landroid/speech/tts/TextToSpeech;)V

    .line 339
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_1

    .line 340
    const-class v0, Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v1, 0x6

    const-string v2, "Can\'t start TTS engine %s while still loading previous engine"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 356
    :goto_0
    return-void

    .line 345
    :cond_1
    const-class v0, Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v1, 0x4

    const-string v2, "Switching to TTS engine: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 349
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-le v0, v1, :cond_2

    .line 350
    invoke-direct {p0, p1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->setEngineByPackageName_GB_HC(Ljava/lang/String;)V

    goto :goto_0

    .line 354
    :cond_2
    iput-object p1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTtsEngine:Ljava/lang/String;

    .line 355
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsChangeListener:Landroid/speech/tts/TextToSpeech$OnInitListener;

    invoke-static {v0, v1, p1}, Lcom/googlecode/eyesfree/compat/speech/tts/TextToSpeechCompatUtils;->newTextToSpeech(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;Ljava/lang/String;)Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTts:Landroid/speech/tts/TextToSpeech;

    goto :goto_0
.end method

.method private speakApi21(Ljava/lang/CharSequence;Ljava/util/HashMap;Ljava/lang/String;FFIF)I
    .locals 6
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p3, "utteranceId"    # Ljava/lang/String;
    .param p4, "pitch"    # F
    .param p5, "rate"    # F
    .param p6, "stream"    # I
    .param p7, "volume"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "FFIF)I"
        }
    .end annotation

    .prologue
    .local p2, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/high16 v5, 0x42c80000    # 100.0f

    .line 310
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 312
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz p2, :cond_0

    .line 313
    invoke-virtual {p2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 314
    .local v2, "key":Ljava/lang/String;
    invoke-virtual {p2, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 318
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "key":Ljava/lang/String;
    :cond_0
    const-string v3, "pitch"

    mul-float v4, p4, v5

    float-to-int v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 319
    const-string v3, "rate"

    mul-float v4, p5, v5

    float-to-int v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 320
    const-string v3, "streamType"

    invoke-virtual {v0, v3, p6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 321
    const-string v3, "volume"

    invoke-virtual {v0, v3, p7}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 323
    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    const/4 v4, 0x2

    invoke-virtual {v3, p1, v4, v0, p3}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/CharSequence;ILandroid/os/Bundle;Ljava/lang/String;)I

    move-result v3

    return v3
.end method

.method private speakCompat(Ljava/lang/CharSequence;Ljava/util/HashMap;)I
    .locals 3
    .param p1, "text"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 305
    .local p2, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p2}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    move-result v0

    return v0
.end method

.method private trySpeak(Ljava/lang/CharSequence;FFLjava/util/HashMap;IF)I
    .locals 9
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "pitch"    # F
    .param p3, "rate"    # F
    .param p5, "stream"    # I
    .param p6, "volume"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "FF",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;IF)I"
        }
    .end annotation

    .prologue
    .line 270
    .local p4, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    if-nez v0, :cond_0

    .line 271
    const/4 v8, -0x1

    .line 301
    :goto_0
    return v8

    .line 274
    :cond_0
    iget v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultPitch:F

    mul-float v4, p2, v0

    .line 275
    .local v4, "effectivePitch":F
    iget v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultRate:F

    mul-float v5, p3, v0

    .line 278
    .local v5, "effectiveRate":F
    const-string v0, "utteranceId"

    invoke-virtual {p4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 279
    .local v3, "utteranceId":Ljava/lang/String;
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-le v0, v1, :cond_2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move v6, p5

    move v7, p6

    .line 280
    invoke-direct/range {v0 .. v7}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->speakApi21(Ljava/lang/CharSequence;Ljava/util/HashMap;Ljava/lang/String;FFIF)I

    move-result v8

    .line 293
    .local v8, "result":I
    :goto_1
    iput v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mCurrentPitch:F

    .line 294
    iput v5, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mCurrentRate:F

    .line 296
    if-eqz v8, :cond_1

    sget-boolean v0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->USE_GOOGLE_TTS_WORKAROUNDS:Z

    if-eqz v0, :cond_1

    .line 297
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->ensureSupportedLocale()V

    .line 300
    :cond_1
    const/4 v0, 0x3

    const-string v1, "Speak call for \"%s\" returned %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v2, v6

    const/4 v6, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v2, v6

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 284
    .end local v8    # "result":I
    :cond_2
    iget v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mCurrentPitch:F

    cmpl-float v0, v0, v4

    if-nez v0, :cond_3

    iget v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mCurrentRate:F

    cmpl-float v0, v0, v5

    if-eqz v0, :cond_4

    .line 285
    :cond_3
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 286
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0, v4}, Landroid/speech/tts/TextToSpeech;->setPitch(F)I

    .line 287
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0, v5}, Landroid/speech/tts/TextToSpeech;->setSpeechRate(F)I

    .line 290
    :cond_4
    invoke-direct {p0, p1, p4}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->speakCompat(Ljava/lang/CharSequence;Ljava/util/HashMap;)I

    move-result v8

    .restart local v8    # "result":I
    goto :goto_1
.end method

.method private unregisterGoogleTtsFixCallbacks()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .prologue
    .line 782
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mLocaleObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 783
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mComponentCallbacks:Landroid/content/ComponentCallbacks;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 784
    return-void
.end method

.method private updateDefaultEngine()V
    .locals 3

    .prologue
    .line 506
    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 510
    .local v0, "resolver":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mInstalledTtsEngines:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 511
    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mInstalledTtsEngines:Ljava/util/LinkedList;

    invoke-static {v1, v2}, Lcom/google/android/marvin/utils/TextToSpeechUtils;->reloadInstalledTtsEngines(Landroid/content/Context;Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSystemTtsEngine:Ljava/lang/String;

    .line 515
    const-string v1, "tts_default_synth"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultTtsEngine:Ljava/lang/String;

    .line 518
    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultTtsEngine:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->setTtsEngine(Ljava/lang/String;Z)V

    .line 519
    return-void
.end method

.method private updateDefaultLocale()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .prologue
    .line 735
    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsEngine:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/marvin/utils/TextToSpeechUtils;->getDefaultLocaleForEngine(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 737
    .local v0, "defaultLocale":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    :goto_0
    iput-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultLocale:Ljava/util/Locale;

    .line 741
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->ensureSupportedLocale()V

    .line 742
    return-void

    .line 737
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateDefaultPitch()V
    .locals 3

    .prologue
    .line 526
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "tts_default_pitch"

    const/16 v2, 0x64

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultPitch:F

    .line 527
    return-void
.end method

.method private updateDefaultRate()V
    .locals 3

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "tts_default_rate"

    const/16 v2, 0x64

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultRate:F

    .line 535
    return-void
.end method


# virtual methods
.method public addListener(Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    return-void
.end method

.method public getEngineInstance()Landroid/speech/tts/TextToSpeech;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    return-object v0
.end method

.method public getEngineLabel()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsEngine:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/marvin/utils/TextToSpeechUtils;->getLabelForEngine(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public isReady()Z
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shutdown()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 241
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mMediaStateMonitor:Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 243
    sget-boolean v0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->USE_GOOGLE_TTS_WORKAROUNDS:Z

    if-eqz v0, :cond_0

    .line 244
    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->unregisterGoogleTtsFixCallbacks()V

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSynthObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 248
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mPitchObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mRateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 251
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-static {v0}, Lcom/google/android/marvin/utils/TextToSpeechUtils;->attemptTtsShutdown(Landroid/speech/tts/TextToSpeech;)V

    .line 252
    iput-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    .line 254
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTts:Landroid/speech/tts/TextToSpeech;

    invoke-static {v0}, Lcom/google/android/marvin/utils/TextToSpeechUtils;->attemptTtsShutdown(Landroid/speech/tts/TextToSpeech;)V

    .line 255
    iput-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTts:Landroid/speech/tts/TextToSpeech;

    .line 256
    return-void
.end method

.method public speak(Ljava/lang/CharSequence;FFLjava/util/HashMap;IF)V
    .locals 7
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "pitch"    # F
    .param p3, "rate"    # F
    .param p5, "stream"    # I
    .param p6, "volume"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "FF",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;IF)V"
        }
    .end annotation

    .prologue
    .local p4, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v6, 0x5

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 193
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 194
    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    const-string v3, "utteranceId"

    invoke-virtual {p4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;->onUtteranceCompleted(Ljava/lang/String;)V

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    const/4 v1, 0x0

    .line 202
    .local v1, "failureException":Ljava/lang/Exception;
    :try_start_0
    invoke-direct/range {p0 .. p6}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->trySpeak(Ljava/lang/CharSequence;FFLjava/util/HashMap;IF)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 208
    .local v2, "result":I
    :goto_1
    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 209
    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsEngine:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->attemptTtsFailover(Ljava/lang/String;)V

    .line 212
    :cond_2
    if-eqz v2, :cond_0

    const-string v3, "utteranceId"

    invoke-virtual {p4, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 214
    if-eqz v1, :cond_3

    .line 215
    const-string v3, "Failed to speak \"%s\" due to an exception"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-static {p0, v6, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 216
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 221
    :goto_2
    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    const-string v3, "utteranceId"

    invoke-virtual {p4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;->onUtteranceCompleted(Ljava/lang/String;)V

    goto :goto_0

    .line 203
    .end local v2    # "result":I
    :catch_0
    move-exception v0

    .line 204
    .local v0, "e":Ljava/lang/Exception;
    move-object v1, v0

    .line 205
    const/4 v2, -0x1

    .restart local v2    # "result":I
    goto :goto_1

    .line 218
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    const-string v3, "Failed to speak \"%s\""

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-static {p0, v6, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public stopAll()V
    .locals 4

    .prologue
    .line 230
    :try_start_0
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    const-string v1, ""

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    :goto_0
    return-void

    .line 231
    :catch_0
    move-exception v0

    goto :goto_0
.end method
