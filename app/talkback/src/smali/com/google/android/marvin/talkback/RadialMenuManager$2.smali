.class Lcom/google/android/marvin/talkback/RadialMenuManager$2;
.super Ljava/lang/Object;
.source "RadialMenuManager.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/RadialMenuManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/RadialMenuManager;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$2;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 195
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$2;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$100(Lcom/google/android/marvin/talkback/RadialMenuManager;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$2;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mRadialMenuHint:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$000(Lcom/google/android/marvin/talkback/RadialMenuManager;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 197
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$2;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$200(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/CachedFeedbackController;

    move-result-object v2

    const v3, 0x7f0a001b

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playHaptic(I)Z

    .line 198
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$2;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$200(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/CachedFeedbackController;

    move-result-object v2

    const v3, 0x7f050020

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(I)Z

    .line 200
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$2;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$300(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$2;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$300(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;->onMenuItemClicked(Landroid/view/MenuItem;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 202
    .local v0, "handled":Z
    :goto_0
    if-nez v0, :cond_0

    if-nez p1, :cond_0

    .line 203
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$2;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mService:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$400(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->interruptAllFeedback()V

    .line 206
    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 207
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$2;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-interface {p1}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v3

    # invokes: Lcom/google/android/marvin/talkback/RadialMenuManager;->playScaleForMenu(Landroid/view/Menu;)V
    invoke-static {v2, v3}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$600(Lcom/google/android/marvin/talkback/RadialMenuManager;Landroid/view/Menu;)V

    .line 210
    :cond_1
    return v1

    .line 200
    .end local v0    # "handled":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
