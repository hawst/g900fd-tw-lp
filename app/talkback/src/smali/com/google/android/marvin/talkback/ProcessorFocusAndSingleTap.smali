.class Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;
.super Ljava/lang/Object;
.source "ProcessorFocusAndSingleTap.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/CursorController$ScrollListener;
.implements Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;
    }
.end annotation


# static fields
.field private static final SUPPORTS_INTERACTION_EVENTS:Z

.field public static final TAP_TIMEOUT:J


# instance fields
.field private final mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mCachedPotentiallyFocusableRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

.field private final mCursorController:Lcom/google/android/marvin/talkback/CursorController;

.field private mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private mFocusedItems:I

.field private final mHandler:Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;

.field private mLastScrollAction:I

.field private mLastScrollFromIndex:I

.field private mLastScrollSource:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private mLastScrollToIndex:I

.field private mLastViewScrolledEvent:J

.field private mLastWindowStateChangedEvent:J

.field private mMaybeRefocus:Z

.field private mMaybeSingleTap:Z

.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;

.field private mSingleTapEnabled:Z

.field private final mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

.field private mViewFocusedLastFailed:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->SUPPORTS_INTERACTION_EVENTS:Z

    .line 61
    invoke-static {}, Landroid/view/ViewConfiguration;->getJumpTapTimeout()I

    move-result v0

    int-to-long v0, v0

    sput-wide v0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->TAP_TIMEOUT:J

    return-void

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 2
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    const/4 v1, -0x1

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollAction:I

    .line 89
    iput v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollFromIndex:I

    .line 90
    iput v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollToIndex:I

    .line 687
    new-instance v0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;-><init>(Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mHandler:Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;

    .line 120
    iput-object p1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 121
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    .line 122
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    .line 123
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v0, p0}, Lcom/google/android/marvin/talkback/CursorController;->addScrollListener(Lcom/google/android/marvin/talkback/CursorController$ScrollListener;)V

    .line 124
    const-string v0, "accessibility"

    invoke-virtual {p1, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 126
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ZZ)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;
    .param p1, "x1"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->followScrollEvent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ZZ)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->followContentChangedEvent()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->cancelSingleTap()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;
    .param p1, "x1"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->setScrollActionImmediately(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;)Lcom/google/android/marvin/talkback/TalkBackService;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;
    .param p1, "x1"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->attemptRefocusNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    return v0
.end method

.method private attemptRefocusNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 2
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v0, 0x0

    .line 508
    iget-boolean v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mMaybeRefocus:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/SpeechController;->isSpeaking()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 521
    :cond_0
    :goto_0
    return v0

    .line 513
    :cond_1
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->supportsWebActions(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 517
    const/16 v1, 0x80

    invoke-virtual {p1, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 521
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    goto :goto_0
.end method

.method private cancelSingleTap()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 504
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mMaybeSingleTap:Z

    .line 505
    return-void
.end method

.method private ensureFocusConsistency(Z)Z
    .locals 12
    .param p1, "shouldPlaceFocus"    # Z

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 358
    const/4 v3, 0x0

    .line 359
    .local v3, "root":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v1, 0x0

    .line 360
    .local v1, "focused":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v2, 0x0

    .line 361
    .local v2, "inputFocused":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v0, 0x0

    .line 364
    .local v0, "firstFocus":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_start_0
    iget-object v6, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v6}, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->getRootInActiveWindow(Landroid/accessibilityservice/AccessibilityService;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 365
    if-nez v3, :cond_0

    .line 402
    new-array v6, v11, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v3, v6, v4

    aput-object v1, v6, v5

    aput-object v2, v6, v9

    aput-object v0, v6, v10

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    :goto_0
    return v4

    .line 370
    :cond_0
    const/4 v6, 0x2

    :try_start_1
    invoke-virtual {v3, v6}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->findFocus(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 371
    if-eqz v1, :cond_2

    .line 372
    iget-object v6, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v6, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v6

    if-eqz v6, :cond_1

    .line 402
    new-array v6, v11, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v3, v6, v4

    aput-object v1, v6, v5

    aput-object v2, v6, v9

    aput-object v0, v6, v10

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v4, v5

    goto :goto_0

    .line 376
    :cond_1
    const/4 v6, 0x2

    :try_start_2
    const-string v7, "Clearing focus from invalid node"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 377
    const/16 v6, 0x80

    invoke-virtual {v1, v6}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 381
    :cond_2
    if-nez p1, :cond_3

    .line 402
    new-array v6, v11, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v3, v6, v4

    aput-object v1, v6, v5

    aput-object v2, v6, v9

    aput-object v0, v6, v10

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 386
    :cond_3
    const/4 v6, 0x1

    :try_start_3
    invoke-virtual {v3, v6}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->findFocus(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    .line 387
    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v6

    if-eqz v6, :cond_4

    .line 402
    new-array v6, v11, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v3, v6, v4

    aput-object v1, v6, v5

    aput-object v2, v6, v9

    aput-object v0, v6, v10

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v4, v5

    goto :goto_0

    .line 392
    :cond_4
    :try_start_4
    iget-object v6, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    sget-object v7, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SHOULD_FOCUS:Lcom/googlecode/eyesfree/utils/NodeFilter;

    const/4 v8, 0x1

    invoke-static {v6, v3, v7, v8}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->searchFromInOrderTraversal(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 394
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v6

    if-eqz v6, :cond_5

    .line 402
    new-array v6, v11, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v3, v6, v4

    aput-object v1, v6, v5

    aput-object v2, v6, v9

    aput-object v0, v6, v10

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v4, v5

    goto :goto_0

    .line 398
    :cond_5
    const/4 v6, 0x6

    :try_start_5
    const-string v7, "Failed to place focus from new window"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 402
    new-array v6, v11, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v3, v6, v4

    aput-object v1, v6, v5

    aput-object v2, v6, v9

    aput-object v0, v6, v10

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v6

    new-array v7, v11, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v3, v7, v4

    aput-object v1, v7, v5

    aput-object v2, v7, v9

    aput-object v0, v7, v10

    invoke-static {v7}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v6
.end method

.method private findChildFromNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 7
    .param p1, "root"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "direction"    # I

    .prologue
    const/4 v5, 0x0

    .line 602
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v1

    .line 603
    .local v1, "childCount":I
    if-nez v1, :cond_1

    move-object v0, v5

    .line 632
    :cond_0
    :goto_0
    return-object v0

    .line 610
    :cond_1
    const/4 v6, 0x1

    if-ne p2, v6, :cond_2

    .line 611
    const/4 v3, 0x1

    .line 612
    .local v3, "increment":I
    const/4 v4, 0x0

    .line 618
    .local v4, "startIndex":I
    :goto_1
    move v2, v4

    .local v2, "childIndex":I
    :goto_2
    if-ltz v2, :cond_4

    if-ge v2, v1, :cond_4

    .line 620
    invoke-virtual {p1, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 621
    .local v0, "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-nez v0, :cond_3

    .line 619
    :goto_3
    add-int/2addr v2, v3

    goto :goto_2

    .line 614
    .end local v0    # "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .end local v2    # "childIndex":I
    .end local v3    # "increment":I
    .end local v4    # "startIndex":I
    :cond_2
    const/4 v3, -0x1

    .line 615
    .restart local v3    # "increment":I
    add-int/lit8 v4, v1, -0x1

    .restart local v4    # "startIndex":I
    goto :goto_1

    .line 625
    .restart local v0    # "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .restart local v2    # "childIndex":I
    :cond_3
    iget-object v6, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v6, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 629
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_3

    .end local v0    # "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_4
    move-object v0, v5

    .line 632
    goto :goto_0
.end method

.method private followContentChangedEvent()Z
    .locals 1

    .prologue
    .line 525
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->ensureFocusConsistency(Z)Z

    move-result v0

    return v0
.end method

.method private followScrollEvent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ZZ)Z
    .locals 9
    .param p1, "source"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "isMovingForward"    # Z
    .param p3, "wasScrollAction"    # Z

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 530
    const/4 v1, 0x0

    .line 531
    .local v1, "root":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v0, 0x0

    .line 535
    .local v0, "focused":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_start_0
    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v4}, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->getRootInActiveWindow(Landroid/accessibilityservice/AccessibilityService;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 536
    if-nez v1, :cond_0

    .line 563
    new-array v4, v8, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v4, v2

    aput-object v0, v4, v3

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    :goto_0
    return v2

    .line 540
    :cond_0
    const/4 v4, 0x2

    :try_start_1
    invoke-virtual {v1, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->findFocus(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 541
    if-eqz v0, :cond_3

    .line 546
    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v4, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz p3, :cond_1

    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/CursorController;->refocus()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_2

    .line 563
    :cond_1
    new-array v4, v8, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v4, v2

    aput-object v0, v4, v3

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v2, v3

    goto :goto_0

    .line 551
    :cond_2
    const/4 v4, 0x3

    :try_start_2
    const-string v5, "Clear focus from %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-static {p0, v4, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 552
    const/16 v4, 0x80

    invoke-virtual {v0, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    .line 556
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->tryFocusingChild(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v4

    if-eqz v4, :cond_4

    .line 563
    new-array v4, v8, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v4, v2

    aput-object v0, v4, v3

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v2, v3

    goto :goto_0

    .line 561
    :cond_4
    :try_start_3
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v4

    .line 563
    new-array v5, v8, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v5, v2

    aput-object v0, v5, v3

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v2, v4

    goto :goto_0

    :catchall_0
    move-exception v4

    new-array v5, v8, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v5, v2

    aput-object v0, v5, v3

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v4
.end method

.method private handleTouchInteractionEnd()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez v0, :cond_0

    .line 454
    :goto_0
    return-void

    .line 447
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mSingleTapEnabled:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mMaybeSingleTap:Z

    if-eqz v0, :cond_1

    .line 448
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mHandler:Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->cancelRefocusTimeout(Z)V

    .line 449
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->performClick(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 452
    :cond_1
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 453
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    goto :goto_0
.end method

.method private handleTouchInteractionStart()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 411
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v1, :cond_0

    .line 412
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 413
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 416
    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/SpeechController;->isSpeaking()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 417
    iput-boolean v3, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mMaybeRefocus:Z

    .line 419
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 424
    .local v0, "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->isTutorialActive()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-class v2, Landroid/webkit/WebView;

    invoke-static {v1, v0, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 427
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->interruptAllFeedback()V

    .line 429
    :cond_1
    new-array v1, v4, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v1, v3

    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 434
    .end local v0    # "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :goto_0
    iput-boolean v4, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mMaybeSingleTap:Z

    .line 435
    iput v3, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mFocusedItems:I

    .line 436
    return-void

    .line 431
    :cond_2
    iput-boolean v4, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mMaybeRefocus:Z

    goto :goto_0
.end method

.method private handleViewScrolled(Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)V
    .locals 8
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "record"    # Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 247
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastViewScrolledEvent:J

    .line 249
    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 250
    .local v1, "source":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-nez v1, :cond_0

    .line 251
    const/4 v4, 0x6

    const-string v5, "Drop scroll with no source node"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v4, v5, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 280
    :goto_0
    return-void

    .line 256
    :cond_0
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollSource:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v1, v5}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 257
    iget v5, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollAction:I

    const/16 v6, 0x1000

    if-eq v5, v6, :cond_1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v5

    iget v6, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollFromIndex:I

    if-gt v5, v6, :cond_1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getToIndex()I

    move-result v5

    iget v6, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollToIndex:I

    if-le v5, v6, :cond_3

    :cond_1
    move v0, v4

    .line 261
    .local v0, "isMovingForward":Z
    :goto_1
    iget v5, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollAction:I

    if-eqz v5, :cond_4

    move v2, v4

    .line 262
    .local v2, "wasScrollAction":Z
    :goto_2
    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mHandler:Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;

    invoke-virtual {v3, v1, v0, v2}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->followScrollDelayed(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ZZ)V

    .line 266
    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mHandler:Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->clearScrollActionDelayed()V

    .line 271
    .end local v0    # "isMovingForward":Z
    .end local v2    # "wasScrollAction":Z
    :goto_3
    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollSource:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v3, :cond_2

    .line 272
    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollSource:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 275
    :cond_2
    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollSource:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 276
    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getFromIndex()I

    move-result v3

    iput v3, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollFromIndex:I

    .line 277
    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getToIndex()I

    move-result v3

    iput v3, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollToIndex:I

    .line 279
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->tryFocusCachedRecord()V

    goto :goto_0

    :cond_3
    move v0, v3

    .line 257
    goto :goto_1

    .restart local v0    # "isMovingForward":Z
    :cond_4
    move v2, v3

    .line 261
    goto :goto_2

    .line 268
    .end local v0    # "isMovingForward":Z
    :cond_5
    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->setScrollActionImmediately(I)V

    goto :goto_3
.end method

.method private handleWindowContentChanged(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)V
    .locals 2
    .param p1, "record"    # Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    .prologue
    .line 234
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 235
    .local v0, "source":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-nez v0, :cond_0

    .line 244
    :goto_0
    return-void

    .line 239
    :cond_0
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 241
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mHandler:Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;

    invoke-virtual {v1, p1}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->followContentChangedDelayed(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)V

    .line 243
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->tryFocusCachedRecord()V

    goto :goto_0
.end method

.method private handleWindowStateChange(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 209
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastWindowStateChangedEvent:J

    .line 212
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollSource:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v1, :cond_0

    .line 213
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollSource:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 214
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollSource:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 216
    :cond_0
    iput v5, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollAction:I

    .line 217
    iput v4, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollFromIndex:I

    .line 218
    iput v4, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollToIndex:I

    .line 223
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 224
    .local v0, "cursor":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getWindowId()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getWindowId()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 225
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/CursorController;->clearCursor()V

    .line 228
    :cond_1
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v1, v5

    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 230
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->tryFocusCachedRecord()V

    .line 231
    return-void
.end method

.method private performClick(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 5
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v4, 0x1

    .line 656
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    new-array v1, v4, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Landroid/widget/EditText;

    aput-object v3, v1, v2

    invoke-static {v0, p1, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesAnyClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 658
    invoke-virtual {p1, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    .line 670
    :cond_0
    :goto_0
    return-void

    .line 665
    :cond_1
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->supportsWebActions(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 669
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    goto :goto_0
.end method

.method private setFocusFromViewHoverEnter(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 4
    .param p1, "touchedNode"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 461
    const/4 v0, 0x0

    .line 464
    .local v0, "focusable":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_start_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v2, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->findFocusFromHover(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 465
    if-nez v0, :cond_0

    .line 494
    new-array v2, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v1

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    :goto_0
    return v1

    .line 469
    :cond_0
    :try_start_1
    sget-boolean v2, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->SUPPORTS_INTERACTION_EVENTS:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez v2, :cond_2

    iget v2, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mFocusedItems:I

    if-nez v2, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isAccessibilityFocused()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 471
    invoke-static {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 473
    iget-boolean v2, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mSingleTapEnabled:Z

    if-eqz v2, :cond_1

    .line 474
    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mHandler:Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;

    invoke-virtual {v2, v0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->refocusAfterTimeout(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 494
    new-array v2, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v1

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 478
    :cond_1
    :try_start_2
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->attemptRefocusNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    .line 494
    new-array v3, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v1

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v1, v2

    goto :goto_0

    .line 481
    :cond_2
    :try_start_3
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v2

    if-nez v2, :cond_3

    .line 494
    new-array v2, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v1

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 486
    :cond_3
    :try_start_4
    iget-boolean v2, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mSingleTapEnabled:Z

    if-eqz v2, :cond_4

    .line 487
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->cancelSingleTap()V

    .line 490
    :cond_4
    iget v2, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mFocusedItems:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mFocusedItems:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 494
    new-array v2, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v1

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v1, v3

    goto :goto_0

    :catchall_0
    move-exception v2

    new-array v3, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v1

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v2
.end method

.method private setFocusOnView(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;Z)Z
    .locals 10
    .param p1, "record"    # Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    .param p2, "isViewFocusedEvent"    # Z

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 302
    const/4 v3, 0x0

    .line 303
    .local v3, "source":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v1, 0x0

    .line 304
    .local v1, "existing":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v0, 0x0

    .line 307
    .local v0, "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_start_0
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 308
    if-nez v3, :cond_0

    .line 350
    new-array v6, v9, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v3, v6, v4

    aput-object v1, v6, v5

    aput-object v0, v6, v8

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    :goto_0
    return v4

    .line 312
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getCurrentItemIndex()I

    move-result v6

    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getFromIndex()I

    move-result v7

    sub-int v2, v6, v7

    .line 313
    .local v2, "index":I
    if-ltz v2, :cond_1

    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v6

    if-ge v2, v6, :cond_1

    .line 314
    invoke-virtual {v3, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 315
    if-eqz v0, :cond_1

    .line 316
    iget-object v6, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v6, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isTopLevelScrollItem(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v6

    if-eqz v6, :cond_1

    .line 350
    new-array v6, v9, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v3, v6, v4

    aput-object v1, v6, v5

    aput-object v0, v6, v8

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v4, v5

    goto :goto_0

    .line 323
    :cond_1
    if-nez p2, :cond_2

    .line 350
    new-array v6, v9, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v3, v6, v4

    aput-object v1, v6, v5

    aput-object v0, v6, v8

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 329
    :cond_2
    :try_start_2
    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v6

    if-eqz v6, :cond_3

    .line 350
    new-array v6, v9, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v3, v6, v4

    aput-object v1, v6, v5

    aput-object v0, v6, v8

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v4, v5

    goto :goto_0

    .line 334
    :cond_3
    const/4 v6, 0x2

    :try_start_3
    invoke-virtual {v3, v6}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->findFocus(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 335
    if-eqz v1, :cond_4

    .line 350
    new-array v6, v9, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v3, v6, v4

    aput-object v1, v6, v5

    aput-object v0, v6, v8

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 342
    :cond_4
    :try_start_4
    iget-object v6, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    sget-object v7, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SHOULD_FOCUS:Lcom/googlecode/eyesfree/utils/NodeFilter;

    invoke-static {v6, v3, v7}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->searchFromBfs(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    .line 344
    if-nez v0, :cond_5

    .line 350
    new-array v6, v9, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v3, v6, v4

    aput-object v1, v6, v5

    aput-object v0, v6, v8

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 348
    :cond_5
    :try_start_5
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v6

    .line 350
    new-array v7, v9, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v3, v7, v4

    aput-object v1, v7, v5

    aput-object v0, v7, v8

    invoke-static {v7}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v4, v6

    goto/16 :goto_0

    .end local v2    # "index":I
    :catchall_0
    move-exception v6

    new-array v7, v9, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v3, v7, v4

    aput-object v1, v7, v5

    aput-object v0, v7, v8

    invoke-static {v7}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v6
.end method

.method private setScrollActionImmediately(I)V
    .locals 1
    .param p1, "action"    # I

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mHandler:Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->cancelClearScrollAction()V

    .line 284
    iput p1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mLastScrollAction:I

    .line 285
    return-void
.end method

.method private tryFocusCachedRecord()V
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mCachedPotentiallyFocusableRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mCachedPotentiallyFocusableRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    iget-boolean v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mViewFocusedLastFailed:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->setFocusOnView(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mCachedPotentiallyFocusableRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->recycle()V

    .line 291
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mCachedPotentiallyFocusableRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    .line 294
    :cond_0
    return-void
.end method

.method private tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 2
    .param p1, "source"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v0, 0x0

    .line 636
    if-nez p1, :cond_1

    .line 649
    :cond_0
    :goto_0
    return v0

    .line 640
    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v1, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 644
    const/16 v1, 0x40

    invoke-virtual {p1, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 648
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mHandler:Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->interruptFollowDelayed()V

    .line 649
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private tryFocusingChild(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)Z
    .locals 5
    .param p1, "parent"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "wasMovingForward"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 573
    if-eqz p2, :cond_0

    move v1, v2

    .line 576
    .local v1, "direction":I
    :goto_0
    const/4 v0, 0x0

    .line 579
    .local v0, "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_start_0
    invoke-direct {p0, p1, v1}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->findChildFromNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 580
    if-nez v0, :cond_1

    .line 586
    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v2, v3

    :goto_1
    return v2

    .line 573
    .end local v0    # "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .end local v1    # "direction":I
    :cond_0
    const/4 v1, -0x1

    goto :goto_0

    .line 584
    .restart local v0    # "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .restart local v1    # "direction":I
    :cond_1
    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    .line 586
    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v2, v4

    goto :goto_1

    :catchall_0
    move-exception v4

    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v4
.end method


# virtual methods
.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 130
    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v4}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v1, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 137
    .local v1, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    goto :goto_0

    .line 143
    :sswitch_0
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->cancelSingleTap()V

    goto :goto_0

    .line 147
    :sswitch_1
    const/16 v4, 0x8

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v5

    if-ne v4, v5, :cond_2

    .line 149
    .local v0, "isViewFocusedEvent":Z
    :goto_1
    invoke-direct {p0, v1, v0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->setFocusOnView(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;Z)Z

    move-result v3

    if-nez v3, :cond_3

    .line 155
    invoke-static {v1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mCachedPotentiallyFocusableRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    .line 156
    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mViewFocusedLastFailed:Z

    goto :goto_0

    .end local v0    # "isViewFocusedEvent":Z
    :cond_2
    move v0, v3

    .line 147
    goto :goto_1

    .line 157
    .restart local v0    # "isViewFocusedEvent":Z
    :cond_3
    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mCachedPotentiallyFocusableRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    if-eqz v3, :cond_0

    .line 158
    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mCachedPotentiallyFocusableRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->recycle()V

    .line 159
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mCachedPotentiallyFocusableRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    goto :goto_0

    .line 163
    .end local v0    # "isViewFocusedEvent":Z
    :sswitch_2
    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    .line 165
    .local v2, "touchedNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v2, :cond_4

    :try_start_0
    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->setFocusFromViewHoverEnter(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 167
    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mHandler:Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;

    invoke-virtual {v4, v2}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->sendEmptyTouchAreaFeedbackDelayed(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    :cond_4
    new-array v4, v0, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v2, v4, v3

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    :catchall_0
    move-exception v4

    new-array v5, v0, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v2, v5, v3

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v4

    .line 175
    .end local v2    # "touchedNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :sswitch_3
    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mHandler:Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->cancelEmptyTouchAreaFeedback()V

    goto :goto_0

    .line 178
    :sswitch_4
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->handleWindowStateChange(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0

    .line 181
    :sswitch_5
    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->handleWindowContentChanged(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)V

    goto :goto_0

    .line 184
    :sswitch_6
    invoke-direct {p0, p1, v1}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->handleViewScrolled(Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)V

    goto :goto_0

    .line 188
    :sswitch_7
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->handleTouchInteractionStart()V

    goto :goto_0

    .line 192
    :sswitch_8
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->handleTouchInteractionEnd()V

    goto :goto_0

    .line 137
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_1
        0x20 -> :sswitch_4
        0x80 -> :sswitch_2
        0x800 -> :sswitch_5
        0x1000 -> :sswitch_6
        0x8000 -> :sswitch_3
        0x100000 -> :sswitch_7
        0x200000 -> :sswitch_8
    .end sparse-switch
.end method

.method public onScroll(I)V
    .locals 0
    .param p1, "action"    # I

    .prologue
    .line 679
    sparse-switch p1, :sswitch_data_0

    .line 685
    :goto_0
    return-void

    .line 682
    :sswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->setScrollActionImmediately(I)V

    goto :goto_0

    .line 679
    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_0
        0x2000 -> :sswitch_0
    .end sparse-switch
.end method

.method public setSingleTapEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 205
    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mSingleTapEnabled:Z

    .line 206
    return-void
.end method
