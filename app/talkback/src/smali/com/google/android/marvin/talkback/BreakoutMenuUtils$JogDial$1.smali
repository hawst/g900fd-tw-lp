.class Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial$1;
.super Ljava/lang/Object;
.source "BreakoutMenuUtils.java"

# interfaces
.implements Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mLastItem:I

.field final synthetic this$0:Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;)V
    .locals 1

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial$1;->this$0:Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial$1;->mLastItem:I

    return-void
.end method


# virtual methods
.method public onMenuItemSelection(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Z
    .locals 3
    .param p1, "item"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .prologue
    .line 79
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getItemId()I

    move-result v1

    .line 80
    .local v1, "itemId":I
    iget v2, p0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial$1;->mLastItem:I

    sub-int v0, v1, v2

    .line 82
    .local v0, "diff":I
    iget v2, p0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial$1;->mLastItem:I

    if-ltz v2, :cond_4

    .line 83
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial$1;->this$0:Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;

    # getter for: Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;->mSegmentCount:I
    invoke-static {v2}, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;->access$000(Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;)I

    move-result v2

    neg-int v2, v2

    if-ne v0, v2, :cond_2

    .line 84
    :cond_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial$1;->this$0:Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;->onPrevious()V

    .line 92
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getItemId()I

    move-result v2

    iput v2, p0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial$1;->mLastItem:I

    .line 95
    const/4 v2, 0x0

    return v2

    .line 85
    :cond_2
    const/4 v2, 0x1

    if-eq v0, v2, :cond_3

    iget-object v2, p0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial$1;->this$0:Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;

    # getter for: Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;->mSegmentCount:I
    invoke-static {v2}, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;->access$000(Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;)I

    move-result v2

    if-ne v0, v2, :cond_1

    .line 86
    :cond_3
    iget-object v2, p0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial$1;->this$0:Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;->onNext()V

    goto :goto_0

    .line 89
    :cond_4
    iget-object v2, p0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial$1;->this$0:Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;->onFirstTouch()V

    goto :goto_0
.end method
