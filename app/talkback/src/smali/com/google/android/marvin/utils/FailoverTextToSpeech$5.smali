.class Lcom/google/android/marvin/utils/FailoverTextToSpeech$5;
.super Ljava/lang/Object;
.source "FailoverTextToSpeech.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/utils/FailoverTextToSpeech;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/utils/FailoverTextToSpeech;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V
    .locals 0

    .prologue
    .line 869
    iput-object p1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech$5;->this$0:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUtteranceCompleted(Ljava/lang/String;)V
    .locals 4
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 872
    const/4 v0, 0x3

    const-string v1, "Received completion for \"%s\""

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 874
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech$5;->this$0:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    # getter for: Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;
    invoke-static {v0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->access$400(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;->onUtteranceCompleted(Ljava/lang/String;)V

    .line 875
    return-void
.end method
