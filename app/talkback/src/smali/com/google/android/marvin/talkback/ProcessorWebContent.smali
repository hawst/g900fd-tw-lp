.class public Lcom/google/android/marvin/talkback/ProcessorWebContent;
.super Ljava/lang/Object;
.source "ProcessorWebContent.java"

# interfaces
.implements Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private mLastNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 60
    return-void
.end method


# virtual methods
.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 9
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v8, 0x0

    .line 65
    const v5, 0x8080

    invoke-static {p1, v5}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->eventMatchesAnyType(Landroid/view/accessibility/AccessibilityEvent;I)Z

    move-result v5

    if-nez v5, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    new-instance v3, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v3, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 70
    .local v3, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v4

    .line 73
    .local v4, "source":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mLastNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mLastNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v5, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 76
    :cond_2
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mLastNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v5, :cond_3

    .line 77
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mLastNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v5}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 79
    :cond_3
    iput-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mLastNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 83
    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->supportsWebActions(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 87
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->isScriptInjectionEnabled(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 90
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getFullScreenReadController()Lcom/google/android/marvin/talkback/FullScreenReadController;

    move-result-object v1

    .line 91
    .local v1, "fullScreen":Lcom/google/android/marvin/talkback/FullScreenReadController;
    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/FullScreenReadController;->isReadingLegacyWebContent()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 94
    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/FullScreenReadController;->interrupt()V

    goto :goto_0

    .line 96
    :cond_4
    const/4 v5, -0x2

    invoke-static {v4, v5}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performSpecialAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Z

    goto :goto_0

    .line 99
    .end local v1    # "fullScreen":Lcom/google/android/marvin/talkback/FullScreenReadController;
    :cond_5
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x11

    if-gt v5, v6, :cond_0

    .line 102
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v6, "com.android.settings"

    const-string v7, "accessibility_toggle_script_injection_preference_title"

    invoke-static {v5, v6, v7}, Lcom/googlecode/eyesfree/utils/AutomationUtils;->getPackageString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 104
    .local v2, "preferenceName":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 105
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v6, 0x7f060187

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 107
    .local v0, "announcement":Ljava/lang/CharSequence;
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v0, v8, v8, v6}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    goto :goto_0
.end method
