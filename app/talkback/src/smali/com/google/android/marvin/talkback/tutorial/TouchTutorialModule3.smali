.class Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;
.super Lcom/google/android/marvin/talkback/tutorial/TutorialModule;
.source "TouchTutorialModule3.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final mContextMenuMonitor:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

.field private final mGestureActionMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

.field private final mGlobalContextMenuGestureDelegate:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;

.field private final mLocalContextMenuGestureDelegate:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;

.field private final mLocalContextMenuHiddenDelegate:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;

.field private final mReadFromTopDelegate:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V
    .locals 3
    .param p1, "parentTutorial"    # Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 150
    const v0, 0x7f0601ce

    invoke-direct {p0, p1, v0}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;I)V

    .line 42
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    invoke-direct {v0}, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mGestureActionMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    .line 45
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

    invoke-direct {v0}, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mContextMenuMonitor:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

    .line 48
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mGlobalContextMenuGestureDelegate:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;

    .line 68
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3$2;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3$2;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mReadFromTopDelegate:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;

    .line 105
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3$3;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3$3;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mLocalContextMenuGestureDelegate:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;

    .line 122
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3$4;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3$4;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mLocalContextMenuHiddenDelegate:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;

    .line 152
    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->setSkipVisible(Z)V

    .line 153
    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->setBackVisible(Z)V

    .line 154
    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->setNextVisible(Z)V

    .line 155
    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->setFinishVisible(Z)V

    .line 156
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;)Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mGestureActionMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->onTrigger1()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;)Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mContextMenuMonitor:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->onTrigger2Hidden()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->interruptContinuousReading()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->onTrigger2()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->onTrigger3()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->onTrigger4()V

    return-void
.end method

.method private interruptContinuousReading()V
    .locals 2

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->getParentTutorial()Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getFullScreenReadController()Lcom/google/android/marvin/talkback/FullScreenReadController;

    move-result-object v0

    .line 256
    .local v0, "fullScreenReadController":Lcom/google/android/marvin/talkback/FullScreenReadController;
    if-eqz v0, :cond_0

    .line 257
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->interrupt()V

    .line 260
    :cond_0
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->interruptSpeech()V

    .line 261
    return-void
.end method

.method private interruptSpeech()V
    .locals 2

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->getParentTutorial()Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    .line 248
    .local v0, "speechController":Lcom/google/android/marvin/talkback/SpeechController;
    if-eqz v0, :cond_0

    .line 249
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/SpeechController;->interrupt()V

    .line 251
    :cond_0
    return-void
.end method

.method private onTrigger0()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 184
    sget-object v1, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->TALKBACK_BREAKOUT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->getGestureDirectionForRequiredAction(Lcom/google/android/marvin/talkback/ShortcutGestureAction;)I

    move-result v0

    .line 186
    .local v0, "direction":I
    if-gez v0, :cond_0

    .line 195
    :goto_0
    return-void

    .line 190
    :cond_0
    const v1, 0x7f0601cf

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v5, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 194
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mGestureActionMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mGlobalContextMenuGestureDelegate:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;->setListener(Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;)V

    goto :goto_0
.end method

.method private onTrigger1()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 198
    const v0, 0x7f0601d0

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f060126

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v5, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 202
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mContextMenuMonitor:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mReadFromTopDelegate:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->setListener(Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;)V

    .line 203
    return-void
.end method

.method private onTrigger2()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 220
    sget-object v1, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->LOCAL_BREAKOUT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->getGestureDirectionForRequiredAction(Lcom/google/android/marvin/talkback/ShortcutGestureAction;)I

    move-result v0

    .line 222
    .local v0, "direction":I
    if-gez v0, :cond_0

    .line 231
    :goto_0
    return-void

    .line 226
    :cond_0
    const v1, 0x7f0601d2

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v5, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 230
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mGestureActionMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mLocalContextMenuGestureDelegate:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;->setListener(Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;)V

    goto :goto_0
.end method

.method private onTrigger2Hidden()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 206
    sget-object v1, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->TALKBACK_BREAKOUT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->getGestureDirectionForRequiredAction(Lcom/google/android/marvin/talkback/ShortcutGestureAction;)I

    move-result v0

    .line 208
    .local v0, "direction":I
    if-gez v0, :cond_0

    .line 217
    :goto_0
    return-void

    .line 212
    :cond_0
    const v1, 0x7f0601d1

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v5, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 216
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mGestureActionMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mGlobalContextMenuGestureDelegate:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;->setListener(Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;)V

    goto :goto_0
.end method

.method private onTrigger3()V
    .locals 3

    .prologue
    .line 234
    const v0, 0x7f0601d3

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 237
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mContextMenuMonitor:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mLocalContextMenuHiddenDelegate:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->setListener(Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;)V

    .line 238
    return-void
.end method

.method private onTrigger4()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 242
    const v0, 0x7f0601d4

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0601b9

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v5, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 244
    return-void
.end method

.method private registerReceivers()V
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mGestureActionMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    sget-object v1, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;->FILTER:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 265
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mContextMenuMonitor:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

    sget-object v1, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->FILTER:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 266
    return-void
.end method

.method private unregisterReceivers()V
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mGestureActionMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 270
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mContextMenuMonitor:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 271
    return-void
.end method


# virtual methods
.method public onPause()V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->unregisterReceivers()V

    .line 168
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->registerReceivers()V

    .line 173
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 160
    invoke-super {p0}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->onStart()V

    .line 162
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->onTrigger0()V

    .line 163
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 177
    invoke-super {p0}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->onStop()V

    .line 179
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mGestureActionMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;->setListener(Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mContextMenuMonitor:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->setListener(Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;)V

    .line 181
    return-void
.end method
