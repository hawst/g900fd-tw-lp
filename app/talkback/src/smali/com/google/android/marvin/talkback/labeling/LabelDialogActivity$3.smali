.class Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$3;
.super Ljava/lang/Object;
.source "LabelDialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->showEditLabelDialog(Lcom/googlecode/eyesfree/labeling/Label;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

.field final synthetic val$editField:Landroid/widget/EditText;

.field final synthetic val$existing:Lcom/googlecode/eyesfree/labeling/Label;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;Lcom/googlecode/eyesfree/labeling/Label;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$3;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    iput-object p2, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$3;->val$existing:Lcom/googlecode/eyesfree/labeling/Label;

    iput-object p3, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$3;->val$editField:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 231
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 232
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$3;->val$existing:Lcom/googlecode/eyesfree/labeling/Label;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$3;->val$editField:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/labeling/Label;->setText(Ljava/lang/String;)V

    .line 233
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$3;->val$existing:Lcom/googlecode/eyesfree/labeling/Label;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/googlecode/eyesfree/labeling/Label;->setTimestamp(J)V

    .line 234
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$3;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->access$300(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/googlecode/eyesfree/labeling/Label;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$3;->val$existing:Lcom/googlecode/eyesfree/labeling/Label;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->updateLabel([Lcom/googlecode/eyesfree/labeling/Label;)V

    .line 238
    :cond_0
    :goto_0
    return-void

    .line 235
    :cond_1
    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    .line 236
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0
.end method
