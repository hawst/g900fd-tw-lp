.class Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;
.super Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;
.source "VolumeMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/VolumeMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VolumeHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/utils/WeakReferenceHandler",
        "<",
        "Lcom/google/android/marvin/talkback/VolumeMonitor;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/VolumeMonitor;)V
    .locals 0
    .param p1, "parent"    # Lcom/google/android/marvin/talkback/VolumeMonitor;

    .prologue
    .line 343
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;-><init>(Ljava/lang/Object;)V

    .line 344
    return-void
.end method


# virtual methods
.method public clearReleaseControl()V
    .locals 1

    .prologue
    .line 387
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->removeMessages(I)V

    .line 388
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->removeMessages(I)V

    .line 389
    return-void
.end method

.method public handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/VolumeMonitor;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;
    .param p2, "parent"    # Lcom/google/android/marvin/talkback/VolumeMonitor;

    .prologue
    .line 348
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 369
    :goto_0
    return-void

    .line 350
    :pswitch_0
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    .line 351
    .local v2, "type":Ljava/lang/Integer;
    iget v3, p1, Landroid/os/Message;->arg1:I

    .line 352
    .local v3, "value":I
    iget v0, p1, Landroid/os/Message;->arg2:I

    .line 354
    .local v0, "prevValue":I
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    # invokes: Lcom/google/android/marvin/talkback/VolumeMonitor;->internalOnVolumeChanged(III)V
    invoke-static {p2, v5, v3, v0}, Lcom/google/android/marvin/talkback/VolumeMonitor;->access$000(Lcom/google/android/marvin/talkback/VolumeMonitor;III)V

    goto :goto_0

    .line 358
    .end local v0    # "prevValue":I
    .end local v2    # "type":Ljava/lang/Integer;
    .end local v3    # "value":I
    :pswitch_1
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 359
    .local v1, "streamType":I
    iget v4, p1, Landroid/os/Message;->arg2:I

    .line 361
    .local v4, "volume":I
    # invokes: Lcom/google/android/marvin/talkback/VolumeMonitor;->internalOnControlAcquired(I)V
    invoke-static {p2, v1}, Lcom/google/android/marvin/talkback/VolumeMonitor;->access$100(Lcom/google/android/marvin/talkback/VolumeMonitor;I)V

    goto :goto_0

    .line 365
    .end local v1    # "streamType":I
    .end local v4    # "volume":I
    :pswitch_2
    # invokes: Lcom/google/android/marvin/talkback/VolumeMonitor;->internalOnReleaseControl()V
    invoke-static {p2}, Lcom/google/android/marvin/talkback/VolumeMonitor;->access$200(Lcom/google/android/marvin/talkback/VolumeMonitor;)V

    goto :goto_0

    .line 348
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic handleMessage(Landroid/os/Message;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Message;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 331
    check-cast p2, Lcom/google/android/marvin/talkback/VolumeMonitor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/VolumeMonitor;)V

    return-void
.end method

.method public onControlAcquired(I)V
    .locals 4
    .param p1, "type"    # I

    .prologue
    const/4 v2, 0x2

    .line 398
    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->removeMessages(I)V

    .line 399
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->removeMessages(I)V

    .line 402
    const/4 v1, 0x0

    invoke-virtual {p0, v2, p1, v1}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 403
    .local v0, "msg":Landroid/os/Message;
    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 404
    return-void
.end method

.method public onVolumeChanged(III)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "value"    # I
    .param p3, "prevValue"    # I

    .prologue
    .line 414
    const/4 v0, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, p2, p3, v1}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 415
    return-void
.end method

.method public releaseControlDelayed()V
    .locals 4

    .prologue
    .line 377
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->clearReleaseControl()V

    .line 379
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 380
    .local v0, "msg":Landroid/os/Message;
    const-wide/16 v2, 0x7d0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 381
    return-void
.end method
