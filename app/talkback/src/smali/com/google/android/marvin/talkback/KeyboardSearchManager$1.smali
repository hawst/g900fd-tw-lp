.class Lcom/google/android/marvin/talkback/KeyboardSearchManager$1;
.super Ljava/lang/Object;
.source "KeyboardSearchManager.java"

# interfaces
.implements Lcom/googlecode/eyesfree/utils/NodeSearch$SearchTextFormatter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/marvin/talkback/KeyboardSearchManager;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$1;->this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDisplayText(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "queryText"    # Ljava/lang/String;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$1;->this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    # getter for: Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->access$000(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    const v1, 0x7f06008b

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTextSize()F
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$1;->this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    # getter for: Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->access$000(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method
