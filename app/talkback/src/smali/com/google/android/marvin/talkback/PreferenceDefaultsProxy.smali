.class public Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;
.super Ljava/lang/Object;
.source "PreferenceDefaultsProxy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$1;,
        Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$OnDefaultsLoadedListener;,
        Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$LoadDefaultOverridesTask;
    }
.end annotation


# static fields
.field private static final PREFERENCE_TYPES_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mCallback:Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$OnDefaultsLoadedListener;

.field private final mConfigurationFile:Ljava/io/File;

.field private final mContext:Landroid/content/Context;

.field private final mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    .line 39
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_speech_volume"

    const-class v2, Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_screenoff"

    const-class v2, Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_proximity"

    const-class v2, Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_vibration"

    const-class v2, Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_soundback"

    const-class v2, Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_use_audio_focus"

    const-class v2, Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_log_level"

    const-class v2, Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_soundback_volume"

    const-class v2, Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_intonation"

    const-class v2, Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_keyboard_echo"

    const-class v2, Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_tts_overlay"

    const-class v2, Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_web_scripts"

    const-class v2, Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_resume_talkback"

    const-class v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_caller_id"

    const-class v2, Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_explore_by_touch"

    const-class v2, Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_auto_scroll"

    const-class v2, Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_single_tap"

    const-class v2, Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_shake_to_read_threshold"

    const-class v2, Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_show_suspension_confirmation_dialog"

    const-class v2, Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_shortcut_up_and_left"

    const-class v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_shortcut_up_and_right"

    const-class v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_shortcut_down_and_left"

    const-class v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_shortcut_down_and_right"

    const-class v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_shortcut_right_and_down"

    const-class v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_shortcut_right_and_up"

    const-class v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_shortcut_left_and_down"

    const-class v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_shortcut_left_and_up"

    const-class v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    const-string v1, "pref_two_part_vertical_gestures"

    const-class v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$OnDefaultsLoadedListener;)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "callback"    # Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$OnDefaultsLoadedListener;

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->mContext:Landroid/content/Context;

    .line 85
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->mPrefs:Landroid/content/SharedPreferences;

    .line 86
    iput-object p2, p0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->mCallback:Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$OnDefaultsLoadedListener;

    .line 87
    new-instance v0, Ljava/io/File;

    const-string v1, "/system/vendor/etc"

    const-string v2, "talkback_defaults.properties"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->mConfigurationFile:Ljava/io/File;

    .line 90
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->mPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$200()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;)Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$OnDefaultsLoadedListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->mCallback:Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$OnDefaultsLoadedListener;

    return-object v0
.end method


# virtual methods
.method public loadOverridesIfNeeded()Z
    .locals 7

    .prologue
    const v6, 0x7f060052

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 101
    iget-object v3, p0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v4, p0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 103
    .local v0, "hasRun":Z
    if-nez v0, :cond_0

    .line 104
    new-instance v3, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$LoadDefaultOverridesTask;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$LoadDefaultOverridesTask;-><init>(Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$1;)V

    new-array v4, v1, [Ljava/io/File;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->mConfigurationFile:Ljava/io/File;

    aput-object v5, v4, v2

    invoke-virtual {v3, v4}, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$LoadDefaultOverridesTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 105
    iget-object v2, p0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2, v3, v6, v1}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->putBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;IZ)V

    .line 110
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method
