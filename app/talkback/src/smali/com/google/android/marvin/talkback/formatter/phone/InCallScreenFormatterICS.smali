.class public final Lcom/google/android/marvin/talkback/formatter/phone/InCallScreenFormatterICS;
.super Ljava/lang/Object;
.source "InCallScreenFormatterICS.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private isPhoneNumber(Ljava/lang/String;)Z
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 95
    const-string v2, "-"

    const-string v3, ""

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 98
    .local v1, "valueNoDashes":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    const/4 v2, 0x1

    .line 101
    :goto_0
    return v2

    .line 100
    :catch_0
    move-exception v0

    .line 101
    .local v0, "iae":Ljava/lang/IllegalArgumentException;
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 49
    invoke-static {p2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 50
    .local v4, "prefs":Landroid/content/SharedPreferences;
    invoke-virtual {p2}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f060034

    const v11, 0x7f0b000a

    invoke-static {v4, v9, v10, v11}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v5

    .line 52
    .local v5, "speakCallerId":Z
    if-nez v5, :cond_1

    .line 88
    :cond_0
    :goto_0
    return v8

    .line 57
    :cond_1
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    .line 58
    .local v0, "eventText":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x6

    if-lt v9, v10, :cond_0

    .line 62
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    .line 63
    .local v6, "title":Ljava/lang/CharSequence;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 64
    invoke-virtual {p3, v6}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 67
    :cond_2
    const/4 v9, 0x3

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 68
    .local v2, "name":Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 70
    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getSpoken()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_3

    :goto_1
    move v8, v7

    goto :goto_0

    :cond_3
    move v7, v8

    goto :goto_1

    .line 73
    :cond_4
    invoke-virtual {p3, v2}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 76
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/google/android/marvin/talkback/formatter/phone/InCallScreenFormatterICS;->isPhoneNumber(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 77
    const/4 v9, 0x5

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 78
    .local v1, "label":Ljava/lang/CharSequence;
    if-eqz v1, :cond_5

    .line 79
    invoke-virtual {p3, v1}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 82
    :cond_5
    const/4 v9, 0x2

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 83
    .local v3, "photo":Ljava/lang/CharSequence;
    if-eqz v3, :cond_6

    .line 84
    invoke-virtual {p3, v3}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 88
    .end local v1    # "label":Ljava/lang/CharSequence;
    .end local v3    # "photo":Ljava/lang/CharSequence;
    :cond_6
    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getSpoken()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_7

    :goto_2
    move v8, v7

    goto :goto_0

    :cond_7
    move v7, v8

    goto :goto_2
.end method
