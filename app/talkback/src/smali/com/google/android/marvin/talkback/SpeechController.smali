.class public Lcom/google/android/marvin/talkback/SpeechController;
.super Ljava/lang/Object;
.source "SpeechController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;,
        Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;,
        Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;,
        Lcom/google/android/marvin/talkback/SpeechController$SpeechControllerListener;
    }
.end annotation


# instance fields
.field private final mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final mAudioManager:Landroid/media/AudioManager;

.field private mCurrentFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

.field private mCurrentFragmentIterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/marvin/talkback/FeedbackFragment;",
            ">;"
        }
    .end annotation
.end field

.field private final mFailoverTts:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

.field private final mFailoverTtsListener:Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

.field private final mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

.field private final mFeedbackQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/marvin/talkback/FeedbackItem;",
            ">;"
        }
    .end annotation
.end field

.field private mFullScreenReadNextCallback:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

.field private final mHandler:Landroid/os/Handler;

.field private mInjectFullScreenReadCallbacks:Z

.field private mIsSpeaking:Z

.field private mLastFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

.field private mNextUtteranceIndex:I

.field private final mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private final mProximityChangeListener:Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;

.field private mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

.field private mScreenIsOn:Z

.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;

.field private final mServiceStateListener:Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;

.field private mSilenceOnProximity:Z

.field private mSpeechListener:Lcom/google/android/marvin/talkback/SpeechController$SpeechControllerListener;

.field private final mSpeechParametersMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSpeechPitch:F

.field private mSpeechRate:F

.field private mSpeechVolume:F

.field private mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

.field private mUseAudioFocus:Z

.field private mUseIntonation:Z

.field private final mUtteranceCompleteActions:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 4
    .param p1, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechParametersMap:Ljava/util/HashMap;

    .line 106
    new-instance v1, Ljava/util/PriorityQueue;

    invoke-direct {v1}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    .line 110
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFeedbackQueue:Ljava/util/LinkedList;

    .line 131
    iput-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFragmentIterator:Ljava/util/Iterator;

    .line 137
    iput-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mLastFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    .line 166
    iput v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mNextUtteranceIndex:I

    .line 965
    new-instance v1, Lcom/google/android/marvin/talkback/SpeechController$1;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/SpeechController$1;-><init>(Lcom/google/android/marvin/talkback/SpeechController;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximityChangeListener:Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;

    .line 975
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mHandler:Landroid/os/Handler;

    .line 980
    new-instance v1, Lcom/google/android/marvin/talkback/SpeechController$2;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/SpeechController$2;-><init>(Lcom/google/android/marvin/talkback/SpeechController;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 992
    new-instance v1, Lcom/google/android/marvin/talkback/SpeechController$3;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/SpeechController$3;-><init>(Lcom/google/android/marvin/talkback/SpeechController;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTtsListener:Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

    .line 1005
    new-instance v1, Lcom/google/android/marvin/talkback/SpeechController$4;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/SpeechController$4;-><init>(Lcom/google/android/marvin/talkback/SpeechController;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 1014
    new-instance v1, Lcom/google/android/marvin/talkback/SpeechController$5;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/SpeechController$5;-><init>(Lcom/google/android/marvin/talkback/SpeechController;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mServiceStateListener:Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;

    .line 187
    iput-object p1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 188
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mServiceStateListener:Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->addServiceStateListener(Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;)V

    .line 190
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioManager:Landroid/media/AudioManager;

    .line 192
    new-instance v1, Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    invoke-direct {v1, p1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTts:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    .line 193
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTts:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTtsListener:Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->addListener(Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;)V

    .line 194
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/CachedFeedbackController;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    .line 196
    iput-boolean v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mInjectFullScreenReadCallbacks:Z

    .line 198
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 199
    .local v0, "prefs":Landroid/content/SharedPreferences;
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 201
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/SpeechController;->reloadPreferences(Landroid/content/SharedPreferences;)V

    .line 203
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mScreenIsOn:Z

    .line 204
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/SpeechController;)Lcom/google/android/marvin/talkback/TalkBackService;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/SpeechController;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/SpeechController;Landroid/content/SharedPreferences;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/SpeechController;
    .param p1, "x1"    # Landroid/content/SharedPreferences;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/SpeechController;->reloadPreferences(Landroid/content/SharedPreferences;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/SpeechController;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/SpeechController;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/SpeechController;->onTtsInitialized(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/SpeechController;Ljava/lang/String;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/SpeechController;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/SpeechController;->onFragmentCompleted(Ljava/lang/String;ZZ)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/SpeechController;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/SpeechController;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/SpeechController;->setProximitySensorState(Z)V

    return-void
.end method

.method private clearCurrentAndQueuedUtterances()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 748
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFeedbackQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 749
    iput-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFragmentIterator:Ljava/util/Iterator;

    .line 751
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    if-eqz v1, :cond_0

    .line 752
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/FeedbackItem;->getUtteranceId()Ljava/lang/String;

    move-result-object v0

    .line 753
    .local v0, "utteranceId":Ljava/lang/String;
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/SpeechController;->onFragmentCompleted(Ljava/lang/String;ZZ)V

    .line 754
    iput-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    .line 756
    .end local v0    # "utteranceId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private clearUtteranceCompletionActions(Z)V
    .locals 4
    .param p1, "execute"    # Z

    .prologue
    .line 764
    if-nez p1, :cond_1

    .line 765
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->clear()V

    .line 778
    :cond_0
    return-void

    .line 769
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 770
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;

    iget-object v0, v1, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;->runnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .line 771
    .local v0, "runnable":Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;
    if-eqz v0, :cond_1

    .line 772
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;

    const/4 v3, 0x3

    invoke-direct {v2, v0, v3}, Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;-><init>(Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private getNextUtteranceId()I
    .locals 2

    .prologue
    .line 503
    iget v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mNextUtteranceIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mNextUtteranceIndex:I

    return v0
.end method

.method private handleSpeechCompleted()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 731
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mScreenIsOn:Z

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/SpeechController;->setProximitySensorState(Z)V

    .line 733
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUseAudioFocus:Z

    if-eqz v0, :cond_0

    .line 734
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 737
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mIsSpeaking:Z

    if-nez v0, :cond_1

    .line 738
    const/4 v0, 0x6

    const-string v1, "Completed speech while already completed!"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 741
    :cond_1
    iput-boolean v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mIsSpeaking:Z

    .line 742
    return-void
.end method

.method private handleSpeechStarting()V
    .locals 4

    .prologue
    const/4 v2, 0x3

    const/4 v3, 0x1

    .line 708
    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/SpeechController;->setProximitySensorState(Z)V

    .line 710
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUseAudioFocus:Z

    if-eqz v0, :cond_0

    .line 711
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1, v2, v2}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 715
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mIsSpeaking:Z

    if-eqz v0, :cond_1

    .line 716
    const/4 v0, 0x6

    const-string v1, "Started speech while already speaking!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 719
    :cond_1
    iput-boolean v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mIsSpeaking:Z

    .line 720
    return-void
.end method

.method private onFragmentCompleted(Ljava/lang/String;ZZ)V
    .locals 4
    .param p1, "utteranceId"    # Ljava/lang/String;
    .param p2, "success"    # Z
    .param p3, "advance"    # Z

    .prologue
    .line 789
    invoke-static {p1}, Lcom/google/android/marvin/talkback/SpeechController;->parseUtteranceId(Ljava/lang/String;)I

    move-result v2

    .line 790
    .local v2, "utteranceIndex":I
    iget-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/FeedbackItem;->getUtteranceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v0, 0x1

    .line 795
    .local v0, "interrupted":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 796
    const/4 v1, 0x3

    .line 804
    .local v1, "status":I
    :goto_1
    const/4 v3, 0x4

    if-ne v1, v3, :cond_3

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->processNextFragmentInternal()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 812
    :goto_2
    return-void

    .line 790
    .end local v0    # "interrupted":Z
    .end local v1    # "status":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 797
    .restart local v0    # "interrupted":Z
    :cond_1
    if-eqz p2, :cond_2

    .line 798
    const/4 v1, 0x4

    .restart local v1    # "status":I
    goto :goto_1

    .line 800
    .end local v1    # "status":I
    :cond_2
    const/4 v1, 0x1

    .restart local v1    # "status":I
    goto :goto_1

    .line 810
    :cond_3
    invoke-direct {p0, v2, v1, v0, p3}, Lcom/google/android/marvin/talkback/SpeechController;->onUtteranceCompleted(IIZZ)V

    goto :goto_2
.end method

.method private onTtsInitialized(Z)V
    .locals 2
    .param p1, "wasSwitchingEngines"    # Z

    .prologue
    const/4 v1, 0x0

    .line 855
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    if-eqz v0, :cond_0

    .line 856
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/FeedbackItem;->getUtteranceId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/marvin/talkback/SpeechController;->onFragmentCompleted(Ljava/lang/String;ZZ)V

    .line 858
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    .line 861
    :cond_0
    if-eqz p1, :cond_2

    .line 862
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->speakCurrentEngine()V

    .line 866
    :cond_1
    :goto_0
    return-void

    .line 863
    :cond_2
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFeedbackQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 864
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->speakNextItem()Z

    goto :goto_0
.end method

.method private onUtteranceCompleted(IIZZ)V
    .locals 6
    .param p1, "utteranceIndex"    # I
    .param p2, "status"    # I
    .param p3, "interrupted"    # Z
    .param p4, "advance"    # Z

    .prologue
    const/4 v5, 0x2

    .line 827
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;

    iget v1, v1, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;->utteranceIndex:I

    if-gt v1, p1, :cond_1

    .line 828
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;

    iget-object v0, v1, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;->runnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .line 829
    .local v0, "runnable":Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;
    if-eqz v0, :cond_0

    .line 830
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;

    invoke-direct {v2, v0, p2}, Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;-><init>(Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 834
    .end local v0    # "runnable":Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;
    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechListener:Lcom/google/android/marvin/talkback/SpeechController$SpeechControllerListener;

    if-eqz v1, :cond_2

    .line 835
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechListener:Lcom/google/android/marvin/talkback/SpeechController$SpeechControllerListener;

    invoke-interface {v1, p1, p2}, Lcom/google/android/marvin/talkback/SpeechController$SpeechControllerListener;->onUtteranceCompleted(II)V

    .line 838
    :cond_2
    if-eqz p3, :cond_4

    .line 842
    const-string v1, "Interrupted %d with %s"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/FeedbackItem;->getUtteranceId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p0, v5, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 850
    :cond_3
    :goto_1
    return-void

    .line 847
    :cond_4
    if-eqz p4, :cond_3

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->speakNextItem()Z

    move-result v1

    if-nez v1, :cond_3

    .line 848
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->handleSpeechCompleted()V

    goto :goto_1
.end method

.method private static parseFloatParam(Ljava/util/HashMap;Ljava/lang/String;F)F
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "F)F"
        }
    .end annotation

    .prologue
    .line 905
    .local p0, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 907
    .local v1, "value":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 917
    .end local p2    # "defaultValue":F
    :goto_0
    return p2

    .line 912
    .restart local p2    # "defaultValue":F
    :cond_0
    :try_start_0
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    goto :goto_0

    .line 913
    :catch_0
    move-exception v0

    .line 914
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method static parseUtteranceId(Ljava/lang/String;)I
    .locals 7
    .param p0, "utteranceId"    # Ljava/lang/String;

    .prologue
    const/4 v1, -0x1

    .line 686
    const-string v2, "talkback_"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 687
    const-class v2, Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v3, 0x6

    const-string v4, "Bad utterance ID: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    invoke-static {v2, v3, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 695
    :goto_0
    return v1

    .line 692
    :cond_0
    :try_start_0
    const-string v2, "talkback_"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 693
    :catch_0
    move-exception v0

    .line 694
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method private playEarconsFromFragment(Lcom/google/android/marvin/talkback/FeedbackFragment;)V
    .locals 7
    .param p1, "fragment"    # Lcom/google/android/marvin/talkback/FeedbackFragment;

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 660
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/FeedbackFragment;->getNonSpeechParams()Landroid/os/Bundle;

    move-result-object v4

    .line 661
    .local v4, "nonSpeechParams":Landroid/os/Bundle;
    const-string v5, "earcon_rate"

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v0

    .line 662
    .local v0, "earconRate":F
    const-string v5, "earcon_volume"

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v1

    .line 665
    .local v1, "earconVolume":F
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/FeedbackFragment;->getEarcons()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 666
    .local v3, "keyResId":I
    iget-object v5, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v0, v1, v6}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(IFFF)Z

    goto :goto_0

    .line 668
    .end local v3    # "keyResId":I
    :cond_0
    return-void
.end method

.method private playHapticsFromFragment(Lcom/google/android/marvin/talkback/FeedbackFragment;)V
    .locals 3
    .param p1, "fragment"    # Lcom/google/android/marvin/talkback/FeedbackFragment;

    .prologue
    .line 676
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/FeedbackFragment;->getHaptics()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 677
    .local v1, "keyResId":I
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    invoke-virtual {v2, v1}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playHaptic(I)Z

    goto :goto_0

    .line 679
    .end local v1    # "keyResId":I
    :cond_0
    return-void
.end method

.method private processNextFragmentInternal()Z
    .locals 15

    .prologue
    const/4 v5, 0x3

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 605
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFragmentIterator:Ljava/util/Iterator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFragmentIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v13

    .line 651
    :goto_0
    return v0

    .line 609
    :cond_1
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFragmentIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/marvin/talkback/FeedbackFragment;

    .line 610
    .local v7, "fragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    invoke-direct {p0, v7}, Lcom/google/android/marvin/talkback/SpeechController;->playEarconsFromFragment(Lcom/google/android/marvin/talkback/FeedbackFragment;)V

    .line 611
    invoke-direct {p0, v7}, Lcom/google/android/marvin/talkback/SpeechController;->playHapticsFromFragment(Lcom/google/android/marvin/talkback/FeedbackFragment;)V

    .line 614
    iget-object v4, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechParametersMap:Ljava/util/HashMap;

    .line 615
    .local v4, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 618
    invoke-virtual {v7}, Lcom/google/android/marvin/talkback/FeedbackFragment;->getSpeechParams()Landroid/os/Bundle;

    move-result-object v11

    .line 619
    .local v11, "speechParams":Landroid/os/Bundle;
    invoke-virtual {v11}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 620
    .local v9, "key":Ljava/lang/String;
    invoke-virtual {v11, v9}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v9, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 624
    .end local v9    # "key":Ljava/lang/String;
    :cond_2
    const-string v0, "utteranceId"

    iget-object v14, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    invoke-virtual {v14}, Lcom/google/android/marvin/talkback/FeedbackItem;->getUtteranceId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v0, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 625
    const-string v0, "streamType"

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v0, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 626
    const-string v0, "volume"

    iget v14, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechVolume:F

    invoke-static {v14}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v0, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 628
    iget v14, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechPitch:F

    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUseIntonation:Z

    if-eqz v0, :cond_6

    const-string v0, "pitch"

    invoke-static {v4, v0, v6}, Lcom/google/android/marvin/talkback/SpeechController;->parseFloatParam(Ljava/util/HashMap;Ljava/lang/String;F)F

    move-result v0

    :goto_2
    mul-float v2, v14, v0

    .line 630
    .local v2, "pitch":F
    iget v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechRate:F

    iget-boolean v14, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUseIntonation:Z

    if-eqz v14, :cond_3

    const-string v14, "rate"

    invoke-static {v4, v14, v6}, Lcom/google/android/marvin/talkback/SpeechController;->parseFloatParam(Ljava/util/HashMap;Ljava/lang/String;F)F

    move-result v6

    :cond_3
    mul-float v3, v0, v6

    .line 633
    .local v3, "rate":F
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/SpeechController;->shouldSilenceSpeech(Lcom/google/android/marvin/talkback/FeedbackItem;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v7}, Lcom/google/android/marvin/talkback/FeedbackFragment;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 634
    :cond_4
    const/4 v1, 0x0

    .line 639
    .local v1, "text":Ljava/lang/CharSequence;
    :goto_3
    if-nez v1, :cond_8

    const/4 v10, 0x0

    .line 640
    .local v10, "logText":Ljava/lang/String;
    :goto_4
    const/4 v0, 0x2

    const-string v6, "Speaking fragment text \"%s\""

    new-array v14, v12, [Ljava/lang/Object;

    aput-object v10, v14, v13

    invoke-static {p0, v0, v6, v14}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 645
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTts:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    iget v6, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechVolume:F

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->speak(Ljava/lang/CharSequence;FFLjava/util/HashMap;IF)V

    .line 647
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    if-eqz v0, :cond_5

    .line 648
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/TextToSpeechOverlay;->speak(Ljava/lang/CharSequence;)V

    :cond_5
    move v0, v12

    .line 651
    goto/16 :goto_0

    .end local v1    # "text":Ljava/lang/CharSequence;
    .end local v2    # "pitch":F
    .end local v3    # "rate":F
    .end local v10    # "logText":Ljava/lang/String;
    :cond_6
    move v0, v6

    .line 628
    goto :goto_2

    .line 636
    .restart local v2    # "pitch":F
    .restart local v3    # "rate":F
    :cond_7
    invoke-virtual {v7}, Lcom/google/android/marvin/talkback/FeedbackFragment;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .restart local v1    # "text":Ljava/lang/CharSequence;
    goto :goto_3

    .line 639
    :cond_8
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    goto :goto_4
.end method

.method private reloadPreferences(Landroid/content/SharedPreferences;)V
    .locals 4
    .param p1, "prefs"    # Landroid/content/SharedPreferences;

    .prologue
    .line 513
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 515
    .local v0, "res":Landroid/content/res/Resources;
    const v2, 0x7f06002f

    const v3, 0x7f0b0007

    invoke-static {p1, v0, v2, v3}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v1

    .line 518
    .local v1, "ttsOverlayEnabled":Z
    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/SpeechController;->setOverlayEnabled(Z)V

    .line 520
    const v2, 0x7f06002d

    const v3, 0x7f0b0006

    invoke-static {p1, v0, v2, v3}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUseIntonation:Z

    .line 522
    const v2, 0x7f060024

    const v3, 0x7f060069

    invoke-static {p1, v0, v2, v3}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getFloatFromStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)F

    move-result v2

    iput v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechPitch:F

    .line 524
    const v2, 0x7f060023

    const v3, 0x7f060068

    invoke-static {p1, v0, v2, v3}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getFloatFromStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)F

    move-result v2

    iput v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechRate:F

    .line 526
    const v2, 0x7f060029

    const v3, 0x7f0b0005

    invoke-static {p1, v0, v2, v3}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUseAudioFocus:Z

    .line 530
    const v2, 0x7f060025

    const v3, 0x7f06006a

    invoke-static {p1, v0, v2, v3}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getIntFromStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechVolume:F

    .line 533
    iget-boolean v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUseAudioFocus:Z

    if-nez v2, :cond_0

    .line 534
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 536
    :cond_0
    return-void
.end method

.method private setOverlayEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 539
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    if-nez v0, :cond_1

    .line 540
    new-instance v0, Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {v0, v1}, Lcom/google/android/marvin/talkback/TextToSpeechOverlay;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    .line 545
    :cond_0
    :goto_0
    return-void

    .line 541
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TextToSpeechOverlay;->hide()V

    .line 543
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    goto :goto_0
.end method

.method private setProximitySensorState(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 932
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

    if-eqz v0, :cond_2

    .line 934
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSilenceOnProximity:Z

    if-nez v0, :cond_1

    .line 935
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

    invoke-virtual {v0}, Lcom/google/android/marvin/utils/ProximitySensor;->stop()V

    .line 936
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

    .line 960
    :cond_0
    :goto_0
    return-void

    .line 940
    :cond_1
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v0

    if-nez v0, :cond_3

    .line 941
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

    invoke-virtual {v0}, Lcom/google/android/marvin/utils/ProximitySensor;->stop()V

    goto :goto_0

    .line 946
    :cond_2
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSilenceOnProximity:Z

    if-eqz v0, :cond_0

    .line 947
    new-instance v0, Lcom/google/android/marvin/utils/ProximitySensor;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {v0, v1}, Lcom/google/android/marvin/utils/ProximitySensor;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

    .line 948
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximityChangeListener:Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/utils/ProximitySensor;->setProximityChangeListener(Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;)V

    .line 955
    :cond_3
    if-eqz p1, :cond_4

    .line 956
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

    invoke-virtual {v0}, Lcom/google/android/marvin/utils/ProximitySensor;->start()V

    goto :goto_0

    .line 958
    :cond_4
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

    invoke-virtual {v0}, Lcom/google/android/marvin/utils/ProximitySensor;->stop()V

    goto :goto_0
.end method

.method private shouldSilenceSpeech(Lcom/google/android/marvin/talkback/FeedbackItem;)Z
    .locals 1
    .param p1, "item"    # Lcom/google/android/marvin/talkback/FeedbackItem;

    .prologue
    .line 560
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/android/marvin/talkback/FeedbackItem;->hasFlag(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/googlecode/eyesfree/compat/media/AudioSystemCompatUtils;->isSourceActive(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 563
    const/4 v0, 0x1

    .line 566
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private speak(Lcom/google/android/marvin/talkback/FeedbackItem;ILcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V
    .locals 11
    .param p1, "item"    # Lcom/google/android/marvin/talkback/FeedbackItem;
    .param p2, "queueMode"    # I
    .param p3, "completedAction"    # Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .prologue
    const/4 v10, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 386
    const/16 v6, 0x10

    invoke-virtual {p1, v6}, Lcom/google/android/marvin/talkback/FeedbackItem;->hasFlag(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 387
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/FeedbackItem;->getFragments()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/talkback/FeedbackFragment;

    .line 388
    .local v1, "fragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/SpeechController;->playEarconsFromFragment(Lcom/google/android/marvin/talkback/FeedbackFragment;)V

    .line 389
    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/SpeechController;->playHapticsFromFragment(Lcom/google/android/marvin/talkback/FeedbackFragment;)V

    goto :goto_0

    .line 395
    .end local v1    # "fragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    if-ne p2, v10, :cond_2

    move v6, v7

    :goto_1
    invoke-virtual {p1, v6}, Lcom/google/android/marvin/talkback/FeedbackItem;->setUninterruptible(Z)V

    .line 396
    invoke-virtual {p1, p3}, Lcom/google/android/marvin/talkback/FeedbackItem;->setCompletedAction(Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    .line 399
    if-eq p2, v7, :cond_3

    if-eq p2, v10, :cond_3

    move v5, v7

    .line 402
    .local v5, "willClearInterruptibleFromQueue":Z
    :goto_2
    if-eqz v5, :cond_4

    .line 404
    iget-object v6, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFeedbackQueue:Ljava/util/LinkedList;

    invoke-virtual {v6, v8}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v3

    .line 405
    .local v3, "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/google/android/marvin/talkback/FeedbackItem;>;"
    :cond_1
    :goto_3
    invoke-interface {v3}, Ljava/util/ListIterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 406
    invoke-interface {v3}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/marvin/talkback/FeedbackItem;

    .line 407
    .local v0, "currentItem":Lcom/google/android/marvin/talkback/FeedbackItem;
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/FeedbackItem;->isUninterruptible()Z

    move-result v6

    if-nez v6, :cond_1

    .line 408
    invoke-interface {v3}, Ljava/util/ListIterator;->remove()V

    .line 409
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/FeedbackItem;->getCompletedAction()Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    move-result-object v4

    .line 411
    .local v4, "queuedItemCompletedAction":Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;
    if-eqz v4, :cond_1

    .line 412
    const/4 v6, 0x3

    invoke-interface {v4, v6}, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;->run(I)V

    goto :goto_3

    .end local v0    # "currentItem":Lcom/google/android/marvin/talkback/FeedbackItem;
    .end local v3    # "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/google/android/marvin/talkback/FeedbackItem;>;"
    .end local v4    # "queuedItemCompletedAction":Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;
    .end local v5    # "willClearInterruptibleFromQueue":Z
    :cond_2
    move v6, v8

    .line 395
    goto :goto_1

    :cond_3
    move v5, v8

    .line 399
    goto :goto_2

    .line 418
    .restart local v5    # "willClearInterruptibleFromQueue":Z
    :cond_4
    iget-object v6, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFeedbackQueue:Ljava/util/LinkedList;

    invoke-virtual {v6, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 421
    iget-object v6, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTts:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    invoke-virtual {v6}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->isReady()Z

    move-result v6

    if-nez v6, :cond_6

    .line 422
    const/4 v6, 0x6

    const-string v7, "Attempted to speak before TTS was initialized."

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {p0, v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 434
    .end local v5    # "willClearInterruptibleFromQueue":Z
    :cond_5
    :goto_4
    return-void

    .line 426
    .restart local v5    # "willClearInterruptibleFromQueue":Z
    :cond_6
    iget-object v6, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    if-eqz v6, :cond_7

    if-eqz v5, :cond_8

    iget-object v6, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    invoke-virtual {v6}, Lcom/google/android/marvin/talkback/FeedbackItem;->isUninterruptible()Z

    move-result v6

    if-nez v6, :cond_8

    .line 428
    :cond_7
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFragmentIterator:Ljava/util/Iterator;

    .line 429
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->speakNextItem()Z

    goto :goto_4

    .line 431
    :cond_8
    const-string v6, "Queued speech item, waiting for \"%s\""

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    invoke-virtual {v9}, Lcom/google/android/marvin/talkback/FeedbackItem;->getUtteranceId()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {p0, v10, v6, v7}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4
.end method

.method private speakCurrentEngine()V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 311
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTts:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    invoke-virtual {v0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->getEngineLabel()Ljava/lang/CharSequence;

    move-result-object v8

    .line 312
    .local v8, "engineLabel":Ljava/lang/CharSequence;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    :goto_0
    return-void

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0600a3

    new-array v5, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v8, v5, v6

    invoke-virtual {v0, v3, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 318
    .local v1, "text":Ljava/lang/String;
    const/4 v5, 0x2

    move-object v0, p0

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;Ljava/util/Set;Ljava/util/Set;IILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private speakNextItem()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 875
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    .line 876
    .local v1, "previousItem":Lcom/google/android/marvin/talkback/FeedbackItem;
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFeedbackQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    .line 879
    .local v0, "nextItem":Lcom/google/android/marvin/talkback/FeedbackItem;
    :goto_0
    iput-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    .line 881
    if-nez v0, :cond_1

    .line 882
    const/4 v2, 0x2

    const-string v4, "No next item, stopping speech queue"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {p0, v2, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    move v2, v3

    .line 892
    :goto_1
    return v2

    .line 876
    .end local v0    # "nextItem":Lcom/google/android/marvin/talkback/FeedbackItem;
    :cond_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFeedbackQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/marvin/talkback/FeedbackItem;

    move-object v0, v2

    goto :goto_0

    .line 886
    .restart local v0    # "nextItem":Lcom/google/android/marvin/talkback/FeedbackItem;
    :cond_1
    if-nez v1, :cond_2

    .line 887
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->handleSpeechStarting()V

    .line 890
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/FeedbackItem;->getFragments()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentFragmentIterator:Ljava/util/Iterator;

    .line 891
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/SpeechController;->speakNextItemInternal(Lcom/google/android/marvin/talkback/FeedbackItem;)V

    .line 892
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private speakNextItemInternal(Lcom/google/android/marvin/talkback/FeedbackItem;)V
    .locals 5
    .param p1, "item"    # Lcom/google/android/marvin/talkback/FeedbackItem;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    .line 579
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->getNextUtteranceId()I

    move-result v2

    .line 580
    .local v2, "utteranceIndex":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "talkback_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 581
    .local v1, "utteranceId":Ljava/lang/String;
    invoke-virtual {p1, v1}, Lcom/google/android/marvin/talkback/FeedbackItem;->setUtteranceId(Ljava/lang/String;)V

    .line 583
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/FeedbackItem;->getCompletedAction()Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    move-result-object v0

    .line 584
    .local v0, "completedAction":Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;
    if-eqz v0, :cond_0

    .line 585
    invoke-virtual {p0, v2, v0}, Lcom/google/android/marvin/talkback/SpeechController;->addUtteranceCompleteAction(ILcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    .line 588
    :cond_0
    iget-boolean v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mInjectFullScreenReadCallbacks:Z

    if-eqz v3, :cond_1

    const/16 v3, 0x8

    invoke-virtual {p1, v3}, Lcom/google/android/marvin/talkback/FeedbackItem;->hasFlag(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 590
    iget-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFullScreenReadNextCallback:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    invoke-virtual {p0, v2, v3}, Lcom/google/android/marvin/talkback/SpeechController;->addUtteranceCompleteAction(ILcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    .line 593
    :cond_1
    if-eqz p1, :cond_2

    const/4 v3, 0x2

    invoke-virtual {p1, v3}, Lcom/google/android/marvin/talkback/FeedbackItem;->hasFlag(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 594
    iput-object p1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mLastFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    .line 597
    :cond_2
    iget-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechListener:Lcom/google/android/marvin/talkback/SpeechController$SpeechControllerListener;

    if-eqz v3, :cond_3

    .line 598
    iget-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechListener:Lcom/google/android/marvin/talkback/SpeechController$SpeechControllerListener;

    invoke-interface {v3, v2}, Lcom/google/android/marvin/talkback/SpeechController$SpeechControllerListener;->onUtteranceStarted(I)V

    .line 601
    :cond_3
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->processNextFragmentInternal()Z

    .line 602
    return-void
.end method


# virtual methods
.method public addUtteranceCompleteAction(ILcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "runnable"    # Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .prologue
    .line 445
    new-instance v0, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;

    invoke-direct {v0, p1, p2}, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;-><init>(ILcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    .line 446
    .local v0, "action":Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 447
    return-void
.end method

.method getFailoverTts()Lcom/google/android/marvin/utils/FailoverTextToSpeech;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTts:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    return-object v0
.end method

.method public interrupt()V
    .locals 1

    .prologue
    .line 471
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->clearCurrentAndQueuedUtterances()V

    .line 474
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/SpeechController;->clearUtteranceCompletionActions(Z)V

    .line 477
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTts:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    invoke-virtual {v0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->stopAll()V

    .line 478
    return-void
.end method

.method public isSpeaking()Z
    .locals 1

    .prologue
    .line 210
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mIsSpeaking:Z

    return v0
.end method

.method public peekNextUtteranceId()I
    .locals 1

    .prologue
    .line 496
    iget v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mNextUtteranceIndex:I

    return v0
.end method

.method public removeUtteranceCompleteAction(Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V
    .locals 3
    .param p1, "runnable"    # Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .prologue
    .line 456
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 458
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 459
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;

    .line 460
    .local v0, "action":Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;
    iget-object v2, v0, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;->runnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    if-ne v2, p1, :cond_0

    .line 461
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 464
    .end local v0    # "action":Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;
    :cond_1
    return-void
.end method

.method public repeatLastUtterance()Z
    .locals 3

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mLastFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    if-nez v0, :cond_0

    .line 272
    const/4 v0, 0x0

    .line 277
    :goto_0
    return v0

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mLastFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/FeedbackItem;->addFlag(I)V

    .line 276
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mLastFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Lcom/google/android/marvin/talkback/FeedbackItem;ILcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    .line 277
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setScreenIsOn(Z)V
    .locals 1
    .param p1, "screenIsOn"    # Z

    .prologue
    .line 235
    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mScreenIsOn:Z

    .line 239
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mScreenIsOn:Z

    if-eqz v0, :cond_0

    .line 240
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/SpeechController;->setProximitySensorState(Z)V

    .line 242
    :cond_0
    return-void
.end method

.method public setShouldInjectAutoReadingCallbacks(ZLcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V
    .locals 1
    .param p1, "shouldInject"    # Z
    .param p2, "nextItemCallback"    # Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .prologue
    .line 250
    if-eqz p1, :cond_1

    move-object v0, p2

    :goto_0
    iput-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFullScreenReadNextCallback:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .line 251
    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mInjectFullScreenReadCallbacks:Z

    .line 253
    if-nez p1, :cond_0

    .line 254
    invoke-virtual {p0, p2}, Lcom/google/android/marvin/talkback/SpeechController;->removeUtteranceCompleteAction(Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    .line 256
    :cond_0
    return-void

    .line 250
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSilenceOnProximity(Z)V
    .locals 1
    .param p1, "silenceOnProximity"    # Z

    .prologue
    .line 225
    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSilenceOnProximity:Z

    .line 228
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSilenceOnProximity:Z

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/SpeechController;->setProximitySensorState(Z)V

    .line 229
    return-void
.end method

.method public shutdown()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 484
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/SpeechController;->interrupt()V

    .line 486
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTts:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    invoke-virtual {v0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->shutdown()V

    .line 488
    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/SpeechController;->setOverlayEnabled(Z)V

    .line 489
    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/SpeechController;->setProximitySensorState(Z)V

    .line 490
    return-void
.end method

.method public speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V
    .locals 8
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "queueMode"    # I
    .param p3, "flags"    # I
    .param p4, "speechParams"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 325
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move v4, p2

    move v5, p3

    move-object v6, p4

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;Ljava/util/Set;Ljava/util/Set;IILandroid/os/Bundle;Landroid/os/Bundle;)V

    .line 326
    return-void
.end method

.method public speak(Ljava/lang/CharSequence;Ljava/util/Set;Ljava/util/Set;IILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 9
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p4, "queueMode"    # I
    .param p5, "flags"    # I
    .param p6, "speechParams"    # Landroid/os/Bundle;
    .param p7, "nonSpeechParams"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;II",
            "Landroid/os/Bundle;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 333
    .local p2, "earcons":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local p3, "haptics":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;Ljava/util/Set;Ljava/util/Set;IILandroid/os/Bundle;Landroid/os/Bundle;Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    .line 334
    return-void
.end method

.method public speak(Ljava/lang/CharSequence;Ljava/util/Set;Ljava/util/Set;IILandroid/os/Bundle;Landroid/os/Bundle;Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V
    .locals 9
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p4, "queueMode"    # I
    .param p5, "flags"    # I
    .param p6, "speechParams"    # Landroid/os/Bundle;
    .param p7, "nonSpeechParams"    # Landroid/os/Bundle;
    .param p8, "completedAction"    # Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;II",
            "Landroid/os/Bundle;",
            "Landroid/os/Bundle;",
            "Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 372
    .local p2, "earcons":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local p3, "haptics":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-static/range {v1 .. v7}, Lcom/google/android/marvin/talkback/FeedbackProcessingUtils;->generateFeedbackItemFromInput(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/util/Set;Ljava/util/Set;ILandroid/os/Bundle;Landroid/os/Bundle;)Lcom/google/android/marvin/talkback/FeedbackItem;

    move-result-object v8

    .line 375
    .local v8, "pendingItem":Lcom/google/android/marvin/talkback/FeedbackItem;
    move-object/from16 v0, p8

    invoke-direct {p0, v8, p4, v0}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Lcom/google/android/marvin/talkback/FeedbackItem;ILcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    .line 376
    return-void
.end method

.method public spellLastUtterance()Z
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 284
    iget-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mLastFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    if-nez v3, :cond_1

    .line 304
    :cond_0
    :goto_0
    return v0

    .line 288
    :cond_1
    iget-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mLastFeedbackItem:Lcom/google/android/marvin/talkback/FeedbackItem;

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/FeedbackItem;->getAggregateText()Ljava/lang/CharSequence;

    move-result-object v9

    .line 289
    .local v9, "aggregateText":Ljava/lang/CharSequence;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 293
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 295
    .local v1, "builder":Landroid/text/SpannableStringBuilder;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-ge v11, v3, :cond_2

    .line 296
    iget-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-interface {v9, v11}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-static {v3, v4}, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->getCleanValueFor(Landroid/content/Context;C)Ljava/lang/String;

    move-result-object v10

    .line 299
    .local v10, "cleanedChar":Ljava/lang/String;
    new-array v3, v12, [Ljava/lang/CharSequence;

    aput-object v10, v3, v0

    invoke-static {v1, v3}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 295
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 302
    .end local v10    # "cleanedChar":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x3

    const/4 v5, 0x2

    move-object v0, p0

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;Ljava/util/Set;Ljava/util/Set;IILandroid/os/Bundle;Landroid/os/Bundle;Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    move v0, v12

    .line 304
    goto :goto_0
.end method
