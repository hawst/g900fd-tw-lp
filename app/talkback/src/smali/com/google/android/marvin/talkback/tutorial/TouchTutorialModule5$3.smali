.class Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$3;
.super Landroid/view/View$AccessibilityDelegate;
.source "TouchTutorialModule5.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$3;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 2
    .param p1, "host"    # Landroid/view/View;
    .param p2, "action"    # I
    .param p3, "args"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 95
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$3;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->access$000(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)Landroid/widget/EditText;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/16 v0, 0x10

    if-ne p2, v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$3;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->access$000(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$3;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->access$100(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$3;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$3$1;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$3$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$3;)V

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->installTriggerDelayedWithFeedback(Ljava/lang/Runnable;)V

    .line 106
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/view/View$AccessibilityDelegate;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method
