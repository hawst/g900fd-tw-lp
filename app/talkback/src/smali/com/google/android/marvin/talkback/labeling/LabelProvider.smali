.class public Lcom/google/android/marvin/talkback/labeling/LabelProvider;
.super Landroid/content/ContentProvider;
.source "LabelProvider.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/labeling/LabelProvider$LabelsDatabaseOpenHelper;
    }
.end annotation


# static fields
.field public static final LABELS_CONTENT_URI:Landroid/net/Uri;

.field private static final LABELS_ID_CONTENT_URI:Landroid/net/Uri;

.field public static final PACKAGE_SUMMARY_URI:Landroid/net/Uri;

.field protected static final sUriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mDatabase:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 53
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.google.android.marvin.talkback.providers.LabelProvider"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "labels"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->LABELS_CONTENT_URI:Landroid/net/Uri;

    .line 58
    sget-object v0, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->LABELS_CONTENT_URI:Landroid/net/Uri;

    const-string v1, "#"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->LABELS_ID_CONTENT_URI:Landroid/net/Uri;

    .line 60
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.google.android.marvin.talkback.providers.LabelProvider"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "packageSummary"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->PACKAGE_SUMMARY_URI:Landroid/net/Uri;

    .line 73
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 75
    sget-object v0, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.marvin.talkback.providers.LabelProvider"

    sget-object v2, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->LABELS_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 76
    sget-object v0, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.marvin.talkback.providers.LabelProvider"

    sget-object v2, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->LABELS_ID_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 77
    sget-object v0, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.marvin.talkback.providers.LabelProvider"

    sget-object v2, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->PACKAGE_SUMMARY_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 78
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 325
    return-void
.end method

.method private combineSelectionAndWhere(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "where"    # Ljava/lang/String;

    .prologue
    .line 299
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    .end local p1    # "selection":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 301
    .restart local p1    # "selection":Ljava/lang/String;
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object p1, p2

    .line 302
    goto :goto_0

    .line 305
    :cond_1
    const-string v0, "(%s) AND (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private initializeDatabaseIfNull()V
    .locals 2

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    .line 316
    new-instance v0, Lcom/google/android/marvin/talkback/labeling/LabelProvider$LabelsDatabaseOpenHelper;

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/marvin/talkback/labeling/LabelProvider$LabelsDatabaseOpenHelper;-><init>(Lcom/google/android/marvin/talkback/labeling/LabelProvider;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/labeling/LabelProvider$LabelsDatabaseOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    .line 318
    :cond_0
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v7, 0x5

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 252
    if-nez p1, :cond_0

    .line 253
    const-string v5, "Unknown URI: %s"

    new-array v6, v8, [Ljava/lang/Object;

    aput-object p1, v6, v3

    invoke-static {p0, v7, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 279
    :goto_0
    return v3

    .line 257
    :cond_0
    sget-object v5, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 278
    const-string v5, "Unknown URI: %s"

    new-array v6, v8, [Ljava/lang/Object;

    aput-object p1, v6, v3

    invoke-static {p0, v7, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 259
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->initializeDatabaseIfNull()V

    .line 261
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 264
    .local v2, "labelIdString":Ljava/lang/String;
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 270
    .local v1, "labelId":I
    const-string v5, "%s = %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, "_id"

    aput-object v7, v6, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 271
    .local v4, "where":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "labels"

    invoke-direct {p0, p2, v4}, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->combineSelectionAndWhere(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 274
    .local v3, "result":I
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, p1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 265
    .end local v1    # "labelId":I
    .end local v3    # "result":I
    .end local v4    # "where":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 266
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v5, "Unknown URI: %s"

    new-array v6, v8, [Ljava/lang/Object;

    aput-object p1, v6, v3

    invoke-static {p0, v7, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 257
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 89
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x5

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 102
    if-nez p1, :cond_1

    .line 103
    const-string v3, "Unknown URI: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v6

    invoke-static {p0, v7, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 130
    :cond_0
    :goto_0
    return-object v2

    .line 107
    :cond_1
    sget-object v3, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 129
    const-string v3, "Unknown URI: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v6

    invoke-static {p0, v7, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 109
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->initializeDatabaseIfNull()V

    .line 111
    if-eqz p2, :cond_0

    .line 115
    const-string v3, "_id"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 116
    const-string v3, "Label ID must be assigned by the database."

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p0, v7, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 120
    :cond_2
    iget-object v3, p0, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "labels"

    invoke-virtual {v3, v4, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 122
    .local v0, "rowId":J
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-gez v3, :cond_3

    .line 123
    const-string v3, "Failed to insert label."

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p0, v7, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 126
    :cond_3
    sget-object v2, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->LABELS_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    goto :goto_0

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 13
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 151
    if-nez p1, :cond_0

    .line 152
    const/4 v1, 0x5

    const-string v2, "Unknown URI: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 153
    const/4 v8, 0x0

    .line 195
    :goto_0
    return-object v8

    .line 156
    :cond_0
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 157
    .local v0, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "labels"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 159
    const/4 v5, 0x0

    .line 161
    .local v5, "groupBy":Ljava/lang/String;
    sget-object v1, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 186
    const/4 v1, 0x5

    const-string v2, "Unknown URI: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 187
    const/4 v8, 0x0

    goto :goto_0

    .line 163
    :pswitch_0
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 164
    const-string p5, "_id"

    .line 190
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->initializeDatabaseIfNull()V

    .line 192
    iget-object v1, p0, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v6, 0x0

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 195
    .local v8, "cursor":Landroid/database/Cursor;
    goto :goto_0

    .line 168
    .end local v8    # "cursor":Landroid/database/Cursor;
    :pswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v11

    .line 171
    .local v11, "labelIdString":Ljava/lang/String;
    :try_start_0
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    .line 177
    .local v10, "labelId":I
    const-string v1, "%s = %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 178
    .local v12, "where":Ljava/lang/String;
    invoke-virtual {v0, v12}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 172
    .end local v10    # "labelId":I
    .end local v12    # "where":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 173
    .local v9, "e":Ljava/lang/NumberFormatException;
    const/4 v1, 0x5

    const-string v2, "Unknown URI: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 174
    const/4 v8, 0x0

    goto :goto_0

    .line 181
    .end local v9    # "e":Ljava/lang/NumberFormatException;
    .end local v11    # "labelIdString":Ljava/lang/String;
    :pswitch_2
    const/4 v1, 0x2

    new-array p2, v1, [Ljava/lang/String;

    .end local p2    # "projection":[Ljava/lang/String;
    const/4 v1, 0x0

    const-string v2, "packageName"

    aput-object v2, p2, v1

    const/4 v1, 0x1

    const-string v2, "COUNT(*)"

    aput-object v2, p2, v1

    .line 182
    .restart local p2    # "projection":[Ljava/lang/String;
    const-string v5, "packageName"

    .line 183
    const-string p5, "packageName"

    .line 184
    goto :goto_1

    .line 161
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 288
    :cond_0
    return-void
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v7, 0x5

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 210
    if-nez p1, :cond_0

    .line 211
    const-string v5, "Unknown URI: %s"

    new-array v6, v8, [Ljava/lang/Object;

    aput-object p1, v6, v3

    invoke-static {p0, v7, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 237
    :goto_0
    return v3

    .line 215
    :cond_0
    sget-object v5, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 236
    const-string v5, "Unknown URI: %s"

    new-array v6, v8, [Ljava/lang/Object;

    aput-object p1, v6, v3

    invoke-static {p0, v7, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 217
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->initializeDatabaseIfNull()V

    .line 219
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 222
    .local v2, "labelIdString":Ljava/lang/String;
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 228
    .local v1, "labelId":I
    const-string v5, "%s = %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, "_id"

    aput-object v7, v6, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 229
    .local v4, "where":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "labels"

    invoke-direct {p0, p3, v4}, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->combineSelectionAndWhere(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, p2, v7, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 232
    .local v3, "result":I
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/labeling/LabelProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, p1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 223
    .end local v1    # "labelId":I
    .end local v3    # "result":I
    .end local v4    # "where":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 224
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v5, "Unknown URI: %s"

    new-array v6, v8, [Ljava/lang/Object;

    aput-object p1, v6, v3

    invoke-static {p0, v7, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 215
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
