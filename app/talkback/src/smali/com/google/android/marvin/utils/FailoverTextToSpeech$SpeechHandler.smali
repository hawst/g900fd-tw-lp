.class Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;
.super Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;
.source "FailoverTextToSpeech.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/utils/FailoverTextToSpeech;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SpeechHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/utils/WeakReferenceHandler",
        "<",
        "Lcom/google/android/marvin/utils/FailoverTextToSpeech;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V
    .locals 0
    .param p1, "parent"    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    .prologue
    .line 943
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;-><init>(Ljava/lang/Object;)V

    .line 944
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;
    .param p2, "parent"    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    .prologue
    .line 948
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 958
    :goto_0
    return-void

    .line 950
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/google/android/marvin/utils/FailoverTextToSpeech;->handleTtsInitialized(I)V
    invoke-static {p2, v0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->access$600(Lcom/google/android/marvin/utils/FailoverTextToSpeech;I)V

    goto :goto_0

    .line 953
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/marvin/utils/FailoverTextToSpeech;->handleUtteranceCompleted(Ljava/lang/String;Z)V
    invoke-static {p2, v0, v1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->access$700(Lcom/google/android/marvin/utils/FailoverTextToSpeech;Ljava/lang/String;Z)V

    goto :goto_0

    .line 956
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    # invokes: Lcom/google/android/marvin/utils/FailoverTextToSpeech;->handleMediaStateChanged(Ljava/lang/String;)V
    invoke-static {p2, v0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->access$800(Lcom/google/android/marvin/utils/FailoverTextToSpeech;Ljava/lang/String;)V

    goto :goto_0

    .line 948
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic handleMessage(Landroid/os/Message;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Message;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 932
    check-cast p2, Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;->handleMessage(Landroid/os/Message;Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V

    return-void
.end method

.method public onMediaStateChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 969
    const/4 v0, 0x3

    invoke-virtual {p0, v0, p1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 970
    return-void
.end method

.method public onTtsInitialized(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 961
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 962
    return-void
.end method

.method public onUtteranceCompleted(Ljava/lang/String;)V
    .locals 1
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 965
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 966
    return-void
.end method
