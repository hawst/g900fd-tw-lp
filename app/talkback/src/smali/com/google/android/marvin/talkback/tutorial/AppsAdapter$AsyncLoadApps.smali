.class Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$AsyncLoadApps;
.super Landroid/os/AsyncTask;
.source "AppsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AsyncLoadApps"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/content/Intent;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Landroid/content/pm/ResolveInfo;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;


# direct methods
.method private constructor <init>(Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$AsyncLoadApps;->this$0:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;
    .param p2, "x1"    # Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$1;

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$AsyncLoadApps;-><init>(Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 94
    check-cast p1, [Landroid/content/Intent;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$AsyncLoadApps;->doInBackground([Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Landroid/content/Intent;)Ljava/util/List;
    .locals 7
    .param p1, "params"    # [Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 99
    .local v4, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    move-object v0, p1

    .local v0, "arr$":[Landroid/content/Intent;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 100
    .local v3, "param":Landroid/content/Intent;
    iget-object v5, p0, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$AsyncLoadApps;->this$0:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;->mPackageManager:Landroid/content/pm/PackageManager;
    invoke-static {v5}, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;->access$100(Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;)Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 99
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 103
    .end local v3    # "param":Landroid/content/Intent;
    :cond_0
    return-object v4
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 94
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$AsyncLoadApps;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 108
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$AsyncLoadApps;->this$0:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;->addAll(Ljava/util/Collection;)V

    .line 109
    return-void
.end method
