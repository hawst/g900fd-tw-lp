.class Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LoadScreenshotTask;
.super Landroid/os/AsyncTask;
.source "LabelManagerPackageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadScreenshotTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field private mImageView:Landroid/widget/ImageView;

.field private mLabel:Lcom/googlecode/eyesfree/labeling/Label;

.field final synthetic this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;Lcom/googlecode/eyesfree/labeling/Label;Landroid/widget/ImageView;)V
    .locals 0
    .param p2, "label"    # Lcom/googlecode/eyesfree/labeling/Label;
    .param p3, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LoadScreenshotTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 238
    iput-object p2, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LoadScreenshotTask;->mLabel:Lcom/googlecode/eyesfree/labeling/Label;

    .line 239
    iput-object p3, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LoadScreenshotTask;->mImageView:Landroid/widget/ImageView;

    .line 240
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v5, 0x2

    .line 244
    iget-object v1, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LoadScreenshotTask;->mLabel:Lcom/googlecode/eyesfree/labeling/Label;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/labeling/Label;->getScreenshotPath()Ljava/lang/String;

    move-result-object v0

    .line 246
    .local v0, "screenshotPath":Ljava/lang/String;
    const-string v1, "Spawning new LoadScreenshotTask(%d) for %s."

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {p0, v5, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 249
    invoke-static {v0}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 227
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LoadScreenshotTask;->doInBackground([Ljava/lang/Void;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "result"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LoadScreenshotTask;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 255
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LoadScreenshotTask;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 256
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 227
    check-cast p1, Landroid/graphics/drawable/Drawable;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LoadScreenshotTask;->onPostExecute(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
