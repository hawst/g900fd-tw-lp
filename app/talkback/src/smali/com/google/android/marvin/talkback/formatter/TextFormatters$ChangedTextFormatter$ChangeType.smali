.class final enum Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;
.super Ljava/lang/Enum;
.source "TextFormatters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ChangeType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

.field public static final enum ADDED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

.field public static final enum REJECTED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

.field public static final enum REMOVED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

.field public static final enum REPLACED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 99
    new-instance v0, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    const-string v1, "REJECTED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->REJECTED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    .line 101
    new-instance v0, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    const-string v1, "REMOVED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->REMOVED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    .line 103
    new-instance v0, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->REPLACED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    .line 105
    new-instance v0, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    const-string v1, "ADDED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->ADDED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    .line 97
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    sget-object v1, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->REJECTED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->REMOVED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->REPLACED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->ADDED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->$VALUES:[Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 97
    const-class v0, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->$VALUES:[Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    invoke-virtual {v0}, [Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    return-object v0
.end method
