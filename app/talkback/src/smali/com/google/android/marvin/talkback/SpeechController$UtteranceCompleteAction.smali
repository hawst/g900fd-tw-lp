.class Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;
.super Ljava/lang/Object;
.source "SpeechController.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/SpeechController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UtteranceCompleteAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;",
        ">;"
    }
.end annotation


# instance fields
.field public runnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

.field public utteranceIndex:I


# direct methods
.method public constructor <init>(ILcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V
    .locals 0
    .param p1, "utteranceIndex"    # I
    .param p2, "runnable"    # Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .prologue
    .line 1039
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040
    iput p1, p0, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;->utteranceIndex:I

    .line 1041
    iput-object p2, p0, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;->runnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .line 1042
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;)I
    .locals 2
    .param p1, "another"    # Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;

    .prologue
    .line 1055
    iget v0, p0, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;->utteranceIndex:I

    iget v1, p1, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;->utteranceIndex:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1038
    check-cast p1, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;->compareTo(Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;)I

    move-result v0

    return v0
.end method
