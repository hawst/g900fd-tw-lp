.class Lcom/google/android/marvin/talkback/TalkBackService$10;
.super Landroid/content/BroadcastReceiver;
.source "TalkBackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/TalkBackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackService;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0

    .prologue
    .line 1655
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackService$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1658
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1660
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.google.android.marvin.talkback.RESUME_FEEDBACK"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1661
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->resumeTalkBack()V

    .line 1673
    :cond_0
    :goto_0
    return-void

    .line 1662
    :cond_1
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1663
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mAutomaticResume:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->access$800(Lcom/google/android/marvin/talkback/TalkBackService;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackService$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    const v4, 0x7f060056

    invoke-virtual {v3, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1664
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v3, "keyguard"

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/KeyguardManager;

    .line 1666
    .local v1, "keyguard":Landroid/app/KeyguardManager;
    invoke-virtual {v1}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1667
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->resumeTalkBack()V

    goto :goto_0

    .line 1669
    .end local v1    # "keyguard":Landroid/app/KeyguardManager;
    :cond_2
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mAutomaticResume:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->access$800(Lcom/google/android/marvin/talkback/TalkBackService;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackService$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    const v4, 0x7f060055

    invoke-virtual {v3, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1670
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->resumeTalkBack()V

    goto :goto_0
.end method
