.class Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;
.super Landroid/content/BroadcastReceiver;
.source "RingerModeAndScreenMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor$RingerModeHandler;
    }
.end annotation


# static fields
.field private static final STATE_CHANGE_FILTER:Landroid/content/IntentFilter;


# instance fields
.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mContext:Landroid/content/Context;

.field private final mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

.field private final mHandler:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor$RingerModeHandler;

.field private mRingerMode:I

.field private mScreenIsOff:Z

.field private final mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

.field private final mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

.field private final mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 44
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->STATE_CHANGE_FILTER:Landroid/content/IntentFilter;

    .line 47
    sget-object v0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->STATE_CHANGE_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 48
    sget-object v0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->STATE_CHANGE_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 49
    sget-object v0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->STATE_CHANGE_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 50
    sget-object v0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->STATE_CHANGE_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 1
    .param p1, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 69
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 64
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mRingerMode:I

    .line 287
    new-instance v0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor$RingerModeHandler;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor$RingerModeHandler;-><init>(Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mHandler:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor$RingerModeHandler;

    .line 70
    iput-object p1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mContext:Landroid/content/Context;

    .line 71
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    .line 72
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getShakeDetector()Lcom/google/android/marvin/talkback/ShakeDetector;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    .line 73
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/CachedFeedbackController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    .line 75
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mAudioManager:Landroid/media/AudioManager;

    .line 76
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mScreenIsOff:Z

    .line 79
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->internalOnReceive(Landroid/content/Intent;)V

    return-void
.end method

.method private appendCurrentTimeAnnouncement(Landroid/text/SpannableStringBuilder;)V
    .locals 6
    .param p1, "builder"    # Landroid/text/SpannableStringBuilder;

    .prologue
    .line 232
    const/16 v1, 0x1401

    .line 234
    .local v1, "timeFlags":I
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 235
    or-int/lit16 v1, v1, 0x80

    .line 238
    :cond_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mContext:Landroid/content/Context;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v2, v4, v5, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 241
    .local v0, "dateTime":Ljava/lang/CharSequence;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {p1, v2}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 242
    return-void
.end method

.method private appendRingerStateAnouncement(Landroid/text/SpannableStringBuilder;)V
    .locals 7
    .param p1, "builder"    # Landroid/text/SpannableStringBuilder;

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 250
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-nez v1, :cond_0

    .line 270
    :goto_0
    return-void

    .line 256
    :cond_0
    iget v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mRingerMode:I

    packed-switch v1, :pswitch_data_0

    .line 264
    const-class v1, Lcom/google/android/marvin/talkback/TalkBackService;

    const/4 v2, 0x6

    const-string v3, "Unknown ringer mode: %d"

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mRingerMode:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v1, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 258
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mContext:Landroid/content/Context;

    const v2, 0x7f0600d7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 269
    .local v0, "announcement":Ljava/lang/String;
    :goto_1
    new-array v1, v4, [Ljava/lang/CharSequence;

    aput-object v0, v1, v6

    invoke-static {p1, v1}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    .line 261
    .end local v0    # "announcement":Ljava/lang/String;
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mContext:Landroid/content/Context;

    const v2, 0x7f0600d6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 262
    .restart local v0    # "announcement":Ljava/lang/String;
    goto :goto_1

    .line 256
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getStreamVolume(I)F
    .locals 4
    .param p1, "streamType"    # I

    .prologue
    .line 279
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, p1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    .line 280
    .local v0, "currentVolume":I
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, p1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    .line 281
    .local v1, "maxVolume":I
    int-to-float v2, v0

    int-to-float v3, v1

    div-float/2addr v2, v3

    return v2
.end method

.method private handleDeviceUnlocked()V
    .locals 5

    .prologue
    .line 124
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mContext:Landroid/content/Context;

    const v2, 0x7f0600d9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "text":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    .line 127
    return-void
.end method

.method private handleRingerModeChanged(I)V
    .locals 4
    .param p1, "ringerMode"    # I

    .prologue
    const/4 v3, 0x0

    .line 212
    iput p1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mRingerMode:I

    .line 215
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 223
    :goto_0
    return-void

    .line 219
    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 220
    .local v0, "text":Landroid/text/SpannableStringBuilder;
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->appendRingerStateAnouncement(Landroid/text/SpannableStringBuilder;)V

    .line 222
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v3, v3, v2}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method private handleScreenOff()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 135
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mScreenIsOff:Z

    .line 137
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {v5, v10}, Lcom/google/android/marvin/talkback/SpeechController;->setScreenIsOn(Z)V

    .line 140
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v5

    if-eqz v5, :cond_0

    .line 179
    :goto_0
    return-void

    .line 145
    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mContext:Landroid/content/Context;

    const v6, 0x7f0600d8

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 148
    .local v0, "builder":Landroid/text/SpannableStringBuilder;
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->appendRingerStateAnouncement(Landroid/text/SpannableStringBuilder;)V

    .line 150
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    if-eqz v5, :cond_1

    .line 151
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    invoke-virtual {v5}, Lcom/google/android/marvin/talkback/ShakeDetector;->pausePolling()V

    .line 154
    :cond_1
    iget v5, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mRingerMode:I

    if-ne v5, v7, :cond_3

    .line 157
    const/4 v5, 0x3

    invoke-direct {p0, v5}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->getStreamVolume(I)F

    move-result v1

    .line 158
    .local v1, "musicVolume":F
    cmpl-float v5, v1, v8

    if-lez v5, :cond_4

    iget-object v5, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v5}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v5}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 165
    :cond_2
    invoke-direct {p0, v7}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->getStreamVolume(I)F

    move-result v2

    .line 166
    .local v2, "ringVolume":F
    const v3, 0x7f050023

    .line 167
    .local v3, "soundId":I
    div-float v5, v2, v1

    invoke-static {v9, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 174
    .end local v2    # "ringVolume":F
    .local v4, "volume":F
    :goto_1
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    invoke-virtual {v5, v3, v9, v4, v8}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(IFFF)Z

    .line 177
    .end local v1    # "musicVolume":F
    .end local v3    # "soundId":I
    .end local v4    # "volume":F
    :cond_3
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v6, 0x0

    invoke-virtual {v5, v0, v10, v7, v6}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    goto :goto_0

    .line 170
    .restart local v1    # "musicVolume":F
    :cond_4
    const v3, 0x7f050023

    .line 171
    .restart local v3    # "soundId":I
    const/high16 v4, 0x3f800000    # 1.0f

    .restart local v4    # "volume":F
    goto :goto_1
.end method

.method private handleScreenOn()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 186
    iput-boolean v3, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mScreenIsOff:Z

    .line 189
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/SpeechController;->setScreenIsOn(Z)V

    .line 191
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v1

    if-eqz v1, :cond_0

    .line 205
    :goto_0
    return-void

    .line 197
    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 198
    .local v0, "builder":Landroid/text/SpannableStringBuilder;
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->appendCurrentTimeAnnouncement(Landroid/text/SpannableStringBuilder;)V

    .line 200
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    if-eqz v1, :cond_1

    .line 201
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/ShakeDetector;->resumePolling()V

    .line 204
    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v3, v3, v2}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method private internalOnReceive(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x0

    .line 87
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v2

    if-nez v2, :cond_0

    .line 88
    const-class v2, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    const-string v3, "Service not initialized during broadcast."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v6, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 110
    :goto_0
    return-void

    .line 93
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 96
    const-string v2, "android.media.EXTRA_RINGER_MODE"

    const/4 v3, 0x2

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 99
    .local v1, "ringerMode":I
    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->handleRingerModeChanged(I)V

    goto :goto_0

    .line 100
    .end local v1    # "ringerMode":I
    :cond_1
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 101
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->handleScreenOn()V

    goto :goto_0

    .line 102
    :cond_2
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 103
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->handleScreenOff()V

    goto :goto_0

    .line 104
    :cond_3
    const-string v2, "android.intent.action.USER_PRESENT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 105
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->handleDeviceUnlocked()V

    goto :goto_0

    .line 107
    :cond_4
    const-class v2, Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v3, "Registered for but not handling action %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v2, v6, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public getFilter()Landroid/content/IntentFilter;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->STATE_CHANGE_FILTER:Landroid/content/IntentFilter;

    return-object v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mHandler:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor$RingerModeHandler;

    invoke-virtual {v0, p2}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor$RingerModeHandler;->onReceive(Landroid/content/Intent;)V

    .line 84
    return-void
.end method
