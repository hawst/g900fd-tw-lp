.class Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$7;
.super Ljava/lang/Object;
.source "TalkBackPreferencesActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->createEnableTreeDebugDialog()Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V
    .locals 0

    .prologue
    .line 687
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$7;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, 0x1

    .line 690
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$7;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    const/4 v3, 0x0

    # setter for: Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTreeDebugDialog:Landroid/app/AlertDialog;
    invoke-static {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->access$302(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 692
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$7;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 694
    .local v1, "prefs":Landroid/content/SharedPreferences;
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$7;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060030

    invoke-static {v1, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->putBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;IZ)V

    .line 699
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$7;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    const v3, 0x7f060031

    # invokes: Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;
    invoke-static {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->access$200(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 702
    .local v0, "prefTreeDebug":Landroid/preference/CheckBoxPreference;
    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 703
    return-void
.end method
