.class Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$5;
.super Ljava/lang/Object;
.source "TouchTutorialModule5.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$5;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGranularityChanged(Lcom/google/android/marvin/talkback/CursorGranularity;)V
    .locals 2
    .param p1, "granularity"    # Lcom/google/android/marvin/talkback/CursorGranularity;

    .prologue
    .line 146
    sget-object v0, Lcom/google/android/marvin/talkback/CursorGranularity;->WORD:Lcom/google/android/marvin/talkback/CursorGranularity;

    invoke-virtual {v0, p1}, Lcom/google/android/marvin/talkback/CursorGranularity;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$5;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    invoke-virtual {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->removeGranularityListener(Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;)Z

    .line 148
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$5;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$5$1;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$5$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$5;)V

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->installTriggerDelayedWithFeedback(Ljava/lang/Runnable;)V

    .line 155
    :cond_0
    return-void
.end method
