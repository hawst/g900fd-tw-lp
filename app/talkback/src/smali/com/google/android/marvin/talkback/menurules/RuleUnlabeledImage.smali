.class Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage;
.super Ljava/lang/Object;
.source "RuleUnlabeledImage.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/menurules/NodeMenuRule;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    return-void
.end method


# virtual methods
.method public accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 55
    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getInfo()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/accessibility/AccessibilityNodeInfo;

    .line 56
    .local v2, "unwrapped":Landroid/view/accessibility/AccessibilityNodeInfo;
    const-class v5, Landroid/widget/ImageView;

    invoke-static {p1, p2, v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z

    move-result v1

    .line 58
    .local v1, "isImage":Z
    invoke-static {p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getNodeText(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    move v0, v3

    .line 61
    .local v0, "hasDescription":Z
    :goto_0
    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    :goto_1
    return v3

    .end local v0    # "hasDescription":Z
    :cond_0
    move v0, v4

    .line 58
    goto :goto_0

    .restart local v0    # "hasDescription":Z
    :cond_1
    move v3, v4

    .line 61
    goto :goto_1
.end method

.method public canCollapseMenu()Z
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x1

    return v0
.end method

.method public getMenuItemsForNode(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/List;
    .locals 16
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/marvin/talkback/TalkBackService;",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    invoke-static/range {p2 .. p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v13

    .line 69
    .local v13, "nodeCopy":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-virtual {v13}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getInfo()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/view/accessibility/AccessibilityNodeInfo;

    .line 70
    .local v14, "unwrappedCopy":Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getLabelManager()Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    move-result-object v12

    .line 71
    .local v12, "labelManager":Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    new-instance v11, Ljava/util/LinkedList;

    invoke-direct {v11}, Ljava/util/LinkedList;-><init>()V

    .line 73
    .local v11, "items":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/widget/RadialMenuItem;>;"
    invoke-virtual {v14}, Landroid/view/accessibility/AccessibilityNodeInfo;->getViewIdResourceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v4}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->getLabelForViewIdFromCache(Ljava/lang/String;)Lcom/googlecode/eyesfree/labeling/Label;

    move-result-object v15

    .line 75
    .local v15, "viewLabel":Lcom/googlecode/eyesfree/labeling/Label;
    if-nez v15, :cond_0

    .line 76
    new-instance v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v3, 0x0

    const v4, 0x7f0d000c

    const/4 v5, 0x0

    const v6, 0x7f060178

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 79
    .local v1, "addLabel":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    .end local v1    # "addLabel":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :goto_0
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/MenuItem;

    .line 92
    .local v10, "item":Landroid/view/MenuItem;
    new-instance v4, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;

    move-object/from16 v0, p1

    invoke-direct {v4, v0, v14, v15}, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/view/accessibility/AccessibilityNodeInfo;Lcom/googlecode/eyesfree/labeling/Label;)V

    invoke-interface {v10, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_1

    .line 81
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "item":Landroid/view/MenuItem;
    :cond_0
    new-instance v2, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v4, 0x0

    const v5, 0x7f0d000d

    const/4 v6, 0x0

    const v7, 0x7f060179

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v7}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 84
    .local v2, "editLabel":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    new-instance v3, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v5, 0x0

    const v6, 0x7f0d000e

    const/4 v7, 0x0

    const v4, 0x7f06017a

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v8}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 87
    .local v3, "removeLabel":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 96
    .end local v2    # "editLabel":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .end local v3    # "removeLabel":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .restart local v9    # "i$":Ljava/util/Iterator;
    :cond_1
    return-object v11
.end method

.method public getUserFriendlyMenuName(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 101
    const v0, 0x7f06013f

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
