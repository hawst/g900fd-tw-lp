.class public Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;
.super Landroid/content/BroadcastReceiver;
.source "GestureActionMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;
    }
.end annotation


# static fields
.field public static final FILTER:Landroid/content/IntentFilter;


# instance fields
.field private mListener:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.marvin.talkback.tutorial.GestureActionPerformedAction"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;->FILTER:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 74
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 56
    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;->mListener:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;

    if-nez v2, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    const-string v2, "com.google.android.marvin.talkback.tutorial.GestureActionPerformedAction"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 64
    const-string v2, "com.google.android.marvin.talkback.tutorial.ShortcutGestureExtraAction"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 65
    .local v1, "gestureActionString":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->safeValueOf(Ljava/lang/String;)Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    move-result-object v0

    .line 67
    .local v0, "gestureAction":Lcom/google/android/marvin/talkback/ShortcutGestureAction;
    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;->mListener:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;

    invoke-interface {v2, v0}, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;->onGestureAction(Lcom/google/android/marvin/talkback/ShortcutGestureAction;)V

    goto :goto_0
.end method

.method public setListener(Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;->mListener:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;

    .line 52
    return-void
.end method
