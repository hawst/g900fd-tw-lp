.class Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$9;
.super Ljava/lang/Object;
.source "TouchTutorialModule5.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$9;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1, "text"    # Landroid/text/Editable;

    .prologue
    .line 238
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$9;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0601e9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 241
    .local v0, "editableText":Ljava/lang/String;
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 242
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$9;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->access$000(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 243
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$9;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    new-instance v2, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$9$1;

    invoke-direct {v2, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$9$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$9;)V

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->installTriggerDelayedWithFeedback(Ljava/lang/Runnable;)V

    .line 250
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 253
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 256
    return-void
.end method
