.class public Lcom/google/android/marvin/utils/ProximitySensor;
.super Ljava/lang/Object;
.source "ProximitySensor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;
    }
.end annotation


# instance fields
.field private mCallback:Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;

.field private final mFarValue:F

.field private final mFilterRunnable:Ljava/lang/Runnable;

.field private final mHandler:Landroid/os/Handler;

.field private mIsActive:Z

.field private mIsClose:Z

.field private final mListener:Landroid/hardware/SensorEventListener;

.field private final mProxSensor:Landroid/hardware/Sensor;

.field private final mSensorManager:Landroid/hardware/SensorManager;

.field private mShouldDropEvents:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mHandler:Landroid/os/Handler;

    .line 119
    new-instance v0, Lcom/google/android/marvin/utils/ProximitySensor$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/utils/ProximitySensor$1;-><init>(Lcom/google/android/marvin/utils/ProximitySensor;)V

    iput-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mListener:Landroid/hardware/SensorEventListener;

    .line 147
    new-instance v0, Lcom/google/android/marvin/utils/ProximitySensor$2;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/utils/ProximitySensor$2;-><init>(Lcom/google/android/marvin/utils/ProximitySensor;)V

    iput-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mFilterRunnable:Ljava/lang/Runnable;

    .line 71
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mSensorManager:Landroid/hardware/SensorManager;

    .line 72
    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mProxSensor:Landroid/hardware/Sensor;

    .line 73
    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mProxSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mProxSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getMaximumRange()F

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mFarValue:F

    .line 74
    return-void

    .line 73
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/marvin/utils/ProximitySensor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/utils/ProximitySensor;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mShouldDropEvents:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/marvin/utils/ProximitySensor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/utils/ProximitySensor;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mShouldDropEvents:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/marvin/utils/ProximitySensor;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/utils/ProximitySensor;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mFilterRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/marvin/utils/ProximitySensor;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/utils/ProximitySensor;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/marvin/utils/ProximitySensor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/utils/ProximitySensor;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mIsClose:Z

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/marvin/utils/ProximitySensor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/utils/ProximitySensor;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mIsClose:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/marvin/utils/ProximitySensor;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/utils/ProximitySensor;

    .prologue
    .line 35
    iget v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mFarValue:F

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/marvin/utils/ProximitySensor;)Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/utils/ProximitySensor;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mCallback:Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;

    return-object v0
.end method


# virtual methods
.method public setProximityChangeListener(Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mCallback:Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;

    .line 78
    return-void
.end method

.method public start()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x1

    .line 107
    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mProxSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mIsActive:Z

    if-eqz v0, :cond_1

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    iput-boolean v3, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mIsActive:Z

    .line 112
    iput-boolean v3, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mShouldDropEvents:Z

    .line 113
    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mProxSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2, v6}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 114
    const-string v0, "Proximity sensor registered at %d."

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v6, v0, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mFilterRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x78

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public stop()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 93
    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mProxSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mIsActive:Z

    if-nez v0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    const/4 v0, 0x2

    const-string v1, "Proximity sensor stopped at %d."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 99
    iput-boolean v6, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mIsActive:Z

    .line 100
    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    goto :goto_0
.end method
