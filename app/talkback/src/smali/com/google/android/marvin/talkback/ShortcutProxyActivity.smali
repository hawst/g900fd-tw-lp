.class public Lcom/google/android/marvin/talkback/ShortcutProxyActivity;
.super Landroid/app/Activity;
.source "ShortcutProxyActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 34
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/ShortcutProxyActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 35
    const-string v1, "performCustomGestureAction"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 36
    const-string v1, "gestureAction"

    sget-object v2, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->TALKBACK_BREAKOUT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 39
    const-string v1, "com.google.android.marvin.feedback.permission.TALKBACK"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/marvin/talkback/ShortcutProxyActivity;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/ShortcutProxyActivity;->finish()V

    .line 41
    return-void
.end method
