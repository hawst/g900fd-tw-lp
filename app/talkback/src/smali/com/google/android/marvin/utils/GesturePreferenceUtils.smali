.class public Lcom/google/android/marvin/utils/GesturePreferenceUtils;
.super Ljava/lang/Object;
.source "GesturePreferenceUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;
    }
.end annotation


# static fields
.field private static final GESTURE_DIRECTIONS:[Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 33
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;

    const v3, 0x7f060044

    const v4, 0x7f060072

    const v5, 0x7f060143

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;-><init>(III)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;

    const v3, 0x7f060045

    const v4, 0x7f060073

    const v5, 0x7f060144

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;-><init>(III)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;

    const v3, 0x7f060042

    const v4, 0x7f060070

    const v5, 0x7f060145

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;-><init>(III)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;

    const v3, 0x7f060043

    const v4, 0x7f060071

    const v5, 0x7f060146

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;-><init>(III)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;

    const v3, 0x7f060046

    const v4, 0x7f060074

    const v5, 0x7f060147

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;-><init>(III)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;

    const v3, 0x7f060047

    const v4, 0x7f060075

    const v5, 0x7f060148

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;-><init>(III)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;

    const v3, 0x7f060048

    const v4, 0x7f060076

    const v5, 0x7f060149

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;-><init>(III)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;

    const v3, 0x7f060049

    const v4, 0x7f060077

    const v5, 0x7f06014a

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;-><init>(III)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/marvin/utils/GesturePreferenceUtils;->GESTURE_DIRECTIONS:[Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;

    return-void
.end method

.method protected static getActionForGesture(Landroid/content/Context;II)Lcom/google/android/marvin/talkback/ShortcutGestureAction;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shortcutPrefKey"    # I
    .param p2, "shortcutPrefDefault"    # I

    .prologue
    .line 86
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 87
    .local v2, "prefs":Landroid/content/SharedPreferences;
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 88
    .local v1, "key":Ljava/lang/String;
    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "defaultValue":Ljava/lang/String;
    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 90
    .local v3, "value":Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->safeValueOf(Ljava/lang/String;)Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    move-result-object v4

    return-object v4
.end method

.method public static getDirectionForAction(Landroid/content/Context;Lcom/google/android/marvin/talkback/ShortcutGestureAction;)I
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "action"    # Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    .prologue
    .line 69
    sget-object v0, Lcom/google/android/marvin/utils/GesturePreferenceUtils;->GESTURE_DIRECTIONS:[Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;

    .local v0, "arr$":[Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v2, v0, v3

    .line 70
    .local v2, "direction":Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;
    iget v5, v2, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;->keyId:I

    iget v6, v2, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;->defaultId:I

    invoke-static {p0, v5, v6}, Lcom/google/android/marvin/utils/GesturePreferenceUtils;->getActionForGesture(Landroid/content/Context;II)Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    move-result-object v1

    .line 72
    .local v1, "currentDirectionAction":Lcom/google/android/marvin/talkback/ShortcutGestureAction;
    if-ne v1, p1, :cond_0

    .line 73
    iget v5, v2, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;->labelId:I

    .line 77
    .end local v1    # "currentDirectionAction":Lcom/google/android/marvin/talkback/ShortcutGestureAction;
    .end local v2    # "direction":Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;
    :goto_1
    return v5

    .line 69
    .restart local v1    # "currentDirectionAction":Lcom/google/android/marvin/talkback/ShortcutGestureAction;
    .restart local v2    # "direction":Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 77
    .end local v1    # "currentDirectionAction":Lcom/google/android/marvin/talkback/ShortcutGestureAction;
    .end local v2    # "direction":Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;
    :cond_1
    const/4 v5, -0x1

    goto :goto_1
.end method
