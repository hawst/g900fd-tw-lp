.class public Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;
.super Ljava/lang/Object;
.source "TalkBackRadialMenuClient.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;
    }
.end annotation


# instance fields
.field private final mMenuInflater:Landroid/view/MenuInflater;

.field private final mMenuRuleProcessor:Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;

.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 2
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 55
    new-instance v0, Landroid/view/MenuInflater;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {v0, v1}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mMenuInflater:Landroid/view/MenuInflater;

    .line 57
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 58
    new-instance v0, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {v0, v1}, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mMenuRuleProcessor:Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;

    .line 62
    :goto_0
    return-void

    .line 60
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mMenuRuleProcessor:Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;

    goto :goto_0
.end method

.method private onCreateGlobalContextMenu(Lcom/googlecode/eyesfree/widget/RadialMenu;)V
    .locals 4
    .param p1, "menu"    # Lcom/googlecode/eyesfree/widget/RadialMenu;

    .prologue
    const v3, 0x7f0d009e

    const/16 v2, 0x10

    .line 128
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mMenuInflater:Landroid/view/MenuInflater;

    const/high16 v1, 0x7f0f0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 130
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v2, :cond_0

    .line 131
    const v0, 0x7f0d009a

    invoke-virtual {p1, v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->removeItem(I)V

    .line 132
    const v0, 0x7f0d009d

    invoke-virtual {p1, v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->removeItem(I)V

    .line 136
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v2, :cond_2

    .line 137
    invoke-virtual {p1, v3}, Lcom/googlecode/eyesfree/widget/RadialMenu;->removeItem(I)V

    .line 143
    :goto_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_1

    .line 144
    const v0, 0x7f0d009b

    invoke-virtual {p1, v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->removeItem(I)V

    .line 146
    :cond_1
    return-void

    .line 139
    :cond_2
    invoke-virtual {p1, v3}, Lcom/googlecode/eyesfree/widget/RadialMenu;->findItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->onCreateSummaryMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V

    goto :goto_0
.end method

.method private onCreateSummaryMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V
    .locals 3
    .param p1, "summaryItem"    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .prologue
    .line 149
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getSubMenu()Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v1

    .line 150
    .local v1, "summary":Lcom/googlecode/eyesfree/widget/RadialSubMenu;
    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {v0, v2}, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    .line 153
    .local v0, "quickNav":Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;
    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient$QuickNavigationJogDial;->populateMenu(Lcom/googlecode/eyesfree/widget/RadialMenu;)V

    .line 155
    invoke-virtual {v1, v0}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->setDefaultSelectionListener(Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;)V

    .line 156
    invoke-virtual {v1, v0}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->setOnMenuVisibilityChangedListener(Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;)V

    .line 157
    return-void
.end method

.method private onPrepareLocalContextMenu(Lcom/googlecode/eyesfree/widget/RadialMenu;)Z
    .locals 3
    .param p1, "menu"    # Lcom/googlecode/eyesfree/widget/RadialMenu;

    .prologue
    .line 160
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 161
    .local v0, "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mMenuRuleProcessor:Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;

    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    .line 162
    :cond_0
    const/4 v1, 0x0

    .line 167
    :goto_0
    return v1

    .line 165
    :cond_1
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mMenuRuleProcessor:Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;

    invoke-virtual {v2, p1, v0}, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;->prepareMenuForNode(Lcom/googlecode/eyesfree/widget/RadialMenu;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    .line 166
    .local v1, "result":Z
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0
.end method


# virtual methods
.method public onCreateRadialMenu(ILcom/googlecode/eyesfree/widget/RadialMenu;)V
    .locals 1
    .param p1, "menuId"    # I
    .param p2, "menu"    # Lcom/googlecode/eyesfree/widget/RadialMenu;

    .prologue
    .line 66
    const/high16 v0, 0x7f0f0000

    if-ne p1, v0, :cond_0

    .line 67
    invoke-direct {p0, p2}, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->onCreateGlobalContextMenu(Lcom/googlecode/eyesfree/widget/RadialMenu;)V

    .line 69
    :cond_0
    return-void
.end method

.method public onMenuItemClicked(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x0

    .line 87
    if-nez p1, :cond_1

    .line 118
    :cond_0
    :goto_0
    return v3

    .line 92
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 93
    .local v1, "itemId":I
    const v4, 0x7f0d009a

    if-ne v1, v4, :cond_2

    .line 94
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getFullScreenReadController()Lcom/google/android/marvin/talkback/FullScreenReadController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/FullScreenReadController;->startReadingFromBeginning()V

    .line 114
    :goto_1
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.marvin.talkback.tutorial.ContextMenuItemClickedAction"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 115
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "com.google.android.marvin.talkback.tutorial.ItemIdExtra"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 116
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v3}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 118
    const/4 v3, 0x1

    goto :goto_0

    .line 95
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    const v4, 0x7f0d009d

    if-ne v1, v4, :cond_3

    .line 96
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getFullScreenReadController()Lcom/google/android/marvin/talkback/FullScreenReadController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/FullScreenReadController;->startReadingFromNextNode()V

    goto :goto_1

    .line 97
    :cond_3
    const v4, 0x7f0d009b

    if-ne v1, v4, :cond_4

    .line 98
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/SpeechController;->repeatLastUtterance()Z

    goto :goto_1

    .line 99
    :cond_4
    const v4, 0x7f0d009c

    if-ne v1, v4, :cond_5

    .line 100
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/SpeechController;->spellLastUtterance()Z

    goto :goto_1

    .line 101
    :cond_5
    const v4, 0x7f0d009f

    if-ne v1, v4, :cond_6

    .line 102
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/TalkBackService;->requestSuspendTalkBack()V

    goto :goto_1

    .line 103
    :cond_6
    const v4, 0x7f0d00a0

    if-ne v1, v4, :cond_0

    .line 104
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-class v4, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 105
    .local v2, "settingsIntent":Landroid/content/Intent;
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 106
    const/high16 v3, 0x4000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 107
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public onMenuItemHovered(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 124
    const/4 v0, 0x0

    return v0
.end method

.method public onPrepareRadialMenu(ILcom/googlecode/eyesfree/widget/RadialMenu;)Z
    .locals 1
    .param p1, "menuId"    # I
    .param p2, "menu"    # Lcom/googlecode/eyesfree/widget/RadialMenu;

    .prologue
    .line 73
    const v0, 0x7f0f0001

    if-ne p1, v0, :cond_0

    .line 74
    invoke-direct {p0, p2}, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;->onPrepareLocalContextMenu(Lcom/googlecode/eyesfree/widget/RadialMenu;)Z

    move-result v0

    .line 77
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
