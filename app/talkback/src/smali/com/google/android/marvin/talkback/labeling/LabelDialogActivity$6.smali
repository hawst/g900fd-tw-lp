.class Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$6;
.super Ljava/lang/Object;
.source "LabelDialogActivity.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)V
    .locals 0

    .prologue
    .line 298
    iput-object p1, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$6;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$6;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mPositiveButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->access$400(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$6;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mPositiveButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->access$400(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->callOnClick()Z

    .line 304
    const/4 v0, 0x1

    .line 307
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
