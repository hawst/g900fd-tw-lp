.class public Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;
.super Ljava/lang/Object;
.source "NotificationFormatter.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# instance fields
.field private final mNotificationHistory:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/app/Notification;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->mNotificationHistory:Ljava/util/LinkedList;

    return-void
.end method

.method private addToHistory(Landroid/app/Notification;)V
    .locals 2
    .param p1, "notification"    # Landroid/app/Notification;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->mNotificationHistory:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->mNotificationHistory:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->mNotificationHistory:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    .line 157
    :cond_0
    return-void
.end method

.method private extractNotification(Landroid/view/accessibility/AccessibilityEvent;)Landroid/app/Notification;
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 84
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getParcelableData()Landroid/os/Parcelable;

    move-result-object v0

    .line 86
    .local v0, "parcelable":Landroid/os/Parcelable;
    instance-of v1, v0, Landroid/app/Notification;

    if-nez v1, :cond_0

    .line 87
    const/4 v0, 0x0

    .line 90
    .end local v0    # "parcelable":Landroid/os/Parcelable;
    :goto_0
    return-object v0

    .restart local v0    # "parcelable":Landroid/os/Parcelable;
    :cond_0
    check-cast v0, Landroid/app/Notification;

    goto :goto_0
.end method

.method private getTypeText(Landroid/content/Context;Landroid/app/Notification;)Ljava/lang/CharSequence;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "notification"    # Landroid/app/Notification;

    .prologue
    .line 222
    iget v0, p2, Landroid/app/Notification;->icon:I

    .line 223
    .local v0, "icon":I
    invoke-static {p1, v0}, Lcom/google/android/marvin/talkback/NotificationType;->getNotificationTypeFromIcon(Landroid/content/Context;I)Lcom/google/android/marvin/talkback/NotificationType;

    move-result-object v1

    .line 225
    .local v1, "type":Lcom/google/android/marvin/talkback/NotificationType;
    if-nez v1, :cond_0

    .line 226
    const/4 v2, 0x0

    .line 229
    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/NotificationType;->getValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private declared-synchronized isRecent(Landroid/app/Notification;)Z
    .locals 4
    .param p1, "notification"    # Landroid/app/Notification;

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->removeFromHistory(Landroid/app/Notification;)Landroid/app/Notification;

    move-result-object v0

    .line 106
    .local v0, "foundInHistory":Landroid/app/Notification;
    if-eqz v0, :cond_0

    .line 107
    iget-wide v2, v0, Landroid/app/Notification;->when:J

    iput-wide v2, p1, Landroid/app/Notification;->when:J

    .line 112
    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->addToHistory(Landroid/app/Notification;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_1
    monitor-exit p0

    return v1

    .line 109
    :cond_0
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p1, Landroid/app/Notification;->when:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 102
    .end local v0    # "foundInHistory":Landroid/app/Notification;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 114
    .restart local v0    # "foundInHistory":Landroid/app/Notification;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private notificationsAreEqual(Landroid/app/Notification;Landroid/app/Notification;)Z
    .locals 5
    .param p1, "first"    # Landroid/app/Notification;
    .param p2, "second"    # Landroid/app/Notification;

    .prologue
    const/4 v2, 0x0

    .line 167
    iget-object v3, p1, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    iget-object v4, p2, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 178
    :cond_0
    :goto_0
    return v2

    .line 171
    :cond_1
    iget-object v0, p1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 172
    .local v0, "firstView":Landroid/widget/RemoteViews;
    iget-object v1, p2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 174
    .local v1, "secondView":Landroid/widget/RemoteViews;
    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->remoteViewsAreEqual(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 178
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private remoteViewsAreEqual(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)Z
    .locals 7
    .param p1, "firstView"    # Landroid/widget/RemoteViews;
    .param p2, "secondView"    # Landroid/widget/RemoteViews;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 188
    if-ne p1, p2, :cond_1

    .line 210
    :cond_0
    :goto_0
    return v4

    .line 192
    :cond_1
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    :cond_2
    move v4, v5

    .line 193
    goto :goto_0

    .line 196
    :cond_3
    invoke-virtual {p1}, Landroid/widget/RemoteViews;->getPackage()Ljava/lang/String;

    move-result-object v1

    .line 197
    .local v1, "firstPackage":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/widget/RemoteViews;->getPackage()Ljava/lang/String;

    move-result-object v3

    .line 199
    .local v3, "secondPackage":Ljava/lang/String;
    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    move v4, v5

    .line 200
    goto :goto_0

    .line 203
    :cond_4
    invoke-virtual {p1}, Landroid/widget/RemoteViews;->getLayoutId()I

    move-result v0

    .line 204
    .local v0, "firstLayoutId":I
    invoke-virtual {p2}, Landroid/widget/RemoteViews;->getLayoutId()I

    move-result v2

    .line 206
    .local v2, "secondLayoutId":I
    if-eq v0, v2, :cond_0

    move v4, v5

    .line 207
    goto :goto_0
.end method

.method private removeFromHistory(Landroid/app/Notification;)Landroid/app/Notification;
    .locals 8
    .param p1, "notification"    # Landroid/app/Notification;

    .prologue
    .line 124
    iget-object v4, p0, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->mNotificationHistory:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 126
    .local v2, "historyIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/Notification;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 127
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Notification;

    .line 128
    .local v3, "recentNotification":Landroid/app/Notification;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, v3, Landroid/app/Notification;->when:J

    sub-long v0, v4, v6

    .line 131
    .local v0, "age":J
    const-wide/32 v4, 0xea60

    cmp-long v4, v0, v4

    if-lez v4, :cond_1

    .line 132
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 136
    :cond_1
    invoke-direct {p0, p1, v3}, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->notificationsAreEqual(Landroid/app/Notification;Landroid/app/Notification;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 137
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 142
    .end local v0    # "age":J
    .end local v3    # "recentNotification":Landroid/app/Notification;
    :goto_1
    return-object v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    const/4 v3, 0x0

    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->extractNotification(Landroid/view/accessibility/AccessibilityEvent;)Landroid/app/Notification;

    move-result-object v0

    .line 55
    .local v0, "notification":Landroid/app/Notification;
    if-nez v0, :cond_1

    .line 74
    :cond_0
    :goto_0
    return v3

    .line 59
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->isRecent(Landroid/app/Notification;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 63
    invoke-direct {p0, p2, v0}, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->getTypeText(Landroid/content/Context;Landroid/app/Notification;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 64
    .local v2, "typeText":Ljava/lang/CharSequence;
    iget-object v1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 66
    .local v1, "tickerText":Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 67
    invoke-virtual {p3, v2}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 70
    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 71
    invoke-virtual {p3, v1}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 74
    :cond_3
    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getSpoken()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0
.end method
