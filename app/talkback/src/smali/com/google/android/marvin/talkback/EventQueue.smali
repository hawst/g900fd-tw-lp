.class Lcom/google/android/marvin/talkback/EventQueue;
.super Ljava/lang/Object;
.source "EventQueue.java"


# instance fields
.field private final mEventQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/accessibility/AccessibilityEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final mQualifyingEvents:Landroid/util/SparseIntArray;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/EventQueue;->mEventQueue:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/EventQueue;->mQualifyingEvents:Landroid/util/SparseIntArray;

    return-void
.end method

.method private enforceEventLimits()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    .line 128
    const/4 v2, 0x0

    .line 130
    .local v2, "eventTypesToPrune":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v7, p0, Lcom/google/android/marvin/talkback/EventQueue;->mQualifyingEvents:Landroid/util/SparseIntArray;

    invoke-virtual {v7}, Landroid/util/SparseIntArray;->size()I

    move-result v7

    if-ge v4, v7, :cond_1

    .line 131
    iget-object v7, p0, Lcom/google/android/marvin/talkback/EventQueue;->mQualifyingEvents:Landroid/util/SparseIntArray;

    invoke-virtual {v7, v4}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v1

    .line 132
    .local v1, "eventType":I
    iget-object v7, p0, Lcom/google/android/marvin/talkback/EventQueue;->mQualifyingEvents:Landroid/util/SparseIntArray;

    invoke-virtual {v7, v4}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v3

    .line 133
    .local v3, "eventsOfType":I
    if-le v3, v9, :cond_0

    .line 134
    or-int/2addr v2, v1

    .line 130
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 138
    .end local v1    # "eventType":I
    .end local v3    # "eventsOfType":I
    :cond_1
    iget-object v7, p0, Lcom/google/android/marvin/talkback/EventQueue;->mEventQueue:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 139
    .local v5, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/view/accessibility/AccessibilityEvent;>;"
    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    if-eqz v2, :cond_3

    .line 140
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/accessibility/AccessibilityEvent;

    .line 143
    .local v6, "next":Landroid/view/accessibility/AccessibilityEvent;
    invoke-static {v6, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->eventMatchesAnyType(Landroid/view/accessibility/AccessibilityEvent;I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 144
    invoke-virtual {v6}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v1

    .line 145
    .restart local v1    # "eventType":I
    iget-object v7, p0, Lcom/google/android/marvin/talkback/EventQueue;->mQualifyingEvents:Landroid/util/SparseIntArray;

    const/4 v8, 0x0

    invoke-virtual {v7, v1, v8}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    .line 146
    .local v0, "eventCountOfType":I
    add-int/lit8 v0, v0, -0x1

    .line 147
    iget-object v7, p0, Lcom/google/android/marvin/talkback/EventQueue;->mQualifyingEvents:Landroid/util/SparseIntArray;

    invoke-virtual {v7, v1, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 148
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 152
    if-gt v0, v9, :cond_2

    .line 153
    xor-int/lit8 v7, v1, -0x1

    and-int/2addr v2, v7

    goto :goto_1

    .line 157
    .end local v0    # "eventCountOfType":I
    .end local v1    # "eventType":I
    .end local v6    # "next":Landroid/view/accessibility/AccessibilityEvent;
    :cond_3
    return-void
.end method


# virtual methods
.method public dequeue()Landroid/view/accessibility/AccessibilityEvent;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 89
    iget-object v3, p0, Lcom/google/android/marvin/talkback/EventQueue;->mEventQueue:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 90
    const/4 v0, 0x0

    .line 101
    :cond_0
    :goto_0
    return-object v0

    .line 93
    :cond_1
    iget-object v3, p0, Lcom/google/android/marvin/talkback/EventQueue;->mEventQueue:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityEvent;

    .line 95
    .local v0, "event":Landroid/view/accessibility/AccessibilityEvent;
    if-eqz v0, :cond_0

    const v3, 0x8080

    invoke-static {v0, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->eventMatchesAnyType(Landroid/view/accessibility/AccessibilityEvent;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 97
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v2

    .line 98
    .local v2, "eventType":I
    iget-object v3, p0, Lcom/google/android/marvin/talkback/EventQueue;->mQualifyingEvents:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    .line 99
    .local v1, "eventCountOfType":I
    iget-object v3, p0, Lcom/google/android/marvin/talkback/EventQueue;->mQualifyingEvents:Landroid/util/SparseIntArray;

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseIntArray;->put(II)V

    goto :goto_0
.end method

.method public enqueue(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 71
    invoke-static {p1}, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->obtain(Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 72
    .local v0, "clone":Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v2

    .line 74
    .local v2, "eventType":I
    const v3, 0x8080

    invoke-static {v0, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->eventMatchesAnyType(Landroid/view/accessibility/AccessibilityEvent;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 75
    iget-object v3, p0, Lcom/google/android/marvin/talkback/EventQueue;->mQualifyingEvents:Landroid/util/SparseIntArray;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    .line 76
    .local v1, "eventCountOfType":I
    iget-object v3, p0, Lcom/google/android/marvin/talkback/EventQueue;->mQualifyingEvents:Landroid/util/SparseIntArray;

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 79
    .end local v1    # "eventCountOfType":I
    :cond_0
    iget-object v3, p0, Lcom/google/android/marvin/talkback/EventQueue;->mEventQueue:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/EventQueue;->enforceEventLimits()V

    .line 81
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/marvin/talkback/EventQueue;->mEventQueue:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    return v0
.end method
