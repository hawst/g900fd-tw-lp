.class Lcom/google/android/marvin/talkback/CallStateMonitor;
.super Landroid/content/BroadcastReceiver;
.source "CallStateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/CallStateMonitor$CallStateHandler;
    }
.end annotation


# static fields
.field private static final STATE_CHANGED_FILTER:Landroid/content/IntentFilter;


# instance fields
.field private final mHandler:Lcom/google/android/marvin/talkback/CallStateMonitor$CallStateHandler;

.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;

.field private final mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PHONE_STATE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/marvin/talkback/CallStateMonitor;->STATE_CHANGED_FILTER:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 1
    .param p1, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 39
    new-instance v0, Lcom/google/android/marvin/talkback/CallStateMonitor$CallStateHandler;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/CallStateMonitor$CallStateHandler;-><init>(Lcom/google/android/marvin/talkback/CallStateMonitor;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CallStateMonitor;->mHandler:Lcom/google/android/marvin/talkback/CallStateMonitor$CallStateHandler;

    .line 42
    iput-object p1, p0, Lcom/google/android/marvin/talkback/CallStateMonitor;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 43
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CallStateMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/CallStateMonitor;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/CallStateMonitor;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/CallStateMonitor;->internalOnReceive(Landroid/content/Intent;)V

    return-void
.end method

.method private internalOnReceive(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    .line 52
    const-string v2, "state"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 54
    .local v1, "state":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v2

    if-nez v2, :cond_1

    .line 55
    const-class v2, Lcom/google/android/marvin/talkback/CallStateMonitor;

    const/4 v3, 0x5

    const-string v4, "Service not initialized during broadcast."

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    sget-object v2, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 61
    iget-object v2, p0, Lcom/google/android/marvin/talkback/CallStateMonitor;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->interruptAllFeedback()V

    .line 64
    :cond_2
    iget-object v2, p0, Lcom/google/android/marvin/talkback/CallStateMonitor;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getShakeDetector()Lcom/google/android/marvin/talkback/ShakeDetector;

    move-result-object v0

    .line 65
    .local v0, "shakeDetector":Lcom/google/android/marvin/talkback/ShakeDetector;
    if-eqz v0, :cond_0

    .line 67
    sget-object v2, Landroid/telephony/TelephonyManager;->EXTRA_STATE_OFFHOOK:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 68
    invoke-virtual {v0, v5}, Lcom/google/android/marvin/talkback/ShakeDetector;->setEnabled(Z)V

    goto :goto_0

    .line 69
    :cond_3
    sget-object v2, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 70
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/marvin/talkback/ShakeDetector;->setEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public getCurrentCallState()I
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CallStateMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/content/IntentFilter;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/marvin/talkback/CallStateMonitor;->STATE_CHANGED_FILTER:Landroid/content/IntentFilter;

    return-object v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CallStateMonitor;->mHandler:Lcom/google/android/marvin/talkback/CallStateMonitor$CallStateHandler;

    invoke-virtual {v0, p2}, Lcom/google/android/marvin/talkback/CallStateMonitor$CallStateHandler;->onReceive(Landroid/content/Intent;)V

    .line 49
    return-void
.end method
