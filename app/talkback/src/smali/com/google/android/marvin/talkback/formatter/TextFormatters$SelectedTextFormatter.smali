.class public final Lcom/google/android/marvin/talkback/formatter/TextFormatters$SelectedTextFormatter;
.super Ljava/lang/Object;
.source "TextFormatters.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/formatter/TextFormatters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SelectedTextFormatter"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private formatPassword(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 9
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 610
    new-instance v2, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v2, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 611
    .local v2, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v1

    .line 612
    .local v1, "fromIndex":I
    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getToIndex()I

    move-result v3

    .line 614
    .local v3, "toIndex":I
    if-gt v3, v1, :cond_0

    .line 621
    :goto_0
    return v4

    .line 618
    :cond_0
    const v6, 0x7f0600ba

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v5

    invoke-virtual {p2, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 620
    .local v0, "formattedText":Ljava/lang/CharSequence;
    invoke-virtual {p3, v0}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    move v4, v5

    .line 621
    goto :goto_0
.end method

.method private shouldDropEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 564
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    .line 565
    .local v0, "eventType":I
    const/16 v7, 0x2000

    if-eq v0, v7, :cond_1

    .line 597
    :cond_0
    :goto_0
    return v6

    .line 572
    :cond_1
    # getter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sAwaitingSelectionCount:I
    invoke-static {}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$000()I

    move-result v7

    if-lez v7, :cond_5

    .line 573
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v8

    # getter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedTimestamp:J
    invoke-static {}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$100()J

    move-result-wide v10

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x96

    cmp-long v7, v8, v10

    if-ltz v7, :cond_2

    move v1, v5

    .line 575
    .local v1, "hasDelayElapsed":Z
    :goto_1
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v7

    # getter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedPackage:Ljava/lang/CharSequence;
    invoke-static {}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$200()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    move v2, v5

    .line 580
    .local v2, "hasPackageChanged":Z
    :goto_2
    if-nez v1, :cond_4

    if-nez v2, :cond_4

    .line 581
    # operator-- for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sAwaitingSelectionCount:I
    invoke-static {}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$010()I

    move v6, v5

    .line 582
    goto :goto_0

    .end local v1    # "hasDelayElapsed":Z
    .end local v2    # "hasPackageChanged":Z
    :cond_2
    move v1, v6

    .line 573
    goto :goto_1

    .restart local v1    # "hasDelayElapsed":Z
    :cond_3
    move v2, v6

    .line 575
    goto :goto_2

    .line 586
    .restart local v2    # "hasPackageChanged":Z
    :cond_4
    # setter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sAwaitingSelectionCount:I
    invoke-static {v6}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$002(I)I

    .line 590
    .end local v1    # "hasDelayElapsed":Z
    .end local v2    # "hasPackageChanged":Z
    :cond_5
    new-instance v3, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v3, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 591
    .local v3, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v4

    .line 592
    .local v4, "source":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isFocused()Z

    move-result v7

    if-nez v7, :cond_0

    .line 593
    const/4 v7, 0x2

    const-string v8, "Dropped selection event from non-focused field"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p0, v7, v8, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    move v6, v5

    .line 594
    goto :goto_0
.end method

.method private shouldReverseCharacterIndices(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 551
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    invoke-static {p1}, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->getAction(Landroid/view/accessibility/AccessibilityEvent;)I

    move-result v0

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    .line 472
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$SelectedTextFormatter;->shouldDropEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 473
    const/4 v10, 0x0

    .line 543
    :goto_0
    return v10

    .line 476
    :cond_0
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v10

    const/high16 v11, 0x20000

    if-ne v10, v11, :cond_1

    const/4 v5, 0x1

    .line 480
    .local v5, "isGranularTraversal":Z
    :goto_1
    if-eqz v5, :cond_2

    .line 482
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->getEventTextOrDescription(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v9

    .line 488
    .local v9, "text":Ljava/lang/CharSequence;
    :goto_2
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getItemCount()I

    move-result v2

    .line 489
    .local v2, "count":I
    invoke-static {p2}, Lcom/googlecode/eyesfree/compat/provider/SettingsCompatUtils$SecureCompatUtils;->shouldSpeakPasswords(Landroid/content/Context;)Z

    move-result v7

    .line 491
    .local v7, "shouldSpeakPasswords":Z
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->isPassword()Z

    move-result v10

    if-eqz v10, :cond_3

    if-nez v7, :cond_3

    .line 492
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$SelectedTextFormatter;->formatPassword(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Lcom/google/android/marvin/talkback/Utterance;)Z

    move-result v10

    goto :goto_0

    .line 476
    .end local v2    # "count":I
    .end local v5    # "isGranularTraversal":Z
    .end local v7    # "shouldSpeakPasswords":Z
    .end local v9    # "text":Ljava/lang/CharSequence;
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 485
    .restart local v5    # "isGranularTraversal":Z
    :cond_2
    # invokes: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->getEventText(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$300(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v9

    .restart local v9    # "text":Ljava/lang/CharSequence;
    goto :goto_2

    .line 499
    .restart local v2    # "count":I
    .restart local v7    # "shouldSpeakPasswords":Z
    :cond_3
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_4

    if-nez v2, :cond_5

    .line 500
    :cond_4
    const/4 v10, 0x0

    goto :goto_0

    .line 506
    :cond_5
    if-eqz v5, :cond_7

    invoke-static {p1}, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->getMovementGranularity(Landroid/view/accessibility/AccessibilityEvent;)I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_7

    const/4 v4, 0x1

    .line 509
    .local v4, "isCharacterTraversal":Z
    :goto_3
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getToIndex()I

    move-result v3

    .line 510
    .local v3, "endIndex":I
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v0

    .line 512
    .local v0, "begIndex":I
    if-eqz v4, :cond_9

    .line 513
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$SelectedTextFormatter;->shouldReverseCharacterIndices(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 514
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v0

    .line 515
    move v3, v0

    .line 526
    :cond_6
    :goto_4
    # invokes: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->areValidIndices(Ljava/lang/CharSequence;II)Z
    invoke-static {v9, v0, v3}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$400(Ljava/lang/CharSequence;II)Z

    move-result v10

    if-nez v10, :cond_a

    .line 527
    const/4 v10, 0x0

    goto :goto_0

    .line 506
    .end local v0    # "begIndex":I
    .end local v3    # "endIndex":I
    .end local v4    # "isCharacterTraversal":Z
    :cond_7
    const/4 v4, 0x0

    goto :goto_3

    .line 517
    .restart local v0    # "begIndex":I
    .restart local v3    # "endIndex":I
    .restart local v4    # "isCharacterTraversal":Z
    :cond_8
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getToIndex()I

    move-result v0

    .line 518
    move v3, v0

    goto :goto_4

    .line 520
    :cond_9
    if-le v0, v3, :cond_6

    .line 521
    move v8, v0

    .line 522
    .local v8, "temp":I
    move v0, v3

    .line 523
    move v3, v8

    goto :goto_4

    .line 532
    .end local v8    # "temp":I
    :cond_a
    if-eq v0, v3, :cond_b

    .line 533
    invoke-interface {v9, v0, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    .line 541
    .local v6, "selectedText":Ljava/lang/CharSequence;
    :goto_5
    invoke-static {p2, v6}, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->cleanUp(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 542
    .local v1, "cleanedText":Ljava/lang/CharSequence;
    invoke-virtual {p3, v1}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 543
    const/4 v10, 0x1

    goto :goto_0

    .line 534
    .end local v1    # "cleanedText":Ljava/lang/CharSequence;
    .end local v6    # "selectedText":Ljava/lang/CharSequence;
    :cond_b
    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    move-result v10

    if-ge v3, v10, :cond_c

    .line 535
    add-int/lit8 v10, v3, 0x1

    invoke-interface {v9, v0, v10}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    .restart local v6    # "selectedText":Ljava/lang/CharSequence;
    goto :goto_5

    .line 538
    .end local v6    # "selectedText":Ljava/lang/CharSequence;
    :cond_c
    const v10, 0x7f06018e

    invoke-virtual {p2, v10}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "selectedText":Ljava/lang/CharSequence;
    goto :goto_5
.end method
