.class Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$10;
.super Ljava/lang/Object;
.source "TalkBackPreferencesActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V
    .locals 0

    .prologue
    .line 747
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 10
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x0

    .line 752
    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v8, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 753
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    # invokes: Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->createEnableTreeDebugDialog()Landroid/app/AlertDialog;
    invoke-static {v9}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->access$600(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)Landroid/app/AlertDialog;

    move-result-object v9

    # setter for: Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTreeDebugDialog:Landroid/app/AlertDialog;
    invoke-static {v8, v9}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->access$302(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/AlertDialog;->show()V

    .line 775
    :goto_0
    return v7

    .line 760
    :cond_0
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-static {v8}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 762
    .local v6, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 764
    .local v4, "prefEditor":Landroid/content/SharedPreferences$Editor;
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    const v9, 0x7f060030

    invoke-virtual {v8, v9}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 766
    # getter for: Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->GESTURE_PREF_KEY_IDS:[I
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->access$700()[I

    move-result-object v0

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget v5, v0, v2

    .line 767
    .local v5, "prefKey":I
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-virtual {v7, v5}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 768
    .local v1, "currentValue":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    const v8, 0x7f060061

    invoke-virtual {v7, v8}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 769
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-virtual {v7, v5}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$10;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    const v9, 0x7f060058

    invoke-virtual {v8, v9}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v7, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 766
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 774
    .end local v1    # "currentValue":Ljava/lang/String;
    .end local v5    # "prefKey":I
    :cond_2
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 775
    const/4 v7, 0x1

    goto :goto_0
.end method
