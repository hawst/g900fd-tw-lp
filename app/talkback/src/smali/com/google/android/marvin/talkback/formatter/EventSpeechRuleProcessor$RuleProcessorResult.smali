.class final enum Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;
.super Ljava/lang/Enum;
.source "EventSpeechRuleProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "RuleProcessorResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

.field public static final enum FORMATTED:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

.field public static final enum NOT_MATCHED:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

.field public static final enum REJECTED:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 68
    new-instance v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    const-string v1, "FORMATTED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;->FORMATTED:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    .line 75
    new-instance v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    const-string v1, "NOT_MATCHED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;->NOT_MATCHED:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    .line 84
    new-instance v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    const-string v1, "REJECTED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;->REJECTED:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    .line 63
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    sget-object v1, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;->FORMATTED:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;->NOT_MATCHED:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;->REJECTED:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;->$VALUES:[Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 63
    const-class v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    return-object v0
.end method

.method public static values()[Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;->$VALUES:[Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    invoke-virtual {v0}, [Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    return-object v0
.end method
