.class public Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;
.super Ljava/lang/Object;
.source "SpeechController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/SpeechController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CompletionRunner"
.end annotation


# instance fields
.field private final mRunnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

.field private final mStatus:I


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;I)V
    .locals 0
    .param p1, "runnable"    # Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;
    .param p2, "status"    # I

    .prologue
    .line 1066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067
    iput-object p1, p0, Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;->mRunnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .line 1068
    iput p2, p0, Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;->mStatus:I

    .line 1069
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1073
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;->mRunnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    iget v1, p0, Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;->mStatus:I

    invoke-interface {v0, v1}, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;->run(I)V

    .line 1074
    return-void
.end method
