.class public Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;
.super Lcom/google/android/marvin/talkback/speechrules/RuleDefault;
.source "RuleNonTextViews.java"


# instance fields
.field private mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/speechrules/RuleDefault;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    return-void
.end method

.method private initLabelManagerIfNeeded()V
    .locals 3

    .prologue
    .line 102
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 103
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getInstance()Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    .line 104
    .local v0, "service":Lcom/google/android/marvin/talkback/TalkBackService;
    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getLabelManager()Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    .line 108
    .end local v0    # "service":Lcom/google/android/marvin/talkback/TalkBackService;
    :cond_0
    return-void
.end method


# virtual methods
.method public accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;->initLabelManagerIfNeeded()V

    .line 49
    const-class v0, Landroid/widget/ImageView;

    invoke-static {p1, p2, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public format(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const v11, 0x7f0600af

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 56
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/speechrules/RuleDefault;->format(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 57
    .local v5, "text":Ljava/lang/CharSequence;
    invoke-static {p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isClickable(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    .line 58
    .local v1, "isClickable":Z
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    move v0, v7

    .line 60
    .local v0, "hasLabel":Z
    :goto_0
    if-eqz v0, :cond_2

    .line 63
    if-eqz v1, :cond_0

    .line 64
    new-array v7, v7, [Ljava/lang/Object;

    aput-object v5, v7, v8

    invoke-virtual {p1, v11, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 92
    .end local v5    # "text":Ljava/lang/CharSequence;
    :cond_0
    :goto_1
    return-object v5

    .end local v0    # "hasLabel":Z
    .restart local v5    # "text":Ljava/lang/CharSequence;
    :cond_1
    move v0, v8

    .line 58
    goto :goto_0

    .line 69
    .restart local v0    # "hasLabel":Z
    :cond_2
    iget-object v9, p0, Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    if-eqz v9, :cond_4

    .line 71
    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getInfo()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/accessibility/AccessibilityNodeInfo;

    .line 74
    .local v6, "unwrappedNode":Landroid/view/accessibility/AccessibilityNodeInfo;
    iget-object v9, p0, Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    invoke-virtual {v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->getViewIdResourceName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->getLabelForViewIdFromCache(Ljava/lang/String;)Lcom/googlecode/eyesfree/labeling/Label;

    move-result-object v2

    .line 76
    .local v2, "label":Lcom/googlecode/eyesfree/labeling/Label;
    if-eqz v2, :cond_4

    .line 77
    invoke-virtual {v2}, Lcom/googlecode/eyesfree/labeling/Label;->getText()Ljava/lang/String;

    move-result-object v3

    .line 78
    .local v3, "labelText":Ljava/lang/CharSequence;
    if-eqz v1, :cond_3

    .line 79
    new-array v7, v7, [Ljava/lang/Object;

    aput-object v3, v7, v8

    invoke-virtual {p1, v11, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_3
    move-object v5, v3

    .line 81
    goto :goto_1

    .line 88
    .end local v2    # "label":Lcom/googlecode/eyesfree/labeling/Label;
    .end local v3    # "labelText":Ljava/lang/CharSequence;
    .end local v6    # "unwrappedNode":Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_4
    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->hashCode()I

    move-result v9

    rem-int/lit8 v9, v9, 0x64

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v4

    .line 89
    .local v4, "nodeInt":I
    if-eqz v1, :cond_5

    .line 90
    const v9, 0x7f0600b1

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v8

    invoke-virtual {p1, v9, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 92
    :cond_5
    const v9, 0x7f0600b0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v8

    invoke-virtual {p1, v9, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1
.end method

.method public bridge synthetic getHintText(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 37
    invoke-super {p0, p1, p2}, Lcom/google/android/marvin/talkback/speechrules/RuleDefault;->getHintText(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
