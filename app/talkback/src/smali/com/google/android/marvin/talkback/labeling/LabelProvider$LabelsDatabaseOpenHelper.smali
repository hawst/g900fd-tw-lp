.class final Lcom/google/android/marvin/talkback/labeling/LabelProvider$LabelsDatabaseOpenHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "LabelProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/labeling/LabelProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LabelsDatabaseOpenHelper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/labeling/LabelProvider;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/labeling/LabelProvider;Landroid/content/Context;)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 335
    iput-object p1, p0, Lcom/google/android/marvin/talkback/labeling/LabelProvider$LabelsDatabaseOpenHelper;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelProvider;

    .line 336
    const-string v0, "labelsDatabase.db"

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 337
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 341
    invoke-static {p1}, Lcom/googlecode/eyesfree/labeling/LabelsTable;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 342
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 346
    invoke-static {p1, p2, p3}, Lcom/googlecode/eyesfree/labeling/LabelsTable;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 347
    return-void
.end method
