.class Lcom/google/android/marvin/talkback/ProcessorLongHover$LongHoverHandler;
.super Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;
.source "ProcessorLongHover.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/ProcessorLongHover;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LongHoverHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/utils/WeakReferenceHandler",
        "<",
        "Lcom/google/android/marvin/talkback/ProcessorLongHover;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/ProcessorLongHover;)V
    .locals 0
    .param p1, "parent"    # Lcom/google/android/marvin/talkback/ProcessorLongHover;

    .prologue
    .line 222
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;-><init>(Ljava/lang/Object;)V

    .line 223
    return-void
.end method


# virtual methods
.method public cancelLongHoverTimeout()V
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorLongHover$LongHoverHandler;->removeMessages(I)V

    .line 246
    return-void
.end method

.method public handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/ProcessorLongHover;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;
    .param p2, "parent"    # Lcom/google/android/marvin/talkback/ProcessorLongHover;

    .prologue
    .line 227
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 235
    :goto_0
    return-void

    .line 229
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/view/accessibility/AccessibilityEvent;

    .line 230
    .local v0, "event":Landroid/view/accessibility/AccessibilityEvent;
    # invokes: Lcom/google/android/marvin/talkback/ProcessorLongHover;->speakLongHover(Landroid/view/accessibility/AccessibilityEvent;)V
    invoke-static {p2, v0}, Lcom/google/android/marvin/talkback/ProcessorLongHover;->access$200(Lcom/google/android/marvin/talkback/ProcessorLongHover;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 231
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    goto :goto_0

    .line 227
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic handleMessage(Landroid/os/Message;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Message;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 214
    check-cast p2, Lcom/google/android/marvin/talkback/ProcessorLongHover;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/marvin/talkback/ProcessorLongHover$LongHoverHandler;->handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/ProcessorLongHover;)V

    return-void
.end method

.method public startLongHoverTimeout(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 238
    invoke-static {p1}, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->obtain(Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 239
    .local v0, "eventClone":Landroid/view/accessibility/AccessibilityEvent;
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v0}, Lcom/google/android/marvin/talkback/ProcessorLongHover$LongHoverHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 241
    .local v1, "msg":Landroid/os/Message;
    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/marvin/talkback/ProcessorLongHover$LongHoverHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 242
    return-void
.end method
