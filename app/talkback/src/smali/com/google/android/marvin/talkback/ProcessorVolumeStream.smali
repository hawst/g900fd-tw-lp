.class public Lcom/google/android/marvin/talkback/ProcessorVolumeStream;
.super Ljava/lang/Object;
.source "ProcessorVolumeStream.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/TalkBackService$KeyEventListener;
.implements Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/ProcessorVolumeStream$LongPressHandler;
    }
.end annotation


# static fields
.field private static final WL_TAG:Ljava/lang/String;


# instance fields
.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mContext:Landroid/content/Context;

.field private final mCursorController:Lcom/google/android/marvin/talkback/CursorController;

.field private final mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

.field private final mKeysLongPressedAndDown:Landroid/util/SparseBooleanArray;

.field private final mLongPressHandler:Lcom/google/android/marvin/talkback/ProcessorVolumeStream$LongPressHandler;

.field private mTouchingScreen:Z

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->WL_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 3
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v1, Landroid/util/SparseBooleanArray;

    invoke-direct {v1}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mKeysLongPressedAndDown:Landroid/util/SparseBooleanArray;

    .line 90
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mTouchingScreen:Z

    .line 94
    iput-object p1, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mContext:Landroid/content/Context;

    .line 95
    const-string v1, "audio"

    invoke-virtual {p1, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mAudioManager:Landroid/media/AudioManager;

    .line 96
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    .line 97
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/CachedFeedbackController;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    .line 98
    new-instance v1, Lcom/google/android/marvin/talkback/ProcessorVolumeStream$LongPressHandler;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/ProcessorVolumeStream$LongPressHandler;-><init>(Lcom/google/android/marvin/talkback/ProcessorVolumeStream;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mLongPressHandler:Lcom/google/android/marvin/talkback/ProcessorVolumeStream$LongPressHandler;

    .line 100
    const-string v1, "power"

    invoke-virtual {p1, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 101
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x2000000a

    sget-object v2, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->WL_TAG:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 103
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/ProcessorVolumeStream;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/ProcessorVolumeStream;
    .param p1, "x1"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->handleVolumeKeyLongPressed(I)V

    return-void
.end method

.method private adjustVolumeFromKeyEvent(I)V
    .locals 4
    .param p1, "keyCode"    # I

    .prologue
    .line 230
    const/16 v1, 0x18

    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    .line 233
    .local v0, "direction":I
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mTouchingScreen:Z

    if-eqz v1, :cond_1

    .line 234
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mAudioManager:Landroid/media/AudioManager;

    const/4 v2, 0x3

    const/16 v3, 0x11

    invoke-virtual {v1, v2, v0, v3}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 244
    :goto_1
    return-void

    .line 230
    .end local v0    # "direction":I
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 241
    .restart local v0    # "direction":I
    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mAudioManager:Landroid/media/AudioManager;

    const/high16 v2, -0x80000000

    const/16 v3, 0x15

    invoke-virtual {v1, v0, v2, v3}, Landroid/media/AudioManager;->adjustSuggestedStreamVolume(III)V

    goto :goto_1
.end method

.method private attemptEditTextNavigation(I)Z
    .locals 8
    .param p1, "keyCode"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 197
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v5}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 199
    .local v1, "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v1, :cond_0

    :try_start_0
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mContext:Landroid/content/Context;

    const-class v6, Landroid/widget/EditText;

    invoke-static {v5, v1, v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_1

    .line 225
    :cond_0
    new-array v3, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v3, v4

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v3, v4

    :goto_0
    return v3

    .line 204
    :cond_1
    :try_start_1
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v5, v1}, Lcom/google/android/marvin/talkback/CursorController;->getGranularityAt(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lcom/google/android/marvin/talkback/CursorGranularity;

    move-result-object v0

    .line 206
    .local v0, "currentGranularity":Lcom/google/android/marvin/talkback/CursorGranularity;
    sget-object v5, Lcom/google/android/marvin/talkback/CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularity;

    if-ne v0, v5, :cond_2

    .line 207
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    sget-object v6, Lcom/google/android/marvin/talkback/CursorGranularity;->CHARACTER:Lcom/google/android/marvin/talkback/CursorGranularity;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/google/android/marvin/talkback/CursorController;->setGranularity(Lcom/google/android/marvin/talkback/CursorGranularity;Z)Z

    .line 210
    :cond_2
    const/4 v2, 0x0

    .line 211
    .local v2, "result":Z
    const/16 v5, 0x18

    if-ne p1, v5, :cond_5

    .line 212
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/google/android/marvin/talkback/CursorController;->next(ZZ)Z

    move-result v2

    .line 218
    :cond_3
    :goto_1
    if-nez v2, :cond_4

    .line 219
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    const v6, 0x7f050003

    invoke-virtual {v5, v6}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225
    :cond_4
    new-array v5, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v5, v4

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 213
    :cond_5
    const/16 v5, 0x19

    if-ne p1, v5, :cond_3

    .line 214
    :try_start_2
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/google/android/marvin/talkback/CursorController;->previous(ZZ)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    goto :goto_1

    .line 225
    .end local v0    # "currentGranularity":Lcom/google/android/marvin/talkback/CursorGranularity;
    .end local v2    # "result":Z
    :catchall_0
    move-exception v5

    new-array v3, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v3, v4

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v5
.end method

.method private handleBothVolumeKeysLongPressed()V
    .locals 0

    .prologue
    .line 194
    return-void
.end method

.method private handleKeyDown(I)Z
    .locals 1
    .param p1, "keyCode"    # I

    .prologue
    .line 140
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->isFromVolumeKey(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->handleVolumeKeyDown(I)V

    .line 142
    const/4 v0, 0x1

    .line 145
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handleKeyUp(I)Z
    .locals 1
    .param p1, "keyCode"    # I

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->isFromVolumeKey(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->handleVolumeKeyUp(I)V

    .line 151
    const/4 v0, 0x1

    .line 154
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handleVolumeKeyDown(I)V
    .locals 4
    .param p1, "keyCode"    # I

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mLongPressHandler:Lcom/google/android/marvin/talkback/ProcessorVolumeStream$LongPressHandler;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mLongPressHandler:Lcom/google/android/marvin/talkback/ProcessorVolumeStream$LongPressHandler;

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/marvin/talkback/ProcessorVolumeStream$LongPressHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 162
    return-void
.end method

.method private handleVolumeKeyLongPressed(I)V
    .locals 2
    .param p1, "keyCode"    # I

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mKeysLongPressedAndDown:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 187
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mKeysLongPressedAndDown:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 188
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->handleBothVolumeKeysLongPressed()V

    .line 190
    :cond_0
    return-void
.end method

.method private handleVolumeKeyUp(I)V
    .locals 3
    .param p1, "keyCode"    # I

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mKeysLongPressedAndDown:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 170
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mKeysLongPressedAndDown:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->delete(I)V

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mLongPressHandler:Lcom/google/android/marvin/talkback/ProcessorVolumeStream$LongPressHandler;

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/marvin/talkback/ProcessorVolumeStream$LongPressHandler;->removeMessages(ILjava/lang/Object;)V

    .line 177
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->attemptEditTextNavigation(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->adjustVolumeFromKeyEvent(I)V

    goto :goto_0
.end method

.method private isFromVolumeKey(I)Z
    .locals 1
    .param p1, "keyCode"    # I

    .prologue
    .line 247
    packed-switch p1, :pswitch_data_0

    .line 253
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 250
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 247
    nop

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 107
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 115
    :goto_0
    return-void

    .line 109
    :sswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mTouchingScreen:Z

    goto :goto_0

    .line 112
    :sswitch_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mTouchingScreen:Z

    goto :goto_0

    .line 107
    :sswitch_data_0
    .sparse-switch
        0x100000 -> :sswitch_0
        0x200000 -> :sswitch_1
    .end sparse-switch
.end method

.method public onKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 119
    const/4 v0, 0x0

    .line 120
    .local v0, "handled":Z
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 129
    :goto_0
    if-eqz v0, :cond_0

    .line 132
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 133
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 136
    :cond_0
    return v0

    .line 122
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->handleKeyDown(I)Z

    move-result v0

    .line 123
    goto :goto_0

    .line 125
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->handleKeyUp(I)Z

    move-result v0

    goto :goto_0

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
