.class Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;
.super Landroid/os/AsyncTask;
.source "LabelManagerPackageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateLabelsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/googlecode/eyesfree/labeling/Label;",
        ">;>;"
    }
.end annotation


# instance fields
.field private mLocale:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;
    .param p2, "x1"    # Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$1;

    .prologue
    .line 196
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;-><init>(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 196
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    .line 206
    const-string v1, "Spawning new UpdateLabelsTask(%d) for (%s, %s)."

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mPackageName:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->access$100(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    iget-object v3, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;->mLocale:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-static {p0, v5, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 209
    iget-object v1, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mLabelProviderClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->access$200(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;)Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mPackageName:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->access$100(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;->mLocale:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->getLabelsForPackage(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 211
    .local v0, "labelsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/googlecode/eyesfree/labeling/Label;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 196
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 216
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/labeling/Label;>;"
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    new-instance v1, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    iget-object v3, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    const v4, 0x7f030002

    invoke-direct {v1, v2, v3, v4, p1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;-><init>(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;Landroid/content/Context;ILjava/util/List;)V

    # setter for: Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mLabelAdapter:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;
    invoke-static {v0, v1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->access$302(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;)Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;

    .line 218
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mLabelList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->access$400(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->mLabelAdapter:Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;->access$300(Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;)Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$LabelAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 219
    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 201
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity$UpdateLabelsTask;->mLocale:Ljava/lang/String;

    .line 202
    return-void
.end method
