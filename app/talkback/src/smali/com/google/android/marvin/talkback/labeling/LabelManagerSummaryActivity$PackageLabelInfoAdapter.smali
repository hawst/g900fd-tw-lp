.class Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;
.super Landroid/widget/ArrayAdapter;
.source "LabelManagerSummaryActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PackageLabelInfoAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 122
    .local p4, "items":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;>;"
    iput-object p1, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    .line 123
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 125
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 126
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 17
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 130
    if-nez p2, :cond_0

    .line 131
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v13, 0x7f030004

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 134
    :cond_0
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;

    .line 135
    .local v8, "packageLabelInfo":Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;
    if-nez v8, :cond_1

    .line 184
    :goto_0
    return-object p2

    .line 139
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    invoke-virtual {v12}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    .line 140
    .local v9, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v8}, Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;->getPackageName()Ljava/lang/String;

    move-result-object v10

    .line 141
    .local v10, "packageName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 142
    .local v2, "applicationLabel":Ljava/lang/CharSequence;
    const/4 v1, 0x0

    .line 145
    .local v1, "applicationIcon":Landroid/graphics/drawable/Drawable;
    const/4 v12, 0x0

    :try_start_0
    invoke-virtual {v9, v10, v12}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    .line 146
    .local v7, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v12, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v9, v12}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 147
    invoke-virtual {v9, v10}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 152
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 153
    move-object v2, v10

    .line 156
    :cond_2
    if-nez v1, :cond_3

    .line 157
    invoke-virtual {v9}, Landroid/content/pm/PackageManager;->getDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 161
    .end local v7    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_3
    :goto_1
    const v12, 0x7f0d0075

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 162
    .local v11, "textView":Landroid/widget/TextView;
    invoke-virtual {v11, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    const v12, 0x7f0d0076

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 165
    .local v3, "countView":Landroid/widget/TextView;
    invoke-virtual {v8}, Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;->getLabelCount()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    const v12, 0x7f0d0071

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 168
    .local v5, "iconImage":Landroid/widget/ImageView;
    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 170
    new-instance v6, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    const-class v13, Lcom/google/android/marvin/talkback/labeling/LabelManagerPackageActivity;

    invoke-direct {v6, v12, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 172
    .local v6, "packageActivityIntent":Landroid/content/Intent;
    const/high16 v12, 0x10000000

    invoke-virtual {v6, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 173
    const/high16 v12, 0x4000000

    invoke-virtual {v6, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 174
    const-string v12, "packageName"

    invoke-virtual {v6, v12, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 177
    new-instance v12, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter$1;

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v6}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter$1;-><init>(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;Landroid/content/Intent;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 148
    .end local v3    # "countView":Landroid/widget/TextView;
    .end local v5    # "iconImage":Landroid/widget/ImageView;
    .end local v6    # "packageActivityIntent":Landroid/content/Intent;
    .end local v11    # "textView":Landroid/widget/TextView;
    :catch_0
    move-exception v4

    .line 149
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v12, 0x4

    :try_start_1
    const-string v13, "Could not load package info for package %s."

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-virtual {v8}, Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;->getPackageName()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13, v14}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 153
    move-object v2, v10

    .line 156
    :cond_4
    if-nez v1, :cond_3

    .line 157
    invoke-virtual {v9}, Landroid/content/pm/PackageManager;->getDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_1

    .line 152
    .end local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_0
    move-exception v12

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 153
    move-object v2, v10

    .line 156
    :cond_5
    if-nez v1, :cond_6

    .line 157
    invoke-virtual {v9}, Landroid/content/pm/PackageManager;->getDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :cond_6
    throw v12
.end method
