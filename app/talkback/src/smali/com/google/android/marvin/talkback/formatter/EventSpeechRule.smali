.class public Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
.super Ljava/lang/Object;
.source "EventSpeechRule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$ContextBasedRule;,
        Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;,
        Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;,
        Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;,
        Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;,
        Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;,
        Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFilter;
    }
.end annotation


# static fields
.field private static final mResourceIdentifier:Ljava/util/regex/Pattern;

.field private static final sEventTypeNameToValueMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sQueueModeNameToQueueModeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sTempBuilder:Landroid/text/SpannableStringBuilder;


# instance fields
.field private mCachedXmlString:Ljava/lang/String;

.field private final mContext:Lcom/google/android/marvin/talkback/TalkBackService;

.field private final mCustomEarcons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mCustomVibrations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mEarcons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mFilter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

.field private final mFormatter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

.field private final mMetadata:Landroid/os/Bundle;

.field private mNode:Lorg/w3c/dom/Node;

.field private mPackageName:Ljava/lang/String;

.field private final mPropertyMatchers:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final mRuleIndex:I

.field private final mVibrationPatterns:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 119
    const-string v0, "@([\\w\\.]+:)?\\w+/\\w+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mResourceIdentifier:Ljava/util/regex/Pattern;

    .line 122
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sTempBuilder:Landroid/text/SpannableStringBuilder;

    .line 125
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    .line 128
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_CLICKED"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_LONG_CLICKED"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_SELECTED"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_FOCUSED"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_TEXT_CHANGED"

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_WINDOW_STATE_CHANGED"

    const/16 v2, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_NOTIFICATION_STATE_CHANGED"

    const/16 v2, 0x40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_HOVER_ENTER"

    const/16 v2, 0x80

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_HOVER_EXIT"

    const/16 v2, 0x100

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_TOUCH_EXPLORATION_GESTURE_START"

    const/16 v2, 0x200

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_TOUCH_EXPLORATION_GESTURE_END"

    const/16 v2, 0x400

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_WINDOW_CONTENT_CHANGED"

    const/16 v2, 0x800

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_SCROLLED"

    const/16 v2, 0x1000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_TEXT_SELECTION_CHANGED"

    const/16 v2, 0x2000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_ANNOUNCEMENT"

    const/16 v2, 0x4000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_ACCESSIBILITY_FOCUSED"

    const v2, 0x8000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED"

    const/high16 v2, 0x10000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY"

    const/high16 v2, 0x20000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_TOUCH_INTERACTION_START"

    const/high16 v2, 0x100000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_TOUCH_INTERACTION_END"

    const/high16 v2, 0x200000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_GESTURE_DETECTION_START"

    const/high16 v2, 0x40000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_GESTURE_DETECTION_END"

    const/high16 v2, 0x80000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sQueueModeNameToQueueModeMap:Ljava/util/HashMap;

    .line 177
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sQueueModeNameToQueueModeMap:Ljava/util/HashMap;

    const-string v1, "INTERRUPT"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sQueueModeNameToQueueModeMap:Ljava/util/HashMap;

    const-string v1, "QUEUE"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sQueueModeNameToQueueModeMap:Ljava/util/HashMap;

    const-string v1, "UNINTERRUPTIBLE"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    return-void
.end method

.method private constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;Lorg/w3c/dom/Node;I)V
    .locals 9
    .param p1, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p2, "node"    # Lorg/w3c/dom/Node;
    .param p3, "ruleIndex"    # I

    .prologue
    .line 252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mMetadata:Landroid/os/Bundle;

    .line 192
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mEarcons:Ljava/util/List;

    .line 198
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mVibrationPatterns:Ljava/util/List;

    .line 204
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCustomEarcons:Ljava/util/List;

    .line 210
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCustomVibrations:Ljava/util/List;

    .line 213
    new-instance v7, Ljava/util/LinkedHashMap;

    invoke-direct {v7}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mPropertyMatchers:Ljava/util/LinkedHashMap;

    .line 229
    const-string v7, "undefined_package_name"

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mPackageName:Ljava/lang/String;

    .line 253
    iput-object p1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 254
    iput-object p2, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mNode:Lorg/w3c/dom/Node;

    .line 256
    const/4 v3, 0x0

    .line 257
    .local v3, "filter":Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;
    const/4 v4, 0x0

    .line 261
    .local v4, "formatter":Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;
    invoke-interface {p2}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 263
    .local v1, "children":Lorg/w3c/dom/NodeList;
    const/4 v5, 0x0

    .local v5, "i":I
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    .local v2, "count":I
    :goto_0
    if-ge v5, v2, :cond_4

    .line 264
    invoke-interface {v1, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 266
    .local v0, "child":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v7

    const/4 v8, 0x1

    if-eq v7, v8, :cond_1

    .line 263
    :cond_0
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 270
    :cond_1
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getUnqualifiedNodeName(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    .line 272
    .local v6, "nodeName":Ljava/lang/String;
    const-string v7, "metadata"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 273
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->populateMetadata(Lorg/w3c/dom/Node;)V

    goto :goto_1

    .line 274
    :cond_2
    const-string v7, "filter"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 275
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->createFilter(Lorg/w3c/dom/Node;)Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

    move-result-object v3

    goto :goto_1

    .line 276
    :cond_3
    const-string v7, "formatter"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 277
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->createFormatter(Lorg/w3c/dom/Node;)Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

    move-result-object v4

    goto :goto_1

    .line 281
    .end local v0    # "child":Lorg/w3c/dom/Node;
    .end local v6    # "nodeName":Ljava/lang/String;
    :cond_4
    instance-of v7, v4, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$ContextBasedRule;

    if-eqz v7, :cond_5

    move-object v7, v4

    .line 282
    check-cast v7, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$ContextBasedRule;

    invoke-interface {v7, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$ContextBasedRule;->initialize(Lcom/google/android/marvin/talkback/TalkBackService;)V

    .line 285
    :cond_5
    instance-of v7, v3, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$ContextBasedRule;

    if-eqz v7, :cond_6

    move-object v7, v3

    .line 286
    check-cast v7, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$ContextBasedRule;

    invoke-interface {v7, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$ContextBasedRule;->initialize(Lcom/google/android/marvin/talkback/TalkBackService;)V

    .line 289
    :cond_6
    iput-object v3, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mFilter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

    .line 290
    iput-object v4, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mFormatter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

    .line 291
    iput p3, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mRuleIndex:I

    .line 292
    return-void
.end method

.method static synthetic access$000(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/w3c/dom/Node;

    .prologue
    .line 63
    invoke-static {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getUnqualifiedNodeName(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/w3c/dom/Node;

    .prologue
    .line 63
    invoke-static {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-static {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getPropertyType(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-static {p0, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->parsePropertyValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Ljava/util/LinkedHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mPropertyMatchers:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mPackageName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;Landroid/content/Context;Ljava/lang/String;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getPropertyValue(Landroid/content/Context;Ljava/lang/String;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Lcom/google/android/marvin/talkback/TalkBackService;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    return-object v0
.end method

.method static synthetic access$700(Landroid/content/Context;Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Lorg/w3c/dom/Node;

    .prologue
    .line 63
    invoke-static {p0, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getLocalizedTextContent(Landroid/content/Context;Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Landroid/content/Context;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-static {p0, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getResourceIdentifierContent(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    .prologue
    .line 63
    iget v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mRuleIndex:I

    return v0
.end method

.method private createFilter(Lorg/w3c/dom/Node;)Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;
    .locals 7
    .param p1, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 533
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 536
    .local v1, "children":Lorg/w3c/dom/NodeList;
    const/4 v3, 0x0

    .local v3, "i":I
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    .local v2, "count":I
    :goto_0
    if-ge v3, v2, :cond_2

    .line 537
    invoke-interface {v1, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 539
    .local v0, "child":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_1

    .line 536
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 543
    :cond_1
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getUnqualifiedNodeName(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    .line 544
    .local v4, "nodeName":Ljava/lang/String;
    const-string v5, "custom"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 545
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    const-class v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

    invoke-direct {p0, v5, v6}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->createNewInstance(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

    .line 549
    .end local v0    # "child":Lorg/w3c/dom/Node;
    .end local v4    # "nodeName":Ljava/lang/String;
    :goto_1
    return-object v5

    :cond_2
    new-instance v5, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFilter;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {v5, p0, v6, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFilter;-><init>(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;Landroid/content/Context;Lorg/w3c/dom/Node;)V

    goto :goto_1
.end method

.method private createFormatter(Lorg/w3c/dom/Node;)Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;
    .locals 7
    .param p1, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 559
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 561
    .local v1, "children":Lorg/w3c/dom/NodeList;
    const/4 v3, 0x0

    .local v3, "i":I
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    .local v2, "count":I
    :goto_0
    if-ge v3, v2, :cond_2

    .line 562
    invoke-interface {v1, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 564
    .local v0, "child":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_1

    .line 561
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 568
    :cond_1
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getUnqualifiedNodeName(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    .line 569
    .local v4, "nodeName":Ljava/lang/String;
    const-string v5, "custom"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 570
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    const-class v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

    invoke-direct {p0, v5, v6}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->createNewInstance(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

    .line 574
    .end local v0    # "child":Lorg/w3c/dom/Node;
    .end local v4    # "nodeName":Ljava/lang/String;
    :goto_1
    return-object v5

    :cond_2
    new-instance v5, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;

    invoke-direct {v5, p0, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;-><init>(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;Lorg/w3c/dom/Node;)V

    goto :goto_1
.end method

.method private createNewInstance(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 8
    .param p1, "className"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 587
    .local p2, "expectedClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    :try_start_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 589
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 597
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    :goto_0
    return-object v2

    .line 590
    :catch_0
    move-exception v1

    .line 591
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 593
    const-class v2, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    const/4 v3, 0x6

    const-string v4, "Rule: #%d. Could not load class: \'%s\'."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mRuleIndex:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object p1, v5, v6

    invoke-static {v2, v3, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 597
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static createSpeechRules(Lcom/google/android/marvin/talkback/TalkBackService;Lorg/w3c/dom/Document;)Ljava/util/ArrayList;
    .locals 14
    .param p0, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p1, "document"    # Lorg/w3c/dom/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/marvin/talkback/TalkBackService;",
            "Lorg/w3c/dom/Document;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v13, 0x1

    .line 611
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 613
    .local v6, "speechRules":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;>;"
    if-eqz p1, :cond_0

    if-nez p0, :cond_1

    .line 638
    :cond_0
    return-object v6

    .line 617
    :cond_1
    invoke-interface {p1}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v7

    invoke-interface {v7}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 619
    .local v1, "children":Lorg/w3c/dom/NodeList;
    const/4 v4, 0x0

    .local v4, "i":I
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    .local v2, "count":I
    :goto_0
    if-ge v4, v2, :cond_0

    .line 620
    invoke-interface {v1, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 622
    .local v0, "child":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v7

    if-eq v7, v13, :cond_2

    .line 619
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 627
    :cond_2
    :try_start_0
    new-instance v5, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    invoke-direct {v5, p0, v0, v4}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;Lorg/w3c/dom/Node;I)V

    .line 629
    .local v5, "rule":Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 630
    .end local v5    # "rule":Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
    :catch_0
    move-exception v3

    .line 631
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 633
    const-class v7, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    const/4 v8, 0x5

    const-string v9, "Failed loading speech rule: %s"

    new-array v10, v13, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v7, v8, v9, v10}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private static getLocalizedTextContent(Landroid/content/Context;Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 646
    invoke-static {p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v1

    .line 647
    .local v1, "textContent":Ljava/lang/String;
    invoke-static {p0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getResourceIdentifierContent(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 649
    .local v0, "resId":I
    if-lez v0, :cond_0

    .line 650
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 653
    .end local v1    # "textContent":Ljava/lang/String;
    :cond_0
    return-object v1
.end method

.method private getNodeDescriptionOrFallback(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 7
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 893
    const/4 v2, 0x0

    .line 896
    .local v2, "source":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_start_0
    new-instance v4, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v4, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v4}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    .line 897
    if-eqz v2, :cond_0

    .line 898
    invoke-static {}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->getInstance()Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    move-result-object v1

    .line 899
    .local v1, "nodeProcessor":Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;
    invoke-virtual {v1, v2, p1, v2}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->getDescriptionForTree(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 901
    .local v3, "treeDescription":Ljava/lang/CharSequence;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    .line 906
    new-array v4, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v2, v4, v6

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 915
    .end local v1    # "nodeProcessor":Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;
    .end local v3    # "treeDescription":Ljava/lang/CharSequence;
    :goto_0
    return-object v3

    .line 906
    :cond_0
    new-array v4, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v2, v4, v6

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 909
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->getEventTextOrDescription(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 911
    .local v0, "eventDescription":Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    move-object v3, v0

    .line 912
    goto :goto_0

    .line 906
    .end local v0    # "eventDescription":Ljava/lang/CharSequence;
    :catchall_0
    move-exception v4

    new-array v5, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v2, v5, v6

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v4

    .line 915
    .restart local v0    # "eventDescription":Ljava/lang/CharSequence;
    :cond_1
    const-string v3, ""

    goto :goto_0
.end method

.method private static final getPropertyType(Ljava/lang/String;)I
    .locals 1
    .param p0, "propertyName"    # Ljava/lang/String;

    .prologue
    .line 448
    invoke-static {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->isBooleanProperty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 449
    const/4 v0, 0x1

    .line 457
    :goto_0
    return v0

    .line 450
    :cond_0
    invoke-static {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->isFloatProperty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 451
    const/4 v0, 0x2

    goto :goto_0

    .line 452
    :cond_1
    invoke-static {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->isIntegerProperty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 453
    const/4 v0, 0x3

    goto :goto_0

    .line 454
    :cond_2
    invoke-static {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->isStringProperty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 455
    const/4 v0, 0x4

    goto :goto_0

    .line 457
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getPropertyValue(Landroid/content/Context;Ljava/lang/String;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/Object;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "property"    # Ljava/lang/String;
    .param p3, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 819
    new-instance v0, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v0, p3}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 822
    .local v0, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    const-string v1, "eventType"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 823
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 877
    :goto_0
    return-object v1

    .line 824
    :cond_0
    const-string v1, "packageName"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 825
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    .line 826
    :cond_1
    const-string v1, "className"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 827
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getClassName()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    .line 828
    :cond_2
    const-string v1, "classNameStrict"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 829
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getClassName()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    .line 830
    :cond_3
    const-string v1, "text"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 831
    invoke-static {p3}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->getEventAggregateText(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    .line 832
    :cond_4
    const-string v1, "beforeText"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 833
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getBeforeText()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    .line 834
    :cond_5
    const-string v1, "contentDescription"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 835
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    .line 836
    :cond_6
    const-string v1, "contentDescriptionOrText"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 837
    invoke-static {p3}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->getEventTextOrDescription(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    .line 838
    :cond_7
    const-string v1, "nodeDescriptionOrFallback"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 839
    invoke-direct {p0, p3}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getNodeDescriptionOrFallback(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    .line 840
    :cond_8
    const-string v1, "eventTime"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 841
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 842
    :cond_9
    const-string v1, "itemCount"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 843
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getItemCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    .line 844
    :cond_a
    const-string v1, "currentItemIndex"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 845
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getCurrentItemIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    .line 846
    :cond_b
    const-string v1, "fromIndex"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 847
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    .line 848
    :cond_c
    const-string v1, "toIndex"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 849
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getToIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    .line 850
    :cond_d
    const-string v1, "scrollable"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 851
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->isScrollable()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_0

    .line 852
    :cond_e
    const-string v1, "scrollX"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 853
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getScrollX()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    .line 854
    :cond_f
    const-string v1, "scrollY"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 855
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getScrollY()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    .line 856
    :cond_10
    const-string v1, "recordCount"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 857
    invoke-static {p3}, Landroid/support/v4/view/accessibility/AccessibilityEventCompat;->getRecordCount(Landroid/view/accessibility/AccessibilityEvent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    .line 858
    :cond_11
    const-string v1, "checked"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 859
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->isChecked()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_0

    .line 860
    :cond_12
    const-string v1, "enabled"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 861
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->isEnabled()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_0

    .line 862
    :cond_13
    const-string v1, "fullScreen"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 863
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->isFullScreen()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_0

    .line 864
    :cond_14
    const-string v1, "password"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 865
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->isPassword()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_0

    .line 866
    :cond_15
    const-string v1, "addedCount"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 867
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getAddedCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    .line 868
    :cond_16
    const-string v1, "removedCount"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 869
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getRemovedCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    .line 870
    :cond_17
    const-string v1, "versionCode"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 871
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->getVersionCode(Landroid/content/Context;Ljava/lang/CharSequence;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    .line 872
    :cond_18
    const-string v1, "versionName"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 873
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->getVersionName(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 874
    :cond_19
    const-string v1, "platformRelease"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 875
    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    goto/16 :goto_0

    .line 876
    :cond_1a
    const-string v1, "platformSdk"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 877
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    .line 879
    :cond_1b
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown property : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static getResourceIdentifierContent(Landroid/content/Context;Ljava/lang/String;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v3, -0x1

    .line 670
    if-nez p1, :cond_1

    .line 688
    :cond_0
    :goto_0
    return v3

    .line 674
    :cond_1
    sget-object v5, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mResourceIdentifier:Ljava/util/regex/Pattern;

    invoke-virtual {v5, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 676
    .local v1, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 680
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 681
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v5

    const/4 v6, 0x2

    if-ge v5, v6, :cond_2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 682
    .local v0, "defaultPackage":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5, v4, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 684
    .local v3, "resId":I
    if-nez v3, :cond_0

    .line 685
    const-class v4, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    const/4 v5, 0x6

    const-string v6, "Failed to load resource: %s"

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    invoke-static {v4, v5, v6, v7}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .end local v0    # "defaultPackage":Ljava/lang/String;
    .end local v3    # "resId":I
    :cond_2
    move-object v0, v4

    .line 681
    goto :goto_1
.end method

.method private static getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 4
    .param p0, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 701
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sTempBuilder:Landroid/text/SpannableStringBuilder;

    .line 702
    .local v0, "builder":Landroid/text/SpannableStringBuilder;
    invoke-static {p0, v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContentRecursive(Lorg/w3c/dom/Node;Landroid/text/SpannableStringBuilder;)V

    .line 703
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 704
    .local v1, "text":Ljava/lang/String;
    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    .line 705
    return-object v1
.end method

.method private static getTextContentRecursive(Lorg/w3c/dom/Node;Landroid/text/SpannableStringBuilder;)V
    .locals 6
    .param p0, "node"    # Lorg/w3c/dom/Node;
    .param p1, "builder"    # Landroid/text/SpannableStringBuilder;

    .prologue
    .line 716
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 717
    .local v1, "children":Lorg/w3c/dom/NodeList;
    const/4 v3, 0x0

    .local v3, "i":I
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    .local v2, "count":I
    :goto_0
    if-ge v3, v2, :cond_1

    .line 718
    invoke-interface {v1, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 719
    .local v0, "child":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 720
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 722
    :cond_0
    invoke-static {v0, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContentRecursive(Lorg/w3c/dom/Node;Landroid/text/SpannableStringBuilder;)V

    .line 717
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 724
    .end local v0    # "child":Lorg/w3c/dom/Node;
    :cond_1
    return-void
.end method

.method private static getUnqualifiedNodeName(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 3
    .param p0, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 733
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v1

    .line 734
    .local v1, "nodeName":Ljava/lang/String;
    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 735
    .local v0, "colonIndex":I
    const/4 v2, -0x1

    if-le v0, v2, :cond_0

    .line 736
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 738
    :cond_0
    return-object v1
.end method

.method private static isBooleanProperty(Ljava/lang/String;)Z
    .locals 1
    .param p0, "propertyName"    # Ljava/lang/String;

    .prologue
    .line 519
    const-string v0, "checked"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "enabled"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "fullScreen"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "scrollable"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "password"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isFloatProperty(Ljava/lang/String;)Z
    .locals 1
    .param p0, "propertyName"    # Ljava/lang/String;

    .prologue
    .line 490
    const-string v0, "eventTime"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static isIntegerProperty(Ljava/lang/String;)Z
    .locals 1
    .param p0, "propertyName"    # Ljava/lang/String;

    .prologue
    .line 468
    const-string v0, "eventType"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "itemCount"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "currentItemIndex"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "fromIndex"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "toIndex"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "scrollX"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "scrollY"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "recordCount"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "addedCount"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "removedCount"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "queuing"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "versionCode"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "platformSdk"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isStringProperty(Ljava/lang/String;)Z
    .locals 1
    .param p0, "propertyName"    # Ljava/lang/String;

    .prologue
    .line 500
    const-string v0, "packageName"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "className"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "classNameStrict"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "text"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "beforeText"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "contentDescription"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "contentDescriptionOrText"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "nodeDescriptionOrFallback"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "versionName"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "platformRelease"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static parsePropertyValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;
    .locals 8
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Comparable",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x5

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 417
    const-string v3, "eventType"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 418
    sget-object v2, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Comparable;

    .line 441
    :goto_0
    return-object v2

    .line 421
    :cond_0
    invoke-static {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getPropertyType(Ljava/lang/String;)I

    move-result v1

    .line 423
    .local v1, "propertyType":I
    packed-switch v1, :pswitch_data_0

    .line 443
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown property: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 425
    :pswitch_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 428
    :pswitch_1
    :try_start_0
    invoke-static {p1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 429
    :catch_0
    move-exception v0

    .line 430
    .local v0, "nfe":Ljava/lang/NumberFormatException;
    const-class v3, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    const-string v4, "Property \'%s\' not float."

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v6

    invoke-static {v3, v7, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 435
    .end local v0    # "nfe":Ljava/lang/NumberFormatException;
    :pswitch_2
    :try_start_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    goto :goto_0

    .line 436
    :catch_1
    move-exception v0

    .line 437
    .restart local v0    # "nfe":Ljava/lang/NumberFormatException;
    const-class v3, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    const-string v4, "Property \'%s\' not int."

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v6

    invoke-static {v3, v7, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .end local v0    # "nfe":Ljava/lang/NumberFormatException;
    :pswitch_3
    move-object v2, p1

    .line 441
    goto :goto_0

    .line 423
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private populateMetadata(Lorg/w3c/dom/Node;)V
    .locals 11
    .param p1, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 375
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 376
    .local v3, "metadata":Lorg/w3c/dom/NodeList;
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    .line 378
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_6

    .line 379
    invoke-interface {v3, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 381
    .local v0, "child":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_0

    .line 378
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 385
    :cond_0
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getUnqualifiedNodeName(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v7

    .line 386
    .local v7, "unqualifiedName":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    .line 388
    .local v6, "textContent":Ljava/lang/String;
    const-string v9, "queuing"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 389
    sget-object v9, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sQueueModeNameToQueueModeMap:Ljava/util/HashMap;

    invoke-virtual {v9, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 390
    .local v4, "mode":I
    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mMetadata:Landroid/os/Bundle;

    invoke-virtual {v9, v7, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 391
    .end local v4    # "mode":I
    :cond_1
    const-string v9, "earcon"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 392
    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v9, v6}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getResourceIdentifierContent(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    .line 393
    .local v5, "resId":I
    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mEarcons:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 394
    .end local v5    # "resId":I
    :cond_2
    const-string v9, "vibration"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 395
    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v9, v6}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getResourceIdentifierContent(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    .line 396
    .restart local v5    # "resId":I
    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mVibrationPatterns:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 397
    .end local v5    # "resId":I
    :cond_3
    const-string v9, "customEarcon"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 398
    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCustomEarcons:Ljava/util/List;

    iget-object v10, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v10, v6}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getResourceIdentifierContent(Landroid/content/Context;Ljava/lang/String;)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 399
    :cond_4
    const-string v9, "customVibration"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 400
    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCustomVibrations:Ljava/util/List;

    iget-object v10, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v10, v6}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getResourceIdentifierContent(Landroid/content/Context;Ljava/lang/String;)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 402
    :cond_5
    invoke-static {v7, v6}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->parsePropertyValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 403
    .local v8, "value":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mMetadata:Landroid/os/Bundle;

    invoke-virtual {v9, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 406
    .end local v0    # "child":Lorg/w3c/dom/Node;
    .end local v6    # "textContent":Ljava/lang/String;
    .end local v7    # "unqualifiedName":Ljava/lang/String;
    .end local v8    # "value":Ljava/lang/String;
    :cond_6
    return-void
.end method


# virtual methods
.method public applyFilter(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mFilter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

    if-nez v0, :cond_0

    .line 339
    const/4 v0, 0x1

    .line 341
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mFilter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-interface {v0, p1, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;->accept(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;)Z

    move-result v0

    goto :goto_0
.end method

.method public applyFormatter(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mFormatter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mFormatter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-interface {v0, p1, v1, p2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;->format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 358
    const/4 v0, 0x0

    .line 365
    :goto_0
    return v0

    .line 361
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/marvin/talkback/Utterance;->getMetadata()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mMetadata:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 362
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCustomEarcons:Ljava/util/List;

    invoke-virtual {p2, v0}, Lcom/google/android/marvin/talkback/Utterance;->addAllAuditory(Ljava/util/Collection;)V

    .line 363
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCustomVibrations:Ljava/util/List;

    invoke-virtual {p2, v0}, Lcom/google/android/marvin/talkback/Utterance;->addAllHaptic(Ljava/util/Collection;)V

    .line 365
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getFilter()Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mFilter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

    return-object v0
.end method

.method public getFormatter()Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mFormatter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCachedXmlString:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mNode:Lorg/w3c/dom/Node;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mNode:Lorg/w3c/dom/Node;

    invoke-static {v0}, Lcom/google/android/marvin/utils/NodeUtils;->asXmlString(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCachedXmlString:Ljava/lang/String;

    .line 301
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mNode:Lorg/w3c/dom/Node;

    .line 304
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCachedXmlString:Ljava/lang/String;

    return-object v0
.end method
