.class Lcom/google/android/marvin/talkback/menurules/RuleViewPager$ViewPagerItemClickListener;
.super Ljava/lang/Object;
.source "RuleViewPager.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/menurules/RuleViewPager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewPagerItemClickListener"
.end annotation


# instance fields
.field private final mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;


# direct methods
.method public constructor <init>(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 0
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    iput-object p1, p0, Lcom/google/android/marvin/talkback/menurules/RuleViewPager$ViewPagerItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 124
    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 128
    if-nez p1, :cond_0

    .line 129
    iget-object v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleViewPager$ViewPagerItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 143
    :goto_0
    return v1

    .line 133
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 134
    .local v0, "itemId":I
    const v2, 0x7f0d0002

    if-ne v0, v2, :cond_1

    .line 135
    iget-object v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleViewPager$ViewPagerItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/16 v3, 0x2000

    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    .line 142
    :goto_1
    iget-object v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleViewPager$ViewPagerItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    .line 136
    :cond_1
    const v2, 0x7f0d0003

    if-ne v0, v2, :cond_2

    .line 137
    iget-object v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleViewPager$ViewPagerItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/16 v3, 0x1000

    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    goto :goto_1

    .line 139
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
