.class Lcom/google/android/marvin/talkback/menurules/RuleSpannables$SpannableMenuClickListener;
.super Ljava/lang/Object;
.source "RuleSpannables.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/menurules/RuleSpannables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SpannableMenuClickListener"
.end annotation


# instance fields
.field final mContext:Landroid/content/Context;

.field final mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Lcom/google/android/marvin/talkback/menurules/RuleSpannables$SpannableMenuClickListener;->mContext:Landroid/content/Context;

    .line 102
    iput-object p2, p0, Lcom/google/android/marvin/talkback/menurules/RuleSpannables$SpannableMenuClickListener;->mUri:Landroid/net/Uri;

    .line 103
    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x0

    .line 107
    iget-object v3, p0, Lcom/google/android/marvin/talkback/menurules/RuleSpannables$SpannableMenuClickListener;->mContext:Landroid/content/Context;

    if-nez v3, :cond_0

    .line 119
    :goto_0
    return v2

    .line 111
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    iget-object v4, p0, Lcom/google/android/marvin/talkback/menurules/RuleSpannables$SpannableMenuClickListener;->mUri:Landroid/net/Uri;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 112
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 114
    :try_start_0
    iget-object v3, p0, Lcom/google/android/marvin/talkback/menurules/RuleSpannables$SpannableMenuClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    const/4 v2, 0x1

    goto :goto_0

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    goto :goto_0
.end method
