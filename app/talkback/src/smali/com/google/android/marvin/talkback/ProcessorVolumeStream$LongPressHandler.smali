.class Lcom/google/android/marvin/talkback/ProcessorVolumeStream$LongPressHandler;
.super Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;
.source "ProcessorVolumeStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/ProcessorVolumeStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LongPressHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/utils/WeakReferenceHandler",
        "<",
        "Lcom/google/android/marvin/talkback/ProcessorVolumeStream;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/ProcessorVolumeStream;)V
    .locals 0
    .param p1, "parent"    # Lcom/google/android/marvin/talkback/ProcessorVolumeStream;

    .prologue
    .line 260
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;-><init>(Ljava/lang/Object;)V

    .line 261
    return-void
.end method


# virtual methods
.method protected handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/ProcessorVolumeStream;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;
    .param p2, "parent"    # Lcom/google/android/marvin/talkback/ProcessorVolumeStream;

    .prologue
    .line 265
    const/4 v0, 0x1

    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v0, v1, :cond_0

    .line 266
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    # invokes: Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->handleVolumeKeyLongPressed(I)V
    invoke-static {p2, v0}, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;->access$000(Lcom/google/android/marvin/talkback/ProcessorVolumeStream;I)V

    .line 268
    :cond_0
    return-void
.end method

.method protected bridge synthetic handleMessage(Landroid/os/Message;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Message;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 256
    check-cast p2, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/marvin/talkback/ProcessorVolumeStream$LongPressHandler;->handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/ProcessorVolumeStream;)V

    return-void
.end method
