.class public Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;
.super Landroid/app/Activity;
.source "GestureChangeNotificationActivity.java"


# instance fields
.field private final mActionNameRes:[I

.field private mAlert:Landroid/app/AlertDialog;

.field private final mGestureNameRes:[I

.field private mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 24
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mGestureNameRes:[I

    .line 34
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mActionNameRes:[I

    return-void

    .line 24
    :array_0
    .array-data 4
        0x7f06014b
        0x7f06014d
        0x7f06014c
        0x7f06014e
        0x7f06014f
        0x7f060151
        0x7f060150
        0x7f060152
    .end array-data

    .line 34
    :array_1
    .array-data 4
        0x7f06011d
        0x7f06011e
        0x7f060121
        0x7f060122
        0x7f060120
        0x7f06011c
        0x7f06011c
        0x7f06011f
    .end array-data
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->clearPreviouslyConfiguredMappings()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->dismissNotification()V

    return-void
.end method

.method private clearPreviouslyConfiguredMappings()V
    .locals 2

    .prologue
    .line 94
    iget-object v1, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 96
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const v1, 0x7f060044

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 97
    const v1, 0x7f060045

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 98
    const v1, 0x7f060042

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 99
    const v1, 0x7f060043

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 100
    const v1, 0x7f060046

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 101
    const v1, 0x7f060047

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 102
    const v1, 0x7f060048

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 103
    const v1, 0x7f060049

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 105
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 106
    return-void
.end method

.method private dismissNotification()V
    .locals 4

    .prologue
    .line 122
    const/4 v2, 0x2

    .line 123
    .local v2, "notificationId":I
    const-string v3, "notification"

    invoke-virtual {p0, v3}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 124
    .local v1, "nm":Landroid/app/NotificationManager;
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 127
    iget-object v3, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 128
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const v3, 0x7f060050

    invoke-virtual {p0, v3}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 129
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 130
    return-void
.end method

.method private getMappingDescription([I[I)Ljava/lang/CharSequence;
    .locals 4
    .param p1, "directions"    # [I
    .param p2, "mappings"    # [I

    .prologue
    .line 109
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 111
    .local v1, "sb":Landroid/text/SpannableStringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 112
    aget v2, p1, v0

    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    aget v3, p2, v0

    invoke-virtual {p0, v3}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 118
    :cond_0
    return-object v1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x0

    .line 47
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mPrefs:Landroid/content/SharedPreferences;

    .line 51
    const v6, 0x7f06016d

    invoke-virtual {p0, v6}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 53
    .local v5, "dialogTitle":Ljava/lang/CharSequence;
    const v6, 0x7f06017f

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mGestureNameRes:[I

    iget-object v9, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mActionNameRes:[I

    invoke-direct {p0, v8, v9}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getMappingDescription([I[I)Ljava/lang/CharSequence;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {p0, v6, v7}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 56
    .local v4, "dialogMessage":Ljava/lang/CharSequence;
    const v6, 0x7f060181

    invoke-virtual {p0, v6}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 58
    .local v1, "acceptButtonText":Ljava/lang/CharSequence;
    new-instance v0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$1;-><init>(Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;)V

    .line 67
    .local v0, "acceptButtonListener":Landroid/content/DialogInterface$OnClickListener;
    const v6, 0x7f060182

    invoke-virtual {p0, v6}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 69
    .local v3, "customizeButtonText":Ljava/lang/CharSequence;
    new-instance v2, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;

    invoke-direct {v2, p0}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;-><init>(Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;)V

    .line 83
    .local v2, "customizeButtonListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v1, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v3, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mAlert:Landroid/app/AlertDialog;

    .line 90
    iget-object v6, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mAlert:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    .line 91
    return-void
.end method
