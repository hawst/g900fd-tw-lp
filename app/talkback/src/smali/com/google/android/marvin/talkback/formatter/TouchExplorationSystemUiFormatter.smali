.class public Lcom/google/android/marvin/talkback/formatter/TouchExplorationSystemUiFormatter;
.super Ljava/lang/Object;
.source "TouchExplorationSystemUiFormatter.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# instance fields
.field private final mLastUtteranceText:Landroid/text/SpannableStringBuilder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationSystemUiFormatter;->mLastUtteranceText:Landroid/text/SpannableStringBuilder;

    return-void
.end method

.method private getRecordText(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v5, 0x0

    .line 69
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 70
    .local v0, "builder":Landroid/text/SpannableStringBuilder;
    invoke-static {p2, v5}, Landroid/support/v4/view/accessibility/AccessibilityEventCompat;->getRecord(Landroid/view/accessibility/AccessibilityEvent;I)Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getText()Ljava/util/List;

    move-result-object v1

    .line 73
    .local v1, "entries":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    invoke-static {v1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 75
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 76
    .local v2, "entry":Ljava/lang/CharSequence;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/CharSequence;

    aput-object v2, v4, v5

    invoke-static {v0, v4}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    .line 79
    .end local v2    # "entry":Ljava/lang/CharSequence;
    :cond_0
    return-object v0
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-direct {p0, p2, p1}, Lcom/google/android/marvin/talkback/formatter/TouchExplorationSystemUiFormatter;->getRecordText(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 49
    .local v0, "recordText":Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v1

    .line 54
    :cond_1
    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationSystemUiFormatter;->mLastUtteranceText:Landroid/text/SpannableStringBuilder;

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 58
    invoke-virtual {p3, v0}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 59
    const v1, 0x7f0a0018

    invoke-virtual {p3, v1}, Lcom/google/android/marvin/talkback/Utterance;->addHaptic(I)V

    .line 60
    const v1, 0x7f050004

    invoke-virtual {p3, v1}, Lcom/google/android/marvin/talkback/Utterance;->addAuditory(I)V

    .line 62
    iget-object v1, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationSystemUiFormatter;->mLastUtteranceText:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 63
    iget-object v1, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationSystemUiFormatter;->mLastUtteranceText:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 65
    const/4 v1, 0x1

    goto :goto_0
.end method
