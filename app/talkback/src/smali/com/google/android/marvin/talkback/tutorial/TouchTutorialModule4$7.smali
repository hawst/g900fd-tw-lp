.class Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7;
.super Landroid/view/View$AccessibilityDelegate;
.source "TouchTutorialModule4.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 3
    .param p1, "host"    # Landroid/view/ViewGroup;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v2, 0x0

    .line 194
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->access$000(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)Landroid/widget/TextView;

    move-result-object v0

    if-ne p2, v0, :cond_0

    .line 195
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_1

    .line 197
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    # operator++ for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mNavigationCount:I
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->access$1108(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)I

    .line 199
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mNavigationCount:I
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->access$1100(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    .line 200
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    # invokes: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->removeAllGranularityListeners()V
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->access$600(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V

    .line 201
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->access$100(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 202
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7$1;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7;)V

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->installTriggerDelayedWithFeedback(Ljava/lang/Runnable;)V

    .line 221
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/view/View$AccessibilityDelegate;->onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0

    .line 209
    :cond_1
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_0

    .line 211
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    # invokes: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->removeAllGranularityListeners()V
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->access$600(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->access$100(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 213
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7$2;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7$2;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7;)V

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->installTriggerDelayed(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
