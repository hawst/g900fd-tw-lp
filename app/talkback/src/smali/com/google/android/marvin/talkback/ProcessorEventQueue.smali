.class public Lcom/google/android/marvin/talkback/ProcessorEventQueue;
.super Ljava/lang/Object;
.source "ProcessorEventQueue.java"

# interfaces
.implements Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/ProcessorEventQueue$ProcessorEventHandler;
    }
.end annotation


# instance fields
.field private final mEventQueue:Lcom/google/android/marvin/talkback/EventQueue;

.field private mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

.field private final mHandler:Lcom/google/android/marvin/talkback/ProcessorEventQueue$ProcessorEventHandler;

.field private mLastEventType:I

.field private mLastWindowStateChanged:J

.field private final mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

.field private mTestingListener:Lcom/google/android/marvin/talkback/test/TalkBackListener;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 2
    .param p1, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Lcom/google/android/marvin/talkback/ProcessorEventQueue$ProcessorEventHandler;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/ProcessorEventQueue$ProcessorEventHandler;-><init>(Lcom/google/android/marvin/talkback/ProcessorEventQueue;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mHandler:Lcom/google/android/marvin/talkback/ProcessorEventQueue$ProcessorEventHandler;

    .line 51
    new-instance v0, Lcom/google/android/marvin/talkback/EventQueue;

    invoke-direct {v0}, Lcom/google/android/marvin/talkback/EventQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventQueue:Lcom/google/android/marvin/talkback/EventQueue;

    .line 68
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mLastWindowStateChanged:J

    .line 71
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    .line 72
    new-instance v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    invoke-direct {v0, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    .line 74
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->loadDefaultRules()V

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/ProcessorEventQueue;)Lcom/google/android/marvin/talkback/EventQueue;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/ProcessorEventQueue;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventQueue:Lcom/google/android/marvin/talkback/EventQueue;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/ProcessorEventQueue;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/ProcessorEventQueue;
    .param p1, "x1"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->processAndRecycleEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method private computeQueuingMode(Lcom/google/android/marvin/talkback/Utterance;Landroid/view/accessibility/AccessibilityEvent;)I
    .locals 8
    .param p1, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v3, 0x0

    .line 186
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/Utterance;->getMetadata()Landroid/os/Bundle;

    move-result-object v1

    .line 187
    .local v1, "metadata":Landroid/os/Bundle;
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    .line 192
    .local v0, "eventType":I
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v4

    and-int/lit16 v4, v4, 0x100c

    if-eqz v4, :cond_0

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mLastWindowStateChanged:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x64

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    .line 195
    const/4 v2, 0x1

    .line 209
    :goto_0
    return v2

    .line 198
    :cond_0
    const-string v4, "queuing"

    invoke-virtual {v1, v4, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 202
    .local v2, "queueMode":I
    iget v4, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mLastEventType:I

    if-ne v4, v0, :cond_1

    const/4 v4, 0x2

    if-eq v2, v4, :cond_1

    move v2, v3

    .line 204
    goto :goto_0

    .line 207
    :cond_1
    iput v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mLastEventType:I

    goto :goto_0
.end method

.method private loadDefaultRules()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f050018

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    .line 102
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f05001b

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    .line 105
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 106
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f05001f

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    .line 121
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f050017

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    .line 122
    return-void

    .line 107
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_2

    .line 108
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f05001e

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    goto :goto_0

    .line 109
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_3

    .line 110
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f05001d

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    goto :goto_0

    .line 111
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_4

    .line 112
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f05001c

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    goto :goto_0

    .line 113
    :cond_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_5

    .line 114
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f05001a

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    goto :goto_0

    .line 115
    :cond_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f050019

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    goto :goto_0
.end method

.method private processAndRecycleEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v4, 0x0

    .line 133
    const/4 v1, 0x3

    const-string v2, "Processing event: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 135
    new-instance v0, Lcom/google/android/marvin/talkback/Utterance;

    invoke-direct {v0}, Lcom/google/android/marvin/talkback/Utterance;-><init>()V

    .line 137
    .local v0, "utterance":Lcom/google/android/marvin/talkback/Utterance;
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->processEvent(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/Utterance;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 138
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mTestingListener:Lcom/google/android/marvin/talkback/test/TalkBackListener;

    if-eqz v1, :cond_0

    .line 139
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mTestingListener:Lcom/google/android/marvin/talkback/test/TalkBackListener;

    invoke-interface {v1, v0}, Lcom/google/android/marvin/talkback/test/TalkBackListener;->onUtteranceQueued(Lcom/google/android/marvin/talkback/Utterance;)V

    .line 142
    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->computeQueuingMode(Lcom/google/android/marvin/talkback/Utterance;Landroid/view/accessibility/AccessibilityEvent;)I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->provideFeedbackForUtterance(ILcom/google/android/marvin/talkback/Utterance;)V

    .line 148
    :goto_0
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    .line 149
    return-void

    .line 145
    :cond_1
    const/4 v1, 0x5

    const-string v2, "Failed to process event"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private provideFeedbackForUtterance(ILcom/google/android/marvin/talkback/Utterance;)V
    .locals 11
    .param p1, "queueMode"    # I
    .param p2, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 158
    invoke-virtual {p2}, Lcom/google/android/marvin/talkback/Utterance;->getMetadata()Landroid/os/Bundle;

    move-result-object v10

    .line 159
    .local v10, "metadata":Landroid/os/Bundle;
    const-string v0, "earcon_rate"

    invoke-virtual {v10, v0, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v8

    .line 160
    .local v8, "earconRate":F
    const-string v0, "earcon_volume"

    invoke-virtual {v10, v0, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v9

    .line 161
    .local v9, "earconVolume":F
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 162
    .local v7, "nonSpeechMetadata":Landroid/os/Bundle;
    const-string v0, "earcon_rate"

    invoke-virtual {v7, v0, v8}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 163
    const-string v0, "earcon_volume"

    invoke-virtual {v7, v0, v9}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 166
    invoke-virtual {p2}, Lcom/google/android/marvin/talkback/Utterance;->getSpoken()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->getAggregateText(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 167
    .local v1, "textToSpeak":Ljava/lang/CharSequence;
    const-string v0, "speech_flags"

    const/4 v2, 0x0

    invoke-virtual {v10, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 168
    .local v5, "flags":I
    const-string v0, "speech_params"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v6

    .line 170
    .local v6, "speechMetadata":Landroid/os/Bundle;
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {p2}, Lcom/google/android/marvin/talkback/Utterance;->getAuditory()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/marvin/talkback/Utterance;->getHaptic()Ljava/util/Set;

    move-result-object v3

    move v4, p1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;Ljava/util/Set;Ljava/util/Set;IILandroid/os/Bundle;Landroid/os/Bundle;)V

    .line 172
    return-void
.end method


# virtual methods
.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 83
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    .line 85
    .local v0, "eventType":I
    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    .line 86
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mLastWindowStateChanged:J

    .line 89
    :cond_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventQueue:Lcom/google/android/marvin/talkback/EventQueue;

    monitor-enter v2

    .line 90
    :try_start_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventQueue:Lcom/google/android/marvin/talkback/EventQueue;

    invoke-virtual {v1, p1}, Lcom/google/android/marvin/talkback/EventQueue;->enqueue(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 91
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mHandler:Lcom/google/android/marvin/talkback/ProcessorEventQueue$ProcessorEventHandler;

    invoke-virtual {v1, p1}, Lcom/google/android/marvin/talkback/ProcessorEventQueue$ProcessorEventHandler;->postSpeak(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 92
    monitor-exit v2

    .line 93
    return-void

    .line 92
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setTestingListener(Lcom/google/android/marvin/talkback/test/TalkBackListener;)V
    .locals 0
    .param p1, "testingListener"    # Lcom/google/android/marvin/talkback/test/TalkBackListener;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mTestingListener:Lcom/google/android/marvin/talkback/test/TalkBackListener;

    .line 79
    return-void
.end method
