.class Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$2;
.super Ljava/lang/Object;
.source "LabelDialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->showAddLabelDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

.field final synthetic val$editField:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$2;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    iput-object p2, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$2;->val$editField:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 191
    const/4 v1, -0x1

    if-ne p2, v1, :cond_1

    .line 192
    iget-object v1, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$2;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mStartIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->access$000(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "EXTRA_STRING_RESOURCE_NAME"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 194
    .local v0, "resourceName":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$2;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->access$300(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$2;->val$editField:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->addLabel(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    .end local v0    # "resourceName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    const/4 v1, -0x2

    if-ne p2, v1, :cond_0

    .line 197
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0
.end method
