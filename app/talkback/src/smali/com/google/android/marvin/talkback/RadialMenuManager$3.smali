.class Lcom/google/android/marvin/talkback/RadialMenuManager$3;
.super Ljava/lang/Object;
.source "RadialMenuManager.java"

# interfaces
.implements Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/RadialMenuManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/RadialMenuManager;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHide(Lcom/googlecode/eyesfree/widget/SimpleOverlay;)V
    .locals 3
    .param p1, "overlay"    # Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    .prologue
    .line 238
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$100(Lcom/google/android/marvin/talkback/RadialMenuManager;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mRadialMenuHint:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$000(Lcom/google/android/marvin/talkback/RadialMenuManager;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 240
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mHintSpeechPending:Z
    invoke-static {v1}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$800(Lcom/google/android/marvin/talkback/RadialMenuManager;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 241
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$500(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/SpeechController;->interrupt()V

    .line 244
    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # operator-- for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mIsRadialMenuShowing:I
    invoke-static {v1}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$710(Lcom/google/android/marvin/talkback/RadialMenuManager;)I

    .line 247
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.marvin.talkback.tutorial.ContextMenuHiddenAction"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 248
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.google.android.marvin.talkback.tutorial.MenuIdExtra"

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 249
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mService:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$400(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 250
    return-void
.end method

.method public onShow(Lcom/googlecode/eyesfree/widget/SimpleOverlay;)V
    .locals 6
    .param p1, "overlay"    # Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    .prologue
    .line 220
    move-object v2, p1

    check-cast v2, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->getMenu()Lcom/googlecode/eyesfree/widget/RadialMenu;

    move-result-object v1

    .line 222
    .local v1, "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$100(Lcom/google/android/marvin/talkback/RadialMenuManager;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mRadialMenuHint:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$000(Lcom/google/android/marvin/talkback/RadialMenuManager;)Ljava/lang/Runnable;

    move-result-object v3

    const-wide/16 v4, 0x7d0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 226
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # invokes: Lcom/google/android/marvin/talkback/RadialMenuManager;->playScaleForMenu(Landroid/view/Menu;)V
    invoke-static {v2, v1}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$600(Lcom/google/android/marvin/talkback/RadialMenuManager;Landroid/view/Menu;)V

    .line 228
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # operator++ for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mIsRadialMenuShowing:I
    invoke-static {v2}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$708(Lcom/google/android/marvin/talkback/RadialMenuManager;)I

    .line 231
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.marvin.talkback.tutorial.ContextMenuShownAction"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 232
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.google.android.marvin.talkback.tutorial.MenuIdExtra"

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->getId()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 233
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mService:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$400(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 234
    return-void
.end method
