.class public Lcom/google/android/marvin/talkback/ShakeDetector;
.super Ljava/lang/Object;
.source "ShakeDetector.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field private final mAccelerometer:Landroid/hardware/Sensor;

.field private final mContext:Lcom/google/android/marvin/talkback/TalkBackService;

.field private final mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

.field private mIsActive:Z

.field private mIsFeatureEnabled:Z

.field private mLastEventValues:[F

.field private mLastSensorUpdate:J

.field private final mPrefs:Landroid/content/SharedPreferences;

.field private final mSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 2
    .param p1, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 46
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mPrefs:Landroid/content/SharedPreferences;

    .line 47
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getFullScreenReadController()Lcom/google/android/marvin/talkback/FullScreenReadController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    .line 48
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mSensorManager:Landroid/hardware/SensorManager;

    .line 49
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mAccelerometer:Landroid/hardware/Sensor;

    .line 50
    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 121
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 13
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 99
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 100
    .local v6, "time":J
    iget-wide v8, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mLastSensorUpdate:J

    sub-long v0, v6, v8

    .line 102
    .local v0, "deltaT":J
    long-to-float v5, v0

    const/high16 v8, 0x43480000    # 200.0f

    cmpl-float v5, v5, v8

    if-lez v5, :cond_0

    .line 103
    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v5, v5, v10

    iget-object v8, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v8, v8, v11

    add-float/2addr v5, v8

    iget-object v8, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v8, v8, v12

    add-float/2addr v5, v8

    iget-object v8, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mLastEventValues:[F

    aget v8, v8, v10

    sub-float/2addr v5, v8

    iget-object v8, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mLastEventValues:[F

    aget v8, v8, v11

    sub-float/2addr v5, v8

    iget-object v8, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mLastEventValues:[F

    aget v8, v8, v12

    sub-float/2addr v5, v8

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 105
    .local v2, "movement":F
    long-to-float v5, v0

    div-float v5, v2, v5

    const v8, 0x461c4000    # 10000.0f

    mul-float v3, v5, v8

    .line 106
    .local v3, "speed":F
    iput-wide v6, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mLastSensorUpdate:J

    .line 107
    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v5}, [F->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [F

    iput-object v5, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mLastEventValues:[F

    .line 109
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v8, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v8}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f06003a

    const v10, 0x7f06006f

    invoke-static {v5, v8, v9, v10}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getIntFromStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)I

    move-result v4

    .line 112
    .local v4, "threshold":I
    if-lez v4, :cond_0

    int-to-float v5, v4

    cmpl-float v5, v3, v5

    if-ltz v5, :cond_0

    .line 113
    iget-object v5, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-virtual {v5}, Lcom/google/android/marvin/talkback/FullScreenReadController;->startReadingFromNextNode()V

    .line 116
    .end local v2    # "movement":F
    .end local v3    # "speed":F
    .end local v4    # "threshold":I
    :cond_0
    return-void
.end method

.method public pausePolling()V
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mIsActive:Z

    if-nez v0, :cond_0

    .line 95
    :goto_0
    return-void

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mIsActive:Z

    goto :goto_0
.end method

.method public resumePolling()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 75
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mIsFeatureEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mIsActive:Z

    if-eqz v0, :cond_1

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mLastSensorUpdate:J

    .line 80
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mLastEventValues:[F

    .line 81
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mAccelerometer:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mIsActive:Z

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mIsFeatureEnabled:Z

    .line 59
    if-eqz p1, :cond_1

    .line 60
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 61
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/ShakeDetector;->resumePolling()V

    .line 67
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/ShakeDetector;->pausePolling()V

    goto :goto_0
.end method
