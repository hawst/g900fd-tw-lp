.class Lcom/google/android/marvin/talkback/ProcessorGestureVibrator$1;
.super Ljava/lang/Object;
.source "ProcessorGestureVibrator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator$1;->this$0:Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator$1;->this$0:Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;

    # getter for: Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->access$000(Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;)Lcom/google/android/marvin/talkback/CachedFeedbackController;

    move-result-object v0

    const v1, 0x7f0a001f

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playHaptic(II)Z

    .line 69
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator$1;->this$0:Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;

    # getter for: Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->access$000(Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;)Lcom/google/android/marvin/talkback/CachedFeedbackController;

    move-result-object v0

    const v1, 0x7f050006

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(IFFF)Z

    .line 70
    return-void
.end method
