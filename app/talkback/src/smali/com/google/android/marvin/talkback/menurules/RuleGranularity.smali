.class public Lcom/google/android/marvin/talkback/menurules/RuleGranularity;
.super Ljava/lang/Object;
.source "RuleGranularity.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/menurules/NodeMenuRule;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    return-void
.end method


# virtual methods
.method public accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 47
    invoke-static {p1, p2}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->getSupportedGranularities(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canCollapseMenu()Z
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x1

    return v0
.end method

.method public getMenuItemsForNode(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/List;
    .locals 17
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/marvin/talkback/TalkBackService;",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v11

    .line 54
    .local v11, "cursorController":Lcom/google/android/marvin/talkback/CursorController;
    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Lcom/google/android/marvin/talkback/CursorController;->getGranularityAt(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lcom/google/android/marvin/talkback/CursorGranularity;

    move-result-object v10

    .line 55
    .local v10, "current":Lcom/google/android/marvin/talkback/CursorGranularity;
    new-instance v16, Ljava/util/LinkedList;

    invoke-direct/range {v16 .. v16}, Ljava/util/LinkedList;-><init>()V

    .line 56
    .local v16, "items":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/widget/RadialMenuItem;>;"
    invoke-static/range {p1 .. p2}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->getSupportedGranularities(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/List;

    move-result-object v12

    .line 58
    .local v12, "granularities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/marvin/talkback/CursorGranularity;>;"
    invoke-static/range {p1 .. p2}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasNavigableWebContent(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v14

    .line 61
    .local v14, "hasWebContent":Z
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 92
    :cond_0
    :goto_0
    return-object v16

    .line 65
    :cond_1
    new-instance v9, Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v9, v0, v1, v14}, Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)V

    .line 68
    .local v9, "clickListener":Lcom/google/android/marvin/talkback/menurules/RuleGranularity$GranularityMenuItemClickListener;
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/marvin/talkback/CursorGranularity;

    .line 69
    .local v13, "granularity":Lcom/google/android/marvin/talkback/CursorGranularity;
    new-instance v2, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v4, 0x0

    iget v5, v13, Lcom/google/android/marvin/talkback/CursorGranularity;->resourceId:I

    const/4 v6, 0x0

    iget v7, v13, Lcom/google/android/marvin/talkback/CursorGranularity;->resourceId:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v7}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 71
    .local v2, "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-virtual {v2, v9}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 72
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    .line 73
    invoke-virtual {v13, v10}, Lcom/google/android/marvin/talkback/CursorGranularity;->equals(Ljava/lang/Object;)Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 76
    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 79
    .end local v2    # "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .end local v13    # "granularity":Lcom/google/android/marvin/talkback/CursorGranularity;
    :cond_2
    if-eqz v14, :cond_0

    .line 84
    new-instance v3, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v5, 0x0

    const v6, 0x7f0d0025

    const/4 v7, 0x0

    const v4, 0x7f060119

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v8}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 87
    .local v3, "specialContent":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-virtual {v3, v9}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 88
    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getUserFriendlyMenuName(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 97
    const v0, 0x7f060141

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
