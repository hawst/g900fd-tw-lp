.class public Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;
.super Landroid/content/BroadcastReceiver;
.source "ContextMenuMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;
    }
.end annotation


# static fields
.field public static final FILTER:Landroid/content/IntentFilter;


# instance fields
.field private mListener:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->FILTER:Landroid/content/IntentFilter;

    .line 47
    sget-object v0, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->FILTER:Landroid/content/IntentFilter;

    const-string v1, "com.google.android.marvin.talkback.tutorial.ContextMenuShownAction"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 48
    sget-object v0, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->FILTER:Landroid/content/IntentFilter;

    const-string v1, "com.google.android.marvin.talkback.tutorial.ContextMenuHiddenAction"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 49
    sget-object v0, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->FILTER:Landroid/content/IntentFilter;

    const-string v1, "com.google.android.marvin.talkback.tutorial.ContextMenuItemClickedAction"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 98
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/high16 v4, -0x80000000

    .line 66
    iget-object v3, p0, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->mListener:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;

    if-nez v3, :cond_0

    .line 95
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "action":Ljava/lang/String;
    const-string v3, "com.google.android.marvin.talkback.tutorial.ContextMenuShownAction"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 72
    const-string v3, "com.google.android.marvin.talkback.tutorial.MenuIdExtra"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 73
    const-string v3, "com.google.android.marvin.talkback.tutorial.MenuIdExtra"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 74
    .local v2, "menuId":I
    iget-object v3, p0, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->mListener:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;

    invoke-interface {v3, v2}, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;->onShow(I)V

    goto :goto_0

    .line 76
    .end local v2    # "menuId":I
    :cond_1
    new-instance v3, Ljava/security/InvalidParameterException;

    const-string v4, "Intent missing Menu ID extra."

    invoke-direct {v3, v4}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 78
    :cond_2
    const-string v3, "com.google.android.marvin.talkback.tutorial.ContextMenuHiddenAction"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 79
    const-string v3, "com.google.android.marvin.talkback.tutorial.MenuIdExtra"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 80
    const-string v3, "com.google.android.marvin.talkback.tutorial.MenuIdExtra"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 81
    .restart local v2    # "menuId":I
    iget-object v3, p0, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->mListener:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;

    invoke-interface {v3, v2}, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;->onHide(I)V

    goto :goto_0

    .line 83
    .end local v2    # "menuId":I
    :cond_3
    new-instance v3, Ljava/security/InvalidParameterException;

    const-string v4, "Intent missing Menu ID extra."

    invoke-direct {v3, v4}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 85
    :cond_4
    const-string v3, "com.google.android.marvin.talkback.tutorial.ContextMenuItemClickedAction"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 86
    const-string v3, "com.google.android.marvin.talkback.tutorial.ItemIdExtra"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 87
    const-string v3, "com.google.android.marvin.talkback.tutorial.ItemIdExtra"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 88
    .local v1, "itemId":I
    iget-object v3, p0, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->mListener:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;

    invoke-interface {v3, v1}, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;->onItemClick(I)V

    goto :goto_0

    .line 90
    .end local v1    # "itemId":I
    :cond_5
    new-instance v3, Ljava/security/InvalidParameterException;

    const-string v4, "Intent missing Item ID extra."

    invoke-direct {v3, v4}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 93
    :cond_6
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Unknown action passed the BroadcastReceiver filter."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public setListener(Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->mListener:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;

    .line 62
    return-void
.end method
