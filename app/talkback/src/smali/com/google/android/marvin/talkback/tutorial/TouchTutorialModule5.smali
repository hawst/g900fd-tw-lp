.class Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;
.super Lcom/google/android/marvin/talkback/tutorial/TutorialModule;
.source "TouchTutorialModule5.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field private final mCustomTextTypedWatcher:Landroid/text/TextWatcher;

.field private mEditText:Landroid/widget/EditText;

.field private final mEditTextClickDelegate:Landroid/view/View$AccessibilityDelegate;

.field private final mEditTextFocusedDelegate:Landroid/view/View$AccessibilityDelegate;

.field private final mEditTextOnTouchListener:Landroid/view/View$OnTouchListener;

.field private final mIgnoreTextChangesDelegate:Landroid/view/View$AccessibilityDelegate;

.field private mLayout:Landroid/widget/LinearLayout;

.field private final mMoveCursorToBeginningDelegate:Landroid/view/View$AccessibilityDelegate;

.field private final mMoveCursorToEndDelegate:Landroid/view/View$AccessibilityDelegate;

.field private final mSelectAllDelegate:Landroid/view/View$AccessibilityDelegate;

.field private final mWordGranularityListener:Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V
    .locals 4
    .param p1, "parentTutorial"    # Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 265
    const v0, 0x7f030010

    const v1, 0x7f0601e0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;II)V

    .line 52
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditTextFocusedDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 73
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$2;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$2;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditTextOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 92
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$3;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$3;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditTextClickDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 114
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$4;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$4;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mMoveCursorToBeginningDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 142
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$5;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$5;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mWordGranularityListener:Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;

    .line 162
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$6;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$6;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mMoveCursorToEndDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 189
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$7;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$7;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mSelectAllDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 219
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$8;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$8;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mIgnoreTextChangesDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 235
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$9;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$9;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mCustomTextTypedWatcher:Landroid/text/TextWatcher;

    .line 268
    const v0, 0x7f0d0097

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mLayout:Landroid/widget/LinearLayout;

    .line 269
    const v0, 0x7f0d0098

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditText:Landroid/widget/EditText;

    .line 271
    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->setSkipVisible(Z)V

    .line 272
    invoke-virtual {p0, v3}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->setBackVisible(Z)V

    .line 273
    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->setNextVisible(Z)V

    .line 274
    invoke-virtual {p0, v3}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->setFinishVisible(Z)V

    .line 275
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->onTrigger1()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->onTrigger2()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->onTrigger3()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->onTrigger4()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->onTrigger5()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->onTrigger6()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->onTrigger7()V

    return-void
.end method

.method private clearDelegates()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 419
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 420
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 421
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 422
    return-void
.end method

.method private onTrigger0()V
    .locals 3

    .prologue
    .line 309
    const v0, 0x7f0601e1

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 312
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditTextFocusedDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 313
    return-void
.end method

.method private onTrigger1()V
    .locals 3

    .prologue
    .line 317
    const v0, 0x7f0601e2

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 320
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditTextOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 323
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditTextClickDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 324
    return-void
.end method

.method private onTrigger2()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 328
    sget-object v5, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->LOCAL_BREAKOUT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->getGestureDirectionForRequiredAction(Lcom/google/android/marvin/talkback/ShortcutGestureAction;)I

    move-result v2

    .line 330
    .local v2, "direction":I
    if-gez v2, :cond_0

    .line 345
    :goto_0
    return-void

    .line 334
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 335
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 336
    .local v3, "localContextMenuDirection":Ljava/lang/String;
    const v5, 0x7f060133

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 337
    .local v1, "cursorControl":Ljava/lang/String;
    const v5, 0x7f060134

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 340
    .local v4, "moveToBeginning":Ljava/lang/String;
    const v5, 0x7f0601e3

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    aput-object v1, v6, v8

    const/4 v7, 0x2

    aput-object v4, v6, v7

    invoke-virtual {p0, v5, v8, v6}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 344
    iget-object v5, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mLayout:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mMoveCursorToBeginningDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto :goto_0
.end method

.method private onTrigger3()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 349
    sget-object v5, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->LOCAL_BREAKOUT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->getGestureDirectionForRequiredAction(Lcom/google/android/marvin/talkback/ShortcutGestureAction;)I

    move-result v2

    .line 351
    .local v2, "direction":I
    if-gez v2, :cond_0

    .line 365
    :goto_0
    return-void

    .line 355
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 356
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 357
    .local v3, "localContextMenuDirection":Ljava/lang/String;
    const v5, 0x7f060141

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 358
    .local v0, "changeGranularity":Ljava/lang/String;
    const v5, 0x7f060112

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 360
    .local v4, "wordGranularity":Ljava/lang/String;
    const v5, 0x7f0601e4

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    aput-object v0, v6, v8

    const/4 v7, 0x2

    aput-object v4, v6, v7

    invoke-virtual {p0, v5, v8, v6}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 364
    iget-object v5, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mWordGranularityListener:Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->addGranularityListener(Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;)Z

    goto :goto_0
.end method

.method private onTrigger4()V
    .locals 3

    .prologue
    .line 369
    const v0, 0x7f0601e5

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 372
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mMoveCursorToEndDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 373
    return-void
.end method

.method private onTrigger5()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 377
    sget-object v5, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->LOCAL_BREAKOUT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->getGestureDirectionForRequiredAction(Lcom/google/android/marvin/talkback/ShortcutGestureAction;)I

    move-result v2

    .line 379
    .local v2, "direction":I
    if-gez v2, :cond_0

    .line 393
    :goto_0
    return-void

    .line 383
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 384
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 385
    .local v3, "localContextMenuDirection":Ljava/lang/String;
    const v5, 0x7f060133

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 386
    .local v1, "cursorControl":Ljava/lang/String;
    const v5, 0x7f060139

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 388
    .local v4, "selectAll":Ljava/lang/String;
    const v5, 0x7f0601e6

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    aput-object v1, v6, v8

    const/4 v7, 0x2

    aput-object v4, v6, v7

    invoke-virtual {p0, v5, v8, v6}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 392
    iget-object v5, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mLayout:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mSelectAllDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto :goto_0
.end method

.method private onTrigger6()V
    .locals 4

    .prologue
    .line 398
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mIgnoreTextChangesDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 399
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0601e9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 401
    .local v0, "editableText":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 402
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->selectAll()V

    .line 403
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 405
    const v1, 0x7f0601e7

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 408
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mCustomTextTypedWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 409
    return-void
.end method

.method private onTrigger7()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 414
    const v0, 0x7f0601e8

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0601bb

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v5, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 416
    return-void
.end method


# virtual methods
.method public onPause()V
    .locals 1

    .prologue
    .line 283
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->clearDelegates()V

    .line 284
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mWordGranularityListener:Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->removeGranularityListener(Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;)Z

    .line 285
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 290
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mIgnoreTextChangesDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 291
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0601e9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 293
    .local v0, "editableText":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->clearComposingText()V

    .line 294
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 295
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    .line 296
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 304
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->onTrigger0()V

    .line 305
    return-void
.end method
