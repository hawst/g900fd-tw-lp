.class Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$4;
.super Ljava/lang/Object;
.source "TouchTutorialModule4.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$4;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHide(I)V
    .locals 2
    .param p1, "menuId"    # I

    .prologue
    .line 122
    const v0, 0x7f0f0001

    if-eq p1, v0, :cond_0

    .line 135
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$4;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    # invokes: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->removeAllGranularityListeners()V
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->access$600(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V

    .line 127
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$4;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mContextMenuMonitor:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->access$700(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->setListener(Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$4;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$4$1;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$4$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$4;)V

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->installTriggerDelayed(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onItemClick(I)V
    .locals 0
    .param p1, "itemId"    # I

    .prologue
    .line 140
    return-void
.end method

.method public onShow(I)V
    .locals 0
    .param p1, "menuId"    # I

    .prologue
    .line 118
    return-void
.end method
