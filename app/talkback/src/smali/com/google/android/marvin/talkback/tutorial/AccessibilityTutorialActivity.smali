.class public Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;
.super Landroid/app/Activity;
.source "AccessibilityTutorialActivity.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$RepeatHandler;
    }
.end annotation


# static fields
.field private static sAllowContextMenus:Z

.field private static sTutorialIsActive:Z


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private final mFinishActivityOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field private final mFinishActivityOnClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mFirstTimeResume:Z

.field private final mInAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mOrientationLocked:Z

.field private mRepeatHandler:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$RepeatHandler;

.field private mRepeatedFormatArgs:[Ljava/lang/Object;

.field private mResourceIdToRepeat:I

.field private mSavedInstanceState:Landroid/os/Bundle;

.field private final mServiceStateListener:Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;

.field private final mUtteranceCompleteRunnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

.field private mViewAnimator:Landroid/widget/ViewAnimator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->sTutorialIsActive:Z

    .line 70
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->sAllowContextMenus:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 82
    iput v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mResourceIdToRepeat:I

    .line 84
    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mOrientationLocked:Z

    .line 87
    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mFirstTimeResume:Z

    .line 89
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mFinishActivityOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 96
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$2;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mFinishActivityOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 490
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$3;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mInAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 509
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$4;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mUtteranceCompleteRunnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .line 517
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$5;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$5;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mServiceStateListener:Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;

    .line 534
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;
    .param p1, "x1"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->onUtteranceComplete(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->repeatInstruction()V

    return-void
.end method

.method private calculateCurrentScreenOrientation()I
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 325
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 326
    .local v0, "displayRotation":I
    const/16 v4, 0xb4

    if-lt v0, v4, :cond_1

    move v1, v2

    .line 327
    .local v1, "isReversed":Z
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 328
    if-eqz v1, :cond_0

    const/16 v3, 0x8

    .line 341
    :cond_0
    :goto_1
    return v3

    .end local v1    # "isReversed":Z
    :cond_1
    move v1, v3

    .line 326
    goto :goto_0

    .line 331
    .restart local v1    # "isReversed":Z
    :cond_2
    const/16 v4, 0x5a

    if-eq v0, v4, :cond_3

    const/16 v4, 0x10e

    if-ne v0, v4, :cond_4

    .line 338
    :cond_3
    if-nez v1, :cond_6

    move v1, v2

    .line 341
    :cond_4
    :goto_2
    if-eqz v1, :cond_5

    const/16 v2, 0x9

    :cond_5
    move v3, v2

    goto :goto_1

    :cond_6
    move v1, v3

    .line 338
    goto :goto_2
.end method

.method private interrupt()V
    .locals 5

    .prologue
    .line 438
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getInstance()Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    .line 439
    .local v0, "service":Lcom/google/android/marvin/talkback/TalkBackService;
    if-nez v0, :cond_0

    .line 440
    const/4 v2, 0x6

    const-string v3, "Failed to get TalkBackService instance."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 446
    :goto_0
    return-void

    .line 444
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v1

    .line 445
    .local v1, "speechController":Lcom/google/android/marvin/talkback/SpeechController;
    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/SpeechController;->interrupt()V

    goto :goto_0
.end method

.method public static isTutorialActive()Z
    .locals 1

    .prologue
    .line 227
    sget-boolean v0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->sTutorialIsActive:Z

    return v0
.end method

.method private onUtteranceComplete(I)V
    .locals 6
    .param p1, "status"    # I

    .prologue
    .line 454
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->setTouchGuardActive(Z)V

    .line 455
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getInstance()Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    .line 456
    .local v0, "service":Lcom/google/android/marvin/talkback/TalkBackService;
    if-eqz v0, :cond_0

    .line 457
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/CachedFeedbackController;

    move-result-object v1

    const v2, 0x7f050014

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(I)Z

    .line 459
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->unlockOrientation()V

    .line 461
    sget-boolean v1, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->sTutorialIsActive:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mResourceIdToRepeat:I

    if-lez v1, :cond_1

    .line 462
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mRepeatHandler:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$RepeatHandler;

    const/4 v2, 0x1

    const-wide/16 v4, 0x3a98

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$RepeatHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 464
    :cond_1
    return-void
.end method

.method private repeatInstruction()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 396
    sget-boolean v0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->sTutorialIsActive:Z

    if-nez v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mRepeatHandler:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$RepeatHandler;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$RepeatHandler;->removeMessages(I)V

    .line 405
    :goto_0
    return-void

    .line 401
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->lockOrientation()V

    .line 402
    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->setTouchGuardActive(Z)V

    .line 404
    iget v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mResourceIdToRepeat:I

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mRepeatedFormatArgs:[Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->speakInternal(I[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected static setAllowContextMenus(Z)V
    .locals 0
    .param p0, "allowed"    # Z

    .prologue
    .line 235
    sput-boolean p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->sAllowContextMenus:Z

    .line 236
    return-void
.end method

.method public static shouldAllowContextMenus()Z
    .locals 1

    .prologue
    .line 231
    sget-boolean v0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->sAllowContextMenus:Z

    return v0
.end method

.method private show(I)V
    .locals 3
    .param p1, "which"    # I

    .prologue
    .line 256
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mViewAnimator:Landroid/widget/ViewAnimator;

    invoke-virtual {v0}, Landroid/widget/ViewAnimator;->getChildCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 257
    :cond_0
    const/4 v0, 0x5

    const-string v1, "Tried to show a module with an index out of bounds."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 272
    :goto_0
    return-void

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mViewAnimator:Landroid/widget/ViewAnimator;

    invoke-virtual {v0}, Landroid/widget/ViewAnimator;->getDisplayedChild()I

    move-result v0

    if-eq p1, v0, :cond_2

    .line 263
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->interrupt()V

    .line 264
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->interrupt()V

    .line 265
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->stopRepeating()V

    .line 266
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mViewAnimator:Landroid/widget/ViewAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ViewAnimator;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 267
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getCurrentModule()Lcom/google/android/marvin/talkback/tutorial/TutorialModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->onPause()V

    .line 268
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getCurrentModule()Lcom/google/android/marvin/talkback/tutorial/TutorialModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->onStop()V

    .line 271
    :cond_2
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mViewAnimator:Landroid/widget/ViewAnimator;

    invoke-virtual {v0, p1}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    goto :goto_0
.end method

.method private varargs speakInternal(I[Ljava/lang/Object;)V
    .locals 10
    .param p1, "resId"    # I
    .param p2, "formatArgs"    # [Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 425
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getInstance()Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v9

    .line 426
    .local v9, "service":Lcom/google/android/marvin/talkback/TalkBackService;
    if-nez v9, :cond_0

    .line 427
    const/4 v2, 0x6

    const-string v3, "Failed to get TalkBackService instance."

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 434
    :goto_0
    return-void

    .line 431
    :cond_0
    invoke-virtual {v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    .line 432
    .local v0, "speechController":Lcom/google/android/marvin/talkback/SpeechController;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 433
    .local v1, "text":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mUtteranceCompleteRunnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    move-object v3, v2

    move v5, v4

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;Ljava/util/Set;Ljava/util/Set;IILandroid/os/Bundle;Landroid/os/Bundle;Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    goto :goto_0
.end method


# virtual methods
.method public getCurrentModule()Lcom/google/android/marvin/talkback/tutorial/TutorialModule;
    .locals 3

    .prologue
    .line 239
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mViewAnimator:Landroid/widget/ViewAnimator;

    invoke-virtual {v1}, Landroid/widget/ViewAnimator;->getCurrentView()Landroid/view/View;

    move-result-object v0

    .line 240
    .local v0, "currentView":Landroid/view/View;
    instance-of v1, v0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;

    if-nez v1, :cond_0

    .line 241
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Current view is not a valid TutorialModule."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 244
    :cond_0
    check-cast v0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;

    .end local v0    # "currentView":Landroid/view/View;
    return-object v0
.end method

.method protected getFullScreenReadController()Lcom/google/android/marvin/talkback/FullScreenReadController;
    .locals 4

    .prologue
    .line 357
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getInstance()Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    .line 358
    .local v0, "service":Lcom/google/android/marvin/talkback/TalkBackService;
    if-nez v0, :cond_0

    .line 359
    const/4 v1, 0x6

    const-string v2, "Failed to get TalkBackService instance."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 360
    const/4 v1, 0x0

    .line 363
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getFullScreenReadController()Lcom/google/android/marvin/talkback/FullScreenReadController;

    move-result-object v1

    goto :goto_0
.end method

.method protected getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;
    .locals 4

    .prologue
    .line 347
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getInstance()Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    .line 348
    .local v0, "service":Lcom/google/android/marvin/talkback/TalkBackService;
    if-nez v0, :cond_0

    .line 349
    const/4 v1, 0x6

    const-string v2, "Failed to get TalkBackService instance."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 350
    const/4 v1, 0x0

    .line 353
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v1

    goto :goto_0
.end method

.method public lockOrientation()V
    .locals 2

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mOrientationLocked:Z

    if-eqz v0, :cond_0

    .line 312
    :goto_0
    return-void

    .line 306
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mOrientationLocked:Z

    .line 307
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    .line 308
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 310
    :cond_1
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->calculateCurrentScreenOrientation()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method protected next()V
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mViewAnimator:Landroid/widget/ViewAnimator;

    invoke-virtual {v0}, Landroid/widget/ViewAnimator;->getDisplayedChild()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->show(I)V

    .line 249
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 487
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 488
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v7, 0x10a0002

    const/16 v5, 0x400

    const/4 v6, 0x1

    .line 106
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 108
    invoke-virtual {p0, v6}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->requestWindowFeature(I)Z

    .line 109
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v5, v5}, Landroid/view/Window;->setFlags(II)V

    .line 112
    iput-object p1, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mSavedInstanceState:Landroid/os/Bundle;

    .line 114
    invoke-static {p0, v7}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 116
    .local v0, "inAnimation":Landroid/view/animation/Animation;
    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mInAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 118
    invoke-static {p0, v7}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 121
    .local v1, "outAnimation":Landroid/view/animation/Animation;
    new-instance v4, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$RepeatHandler;

    invoke-direct {v4, p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$RepeatHandler;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V

    iput-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mRepeatHandler:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$RepeatHandler;

    .line 122
    new-instance v4, Landroid/widget/ViewAnimator;

    invoke-direct {v4, p0}, Landroid/widget/ViewAnimator;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mViewAnimator:Landroid/widget/ViewAnimator;

    .line 123
    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mViewAnimator:Landroid/widget/ViewAnimator;

    invoke-virtual {v4, v0}, Landroid/widget/ViewAnimator;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 124
    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mViewAnimator:Landroid/widget/ViewAnimator;

    invoke-virtual {v4, v1}, Landroid/widget/ViewAnimator;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 125
    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mViewAnimator:Landroid/widget/ViewAnimator;

    new-instance v5, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/ViewAnimator;->addView(Landroid/view/View;)V

    .line 126
    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mViewAnimator:Landroid/widget/ViewAnimator;

    new-instance v5, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/ViewAnimator;->addView(Landroid/view/View;)V

    .line 127
    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mViewAnimator:Landroid/widget/ViewAnimator;

    new-instance v5, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/ViewAnimator;->addView(Landroid/view/View;)V

    .line 128
    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mViewAnimator:Landroid/widget/ViewAnimator;

    new-instance v5, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/ViewAnimator;->addView(Landroid/view/View;)V

    .line 131
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v4, v5, :cond_0

    .line 132
    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mViewAnimator:Landroid/widget/ViewAnimator;

    new-instance v5, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/ViewAnimator;->addView(Landroid/view/View;)V

    .line 136
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    .line 137
    .local v3, "window":Landroid/view/Window;
    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 138
    .local v2, "params":Landroid/view/WindowManager$LayoutParams;
    const/4 v4, 0x5

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    .line 139
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v4, v4, 0x80

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 140
    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 142
    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mViewAnimator:Landroid/widget/ViewAnimator;

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->setContentView(Landroid/view/View;)V

    .line 144
    const-string v4, "accessibility"

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/accessibility/AccessibilityManager;

    iput-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 146
    iput-boolean v6, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mFirstTimeResume:Z

    .line 147
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 151
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 152
    const/4 v1, 0x0

    sput-boolean v1, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->sTutorialIsActive:Z

    .line 154
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getInstance()Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    .line 155
    .local v0, "service":Lcom/google/android/marvin/talkback/TalkBackService;
    if-eqz v0, :cond_0

    .line 156
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mServiceStateListener:Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->removeServiceStateListener(Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;)V

    .line 159
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getCurrentModule()Lcom/google/android/marvin/talkback/tutorial/TutorialModule;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->onPause()V

    .line 161
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->interrupt()V

    .line 165
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mRepeatHandler:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$RepeatHandler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$RepeatHandler;->removeMessages(I)V

    .line 167
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->unlockOrientation()V

    .line 168
    return-void
.end method

.method protected onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 172
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 173
    sput-boolean v6, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->sTutorialIsActive:Z

    .line 180
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getServiceState()I

    move-result v1

    .line 185
    .local v1, "serviceState":I
    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 186
    const v2, 0x7f0601ed

    const v3, 0x7f0601ee

    invoke-virtual {p0, v2, v3}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->showAlertDialogAndFinish(II)V

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v2

    if-nez v2, :cond_3

    .line 191
    :cond_2
    const v2, 0x7f0601eb

    const v3, 0x7f0601ec

    invoke-virtual {p0, v2, v3}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->showAlertDialogAndFinish(II)V

    goto :goto_0

    .line 196
    :cond_3
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getInstance()Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    .line 197
    .local v0, "service":Lcom/google/android/marvin/talkback/TalkBackService;
    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mServiceStateListener:Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;

    invoke-virtual {v0, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->addServiceStateListener(Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;)V

    .line 199
    iget-boolean v2, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mFirstTimeResume:Z

    if-eqz v2, :cond_4

    .line 201
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->lockOrientation()V

    .line 203
    iput-boolean v4, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mFirstTimeResume:Z

    .line 205
    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mSavedInstanceState:Landroid/os/Bundle;

    if-eqz v2, :cond_5

    .line 206
    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mSavedInstanceState:Landroid/os/Bundle;

    const-string v3, "active_module"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->show(I)V

    .line 212
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getCurrentModule()Lcom/google/android/marvin/talkback/tutorial/TutorialModule;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->onResume()V

    .line 214
    iget v2, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mResourceIdToRepeat:I

    if-lez v2, :cond_0

    .line 215
    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mRepeatHandler:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$RepeatHandler;

    const-wide/16 v4, 0x5dc

    invoke-virtual {v2, v6, v4, v5}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$RepeatHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 208
    :cond_5
    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->show(I)V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 221
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 223
    const-string v0, "active_module"

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mViewAnimator:Landroid/widget/ViewAnimator;

    invoke-virtual {v1}, Landroid/widget/ViewAnimator;->getDisplayedChild()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 224
    return-void
.end method

.method public playTriggerSound()V
    .locals 3

    .prologue
    .line 289
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getInstance()Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    .line 290
    .local v0, "service":Lcom/google/android/marvin/talkback/TalkBackService;
    if-eqz v0, :cond_0

    .line 291
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/CachedFeedbackController;

    move-result-object v1

    const v2, 0x7f050021

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(I)Z

    .line 293
    :cond_0
    return-void
.end method

.method protected previous()V
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mViewAnimator:Landroid/widget/ViewAnimator;

    invoke-virtual {v0}, Landroid/widget/ViewAnimator;->getDisplayedChild()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->show(I)V

    .line 253
    return-void
.end method

.method public setTouchGuardActive(Z)V
    .locals 3
    .param p1, "active"    # Z

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getCurrentModule()Lcom/google/android/marvin/talkback/tutorial/TutorialModule;

    move-result-object v1

    const v2, 0x7f0d0099

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 277
    .local v0, "touchGuard":Landroid/view/View;
    if-eqz p1, :cond_0

    .line 278
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 282
    :goto_0
    return-void

    .line 280
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected showAlertDialogAndFinish(II)V
    .locals 2
    .param p1, "titleId"    # I
    .param p2, "messageId"    # I

    .prologue
    .line 467
    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->showAlertDialogAndFinish(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    return-void
.end method

.method protected showAlertDialogAndFinish(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 471
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->interrupt()V

    .line 472
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->stopRepeating()V

    .line 474
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mFinishActivityOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0601ea

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mFinishActivityOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 483
    return-void
.end method

.method public varargs speakInstruction(IZ[Ljava/lang/Object;)V
    .locals 1
    .param p1, "resId"    # I
    .param p2, "repeat"    # Z
    .param p3, "formatArgs"    # [Ljava/lang/Object;

    .prologue
    .line 375
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->stopRepeating()V

    .line 377
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->lockOrientation()V

    .line 378
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->setTouchGuardActive(Z)V

    .line 380
    invoke-direct {p0, p1, p3}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->speakInternal(I[Ljava/lang/Object;)V

    .line 382
    if-eqz p2, :cond_0

    .line 383
    iput p1, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mResourceIdToRepeat:I

    .line 384
    iput-object p3, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mRepeatedFormatArgs:[Ljava/lang/Object;

    .line 388
    :goto_0
    return-void

    .line 386
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mResourceIdToRepeat:I

    goto :goto_0
.end method

.method public stopRepeating()V
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mRepeatHandler:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$RepeatHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$RepeatHandler;->removeMessages(I)V

    .line 414
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mResourceIdToRepeat:I

    .line 415
    return-void
.end method

.method public unlockOrientation()V
    .locals 1

    .prologue
    .line 316
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mOrientationLocked:Z

    if-nez v0, :cond_0

    .line 322
    :goto_0
    return-void

    .line 320
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->mOrientationLocked:Z

    .line 321
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->setRequestedOrientation(I)V

    goto :goto_0
.end method
