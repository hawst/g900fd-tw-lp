.class public Lcom/google/android/marvin/talkback/FeedbackFragment;
.super Ljava/lang/Object;
.source "FeedbackFragment.java"


# instance fields
.field private mEarcons:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mHaptics:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNonSpeechParams:Landroid/os/Bundle;

.field private mSpeechParams:Landroid/os/Bundle;

.field private mText:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "speechParams"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 71
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, p2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/marvin/talkback/FeedbackFragment;-><init>(Ljava/lang/CharSequence;Ljava/util/Set;Ljava/util/Set;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 72
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/util/Set;Ljava/util/Set;Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p4, "speechParams"    # Landroid/os/Bundle;
    .param p5, "nonSpeechParams"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/os/Bundle;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 75
    .local p2, "earcons":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local p3, "haptics":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mText:Ljava/lang/CharSequence;

    .line 78
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mEarcons:Ljava/util/Set;

    .line 79
    if-eqz p2, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mEarcons:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 83
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mHaptics:Ljava/util/Set;

    .line 84
    if-eqz p3, :cond_1

    .line 85
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mHaptics:Ljava/util/Set;

    invoke-interface {v0, p3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 88
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mSpeechParams:Landroid/os/Bundle;

    .line 89
    if-eqz p4, :cond_2

    .line 90
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mSpeechParams:Landroid/os/Bundle;

    invoke-virtual {v0, p4}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 93
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mNonSpeechParams:Landroid/os/Bundle;

    .line 94
    if-eqz p5, :cond_3

    .line 95
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mNonSpeechParams:Landroid/os/Bundle;

    invoke-virtual {v0, p5}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 97
    :cond_3
    return-void
.end method


# virtual methods
.method public addEarcon(I)V
    .locals 2
    .param p1, "earconId"    # I

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mEarcons:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 127
    return-void
.end method

.method public addHaptic(I)V
    .locals 2
    .param p1, "hapticId"    # I

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mHaptics:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 150
    return-void
.end method

.method public clearAllEarcons()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mEarcons:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 134
    return-void
.end method

.method public clearAllHaptics()V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mHaptics:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 157
    return-void
.end method

.method public getEarcons()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mEarcons:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getHaptics()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mHaptics:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getNonSpeechParams()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mNonSpeechParams:Landroid/os/Bundle;

    return-object v0
.end method

.method public getSpeechParams()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mSpeechParams:Landroid/os/Bundle;

    return-object v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public setNonSpeechParams(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "nonSpeechParams"    # Landroid/os/Bundle;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mNonSpeechParams:Landroid/os/Bundle;

    .line 189
    return-void
.end method

.method public setSpeechParams(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "speechParams"    # Landroid/os/Bundle;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mSpeechParams:Landroid/os/Bundle;

    .line 173
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mText:Ljava/lang/CharSequence;

    .line 111
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{text:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", earcons:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mEarcons:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", haptics:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mHaptics:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", speechParams:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mSpeechParams:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "nonSpeechParams:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/FeedbackFragment;->mNonSpeechParams:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
