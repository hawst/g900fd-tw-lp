.class Lcom/google/android/marvin/talkback/ProcessorScrollPosition;
.super Ljava/lang/Object;
.source "ProcessorScrollPosition.java"

# interfaces
.implements Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;
    }
.end annotation


# instance fields
.field private final mCachedFromValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

.field private final mHandler:Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;

.field private mRecentlyExplored:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private final mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

.field private final mSpeechParams:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 3
    .param p1, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mCachedFromValues:Ljava/util/HashMap;

    .line 53
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mSpeechParams:Landroid/os/Bundle;

    .line 54
    new-instance v0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;-><init>(Lcom/google/android/marvin/talkback/ProcessorScrollPosition;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mHandler:Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;

    .line 63
    iput-object p1, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mContext:Landroid/content/Context;

    .line 64
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    .line 66
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 67
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getFullScreenReadController()Lcom/google/android/marvin/talkback/FullScreenReadController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    .line 72
    :goto_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mSpeechParams:Landroid/os/Bundle;

    const-string v1, "pitch"

    const v2, 0x3f99999a    # 1.2f

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 73
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mSpeechParams:Landroid/os/Bundle;

    const-string v1, "rate"

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 74
    return-void

    .line 69
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    goto :goto_0
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/ProcessorScrollPosition;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/ProcessorScrollPosition;
    .param p1, "x1"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->handleScrollFeedback(Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/marvin/talkback/ProcessorScrollPosition;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/ProcessorScrollPosition;
    .param p1, "x1"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->handleSeekFeedback(Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method private getDescriptionForScrollEvent(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 11
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 245
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->getEventTextOrDescription(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 246
    .local v2, "text":Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 265
    .end local v2    # "text":Ljava/lang/CharSequence;
    :goto_0
    return-object v2

    .line 251
    .restart local v2    # "text":Ljava/lang/CharSequence;
    :cond_0
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v4

    add-int/lit8 v0, v4, 0x1

    .line 252
    .local v0, "fromIndex":I
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getItemCount()I

    move-result v1

    .line 253
    .local v1, "itemCount":I
    if-ltz v0, :cond_1

    if-gtz v1, :cond_2

    .line 254
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 259
    :cond_2
    invoke-static {p1}, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->getToIndex(Landroid/view/accessibility/AccessibilityEvent;)I

    move-result v4

    add-int/lit8 v3, v4, 0x1

    .line 260
    .local v3, "toIndex":I
    if-eq v0, v3, :cond_3

    if-lez v3, :cond_3

    if-le v3, v1, :cond_4

    .line 261
    :cond_3
    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mContext:Landroid/content/Context;

    const v5, 0x7f0600b8

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 265
    :cond_4
    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mContext:Landroid/content/Context;

    const v5, 0x7f0600b7

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private getDescriptionForSeekEvent(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 9
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 271
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->getEventTextOrDescription(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 272
    .local v3, "text":Ljava/lang/CharSequence;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 285
    .end local v3    # "text":Ljava/lang/CharSequence;
    :goto_0
    return-object v3

    .line 277
    .restart local v3    # "text":Ljava/lang/CharSequence;
    :cond_0
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getCurrentItemIndex()I

    move-result v0

    .line 278
    .local v0, "currentItemIndex":I
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getItemCount()I

    move-result v1

    .line 279
    .local v1, "itemCount":I
    if-ltz v0, :cond_1

    if-le v0, v1, :cond_2

    .line 280
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 284
    :cond_2
    mul-int/lit8 v4, v0, 0x64

    div-int v2, v4, v1

    .line 285
    .local v2, "percentage":I
    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mContext:Landroid/content/Context;

    const v5, 0x7f0600b3

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private handleScrollFeedback(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 191
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->getDescriptionForScrollEvent(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 192
    .local v0, "text":Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 198
    :goto_0
    return-void

    .line 197
    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mSpeechParams:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method private handleSeekFeedback(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 9
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/16 v5, 0xe

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 208
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v3, v5, :cond_1

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v1, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 213
    .local v1, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 214
    .local v0, "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v0, :cond_0

    .line 219
    :try_start_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v3, v4, :cond_2

    .line 221
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isAccessibilityFocused()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_4

    .line 239
    new-array v3, v8, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v7

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 224
    :cond_2
    :try_start_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v3, v5, :cond_4

    .line 226
    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mRecentlyExplored:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mRecentlyExplored:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v3, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-nez v3, :cond_4

    .line 239
    :cond_3
    new-array v3, v8, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v7

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 231
    :cond_4
    :try_start_2
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->getDescriptionForSeekEvent(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 232
    .local v2, "text":Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    if-eqz v3, :cond_5

    .line 239
    new-array v3, v8, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v7

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 237
    :cond_5
    :try_start_3
    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mSpeechParams:Landroid/os/Bundle;

    invoke-virtual {v3, v2, v4, v5, v6}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 239
    new-array v3, v8, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v7

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .end local v2    # "text":Ljava/lang/CharSequence;
    :catchall_0
    move-exception v3

    new-array v4, v8, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v4, v7

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v3
.end method

.method private shouldIgnoreEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 138
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 146
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 142
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 144
    :sswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->shouldIgnoreScrollEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    goto :goto_0

    .line 138
    :sswitch_data_0
    .sparse-switch
        0x800 -> :sswitch_0
        0x1000 -> :sswitch_1
        0x8000 -> :sswitch_0
        0x10000 -> :sswitch_0
    .end sparse-switch
.end method

.method private shouldIgnoreScrollEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 152
    iget-object v6, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-virtual {v6}, Lcom/google/android/marvin/talkback/FullScreenReadController;->isActive()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 182
    :cond_0
    :goto_0
    return v4

    .line 156
    :cond_1
    new-instance v3, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v3, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 157
    .local v3, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    .line 159
    .local v2, "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-nez v2, :cond_2

    .line 160
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0xe

    if-ge v6, v7, :cond_0

    move v4, v5

    .line 165
    goto :goto_0

    .line 169
    :cond_2
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v6

    add-int/lit8 v1, v6, 0x1

    .line 170
    .local v1, "fromIndex":I
    iget-object v6, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mCachedFromValues:Ljava/util/HashMap;

    invoke-virtual {v6, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 172
    .local v0, "cachedFromIndex":Ljava/lang/Integer;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-ne v6, v1, :cond_3

    .line 175
    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    .line 181
    :cond_3
    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mCachedFromValues:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v2, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v4, v5

    .line 182
    goto :goto_0
.end method

.method private updateRecentlyExplored(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 110
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_1

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v2

    const/16 v3, 0x80

    if-ne v2, v3, :cond_0

    .line 120
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v1, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 121
    .local v1, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 123
    .local v0, "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mRecentlyExplored:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v2, :cond_2

    .line 124
    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mRecentlyExplored:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 128
    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mRecentlyExplored:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 129
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mRecentlyExplored:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 132
    :cond_2
    if-eqz v0, :cond_0

    .line 133
    invoke-static {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mRecentlyExplored:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    goto :goto_0
.end method


# virtual methods
.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->updateRecentlyExplored(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 80
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->shouldIgnoreEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mHandler:Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;

    # invokes: Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->cancelSeekFeedback()V
    invoke-static {v0}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->access$000(Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mHandler:Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;

    # invokes: Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->cancelScrollFeedback()V
    invoke-static {v0}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->access$100(Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;)V

    .line 87
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 98
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mContext:Landroid/content/Context;

    const-class v1, Landroid/widget/SeekBar;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->eventMatchesClass(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mHandler:Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;

    # invokes: Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->postSeekFeedback(Landroid/view/accessibility/AccessibilityEvent;)V
    invoke-static {v0, p1}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->access$300(Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0

    .line 90
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mCachedFromValues:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    goto :goto_0

    .line 93
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->mHandler:Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;

    # invokes: Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->postScrollFeedback(Landroid/view/accessibility/AccessibilityEvent;)V
    invoke-static {v0, p1}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->access$200(Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0

    .line 87
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x20 -> :sswitch_1
        0x1000 -> :sswitch_2
    .end sparse-switch
.end method
