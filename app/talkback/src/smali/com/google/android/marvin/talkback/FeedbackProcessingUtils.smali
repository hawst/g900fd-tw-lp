.class public Lcom/google/android/marvin/talkback/FeedbackProcessingUtils;
.super Ljava/lang/Object;
.source "FeedbackProcessingUtils.java"


# direct methods
.method private static addFormattingCharacteristics(Lcom/google/android/marvin/talkback/FeedbackItem;)V
    .locals 15
    .param p0, "item"    # Lcom/google/android/marvin/talkback/FeedbackItem;

    .prologue
    .line 152
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FeedbackItem;->getFragments()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-ge v3, v12, :cond_5

    .line 153
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FeedbackItem;->getFragments()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/talkback/FeedbackFragment;

    .line 154
    .local v1, "fragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/FeedbackFragment;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 155
    .local v2, "fragmentText":Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_0

    instance-of v12, v2, Landroid/text/Spannable;

    if-nez v12, :cond_1

    .line 152
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move-object v8, v2

    .line 159
    check-cast v8, Landroid/text/Spannable;

    .line 160
    .local v8, "spannable":Landroid/text/Spannable;
    const/4 v12, 0x0

    invoke-interface {v8}, Landroid/text/Spannable;->length()I

    move-result v13

    const-class v14, Ljava/lang/Object;

    invoke-interface {v8, v12, v13, v14}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v9

    .line 161
    .local v9, "spans":[Ljava/lang/Object;
    move-object v0, v9

    .local v0, "arr$":[Ljava/lang/Object;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_0

    aget-object v6, v0, v4

    .line 162
    .local v6, "span":Ljava/lang/Object;
    const/4 v7, 0x0

    .line 163
    .local v7, "spanHandled":Z
    instance-of v12, v6, Landroid/text/style/URLSpan;

    if-eqz v12, :cond_4

    move-object v11, v6

    .line 164
    check-cast v11, Landroid/text/style/URLSpan;

    .line 165
    .local v11, "urlSpan":Landroid/text/style/URLSpan;
    invoke-static {p0, v1, v3, v8, v11}, Lcom/google/android/marvin/talkback/FeedbackProcessingUtils;->handleUrlSpan(Lcom/google/android/marvin/talkback/FeedbackItem;Lcom/google/android/marvin/talkback/FeedbackFragment;ILandroid/text/Spannable;Landroid/text/style/URLSpan;)Z

    move-result v7

    .line 171
    .end local v11    # "urlSpan":Landroid/text/style/URLSpan;
    :cond_2
    :goto_2
    if-eqz v7, :cond_3

    .line 174
    invoke-interface {v8, v6}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v12

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    invoke-interface {v8, v12, v13}, Landroid/text/Spannable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v8

    .end local v8    # "spannable":Landroid/text/Spannable;
    check-cast v8, Landroid/text/Spannable;

    .line 179
    .restart local v8    # "spannable":Landroid/text/Spannable;
    add-int/lit8 v3, v3, 0x2

    .line 161
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 166
    :cond_4
    instance-of v12, v6, Landroid/text/style/StyleSpan;

    if-eqz v12, :cond_2

    move-object v10, v6

    .line 167
    check-cast v10, Landroid/text/style/StyleSpan;

    .line 168
    .local v10, "styleSpan":Landroid/text/style/StyleSpan;
    invoke-static {p0, v1, v3, v8, v10}, Lcom/google/android/marvin/talkback/FeedbackProcessingUtils;->handleStyleSpan(Lcom/google/android/marvin/talkback/FeedbackItem;Lcom/google/android/marvin/talkback/FeedbackFragment;ILandroid/text/Spannable;Landroid/text/style/StyleSpan;)Z

    move-result v7

    goto :goto_2

    .line 183
    .end local v0    # "arr$":[Ljava/lang/Object;
    .end local v1    # "fragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    .end local v2    # "fragmentText":Ljava/lang/CharSequence;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "span":Ljava/lang/Object;
    .end local v7    # "spanHandled":Z
    .end local v8    # "spannable":Landroid/text/Spannable;
    .end local v9    # "spans":[Ljava/lang/Object;
    .end local v10    # "styleSpan":Landroid/text/style/StyleSpan;
    :cond_5
    return-void
.end method

.method private static cleanupItemText(Landroid/content/Context;Lcom/google/android/marvin/talkback/FeedbackItem;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "item"    # Lcom/google/android/marvin/talkback/FeedbackItem;

    .prologue
    .line 186
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/FeedbackItem;->getFragments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/marvin/talkback/FeedbackFragment;

    .line 187
    .local v0, "fragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/FeedbackFragment;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 188
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/FeedbackFragment;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->collapseRepeatedCharacters(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 190
    .local v2, "processedText":Ljava/lang/CharSequence;
    invoke-static {p0, v2}, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->cleanUp(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 191
    invoke-virtual {v0, v2}, Lcom/google/android/marvin/talkback/FeedbackFragment;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 194
    .end local v0    # "fragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    .end local v2    # "processedText":Ljava/lang/CharSequence;
    :cond_1
    return-void
.end method

.method private static clearFragmentMetadata(Lcom/google/android/marvin/talkback/FeedbackFragment;)V
    .locals 1
    .param p0, "fragment"    # Lcom/google/android/marvin/talkback/FeedbackFragment;

    .prologue
    .line 327
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/FeedbackFragment;->setSpeechParams(Landroid/os/Bundle;)V

    .line 328
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/FeedbackFragment;->setNonSpeechParams(Landroid/os/Bundle;)V

    .line 329
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FeedbackFragment;->clearAllEarcons()V

    .line 330
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FeedbackFragment;->clearAllHaptics()V

    .line 331
    return-void
.end method

.method private static copyFragmentMetadata(Lcom/google/android/marvin/talkback/FeedbackFragment;Lcom/google/android/marvin/talkback/FeedbackFragment;)V
    .locals 3
    .param p0, "from"    # Lcom/google/android/marvin/talkback/FeedbackFragment;
    .param p1, "to"    # Lcom/google/android/marvin/talkback/FeedbackFragment;

    .prologue
    .line 315
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FeedbackFragment;->getSpeechParams()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/android/marvin/talkback/FeedbackFragment;->setSpeechParams(Landroid/os/Bundle;)V

    .line 316
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FeedbackFragment;->getNonSpeechParams()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/android/marvin/talkback/FeedbackFragment;->setNonSpeechParams(Landroid/os/Bundle;)V

    .line 317
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FeedbackFragment;->getEarcons()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 318
    .local v1, "id":I
    invoke-virtual {p1, v1}, Lcom/google/android/marvin/talkback/FeedbackFragment;->addEarcon(I)V

    goto :goto_0

    .line 321
    .end local v1    # "id":I
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FeedbackFragment;->getHaptics()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 322
    .restart local v1    # "id":I
    invoke-virtual {p1, v1}, Lcom/google/android/marvin/talkback/FeedbackFragment;->addHaptic(I)V

    goto :goto_1

    .line 324
    .end local v1    # "id":I
    :cond_1
    return-void
.end method

.method public static generateFeedbackItemFromInput(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/util/Set;Ljava/util/Set;ILandroid/os/Bundle;Landroid/os/Bundle;)Lcom/google/android/marvin/talkback/FeedbackItem;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p4, "flags"    # I
    .param p5, "speechParams"    # Landroid/os/Bundle;
    .param p6, "nonSpeechParams"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/CharSequence;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;I",
            "Landroid/os/Bundle;",
            "Landroid/os/Bundle;",
            ")",
            "Lcom/google/android/marvin/talkback/FeedbackItem;"
        }
    .end annotation

    .prologue
    .line 76
    .local p2, "earcons":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local p3, "haptics":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    new-instance v6, Lcom/google/android/marvin/talkback/FeedbackItem;

    invoke-direct {v6}, Lcom/google/android/marvin/talkback/FeedbackItem;-><init>()V

    .line 77
    .local v6, "feedbackItem":Lcom/google/android/marvin/talkback/FeedbackItem;
    new-instance v0, Lcom/google/android/marvin/talkback/FeedbackFragment;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/marvin/talkback/FeedbackFragment;-><init>(Ljava/lang/CharSequence;Ljava/util/Set;Ljava/util/Set;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 79
    .local v0, "initialFragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    invoke-virtual {v6, v0}, Lcom/google/android/marvin/talkback/FeedbackItem;->addFragment(Lcom/google/android/marvin/talkback/FeedbackFragment;)V

    .line 80
    invoke-virtual {v6, p4}, Lcom/google/android/marvin/talkback/FeedbackItem;->addFlag(I)V

    .line 83
    invoke-static {v6}, Lcom/google/android/marvin/talkback/FeedbackProcessingUtils;->addFormattingCharacteristics(Lcom/google/android/marvin/talkback/FeedbackItem;)V

    .line 84
    invoke-static {v6}, Lcom/google/android/marvin/talkback/FeedbackProcessingUtils;->splitLongText(Lcom/google/android/marvin/talkback/FeedbackItem;)V

    .line 85
    invoke-static {p0, v6}, Lcom/google/android/marvin/talkback/FeedbackProcessingUtils;->cleanupItemText(Landroid/content/Context;Lcom/google/android/marvin/talkback/FeedbackItem;)V

    .line 87
    return-object v6
.end method

.method private static handleStyleSpan(Lcom/google/android/marvin/talkback/FeedbackItem;Lcom/google/android/marvin/talkback/FeedbackFragment;ILandroid/text/Spannable;Landroid/text/style/StyleSpan;)Z
    .locals 10
    .param p0, "item"    # Lcom/google/android/marvin/talkback/FeedbackItem;
    .param p1, "fragment"    # Lcom/google/android/marvin/talkback/FeedbackFragment;
    .param p2, "fragmentPosition"    # I
    .param p3, "spannable"    # Landroid/text/Spannable;
    .param p4, "span"    # Landroid/text/style/StyleSpan;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 263
    invoke-interface {p3, p4}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    .line 264
    .local v4, "spanStart":I
    invoke-interface {p3, p4}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    .line 265
    .local v2, "spanEnd":I
    invoke-virtual {p4}, Landroid/text/style/StyleSpan;->getStyle()I

    move-result v6

    .line 267
    .local v6, "style":I
    if-ltz v4, :cond_0

    if-gez v2, :cond_1

    .line 311
    :cond_0
    :goto_0
    return v8

    .line 273
    :cond_1
    packed-switch v6, :pswitch_data_0

    goto :goto_0

    .line 275
    :pswitch_0
    const v7, 0x3f733333    # 0.95f

    .line 276
    .local v7, "voicePitch":F
    const/high16 v1, 0x7f050000

    .line 290
    .local v1, "earconId":I
    :goto_1
    new-instance v0, Lcom/google/android/marvin/talkback/FeedbackFragment;

    invoke-interface {p3, v8, v4}, Landroid/text/Spannable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-direct {v0, v8, v9}, Lcom/google/android/marvin/talkback/FeedbackFragment;-><init>(Ljava/lang/CharSequence;Landroid/os/Bundle;)V

    .line 292
    .local v0, "beforeSpanFragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    invoke-static {p1, v0}, Lcom/google/android/marvin/talkback/FeedbackProcessingUtils;->copyFragmentMetadata(Lcom/google/android/marvin/talkback/FeedbackFragment;Lcom/google/android/marvin/talkback/FeedbackFragment;)V

    .line 293
    invoke-virtual {p0, v0, p2}, Lcom/google/android/marvin/talkback/FeedbackItem;->addFragmentAtPosition(Lcom/google/android/marvin/talkback/FeedbackFragment;I)V

    .line 297
    new-instance v3, Lcom/google/android/marvin/talkback/FeedbackFragment;

    invoke-interface {p3, v4, v2}, Landroid/text/Spannable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-direct {v3, v8, v9}, Lcom/google/android/marvin/talkback/FeedbackFragment;-><init>(Ljava/lang/CharSequence;Landroid/os/Bundle;)V

    .line 299
    .local v3, "spanFragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    new-instance v5, Landroid/os/Bundle;

    sget-object v8, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-direct {v5, v8}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 300
    .local v5, "speechParams":Landroid/os/Bundle;
    const-string v8, "pitch"

    invoke-virtual {v5, v8, v7}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 301
    invoke-virtual {v3, v5}, Lcom/google/android/marvin/talkback/FeedbackFragment;->setSpeechParams(Landroid/os/Bundle;)V

    .line 302
    invoke-virtual {v3, v1}, Lcom/google/android/marvin/talkback/FeedbackFragment;->addEarcon(I)V

    .line 303
    add-int/lit8 v8, p2, 0x1

    invoke-virtual {p0, v3, v8}, Lcom/google/android/marvin/talkback/FeedbackItem;->addFragmentAtPosition(Lcom/google/android/marvin/talkback/FeedbackFragment;I)V

    .line 308
    invoke-interface {p3}, Landroid/text/Spannable;->length()I

    move-result v8

    invoke-interface {p3, v2, v8}, Landroid/text/Spannable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/google/android/marvin/talkback/FeedbackFragment;->setText(Ljava/lang/CharSequence;)V

    .line 309
    invoke-static {p1}, Lcom/google/android/marvin/talkback/FeedbackProcessingUtils;->clearFragmentMetadata(Lcom/google/android/marvin/talkback/FeedbackFragment;)V

    .line 311
    const/4 v8, 0x1

    goto :goto_0

    .line 279
    .end local v0    # "beforeSpanFragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    .end local v1    # "earconId":I
    .end local v3    # "spanFragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    .end local v5    # "speechParams":Landroid/os/Bundle;
    .end local v7    # "voicePitch":F
    :pswitch_1
    const v7, 0x3f866666    # 1.05f

    .line 280
    .restart local v7    # "voicePitch":F
    const v1, 0x7f050009

    .line 281
    .restart local v1    # "earconId":I
    goto :goto_1

    .line 273
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static handleUrlSpan(Lcom/google/android/marvin/talkback/FeedbackItem;Lcom/google/android/marvin/talkback/FeedbackFragment;ILandroid/text/Spannable;Landroid/text/style/URLSpan;)Z
    .locals 7
    .param p0, "item"    # Lcom/google/android/marvin/talkback/FeedbackItem;
    .param p1, "fragment"    # Lcom/google/android/marvin/talkback/FeedbackFragment;
    .param p2, "fragmentPosition"    # I
    .param p3, "spannable"    # Landroid/text/Spannable;
    .param p4, "span"    # Landroid/text/style/URLSpan;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 212
    invoke-interface {p3, p4}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v3

    .line 213
    .local v3, "spanStart":I
    invoke-interface {p3, p4}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    .line 215
    .local v1, "spanEnd":I
    if-ltz v3, :cond_0

    if-gez v1, :cond_1

    .line 244
    :cond_0
    :goto_0
    return v5

    .line 223
    :cond_1
    new-instance v0, Lcom/google/android/marvin/talkback/FeedbackFragment;

    invoke-interface {p3, v5, v3}, Landroid/text/Spannable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-direct {v0, v5, v6}, Lcom/google/android/marvin/talkback/FeedbackFragment;-><init>(Ljava/lang/CharSequence;Landroid/os/Bundle;)V

    .line 225
    .local v0, "beforeSpanFragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    invoke-static {p1, v0}, Lcom/google/android/marvin/talkback/FeedbackProcessingUtils;->copyFragmentMetadata(Lcom/google/android/marvin/talkback/FeedbackFragment;Lcom/google/android/marvin/talkback/FeedbackFragment;)V

    .line 226
    invoke-virtual {p0, v0, p2}, Lcom/google/android/marvin/talkback/FeedbackItem;->addFragmentAtPosition(Lcom/google/android/marvin/talkback/FeedbackFragment;I)V

    .line 230
    new-instance v2, Lcom/google/android/marvin/talkback/FeedbackFragment;

    invoke-interface {p3, v3, v1}, Landroid/text/Spannable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-direct {v2, v5, v6}, Lcom/google/android/marvin/talkback/FeedbackFragment;-><init>(Ljava/lang/CharSequence;Landroid/os/Bundle;)V

    .line 232
    .local v2, "spanFragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    new-instance v4, Landroid/os/Bundle;

    sget-object v5, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-direct {v4, v5}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 233
    .local v4, "speechParams":Landroid/os/Bundle;
    const-string v5, "pitch"

    const v6, 0x3f733333    # 0.95f

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 234
    invoke-virtual {v2, v4}, Lcom/google/android/marvin/talkback/FeedbackFragment;->setSpeechParams(Landroid/os/Bundle;)V

    .line 235
    const v5, 0x7f050008

    invoke-virtual {v2, v5}, Lcom/google/android/marvin/talkback/FeedbackFragment;->addEarcon(I)V

    .line 236
    add-int/lit8 v5, p2, 0x1

    invoke-virtual {p0, v2, v5}, Lcom/google/android/marvin/talkback/FeedbackItem;->addFragmentAtPosition(Lcom/google/android/marvin/talkback/FeedbackFragment;I)V

    .line 241
    invoke-interface {p3}, Landroid/text/Spannable;->length()I

    move-result v5

    invoke-interface {p3, v1, v5}, Landroid/text/Spannable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/google/android/marvin/talkback/FeedbackFragment;->setText(Ljava/lang/CharSequence;)V

    .line 242
    invoke-static {p1}, Lcom/google/android/marvin/talkback/FeedbackProcessingUtils;->clearFragmentMetadata(Lcom/google/android/marvin/talkback/FeedbackFragment;)V

    .line 244
    const/4 v5, 0x1

    goto :goto_0
.end method

.method private static splitLongText(Lcom/google/android/marvin/talkback/FeedbackItem;)V
    .locals 12
    .param p0, "item"    # Lcom/google/android/marvin/talkback/FeedbackItem;

    .prologue
    .line 98
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FeedbackItem;->getFragments()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-ge v5, v10, :cond_4

    .line 99
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FeedbackItem;->getFragments()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/marvin/talkback/FeedbackFragment;

    .line 100
    .local v2, "fragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/FeedbackFragment;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    .line 101
    .local v4, "fragmentText":Ljava/lang/CharSequence;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 98
    :cond_0
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 105
    :cond_1
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v10

    const/16 v11, 0xf9f

    if-le v10, v11, :cond_0

    .line 109
    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/FeedbackItem;->removeFragment(Lcom/google/android/marvin/talkback/FeedbackFragment;)Z

    .line 114
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 115
    .local v1, "end":I
    const/4 v8, 0x0

    .line 116
    .local v8, "start":I
    const/4 v6, 0x0

    .line 118
    .local v6, "splitFragments":I
    :goto_2
    if-ge v8, v1, :cond_3

    .line 119
    add-int/lit16 v3, v8, 0xf9f

    .line 124
    .local v3, "fragmentEnd":I
    const/16 v10, 0x20

    invoke-static {v4, v10, v8, v3}, Landroid/text/TextUtils;->lastIndexOf(Ljava/lang/CharSequence;CII)I

    move-result v7

    .line 126
    .local v7, "splitLocation":I
    if-gez v7, :cond_2

    .line 127
    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 129
    :cond_2
    invoke-static {v4, v8, v7}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v9

    .line 131
    .local v9, "textSection":Ljava/lang/CharSequence;
    new-instance v0, Lcom/google/android/marvin/talkback/FeedbackFragment;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/FeedbackFragment;->getSpeechParams()Landroid/os/Bundle;

    move-result-object v10

    invoke-direct {v0, v9, v10}, Lcom/google/android/marvin/talkback/FeedbackFragment;-><init>(Ljava/lang/CharSequence;Landroid/os/Bundle;)V

    .line 133
    .local v0, "additionalFragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    add-int v10, v5, v6

    invoke-virtual {p0, v0, v10}, Lcom/google/android/marvin/talkback/FeedbackItem;->addFragmentAtPosition(Lcom/google/android/marvin/talkback/FeedbackFragment;I)V

    .line 134
    add-int/lit8 v6, v6, 0x1

    .line 135
    add-int/lit8 v8, v7, 0x1

    .line 136
    goto :goto_2

    .line 140
    .end local v0    # "additionalFragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    .end local v3    # "fragmentEnd":I
    .end local v7    # "splitLocation":I
    .end local v9    # "textSection":Ljava/lang/CharSequence;
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FeedbackItem;->getFragments()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/marvin/talkback/FeedbackFragment;

    invoke-static {v2, v10}, Lcom/google/android/marvin/talkback/FeedbackProcessingUtils;->copyFragmentMetadata(Lcom/google/android/marvin/talkback/FeedbackFragment;Lcom/google/android/marvin/talkback/FeedbackFragment;)V

    goto :goto_1

    .line 143
    .end local v1    # "end":I
    .end local v2    # "fragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    .end local v4    # "fragmentText":Ljava/lang/CharSequence;
    .end local v6    # "splitFragments":I
    .end local v8    # "start":I
    :cond_4
    return-void
.end method
