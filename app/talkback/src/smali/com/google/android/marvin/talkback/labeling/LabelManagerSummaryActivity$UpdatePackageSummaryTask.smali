.class Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;
.super Landroid/os/AsyncTask;
.source "LabelManagerSummaryActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdatePackageSummaryTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;",
        ">;>;"
    }
.end annotation


# instance fields
.field private mLocale:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;
    .param p2, "x1"    # Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$1;

    .prologue
    .line 193
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;-><init>(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 193
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    .line 203
    const-string v0, "Spawning new UpdatePackageSummaryTask(%d) for %s."

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;->mLocale:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {p0, v4, v0, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 206
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->mLabelProviderClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->access$100(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;)Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;->mLocale:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->getPackageSummary(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 193
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/labeling/PackageLabelInfo;>;"
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 211
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    new-instance v1, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    iget-object v3, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    const v4, 0x7f030004

    invoke-direct {v1, v2, v3, v4, p1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;-><init>(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;Landroid/content/Context;ILjava/util/List;)V

    # setter for: Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->mPackageLabelInfoAdapter:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;
    invoke-static {v0, v1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->access$202(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;)Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;

    .line 215
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->mPackageList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->access$300(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->mPackageLabelInfoAdapter:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->access$200(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;)Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 216
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->mPackageList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->access$300(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 217
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->mNoPackagesMessage:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->access$400(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 222
    :goto_0
    return-void

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->mPackageList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->access$300(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 220
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->mNoPackagesMessage:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->access$400(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 198
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;->mLocale:Ljava/lang/String;

    .line 199
    return-void
.end method
