.class Lcom/google/android/marvin/talkback/menurules/RuleEditText;
.super Ljava/lang/Object;
.source "RuleEditText.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/menurules/NodeMenuRule;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    return-void
.end method


# virtual methods
.method public accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 46
    const-class v0, Landroid/widget/EditText;

    invoke-static {p1, p2, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public canCollapseMenu()Z
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x1

    return v0
.end method

.method public getMenuItemsForNode(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/List;
    .locals 19
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/marvin/talkback/TalkBackService;",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    invoke-static/range {p2 .. p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v18

    .line 54
    .local v18, "nodeCopy":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v14

    .line 55
    .local v14, "cursorController":Lcom/google/android/marvin/talkback/CursorController;
    new-instance v17, Ljava/util/LinkedList;

    invoke-direct/range {v17 .. v17}, Ljava/util/LinkedList;-><init>()V

    .line 59
    .local v17, "items":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/widget/RadialMenuItem;>;"
    invoke-virtual/range {v18 .. v18}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 60
    const/4 v9, 0x1

    new-array v9, v9, [I

    const/4 v10, 0x0

    const/16 v11, 0x200

    aput v11, v9, v10

    move-object/from16 v0, v18

    invoke-static {v0, v9}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 62
    new-instance v2, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v4, 0x0

    const v5, 0x7f0d0004

    const/4 v6, 0x0

    const v9, 0x7f060134

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v7}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 65
    .local v2, "moveToBeginning":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    .end local v2    # "moveToBeginning":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :cond_0
    const/4 v9, 0x1

    new-array v9, v9, [I

    const/4 v10, 0x0

    const/16 v11, 0x100

    aput v11, v9, v10

    move-object/from16 v0, v18

    invoke-static {v0, v9}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 70
    new-instance v3, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v5, 0x0

    const v6, 0x7f0d0005

    const/4 v7, 0x0

    const v9, 0x7f060135

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v8}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 73
    .local v3, "moveToEnd":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    .end local v3    # "moveToEnd":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :cond_1
    const/4 v9, 0x1

    new-array v9, v9, [I

    const/4 v10, 0x0

    const/high16 v11, 0x10000

    aput v11, v9, v10

    move-object/from16 v0, v18

    invoke-static {v0, v9}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 78
    new-instance v4, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v6, 0x0

    const v7, 0x7f0d0006

    const/4 v8, 0x0

    const v9, 0x7f060136

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v5, p1

    invoke-direct/range {v4 .. v9}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 81
    .local v4, "cut":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    .end local v4    # "cut":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :cond_2
    const/4 v9, 0x1

    new-array v9, v9, [I

    const/4 v10, 0x0

    const/16 v11, 0x4000

    aput v11, v9, v10

    move-object/from16 v0, v18

    invoke-static {v0, v9}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 86
    new-instance v5, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v7, 0x0

    const v8, 0x7f0d0007

    const/4 v9, 0x0

    const v10, 0x7f060137

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v6, p1

    invoke-direct/range {v5 .. v10}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 89
    .local v5, "copy":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    .end local v5    # "copy":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :cond_3
    const/4 v9, 0x1

    new-array v9, v9, [I

    const/4 v10, 0x0

    const v11, 0x8000

    aput v11, v9, v10

    move-object/from16 v0, v18

    invoke-static {v0, v9}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 94
    new-instance v6, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v8, 0x0

    const v9, 0x7f0d0008

    const/4 v10, 0x0

    const v11, 0x7f060138

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v7, p1

    invoke-direct/range {v6 .. v11}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 97
    .local v6, "paste":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    .end local v6    # "paste":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :cond_4
    const/4 v9, 0x1

    new-array v9, v9, [I

    const/4 v10, 0x0

    const/high16 v11, 0x20000

    aput v11, v9, v10

    move-object/from16 v0, v18

    invoke-static {v0, v9}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 102
    new-instance v7, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v9, 0x0

    const v10, 0x7f0d0009

    const/4 v11, 0x0

    const v12, 0x7f060139

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v8, p1

    invoke-direct/range {v7 .. v12}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 105
    .local v7, "select":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    move-object/from16 v0, v17

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    .end local v7    # "select":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :cond_5
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x12

    if-lt v9, v10, :cond_6

    .line 112
    invoke-virtual {v14}, Lcom/google/android/marvin/talkback/CursorController;->isSelectionModeActive()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 113
    new-instance v8, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v10, 0x0

    const v11, 0x7f0d000b

    const/4 v12, 0x0

    const v9, 0x7f06013b

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v9, p1

    invoke-direct/range {v8 .. v13}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 123
    .local v8, "selectionMode":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :goto_0
    move-object/from16 v0, v17

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    .end local v8    # "selectionMode":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :cond_6
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/view/MenuItem;

    .line 128
    .local v16, "item":Landroid/view/MenuItem;
    new-instance v9, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-direct {v9, v0, v1}, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_1

    .line 117
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v16    # "item":Landroid/view/MenuItem;
    :cond_7
    new-instance v8, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v10, 0x0

    const v11, 0x7f0d000a

    const/4 v12, 0x0

    const v9, 0x7f06013a

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v9, p1

    invoke-direct/range {v8 .. v13}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .restart local v8    # "selectionMode":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    goto :goto_0

    .line 131
    .end local v8    # "selectionMode":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .restart local v15    # "i$":Ljava/util/Iterator;
    :cond_8
    return-object v17
.end method

.method public getUserFriendlyMenuName(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 136
    const v0, 0x7f060133

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
