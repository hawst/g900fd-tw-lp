.class public Lcom/google/android/marvin/talkback/TalkBackAnalytics;
.super Ljava/lang/Object;
.source "TalkBackAnalytics.java"


# instance fields
.field private final finalizePingListener:Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest$OnAllLabelsFetchedListener;

.field private final mCustomGestureDefaults:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLastPingTime:J

.field private mNumGranularityChanges:I

.field private mNumTextEdits:I

.field private final mPrefDefaults:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mReady:Z

.field private mService:Lcom/google/android/marvin/talkback/TalkBackService;

.field private final mSwipeCounts:Landroid/util/SparseIntArray;

.field private mTracker:Lcom/google/android/gms/analytics/Tracker;

.field private final ttsEngineListener:Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 5
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    const/16 v4, 0x10

    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-boolean v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mReady:Z

    .line 57
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mLastPingTime:J

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mCustomGestureDefaults:Ljava/util/Map;

    .line 73
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0, v3}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mSwipeCounts:Landroid/util/SparseIntArray;

    .line 76
    iput v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mNumGranularityChanges:I

    .line 79
    iput v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mNumTextEdits:I

    .line 247
    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackAnalytics$2;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackAnalytics$2;-><init>(Lcom/google/android/marvin/talkback/TalkBackAnalytics;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->finalizePingListener:Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest$OnAllLabelsFetchedListener;

    .line 267
    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackAnalytics$3;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackAnalytics$3;-><init>(Lcom/google/android/marvin/talkback/TalkBackAnalytics;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->ttsEngineListener:Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

    .line 87
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 88
    invoke-static {p1}, Lcom/google/android/gms/analytics/GoogleAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/analytics/GoogleAnalytics;

    move-result-object v0

    const v1, 0x7f040005

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/GoogleAnalytics;->newTracker(I)Lcom/google/android/gms/analytics/Tracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mTracker:Lcom/google/android/gms/analytics/Tracker;

    .line 90
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->setPrefDefaults()V

    .line 91
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->setCustomGestureDefaults()V

    .line 93
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v4, :cond_0

    .line 94
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mSwipeCounts:Landroid/util/SparseIntArray;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 95
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mSwipeCounts:Landroid/util/SparseIntArray;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 96
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mSwipeCounts:Landroid/util/SparseIntArray;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 97
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mSwipeCounts:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v3, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/SpeechController;->getFailoverTts()Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->ttsEngineListener:Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->addListener(Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;)V

    .line 101
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v4, :cond_1

    .line 102
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v0

    new-instance v1, Lcom/google/android/marvin/talkback/TalkBackAnalytics$1;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/TalkBackAnalytics$1;-><init>(Lcom/google/android/marvin/talkback/TalkBackAnalytics;)V

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/CursorController;->addGranularityListener(Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;)V

    .line 112
    :cond_1
    return-void
.end method

.method static synthetic access$008(Lcom/google/android/marvin/talkback/TalkBackAnalytics;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    .prologue
    .line 31
    iget v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mNumGranularityChanges:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mNumGranularityChanges:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/TalkBackAnalytics;)Lcom/google/android/gms/analytics/Tracker;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mTracker:Lcom/google/android/gms/analytics/Tracker;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/TalkBackAnalytics;)Lcom/google/android/marvin/talkback/TalkBackService;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/marvin/talkback/TalkBackAnalytics;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackAnalytics;
    .param p1, "x1"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mReady:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/TalkBackAnalytics;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->conditionalPing()V

    return-void
.end method

.method private conditionalPing()V
    .locals 6

    .prologue
    .line 139
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 140
    .local v0, "currentTime":J
    iget-boolean v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mReady:Z

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mLastPingTime:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0xdbba00

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 141
    iput-wide v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mLastPingTime:J

    .line 142
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->ping()V

    .line 144
    :cond_0
    return-void
.end method

.method private ping()V
    .locals 15

    .prologue
    const/4 v14, 0x7

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 151
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mTracker:Lcom/google/android/gms/analytics/Tracker;

    new-instance v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-direct {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;-><init>()V

    const/16 v8, 0x9

    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v10, v8}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-virtual {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    .line 158
    :try_start_0
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mTracker:Lcom/google/android/gms/analytics/Tracker;

    new-instance v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-direct {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;-><init>()V

    const/4 v8, 0x2

    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/marvin/talkback/SpeechController;->getFailoverTts()Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->getEngineInstance()Landroid/speech/tts/TextToSpeech;

    move-result-object v9

    invoke-virtual {v9}, Landroid/speech/tts/TextToSpeech;->getLanguage()Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-virtual {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :goto_0
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v6}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 168
    .local v0, "currentPrefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v4

    .line 169
    .local v4, "prefMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 170
    .local v3, "pref":Ljava/lang/String;
    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 174
    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 176
    .local v5, "value":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mCustomGestureDefaults:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mCustomGestureDefaults:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 178
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mTracker:Lcom/google/android/gms/analytics/Tracker;

    new-instance v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-direct {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;-><init>()V

    const/4 v8, 0x5

    invoke-virtual {v6, v8, v3}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    const/4 v8, 0x6

    invoke-virtual {v6, v8, v5}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-virtual {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    goto :goto_1

    .line 183
    :cond_1
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 185
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mTracker:Lcom/google/android/gms/analytics/Tracker;

    new-instance v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-direct {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;-><init>()V

    invoke-virtual {v6, v13, v3}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    const/4 v8, 0x4

    invoke-virtual {v6, v8, v5}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-virtual {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    goto/16 :goto_1

    .line 192
    .end local v3    # "pref":Ljava/lang/String;
    .end local v5    # "value":Ljava/lang/String;
    :cond_2
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x10

    if-lt v6, v7, :cond_3

    .line 195
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mTracker:Lcom/google/android/gms/analytics/Tracker;

    new-instance v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-direct {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;-><init>()V

    const-string v8, "Up"

    invoke-virtual {v6, v14, v8}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mSwipeCounts:Landroid/util/SparseIntArray;

    invoke-virtual {v8, v10}, Landroid/util/SparseIntArray;->get(I)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v6, v10, v8}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomMetric(IF)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-virtual {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    .line 201
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mTracker:Lcom/google/android/gms/analytics/Tracker;

    new-instance v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-direct {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;-><init>()V

    const-string v8, "Left"

    invoke-virtual {v6, v14, v8}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mSwipeCounts:Landroid/util/SparseIntArray;

    invoke-virtual {v8, v13}, Landroid/util/SparseIntArray;->get(I)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v6, v10, v8}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomMetric(IF)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-virtual {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    .line 207
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mTracker:Lcom/google/android/gms/analytics/Tracker;

    new-instance v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-direct {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;-><init>()V

    const-string v8, "Down"

    invoke-virtual {v6, v14, v8}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mSwipeCounts:Landroid/util/SparseIntArray;

    invoke-virtual {v8, v12}, Landroid/util/SparseIntArray;->get(I)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v6, v10, v8}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomMetric(IF)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-virtual {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    .line 213
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mTracker:Lcom/google/android/gms/analytics/Tracker;

    new-instance v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-direct {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;-><init>()V

    const-string v8, "Right"

    invoke-virtual {v6, v14, v8}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mSwipeCounts:Landroid/util/SparseIntArray;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/util/SparseIntArray;->get(I)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v6, v10, v8}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomMetric(IF)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-virtual {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    .line 219
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mSwipeCounts:Landroid/util/SparseIntArray;

    invoke-virtual {v6, v10, v11}, Landroid/util/SparseIntArray;->put(II)V

    .line 220
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mSwipeCounts:Landroid/util/SparseIntArray;

    invoke-virtual {v6, v13, v11}, Landroid/util/SparseIntArray;->put(II)V

    .line 221
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mSwipeCounts:Landroid/util/SparseIntArray;

    invoke-virtual {v6, v12, v11}, Landroid/util/SparseIntArray;->put(II)V

    .line 222
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mSwipeCounts:Landroid/util/SparseIntArray;

    const/4 v7, 0x4

    invoke-virtual {v6, v7, v11}, Landroid/util/SparseIntArray;->put(II)V

    .line 225
    :cond_3
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mTracker:Lcom/google/android/gms/analytics/Tracker;

    new-instance v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-direct {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;-><init>()V

    iget v8, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mNumGranularityChanges:I

    int-to-float v8, v8

    invoke-virtual {v6, v12, v8}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomMetric(IF)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-virtual {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    .line 228
    iput v11, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mNumGranularityChanges:I

    .line 230
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mTracker:Lcom/google/android/gms/analytics/Tracker;

    new-instance v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-direct {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;-><init>()V

    iget v8, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mNumTextEdits:I

    int-to-float v8, v8

    invoke-virtual {v6, v13, v8}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomMetric(IF)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-virtual {v6}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    .line 233
    iput v11, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mNumTextEdits:I

    .line 235
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getLabelManager()Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    move-result-object v2

    .line 236
    .local v2, "labelManager":Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    if-nez v2, :cond_4

    .line 237
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v6}, Lcom/google/android/gms/analytics/GoogleAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/analytics/GoogleAnalytics;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/analytics/GoogleAnalytics;->dispatchLocalHits()V

    .line 241
    :goto_2
    return-void

    .line 239
    :cond_4
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->finalizePingListener:Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest$OnAllLabelsFetchedListener;

    invoke-virtual {v2, v6}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->getAllLabelsFromDatabase(Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest$OnAllLabelsFetchedListener;)V

    goto :goto_2

    .line 162
    .end local v0    # "currentPrefs":Landroid/content/SharedPreferences;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "labelManager":Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    .end local v4    # "prefMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    :catch_0
    move-exception v6

    goto/16 :goto_0
.end method

.method private setCustomGestureDefaults()V
    .locals 4

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mCustomGestureDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060044

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f060072

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mCustomGestureDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060045

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f060073

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mCustomGestureDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060042

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f060070

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mCustomGestureDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060043

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f060071

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mCustomGestureDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060046

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f060074

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mCustomGestureDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060047

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f060075

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mCustomGestureDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060048

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f060076

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mCustomGestureDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060049

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f060077

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mCustomGestureDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f06004d

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f060078

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mCustomGestureDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f06004e

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f060079

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mCustomGestureDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f06004b

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f06007b

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    return-void
.end method

.method private setPrefDefaults()V
    .locals 4

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060023

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f060068

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060024

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f060069

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060025

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f06006a

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060021

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0b0001

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060022

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0b0002

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060027

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0b0003

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060028

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0b0004

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060029

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0b0005

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f06002b

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f06006b

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f06002c

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f06006c

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f06002d

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0b0006

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f06002e

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f06006d

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f06002f

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0b0007

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060032

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0b0009

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060033

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f06006e

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060034

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0b000a

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060035

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0b000b

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060037

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0b000c

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060038

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0b000d

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060039

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0b000e

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f06003a

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f06006f

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060051

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0b000f

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f06004f

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f06007a

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mPrefDefaults:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f060030

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0b0008

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    return-void
.end method


# virtual methods
.method public onGesture(I)V
    .locals 2
    .param p1, "gestureId"    # I

    .prologue
    .line 120
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mSwipeCounts:Landroid/util/SparseIntArray;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mSwipeCounts:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 126
    :cond_1
    return-void
.end method

.method public onTextEdited()V
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mNumTextEdits:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mNumTextEdits:I

    .line 133
    return-void
.end method
