.class Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$4;
.super Landroid/view/View$AccessibilityDelegate;
.source "TouchTutorialModule5.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$4;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 4
    .param p1, "host"    # Landroid/view/ViewGroup;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 119
    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$4;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->access$000(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)Landroid/widget/EditText;

    move-result-object v2

    if-ne p2, v2, :cond_0

    .line 120
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v2

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_0

    .line 121
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v1

    .line 122
    .local v1, "selectionStartIndex":I
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getToIndex()I

    move-result v0

    .line 124
    .local v0, "selectionEndIndex":I
    if-nez v1, :cond_0

    if-nez v0, :cond_0

    .line 125
    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$4;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->mLayout:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->access$100(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;)Landroid/widget/LinearLayout;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 126
    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$4;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;

    new-instance v3, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$4$1;

    invoke-direct {v3, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$4$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5$4;)V

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule5;->installTriggerDelayedWithFeedback(Ljava/lang/Runnable;)V

    .line 135
    .end local v0    # "selectionEndIndex":I
    .end local v1    # "selectionStartIndex":I
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/view/View$AccessibilityDelegate;->onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v2

    return v2
.end method
