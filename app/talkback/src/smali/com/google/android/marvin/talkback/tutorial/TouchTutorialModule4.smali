.class Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;
.super Lcom/google/android/marvin/talkback/tutorial/TutorialModule;
.source "TouchTutorialModule4.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field private static final IS_LAST_LESSON:Z


# instance fields
.field private final mCharacterGranularityListener:Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;

.field private final mContextMenuMonitor:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

.field private final mGestureMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

.field private final mIncorrectGranularityListener:Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;

.field private mLayout:Landroid/widget/LinearLayout;

.field private final mLocalContextMenuGestureDelegate:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;

.field private final mLocalContextMenuHiddenDelegate:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;

.field private final mNavigateByGranularityDelegate:Landroid/view/View$AccessibilityDelegate;

.field private mNavigationCount:I

.field private mTextView:Landroid/widget/TextView;

.field private final mTextViewFocusLostDelegate:Landroid/view/View$AccessibilityDelegate;

.field private final mTextViewFocusedDelegate:Landroid/view/View$AccessibilityDelegate;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 233
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->IS_LAST_LESSON:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V
    .locals 4
    .param p1, "parentTutorial"    # Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 237
    const v0, 0x7f03000f

    const v3, 0x7f0601d5

    invoke-direct {p0, p1, v0, v3}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;II)V

    .line 44
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    invoke-direct {v0}, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mGestureMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    .line 47
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

    invoke-direct {v0}, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mContextMenuMonitor:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

    .line 50
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mTextViewFocusedDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 70
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$2;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$2;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mTextViewFocusLostDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 92
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$3;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$3;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLocalContextMenuGestureDelegate:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;

    .line 113
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$4;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$4;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLocalContextMenuHiddenDelegate:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;

    .line 147
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$5;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$5;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mCharacterGranularityListener:Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;

    .line 168
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$6;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$6;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mIncorrectGranularityListener:Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;

    .line 189
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$7;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mNavigateByGranularityDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 230
    iput v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mNavigationCount:I

    .line 240
    const v0, 0x7f0d0095

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLayout:Landroid/widget/LinearLayout;

    .line 241
    const v0, 0x7f0d0096

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mTextView:Landroid/widget/TextView;

    .line 243
    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->setSkipVisible(Z)V

    .line 244
    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->setBackVisible(Z)V

    .line 245
    sget-boolean v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->IS_LAST_LESSON:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->setNextVisible(Z)V

    .line 246
    sget-boolean v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->IS_LAST_LESSON:Z

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->setFinishVisible(Z)V

    .line 247
    return-void

    :cond_0
    move v0, v2

    .line 245
    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->onTrigger4GranularityChanged()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mNavigationCount:I

    return v0
.end method

.method static synthetic access$1108(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mNavigationCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mNavigationCount:I

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->onTrigger4()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->onTrigger1()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mGestureMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->onTrigger2FocusLost()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->onTrigger2()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->removeAllGranularityListeners()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mContextMenuMonitor:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->onTrigger3MenuHidden()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->onTrigger3()V

    return-void
.end method

.method private clearDelegates()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 379
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 380
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mContextMenuMonitor:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->setListener(Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;)V

    .line 381
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mGestureMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;->setListener(Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;)V

    .line 382
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->removeAllGranularityListeners()V

    .line 383
    return-void
.end method

.method private onTrigger0()V
    .locals 3

    .prologue
    .line 274
    const v0, 0x7f0601d6

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 277
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mTextViewFocusedDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 278
    return-void
.end method

.method private onTrigger1()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 281
    sget-object v1, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->LOCAL_BREAKOUT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->getGestureDirectionForRequiredAction(Lcom/google/android/marvin/talkback/ShortcutGestureAction;)I

    move-result v0

    .line 283
    .local v0, "direction":I
    if-gez v0, :cond_0

    .line 295
    :goto_0
    return-void

    .line 287
    :cond_0
    const v1, 0x7f0601d7

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v5, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 291
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mGestureMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLocalContextMenuGestureDelegate:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;->setListener(Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;)V

    .line 294
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mTextViewFocusLostDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto :goto_0
.end method

.method private onTrigger2()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 305
    const v0, 0x7f0601d9

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f060111

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v5, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 310
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mCharacterGranularityListener:Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->addGranularityListener(Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;)Z

    .line 314
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mContextMenuMonitor:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLocalContextMenuHiddenDelegate:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->setListener(Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor$ContextMenuListener;)V

    .line 315
    return-void
.end method

.method private onTrigger2FocusLost()V
    .locals 3

    .prologue
    .line 298
    const v0, 0x7f0601d8

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 301
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mTextViewFocusedDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 302
    return-void
.end method

.method private onTrigger3()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 336
    const v0, 0x7f0601db

    const/4 v1, 0x1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 338
    iput v3, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mNavigationCount:I

    .line 342
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mNavigateByGranularityDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 345
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mIncorrectGranularityListener:Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->addGranularityListener(Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;)Z

    .line 346
    return-void
.end method

.method private onTrigger3MenuHidden()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 318
    sget-object v1, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->LOCAL_BREAKOUT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->getGestureDirectionForRequiredAction(Lcom/google/android/marvin/talkback/ShortcutGestureAction;)I

    move-result v0

    .line 320
    .local v0, "direction":I
    if-gez v0, :cond_0

    .line 333
    :goto_0
    return-void

    .line 324
    :cond_0
    const v1, 0x7f0601da

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f060111

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p0, v1, v6, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 329
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mGestureMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLocalContextMenuGestureDelegate:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;->setListener(Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;)V

    .line 332
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mTextViewFocusLostDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto :goto_0
.end method

.method private onTrigger4()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 367
    sget-boolean v2, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->IS_LAST_LESSON:Z

    if-eqz v2, :cond_0

    const v0, 0x7f0601bb

    .line 370
    .local v0, "buttonLabelResId":I
    :goto_0
    sget-boolean v2, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->IS_LAST_LESSON:Z

    if-eqz v2, :cond_1

    const v1, 0x7f0601de

    .line 375
    .local v1, "instructionResId":I
    :goto_1
    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v5, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 376
    return-void

    .line 367
    .end local v0    # "buttonLabelResId":I
    .end local v1    # "instructionResId":I
    :cond_0
    const v0, 0x7f0601b9

    goto :goto_0

    .line 370
    .restart local v0    # "buttonLabelResId":I
    :cond_1
    const v1, 0x7f0601dd

    goto :goto_1
.end method

.method private onTrigger4GranularityChanged()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 349
    sget-object v1, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->LOCAL_BREAKOUT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->getGestureDirectionForRequiredAction(Lcom/google/android/marvin/talkback/ShortcutGestureAction;)I

    move-result v0

    .line 351
    .local v0, "direction":I
    if-gez v0, :cond_0

    .line 364
    :goto_0
    return-void

    .line 355
    :cond_0
    const v1, 0x7f0601dc

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f060111

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p0, v1, v6, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 360
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mGestureMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLocalContextMenuGestureDelegate:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;->setListener(Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;)V

    .line 363
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mTextViewFocusLostDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto :goto_0
.end method

.method private registerReceivers()V
    .locals 2

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mGestureMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    sget-object v1, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;->FILTER:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 394
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mContextMenuMonitor:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

    sget-object v1, Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;->FILTER:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 395
    return-void
.end method

.method private removeAllGranularityListeners()V
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mCharacterGranularityListener:Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->removeGranularityListener(Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;)Z

    .line 387
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mIncorrectGranularityListener:Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->removeGranularityListener(Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;)Z

    .line 388
    return-void
.end method

.method private unregisterReceivers()V
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mGestureMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 399
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mContextMenuMonitor:Lcom/google/android/marvin/talkback/tutorial/ContextMenuMonitor;

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 400
    return-void
.end method


# virtual methods
.method public onPause()V
    .locals 0

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->clearDelegates()V

    .line 257
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->unregisterReceivers()V

    .line 258
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 262
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->registerReceivers()V

    .line 270
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->onTrigger0()V

    .line 271
    return-void
.end method
