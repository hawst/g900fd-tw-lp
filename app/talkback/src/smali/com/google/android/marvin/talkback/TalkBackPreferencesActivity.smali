.class public Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;
.super Landroid/preference/PreferenceActivity;
.source "TalkBackPreferencesActivity.java"


# static fields
.field private static final GESTURE_PREF_KEY_IDS:[I


# instance fields
.field private mExploreByTouchDialog:Landroid/app/AlertDialog;

.field private final mHandler:Landroid/os/Handler;

.field private mPrefProxy:Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;

.field private final mPreferenceChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private final mTouchExplorationChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

.field private final mTouchExploreObserver:Landroid/database/ContentObserver;

.field private final mTreeDebugChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

.field private mTreeDebugDialog:Landroid/app/AlertDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->GESTURE_PREF_KEY_IDS:[I

    return-void

    :array_0
    .array-data 4
        0x7f060044
        0x7f060045
        0x7f060048
        0x7f060049
        0x7f060046
        0x7f060047
        0x7f060042
        0x7f060043
        0x7f06004d
        0x7f06004e
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 715
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mHandler:Landroid/os/Handler;

    .line 717
    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$8;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$8;-><init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTouchExploreObserver:Landroid/database/ContentObserver;

    .line 729
    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$9;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$9;-><init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTouchExplorationChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    .line 746
    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$10;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$10;-><init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTreeDebugChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    .line 784
    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$11;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$11;-><init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mPreferenceChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mExploreByTouchDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->setTouchExplorationRequested(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;I)Landroid/preference/Preference;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;
    .param p1, "x1"    # I

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTreeDebugDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->updateTouchExplorationState()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->createDisableExploreByTouchDialog()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->createEnableTreeDebugDialog()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700()[I
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->GESTURE_PREF_KEY_IDS:[I

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private assignLabelManagerIntent()V
    .locals 5

    .prologue
    .line 237
    const v3, 0x7f06003f

    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 240
    .local v0, "category":Landroid/preference/PreferenceGroup;
    const v3, 0x7f06003c

    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v2

    .line 242
    .local v2, "prefManageLabels":Landroid/preference/Preference;
    if-eqz v0, :cond_0

    if-nez v2, :cond_1

    .line 255
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x12

    if-ge v3, v4, :cond_2

    .line 247
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 251
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 252
    .local v1, "labelManagerIntent":Landroid/content/Intent;
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 253
    const/high16 v3, 0x4000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 254
    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private assignTutorialIntent()V
    .locals 6

    .prologue
    .line 212
    const v4, 0x7f06003d

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 214
    .local v0, "category":Landroid/preference/PreferenceGroup;
    const v4, 0x7f06003b

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v1

    .line 216
    .local v1, "prefTutorial":Landroid/preference/Preference;
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 231
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v2, v4, Landroid/content/res/Configuration;->touchscreen:I

    .line 221
    .local v2, "touchscreenState":I
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v4, v5, :cond_2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_3

    .line 223
    :cond_2
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 227
    :cond_3
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 228
    .local v3, "tutorialIntent":Landroid/content/Intent;
    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 229
    const/high16 v4, 0x4000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 230
    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private checkAccelerometerSupport()V
    .locals 6

    .prologue
    .line 490
    const-string v4, "sensor"

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    .line 491
    .local v2, "manager":Landroid/hardware/SensorManager;
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    .line 493
    .local v0, "accel":Landroid/hardware/Sensor;
    if-eqz v0, :cond_1

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v4, v5, :cond_1

    .line 505
    :cond_0
    :goto_0
    return-void

    .line 497
    :cond_1
    const v4, 0x7f060020

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceGroup;

    .line 499
    .local v1, "category":Landroid/preference/PreferenceGroup;
    const v4, 0x7f06003a

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/ListPreference;

    .line 502
    .local v3, "prefShake":Landroid/preference/ListPreference;
    if-eqz v3, :cond_0

    .line 503
    invoke-virtual {v1, v3}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method private checkInstalledBacks()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 512
    const v9, 0x7f060026

    invoke-direct {p0, v9}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 514
    .local v0, "category":Landroid/preference/PreferenceGroup;
    const v9, 0x7f060027

    invoke-direct {p0, v9}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    .line 516
    .local v4, "prefVibration":Landroid/preference/CheckBoxPreference;
    const-string v9, "com.google.android.marvin.kickback"

    invoke-static {p0, v9}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->getVersionCode(Landroid/content/Context;Ljava/lang/CharSequence;)I

    move-result v1

    .line 518
    .local v1, "kickBackVersionCode":I
    const/4 v9, 0x5

    if-lt v1, v9, :cond_4

    move v5, v6

    .line 521
    .local v5, "removeKickBack":Z
    :goto_0
    if-eqz v5, :cond_0

    .line 522
    if-eqz v4, :cond_0

    .line 523
    invoke-virtual {v0, v4}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 527
    :cond_0
    const v9, 0x7f060028

    invoke-direct {p0, v9}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    .line 529
    .local v2, "prefSoundBack":Landroid/preference/CheckBoxPreference;
    const v9, 0x7f06002c

    invoke-direct {p0, v9}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v3

    .line 531
    .local v3, "prefSoundBackVolume":Landroid/preference/Preference;
    const-string v9, "com.google.android.marvin.soundback"

    invoke-static {p0, v9}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->getVersionCode(Landroid/content/Context;Ljava/lang/CharSequence;)I

    move-result v7

    .line 533
    .local v7, "soundBackVersionCode":I
    const/4 v9, 0x7

    if-lt v7, v9, :cond_5

    .line 536
    .local v6, "removeSoundBack":Z
    :goto_1
    if-eqz v6, :cond_2

    .line 537
    if-eqz v3, :cond_1

    .line 538
    invoke-virtual {v0, v3}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 541
    :cond_1
    if-eqz v2, :cond_2

    .line 542
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 546
    :cond_2
    if-eqz v5, :cond_3

    if-eqz v6, :cond_3

    .line 547
    if-eqz v0, :cond_3

    .line 548
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v8

    invoke-virtual {v8, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 551
    :cond_3
    return-void

    .end local v2    # "prefSoundBack":Landroid/preference/CheckBoxPreference;
    .end local v3    # "prefSoundBackVolume":Landroid/preference/Preference;
    .end local v5    # "removeKickBack":Z
    .end local v6    # "removeSoundBack":Z
    .end local v7    # "soundBackVersionCode":I
    :cond_4
    move v5, v8

    .line 518
    goto :goto_0

    .restart local v2    # "prefSoundBack":Landroid/preference/CheckBoxPreference;
    .restart local v3    # "prefSoundBackVolume":Landroid/preference/Preference;
    .restart local v5    # "removeKickBack":Z
    .restart local v7    # "soundBackVersionCode":I
    :cond_5
    move v6, v8

    .line 533
    goto :goto_1
.end method

.method private checkProximitySupport()V
    .locals 5

    .prologue
    .line 467
    const-string v4, "sensor"

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    .line 468
    .local v1, "manager":Landroid/hardware/SensorManager;
    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v3

    .line 470
    .local v3, "proximity":Landroid/hardware/Sensor;
    if-eqz v3, :cond_1

    .line 483
    :cond_0
    :goto_0
    return-void

    .line 474
    :cond_1
    const v4, 0x7f060020

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 476
    .local v0, "category":Landroid/preference/PreferenceGroup;
    const v4, 0x7f060022

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    .line 479
    .local v2, "prefProximity":Landroid/preference/CheckBoxPreference;
    if-eqz v2, :cond_0

    .line 480
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 481
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method private checkTelephonySupport()V
    .locals 5

    .prologue
    .line 424
    const-string v4, "phone"

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 425
    .local v3, "telephony":Landroid/telephony/TelephonyManager;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    .line 427
    .local v1, "phoneType":I
    if-eqz v1, :cond_1

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 431
    :cond_1
    const v4, 0x7f060020

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 433
    .local v0, "category":Landroid/preference/PreferenceGroup;
    const v4, 0x7f060034

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v2

    .line 435
    .local v2, "prefCallerId":Landroid/preference/Preference;
    if-eqz v2, :cond_0

    .line 436
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method private checkTouchExplorationSupport()V
    .locals 3

    .prologue
    .line 261
    const v1, 0x7f06003f

    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 263
    .local v0, "category":Landroid/preference/PreferenceGroup;
    if-nez v0, :cond_0

    .line 274
    :goto_0
    return-void

    .line 268
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_1

    .line 269
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 273
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->checkTouchExplorationSupportInner(Landroid/preference/PreferenceGroup;)V

    goto :goto_0
.end method

.method private checkTouchExplorationSupportInner(Landroid/preference/PreferenceGroup;)V
    .locals 6
    .param p1, "category"    # Landroid/preference/PreferenceGroup;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 284
    const v4, 0x7f060036

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    .line 286
    .local v1, "prefTouchExploration":Landroid/preference/CheckBoxPreference;
    if-nez v1, :cond_0

    .line 313
    :goto_0
    return-void

    .line 291
    :cond_0
    const v4, 0x7f060038

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 293
    .local v0, "prefSingleTap":Landroid/preference/CheckBoxPreference;
    if-eqz v0, :cond_1

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-ge v4, v5, :cond_1

    .line 295
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 300
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setPersistent(Z)V

    .line 303
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->updateTouchExplorationState()V

    .line 306
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTouchExplorationChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 309
    const v4, 0x7f060040

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v3

    .line 311
    .local v3, "shortcutsScreen":Landroid/preference/Preference;
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 312
    .local v2, "shortcutsIntent":Landroid/content/Intent;
    invoke-virtual {v3, v2}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private checkVibrationSupport()V
    .locals 4

    .prologue
    .line 445
    const-string v3, "vibrator"

    invoke-virtual {p0, v3}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Vibrator;

    .line 447
    .local v2, "vibrator":Landroid/os/Vibrator;
    if-eqz v2, :cond_1

    invoke-static {v2}, Lcom/googlecode/eyesfree/compat/os/VibratorCompatUtils;->hasVibrator(Landroid/os/Vibrator;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 460
    :cond_0
    :goto_0
    return-void

    .line 451
    :cond_1
    const v3, 0x7f060026

    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 453
    .local v0, "category":Landroid/preference/PreferenceGroup;
    const v3, 0x7f060027

    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    .line 456
    .local v1, "prefVibration":Landroid/preference/CheckBoxPreference;
    if-eqz v1, :cond_0

    .line 457
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 458
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method private checkWebScriptsSupport()V
    .locals 3

    .prologue
    .line 409
    const v2, 0x7f06002a

    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 411
    .local v0, "category":Landroid/preference/PreferenceGroup;
    const v2, 0x7f060032

    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v1

    .line 413
    .local v1, "prefWebScripts":Landroid/preference/Preference;
    if-eqz v1, :cond_0

    .line 414
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 417
    :cond_0
    return-void
.end method

.method private createDisableExploreByTouchDialog()Landroid/app/AlertDialog;
    .locals 5

    .prologue
    .line 634
    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$2;-><init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V

    .line 641
    .local v0, "cancel":Landroid/content/DialogInterface$OnCancelListener;
    new-instance v1, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$3;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$3;-><init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V

    .line 648
    .local v1, "cancelClick":Landroid/content/DialogInterface$OnClickListener;
    new-instance v2, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$4;

    invoke-direct {v2, p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$4;-><init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V

    .line 663
    .local v2, "okClick":Landroid/content/DialogInterface$OnClickListener;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f060183

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f060184

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/high16 v4, 0x1040000

    invoke-virtual {v3, v4, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x1040013

    invoke-virtual {v3, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method

.method private createEnableTreeDebugDialog()Landroid/app/AlertDialog;
    .locals 5

    .prologue
    .line 673
    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$5;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$5;-><init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V

    .line 680
    .local v0, "cancel":Landroid/content/DialogInterface$OnCancelListener;
    new-instance v1, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$6;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$6;-><init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V

    .line 687
    .local v1, "cancelClick":Landroid/content/DialogInterface$OnClickListener;
    new-instance v2, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$7;

    invoke-direct {v2, p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$7;-><init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V

    .line 706
    .local v2, "okClick":Landroid/content/DialogInterface$OnClickListener;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f060185

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f060186

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/high16 v4, 0x1040000

    invoke-virtual {v3, v4, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x1040013

    invoke-virtual {v3, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method

.method private findPreferenceByResId(I)Landroid/preference/Preference;
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 600
    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    return-object v0
.end method

.method private fixListSummaries(Landroid/preference/PreferenceGroup;)V
    .locals 5
    .param p1, "group"    # Landroid/preference/PreferenceGroup;

    .prologue
    .line 378
    if-nez p1, :cond_1

    .line 399
    :cond_0
    return-void

    .line 382
    :cond_1
    invoke-virtual {p1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    .line 384
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 385
    invoke-virtual {p1, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    .line 387
    .local v2, "preference":Landroid/preference/Preference;
    instance-of v3, v2, Landroid/preference/PreferenceGroup;

    if-eqz v3, :cond_3

    .line 388
    check-cast v2, Landroid/preference/PreferenceGroup;

    .end local v2    # "preference":Landroid/preference/Preference;
    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->fixListSummaries(Landroid/preference/PreferenceGroup;)V

    .line 384
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 389
    .restart local v2    # "preference":Landroid/preference/Preference;
    :cond_3
    instance-of v3, v2, Landroid/preference/ListPreference;

    if-eqz v3, :cond_2

    .line 393
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mPreferenceChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    move-object v3, v2

    check-cast v3, Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v2, v3}, Landroid/preference/Preference$OnPreferenceChangeListener;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z

    .line 396
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mPreferenceChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_1
.end method

.method private isTouchExplorationEnabled(Landroid/content/ContentResolver;)Z
    .locals 3
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 369
    const-string v2, "touch_exploration_enabled"

    invoke-static {p1, v2, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private registerTouchSettingObserver()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 204
    const-string v1, "touch_exploration_enabled"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 205
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTouchExploreObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 206
    return-void
.end method

.method private setTouchExplorationRequested(Z)Z
    .locals 5
    .param p1, "requestedState"    # Z

    .prologue
    const/4 v1, 0x0

    .line 611
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 616
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060035

    invoke-static {v0, v2, v3, p1}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->putBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;IZ)V

    .line 621
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v2

    if-nez v2, :cond_0

    .line 622
    const/4 v1, 0x1

    .line 630
    :goto_0
    return v1

    .line 629
    :cond_0
    const/4 v2, 0x3

    const-string v3, "TalkBack active, waiting for EBT request to take effect"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private showTalkBackVersion()V
    .locals 11

    .prologue
    .line 559
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 562
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-lt v5, v6, :cond_0

    .line 563
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 564
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_0

    .line 565
    const v5, 0x7f060086

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {p0, v5, v6}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 571
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    :cond_0
    const v5, 0x7f06003e

    invoke-direct {p0, v5}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v3

    .line 573
    .local v3, "playStoreButton":Landroid/preference/Preference;
    if-nez v3, :cond_1

    .line 591
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v3    # "playStoreButton":Landroid/preference/Preference;
    :goto_0
    return-void

    .line 577
    .restart local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    .restart local v3    # "playStoreButton":Landroid/preference/Preference;
    :cond_1
    iget v5, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 578
    .local v4, "versionNumber":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    .line 580
    .local v1, "length":I
    const v5, 0x7f0600ee

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    add-int/lit8 v10, v1, -0x7

    invoke-virtual {v4, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v9, v1, -0x7

    add-int/lit8 v10, v1, -0x5

    invoke-virtual {v4, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v9, v1, -0x5

    add-int/lit8 v10, v1, -0x3

    invoke-virtual {v4, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v9, v1, -0x3

    invoke-virtual {v4, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p0, v5, v6}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 588
    .end local v1    # "length":I
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v3    # "playStoreButton":Landroid/preference/Preference;
    .end local v4    # "versionNumber":Ljava/lang/String;
    :catch_0
    move-exception v5

    goto/16 :goto_0
.end method

.method private updateTouchExplorationState()V
    .locals 13
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const v12, 0x7f060035

    .line 322
    const v7, 0x7f060036

    invoke-direct {p0, v7}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    .line 325
    .local v1, "prefTouchExploration":Landroid/preference/CheckBoxPreference;
    if-nez v1, :cond_1

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 330
    .local v6, "resolver":Landroid/content/ContentResolver;
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 331
    .local v5, "res":Landroid/content/res/Resources;
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 332
    .local v2, "prefs":Landroid/content/SharedPreferences;
    const v7, 0x7f0b000b

    invoke-static {v2, v5, v12, v7}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v4

    .line 334
    .local v4, "requestedState":Z
    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    .line 339
    .local v3, "reflectedState":Z
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 340
    invoke-direct {p0, v6}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->isTouchExplorationEnabled(Landroid/content/ContentResolver;)Z

    move-result v0

    .line 348
    .local v0, "actualState":Z
    :goto_1
    if-eq v4, v0, :cond_2

    .line 349
    const/4 v7, 0x3

    const-string v8, "Set touch exploration preference to reflect actual state %b"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {p0, v7, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 351
    invoke-static {v2, v5, v12, v0}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->putBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;IZ)V

    .line 357
    :cond_2
    if-eq v3, v0, :cond_0

    .line 358
    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 342
    .end local v0    # "actualState":Z
    :cond_3
    move v0, v4

    .restart local v0    # "actualState":Z
    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 107
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 109
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mPrefs:Landroid/content/SharedPreferences;

    .line 110
    new-instance v1, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;

    new-instance v2, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$1;

    invoke-direct {v2, p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$1;-><init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V

    invoke-direct {v1, p0, v2}, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;-><init>(Landroid/content/Context;Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$OnDefaultsLoadedListener;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mPrefProxy:Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;

    .line 124
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mPrefProxy:Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->loadOverridesIfNeeded()Z

    .line 126
    const v1, 0x7f040002

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->addPreferencesFromResource(I)V

    .line 128
    const v1, 0x7f060031

    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 130
    .local v0, "prefTreeDebug":Landroid/preference/CheckBoxPreference;
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTreeDebugChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 132
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->fixListSummaries(Landroid/preference/PreferenceGroup;)V

    .line 134
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->assignTutorialIntent()V

    .line 135
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->assignLabelManagerIntent()V

    .line 137
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->checkTouchExplorationSupport()V

    .line 138
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->checkWebScriptsSupport()V

    .line 139
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->checkTelephonySupport()V

    .line 140
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->checkVibrationSupport()V

    .line 141
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->checkProximitySupport()V

    .line 142
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->checkAccelerometerSupport()V

    .line 143
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->checkInstalledBacks()V

    .line 144
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->showTalkBackVersion()V

    .line 145
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 166
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 168
    sget-boolean v0, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_TOUCH_PREF:Z

    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTouchExploreObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mExploreByTouchDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 173
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mExploreByTouchDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTreeDebugDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    .line 177
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTreeDebugDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 179
    :cond_2
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 192
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 193
    const-string v0, "exploreDialogActive"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->createDisableExploreByTouchDialog()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mExploreByTouchDialog:Landroid/app/AlertDialog;

    .line 197
    :cond_0
    const-string v0, "treeDebugDialogActive"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->createEnableTreeDebugDialog()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTreeDebugDialog:Landroid/app/AlertDialog;

    .line 200
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 149
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 151
    sget-boolean v0, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_TOUCH_PREF:Z

    if-eqz v0, :cond_0

    .line 152
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->registerTouchSettingObserver()V

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mExploreByTouchDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 156
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mExploreByTouchDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTreeDebugDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    .line 160
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTreeDebugDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 162
    :cond_2
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 183
    const-string v3, "exploreDialogActive"

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mExploreByTouchDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 185
    const-string v0, "treeDebugDialogActive"

    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTreeDebugDialog:Landroid/app/AlertDialog;

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 187
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 188
    return-void

    :cond_0
    move v0, v2

    .line 183
    goto :goto_0

    :cond_1
    move v1, v2

    .line 185
    goto :goto_1
.end method
