.class Lcom/google/android/marvin/talkback/ProcessorLongHover;
.super Ljava/lang/Object;
.source "ProcessorLongHover.java"

# interfaces
.implements Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/ProcessorLongHover$LongHoverHandler;
    }
.end annotation


# static fields
.field private static final TRIGGER_ACTION:I


# instance fields
.field private final mContext:Lcom/google/android/marvin/talkback/TalkBackService;

.field private final mHandler:Lcom/google/android/marvin/talkback/ProcessorLongHover$LongHoverHandler;

.field private mIsTouchExploring:Z

.field private final mLongHoverRunnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

.field private mPendingLongHoverEvent:Landroid/view/accessibility/AccessibilityEvent;

.field private final mRuleProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

.field private final mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

.field private mWaitingForExit:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const v0, 0x8000

    :goto_0
    sput v0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->TRIGGER_ACTION:I

    return-void

    :cond_0
    const/16 v0, 0x80

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 1
    .param p1, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    new-instance v0, Lcom/google/android/marvin/talkback/ProcessorLongHover$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/ProcessorLongHover$1;-><init>(Lcom/google/android/marvin/talkback/ProcessorLongHover;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mLongHoverRunnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .line 64
    iput-object p1, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 65
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    .line 67
    invoke-static {}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->getInstance()Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mRuleProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    .line 69
    new-instance v0, Lcom/google/android/marvin/talkback/ProcessorLongHover$LongHoverHandler;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/ProcessorLongHover$LongHoverHandler;-><init>(Lcom/google/android/marvin/talkback/ProcessorLongHover;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mHandler:Lcom/google/android/marvin/talkback/ProcessorLongHover$LongHoverHandler;

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/ProcessorLongHover;)Landroid/view/accessibility/AccessibilityEvent;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/ProcessorLongHover;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mPendingLongHoverEvent:Landroid/view/accessibility/AccessibilityEvent;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/ProcessorLongHover;)Lcom/google/android/marvin/talkback/ProcessorLongHover$LongHoverHandler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/ProcessorLongHover;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mHandler:Lcom/google/android/marvin/talkback/ProcessorLongHover$LongHoverHandler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/ProcessorLongHover;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/ProcessorLongHover;
    .param p1, "x1"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorLongHover;->speakLongHover(Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method private cacheEnteredNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 1
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mWaitingForExit:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mWaitingForExit:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mWaitingForExit:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 157
    :cond_0
    if-eqz p1, :cond_1

    .line 158
    invoke-static {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mWaitingForExit:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 160
    :cond_1
    return-void
.end method

.method private cancelLongHover()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mHandler:Lcom/google/android/marvin/talkback/ProcessorLongHover$LongHoverHandler;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/ProcessorLongHover$LongHoverHandler;->cancelLongHoverTimeout()V

    .line 186
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mPendingLongHoverEvent:Landroid/view/accessibility/AccessibilityEvent;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mLongHoverRunnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/SpeechController;->removeUtteranceCompleteAction(Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    .line 189
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mPendingLongHoverEvent:Landroid/view/accessibility/AccessibilityEvent;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mPendingLongHoverEvent:Landroid/view/accessibility/AccessibilityEvent;

    .line 192
    :cond_0
    return-void
.end method

.method private postLongHoverRunnable(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 170
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorLongHover;->cancelLongHover()V

    .line 172
    invoke-static {p1}, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->obtain(Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mPendingLongHoverEvent:Landroid/view/accessibility/AccessibilityEvent;

    .line 175
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/SpeechController;->peekNextUtteranceId()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mLongHoverRunnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/marvin/talkback/SpeechController;->addUtteranceCompleteAction(ILcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    .line 177
    return-void
.end method

.method private speakLongHover(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 7
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v6, 0x2

    .line 120
    invoke-static {}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->isTutorialActive()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 121
    const-string v3, "Dropping long hover hint speech because tutorial is active."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0, v6, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    new-instance v0, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v0, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 127
    .local v0, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 129
    .local v1, "source":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v1, :cond_0

    .line 136
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v3

    const/16 v4, 0x80

    if-ne v3, v4, :cond_2

    .line 137
    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v3, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->findFocusFromHover(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 138
    if-eqz v1, :cond_0

    .line 143
    :cond_2
    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mRuleProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    invoke-virtual {v3, v1}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->getHintForNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 144
    .local v2, "text":Ljava/lang/CharSequence;
    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 147
    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v3, v2, v4, v6, v5}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/16 v4, 0x100

    .line 74
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    .line 76
    .local v0, "eventType":I
    const/16 v3, 0x80

    if-eq v0, v3, :cond_0

    const/16 v3, 0x200

    if-ne v0, v3, :cond_1

    .line 78
    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mIsTouchExploring:Z

    .line 81
    :cond_1
    const/16 v3, 0x400

    if-ne v0, v3, :cond_3

    .line 82
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mIsTouchExploring:Z

    .line 83
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/ProcessorLongHover;->cacheEnteredNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 84
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorLongHover;->cancelLongHover()V

    .line 110
    :cond_2
    :goto_0
    return-void

    .line 88
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mIsTouchExploring:Z

    if-eqz v3, :cond_2

    sget v3, Lcom/google/android/marvin/talkback/ProcessorLongHover;->TRIGGER_ACTION:I

    if-eq v0, v3, :cond_4

    if-ne v0, v4, :cond_2

    .line 93
    :cond_4
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v1, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 94
    .local v1, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    .line 96
    .local v2, "source":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v2, :cond_2

    .line 100
    sget v3, Lcom/google/android/marvin/talkback/ProcessorLongHover;->TRIGGER_ACTION:I

    if-ne v0, v3, :cond_6

    .line 101
    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/ProcessorLongHover;->cacheEnteredNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 102
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorLongHover;->postLongHoverRunnable(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 109
    :cond_5
    :goto_1
    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    .line 103
    :cond_6
    if-ne v0, v4, :cond_5

    .line 104
    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover;->mWaitingForExit:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 105
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorLongHover;->cancelLongHover()V

    goto :goto_1
.end method
