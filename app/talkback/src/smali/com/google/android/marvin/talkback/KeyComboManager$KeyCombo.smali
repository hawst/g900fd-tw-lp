.class Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;
.super Ljava/lang/Object;
.source "KeyComboManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/KeyComboManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "KeyCombo"
.end annotation


# instance fields
.field private final mId:I

.field private final mKeyCode:I

.field private final mMetaMask:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "modifiers"    # I
    .param p3, "keyCode"    # I

    .prologue
    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    iput p1, p0, Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;->mId:I

    .line 184
    iput p2, p0, Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;->mMetaMask:I

    .line 185
    iput p3, p0, Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;->mKeyCode:I

    .line 186
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;

    .prologue
    .line 168
    iget v0, p0, Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;->mId:I

    return v0
.end method


# virtual methods
.method public matches(Landroid/view/KeyEvent;)I
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 189
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 190
    .local v0, "keyCode":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v2

    const v3, 0x71100f

    and-int v1, v2, v3

    .line 193
    .local v1, "metaState":I
    iget v2, p0, Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;->mMetaMask:I

    if-ne v1, v2, :cond_1

    if-ltz v0, :cond_0

    iget v2, p0, Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;->mKeyCode:I

    if-ne v0, v2, :cond_1

    .line 194
    :cond_0
    const/4 v2, 0x2

    .line 204
    :goto_0
    return v2

    .line 198
    :cond_1
    iget v2, p0, Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;->mKeyCode:I

    invoke-static {v2}, Landroid/view/KeyEvent;->isModifierKey(I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;->mMetaMask:I

    and-int/2addr v2, v1

    if-eqz v2, :cond_2

    .line 200
    const/4 v2, 0x1

    goto :goto_0

    .line 204
    :cond_2
    const/4 v2, -0x1

    goto :goto_0
.end method
