.class Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$ExploreByTouchUpdateHelper;
.super Ljava/lang/Object;
.source "TalkBackUpdateHelper.java"

# interfaces
.implements Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ExploreByTouchUpdateHelper"
.end annotation


# instance fields
.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$ExploreByTouchUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 399
    return-void
.end method


# virtual methods
.method public attemptToProcess(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 14
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 421
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v2, 0x20

    if-eq v0, v2, :cond_0

    move v0, v11

    .line 456
    :goto_0
    return v0

    .line 426
    :cond_0
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v6

    .line 427
    .local v6, "eventText":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    if-eqz v6, :cond_1

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move v0, v11

    .line 428
    goto :goto_0

    .line 432
    :cond_2
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$ExploreByTouchUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v2, "enable_explore_by_touch_warning_title"

    invoke-static {v0, v2}, Lcom/googlecode/eyesfree/utils/AutomationUtils;->getInternalString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 434
    .local v10, "title":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    .line 435
    .local v7, "eventTitle":Ljava/lang/CharSequence;
    invoke-static {v10, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v11

    .line 436
    goto :goto_0

    .line 440
    :cond_3
    new-instance v8, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v8, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 441
    .local v8, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {v8}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 442
    .local v1, "root":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-nez v1, :cond_4

    .line 443
    const-string v0, "Missing event source node"

    new-array v2, v11, [Ljava/lang/Object;

    invoke-static {p0, v13, v0, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    move v0, v12

    .line 444
    goto :goto_0

    .line 448
    :cond_4
    const-string v9, "android.widget.Button"

    .line 449
    .local v9, "targetClassName":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$ExploreByTouchUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x104000a

    invoke-virtual {v0, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 450
    .local v3, "targetText":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$ExploreByTouchUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v2, "android.widget.Button"

    const/16 v4, 0x10

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/googlecode/eyesfree/utils/AutomationUtils;->performActionOnView(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/CharSequence;Ljava/lang/String;ILandroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 452
    const-string v0, "Failed to click the button"

    new-array v2, v11, [Ljava/lang/Object;

    invoke-static {p0, v13, v0, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    move v0, v12

    .line 453
    goto :goto_0

    :cond_5
    move v0, v12

    .line 456
    goto :goto_0
.end method

.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 405
    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$ExploreByTouchUpdateHelper;->attemptToProcess(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$ExploreByTouchUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackService;->postRemoveEventListener(Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;)V

    .line 408
    :cond_0
    return-void
.end method
