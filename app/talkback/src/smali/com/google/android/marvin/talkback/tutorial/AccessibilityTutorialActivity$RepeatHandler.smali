.class Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$RepeatHandler;
.super Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;
.source "AccessibilityTutorialActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RepeatHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/utils/WeakReferenceHandler",
        "<",
        "Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V
    .locals 0
    .param p1, "parent"    # Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    .prologue
    .line 538
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;-><init>(Ljava/lang/Object;)V

    .line 539
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;
    .param p2, "parent"    # Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    .prologue
    .line 543
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 544
    # invokes: Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->repeatInstruction()V
    invoke-static {p2}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->access$100(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V

    .line 546
    :cond_0
    return-void
.end method

.method public bridge synthetic handleMessage(Landroid/os/Message;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Message;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 534
    check-cast p2, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity$RepeatHandler;->handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V

    return-void
.end method
