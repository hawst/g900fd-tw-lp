.class Lcom/google/android/marvin/utils/ProximitySensor$2;
.super Ljava/lang/Object;
.source "ProximitySensor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/utils/ProximitySensor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/utils/ProximitySensor;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/utils/ProximitySensor;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/android/marvin/utils/ProximitySensor$2;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 150
    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor$2;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    # setter for: Lcom/google/android/marvin/utils/ProximitySensor;->mShouldDropEvents:Z
    invoke-static {v0, v6}, Lcom/google/android/marvin/utils/ProximitySensor;->access$002(Lcom/google/android/marvin/utils/ProximitySensor;Z)Z

    .line 151
    const/4 v0, 0x2

    const-string v1, "Stopped filtering proximity events at %d."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 153
    return-void
.end method
