.class public Lcom/google/android/marvin/talkback/menurules/RuleSpannables;
.super Ljava/lang/Object;
.source "RuleSpannables.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/menurules/NodeMenuRule;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/menurules/RuleSpannables$SpannableMenuClickListener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    return-void
.end method


# virtual methods
.method public accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v3, 0x0

    .line 32
    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 33
    .local v1, "text":Ljava/lang/CharSequence;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    instance-of v4, v1, Landroid/text/SpannableString;

    if-eqz v4, :cond_0

    .line 34
    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/SpannableString;

    .line 35
    .local v0, "spannable":Landroid/text/SpannableString;
    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v4

    const-class v5, Landroid/text/style/URLSpan;

    invoke-virtual {v0, v3, v4, v5}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/text/style/URLSpan;

    .line 36
    .local v2, "urlSpans":[Landroid/text/style/URLSpan;
    array-length v4, v2

    if-lez v4, :cond_0

    .line 37
    const/4 v3, 0x1

    .line 41
    .end local v0    # "spannable":Landroid/text/SpannableString;
    .end local v2    # "urlSpans":[Landroid/text/style/URLSpan;
    :cond_0
    return v3
.end method

.method public canCollapseMenu()Z
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    return v0
.end method

.method public getMenuItemsForNode(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/List;
    .locals 16
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/marvin/talkback/TalkBackService;",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    invoke-virtual/range {p2 .. p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    check-cast v10, Landroid/text/SpannableString;

    .line 48
    .local v10, "spannable":Landroid/text/SpannableString;
    const/4 v2, 0x0

    invoke-virtual {v10}, Landroid/text/SpannableString;->length()I

    move-result v3

    const-class v5, Landroid/text/style/URLSpan;

    invoke-virtual {v10, v2, v3, v5}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v15

    check-cast v15, [Landroid/text/style/URLSpan;

    .line 49
    .local v15, "urlSpans":[Landroid/text/style/URLSpan;
    new-instance v9, Ljava/util/LinkedList;

    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V

    .line 51
    .local v9, "result":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/googlecode/eyesfree/widget/RadialMenuItem;>;"
    if-eqz v15, :cond_0

    array-length v2, v15

    if-nez v2, :cond_1

    .line 79
    :cond_0
    return-object v9

    .line 55
    :cond_1
    const/4 v8, 0x0

    .line 57
    .local v8, "menuItemId":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v2, v15

    if-ge v4, v2, :cond_0

    .line 58
    aget-object v14, v15, v4

    .line 59
    .local v14, "urlSpan":Landroid/text/style/URLSpan;
    invoke-virtual {v14}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v13

    .line 60
    .local v13, "url":Ljava/lang/String;
    invoke-virtual {v10, v14}, Landroid/text/SpannableString;->getSpanStart(Ljava/lang/Object;)I

    move-result v11

    .line 61
    .local v11, "start":I
    invoke-virtual {v10, v14}, Landroid/text/SpannableString;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    .line 62
    .local v7, "end":I
    invoke-virtual {v10, v11, v7}, Landroid/text/SpannableString;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    .line 63
    .local v6, "label":Ljava/lang/CharSequence;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 57
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 67
    :cond_3
    invoke-static {v13}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    .line 68
    .local v12, "uri":Landroid/net/Uri;
    invoke-virtual {v12}, Landroid/net/Uri;->isRelative()Z

    move-result v2

    if-nez v2, :cond_2

    .line 73
    new-instance v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 75
    .local v1, "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    new-instance v2, Lcom/google/android/marvin/talkback/menurules/RuleSpannables$SpannableMenuClickListener;

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v12}, Lcom/google/android/marvin/talkback/menurules/RuleSpannables$SpannableMenuClickListener;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 76
    invoke-virtual {v9, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getUserFriendlyMenuName(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    const v0, 0x7f06018d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
