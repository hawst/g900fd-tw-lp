.class Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$3;
.super Ljava/lang/Object;
.source "TouchTutorialModule4.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$3;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGestureAction(Lcom/google/android/marvin/talkback/ShortcutGestureAction;)V
    .locals 2
    .param p1, "gesture"    # Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    .prologue
    const/4 v1, 0x0

    .line 96
    sget-object v0, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->LOCAL_BREAKOUT:Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    if-ne p1, v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$3;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->access$100(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$3;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->mGestureMonitor:Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->access$300(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;)Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor;->setListener(Lcom/google/android/marvin/talkback/tutorial/GestureActionMonitor$GestureActionListener;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$3;->this$0:Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$3$1;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$3$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4$3;)V

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule4;->installTriggerDelayedWithFeedback(Ljava/lang/Runnable;)V

    .line 106
    :cond_0
    return-void
.end method
