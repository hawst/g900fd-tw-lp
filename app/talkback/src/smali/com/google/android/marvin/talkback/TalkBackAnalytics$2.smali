.class Lcom/google/android/marvin/talkback/TalkBackAnalytics$2;
.super Ljava/lang/Object;
.source "TalkBackAnalytics.java"

# interfaces
.implements Lcom/googlecode/eyesfree/labeling/AllLabelsFetchRequest$OnAllLabelsFetchedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/TalkBackAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackAnalytics;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackAnalytics;)V
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics$2;->this$0:Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAllLabelsFetched(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/labeling/Label;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 251
    .local p1, "results":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/labeling/Label;>;"
    if-eqz p1, :cond_0

    .line 252
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics$2;->this$0:Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mTracker:Lcom/google/android/gms/analytics/Tracker;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->access$100(Lcom/google/android/marvin/talkback/TalkBackAnalytics;)Lcom/google/android/gms/analytics/Tracker;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-direct {v2}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;-><init>()V

    const/16 v3, 0x8

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics$2;->this$0:Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mService:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->access$200(Lcom/google/android/marvin/talkback/TalkBackAnalytics;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/GoogleAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/analytics/GoogleAnalytics;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/GoogleAnalytics;->dispatchLocalHits()V

    .line 259
    return-void

    .line 252
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
