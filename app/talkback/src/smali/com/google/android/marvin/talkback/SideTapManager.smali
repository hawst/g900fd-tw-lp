.class public Lcom/google/android/marvin/talkback/SideTapManager;
.super Ljava/lang/Object;
.source "SideTapManager.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/CachedFeedbackController$HapticFeedbackListener;
.implements Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;
.implements Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;


# instance fields
.field private mCallbackContext:Lcom/google/android/marvin/talkback/TalkBackService;

.field private mContext:Lcom/google/android/marvin/talkback/TalkBackService;

.field private mIntegratedTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

.field private mLastHapticTime:J

.field private mLastTouchTime:J


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 4
    .param p1, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    const-wide/16 v0, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-wide v0, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mLastTouchTime:J

    .line 48
    iput-wide v0, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mLastHapticTime:J

    .line 58
    iput-object p1, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 59
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mCallbackContext:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 60
    new-instance v1, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v2, "sensor"

    invoke-virtual {v0, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    invoke-direct {v1, v0}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;-><init>(Landroid/hardware/SensorManager;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mIntegratedTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

    .line 62
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mIntegratedTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

    invoke-virtual {v0, p0}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->addListener(Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector$TapListener;)V

    .line 63
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mIntegratedTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

    const-wide/32 v2, 0xf4240

    invoke-virtual {v0, v2, v3}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->setPostDelayTimeMillis(J)V

    .line 64
    return-void
.end method


# virtual methods
.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 139
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/high16 v1, 0x100000

    if-ne v0, v1, :cond_0

    .line 140
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mLastTouchTime:J

    .line 142
    :cond_0
    return-void
.end method

.method public onDoubleTap(J)V
    .locals 11
    .param p1, "timeStamp"    # J

    .prologue
    const-wide/32 v8, 0x3b9aca00

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 161
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v0

    .line 162
    .local v0, "talkBackActive":Z
    iget-wide v6, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mLastTouchTime:J

    sub-long v6, p1, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-lez v5, :cond_1

    move v2, v3

    .line 164
    .local v2, "tapIsntFromScreenTouch":Z
    :goto_0
    iget-wide v6, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mLastHapticTime:J

    sub-long v6, p1, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-lez v5, :cond_2

    move v1, v3

    .line 166
    .local v1, "tapIsntFromHaptic":Z
    :goto_1
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 167
    iget-object v3, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mCallbackContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const v4, 0x7f06004e

    const v5, 0x7f060079

    invoke-virtual {v3, v4, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    .line 170
    :cond_0
    return-void

    .end local v1    # "tapIsntFromHaptic":Z
    .end local v2    # "tapIsntFromScreenTouch":Z
    :cond_1
    move v2, v4

    .line 162
    goto :goto_0

    .restart local v2    # "tapIsntFromScreenTouch":Z
    :cond_2
    move v1, v4

    .line 164
    goto :goto_1
.end method

.method public onHapticFeedbackStarting(J)V
    .locals 1
    .param p1, "currentNanoTime"    # J

    .prologue
    .line 177
    iput-wide p1, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mLastHapticTime:J

    .line 178
    return-void
.end method

.method public onReloadPreferences()V
    .locals 10

    .prologue
    const v5, 0x7f060058

    const-wide v8, 0x3fc3333333333333L    # 0.15

    const v7, 0x7f06007a

    const v6, 0x7f06004f

    .line 77
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 79
    .local v1, "settings":Landroid/content/SharedPreferences;
    const/4 v0, 0x0

    .line 80
    .local v0, "enableTapDetection":Z
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f06004d

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const v4, 0x7f060078

    invoke-virtual {v3, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 84
    const/4 v0, 0x1

    .line 87
    :cond_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f06004e

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const v4, 0x7f060079

    invoke-virtual {v3, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 91
    const/4 v0, 0x1

    .line 92
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mIntegratedTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

    const-wide/32 v4, 0x1dcd6500

    invoke-virtual {v2, v4, v5}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->setMaxDoubleTapSpacingNanos(J)V

    .line 98
    :goto_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const v4, 0x7f060065

    invoke-virtual {v3, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 101
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mIntegratedTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

    const-wide v4, 0x3fe4cccccccccccdL    # 0.65

    invoke-virtual {v2, v4, v5}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->setTapDetectionQuality(D)V

    .line 105
    :cond_1
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const v4, 0x7f060064

    invoke-virtual {v3, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 108
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mIntegratedTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    invoke-virtual {v2, v4, v5}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->setTapDetectionQuality(D)V

    .line 111
    :cond_2
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const v4, 0x7f060063

    invoke-virtual {v3, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 114
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mIntegratedTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

    const-wide v4, 0x3fd3333333333333L    # 0.3

    invoke-virtual {v2, v4, v5}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->setTapDetectionQuality(D)V

    .line 118
    :cond_3
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const v4, 0x7f060062

    invoke-virtual {v3, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 121
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mIntegratedTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

    invoke-virtual {v2, v8, v9}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->setTapDetectionQuality(D)V

    .line 125
    :cond_4
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mIntegratedTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

    invoke-virtual {v2, v8, v9}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->setDoubleTapDetectionQuality(D)V

    .line 128
    if-eqz v0, :cond_6

    .line 129
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mIntegratedTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->start()V

    .line 133
    :goto_1
    return-void

    .line 94
    :cond_5
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mIntegratedTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->setMaxDoubleTapSpacingNanos(J)V

    goto/16 :goto_0

    .line 131
    :cond_6
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mIntegratedTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->stop()V

    goto :goto_1
.end method

.method public onSingleTap(J)V
    .locals 11
    .param p1, "timeStamp"    # J

    .prologue
    const-wide/32 v8, 0x3b9aca00

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 147
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v0

    .line 148
    .local v0, "talkBackActive":Z
    iget-wide v6, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mLastTouchTime:J

    sub-long v6, p1, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-lez v5, :cond_1

    move v2, v3

    .line 150
    .local v2, "tapIsntFromScreenTouch":Z
    :goto_0
    iget-wide v6, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mLastHapticTime:J

    sub-long v6, p1, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-lez v5, :cond_2

    move v1, v3

    .line 152
    .local v1, "tapIsntFromHaptic":Z
    :goto_1
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 153
    iget-object v3, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mCallbackContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const v4, 0x7f06004d

    const v5, 0x7f060078

    invoke-virtual {v3, v4, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    .line 156
    :cond_0
    return-void

    .end local v1    # "tapIsntFromHaptic":Z
    .end local v2    # "tapIsntFromScreenTouch":Z
    :cond_1
    move v2, v4

    .line 148
    goto :goto_0

    .restart local v2    # "tapIsntFromScreenTouch":Z
    :cond_2
    move v1, v4

    .line 150
    goto :goto_1
.end method

.method public onSuspendInfrastructure()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mIntegratedTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;->stop()V

    .line 71
    return-void
.end method

.method setCallbackContext(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0
    .param p1, "callbackContext"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mCallbackContext:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 189
    return-void
.end method

.method setIntegratedTapDetector(Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;)V
    .locals 0
    .param p1, "itd"    # Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/marvin/talkback/SideTapManager;->mIntegratedTapDetector:Lcom/googlecode/eyesfree/picidae/IntegratedTapDetector;

    .line 198
    return-void
.end method
