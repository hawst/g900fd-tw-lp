.class public Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;
.super Ljava/lang/Object;
.source "NodeSpeechRuleProcessor.java"


# static fields
.field private static final COMPARATOR:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;

.field private static final mRuleSwitch:Lcom/google/android/marvin/talkback/speechrules/RuleSwitch;

.field private static final mRules:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;",
            ">;"
        }
    .end annotation
.end field

.field private static sInstance:Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 45
    new-instance v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->COMPARATOR:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;

    .line 47
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    .line 48
    new-instance v0, Lcom/google/android/marvin/talkback/speechrules/RuleSwitch;

    invoke-direct {v0}, Lcom/google/android/marvin/talkback/speechrules/RuleSwitch;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRuleSwitch:Lcom/google/android/marvin/talkback/speechrules/RuleSwitch;

    .line 55
    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleHintTemplate;

    const-class v2, Landroid/widget/Spinner;

    const v3, 0x7f0600b9

    const v4, 0x7f0600d0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleHintTemplate;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 57
    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    sget-object v1, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRuleSwitch:Lcom/google/android/marvin/talkback/speechrules/RuleSwitch;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 58
    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 59
    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;

    const-class v2, Landroid/widget/RadioButton;

    const v3, 0x7f0600bd

    invoke-direct {v1, v2, v3}, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;-><init>(Ljava/lang/Class;I)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 61
    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;

    const-class v2, Landroid/widget/CompoundButton;

    const v3, 0x7f0600bc

    invoke-direct {v1, v2, v3}, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;-><init>(Ljava/lang/Class;I)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 63
    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;

    const-class v2, Landroid/widget/Button;

    const v3, 0x7f0600af

    invoke-direct {v1, v2, v3}, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;-><init>(Ljava/lang/Class;I)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 65
    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleEditText;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/speechrules/RuleEditText;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 66
    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleSeekBar;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/speechrules/RuleSeekBar;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 67
    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleContainer;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/speechrules/RuleContainer;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 68
    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleCollection;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/speechrules/RuleCollection;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 70
    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleViewGroup;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/speechrules/RuleViewGroup;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 73
    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleDefault;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/speechrules/RuleDefault;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 74
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    .line 93
    return-void
.end method

.method private appendDescriptionForTree(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/text/SpannableStringBuilder;Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 8
    .param p1, "announcedNode"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "builder"    # Landroid/text/SpannableStringBuilder;
    .param p3, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p4, "source"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 140
    if-nez p1, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    invoke-virtual {p1, p4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    move-object v5, p3

    .line 146
    .local v5, "nodeEvent":Landroid/view/accessibility/AccessibilityEvent;
    :goto_1
    invoke-virtual {p0, p1, v5}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->getDescriptionForNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 147
    .local v4, "nodeDesc":Ljava/lang/CharSequence;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 148
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/CharSequence;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    invoke-static {p2, v6}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 150
    invoke-direct {p0, p1, p3, p2}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->appendMetadataToBuilder(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;Landroid/text/SpannableStringBuilder;)V

    .line 153
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    .line 154
    .local v0, "announcedDescription":Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 160
    .end local v0    # "announcedDescription":Ljava/lang/CharSequence;
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->getSortedChildren(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/TreeSet;

    move-result-object v2

    .line 161
    .local v2, "children":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;>;"
    invoke-virtual {v2}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 162
    .local v1, "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isVisibleOrLegacy(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    invoke-static {v6, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isAccessibilityFocusable(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 164
    invoke-direct {p0, v1, p2, p3, p4}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->appendDescriptionForTree(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/text/SpannableStringBuilder;Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_2

    .line 145
    .end local v1    # "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .end local v2    # "children":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "nodeDesc":Ljava/lang/CharSequence;
    .end local v5    # "nodeEvent":Landroid/view/accessibility/AccessibilityEvent;
    :cond_4
    const/4 v5, 0x0

    goto :goto_1

    .line 168
    .restart local v2    # "children":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "nodeDesc":Ljava/lang/CharSequence;
    .restart local v5    # "nodeEvent":Landroid/view/accessibility/AccessibilityEvent;
    :cond_5
    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method private appendMetadataToBuilder(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;Landroid/text/SpannableStringBuilder;)V
    .locals 4
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p3, "descriptionBuilder"    # Landroid/text/SpannableStringBuilder;

    .prologue
    .line 269
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isCheckable()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRuleSwitch:Lcom/google/android/marvin/talkback/speechrules/RuleSwitch;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, p1, p2}, Lcom/google/android/marvin/talkback/speechrules/RuleSwitch;->accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 270
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    const v0, 0x7f0600c7

    .line 271
    .local v0, "res":I
    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p3, v1}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 273
    .end local v0    # "res":I
    :cond_0
    return-void

    .line 270
    :cond_1
    const v0, 0x7f0600c8

    goto :goto_0
.end method

.method private appendRootMetadataToBuilder(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/text/SpannableStringBuilder;)V
    .locals 4
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "descriptionBuilder"    # Landroid/text/SpannableStringBuilder;

    .prologue
    .line 246
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isActionableForAccessibility(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 247
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    const v3, 0x7f0600c9

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p2, v0}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 257
    :cond_0
    return-void
.end method

.method private formatTextWithLabel(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/text/SpannableStringBuilder;)V
    .locals 7
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "builder"    # Landroid/text/SpannableStringBuilder;

    .prologue
    const/4 v3, 0x0

    .line 218
    invoke-static {p1}, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityNodeInfoCompatUtils;->getLabeledBy(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 220
    .local v1, "labelNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-nez v1, :cond_1

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 225
    .local v0, "labelDescription":Landroid/text/SpannableStringBuilder;
    invoke-direct {p0, v1, v0, v3, v3}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->appendDescriptionForTree(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/text/SpannableStringBuilder;Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 226
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 230
    iget-object v3, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    const v4, 0x7f0600ab

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    const/4 v6, 0x1

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 234
    .local v2, "labeled":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 235
    invoke-virtual {p2, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method public static getInstance()Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;
    .locals 2

    .prologue
    .line 81
    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->sInstance:Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "NodeSpeechRuleProcessor not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->sInstance:Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    return-object v0
.end method

.method private getSortedChildren(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/TreeSet;
    .locals 4
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ")",
            "Ljava/util/TreeSet",
            "<",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176
    new-instance v1, Ljava/util/TreeSet;

    sget-object v3, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->COMPARATOR:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;

    invoke-direct {v1, v3}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 179
    .local v1, "children":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 180
    invoke-virtual {p1, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 181
    .local v0, "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-nez v0, :cond_0

    .line 179
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 185
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 188
    .end local v0    # "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_1
    return-object v1
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 77
    new-instance v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->sInstance:Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    .line 78
    return-void
.end method


# virtual methods
.method public getDescriptionForNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 6
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 202
    sget-object v2, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;

    .line 203
    .local v1, "rule":Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;
    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    invoke-interface {v1, v2, p1, p2}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;->accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 204
    const/4 v2, 0x2

    const-string v3, "Processing node using %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 205
    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    invoke-interface {v1, v2, p1, p2}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;->format(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 209
    .end local v1    # "rule":Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getDescriptionForTree(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "announcedNode"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p3, "source"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 107
    if-nez p1, :cond_0

    .line 108
    const/4 v0, 0x0

    .line 117
    :goto_0
    return-object v0

    .line 111
    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 113
    .local v0, "builder":Landroid/text/SpannableStringBuilder;
    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->appendDescriptionForTree(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/text/SpannableStringBuilder;Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 114
    invoke-direct {p0, p1, v0}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->formatTextWithLabel(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/text/SpannableStringBuilder;)V

    .line 115
    invoke-direct {p0, p1, v0}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->appendRootMetadataToBuilder(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/text/SpannableStringBuilder;)V

    goto :goto_0
.end method

.method public getHintForNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;
    .locals 6
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v2, 0x0

    .line 127
    sget-object v3, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;

    .line 128
    .local v1, "rule":Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;
    instance-of v3, v1, Lcom/google/android/marvin/talkback/speechrules/NodeHintRule;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    invoke-interface {v1, v3, p1, v2}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;->accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 129
    const/4 v2, 0x2

    const-string v3, "Processing node hint using %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 130
    check-cast v1, Lcom/google/android/marvin/talkback/speechrules/NodeHintRule;

    .end local v1    # "rule":Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;
    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    invoke-interface {v1, v2, p1}, Lcom/google/android/marvin/talkback/speechrules/NodeHintRule;->getHintText(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 134
    :cond_1
    return-object v2
.end method
