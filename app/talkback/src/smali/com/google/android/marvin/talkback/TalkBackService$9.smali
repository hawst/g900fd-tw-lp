.class Lcom/google/android/marvin/talkback/TalkBackService$9;
.super Landroid/content/BroadcastReceiver;
.source "TalkBackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/TalkBackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackService;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0

    .prologue
    .line 1638
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackService$9;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1641
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1643
    .local v0, "action":Ljava/lang/String;
    const-string v3, "performCustomGestureAction"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1644
    const-string v3, "gestureAction"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1645
    .local v2, "gestureActionString":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->safeValueOf(Ljava/lang/String;)Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    move-result-object v1

    .line 1647
    .local v1, "gestureAction":Lcom/google/android/marvin/talkback/ShortcutGestureAction;
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackService$9;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # invokes: Lcom/google/android/marvin/talkback/TalkBackService;->performGestureAction(Lcom/google/android/marvin/talkback/ShortcutGestureAction;)V
    invoke-static {v3, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->access$700(Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/ShortcutGestureAction;)V

    .line 1649
    .end local v1    # "gestureAction":Lcom/google/android/marvin/talkback/ShortcutGestureAction;
    .end local v2    # "gestureActionString":Ljava/lang/String;
    :cond_0
    return-void
.end method
