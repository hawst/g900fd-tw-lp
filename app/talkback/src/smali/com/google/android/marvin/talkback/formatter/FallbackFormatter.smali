.class public Lcom/google/android/marvin/talkback/formatter/FallbackFormatter;
.super Ljava/lang/Object;
.source "FallbackFormatter.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    const v4, 0x7f0a001a

    const v3, 0x7f050005

    .line 29
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getSource()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 32
    .local v0, "source":Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v0, :cond_0

    .line 33
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->recycle()V

    .line 34
    const/4 v2, 0x0

    .line 58
    :goto_0
    return v2

    .line 38
    :cond_0
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 53
    :goto_1
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->getEventTextOrDescription(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 54
    .local v1, "text":Ljava/lang/CharSequence;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 55
    invoke-virtual {p3, v1}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 58
    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    .line 40
    .end local v1    # "text":Ljava/lang/CharSequence;
    :sswitch_0
    invoke-virtual {p3, v4}, Lcom/google/android/marvin/talkback/Utterance;->addHaptic(I)V

    .line 41
    invoke-virtual {p3, v3}, Lcom/google/android/marvin/talkback/Utterance;->addAuditory(I)V

    goto :goto_1

    .line 44
    :sswitch_1
    invoke-virtual {p3, v4}, Lcom/google/android/marvin/talkback/Utterance;->addHaptic(I)V

    .line 45
    invoke-virtual {p3, v3}, Lcom/google/android/marvin/talkback/Utterance;->addAuditory(I)V

    goto :goto_1

    .line 48
    :sswitch_2
    const v2, 0x7f0a0018

    invoke-virtual {p3, v2}, Lcom/google/android/marvin/talkback/Utterance;->addHaptic(I)V

    .line 49
    const v2, 0x7f050004

    invoke-virtual {p3, v2}, Lcom/google/android/marvin/talkback/Utterance;->addAuditory(I)V

    goto :goto_1

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x8 -> :sswitch_0
        0x80 -> :sswitch_2
    .end sparse-switch
.end method
