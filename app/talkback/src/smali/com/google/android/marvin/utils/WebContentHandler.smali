.class public Lcom/google/android/marvin/utils/WebContentHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "WebContentHandler.java"


# instance fields
.field private final mAriaRoleToDesc:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mInputTypeToDesc:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOutputBuilder:Ljava/lang/StringBuilder;

.field private mPostorderTextStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mTagToDesc:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "htmlInputMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p2, "htmlRoleMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p3, "htmlTagMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mInputTypeToDesc:Ljava/util/Map;

    .line 58
    iput-object p2, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mAriaRoleToDesc:Ljava/util/Map;

    .line 59
    iput-object p3, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mTagToDesc:Ljava/util/Map;

    .line 60
    return-void
.end method


# virtual methods
.method public characters([CII)V
    .locals 1
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 141
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 149
    iget-object v1, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mPostorderTextStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 151
    .local v0, "postorderText":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 152
    invoke-virtual {p0}, Lcom/google/android/marvin/utils/WebContentHandler;->fixWhiteSpace()V

    .line 155
    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    return-void
.end method

.method public fixWhiteSpace()V
    .locals 4

    .prologue
    .line 163
    iget-object v2, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 165
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 166
    iget-object v2, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    .line 168
    .local v1, "lastCharacter":C
    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v2

    if-nez v2, :cond_0

    .line 169
    iget-object v2, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    .end local v1    # "lastCharacter":C
    :cond_0
    return-void
.end method

.method public getOutput()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public startDocument()V
    .locals 1

    .prologue
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    .line 65
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mPostorderTextStack:Ljava/util/Stack;

    .line 66
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 14
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "attributes"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/google/android/marvin/utils/WebContentHandler;->fixWhiteSpace()V

    .line 76
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    .line 77
    .local v4, "locale":Ljava/util/Locale;
    const-string v12, "aria-label"

    move-object/from16 v0, p4

    invoke-interface {v0, v12}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 78
    .local v2, "ariaLabel":Ljava/lang/String;
    const-string v12, "alt"

    move-object/from16 v0, p4

    invoke-interface {v0, v12}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 79
    .local v1, "alt":Ljava/lang/String;
    const-string v12, "title"

    move-object/from16 v0, p4

    invoke-interface {v0, v12}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 81
    .local v8, "title":Ljava/lang/String;
    if-eqz v2, :cond_3

    .line 82
    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    :cond_0
    :goto_0
    const-string v12, "role"

    move-object/from16 v0, p4

    invoke-interface {v0, v12}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 95
    .local v5, "role":Ljava/lang/String;
    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mAriaRoleToDesc:Ljava/util/Map;

    invoke-interface {v12, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 96
    .local v6, "roleName":Ljava/lang/String;
    const-string v12, "type"

    move-object/from16 v0, p4

    invoke-interface {v0, v12}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 97
    .local v9, "type":Ljava/lang/String;
    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mTagToDesc:Ljava/util/Map;

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 99
    .local v7, "tagInfo":Ljava/lang/String;
    if-eqz v6, :cond_5

    .line 100
    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mPostorderTextStack:Ljava/util/Stack;

    invoke-virtual {v12, v6}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    :goto_1
    const-string v12, "value"

    move-object/from16 v0, p4

    invoke-interface {v0, v12}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 121
    .local v11, "value":Ljava/lang/String;
    if-eqz v11, :cond_2

    .line 122
    move-object/from16 v3, p3

    .line 124
    .local v3, "elementType":Ljava/lang/String;
    const-string v12, "input"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    if-eqz v9, :cond_1

    .line 125
    move-object v3, v9

    .line 128
    :cond_1
    const-string v12, "checkbox"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "radio"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 129
    invoke-virtual {p0}, Lcom/google/android/marvin/utils/WebContentHandler;->fixWhiteSpace()V

    .line 130
    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    .end local v3    # "elementType":Ljava/lang/String;
    :cond_2
    return-void

    .line 83
    .end local v5    # "role":Ljava/lang/String;
    .end local v6    # "roleName":Ljava/lang/String;
    .end local v7    # "tagInfo":Ljava/lang/String;
    .end local v9    # "type":Ljava/lang/String;
    .end local v11    # "value":Ljava/lang/String;
    :cond_3
    if-eqz v1, :cond_4

    .line 84
    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 85
    :cond_4
    if-eqz v8, :cond_0

    .line 86
    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 101
    .restart local v5    # "role":Ljava/lang/String;
    .restart local v6    # "roleName":Ljava/lang/String;
    .restart local v7    # "tagInfo":Ljava/lang/String;
    .restart local v9    # "type":Ljava/lang/String;
    :cond_5
    const-string v12, "input"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    if-eqz v9, :cond_7

    .line 102
    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mInputTypeToDesc:Ljava/util/Map;

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 104
    .local v10, "typeInfo":Ljava/lang/String;
    if-eqz v10, :cond_6

    .line 105
    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mPostorderTextStack:Ljava/util/Stack;

    invoke-virtual {v12, v10}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 107
    :cond_6
    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mPostorderTextStack:Ljava/util/Stack;

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 109
    .end local v10    # "typeInfo":Ljava/lang/String;
    :cond_7
    if-eqz v7, :cond_8

    .line 110
    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mPostorderTextStack:Ljava/util/Stack;

    invoke-virtual {v12, v7}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 112
    :cond_8
    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mPostorderTextStack:Ljava/util/Stack;

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method
