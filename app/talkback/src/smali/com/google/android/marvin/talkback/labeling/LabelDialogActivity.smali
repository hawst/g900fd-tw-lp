.class public Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;
.super Landroid/app/Activity;
.source "LabelDialogActivity.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field private final mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private final mEditActionListener:Landroid/widget/TextView$OnEditorActionListener;

.field private mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

.field private mPositiveButton:Landroid/widget/Button;

.field private mStartIntent:Landroid/content/Intent;

.field private final mTextValidator:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 291
    new-instance v0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$5;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$5;-><init>(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 298
    new-instance v0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$6;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$6;-><init>(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mEditActionListener:Landroid/widget/TextView$OnEditorActionListener;

    .line 311
    new-instance v0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$7;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$7;-><init>(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mTextValidator:Landroid/text/TextWatcher;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mStartIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;Lcom/googlecode/eyesfree/labeling/Label;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;
    .param p1, "x1"    # Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->showEditLabelDialog(Lcom/googlecode/eyesfree/labeling/Label;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;Lcom/googlecode/eyesfree/labeling/Label;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;
    .param p1, "x1"    # Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->showRemoveLabelDialog(Lcom/googlecode/eyesfree/labeling/Label;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mPositiveButton:Landroid/widget/Button;

    return-object v0
.end method

.method private ensureIntentConsistency(Landroid/content/Intent;)Z
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/high16 v8, -0x8000000000000000L

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 129
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 130
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 132
    .local v1, "extras":Landroid/os/Bundle;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_0
    move v5, v6

    .line 152
    :cond_1
    :goto_0
    return v5

    .line 137
    :cond_2
    const-string v7, "com.google.android.marvin.talkback.labeling.ADD_LABEL"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 139
    const-string v7, "EXTRA_STRING_RESOURCE_NAME"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 140
    .local v4, "resourceName":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .end local v4    # "resourceName":Ljava/lang/String;
    :cond_3
    move v5, v6

    .line 152
    goto :goto_0

    .line 143
    :cond_4
    const-string v7, "com.google.android.marvin.talkback.labeling.EDIT_LABEL"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    const-string v7, "com.google.android.marvin.talkback.labeling.REMOVE_LABEL"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 145
    :cond_5
    const-string v7, "EXTRA_LONG_LABEL_ID"

    invoke-virtual {v1, v7, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 146
    .local v2, "labelId":J
    cmp-long v7, v2, v8

    if-eqz v7, :cond_3

    goto :goto_0
.end method

.method private getApplicationName(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 165
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {v3, p1, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 171
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    :goto_0
    if-eqz v0, :cond_0

    .line 172
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 177
    .local v1, "appLabel":Ljava/lang/CharSequence;
    :goto_1
    return-object v1

    .line 166
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v1    # "appLabel":Ljava/lang/CharSequence;
    :catch_0
    move-exception v2

    .line 167
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v0, 0x0

    .restart local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    goto :goto_0

    .line 174
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "appLabel":Ljava/lang/CharSequence;
    goto :goto_1
.end method

.method private showAddLabelDialog()V
    .locals 8

    .prologue
    .line 181
    const v6, 0x7f090009

    invoke-virtual {p0, v6}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->setTheme(I)V

    .line 182
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 183
    .local v5, "li":Landroid/view/LayoutInflater;
    const v6, 0x7f030001

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 184
    .local v3, "dialogView":Landroid/view/View;
    const v6, 0x7f0d0070

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    .line 185
    .local v4, "editField":Landroid/widget/EditText;
    iget-object v6, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mEditActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v4, v6}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 186
    iget-object v6, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mTextValidator:Landroid/text/TextWatcher;

    invoke-virtual {v4, v6}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 188
    new-instance v1, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$2;

    invoke-direct {v1, p0, v4}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$2;-><init>(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;Landroid/widget/EditText;)V

    .line 202
    .local v1, "buttonClickListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f06017b

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f060178

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x104000a

    invoke-virtual {v6, v7, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const/high16 v7, 0x1040000

    invoke-virtual {v6, v7, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 211
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 212
    .local v2, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 213
    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 215
    const/4 v6, -0x1

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mPositiveButton:Landroid/widget/Button;

    .line 216
    iget-object v6, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mPositiveButton:Landroid/widget/Button;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 217
    return-void
.end method

.method private showEditLabelDialog(Lcom/googlecode/eyesfree/labeling/Label;)V
    .locals 8
    .param p1, "existing"    # Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    .line 220
    const v6, 0x7f090009

    invoke-virtual {p0, v6}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->setTheme(I)V

    .line 221
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 222
    .local v5, "li":Landroid/view/LayoutInflater;
    const v6, 0x7f030001

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 223
    .local v3, "dialogView":Landroid/view/View;
    const v6, 0x7f0d0070

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    .line 224
    .local v4, "editField":Landroid/widget/EditText;
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/labeling/Label;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 225
    iget-object v6, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mEditActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v4, v6}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 226
    iget-object v6, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mTextValidator:Landroid/text/TextWatcher;

    invoke-virtual {v4, v6}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 228
    new-instance v1, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$3;

    invoke-direct {v1, p0, p1, v4}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$3;-><init>(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;Lcom/googlecode/eyesfree/labeling/Label;Landroid/widget/EditText;)V

    .line 241
    .local v1, "buttonClickListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f06017b

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f060179

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x104000a

    invoke-virtual {v6, v7, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const/high16 v7, 0x1040000

    invoke-virtual {v6, v7, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 250
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 251
    .local v2, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 252
    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 254
    const/4 v6, -0x1

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mPositiveButton:Landroid/widget/Button;

    .line 255
    return-void
.end method

.method private showRemoveLabelDialog(Lcom/googlecode/eyesfree/labeling/Label;)V
    .locals 8
    .param p1, "existing"    # Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 258
    const v3, 0x7f090009

    invoke-virtual {p0, v3}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->setTheme(I)V

    .line 259
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/labeling/Label;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->getApplicationName(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 262
    .local v0, "appName":Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 263
    const v3, 0x7f06017c

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/labeling/Label;->getText()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p0, v3, v4}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 269
    .local v2, "message":Ljava/lang/CharSequence;
    :goto_0
    new-instance v1, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$4;

    invoke-direct {v1, p0, p1}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$4;-><init>(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;Lcom/googlecode/eyesfree/labeling/Label;)V

    .line 280
    .local v1, "buttonClickListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f06017a

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x1040013

    invoke-virtual {v3, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x1040009

    invoke-virtual {v3, v4, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 289
    return-void

    .line 265
    .end local v1    # "buttonClickListener":Landroid/content/DialogInterface$OnClickListener;
    .end local v2    # "message":Ljava/lang/CharSequence;
    :cond_0
    const v3, 0x7f06017d

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/labeling/Label;->getText()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    aput-object v0, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "message":Ljava/lang/CharSequence;
    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 79
    new-instance v0, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    invoke-direct {v0, p0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    .line 80
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mStartIntent:Landroid/content/Intent;

    .line 81
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mStartIntent:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->ensureIntentConsistency(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->finish()V

    .line 86
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 90
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 91
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->shutdown()V

    .line 92
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getInstance()Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->resetFocusedNode()V

    .line 93
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 97
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 99
    const-string v0, "com.google.android.marvin.talkback.labeling.ADD_LABEL"

    iget-object v1, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mStartIntent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->showAddLabelDialog()V

    .line 126
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mStartIntent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "EXTRA_LONG_LABEL_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$1;

    invoke-direct {v2, p0}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$1;-><init>(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->getLabelForLabelIdFromDatabase(Ljava/lang/Long;Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest$OnLabelFetchedListener;)V

    goto :goto_0
.end method
