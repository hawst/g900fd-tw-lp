.class public final Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;
.super Ljava/lang/Object;
.source "DayOrWeekOrAgendaViewSelectedFormatter.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# static fields
.field private static CLASS_NAME_AGENDA_VIEW:Ljava/lang/String;

.field public static final CONTENT_URI_CALENDARS:Landroid/net/Uri;

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final SDK_INT:I


# instance fields
.field private final mColorToDisplayNameMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLastDateFragment:Ljava/lang/String;

.field private mLastTimeFragment:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 44
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->SDK_INT:I

    .line 48
    const-string v0, ""

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->CLASS_NAME_AGENDA_VIEW:Ljava/lang/String;

    .line 51
    sget v0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->SDK_INT:I

    packed-switch v0, :pswitch_data_0

    .line 67
    :goto_0
    const-string v0, "content://com.android.calendar/calendars"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->CONTENT_URI_CALENDARS:Landroid/net/Uri;

    .line 83
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "color"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "displayName"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->PROJECTION:[Ljava/lang/String;

    return-void

    .line 53
    :pswitch_0
    const-string v0, "com.android.calendar.AgendaListView"

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->CLASS_NAME_AGENDA_VIEW:Ljava/lang/String;

    goto :goto_0

    .line 56
    :pswitch_1
    const-string v0, "com.android.calendar.agenda.AgendaListView"

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->CLASS_NAME_AGENDA_VIEW:Ljava/lang/String;

    goto :goto_0

    .line 51
    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mColorToDisplayNameMap:Landroid/util/SparseArray;

    return-void
.end method

.method private appendDisplayName(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Landroid/text/SpannableStringBuilder;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p3, "textBuilder"    # Landroid/text/SpannableStringBuilder;

    .prologue
    .line 326
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getParcelableData()Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 327
    .local v2, "parcelableData":Landroid/os/Bundle;
    if-nez v2, :cond_1

    .line 346
    :cond_0
    :goto_0
    return-void

    .line 331
    :cond_1
    const-string v3, "color"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 332
    .local v1, "color":I
    iget-object v3, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mColorToDisplayNameMap:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 333
    .local v0, "accountDescription":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 334
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->reloadColorToDisplayNameMap(Landroid/content/Context;)V

    .line 336
    :cond_2
    iget-object v3, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mColorToDisplayNameMap:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    .line 337
    iget-object v3, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mColorToDisplayNameMap:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "accountDescription":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 338
    .restart local v0    # "accountDescription":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 339
    const v3, 0x7f0601b4

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 340
    const/16 v3, 0x2c

    invoke-virtual {p3, v3}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 341
    const/16 v3, 0x20

    invoke-virtual {p3, v3}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 342
    invoke-virtual {p3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 343
    const/16 v3, 0x2e

    invoke-virtual {p3, v3}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method private appendEventCountAnnouncement(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Landroid/text/SpannableStringBuilder;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p3, "textBuilder"    # Landroid/text/SpannableStringBuilder;

    .prologue
    .line 257
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getCurrentItemIndex()I

    move-result v2

    add-int/lit8 v1, v2, 0x1

    .line 258
    .local v1, "eventIndex":I
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getItemCount()I

    move-result v0

    .line 259
    .local v0, "eventCount":I
    const v2, 0x7f0601b3

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 261
    const/16 v2, 0x20

    invoke-virtual {p3, v2}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 262
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0001

    invoke-virtual {v2, v3, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 264
    return-void
.end method

.method private appendEventText(Landroid/view/accessibility/AccessibilityEvent;Landroid/text/SpannableStringBuilder;)V
    .locals 4
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "textBuilder"    # Landroid/text/SpannableStringBuilder;

    .prologue
    .line 149
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    .line 150
    .local v2, "text":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 151
    .local v1, "subText":Ljava/lang/CharSequence;
    invoke-virtual {p2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 152
    const/16 v3, 0x20

    invoke-virtual {p2, v3}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    .line 154
    .end local v1    # "subText":Ljava/lang/CharSequence;
    :cond_0
    return-void
.end method

.method private appendSelectedEventDetails(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Landroid/text/SpannableStringBuilder;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p3, "textBuilder"    # Landroid/text/SpannableStringBuilder;

    .prologue
    .line 276
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getParcelableData()Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/os/Bundle;

    .line 277
    .local v7, "parcelableData":Landroid/os/Bundle;
    if-nez v7, :cond_1

    .line 315
    :cond_0
    :goto_0
    return-void

    .line 282
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->appendDisplayName(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Landroid/text/SpannableStringBuilder;)V

    .line 285
    const-string v1, "title"

    invoke-virtual {v7, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v9

    .line 286
    .local v9, "title":Ljava/lang/CharSequence;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 287
    const/16 v1, 0x20

    invoke-virtual {p3, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 288
    invoke-virtual {p3, v9}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 289
    const/16 v1, 0x2e

    invoke-virtual {p3, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 293
    :cond_2
    const-string v1, "startMillis"

    invoke-virtual {v7, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 294
    .local v2, "startMillis":J
    const-wide/16 v10, 0x0

    cmp-long v1, v2, v10

    if-lez v1, :cond_4

    .line 295
    const-string v1, "endMillis"

    invoke-virtual {v7, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 296
    .local v4, "endMillis":J
    const v6, 0x81413

    .line 299
    .local v6, "flags":I
    invoke-static {p1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 300
    or-int/lit16 v6, v6, 0x80

    :cond_3
    move-object v1, p1

    .line 302
    invoke-static/range {v1 .. v6}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v8

    .line 303
    .local v8, "timeRange":Ljava/lang/String;
    const/16 v1, 0x20

    invoke-virtual {p3, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 304
    invoke-virtual {p3, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 305
    const/16 v1, 0x2e

    invoke-virtual {p3, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 309
    .end local v4    # "endMillis":J
    .end local v6    # "flags":I
    .end local v8    # "timeRange":Ljava/lang/String;
    :cond_4
    const-string v1, "location"

    invoke-virtual {v7, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 310
    .local v0, "location":Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 311
    const/16 v1, 0x20

    invoke-virtual {p3, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 312
    invoke-virtual {p3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 313
    const/16 v1, 0x2e

    invoke-virtual {p3, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method private appendSelectedEventIndexAnouncement(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Landroid/text/SpannableStringBuilder;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p3, "textBuilder"    # Landroid/text/SpannableStringBuilder;

    .prologue
    const/16 v2, 0x20

    .line 239
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getItemCount()I

    move-result v0

    .line 240
    .local v0, "eventCount":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 241
    invoke-virtual {p3, v2}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 242
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->appendEventCountAnnouncement(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Landroid/text/SpannableStringBuilder;)V

    .line 243
    const/16 v1, 0x2e

    invoke-virtual {p3, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 244
    invoke-virtual {p3, v2}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 246
    :cond_0
    return-void
.end method

.method private appendSelectedRange(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Landroid/text/SpannableStringBuilder;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p3, "textBuilder"    # Landroid/text/SpannableStringBuilder;

    .prologue
    const/16 v10, 0x20

    const/4 v9, 0x0

    const/16 v8, 0x2e

    const/16 v7, 0x2c

    .line 165
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 166
    .local v2, "eventText":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getClassName()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 170
    .local v0, "className":Ljava/lang/String;
    const-string v6, "com.android.calendar.DayView"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 171
    move-object v4, v2

    .line 172
    .local v4, "timeFragment":Ljava/lang/String;
    sget v6, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->SDK_INT:I

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    .line 174
    :pswitch_0
    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mLastTimeFragment:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 175
    iput-object v4, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mLastTimeFragment:Ljava/lang/String;

    .line 176
    invoke-virtual {p3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 177
    invoke-virtual {p3, v8}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    .line 181
    :pswitch_1
    const/4 v1, 0x0

    .line 182
    .local v1, "dateFragment":Ljava/lang/String;
    invoke-virtual {v2, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 183
    .local v3, "firstCommaIndex":I
    const/4 v6, -0x1

    if-le v3, v6, :cond_2

    .line 184
    invoke-virtual {v2, v9, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 185
    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 187
    :cond_2
    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mLastTimeFragment:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 188
    iput-object v4, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mLastTimeFragment:Ljava/lang/String;

    .line 189
    invoke-virtual {p3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 190
    if-nez v1, :cond_4

    .line 191
    invoke-virtual {p3, v8}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 196
    :cond_3
    :goto_1
    if-eqz v1, :cond_0

    .line 197
    invoke-virtual {p3, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 198
    invoke-virtual {p3, v8}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    .line 193
    :cond_4
    invoke-virtual {p3, v7}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    goto :goto_1

    .line 203
    .end local v1    # "dateFragment":Ljava/lang/String;
    .end local v3    # "firstCommaIndex":I
    .end local v4    # "timeFragment":Ljava/lang/String;
    :cond_5
    invoke-virtual {v2, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 204
    .restart local v3    # "firstCommaIndex":I
    invoke-virtual {v2, v9, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 205
    .restart local v4    # "timeFragment":Ljava/lang/String;
    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 206
    .restart local v1    # "dateFragment":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mLastDateFragment:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 207
    iput-object v1, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mLastDateFragment:Ljava/lang/String;

    .line 208
    invoke-virtual {p3, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 209
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getAddedCount()I

    move-result v5

    .line 210
    .local v5, "todayEventCount":I
    if-lez v5, :cond_6

    .line 211
    invoke-virtual {p3, v7}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 212
    invoke-virtual {p3, v10}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 213
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->appendEventCountAnnouncement(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Landroid/text/SpannableStringBuilder;)V

    .line 215
    :cond_6
    invoke-virtual {p3, v8}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 216
    invoke-virtual {p3, v10}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 218
    .end local v5    # "todayEventCount":I
    :cond_7
    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mLastTimeFragment:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 219
    iput-object v4, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mLastTimeFragment:Ljava/lang/String;

    .line 220
    invoke-virtual {p3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 221
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getItemCount()I

    move-result v6

    if-lez v6, :cond_0

    .line 222
    invoke-virtual {p3, v7}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 223
    invoke-virtual {p3, v10}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    .line 172
    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private formatAgendaViewSelected(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Lcom/google/android/marvin/talkback/Utterance;)V
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    .line 133
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 134
    .local v0, "textBuilder":Landroid/text/SpannableStringBuilder;
    invoke-direct {p0, p2, p1, v0}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->appendDisplayName(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Landroid/text/SpannableStringBuilder;)V

    .line 135
    invoke-direct {p0, p1, v0}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->appendEventText(Landroid/view/accessibility/AccessibilityEvent;Landroid/text/SpannableStringBuilder;)V

    .line 137
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 138
    invoke-virtual {p3, v0}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 140
    :cond_0
    return-void
.end method

.method private formatDayOrWeekViewSelected(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Lcom/google/android/marvin/talkback/Utterance;)V
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    .line 114
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 115
    .local v0, "textBuilder":Landroid/text/SpannableStringBuilder;
    invoke-direct {p0, p2, p1, v0}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->appendSelectedRange(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Landroid/text/SpannableStringBuilder;)V

    .line 116
    invoke-direct {p0, p2, p1, v0}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->appendSelectedEventIndexAnouncement(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Landroid/text/SpannableStringBuilder;)V

    .line 117
    invoke-direct {p0, p2, p1, v0}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->appendSelectedEventDetails(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Landroid/text/SpannableStringBuilder;)V

    .line 119
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 120
    invoke-virtual {p3, v0}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 122
    :cond_0
    return-void
.end method

.method private reloadColorToDisplayNameMap(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 354
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->CONTENT_URI_CALENDARS:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->PROJECTION:[Ljava/lang/String;

    const-string v3, "selected=?"

    new-array v4, v10, [Ljava/lang/String;

    const-string v5, "1"

    aput-object v5, v4, v9

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 358
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_0

    .line 368
    :goto_0
    return-void

    .line 361
    :cond_0
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 362
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    .line 363
    invoke-interface {v7, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 364
    .local v6, "color":I
    invoke-interface {v7, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 365
    .local v8, "dispayName":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mColorToDisplayNameMap:Landroid/util/SparseArray;

    invoke-virtual {v0, v6, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1

    .line 367
    .end local v6    # "color":I
    .end local v8    # "dispayName":Ljava/lang/String;
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    .line 95
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getClassName()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, "className":Ljava/lang/String;
    sget-object v1, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->CLASS_NAME_AGENDA_VIEW:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->formatAgendaViewSelected(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Lcom/google/android/marvin/talkback/Utterance;)V

    .line 102
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 99
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->formatDayOrWeekViewSelected(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Lcom/google/android/marvin/talkback/Utterance;)V

    goto :goto_0
.end method
