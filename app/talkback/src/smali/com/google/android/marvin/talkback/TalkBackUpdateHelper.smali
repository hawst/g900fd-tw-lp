.class Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;
.super Ljava/lang/Object;
.source "TalkBackUpdateHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$ExploreByTouchUpdateHelper;,
        Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$NotificationPosterRunnable;
    }
.end annotation


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mNotificationManager:Landroid/app/NotificationManager;

.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;

.field private final mSharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 2
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mHandler:Landroid/os/Handler;

    .line 71
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 72
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mNotificationManager:Landroid/app/NotificationManager;

    .line 74
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;)Landroid/app/NotificationManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mNotificationManager:Landroid/app/NotificationManager;

    return-object v0
.end method

.method private buildGestureChangeNotification(Landroid/content/Intent;)Landroid/app/Notification;
    .locals 9
    .param p1, "clickIntent"    # Landroid/content/Intent;

    .prologue
    const v8, 0x7f06016d

    const/4 v7, 0x0

    .line 281
    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const/high16 v6, 0x8000000

    invoke-static {v5, v7, p1, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 284
    .local v3, "pendingIntent":Landroid/app/PendingIntent;
    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v5, v8}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 286
    .local v4, "ticker":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v5, v8}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 288
    .local v1, "contentTitle":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v6, 0x7f06016e

    invoke-virtual {v5, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 290
    .local v0, "contentText":Ljava/lang/String;
    new-instance v5, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v6, 0x7f02001c

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    const-wide/16 v6, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 299
    .local v2, "notification":Landroid/app/Notification;
    iget v5, v2, Landroid/app/Notification;->defaults:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v2, Landroid/app/Notification;->defaults:I

    .line 300
    iget v5, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v2, Landroid/app/Notification;->flags:I

    .line 301
    return-object v2
.end method

.method private deprecateStringPreference(Landroid/content/SharedPreferences$Editor;II)V
    .locals 4
    .param p1, "editor"    # Landroid/content/SharedPreferences$Editor;
    .param p2, "resIdPrefKey"    # I
    .param p3, "deprecatedDefaultResId"    # I

    .prologue
    .line 315
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3, p2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 316
    .local v0, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3, p3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 317
    .local v1, "oldDefault":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 319
    .local v2, "userSetOrOldDefault":Ljava/lang/String;
    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 320
    return-void
.end method

.method private displaySettingsAvailableNotification()V
    .locals 11

    .prologue
    const v9, 0x7f06016b

    const/4 v10, 0x1

    .line 201
    new-instance v2, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 202
    .local v2, "launchMarketIntent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v7, 0x7f060080

    invoke-virtual {v6, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 205
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const/4 v7, 0x0

    const/high16 v8, 0x8000000

    invoke-static {v6, v7, v2, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 208
    .local v4, "pendingIntent":Landroid/app/PendingIntent;
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v6, v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 209
    .local v5, "ticker":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v6, v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 210
    .local v1, "contentTitle":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v7, 0x7f06016c

    invoke-virtual {v6, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 211
    .local v0, "contentText":Ljava/lang/String;
    new-instance v6, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f02001c

    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    .line 221
    .local v3, "notification":Landroid/app/Notification;
    iget v6, v3, Landroid/app/Notification;->defaults:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v3, Landroid/app/Notification;->defaults:I

    .line 222
    iget v6, v3, Landroid/app/Notification;->flags:I

    or-int/lit8 v6, v6, 0x12

    iput v6, v3, Landroid/app/Notification;->flags:I

    .line 224
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v6, v10, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 225
    return-void
.end method

.method private needsExploreByTouchHelper(I)Z
    .locals 3
    .param p1, "previousVersion"    # I

    .prologue
    const/4 v0, 0x1

    .line 192
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ne v1, v2, :cond_0

    const/16 v1, 0x44

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "first_time_user"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyUserOfBuiltInGestureChanges()V
    .locals 6

    .prologue
    const/4 v4, 0x3

    .line 263
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-class v3, Lcom/google/android/marvin/talkback/NotificationActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 265
    .local v0, "notificationIntent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 266
    const/high16 v2, 0x800000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 267
    const-string v2, "title"

    const v3, 0x7f06016d

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 269
    const-string v2, "message"

    const v3, 0x7f060180

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 271
    const-string v2, "notificationId"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 274
    new-instance v1, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$NotificationPosterRunnable;

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->buildGestureChangeNotification(Landroid/content/Intent;)Landroid/app/Notification;

    move-result-object v2

    invoke-direct {v1, p0, v2, v4}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$NotificationPosterRunnable;-><init>(Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;Landroid/app/Notification;I)V

    .line 277
    .local v1, "runnable":Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$NotificationPosterRunnable;
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1388

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 278
    return-void
.end method

.method private notifyUserOfGestureChanges()V
    .locals 6

    .prologue
    .line 232
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 235
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const v3, 0x7f060044

    const v4, 0x7f06007e

    invoke-direct {p0, v0, v3, v4}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->deprecateStringPreference(Landroid/content/SharedPreferences$Editor;II)V

    .line 237
    const v3, 0x7f060045

    const v4, 0x7f06007f

    invoke-direct {p0, v0, v3, v4}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->deprecateStringPreference(Landroid/content/SharedPreferences$Editor;II)V

    .line 239
    const v3, 0x7f060042

    const v4, 0x7f06007c

    invoke-direct {p0, v0, v3, v4}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->deprecateStringPreference(Landroid/content/SharedPreferences$Editor;II)V

    .line 241
    const v3, 0x7f060043

    const v4, 0x7f06007d

    invoke-direct {p0, v0, v3, v4}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->deprecateStringPreference(Landroid/content/SharedPreferences$Editor;II)V

    .line 245
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v4, 0x7f060050

    invoke-virtual {v3, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 248
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 251
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-class v4, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 253
    .local v1, "notificationIntent":Landroid/content/Intent;
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 254
    const/high16 v3, 0x800000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 256
    new-instance v2, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$NotificationPosterRunnable;

    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->buildGestureChangeNotification(Landroid/content/Intent;)Landroid/app/Notification;

    move-result-object v3

    const/4 v4, 0x2

    invoke-direct {v2, p0, v3, v4}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$NotificationPosterRunnable;-><init>(Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;Landroid/app/Notification;I)V

    .line 258
    .local v2, "runnable":Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$NotificationPosterRunnable;
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1388

    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 259
    return-void
.end method

.method private remapContinuousReadingMenu()V
    .locals 11

    .prologue
    .line 327
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 328
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v8, "READ_ALL_BREAKOUT"

    .line 329
    .local v8, "targetValue":Ljava/lang/String;
    const-string v7, "LOCAL_BREAKOUT"

    .line 330
    .local v7, "replaceValue":Ljava/lang/String;
    const/16 v9, 0x8

    new-array v2, v9, [I

    fill-array-data v2, :array_0

    .line 340
    .local v2, "gestureKeys":[I
    move-object v0, v2

    .local v0, "arr$":[I
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v5, :cond_1

    aget v4, v0, v3

    .line 341
    .local v4, "key":I
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v9, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 342
    .local v6, "prefKey":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v10, ""

    invoke-interface {v9, v6, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "READ_ALL_BREAKOUT"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 343
    const-string v9, "LOCAL_BREAKOUT"

    invoke-interface {v1, v6, v9}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 340
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 347
    .end local v4    # "key":I
    .end local v6    # "prefKey":Ljava/lang/String;
    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 348
    return-void

    .line 330
    :array_0
    .array-data 4
        0x7f060044
        0x7f060045
        0x7f060048
        0x7f060049
        0x7f060046
        0x7f060047
        0x7f060042
        0x7f060043
    .end array-data
.end method

.method private remapShakeToReadPref()V
    .locals 6

    .prologue
    const v5, 0x7f060039

    .line 357
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b000e

    invoke-static {v2, v3, v5, v4}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v1

    .line 361
    .local v1, "oldPrefOn":Z
    if-eqz v1, :cond_0

    .line 362
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 363
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f06003a

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v4, 0x7f060054

    invoke-virtual {v3, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 365
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 366
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 368
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method


# virtual methods
.method public checkUpdate()V
    .locals 13

    .prologue
    const/16 v12, 0x10

    const/4 v11, -0x1

    const/4 v10, 0x0

    .line 99
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v9, "app_version"

    invoke-interface {v8, v9, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 101
    .local v7, "previousVersion":I
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v8}, Lcom/google/android/marvin/talkback/TalkBackService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 105
    .local v4, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v8}, Lcom/google/android/marvin/talkback/TalkBackService;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v4, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 106
    .local v3, "packageInfo":Landroid/content/pm/PackageInfo;
    iget v0, v3, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    .local v0, "currentVersion":I
    if-ne v7, v0, :cond_0

    .line 177
    .end local v0    # "currentVersion":I
    .end local v3    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return-void

    .line 107
    :catch_0
    move-exception v1

    .line 108
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 116
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0    # "currentVersion":I
    .restart local v3    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 117
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v8, "app_version"

    invoke-interface {v2, v8, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 120
    const/16 v8, 0xc

    if-ge v7, v8, :cond_1

    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0xe

    if-ge v8, v9, :cond_1

    .line 123
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v9, "com.marvin.preferences"

    invoke-static {v8, v9}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->hasPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 124
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->displaySettingsAvailableNotification()V

    .line 129
    :cond_1
    const/16 v8, 0x2a

    if-ge v7, v8, :cond_3

    .line 131
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v9, "com.google.android.marvin.kickback"

    invoke-static {v8, v9}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->hasPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 132
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v9, 0x7f060027

    invoke-virtual {v8, v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 133
    .local v6, "prefVibration":Ljava/lang/String;
    invoke-interface {v2, v6, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 137
    .end local v6    # "prefVibration":Ljava/lang/String;
    :cond_2
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v9, "com.google.android.marvin.soundback"

    invoke-static {v8, v9}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->hasPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 138
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v9, 0x7f060028

    invoke-virtual {v8, v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 139
    .local v5, "prefSoundback":Ljava/lang/String;
    invoke-interface {v2, v5, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 144
    .end local v5    # "prefSoundback":Ljava/lang/String;
    :cond_3
    invoke-direct {p0, v7}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->needsExploreByTouchHelper(I)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 145
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    new-instance v9, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$ExploreByTouchUpdateHelper;

    iget-object v10, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {v9, v10}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$ExploreByTouchUpdateHelper;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    invoke-virtual {v8, v9}, Lcom/google/android/marvin/talkback/TalkBackService;->addEventListener(Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;)V

    .line 149
    :cond_4
    const/16 v8, 0x44

    if-lt v7, v8, :cond_5

    const/16 v8, 0x4a

    if-ge v7, v8, :cond_5

    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v8, v12, :cond_5

    .line 151
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->notifyUserOfGestureChanges()V

    .line 157
    :cond_5
    const/16 v8, 0x54

    if-ge v7, v8, :cond_6

    .line 158
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->remapContinuousReadingMenu()V

    .line 164
    :cond_6
    const/16 v8, 0x5a

    if-ge v7, v8, :cond_7

    .line 165
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->remapShakeToReadPref()V

    .line 171
    :cond_7
    if-eq v7, v11, :cond_8

    const/16 v8, 0x61

    if-ge v7, v8, :cond_8

    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v8, v12, :cond_8

    .line 173
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->notifyUserOfBuiltInGestureChanges()V

    .line 176
    :cond_8
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0
.end method

.method public showPendingNotifications()V
    .locals 6

    .prologue
    .line 81
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v5, 0x7f060050

    invoke-virtual {v4, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 84
    .local v2, "userMustAcceptGestureChange":Z
    if-eqz v2, :cond_0

    .line 86
    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-class v4, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 88
    .local v0, "notificationIntent":Landroid/content/Intent;
    const/high16 v3, 0x10000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 89
    const/high16 v3, 0x800000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 91
    new-instance v1, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$NotificationPosterRunnable;

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->buildGestureChangeNotification(Landroid/content/Intent;)Landroid/app/Notification;

    move-result-object v3

    const/4 v4, 0x2

    invoke-direct {v1, p0, v3, v4}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$NotificationPosterRunnable;-><init>(Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;Landroid/app/Notification;I)V

    .line 94
    .local v1, "runnable":Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$NotificationPosterRunnable;
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1388

    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 96
    .end local v0    # "notificationIntent":Landroid/content/Intent;
    .end local v1    # "runnable":Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$NotificationPosterRunnable;
    :cond_0
    return-void
.end method
