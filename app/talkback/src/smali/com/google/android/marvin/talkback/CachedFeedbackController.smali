.class public Lcom/google/android/marvin/talkback/CachedFeedbackController;
.super Ljava/lang/Object;
.source "CachedFeedbackController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/CachedFeedbackController$HapticFeedbackListener;
    }
.end annotation


# instance fields
.field private mAuditoryEnabled:Z

.field private final mContext:Landroid/content/Context;

.field private mHapticEnabled:Z

.field private final mHapticFeedbackListeners:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/marvin/talkback/CachedFeedbackController$HapticFeedbackListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mPreferenceListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private final mResources:Landroid/content/res/Resources;

.field private final mSoundIds:Landroid/util/SparseIntArray;

.field private final mSoundPool:Landroid/media/SoundPool;

.field private final mUseCompatKickBack:Z

.field private final mUseCompatSoundBack:Z

.field private final mVibrator:Landroid/os/Vibrator;

.field private mVolumeAdjustment:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mSoundIds:Landroid/util/SparseIntArray;

    .line 72
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mHapticFeedbackListeners:Ljava/util/Collection;

    .line 76
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mVolumeAdjustment:F

    .line 357
    new-instance v1, Lcom/google/android/marvin/talkback/CachedFeedbackController$2;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/CachedFeedbackController$2;-><init>(Lcom/google/android/marvin/talkback/CachedFeedbackController;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mPreferenceListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 85
    iput-object p1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mContext:Landroid/content/Context;

    .line 86
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mResources:Landroid/content/res/Resources;

    .line 87
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x14

    if-le v1, v2, :cond_0

    .line 88
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->createSoundPoolApi21()Landroid/media/SoundPool;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mSoundPool:Landroid/media/SoundPool;

    .line 92
    :goto_0
    const-string v1, "vibrator"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    iput-object v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mVibrator:Landroid/os/Vibrator;

    .line 95
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->shouldUseCompatKickBack()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mUseCompatKickBack:Z

    .line 96
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->shouldUseCompatSoundBack()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mUseCompatSoundBack:Z

    .line 98
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 99
    .local v0, "prefs":Landroid/content/SharedPreferences;
    iget-object v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mPreferenceListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 100
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->updatePreferences(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 101
    return-void

    .line 90
    .end local v0    # "prefs":Landroid/content/SharedPreferences;
    :cond_0
    new-instance v1, Landroid/media/SoundPool;

    const/16 v2, 0xa

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mSoundPool:Landroid/media/SoundPool;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/CachedFeedbackController;Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/CachedFeedbackController;
    .param p1, "x1"    # Landroid/content/SharedPreferences;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->updatePreferences(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-void
.end method

.method private createSoundPoolApi21()Landroid/media/SoundPool;
    .locals 3

    .prologue
    .line 104
    new-instance v1, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v1}, Landroid/media/AudioAttributes$Builder;-><init>()V

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Landroid/media/AudioAttributes$Builder;->setUsage(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/media/AudioAttributes$Builder;->setContentType(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v0

    .line 108
    .local v0, "aa":Landroid/media/AudioAttributes;
    new-instance v1, Landroid/media/SoundPool$Builder;

    invoke-direct {v1}, Landroid/media/SoundPool$Builder;-><init>()V

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/media/SoundPool$Builder;->setMaxStreams(I)Landroid/media/SoundPool$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/media/SoundPool$Builder;->setAudioAttributes(Landroid/media/AudioAttributes;)Landroid/media/SoundPool$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/SoundPool$Builder;->build()Landroid/media/SoundPool;

    move-result-object v1

    return-object v1
.end method

.method private shouldUseCompatKickBack()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 329
    iget-object v2, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mContext:Landroid/content/Context;

    const-string v3, "com.google.android.marvin.kickback"

    invoke-static {v2, v3}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->hasPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 339
    :cond_0
    :goto_0
    return v1

    .line 333
    :cond_1
    iget-object v2, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mContext:Landroid/content/Context;

    const-string v3, "com.google.android.marvin.kickback"

    invoke-static {v2, v3}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->getVersionCode(Landroid/content/Context;Ljava/lang/CharSequence;)I

    move-result v0

    .line 335
    .local v0, "kickBackVersionCode":I
    const/4 v2, 0x5

    if-lt v0, v2, :cond_0

    .line 339
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private shouldUseCompatSoundBack()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 343
    iget-object v2, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mContext:Landroid/content/Context;

    const-string v3, "com.google.android.marvin.soundback"

    invoke-static {v2, v3}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->hasPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 353
    :cond_0
    :goto_0
    return v1

    .line 347
    :cond_1
    iget-object v2, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mContext:Landroid/content/Context;

    const-string v3, "com.google.android.marvin.soundback"

    invoke-static {v2, v3}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->getVersionCode(Landroid/content/Context;Ljava/lang/CharSequence;)I

    move-result v0

    .line 349
    .local v0, "kickBackVersionCode":I
    const/4 v2, 0x7

    if-lt v0, v2, :cond_0

    .line 353
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private updateAuditoryFromPreference(Landroid/content/SharedPreferences;)V
    .locals 4
    .param p1, "prefs"    # Landroid/content/SharedPreferences;

    .prologue
    .line 317
    iget-boolean v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mUseCompatSoundBack:Z

    if-eqz v1, :cond_0

    .line 318
    iget-object v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mContext:Landroid/content/Context;

    const-string v2, "com.google.android.marvin.soundback"

    invoke-static {v1, v2}, Lcom/google/android/marvin/utils/SecureSettingsUtils;->isAccessibilityServiceEnabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 325
    .local v0, "enabled":Z
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->setAuditoryEnabled(Z)V

    .line 326
    return-void

    .line 321
    .end local v0    # "enabled":Z
    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f060028

    const v3, 0x7f0b0004

    invoke-static {p1, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v0

    .restart local v0    # "enabled":Z
    goto :goto_0
.end method

.method private updateHapticFromPreference(Landroid/content/SharedPreferences;)V
    .locals 4
    .param p1, "prefs"    # Landroid/content/SharedPreferences;

    .prologue
    .line 303
    iget-boolean v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mUseCompatKickBack:Z

    if-eqz v1, :cond_0

    .line 304
    iget-object v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mContext:Landroid/content/Context;

    const-string v2, "com.google.android.marvin.kickback"

    invoke-static {v1, v2}, Lcom/google/android/marvin/utils/SecureSettingsUtils;->isAccessibilityServiceEnabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 311
    .local v0, "enabled":Z
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->setHapticEnabled(Z)V

    .line 312
    return-void

    .line 307
    .end local v0    # "enabled":Z
    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f060027

    const v3, 0x7f0b0003

    invoke-static {p1, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v0

    .restart local v0    # "enabled":Z
    goto :goto_0
.end method

.method private updatePreferences(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1, "prefs"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 280
    if-nez p2, :cond_1

    .line 281
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->updateHapticFromPreference(Landroid/content/SharedPreferences;)V

    .line 282
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->updateAuditoryFromPreference(Landroid/content/SharedPreferences;)V

    .line 283
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->updateVolumeAdjustmentFromPreference(Landroid/content/SharedPreferences;)V

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 284
    :cond_1
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mContext:Landroid/content/Context;

    const v1, 0x7f060027

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 285
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->updateHapticFromPreference(Landroid/content/SharedPreferences;)V

    goto :goto_0

    .line 286
    :cond_2
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mContext:Landroid/content/Context;

    const v1, 0x7f060028

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 287
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->updateAuditoryFromPreference(Landroid/content/SharedPreferences;)V

    goto :goto_0

    .line 288
    :cond_3
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mContext:Landroid/content/Context;

    const v1, 0x7f06002c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->updateVolumeAdjustmentFromPreference(Landroid/content/SharedPreferences;)V

    goto :goto_0
.end method

.method private updateVolumeAdjustmentFromPreference(Landroid/content/SharedPreferences;)V
    .locals 4
    .param p1, "prefs"    # Landroid/content/SharedPreferences;

    .prologue
    .line 294
    iget-object v1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f06002c

    const v3, 0x7f06006c

    invoke-static {p1, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getIntFromStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)I

    move-result v0

    .line 297
    .local v0, "adjustment":I
    int-to-float v1, v0

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->setVolumeAdjustment(F)V

    .line 298
    return-void
.end method


# virtual methods
.method public interrupt()V
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 261
    return-void
.end method

.method public playAuditory(I)Z
    .locals 2
    .param p1, "resId"    # I

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 211
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v1, v1, v0}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(IFFF)Z

    move-result v0

    return v0
.end method

.method public playAuditory(IFFF)Z
    .locals 9
    .param p1, "resId"    # I
    .param p2, "rate"    # F
    .param p3, "volume"    # F
    .param p4, "pan"    # F

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 225
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mAuditoryEnabled:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 252
    :cond_0
    :goto_0
    return v4

    .line 229
    :cond_1
    sub-float v0, v6, p4

    invoke-static {v6, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    mul-float/2addr v0, p3

    iget v5, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mVolumeAdjustment:F

    mul-float v2, v0, v5

    .line 230
    .local v2, "leftVolume":F
    add-float v0, v6, p4

    invoke-static {v6, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    mul-float/2addr v0, p3

    iget v5, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mVolumeAdjustment:F

    mul-float v3, v0, v5

    .line 232
    .local v3, "rightVolume":F
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mSoundIds:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    .line 233
    .local v1, "soundId":I
    if-eqz v1, :cond_2

    .line 234
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mSoundPool:Landroid/media/SoundPool;

    move v5, v4

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    move-result v7

    .line 235
    .local v7, "streamId":I
    if-eqz v7, :cond_2

    move v4, v8

    .line 236
    goto :goto_0

    .line 242
    .end local v7    # "streamId":I
    :cond_2
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mSoundPool:Landroid/media/SoundPool;

    new-instance v4, Lcom/google/android/marvin/talkback/CachedFeedbackController$1;

    invoke-direct {v4, p0, v2, v3, p2}, Lcom/google/android/marvin/talkback/CachedFeedbackController$1;-><init>(Lcom/google/android/marvin/talkback/CachedFeedbackController;FFF)V

    invoke-virtual {v0, v4}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mSoundIds:Landroid/util/SparseIntArray;

    iget-object v4, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mSoundPool:Landroid/media/SoundPool;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5, p1, v8}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v4

    invoke-virtual {v0, p1, v4}, Landroid/util/SparseIntArray;->put(II)V

    move v4, v8

    .line 252
    goto :goto_0
.end method

.method public playHaptic(I)Z
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 163
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playHaptic(II)Z

    move-result v0

    return v0
.end method

.method public playHaptic(II)Z
    .locals 12
    .param p1, "resId"    # I
    .param p2, "repeatIndex"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 175
    iget-boolean v7, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mHapticEnabled:Z

    if-eqz v7, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move v7, v9

    .line 200
    :goto_0
    return v7

    .line 180
    :cond_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 181
    .local v0, "currentTime":J
    iget-object v7, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mHapticFeedbackListeners:Ljava/util/Collection;

    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 182
    .local v4, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/marvin/talkback/CachedFeedbackController$HapticFeedbackListener;>;"
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 183
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/marvin/talkback/CachedFeedbackController$HapticFeedbackListener;

    invoke-interface {v7, v0, v1}, Lcom/google/android/marvin/talkback/CachedFeedbackController$HapticFeedbackListener;->onHapticFeedbackStarting(J)V

    goto :goto_1

    .line 188
    :cond_2
    :try_start_0
    iget-object v7, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v7, p1}, Landroid/content/res/Resources;->getIntArray(I)[I
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 194
    .local v6, "patternArray":[I
    array-length v7, v6

    new-array v5, v7, [J

    .line 195
    .local v5, "pattern":[J
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    array-length v7, v6

    if-ge v3, v7, :cond_3

    .line 196
    aget v7, v6, v3

    int-to-long v10, v7

    aput-wide v10, v5, v3

    .line 195
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 189
    .end local v3    # "i":I
    .end local v5    # "pattern":[J
    .end local v6    # "patternArray":[I
    :catch_0
    move-exception v2

    .line 190
    .local v2, "e":Landroid/content/res/Resources$NotFoundException;
    const/4 v7, 0x6

    const-string v10, "Failed to load pattern %d"

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v8, v9

    invoke-static {p0, v7, v10, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    move v7, v9

    .line 191
    goto :goto_0

    .line 199
    .end local v2    # "e":Landroid/content/res/Resources$NotFoundException;
    .restart local v3    # "i":I
    .restart local v5    # "pattern":[J
    .restart local v6    # "patternArray":[I
    :cond_3
    iget-object v7, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v7, v5, p2}, Landroid/os/Vibrator;->vibrate([JI)V

    move v7, v8

    .line 200
    goto :goto_0
.end method

.method public setAuditoryEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 125
    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mAuditoryEnabled:Z

    .line 126
    return-void
.end method

.method public setHapticEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 118
    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mHapticEnabled:Z

    .line 119
    return-void
.end method

.method public setVolumeAdjustment(F)V
    .locals 0
    .param p1, "adjustment"    # F

    .prologue
    .line 135
    iput p1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mVolumeAdjustment:F

    .line 136
    return-void
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mSoundPool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 269
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 270
    return-void
.end method
