.class Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;
.super Ljava/lang/Object;
.source "EventSpeechRule.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DefaultFormatter"
.end annotation


# instance fields
.field private final mQuantityProperty:Ljava/lang/String;

.field private final mSelectors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mTemplateNode:Lorg/w3c/dom/Node;

.field final synthetic this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;Lorg/w3c/dom/Node;)V
    .locals 10
    .param p2, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 957
    iput-object p1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 958
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mSelectors:Ljava/util/List;

    .line 959
    const/4 v5, 0x0

    .line 960
    .local v5, "templateNode":Lorg/w3c/dom/Node;
    const/4 v4, 0x0

    .line 961
    .local v4, "quantityProperty":Ljava/lang/String;
    invoke-interface {p2}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 962
    .local v1, "children":Lorg/w3c/dom/NodeList;
    const/4 v3, 0x0

    .local v3, "i":I
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    .local v2, "count":I
    :goto_0
    if-ge v3, v2, :cond_4

    .line 963
    invoke-interface {v1, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 965
    .local v0, "child":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v7

    const/4 v8, 0x1

    if-eq v7, v8, :cond_0

    .line 962
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 969
    :cond_0
    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getUnqualifiedNodeName(Lorg/w3c/dom/Node;)Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$000(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    .line 971
    .local v6, "unqualifiedName":Ljava/lang/String;
    const-string v7, "template"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 972
    move-object v5, v0

    goto :goto_1

    .line 973
    :cond_1
    const-string v7, "property"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 974
    iget-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mSelectors:Ljava/util/List;

    new-instance v8, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;

    # getter for: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$600(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v9

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getLocalizedTextContent(Landroid/content/Context;Lorg/w3c/dom/Node;)Ljava/lang/String;
    invoke-static {v9, v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$700(Landroid/content/Context;Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v6, v9}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 976
    :cond_2
    const-string v7, "quantity"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 977
    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$100(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 979
    :cond_3
    iget-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mSelectors:Ljava/util/List;

    new-instance v8, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$100(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v6, v9}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 984
    .end local v0    # "child":Lorg/w3c/dom/Node;
    .end local v6    # "unqualifiedName":Ljava/lang/String;
    :cond_4
    iput-object v5, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mTemplateNode:Lorg/w3c/dom/Node;

    .line 985
    iput-object v4, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mQuantityProperty:Ljava/lang/String;

    .line 986
    return-void
.end method

.method private formatTemplateOrAppendSpaceSeparatedValueIfNoTemplate(Lcom/google/android/marvin/talkback/Utterance;I[Ljava/lang/Object;)V
    .locals 15
    .param p1, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;
    .param p2, "quantity"    # I
    .param p3, "arguments"    # [Ljava/lang/Object;

    .prologue
    .line 1038
    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mTemplateNode:Lorg/w3c/dom/Node;

    if-eqz v9, :cond_3

    .line 1039
    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    # getter for: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v9}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$600(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mTemplateNode:Lorg/w3c/dom/Node;

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;
    invoke-static {v10}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$100(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v10

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getResourceIdentifierContent(Landroid/content/Context;Ljava/lang/String;)I
    invoke-static {v9, v10}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$800(Landroid/content/Context;Ljava/lang/String;)I

    move-result v7

    .line 1042
    .local v7, "templateRes":I
    if-gez v7, :cond_1

    .line 1050
    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mTemplateNode:Lorg/w3c/dom/Node;

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;
    invoke-static {v9}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$100(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v8

    .line 1063
    .local v8, "templateString":Ljava/lang/String;
    :goto_0
    :try_start_0
    move-object/from16 v0, p3

    invoke-static {v8, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1064
    .local v3, "formatted":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 1065
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/util/MissingFormatArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1082
    .end local v3    # "formatted":Ljava/lang/String;
    .end local v7    # "templateRes":I
    .end local v8    # "templateString":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 1052
    .restart local v7    # "templateRes":I
    :cond_1
    const/4 v9, -0x1

    move/from16 v0, p2

    if-eq v0, v9, :cond_2

    .line 1056
    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    # getter for: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v9}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$600(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    move/from16 v0, p2

    invoke-virtual {v9, v7, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "templateString":Ljava/lang/String;
    goto :goto_0

    .line 1059
    .end local v8    # "templateString":Ljava/lang/String;
    :cond_2
    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    # getter for: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v9}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$600(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mTemplateNode:Lorg/w3c/dom/Node;

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getLocalizedTextContent(Landroid/content/Context;Lorg/w3c/dom/Node;)Ljava/lang/String;
    invoke-static {v9, v10}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$700(Landroid/content/Context;Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "templateString":Ljava/lang/String;
    goto :goto_0

    .line 1067
    :catch_0
    move-exception v6

    .line 1068
    .local v6, "mfae":Ljava/util/MissingFormatArgumentException;
    const-class v9, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;

    const/4 v10, 0x6

    const-string v11, "Speech rule: \'%d\' has inconsistency between template: \'%s\' and arguments: \'%s\'. Possibliy #template arguments does not match #parameters. %s"

    const/4 v12, 0x4

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    # getter for: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mRuleIndex:I
    invoke-static {v14}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$900(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    iget-object v14, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mTemplateNode:Lorg/w3c/dom/Node;

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;
    invoke-static {v14}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$100(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    aput-object p3, v12, v13

    const/4 v13, 0x3

    invoke-virtual {v6}, Ljava/util/MissingFormatArgumentException;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v9, v10, v11, v12}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 1075
    .end local v6    # "mfae":Ljava/util/MissingFormatArgumentException;
    .end local v7    # "templateRes":I
    .end local v8    # "templateString":Ljava/lang/String;
    :cond_3
    move-object/from16 v2, p3

    .local v2, "arr$":[Ljava/lang/Object;
    array-length v5, v2

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_2
    if-ge v4, v5, :cond_0

    aget-object v1, v2, v4

    .line 1076
    .local v1, "arg":Ljava/lang/Object;
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1077
    .restart local v3    # "formatted":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 1078
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 1075
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 14
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    .line 991
    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mSelectors:Ljava/util/List;

    .line 992
    .local v9, "selectors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v11

    new-array v1, v11, [Ljava/lang/Object;

    .line 994
    .local v1, "arguments":[Ljava/lang/Object;
    const/4 v3, 0x0

    .local v3, "i":I
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    .local v2, "count":I
    :goto_0
    if-ge v3, v2, :cond_2

    .line 995
    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;

    .line 996
    .local v6, "selector":Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;, "Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v7, v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;->first:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    .line 997
    .local v7, "selectorType":Ljava/lang/String;
    iget-object v8, v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/String;

    .line 999
    .local v8, "selectorValue":Ljava/lang/String;
    const-string v11, "property"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1000
    iget-object v11, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    move-object/from16 v0, p2

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getPropertyValue(Landroid/content/Context;Ljava/lang/String;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/Object;
    invoke-static {v11, v0, v8, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$500(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;Landroid/content/Context;Ljava/lang/String;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/Object;

    move-result-object v4

    .line 1001
    .local v4, "propertyValue":Ljava/lang/Object;
    if-eqz v4, :cond_0

    .end local v4    # "propertyValue":Ljava/lang/Object;
    :goto_1
    aput-object v4, v1, v3

    .line 994
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1001
    .restart local v4    # "propertyValue":Ljava/lang/Object;
    :cond_0
    const-string v4, ""

    goto :goto_1

    .line 1003
    .end local v4    # "propertyValue":Ljava/lang/Object;
    :cond_1
    new-instance v12, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Unknown selector type: ["

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v11, v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;->first:Ljava/lang/Object;

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, ", "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v11, v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;->second:Ljava/lang/Object;

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, "]"

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v12, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 1008
    .end local v6    # "selector":Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;, "Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "selectorType":Ljava/lang/String;
    .end local v8    # "selectorValue":Ljava/lang/String;
    :cond_2
    iget-object v11, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mQuantityProperty:Ljava/lang/String;

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_4

    const/4 v5, -0x1

    .line 1011
    .local v5, "quantity":I
    :goto_2
    move-object/from16 v0, p3

    invoke-direct {p0, v0, v5, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->formatTemplateOrAppendSpaceSeparatedValueIfNoTemplate(Lcom/google/android/marvin/talkback/Utterance;I[Ljava/lang/Object;)V

    .line 1013
    const v11, 0x808c

    invoke-static {p1, v11}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->eventMatchesAnyType(Landroid/view/accessibility/AccessibilityEvent;I)Z

    move-result v10

    .line 1017
    .local v10, "speakState":Z
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/marvin/talkback/Utterance;->getSpoken()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_3

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->isEnabled()Z

    move-result v11

    if-nez v11, :cond_3

    if-eqz v10, :cond_3

    .line 1018
    iget-object v11, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    # getter for: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v11}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$600(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v11

    const v12, 0x7f0600c9

    invoke-virtual {v11, v12}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 1021
    :cond_3
    const/4 v11, 0x1

    return v11

    .line 1008
    .end local v5    # "quantity":I
    .end local v10    # "speakState":Z
    :cond_4
    iget-object v11, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    iget-object v12, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mQuantityProperty:Ljava/lang/String;

    move-object/from16 v0, p2

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getPropertyValue(Landroid/content/Context;Ljava/lang/String;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/Object;
    invoke-static {v11, v0, v12, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$500(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;Landroid/content/Context;Ljava/lang/String;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v5

    goto :goto_2
.end method
