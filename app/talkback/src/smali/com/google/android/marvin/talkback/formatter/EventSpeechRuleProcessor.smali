.class public Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;
.super Ljava/lang/Object;
.source "EventSpeechRuleProcessor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$1;,
        Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;
    }
.end annotation


# instance fields
.field private final mContext:Lcom/google/android/marvin/talkback/TalkBackService;

.field private mDocumentBuilder:Ljavax/xml/parsers/DocumentBuilder;

.field private final mPackageNameToSpeechRulesMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 1
    .param p1, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mPackageNameToSpeechRulesMap:Ljava/util/Map;

    .line 109
    iput-object p1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 110
    return-void
.end method

.method private addSpeechRuleLocked(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Z
    .locals 3
    .param p1, "speechRule"    # Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    .prologue
    .line 198
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 200
    .local v0, "packageName":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mPackageNameToSpeechRulesMap:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 202
    .local v1, "packageSpeechRules":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;>;"
    if-nez v1, :cond_0

    .line 203
    new-instance v1, Ljava/util/LinkedList;

    .end local v1    # "packageSpeechRules":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;>;"
    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 204
    .restart local v1    # "packageSpeechRules":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;>;"
    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mPackageNameToSpeechRulesMap:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    :cond_0
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v2

    return v2
.end method

.method private getDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/parsers/ParserConfigurationException;
        }
    .end annotation

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mDocumentBuilder:Ljavax/xml/parsers/DocumentBuilder;

    if-nez v0, :cond_0

    .line 275
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mDocumentBuilder:Ljavax/xml/parsers/DocumentBuilder;

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mDocumentBuilder:Ljavax/xml/parsers/DocumentBuilder;

    return-object v0
.end method

.method private parseSpeechStrategy(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
    .locals 8
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 259
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->getDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 260
    .local v0, "builder":Ljavax/xml/parsers/DocumentBuilder;
    invoke-virtual {v0, p1}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 266
    .end local v0    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    :goto_0
    return-object v2

    .line 261
    :catch_0
    move-exception v1

    .line 262
    .local v1, "e":Ljava/lang/Exception;
    const-class v2, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const/4 v3, 0x6

    const-string v4, "Could not open speechstrategy xml file\n%s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v2, v3, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 266
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private processEvent(Ljava/util/List;Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/Utterance;)Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;
    .locals 11
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;",
            ">;",
            "Landroid/view/accessibility/AccessibilityEvent;",
            "Lcom/google/android/marvin/talkback/Utterance;",
            ")",
            "Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;"
        }
    .end annotation

    .prologue
    .local p1, "speechRules":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;>;"
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 220
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    .line 223
    .local v2, "speechRule":Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
    :try_start_0
    invoke-virtual {v2, p2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->applyFilter(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 224
    invoke-virtual {v2, p2, p3}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->applyFormatter(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/Utterance;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 225
    const-class v3, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const/4 v4, 0x2

    const-string v5, "Processed event using rule:\n%s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-static {v3, v4, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 227
    sget-object v3, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;->FORMATTED:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    .line 244
    .end local v2    # "speechRule":Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
    :goto_1
    return-object v3

    .line 229
    .restart local v2    # "speechRule":Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
    :cond_1
    const-class v3, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const/4 v4, 0x2

    const-string v5, "The \"%s\" filter accepted the event, but the \"%s\" formatter indicated the event should be dropped."

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getFilter()Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getFormatter()Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v3, v4, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 234
    sget-object v3, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;->REJECTED:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 237
    :catch_0
    move-exception v0

    .line 238
    .local v0, "e":Ljava/lang/Exception;
    const-class v3, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const/4 v4, 0x6

    const-string v5, "Error while processing rule:\n%s"

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v2, v6, v9

    invoke-static {v3, v4, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 240
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 244
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "speechRule":Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
    :cond_2
    sget-object v3, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;->NOT_MATCHED:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    goto :goto_1
.end method


# virtual methods
.method public addSpeechStrategy(Ljava/lang/Iterable;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 181
    .local p1, "speechRules":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;>;"
    const/4 v0, 0x0

    .line 183
    .local v0, "count":I
    iget-object v4, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mPackageNameToSpeechRulesMap:Ljava/util/Map;

    monitor-enter v4

    .line 184
    :try_start_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    .line 185
    .local v2, "speechRule":Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechRuleLocked(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 186
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 189
    .end local v2    # "speechRule":Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
    :cond_1
    monitor-exit v4

    .line 191
    return v0

    .line 189
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public addSpeechStrategy(I)V
    .locals 12
    .param p1, "resourceId"    # I

    .prologue
    .line 162
    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 163
    .local v3, "res":Landroid/content/res/Resources;
    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v5

    .line 164
    .local v5, "speechStrategy":Ljava/lang/String;
    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    .line 165
    .local v2, "inputStream":Ljava/io/InputStream;
    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->parseSpeechStrategy(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v1

    .line 166
    .local v1, "document":Lorg/w3c/dom/Document;
    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v6, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->createSpeechRules(Lcom/google/android/marvin/talkback/TalkBackService;Lorg/w3c/dom/Document;)Ljava/util/ArrayList;

    move-result-object v4

    .line 169
    .local v4, "speechRules":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;>;"
    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(Ljava/lang/Iterable;)I

    move-result v0

    .line 171
    .local v0, "added":I
    const-class v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const/4 v7, 0x4

    const-string v8, "%d speech rules appended from: %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object v5, v9, v10

    invoke-static {v6, v7, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 173
    return-void
.end method

.method public processEvent(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 123
    iget-object v4, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mPackageNameToSpeechRulesMap:Ljava/util/Map;

    monitor-enter v4

    .line 125
    :try_start_0
    iget-object v5, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mPackageNameToSpeechRulesMap:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 128
    .local v1, "speechRules":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;>;"
    if-eqz v1, :cond_0

    .line 129
    invoke-direct {p0, v1, p1, p2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->processEvent(Ljava/util/List;Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/Utterance;)Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    move-result-object v0

    .line 130
    .local v0, "packageResult":Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;
    sget-object v5, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$1;->$SwitchMap$com$google$android$marvin$talkback$formatter$EventSpeechRuleProcessor$RuleProcessorResult:[I

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 141
    .end local v0    # "packageResult":Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;
    :cond_0
    iget-object v5, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mPackageNameToSpeechRulesMap:Ljava/util/Map;

    const-string v6, "undefined_package_name"

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "speechRules":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;>;"
    check-cast v1, Ljava/util/List;

    .line 143
    .restart local v1    # "speechRules":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;>;"
    if-eqz v1, :cond_2

    .line 144
    invoke-direct {p0, v1, p1, p2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->processEvent(Ljava/util/List;Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/Utterance;)Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    move-result-object v5

    sget-object v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;->FORMATTED:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;

    if-ne v5, v6, :cond_1

    :goto_0
    monitor-exit v4

    .line 148
    :goto_1
    return v2

    .line 132
    .restart local v0    # "packageResult":Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;
    :pswitch_0
    monitor-exit v4

    goto :goto_1

    .line 146
    .end local v0    # "packageResult":Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;
    .end local v1    # "speechRules":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;>;"
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 134
    .restart local v0    # "packageResult":Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;
    .restart local v1    # "speechRules":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;>;"
    :pswitch_1
    :try_start_1
    monitor-exit v4

    move v2, v3

    goto :goto_1

    .end local v0    # "packageResult":Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor$RuleProcessorResult;
    :cond_1
    move v2, v3

    .line 144
    goto :goto_0

    .line 146
    :cond_2
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v2, v3

    .line 148
    goto :goto_1

    .line 130
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
