.class public Lcom/google/android/marvin/talkback/KeyComboManager;
.super Ljava/lang/Object;
.source "KeyComboManager.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/TalkBackService$KeyEventListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;,
        Lcom/google/android/marvin/talkback/KeyComboManager$KeyComboListener;
    }
.end annotation


# instance fields
.field private mHasPartialMatch:Z

.field private final mKeyCombos:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;",
            ">;"
        }
    .end annotation
.end field

.field private mKeyCount:I

.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/marvin/talkback/KeyComboManager$KeyComboListener;",
            ">;"
        }
    .end annotation
.end field

.field private mPerformedCombo:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mKeyCombos:Ljava/util/LinkedList;

    .line 48
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mListeners:Ljava/util/List;

    .line 168
    return-void
.end method

.method private onKeyDown(Landroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 121
    iget v7, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mKeyCount:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mKeyCount:I

    .line 124
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getModifiers()I

    move-result v7

    if-nez v7, :cond_0

    .line 147
    :goto_0
    return v5

    .line 129
    :cond_0
    iput-boolean v5, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mHasPartialMatch:Z

    .line 131
    iget-object v5, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mKeyCombos:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;

    .line 132
    .local v2, "keyCombo":Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;
    invoke-virtual {v2, p1}, Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;->matches(Landroid/view/KeyEvent;)I

    move-result v4

    .line 133
    .local v4, "match":I
    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 134
    iget-object v5, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mListeners:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/marvin/talkback/KeyComboManager$KeyComboListener;

    .line 135
    .local v3, "listener":Lcom/google/android/marvin/talkback/KeyComboManager$KeyComboListener;
    # getter for: Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;->mId:I
    invoke-static {v2}, Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;->access$000(Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;)I

    move-result v5

    invoke-interface {v3, v5}, Lcom/google/android/marvin/talkback/KeyComboManager$KeyComboListener;->onComboPerformed(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 136
    iput-boolean v6, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mPerformedCombo:Z

    move v5, v6

    .line 137
    goto :goto_0

    .line 142
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "listener":Lcom/google/android/marvin/talkback/KeyComboManager$KeyComboListener;
    :cond_3
    if-ne v4, v6, :cond_1

    .line 143
    iput-boolean v6, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mHasPartialMatch:Z

    goto :goto_1

    .line 147
    .end local v2    # "keyCombo":Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;
    .end local v4    # "match":I
    :cond_4
    iget-boolean v5, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mHasPartialMatch:Z

    goto :goto_0
.end method

.method private onKeyUp()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 151
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mPerformedCombo:Z

    .line 153
    .local v0, "handled":Z
    iget v1, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mKeyCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mKeyCount:I

    .line 155
    iget v1, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mKeyCount:I

    if-nez v1, :cond_0

    .line 157
    iput-boolean v2, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mPerformedCombo:Z

    .line 158
    iput-boolean v2, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mHasPartialMatch:Z

    .line 161
    :cond_0
    return v0
.end method


# virtual methods
.method public addCombo(III)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "modifiers"    # I
    .param p3, "keyCode"    # I

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mKeyCombos:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/marvin/talkback/KeyComboManager$KeyCombo;-><init>(III)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 93
    return-void
.end method

.method public addListener(Lcom/google/android/marvin/talkback/KeyComboManager$KeyComboListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/marvin/talkback/KeyComboManager$KeyComboListener;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    return-void
.end method

.method public loadDefaultCombos()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 56
    const/4 v0, 0x3

    .line 57
    .local v0, "ALT_SHIFT":I
    const v1, 0x7f0d000f

    const/16 v2, 0x43

    invoke-virtual {p0, v1, v3, v2}, Lcom/google/android/marvin/talkback/KeyComboManager;->addCombo(III)V

    .line 58
    const v1, 0x7f0d0010

    const/16 v2, 0x24

    invoke-virtual {p0, v1, v3, v2}, Lcom/google/android/marvin/talkback/KeyComboManager;->addCombo(III)V

    .line 59
    const v1, 0x7f0d0011

    const/16 v2, 0x2a

    invoke-virtual {p0, v1, v3, v2}, Lcom/google/android/marvin/talkback/KeyComboManager;->addCombo(III)V

    .line 60
    const v1, 0x7f0d0012

    const/16 v2, 0x2e

    invoke-virtual {p0, v1, v3, v2}, Lcom/google/android/marvin/talkback/KeyComboManager;->addCombo(III)V

    .line 61
    const v1, 0x7f0d0013

    const/16 v2, 0x42

    invoke-virtual {p0, v1, v3, v2}, Lcom/google/android/marvin/talkback/KeyComboManager;->addCombo(III)V

    .line 62
    const v1, 0x7f0d0014

    const/16 v2, 0x13

    invoke-virtual {p0, v1, v3, v2}, Lcom/google/android/marvin/talkback/KeyComboManager;->addCombo(III)V

    .line 63
    const v1, 0x7f0d0015

    const/16 v2, 0x14

    invoke-virtual {p0, v1, v3, v2}, Lcom/google/android/marvin/talkback/KeyComboManager;->addCombo(III)V

    .line 64
    const v1, 0x7f0d0017

    const/16 v2, 0x16

    invoke-virtual {p0, v1, v3, v2}, Lcom/google/android/marvin/talkback/KeyComboManager;->addCombo(III)V

    .line 65
    const v1, 0x7f0d0016

    const/16 v2, 0x15

    invoke-virtual {p0, v1, v3, v2}, Lcom/google/android/marvin/talkback/KeyComboManager;->addCombo(III)V

    .line 66
    const v1, 0x7f0d0018

    const/16 v2, 0x36

    invoke-virtual {p0, v1, v3, v2}, Lcom/google/android/marvin/talkback/KeyComboManager;->addCombo(III)V

    .line 67
    const v1, 0x7f0d0019

    const/16 v2, 0x45

    invoke-virtual {p0, v1, v3, v2}, Lcom/google/android/marvin/talkback/KeyComboManager;->addCombo(III)V

    .line 68
    const v1, 0x7f0d001a

    const/16 v2, 0x46

    invoke-virtual {p0, v1, v3, v2}, Lcom/google/android/marvin/talkback/KeyComboManager;->addCombo(III)V

    .line 69
    const v1, 0x7f0d001b

    const/16 v2, 0x4c

    invoke-virtual {p0, v1, v3, v2}, Lcom/google/android/marvin/talkback/KeyComboManager;->addCombo(III)V

    .line 70
    return-void
.end method

.method public onKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x0

    .line 104
    iget-object v1, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 116
    :goto_0
    return v0

    .line 108
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 110
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/KeyComboManager;->onKeyDown(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 112
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/KeyComboManager;->mHasPartialMatch:Z

    goto :goto_0

    .line 114
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/KeyComboManager;->onKeyUp()Z

    move-result v0

    goto :goto_0

    .line 108
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
