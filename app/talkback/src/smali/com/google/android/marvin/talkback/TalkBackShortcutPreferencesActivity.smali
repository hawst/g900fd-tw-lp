.class public Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;
.super Landroid/preference/PreferenceActivity;
.source "TalkBackShortcutPreferencesActivity.java"


# static fields
.field private static final GESTURE_PREF_KEY_IDS:[I


# instance fields
.field private final mGesturePrefKeys:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mGesturePrefValues:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mPreferenceChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

.field private mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->GESTURE_PREF_KEY_IDS:[I

    return-void

    :array_0
    .array-data 4
        0x7f060044
        0x7f060045
        0x7f060048
        0x7f060049
        0x7f060046
        0x7f060047
        0x7f060042
        0x7f060043
        0x7f06004d
        0x7f06004e
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->GESTURE_PREF_KEY_IDS:[I

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->mGesturePrefKeys:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->mGesturePrefValues:Ljava/util/ArrayList;

    .line 181
    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity$1;-><init>(Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->mPreferenceChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    return-void
.end method

.method private fixListSummaries(Landroid/preference/PreferenceGroup;)V
    .locals 4
    .param p1, "root"    # Landroid/preference/PreferenceGroup;

    .prologue
    .line 139
    if-nez p1, :cond_1

    .line 154
    :cond_0
    return-void

    .line 143
    :cond_1
    invoke-virtual {p1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    .line 145
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 146
    invoke-virtual {p1, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    .line 147
    .local v2, "preference":Landroid/preference/Preference;
    instance-of v3, v2, Landroid/preference/ListPreference;

    if-eqz v3, :cond_3

    .line 148
    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->fixUnboundPrefSummary(Landroid/preference/Preference;)V

    .line 149
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->mPreferenceChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 145
    .end local v2    # "preference":Landroid/preference/Preference;
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 150
    .restart local v2    # "preference":Landroid/preference/Preference;
    :cond_3
    instance-of v3, v2, Landroid/preference/PreferenceGroup;

    if-eqz v3, :cond_2

    .line 151
    check-cast v2, Landroid/preference/PreferenceGroup;

    .end local v2    # "preference":Landroid/preference/Preference;
    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->fixListSummaries(Landroid/preference/PreferenceGroup;)V

    goto :goto_1
.end method

.method private fixUnboundPrefSummary(Landroid/preference/Preference;)V
    .locals 4
    .param p1, "pref"    # Landroid/preference/Preference;

    .prologue
    .line 162
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 163
    .local v0, "prefKey":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, ""

    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 164
    .local v1, "prefValue":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->mGesturePrefKeys:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 170
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->mGesturePrefValues:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 171
    const-string v2, ""

    invoke-virtual {p1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 174
    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 15
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 71
    invoke-super/range {p0 .. p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 73
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v12

    iput-object v12, p0, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->mPrefs:Landroid/content/SharedPreferences;

    .line 75
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 78
    sget-object v0, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->GESTURE_PREF_KEY_IDS:[I

    .local v0, "arr$":[I
    array-length v7, v0

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v7, :cond_0

    aget v6, v0, v5

    .line 79
    .local v6, "id":I
    iget-object v12, p0, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->mGesturePrefKeys:Ljava/util/ArrayList;

    invoke-virtual {p0, v6}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 82
    .end local v6    # "id":I
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a0013

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 84
    .local v2, "defaultGesturePrefValues":[Ljava/lang/String;
    iget-object v12, p0, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->mGesturePrefValues:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 86
    const v12, 0x7f040001

    invoke-virtual {p0, v12}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->addPreferencesFromResource(I)V

    .line 88
    iget-object v12, p0, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->mPrefs:Landroid/content/SharedPreferences;

    const v13, 0x7f060030

    invoke-virtual {p0, v13}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-interface {v12, v13, v14}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    .line 90
    .local v11, "treeDebugEnabled":Z
    if-eqz v11, :cond_2

    .line 92
    array-length v10, v2

    .line 94
    .local v10, "numDefaultValues":I
    add-int/lit8 v12, v10, 0x1

    invoke-static {v2, v12}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    .line 96
    .local v9, "newGesturePrefValues":[Ljava/lang/String;
    const v12, 0x7f060061

    invoke-virtual {p0, v12}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v9, v10

    .line 99
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a0012

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 101
    .local v1, "defaultGesturePrefLabels":[Ljava/lang/String;
    add-int/lit8 v12, v10, 0x1

    invoke-static {v1, v12}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    .line 103
    .local v8, "newGesturePrefLabels":[Ljava/lang/String;
    const v12, 0x7f060128

    invoke-virtual {p0, v12}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v8, v10

    .line 106
    sget-object v0, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->GESTURE_PREF_KEY_IDS:[I

    array-length v7, v0

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v7, :cond_1

    aget v3, v0, v5

    .line 107
    .local v3, "gestureKey":I
    invoke-virtual {p0, v3}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/ListPreference;

    .line 109
    .local v4, "gesturePref":Landroid/preference/ListPreference;
    invoke-virtual {v4, v8}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 110
    invoke-virtual {v4, v9}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 106
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 113
    .end local v3    # "gestureKey":I
    .end local v4    # "gesturePref":Landroid/preference/ListPreference;
    :cond_1
    iget-object v12, p0, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->mGesturePrefValues:Ljava/util/ArrayList;

    const v13, 0x7f060061

    invoke-virtual {p0, v13}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    .end local v1    # "defaultGesturePrefLabels":[Ljava/lang/String;
    .end local v8    # "newGesturePrefLabels":[Ljava/lang/String;
    .end local v9    # "newGesturePrefValues":[Ljava/lang/String;
    .end local v10    # "numDefaultValues":I
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v12

    invoke-direct {p0, v12}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->fixListSummaries(Landroid/preference/PreferenceGroup;)V

    .line 117
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 124
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 129
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 126
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->finish()V

    .line 127
    const/4 v0, 0x1

    goto :goto_0

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
