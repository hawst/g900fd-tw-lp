.class Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$11;
.super Ljava/lang/Object;
.source "TalkBackPreferencesActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V
    .locals 0

    .prologue
    .line 785
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$11;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 10
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const v9, 0x7f060033

    const/4 v8, 0x1

    .line 788
    instance-of v5, p1, Landroid/preference/ListPreference;

    if-eqz v5, :cond_0

    instance-of v5, p2, Ljava/lang/String;

    if-eqz v5, :cond_0

    move-object v3, p1

    .line 789
    check-cast v3, Landroid/preference/ListPreference;

    .local v3, "listPreference":Landroid/preference/ListPreference;
    move-object v5, p2

    .line 790
    check-cast v5, Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    .line 791
    .local v1, "index":I
    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    .line 793
    .local v0, "entries":[Ljava/lang/CharSequence;
    if-ltz v1, :cond_2

    array-length v5, v0

    if-ge v1, v5, :cond_2

    .line 794
    aget-object v5, v0, v1

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "%"

    const-string v7, "%%"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 800
    .end local v0    # "entries":[Ljava/lang/CharSequence;
    .end local v1    # "index":I
    .end local v3    # "listPreference":Landroid/preference/ListPreference;
    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 801
    .local v2, "key":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$11;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-virtual {v5, v9}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 802
    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$11;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mPrefs:Landroid/content/SharedPreferences;
    invoke-static {v5}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->access$800(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)Landroid/content/SharedPreferences;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$11;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-virtual {v6}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f06006e

    invoke-static {v5, v6, v9, v7}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Ljava/lang/String;

    move-result-object v4

    .line 805
    .local v4, "oldValue":Ljava/lang/String;
    invoke-virtual {p2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 808
    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$11;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mPrefs:Landroid/content/SharedPreferences;
    invoke-static {v5}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->access$800(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)Landroid/content/SharedPreferences;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$11;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-virtual {v6}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f060051

    invoke-static {v5, v6, v7, v8}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->putBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;IZ)V

    .line 813
    .end local v4    # "oldValue":Ljava/lang/String;
    :cond_1
    return v8

    .line 796
    .end local v2    # "key":Ljava/lang/String;
    .restart local v0    # "entries":[Ljava/lang/CharSequence;
    .restart local v1    # "index":I
    .restart local v3    # "listPreference":Landroid/preference/ListPreference;
    :cond_2
    const-string v5, ""

    invoke-virtual {p1, v5}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
