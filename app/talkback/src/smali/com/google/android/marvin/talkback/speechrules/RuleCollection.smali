.class public Lcom/google/android/marvin/talkback/speechrules/RuleCollection;
.super Lcom/google/android/marvin/talkback/speechrules/RuleDefault;
.source "RuleCollection.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/speechrules/RuleDefault;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v2, 0x0

    .line 47
    if-nez p3, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v2

    .line 51
    :cond_1
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getParcelableData()Landroid/os/Parcelable;

    move-result-object v1

    .line 52
    .local v1, "parcelable":Landroid/os/Parcelable;
    instance-of v3, v1, Landroid/os/Bundle;

    if-eqz v3, :cond_0

    move-object v0, v1

    .line 53
    check-cast v0, Landroid/os/Bundle;

    .line 54
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "AccessibilityNodeInfo.CollectionItemInfo.rowIndex"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "AccessibilityNodeInfo.CollectionItemInfo.columnIndex"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "AccessibilityNodeInfo.CollectionItemInfo.heading"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 56
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public format(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 65
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 66
    .local v0, "builder":Landroid/text/SpannableStringBuilder;
    new-array v3, v8, [Ljava/lang/CharSequence;

    invoke-static {p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getNodeText(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v0, v3}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 68
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getParcelableData()Landroid/os/Parcelable;

    move-result-object v2

    .local v2, "parcelable":Landroid/os/Parcelable;
    move-object v1, v2

    .line 70
    check-cast v1, Landroid/os/Bundle;

    .line 72
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v3, "AccessibilityNodeInfo.CollectionItemInfo.heading"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 73
    new-array v3, v8, [Ljava/lang/CharSequence;

    const v4, 0x7f060192

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v0, v3}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 77
    :cond_0
    const-string v3, "AccessibilityNodeInfo.CollectionItemInfo.rowIndex"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 78
    new-array v3, v8, [Ljava/lang/CharSequence;

    const v4, 0x7f060190

    new-array v5, v8, [Ljava/lang/Object;

    const-string v6, "AccessibilityNodeInfo.CollectionItemInfo.rowIndex"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v0, v3}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 82
    :cond_1
    const-string v3, "AccessibilityNodeInfo.CollectionItemInfo.rowIndex"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 83
    new-array v3, v8, [Ljava/lang/CharSequence;

    const v4, 0x7f060191

    new-array v5, v8, [Ljava/lang/Object;

    const-string v6, "AccessibilityNodeInfo.CollectionItemInfo.columnIndex"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v0, v3}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 87
    :cond_2
    return-object v0
.end method

.method public bridge synthetic getHintText(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 35
    invoke-super {p0, p1, p2}, Lcom/google/android/marvin/talkback/speechrules/RuleDefault;->getHintText(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
