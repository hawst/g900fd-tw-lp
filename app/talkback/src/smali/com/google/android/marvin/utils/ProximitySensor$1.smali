.class Lcom/google/android/marvin/utils/ProximitySensor$1;
.super Ljava/lang/Object;
.source "ProximitySensor.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/utils/ProximitySensor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/utils/ProximitySensor;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/utils/ProximitySensor;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 7
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    const/4 v6, 0x1

    .line 122
    const/4 v0, 0x2

    const-string v1, "Processing onAccuracyChanged event at %d."

    new-array v2, v6, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    # setter for: Lcom/google/android/marvin/utils/ProximitySensor;->mShouldDropEvents:Z
    invoke-static {v0, v6}, Lcom/google/android/marvin/utils/ProximitySensor;->access$002(Lcom/google/android/marvin/utils/ProximitySensor;Z)Z

    .line 125
    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    # getter for: Lcom/google/android/marvin/utils/ProximitySensor;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/marvin/utils/ProximitySensor;->access$200(Lcom/google/android/marvin/utils/ProximitySensor;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    # getter for: Lcom/google/android/marvin/utils/ProximitySensor;->mFilterRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/marvin/utils/ProximitySensor;->access$100(Lcom/google/android/marvin/utils/ProximitySensor;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    # getter for: Lcom/google/android/marvin/utils/ProximitySensor;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/marvin/utils/ProximitySensor;->access$200(Lcom/google/android/marvin/utils/ProximitySensor;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    # getter for: Lcom/google/android/marvin/utils/ProximitySensor;->mFilterRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/marvin/utils/ProximitySensor;->access$100(Lcom/google/android/marvin/utils/ProximitySensor;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x78

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 127
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 7
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 131
    iget-object v2, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    # getter for: Lcom/google/android/marvin/utils/ProximitySensor;->mShouldDropEvents:Z
    invoke-static {v2}, Lcom/google/android/marvin/utils/ProximitySensor;->access$000(Lcom/google/android/marvin/utils/ProximitySensor;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 132
    const-string v2, "Dropping onSensorChanged event at %d."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {p0, v6, v2, v0}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 141
    :goto_0
    return-void

    .line 137
    :cond_0
    const-string v2, "Processing onSensorChanged event at %d."

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {p0, v6, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 139
    iget-object v2, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v1

    iget-object v4, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    # getter for: Lcom/google/android/marvin/utils/ProximitySensor;->mFarValue:F
    invoke-static {v4}, Lcom/google/android/marvin/utils/ProximitySensor;->access$400(Lcom/google/android/marvin/utils/ProximitySensor;)F

    move-result v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1

    :goto_1
    # setter for: Lcom/google/android/marvin/utils/ProximitySensor;->mIsClose:Z
    invoke-static {v2, v0}, Lcom/google/android/marvin/utils/ProximitySensor;->access$302(Lcom/google/android/marvin/utils/ProximitySensor;Z)Z

    .line 140
    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    # getter for: Lcom/google/android/marvin/utils/ProximitySensor;->mCallback:Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;
    invoke-static {v0}, Lcom/google/android/marvin/utils/ProximitySensor;->access$500(Lcom/google/android/marvin/utils/ProximitySensor;)Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    # getter for: Lcom/google/android/marvin/utils/ProximitySensor;->mIsClose:Z
    invoke-static {v1}, Lcom/google/android/marvin/utils/ProximitySensor;->access$300(Lcom/google/android/marvin/utils/ProximitySensor;)Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;->onProximityChanged(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 139
    goto :goto_1
.end method
