.class Lcom/google/android/marvin/talkback/TalkBackAnalytics$3;
.super Ljava/lang/Object;
.source "TalkBackAnalytics.java"

# interfaces
.implements Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/TalkBackAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackAnalytics;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackAnalytics;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTtsInitialized(Z)V
    .locals 2
    .param p1, "wasSwitchingEngines"    # Z

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/marvin/talkback/TalkBackAnalytics;->mReady:Z
    invoke-static {v0, v1}, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->access$302(Lcom/google/android/marvin/talkback/TalkBackAnalytics;Z)Z

    .line 271
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    # invokes: Lcom/google/android/marvin/talkback/TalkBackAnalytics;->conditionalPing()V
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->access$400(Lcom/google/android/marvin/talkback/TalkBackAnalytics;)V

    .line 272
    return-void
.end method

.method public onUtteranceCompleted(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "utteranceId"    # Ljava/lang/String;
    .param p2, "success"    # Z

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackAnalytics$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    # invokes: Lcom/google/android/marvin/talkback/TalkBackAnalytics;->conditionalPing()V
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->access$400(Lcom/google/android/marvin/talkback/TalkBackAnalytics;)V

    .line 277
    return-void
.end method
