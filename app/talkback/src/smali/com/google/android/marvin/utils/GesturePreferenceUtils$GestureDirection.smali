.class Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;
.super Ljava/lang/Object;
.source "GesturePreferenceUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/utils/GesturePreferenceUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GestureDirection"
.end annotation


# instance fields
.field public final defaultId:I

.field public final keyId:I

.field public final labelId:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1, "key"    # I
    .param p2, "defaultValue"    # I
    .param p3, "label"    # I

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput p1, p0, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;->keyId:I

    .line 106
    iput p2, p0, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;->defaultId:I

    .line 107
    iput p3, p0, Lcom/google/android/marvin/utils/GesturePreferenceUtils$GestureDirection;->labelId:I

    .line 108
    return-void
.end method
