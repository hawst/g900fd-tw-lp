.class Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;
.super Ljava/lang/Object;
.source "RuleEditText.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/menurules/RuleEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EditTextMenuItemClickListener"
.end annotation


# instance fields
.field private final mCursorController:Lcom/google/android/marvin/talkback/CursorController;

.field private final mFeedback:Lcom/google/android/marvin/talkback/CachedFeedbackController;

.field private final mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 1
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/CachedFeedbackController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;->mFeedback:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    .line 152
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    .line 153
    iput-object p2, p0, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 154
    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 9
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/16 v8, 0x10

    const/4 v7, 0x0

    const v5, 0x8000

    const/4 v6, 0x1

    .line 158
    if-nez p1, :cond_0

    .line 159
    iget-object v4, p0, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 211
    :goto_0
    return v6

    .line 163
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 164
    .local v1, "itemId":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 167
    .local v0, "args":Landroid/os/Bundle;
    const v4, 0x7f0d0004

    if-ne v1, v4, :cond_2

    .line 168
    const-string v4, "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT"

    invoke-virtual {v0, v4, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 170
    iget-object v4, p0, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/16 v5, 0x200

    invoke-virtual {v4, v5, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(ILandroid/os/Bundle;)Z

    move-result v2

    .line 201
    .local v2, "result":Z
    :goto_1
    if-eqz v2, :cond_a

    .line 202
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getInstance()Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v3

    .line 203
    .local v3, "service":Lcom/google/android/marvin/talkback/TalkBackService;
    if-eqz v3, :cond_1

    .line 204
    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getAnalytics()Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->onTextEdited()V

    .line 210
    .end local v3    # "service":Lcom/google/android/marvin/talkback/TalkBackService;
    :cond_1
    :goto_2
    iget-object v4, p0, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    .line 172
    .end local v2    # "result":Z
    :cond_2
    const v4, 0x7f0d0005

    if-ne v1, v4, :cond_3

    .line 173
    const-string v4, "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT"

    invoke-virtual {v0, v4, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 175
    iget-object v4, p0, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/16 v5, 0x100

    invoke-virtual {v4, v5, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(ILandroid/os/Bundle;)Z

    move-result v2

    .restart local v2    # "result":Z
    goto :goto_1

    .line 177
    .end local v2    # "result":Z
    :cond_3
    const v4, 0x7f0d0006

    if-ne v1, v4, :cond_4

    .line 178
    iget-object v4, p0, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/high16 v5, 0x10000

    invoke-virtual {v4, v5}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    move-result v2

    .restart local v2    # "result":Z
    goto :goto_1

    .line 179
    .end local v2    # "result":Z
    :cond_4
    const v4, 0x7f0d0007

    if-ne v1, v4, :cond_5

    .line 180
    iget-object v4, p0, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/16 v5, 0x4000

    invoke-virtual {v4, v5}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    move-result v2

    .restart local v2    # "result":Z
    goto :goto_1

    .line 181
    .end local v2    # "result":Z
    :cond_5
    const v4, 0x7f0d0008

    if-ne v1, v4, :cond_6

    .line 182
    invoke-static {}, Lcom/google/android/marvin/talkback/ActionHistory;->getInstance()Lcom/google/android/marvin/talkback/ActionHistory;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/google/android/marvin/talkback/ActionHistory;->before(I)V

    .line 183
    iget-object v4, p0, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4, v5}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    move-result v2

    .line 184
    .restart local v2    # "result":Z
    invoke-static {}, Lcom/google/android/marvin/talkback/ActionHistory;->getInstance()Lcom/google/android/marvin/talkback/ActionHistory;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/google/android/marvin/talkback/ActionHistory;->after(I)V

    goto :goto_1

    .line 185
    .end local v2    # "result":Z
    :cond_6
    const v4, 0x7f0d0009

    if-ne v1, v4, :cond_7

    .line 186
    const-string v4, "ACTION_ARGUMENT_SELECTION_START_INT"

    invoke-virtual {v0, v4, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 187
    const-string v4, "ACTION_ARGUMENT_SELECTION_END_INT"

    iget-object v5, p0, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v5}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 189
    iget-object v4, p0, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/high16 v5, 0x20000

    invoke-virtual {v4, v5, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(ILandroid/os/Bundle;)Z

    move-result v2

    .restart local v2    # "result":Z
    goto :goto_1

    .line 191
    .end local v2    # "result":Z
    :cond_7
    const v4, 0x7f0d000a

    if-ne v1, v4, :cond_8

    .line 192
    iget-object v4, p0, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/marvin/talkback/CursorController;->setSelectionModeActive(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)V

    .line 193
    const/4 v2, 0x1

    .restart local v2    # "result":Z
    goto/16 :goto_1

    .line 194
    .end local v2    # "result":Z
    :cond_8
    const v4, 0x7f0d000b

    if-ne v1, v4, :cond_9

    .line 195
    iget-object v4, p0, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4, v5, v7}, Lcom/google/android/marvin/talkback/CursorController;->setSelectionModeActive(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)V

    .line 196
    const/4 v2, 0x1

    .restart local v2    # "result":Z
    goto/16 :goto_1

    .line 198
    .end local v2    # "result":Z
    :cond_9
    const/4 v2, 0x0

    .restart local v2    # "result":Z
    goto/16 :goto_1

    .line 207
    :cond_a
    iget-object v4, p0, Lcom/google/android/marvin/talkback/menurules/RuleEditText$EditTextMenuItemClickListener;->mFeedback:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    const v5, 0x7f050003

    invoke-virtual {v4, v5}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(I)Z

    goto/16 :goto_2
.end method
