.class public Lcom/google/android/marvin/talkback/TalkBackService;
.super Landroid/accessibilityservice/AccessibilityService;
.source "TalkBackService.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/TalkBackService$11;,
        Lcom/google/android/marvin/talkback/TalkBackService$KeyEventListener;,
        Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;
    }
.end annotation


# static fields
.field static final SUPPORTS_TOUCH_PREF:Z

.field private static final SUPPORTS_WEB_SCRIPT_TOGGLE:Z

.field private static sInstance:Lcom/google/android/marvin/talkback/TalkBackService;


# instance fields
.field private final mAccessibilityEventListeners:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private final mActiveReceiver:Landroid/content/BroadcastReceiver;

.field private mAnalytics:Lcom/google/android/marvin/talkback/TalkBackAnalytics;

.field private mAutomaticResume:Ljava/lang/String;

.field private mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

.field private mCursorController:Lcom/google/android/marvin/talkback/CursorController;

.field private mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

.field private mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

.field private final mHandler:Landroid/os/Handler;

.field private mIsUserTouchExploring:Z

.field private final mKeyComboListener:Lcom/google/android/marvin/talkback/KeyComboManager$KeyComboListener;

.field private final mKeyEventListeners:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/marvin/talkback/TalkBackService$KeyEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private mKeyboardSearchManager:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

.field private mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

.field private mLastSpokenEvent:Landroid/view/accessibility/AccessibilityEvent;

.field private mLastWindowStateChanged:J

.field private mOrientationMonitor:Lcom/google/android/marvin/talkback/OrientationMonitor;

.field private mPackageReceiver:Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mPrefProxy:Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mProcessorEventQueue:Lcom/google/android/marvin/talkback/ProcessorEventQueue;

.field private mProcessorFollowFocus:Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;

.field private mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

.field private mRingerModeAndScreenMonitor:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

.field private mSavedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private mServiceState:I

.field private mServiceStateListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

.field private final mSharedPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private mSideTapManager:Lcom/google/android/marvin/talkback/SideTapManager;

.field private mSpeakCallerId:Z

.field private mSpeakWhenScreenOff:Z

.field private mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

.field private mSuspendDialog:Landroid/app/AlertDialog;

.field private final mSuspendedReceiver:Landroid/content/BroadcastReceiver;

.field private mSystemUeh:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private mTestingListener:Lcom/google/android/marvin/talkback/test/TalkBackListener;

.field private final mTouchExploreObserver:Landroid/database/ContentObserver;

.field private mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

.field private mVerticalGestureCycleGranularity:Z

.field private mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 102
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_TOUCH_PREF:Z

    .line 130
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_WEB_SCRIPT_TOGGLE:Z

    .line 161
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/marvin/talkback/TalkBackService;->sInstance:Lcom/google/android/marvin/talkback/TalkBackService;

    return-void

    :cond_0
    move v0, v2

    .line 102
    goto :goto_0

    :cond_1
    move v1, v2

    .line 130
    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 99
    invoke-direct {p0}, Landroid/accessibilityservice/AccessibilityService;-><init>()V

    .line 175
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityEventListeners:Ljava/util/LinkedList;

    .line 182
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mKeyEventListeners:Ljava/util/LinkedList;

    .line 189
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mServiceStateListeners:Ljava/util/List;

    .line 259
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLastWindowStateChanged:J

    .line 1566
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mHandler:Landroid/os/Handler;

    .line 1568
    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackService$6;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackService$6;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mKeyComboListener:Lcom/google/android/marvin/talkback/KeyComboManager$KeyComboListener;

    .line 1605
    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackService$7;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackService$7;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSharedPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 1618
    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackService$8;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/google/android/marvin/talkback/TalkBackService$8;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mTouchExploreObserver:Landroid/database/ContentObserver;

    .line 1638
    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackService$9;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackService$9;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mActiveReceiver:Landroid/content/BroadcastReceiver;

    .line 1655
    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackService$10;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackService$10;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSuspendedReceiver:Landroid/content/BroadcastReceiver;

    .line 1715
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/TalkBackService;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSuspendDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/TalkBackService;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSavedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p1, "x1"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSavedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->reloadPreferences()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/TalkBackService;)Ljava/util/LinkedList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityEventListeners:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/CursorController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->onTouchExplorationEnabled()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/ShortcutGestureAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p1, "x1"    # Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackService;->performGestureAction(Lcom/google/android/marvin/talkback/ShortcutGestureAction;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/marvin/talkback/TalkBackService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAutomaticResume:Ljava/lang/String;

    return-object v0
.end method

.method private cacheEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 1363
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLastSpokenEvent:Landroid/view/accessibility/AccessibilityEvent;

    if-eqz v0, :cond_0

    .line 1364
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLastSpokenEvent:Landroid/view/accessibility/AccessibilityEvent;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    .line 1367
    :cond_0
    invoke-static {p1}, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->obtain(Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLastSpokenEvent:Landroid/view/accessibility/AccessibilityEvent;

    .line 1368
    return-void
.end method

.method private confirmSuspendTalkBack()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 419
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSuspendDialog:Landroid/app/AlertDialog;

    if-eqz v6, :cond_1

    .line 420
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSuspendDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 474
    :goto_0
    return-void

    .line 423
    :cond_0
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSuspendDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->dismiss()V

    .line 424
    iput-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSuspendDialog:Landroid/app/AlertDialog;

    .line 428
    :cond_1
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 429
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f030006

    invoke-virtual {v1, v6, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ScrollView;

    .line 431
    .local v5, "root":Landroid/widget/ScrollView;
    const v6, 0x7f0d007b

    invoke-virtual {v5, v6}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 432
    .local v0, "confirmCheckBox":Landroid/widget/CheckBox;
    const v6, 0x7f0d007a

    invoke-virtual {v5, v6}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 434
    .local v2, "message":Landroid/widget/TextView;
    new-instance v3, Lcom/google/android/marvin/talkback/TalkBackService$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/marvin/talkback/TalkBackService$1;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/widget/CheckBox;)V

    .line 448
    .local v3, "okayClick":Landroid/content/DialogInterface$OnClickListener;
    new-instance v4, Lcom/google/android/marvin/talkback/TalkBackService$2;

    invoke-direct {v4, p0}, Lcom/google/android/marvin/talkback/TalkBackService$2;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    .line 455
    .local v4, "onDismissListener":Landroid/content/DialogInterface$OnDismissListener;
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAutomaticResume:Ljava/lang/String;

    const v7, 0x7f060056

    invoke-virtual {p0, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 456
    const v6, 0x7f060175

    invoke-virtual {p0, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 463
    :goto_1
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f060171

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const/high16 v7, 0x1040000

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x104000a

    invoke-virtual {v6, v7, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSuspendDialog:Landroid/app/AlertDialog;

    .line 471
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSuspendDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/16 v7, 0x7da

    invoke-virtual {v6, v7}, Landroid/view/Window;->setType(I)V

    .line 472
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSuspendDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 473
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSuspendDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 457
    :cond_2
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAutomaticResume:Ljava/lang/String;

    const v7, 0x7f060057

    invoke-virtual {p0, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 458
    const v6, 0x7f060173

    invoke-virtual {p0, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 460
    :cond_3
    const v6, 0x7f060174

    invoke-virtual {p0, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public static getInstance()Lcom/google/android/marvin/talkback/TalkBackService;
    .locals 1

    .prologue
    .line 909
    sget-object v0, Lcom/google/android/marvin/talkback/TalkBackService;->sInstance:Lcom/google/android/marvin/talkback/TalkBackService;

    return-object v0
.end method

.method public static getServiceState()I
    .locals 2

    .prologue
    .line 889
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getInstance()Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    .line 890
    .local v0, "service":Lcom/google/android/marvin/talkback/TalkBackService;
    if-nez v0, :cond_0

    .line 891
    const/4 v1, 0x0

    .line 894
    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/google/android/marvin/talkback/TalkBackService;->mServiceState:I

    goto :goto_0
.end method

.method private initializeInfrastructure()V
    .locals 10

    .prologue
    const/16 v9, 0x12

    const/16 v8, 0x10

    .line 924
    invoke-static {p0}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->initialize(Landroid/content/Context;)V

    .line 925
    invoke-static {}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->getInstance()Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    move-result-object v6

    invoke-virtual {v6, p0}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->init(Landroid/content/Context;)V

    .line 927
    new-instance v6, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;

    new-instance v7, Lcom/google/android/marvin/talkback/TalkBackService$4;

    invoke-direct {v7, p0}, Lcom/google/android/marvin/talkback/TalkBackService$4;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    invoke-direct {v6, p0, v7}, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;-><init>(Landroid/content/Context;Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$OnDefaultsLoadedListener;)V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefProxy:Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;

    .line 936
    const-string v6, "accessibility"

    invoke-virtual {p0, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/accessibility/AccessibilityManager;

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 937
    const-string v6, "power"

    invoke-virtual {p0, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/PowerManager;

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPowerManager:Landroid/os/PowerManager;

    .line 940
    new-instance v6, Lcom/google/android/marvin/talkback/CachedFeedbackController;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/CachedFeedbackController;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    .line 942
    new-instance v6, Lcom/google/android/marvin/talkback/SpeechController;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/SpeechController;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    .line 944
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v6, v8, :cond_0

    .line 945
    new-instance v6, Lcom/google/android/marvin/talkback/CursorController;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/CursorController;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    .line 946
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityEventListeners:Ljava/util/LinkedList;

    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v6, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 949
    :cond_0
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v6, v8, :cond_1

    .line 950
    new-instance v6, Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    .line 951
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityEventListeners:Ljava/util/LinkedList;

    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-virtual {v6, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 954
    :cond_1
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v6, v8, :cond_2

    .line 955
    new-instance v6, Lcom/google/android/marvin/talkback/ShakeDetector;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/ShakeDetector;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    .line 958
    :cond_2
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v6, v8, :cond_3

    .line 959
    new-instance v6, Lcom/google/android/marvin/talkback/SideTapManager;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/SideTapManager;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSideTapManager:Lcom/google/android/marvin/talkback/SideTapManager;

    .line 960
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityEventListeners:Ljava/util/LinkedList;

    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSideTapManager:Lcom/google/android/marvin/talkback/SideTapManager;

    invoke-virtual {v6, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 965
    :cond_3
    new-instance v6, Lcom/google/android/marvin/talkback/ProcessorEventQueue;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/ProcessorEventQueue;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorEventQueue:Lcom/google/android/marvin/talkback/ProcessorEventQueue;

    .line 966
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorEventQueue:Lcom/google/android/marvin/talkback/ProcessorEventQueue;

    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mTestingListener:Lcom/google/android/marvin/talkback/test/TalkBackListener;

    invoke-virtual {v6, v7}, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->setTestingListener(Lcom/google/android/marvin/talkback/test/TalkBackListener;)V

    .line 968
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityEventListeners:Ljava/util/LinkedList;

    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorEventQueue:Lcom/google/android/marvin/talkback/ProcessorEventQueue;

    invoke-virtual {v6, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 969
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityEventListeners:Ljava/util/LinkedList;

    new-instance v7, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;

    invoke-direct {v7, p0}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    invoke-virtual {v6, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 971
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0xe

    if-lt v6, v7, :cond_4

    .line 972
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityEventListeners:Ljava/util/LinkedList;

    new-instance v7, Lcom/google/android/marvin/talkback/ProcessorLongHover;

    invoke-direct {v7, p0}, Lcom/google/android/marvin/talkback/ProcessorLongHover;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    invoke-virtual {v6, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 975
    :cond_4
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v6, v8, :cond_5

    .line 976
    new-instance v6, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorFollowFocus:Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;

    .line 977
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityEventListeners:Ljava/util/LinkedList;

    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorFollowFocus:Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;

    invoke-virtual {v6, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 978
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    if-eqz v6, :cond_5

    .line 979
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorFollowFocus:Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;

    invoke-virtual {v6, v7}, Lcom/google/android/marvin/talkback/CursorController;->addScrollListener(Lcom/google/android/marvin/talkback/CursorController$ScrollListener;)V

    .line 983
    :cond_5
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v6, v8, :cond_6

    .line 984
    new-instance v6, Lcom/google/android/marvin/talkback/VolumeMonitor;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/VolumeMonitor;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    .line 987
    :cond_6
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v6, v9, :cond_7

    .line 988
    new-instance v6, Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;

    invoke-direct {v6}, Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;-><init>()V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPackageReceiver:Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;

    .line 991
    :cond_7
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x11

    if-lt v6, v7, :cond_8

    .line 992
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityEventListeners:Ljava/util/LinkedList;

    new-instance v7, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;

    invoke-direct {v7}, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;-><init>()V

    invoke-virtual {v6, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 995
    :cond_8
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v6, v8, :cond_9

    .line 996
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityEventListeners:Ljava/util/LinkedList;

    new-instance v7, Lcom/google/android/marvin/talkback/ProcessorWebContent;

    invoke-direct {v7, p0}, Lcom/google/android/marvin/talkback/ProcessorWebContent;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    invoke-virtual {v6, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 999
    :cond_9
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v6, v9, :cond_a

    .line 1000
    new-instance v4, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;

    invoke-direct {v4, p0}, Lcom/google/android/marvin/talkback/ProcessorVolumeStream;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    .line 1001
    .local v4, "processorVolumeStream":Lcom/google/android/marvin/talkback/ProcessorVolumeStream;
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityEventListeners:Ljava/util/LinkedList;

    invoke-virtual {v6, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1002
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mKeyEventListeners:Ljava/util/LinkedList;

    invoke-virtual {v6, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1005
    .end local v4    # "processorVolumeStream":Lcom/google/android/marvin/talkback/ProcessorVolumeStream;
    :cond_a
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v6, v9, :cond_b

    .line 1006
    new-instance v6, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    invoke-direct {v6, p0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    .line 1007
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityEventListeners:Ljava/util/LinkedList;

    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    invoke-virtual {v6, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1010
    :cond_b
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v6, v9, :cond_d

    .line 1011
    new-instance v2, Lcom/google/android/marvin/talkback/KeyComboManager;

    invoke-direct {v2}, Lcom/google/android/marvin/talkback/KeyComboManager;-><init>()V

    .line 1012
    .local v2, "keyComboManager":Lcom/google/android/marvin/talkback/KeyComboManager;
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mKeyComboListener:Lcom/google/android/marvin/talkback/KeyComboManager$KeyComboListener;

    invoke-virtual {v2, v6}, Lcom/google/android/marvin/talkback/KeyComboManager;->addListener(Lcom/google/android/marvin/talkback/KeyComboManager$KeyComboListener;)V

    .line 1014
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v6, v9, :cond_c

    .line 1015
    new-instance v6, Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    invoke-direct {v6, p0, v7}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mKeyboardSearchManager:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    .line 1016
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mKeyEventListeners:Ljava/util/LinkedList;

    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mKeyboardSearchManager:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    invoke-virtual {v6, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1017
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityEventListeners:Ljava/util/LinkedList;

    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mKeyboardSearchManager:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    invoke-virtual {v6, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1018
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mKeyboardSearchManager:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    invoke-virtual {v2, v6}, Lcom/google/android/marvin/talkback/KeyComboManager;->addListener(Lcom/google/android/marvin/talkback/KeyComboManager$KeyComboListener;)V

    .line 1020
    :cond_c
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2, v6}, Lcom/google/android/marvin/talkback/KeyComboManager;->addListener(Lcom/google/android/marvin/talkback/KeyComboManager$KeyComboListener;)V

    .line 1021
    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/KeyComboManager;->loadDefaultCombos()V

    .line 1022
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mKeyEventListeners:Ljava/util/LinkedList;

    invoke-virtual {v6, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1025
    .end local v2    # "keyComboManager":Lcom/google/android/marvin/talkback/KeyComboManager;
    :cond_d
    new-instance v6, Lcom/google/android/marvin/talkback/OrientationMonitor;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/OrientationMonitor;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mOrientationMonitor:Lcom/google/android/marvin/talkback/OrientationMonitor;

    .line 1027
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 1028
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    const-string v6, "android.hardware.telephony"

    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    .line 1032
    .local v1, "deviceIsPhone":Z
    if-eqz v1, :cond_e

    .line 1033
    new-instance v6, Lcom/google/android/marvin/talkback/CallStateMonitor;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/CallStateMonitor;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    .line 1036
    :cond_e
    const-string v6, "android.hardware.touchscreen"

    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 1039
    .local v0, "deviceHasTouchscreen":Z
    if-nez v1, :cond_f

    if-eqz v0, :cond_10

    .line 1043
    :cond_f
    new-instance v6, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRingerModeAndScreenMonitor:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    .line 1047
    :cond_10
    new-instance v5, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    .line 1048
    .local v5, "radialMenuClient":Lcom/google/android/marvin/talkback/TalkBackRadialMenuClient;
    new-instance v6, Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/RadialMenuManager;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    .line 1049
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-virtual {v6, v5}, Lcom/google/android/marvin/talkback/RadialMenuManager;->setClient(Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;)V

    .line 1051
    new-instance v6, Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/TalkBackAnalytics;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAnalytics:Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    .line 1052
    return-void
.end method

.method public static isServiceActive()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 902
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getServiceState()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maintainExplorationState(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 1346
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    .line 1348
    .local v0, "eventType":I
    const/16 v1, 0x200

    if-ne v0, v1, :cond_1

    .line 1349
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mIsUserTouchExploring:Z

    .line 1355
    :cond_0
    :goto_0
    return-void

    .line 1350
    :cond_1
    const/16 v1, 0x400

    if-ne v0, v1, :cond_2

    .line 1351
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mIsUserTouchExploring:Z

    goto :goto_0

    .line 1352
    :cond_2
    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    .line 1353
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLastWindowStateChanged:J

    goto :goto_0
.end method

.method private onTouchExplorationEnabled()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1511
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "first_time_user"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1528
    :cond_0
    :goto_0
    return-void

    .line 1515
    :cond_1
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1516
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "first_time_user"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1517
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1519
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v1, v3, Landroid/content/res/Configuration;->touchscreen:I

    .line 1521
    .local v1, "touchscreenState":I
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v3, v4, :cond_0

    if-eq v1, v5, :cond_0

    .line 1523
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1524
    .local v2, "tutorial":Landroid/content/Intent;
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1525
    const/high16 v3, 0x4000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1526
    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private performGestureAction(Lcom/google/android/marvin/talkback/ShortcutGestureAction;)V
    .locals 3
    .param p1, "value"    # Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    .prologue
    .line 785
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.marvin.talkback.tutorial.GestureActionPerformedAction"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 786
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.google.android.marvin.talkback.tutorial.ShortcutGestureExtraAction"

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 787
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 789
    sget-object v1, Lcom/google/android/marvin/talkback/TalkBackService$11;->$SwitchMap$com$google$android$marvin$talkback$ShortcutGestureAction:[I

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 819
    :goto_0
    return-void

    .line 791
    :pswitch_0
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->performGlobalAction(Landroid/accessibilityservice/AccessibilityService;I)Z

    goto :goto_0

    .line 794
    :pswitch_1
    const/4 v1, 0x2

    invoke-static {p0, v1}, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->performGlobalAction(Landroid/accessibilityservice/AccessibilityService;I)Z

    goto :goto_0

    .line 797
    :pswitch_2
    const/4 v1, 0x3

    invoke-static {p0, v1}, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->performGlobalAction(Landroid/accessibilityservice/AccessibilityService;I)Z

    goto :goto_0

    .line 800
    :pswitch_3
    const/4 v1, 0x4

    invoke-static {p0, v1}, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->performGlobalAction(Landroid/accessibilityservice/AccessibilityService;I)Z

    goto :goto_0

    .line 804
    :pswitch_4
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    const/high16 v2, 0x7f0f0000

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/RadialMenuManager;->showRadialMenu(I)Z

    goto :goto_0

    .line 807
    :pswitch_5
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    const v2, 0x7f0f0001

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/RadialMenuManager;->showRadialMenu(I)Z

    goto :goto_0

    .line 810
    :pswitch_6
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/FullScreenReadController;->startReadingFromBeginning()V

    goto :goto_0

    .line 813
    :pswitch_7
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/FullScreenReadController;->startReadingFromNextNode()V

    goto :goto_0

    .line 816
    :pswitch_8
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getRootInActiveWindow()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;-><init>(Ljava/lang/Object;)V

    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/TreeDebug;->logNodeTree(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 789
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private processEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 1377
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityEventListeners:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;

    .line 1378
    .local v0, "eventProcessor":Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;
    invoke-interface {v0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;->onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0

    .line 1380
    .end local v0    # "eventProcessor":Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;
    :cond_0
    return-void
.end method

.method private registerTouchSettingObserver(Landroid/content/ContentResolver;)V
    .locals 3
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 914
    const-string v1, "touch_exploration_enabled"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 915
    .local v0, "uri":Landroid/net/Uri;
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mTouchExploreObserver:Landroid/database/ContentObserver;

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 916
    return-void
.end method

.method private reloadPreferences()V
    .locals 12

    .prologue
    .line 1413
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefProxy:Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;

    invoke-virtual {v9}, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->loadOverridesIfNeeded()Z

    .line 1415
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1417
    .local v2, "res":Landroid/content/res/Resources;
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v10, 0x7f060021

    const v11, 0x7f0b0001

    invoke-static {v9, v2, v10, v11}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v9

    iput-boolean v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeakWhenScreenOff:Z

    .line 1419
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v10, 0x7f060034

    const v11, 0x7f0b000a

    invoke-static {v9, v2, v10, v11}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v9

    iput-boolean v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeakCallerId:Z

    .line 1422
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v10, 0x7f060033

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const v11, 0x7f060055

    invoke-virtual {p0, v11}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAutomaticResume:Ljava/lang/String;

    .line 1425
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v10, 0x7f060022

    const v11, 0x7f0b0002

    invoke-static {v9, v2, v10, v11}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v4

    .line 1427
    .local v4, "silenceOnProximity":Z
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {v9, v4}, Lcom/google/android/marvin/talkback/SpeechController;->setSilenceOnProximity(Z)V

    .line 1429
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v10, 0x7f06002b

    const v11, 0x7f06006b

    invoke-static {v9, v2, v10, v11}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getIntFromStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)I

    move-result v0

    .line 1431
    .local v0, "logLevel":I
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/LogUtils;->setLogLevel(I)V

    .line 1433
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorFollowFocus:Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;

    if-eqz v9, :cond_0

    .line 1434
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v10, 0x7f060038

    const v11, 0x7f0b000d

    invoke-static {v9, v2, v10, v11}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v7

    .line 1437
    .local v7, "useSingleTap":Z
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorFollowFocus:Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;

    invoke-virtual {v9, v7}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->setSingleTapEnabled(Z)V

    .line 1440
    invoke-static {v7}, Lcom/google/android/marvin/talkback/speechrules/NodeHintRule$NodeHintHelper;->updateActionResId(Z)V

    .line 1443
    .end local v7    # "useSingleTap":Z
    :cond_0
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    if-eqz v9, :cond_2

    .line 1444
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v10, 0x7f06003a

    const v11, 0x7f06006f

    invoke-static {v9, v2, v10, v11}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getIntFromStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)I

    move-result v3

    .line 1447
    .local v3, "shakeThreshold":I
    if-lez v3, :cond_6

    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    invoke-virtual {v9}, Lcom/google/android/marvin/talkback/CallStateMonitor;->getCurrentCallState()I

    move-result v9

    if-nez v9, :cond_6

    :cond_1
    const/4 v6, 0x1

    .line 1450
    .local v6, "useShake":Z
    :goto_0
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    invoke-virtual {v9, v6}, Lcom/google/android/marvin/talkback/ShakeDetector;->setEnabled(Z)V

    .line 1453
    .end local v3    # "shakeThreshold":I
    .end local v6    # "useShake":Z
    :cond_2
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSideTapManager:Lcom/google/android/marvin/talkback/SideTapManager;

    if-eqz v9, :cond_3

    .line 1454
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSideTapManager:Lcom/google/android/marvin/talkback/SideTapManager;

    invoke-virtual {v9}, Lcom/google/android/marvin/talkback/SideTapManager;->onReloadPreferences()V

    .line 1457
    :cond_3
    sget-boolean v9, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_TOUCH_PREF:Z

    if-eqz v9, :cond_4

    .line 1458
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v10, 0x7f060035

    const v11, 0x7f0b000b

    invoke-static {v9, v2, v10, v11}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v5

    .line 1460
    .local v5, "touchExploration":Z
    invoke-direct {p0, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->requestTouchExploration(Z)V

    .line 1462
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v10, 0x7f06004b

    const v11, 0x7f06007b

    invoke-static {v9, v2, v10, v11}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Ljava/lang/String;

    move-result-object v8

    .line 1465
    .local v8, "verticalGesturesPref":Ljava/lang/String;
    const v9, 0x7f060067

    invoke-virtual {p0, v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    iput-boolean v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVerticalGestureCycleGranularity:Z

    .line 1469
    .end local v5    # "touchExploration":Z
    .end local v8    # "verticalGesturesPref":Ljava/lang/String;
    :cond_4
    sget-boolean v9, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_WEB_SCRIPT_TOGGLE:Z

    if-eqz v9, :cond_5

    .line 1470
    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v10, 0x7f060032

    const v11, 0x7f0b0009

    invoke-static {v9, v2, v10, v11}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v1

    .line 1472
    .local v1, "requestWebScripts":Z
    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->requestWebScripts(Z)V

    .line 1474
    .end local v1    # "requestWebScripts":Z
    :cond_5
    return-void

    .line 1447
    .restart local v3    # "shakeThreshold":I
    :cond_6
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private requestTouchExploration(Z)V
    .locals 5
    .param p1, "requestedState"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1485
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getServiceInfo()Landroid/accessibilityservice/AccessibilityServiceInfo;

    move-result-object v1

    .line 1486
    .local v1, "info":Landroid/accessibilityservice/AccessibilityServiceInfo;
    if-nez v1, :cond_1

    .line 1487
    const/4 v2, 0x6

    const-string v3, "Failed to change touch exploration request state, service info was null"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 1505
    :cond_0
    :goto_0
    return-void

    .line 1492
    :cond_1
    iget v2, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    .line 1494
    .local v0, "currentState":Z
    :cond_2
    if-eq v0, p1, :cond_0

    .line 1498
    if-eqz p1, :cond_3

    .line 1499
    iget v2, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    .line 1504
    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->setServiceInfo(Landroid/accessibilityservice/AccessibilityServiceInfo;)V

    goto :goto_0

    .line 1501
    :cond_3
    iget v2, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    and-int/lit8 v2, v2, -0x5

    iput v2, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    goto :goto_1
.end method

.method private requestWebScripts(Z)V
    .locals 5
    .param p1, "requestedState"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1540
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getServiceInfo()Landroid/accessibilityservice/AccessibilityServiceInfo;

    move-result-object v1

    .line 1541
    .local v1, "info":Landroid/accessibilityservice/AccessibilityServiceInfo;
    if-nez v1, :cond_1

    .line 1542
    const/4 v2, 0x6

    const-string v3, "Failed to change web script injection request state, service info was null"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 1561
    :cond_0
    :goto_0
    return-void

    .line 1547
    :cond_1
    iget v2, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    .line 1550
    .local v0, "currentState":Z
    :cond_2
    if-eq v0, p1, :cond_0

    .line 1554
    if-eqz p1, :cond_3

    .line 1555
    iget v2, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    or-int/lit8 v2, v2, 0x8

    iput v2, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    .line 1560
    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->setServiceInfo(Landroid/accessibilityservice/AccessibilityServiceInfo;)V

    goto :goto_0

    .line 1557
    :cond_3
    iget v2, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    and-int/lit8 v2, v2, -0x9

    iput v2, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    goto :goto_1
.end method

.method private resumeInfrastructure()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 1060
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1061
    const/4 v4, 0x6

    const-string v5, "Attempted to resume while not suspended"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p0, v4, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 1125
    :goto_0
    return-void

    .line 1065
    :cond_0
    invoke-direct {p0, v8}, Lcom/google/android/marvin/talkback/TalkBackService;->setServiceState(I)V

    .line 1066
    invoke-virtual {p0, v8}, Lcom/google/android/marvin/talkback/TalkBackService;->stopForeground(Z)V

    .line 1068
    new-instance v1, Landroid/accessibilityservice/AccessibilityServiceInfo;

    invoke-direct {v1}, Landroid/accessibilityservice/AccessibilityServiceInfo;-><init>()V

    .line 1069
    .local v1, "info":Landroid/accessibilityservice/AccessibilityServiceInfo;
    const/4 v4, -0x1

    iput v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->eventTypes:I

    .line 1070
    iget v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    .line 1071
    iget v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    .line 1072
    iget v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    .line 1073
    iget v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    .line 1074
    iget v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    or-int/lit8 v4, v4, 0x8

    iput v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    .line 1075
    iget v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    or-int/lit8 v4, v4, 0x10

    iput v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    .line 1076
    iget v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    or-int/lit8 v4, v4, 0x20

    iput v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    .line 1077
    const-wide/16 v4, 0x0

    iput-wide v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->notificationTimeout:J

    .line 1080
    sget-boolean v4, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_TOUCH_PREF:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f060035

    const v7, 0x7f0b000b

    invoke-static {v4, v5, v6, v7}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1083
    iget v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    .line 1086
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->setServiceInfo(Landroid/accessibilityservice/AccessibilityServiceInfo;)V

    .line 1088
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    if-eqz v4, :cond_2

    .line 1089
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    invoke-virtual {v5}, Lcom/google/android/marvin/talkback/CallStateMonitor;->getFilter()Landroid/content/IntentFilter;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1092
    :cond_2
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRingerModeAndScreenMonitor:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    if-eqz v4, :cond_3

    .line 1093
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRingerModeAndScreenMonitor:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRingerModeAndScreenMonitor:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    invoke-virtual {v5}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->getFilter()Landroid/content/IntentFilter;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1096
    :cond_3
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    if-eqz v4, :cond_4

    .line 1097
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-virtual {v5}, Lcom/google/android/marvin/talkback/RadialMenuManager;->getFilter()Landroid/content/IntentFilter;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1100
    :cond_4
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    if-eqz v4, :cond_5

    .line 1101
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    invoke-virtual {v5}, Lcom/google/android/marvin/talkback/VolumeMonitor;->getFilter()Landroid/content/IntentFilter;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1104
    :cond_5
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPackageReceiver:Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;

    if-eqz v4, :cond_6

    .line 1105
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPackageReceiver:Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPackageReceiver:Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;

    invoke-virtual {v5}, Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;->getFilter()Landroid/content/IntentFilter;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1106
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    if-eqz v4, :cond_6

    .line 1107
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    invoke-virtual {v4}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->ensureDataConsistency()V

    .line 1111
    :cond_6
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSharedPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1114
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1115
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v4, "performCustomGestureAction"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1116
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mActiveReceiver:Landroid/content/BroadcastReceiver;

    const-string v5, "com.google.android.marvin.feedback.permission.TALKBACK"

    const/4 v6, 0x0

    invoke-virtual {p0, v4, v0, v5, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 1119
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1120
    .local v2, "packageManager":Landroid/content/pm/PackageManager;
    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/google/android/marvin/talkback/ShortcutProxyActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1121
    .local v3, "shortcutProxy":Landroid/content/ComponentName;
    invoke-virtual {v2, v3, v8, v8}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 1124
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->reloadPreferences()V

    goto/16 :goto_0
.end method

.method private setServiceState(I)V
    .locals 3
    .param p1, "newState"    # I

    .prologue
    .line 370
    iget v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mServiceState:I

    if-ne v2, p1, :cond_1

    .line 378
    :cond_0
    return-void

    .line 374
    :cond_1
    iput p1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mServiceState:I

    .line 375
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mServiceStateListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;

    .line 376
    .local v1, "listener":Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;
    invoke-interface {v1, p1}, Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;->onServiceStateChanged(I)V

    goto :goto_0
.end method

.method private shouldDropEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 1235
    if-nez p1, :cond_0

    .line 1236
    const/4 v7, 0x1

    .line 1308
    :goto_0
    return v7

    .line 1240
    :cond_0
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1241
    const/4 v7, 0x1

    goto :goto_0

    .line 1245
    :cond_1
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLastSpokenEvent:Landroid/view/accessibility/AccessibilityEvent;

    invoke-static {v7, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->eventEquals(Landroid/view/accessibility/AccessibilityEvent;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1246
    const/4 v7, 0x2

    const-string v8, "Drop duplicate event"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {p0, v7, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 1247
    const/4 v7, 0x1

    goto :goto_0

    .line 1253
    :cond_2
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v7}, Landroid/support/v4/view/accessibility/AccessibilityManagerCompat;->isTouchExplorationEnabled(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v7

    and-int/lit16 v7, v7, 0x100c

    if-eqz v7, :cond_3

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLastWindowStateChanged:J

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x64

    cmp-long v7, v8, v10

    if-gez v7, :cond_3

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackService;->shouldKeepAutomaticEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 1257
    const/4 v7, 0x2

    const-string v8, "Drop event after window state change"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {p0, v7, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 1258
    const/4 v7, 0x1

    goto :goto_0

    .line 1262
    :cond_3
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v7

    const/16 v8, 0x40

    if-ne v7, v8, :cond_4

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getParcelableData()Landroid/os/Parcelable;

    move-result-object v7

    if-eqz v7, :cond_4

    const/4 v1, 0x1

    .line 1266
    .local v1, "isNotification":Z
    :goto_1
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    invoke-virtual {v7}, Lcom/google/android/marvin/talkback/CallStateMonitor;->getCurrentCallState()I

    move-result v7

    if-eqz v7, :cond_5

    const/4 v2, 0x1

    .line 1268
    .local v2, "isPhoneActive":Z
    :goto_2
    iget-boolean v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeakCallerId:Z

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    invoke-virtual {v7}, Lcom/google/android/marvin/talkback/CallStateMonitor;->getCurrentCallState()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_6

    const/4 v4, 0x1

    .line 1272
    .local v4, "shouldSpeakCallerId":Z
    :goto_3
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v7}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v7

    if-nez v7, :cond_8

    if-nez v4, :cond_8

    .line 1273
    iget-boolean v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeakWhenScreenOff:Z

    if-nez v7, :cond_7

    .line 1276
    const/4 v7, 0x2

    const-string v8, "Drop event due to screen state and user pref"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {p0, v7, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 1277
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 1262
    .end local v1    # "isNotification":Z
    .end local v2    # "isPhoneActive":Z
    .end local v4    # "shouldSpeakCallerId":Z
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 1266
    .restart local v1    # "isNotification":Z
    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    .line 1268
    .restart local v2    # "isPhoneActive":Z
    :cond_6
    const/4 v4, 0x0

    goto :goto_3

    .line 1278
    .restart local v4    # "shouldSpeakCallerId":Z
    :cond_7
    if-nez v1, :cond_8

    .line 1281
    const/4 v7, 0x2

    const-string v8, "Drop non-notification event due to screen state"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {p0, v7, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 1282
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 1286
    :cond_8
    const v7, 0x28080

    invoke-static {p1, v7}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->eventMatchesAnyType(Landroid/view/accessibility/AccessibilityEvent;I)Z

    move-result v0

    .line 1288
    .local v0, "canInterruptRadialMenu":Z
    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-virtual {v7}, Lcom/google/android/marvin/talkback/RadialMenuManager;->isRadialMenuShowing()Z

    move-result v7

    if-eqz v7, :cond_9

    if-nez v0, :cond_9

    const/4 v5, 0x1

    .line 1292
    .local v5, "silencedByRadialMenu":Z
    :goto_4
    if-eqz v5, :cond_a

    .line 1293
    const/4 v7, 0x2

    const-string v8, "Drop event due to radial menu state"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {p0, v7, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 1294
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 1288
    .end local v5    # "silencedByRadialMenu":Z
    :cond_9
    const/4 v5, 0x0

    goto :goto_4

    .line 1298
    .restart local v5    # "silencedByRadialMenu":Z
    :cond_a
    if-eqz v1, :cond_c

    iget-boolean v7, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mIsUserTouchExploring:Z

    if-nez v7, :cond_b

    if-eqz v2, :cond_c

    .line 1299
    :cond_b
    const/4 v7, 0x2

    const-string v8, "Drop notification due to touch or phone state"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {p0, v7, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 1300
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 1303
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v6, v7, Landroid/content/res/Configuration;->touchscreen:I

    .line 1304
    .local v6, "touchscreenState":I
    const v7, 0x3c0780

    invoke-static {p1, v7}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->eventMatchesAnyType(Landroid/view/accessibility/AccessibilityEvent;I)Z

    move-result v3

    .line 1308
    .local v3, "isTouchInteractionStateChange":Z
    const/4 v7, 0x1

    if-ne v6, v7, :cond_d

    if-eqz v3, :cond_d

    const/4 v7, 0x1

    goto/16 :goto_0

    :cond_d
    const/4 v7, 0x0

    goto/16 :goto_0
.end method

.method private shouldKeepAutomaticEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1320
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v1, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 1323
    .local v1, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v4

    const/16 v5, 0x8

    if-ne v4, v5, :cond_1

    .line 1324
    const/4 v0, 0x0

    .line 1327
    .local v0, "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_start_0
    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 1328
    const-class v4, Landroid/widget/EditText;

    invoke-static {p0, v0, v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_0

    .line 1333
    new-array v4, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v4, v3

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 1337
    .end local v0    # "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :goto_0
    return v2

    .line 1333
    .restart local v0    # "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_0
    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .end local v0    # "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_1
    move v2, v3

    .line 1337
    goto :goto_0

    .line 1333
    .restart local v0    # "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :catchall_0
    move-exception v4

    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v4
.end method

.method private shutdownInfrastructure()V
    .locals 1

    .prologue
    .line 1209
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    if-eqz v0, :cond_0

    .line 1210
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/CursorController;->shutdown()V

    .line 1213
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    if-eqz v0, :cond_1

    .line 1214
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->shutdown()V

    .line 1217
    :cond_1
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    if-eqz v0, :cond_2

    .line 1218
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->shutdown()V

    .line 1221
    :cond_2
    invoke-static {}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->getInstance()Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->shutdown()V

    .line 1222
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->shutdown()V

    .line 1223
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/SpeechController;->shutdown()V

    .line 1224
    return-void
.end method

.method private suspendInfrastructure()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 1133
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1134
    const/4 v4, 0x6

    const-string v5, "Attempted to suspend while already suspended"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p0, v4, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 1203
    :cond_0
    :goto_0
    return-void

    .line 1138
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->interruptAllFeedback()V

    .line 1139
    invoke-direct {p0, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->setServiceState(I)V

    .line 1142
    sget-boolean v4, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_TOUCH_PREF:Z

    if-eqz v4, :cond_2

    .line 1143
    invoke-direct {p0, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->requestTouchExploration(Z)V

    .line 1146
    :cond_2
    sget-boolean v4, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_WEB_SCRIPT_TOGGLE:Z

    if-eqz v4, :cond_3

    .line 1147
    invoke-direct {p0, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->requestWebScripts(Z)V

    .line 1150
    :cond_3
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSharedPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1152
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mActiveReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1154
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    if-eqz v4, :cond_4

    .line 1155
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1158
    :cond_4
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRingerModeAndScreenMonitor:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    if-eqz v4, :cond_5

    .line 1159
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRingerModeAndScreenMonitor:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1162
    :cond_5
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    if-eqz v4, :cond_6

    .line 1163
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1164
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/RadialMenuManager;->clearCache()V

    .line 1167
    :cond_6
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    if-eqz v4, :cond_7

    .line 1168
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1169
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/VolumeMonitor;->releaseControl()V

    .line 1172
    :cond_7
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPackageReceiver:Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;

    if-eqz v4, :cond_8

    .line 1173
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPackageReceiver:Lcom/googlecode/eyesfree/labeling/PackageRemovalReceiver;

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1176
    :cond_8
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    if-eqz v4, :cond_9

    .line 1177
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    invoke-virtual {v4, v6}, Lcom/google/android/marvin/talkback/ShakeDetector;->setEnabled(Z)V

    .line 1181
    :cond_9
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSideTapManager:Lcom/google/android/marvin/talkback/SideTapManager;

    if-eqz v4, :cond_a

    .line 1182
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSideTapManager:Lcom/google/android/marvin/talkback/SideTapManager;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/SideTapManager;->onSuspendInfrastructure()V

    .line 1185
    :cond_a
    sget-boolean v4, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_TOUCH_PREF:Z

    if-eqz v4, :cond_b

    .line 1186
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1187
    .local v2, "resolver":Landroid/content/ContentResolver;
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mTouchExploreObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v4}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1191
    .end local v2    # "resolver":Landroid/content/ContentResolver;
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1192
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/google/android/marvin/talkback/ShortcutProxyActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1193
    .local v3, "shortcutProxy":Landroid/content/ComponentName;
    const/4 v4, 0x1

    invoke-virtual {v1, v3, v7, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 1197
    const-string v4, "notification"

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1198
    .local v0, "nm":Landroid/app/NotificationManager;
    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    .line 1200
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    if-eqz v4, :cond_0

    .line 1201
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/TextToSpeechOverlay;->hide()V

    goto/16 :goto_0
.end method


# virtual methods
.method public addEventListener(Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;

    .prologue
    .line 1388
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityEventListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1389
    return-void
.end method

.method public addServiceStateListener(Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;

    .prologue
    .line 381
    if-eqz p1, :cond_0

    .line 382
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mServiceStateListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384
    :cond_0
    return-void
.end method

.method public getAnalytics()Lcom/google/android/marvin/talkback/TalkBackAnalytics;
    .locals 2

    .prologue
    .line 702
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAnalytics:Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    if-nez v0, :cond_0

    .line 703
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mAnalytics has not been initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 706
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAnalytics:Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    return-object v0
.end method

.method public getCursorController()Lcom/google/android/marvin/talkback/CursorController;
    .locals 2

    .prologue
    .line 678
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    if-nez v0, :cond_0

    .line 679
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mCursorController has not been initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 682
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    return-object v0
.end method

.method public getFeedbackController()Lcom/google/android/marvin/talkback/CachedFeedbackController;
    .locals 2

    .prologue
    .line 670
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    if-nez v0, :cond_0

    .line 671
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mFeedbackController has not been initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 674
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    return-object v0
.end method

.method public getFullScreenReadController()Lcom/google/android/marvin/talkback/FullScreenReadController;
    .locals 2

    .prologue
    .line 686
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    if-nez v0, :cond_0

    .line 687
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mFullScreenReadController has not been initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 690
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    return-object v0
.end method

.method public getLabelManager()Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    .locals 2

    .prologue
    .line 694
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 695
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mLabelManager has not been initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 698
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    return-object v0
.end method

.method public getShakeDetector()Lcom/google/android/marvin/talkback/ShakeDetector;
    .locals 1

    .prologue
    .line 715
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    return-object v0
.end method

.method public getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;
    .locals 2

    .prologue
    .line 662
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    if-nez v0, :cond_0

    .line 663
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mSpeechController has not been initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 666
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    return-object v0
.end method

.method public interruptAllFeedback()V
    .locals 2

    .prologue
    .line 830
    invoke-static {}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->isTutorialActive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 856
    :cond_0
    :goto_0
    return-void

    .line 835
    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    if-eqz v1, :cond_2

    .line 836
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 837
    .local v0, "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasLegacyWebContent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 838
    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->isScriptInjectionEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 839
    const/4 v1, -0x3

    invoke-static {v0, v1}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performSpecialAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Z

    .line 845
    .end local v0    # "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_2
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    if-eqz v1, :cond_3

    .line 846
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/FullScreenReadController;->interrupt()V

    .line 849
    :cond_3
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    if-eqz v1, :cond_4

    .line 850
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/SpeechController;->interrupt()V

    .line 853
    :cond_4
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    if-eqz v1, :cond_0

    .line 854
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->interrupt()V

    goto :goto_0
.end method

.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mTestingListener:Lcom/google/android/marvin/talkback/test/TalkBackListener;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mTestingListener:Lcom/google/android/marvin/talkback/test/TalkBackListener;

    invoke-interface {v0, p1}, Lcom/google/android/marvin/talkback/test/TalkBackListener;->onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 355
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackService;->shouldDropEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 367
    :goto_0
    return-void

    .line 359
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackService;->maintainExplorationState(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 362
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_2

    .line 363
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackService;->cacheEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 366
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackService;->processEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 341
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mOrientationMonitor:Lcom/google/android/marvin/talkback/OrientationMonitor;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mOrientationMonitor:Lcom/google/android/marvin/talkback/OrientationMonitor;

    invoke-virtual {v0, p1}, Lcom/google/android/marvin/talkback/OrientationMonitor;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/RadialMenuManager;->clearCache()V

    .line 347
    return-void
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 310
    invoke-super {p0}, Landroid/accessibilityservice/AccessibilityService;->onCreate()V

    .line 312
    sput-object p0, Lcom/google/android/marvin/talkback/TalkBackService;->sInstance:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 313
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->setServiceState(I)V

    .line 315
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    .line 317
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSystemUeh:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 318
    invoke-static {p0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 320
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->initializeInfrastructure()V

    .line 321
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 325
    invoke-super {p0}, Landroid/accessibilityservice/AccessibilityService;->onDestroy()V

    .line 327
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->suspendInfrastructure()V

    .line 331
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/marvin/talkback/TalkBackService;->sInstance:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 334
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->shutdownInfrastructure()V

    .line 335
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->setServiceState(I)V

    .line 336
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mServiceStateListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 337
    return-void
.end method

.method protected onGesture(I)Z
    .locals 8
    .param p1, "gestureId"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 552
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v4

    if-nez v4, :cond_0

    .line 657
    :goto_0
    return v2

    .line 557
    :cond_0
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mKeyboardSearchManager:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    invoke-virtual {v4, p1}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->onGesture(I)Z

    move-result v4

    if-eqz v4, :cond_1

    move v2, v3

    .line 558
    goto :goto_0

    .line 561
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getAnalytics()Lcom/google/android/marvin/talkback/TalkBackAnalytics;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/google/android/marvin/talkback/TalkBackAnalytics;->onGesture(I)V

    .line 563
    const/4 v4, 0x2

    const-string v5, "Recognized gesture %s"

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {p0, v4, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 565
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    const v4, 0x7f050007

    invoke-virtual {v2, v4}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(I)Z

    .line 570
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-gt v2, v4, :cond_2

    .line 571
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->interruptAllFeedback()V

    .line 574
    :cond_2
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/RadialMenuManager;->dismissAll()V

    .line 576
    const/4 v0, 0x1

    .line 577
    .local v0, "handled":Z
    const/4 v1, 0x0

    .line 580
    .local v1, "result":Z
    packed-switch p1, :pswitch_data_0

    .line 611
    const/4 v0, 0x0

    .line 614
    :goto_1
    if-eqz v0, :cond_6

    .line 615
    if-nez v1, :cond_3

    .line 616
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    const v4, 0x7f050003

    invoke-virtual {v2, v4}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(I)Z

    :cond_3
    move v2, v3

    .line 618
    goto :goto_0

    .line 583
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2, v3, v3}, Lcom/google/android/marvin/talkback/CursorController;->previous(ZZ)Z

    move-result v1

    .line 584
    goto :goto_1

    .line 587
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2, v3, v3}, Lcom/google/android/marvin/talkback/CursorController;->next(ZZ)Z

    move-result v1

    .line 588
    goto :goto_1

    .line 591
    :pswitch_2
    iget-boolean v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVerticalGestureCycleGranularity:Z

    if-eqz v2, :cond_4

    .line 592
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/CursorController;->previousGranularity()Z

    move-result v1

    goto :goto_1

    .line 594
    :cond_4
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/CursorController;->jumpToTop()Z

    move-result v1

    .line 596
    goto :goto_1

    .line 598
    :pswitch_3
    iget-boolean v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVerticalGestureCycleGranularity:Z

    if-eqz v2, :cond_5

    .line 599
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/CursorController;->nextGranularity()Z

    move-result v1

    goto :goto_1

    .line 601
    :cond_5
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/CursorController;->jumpToBottom()Z

    move-result v1

    .line 603
    goto :goto_1

    .line 605
    :pswitch_4
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/CursorController;->less()Z

    move-result v1

    .line 606
    goto :goto_1

    .line 608
    :pswitch_5
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/CursorController;->more()Z

    move-result v1

    .line 609
    goto :goto_1

    .line 622
    :cond_6
    packed-switch p1, :pswitch_data_1

    move v2, v3

    .line 657
    goto/16 :goto_0

    .line 624
    :pswitch_6
    const v2, 0x7f060044

    const v4, 0x7f060072

    invoke-virtual {p0, v2, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    move v2, v3

    .line 626
    goto/16 :goto_0

    .line 628
    :pswitch_7
    const v2, 0x7f060045

    const v4, 0x7f060073

    invoke-virtual {p0, v2, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    move v2, v3

    .line 630
    goto/16 :goto_0

    .line 632
    :pswitch_8
    const v2, 0x7f060042

    const v4, 0x7f060070

    invoke-virtual {p0, v2, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    move v2, v3

    .line 634
    goto/16 :goto_0

    .line 636
    :pswitch_9
    const v2, 0x7f060043

    const v4, 0x7f060071

    invoke-virtual {p0, v2, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    move v2, v3

    .line 638
    goto/16 :goto_0

    .line 640
    :pswitch_a
    const v2, 0x7f060046

    const v4, 0x7f060074

    invoke-virtual {p0, v2, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    move v2, v3

    .line 642
    goto/16 :goto_0

    .line 644
    :pswitch_b
    const v2, 0x7f060047

    const v4, 0x7f060075

    invoke-virtual {p0, v2, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    move v2, v3

    .line 646
    goto/16 :goto_0

    .line 648
    :pswitch_c
    const v2, 0x7f060048

    const v4, 0x7f060076

    invoke-virtual {p0, v2, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    move v2, v3

    .line 650
    goto/16 :goto_0

    .line 652
    :pswitch_d
    const v2, 0x7f060049

    const v4, 0x7f060077

    invoke-virtual {p0, v2, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    move v2, v3

    .line 654
    goto/16 :goto_0

    .line 580
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 622
    :pswitch_data_1
    .packed-switch 0x9
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_8
        :pswitch_9
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onInterrupt()V
    .locals 0

    .prologue
    .line 825
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->interruptAllFeedback()V

    .line 826
    return-void
.end method

.method protected onKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    .line 537
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v3

    if-nez v3, :cond_1

    .line 547
    :cond_0
    :goto_0
    return v2

    .line 541
    :cond_1
    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mKeyEventListeners:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/talkback/TalkBackService$KeyEventListener;

    .line 542
    .local v1, "listener":Lcom/google/android/marvin/talkback/TalkBackService$KeyEventListener;
    invoke-interface {v1, p1}, Lcom/google/android/marvin/talkback/TalkBackService$KeyEventListener;->onKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 543
    const/4 v2, 0x1

    goto :goto_0
.end method

.method protected onServiceConnected()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 860
    const/4 v2, 0x2

    const-string v3, "System bound to service."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 861
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->resumeInfrastructure()V

    .line 864
    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    .line 865
    .local v0, "helper":Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->showPendingNotifications()V

    .line 866
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->checkUpdate()V

    .line 869
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_0

    .line 870
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 871
    .local v1, "resolver":Landroid/content/ContentResolver;
    const-string v2, "touch_exploration_enabled"

    invoke-static {v1, v2, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 873
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->onTouchExplorationEnabled()V

    .line 879
    .end local v1    # "resolver":Landroid/content/ContentResolver;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v3, 0x7f060053

    invoke-virtual {p0, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 880
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->suspendTalkBack()V

    .line 882
    :cond_1
    return-void

    .line 875
    .restart local v1    # "resolver":Landroid/content/ContentResolver;
    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->registerTouchSettingObserver(Landroid/content/ContentResolver;)V

    goto :goto_0
.end method

.method performCustomGesture(II)V
    .locals 5
    .param p1, "keyResId"    # I
    .param p2, "defaultResId"    # I

    .prologue
    .line 770
    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 771
    .local v2, "key":Ljava/lang/String;
    invoke-virtual {p0, p2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 772
    .local v0, "defaultValue":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v4, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 773
    .local v3, "value":Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->safeValueOf(Ljava/lang/String;)Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    move-result-object v1

    .line 775
    .local v1, "gestureAction":Lcom/google/android/marvin/talkback/ShortcutGestureAction;
    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->performGestureAction(Lcom/google/android/marvin/talkback/ShortcutGestureAction;)V

    .line 776
    return-void
.end method

.method postRemoveEventListener(Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;

    .prologue
    .line 1398
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/marvin/talkback/TalkBackService$5;

    invoke-direct {v1, p0, p1}, Lcom/google/android/marvin/talkback/TalkBackService$5;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1404
    return-void
.end method

.method public removeServiceStateListener(Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/marvin/talkback/TalkBackService$ServiceStateListener;

    .prologue
    .line 387
    if-eqz p1, :cond_0

    .line 388
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mServiceStateListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 390
    :cond_0
    return-void
.end method

.method public requestSuspendTalkBack()V
    .locals 5

    .prologue
    .line 404
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060051

    const v4, 0x7f0b000f

    invoke-static {v1, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v0

    .line 407
    .local v0, "showConfirmation":Z
    if-eqz v0, :cond_0

    .line 408
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->confirmSuspendTalkBack()V

    .line 412
    :goto_0
    return-void

    .line 410
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->suspendTalkBack()V

    goto :goto_0
.end method

.method public resetFocusedNode()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 734
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 735
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/google/android/marvin/talkback/TalkBackService$3;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/TalkBackService$3;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 760
    return-void
.end method

.method resumeTalkBack()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 523
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524
    const/4 v0, 0x6

    const-string v1, "Attempted to resume TalkBack when not suspended."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 532
    :goto_0
    return-void

    .line 528
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v1, 0x7f060053

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->storeBooleanAsync(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 530
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSuspendedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 531
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->resumeInfrastructure()V

    goto :goto_0
.end method

.method public saveFocusedNode()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 721
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSavedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v0, :cond_0

    .line 722
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSavedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 725
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/googlecode/eyesfree/utils/FocusFinder;->getFocusedNode(Landroid/accessibilityservice/AccessibilityService;Z)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSavedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 726
    return-void
.end method

.method suspendTalkBack()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 480
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v4

    if-nez v4, :cond_0

    .line 481
    const/4 v4, 0x6

    const-string v5, "Attempted to suspend TalkBack while already suspended."

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {p0, v4, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 517
    :goto_0
    return-void

    .line 485
    :cond_0
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v5, 0x7f060053

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v8}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->storeBooleanAsync(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 486
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    const v5, 0x7f05000b

    invoke-virtual {v4, v5}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(I)Z

    .line 488
    sget-boolean v4, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_TOUCH_PREF:Z

    if-eqz v4, :cond_1

    .line 489
    invoke-direct {p0, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->requestTouchExploration(Z)V

    .line 492
    :cond_1
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    if-eqz v4, :cond_2

    .line 493
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/CursorController;->clearCursor()V

    .line 496
    :cond_2
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 497
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v4, "com.google.android.marvin.talkback.RESUME_FEEDBACK"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 498
    const-string v4, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 499
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSuspendedReceiver:Landroid/content/BroadcastReceiver;

    const-string v5, "com.google.android.marvin.feedback.permission.TALKBACK"

    const/4 v6, 0x0

    invoke-virtual {p0, v4, v0, v5, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 502
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->suspendInfrastructure()V

    .line 504
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.google.android.marvin.talkback.RESUME_FEEDBACK"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 505
    .local v3, "resumeIntent":Landroid/content/Intent;
    invoke-static {p0, v7, v3, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 506
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    new-instance v4, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v4, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f06016f

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const v5, 0x7f060170

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const v5, 0x7f02001c

    invoke-virtual {v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    .line 516
    .local v1, "notification":Landroid/app/Notification;
    const v4, 0x7f0d0001

    invoke-virtual {p0, v4, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->startForeground(ILandroid/app/Notification;)V

    goto/16 :goto_0
.end method

.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "thread"    # Ljava/lang/Thread;
    .param p2, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 1679
    :try_start_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/RadialMenuManager;->isRadialMenuShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1680
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/RadialMenuManager;->dismissAll()V

    .line 1683
    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSuspendDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 1684
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSuspendDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1689
    :cond_1
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSystemUeh:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v0, :cond_2

    .line 1690
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSystemUeh:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 1693
    :cond_2
    :goto_0
    return-void

    .line 1686
    :catch_0
    move-exception v0

    .line 1689
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSystemUeh:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v0, :cond_2

    .line 1690
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSystemUeh:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1689
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSystemUeh:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v1, :cond_3

    .line 1690
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSystemUeh:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v1, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    :cond_3
    throw v0
.end method
