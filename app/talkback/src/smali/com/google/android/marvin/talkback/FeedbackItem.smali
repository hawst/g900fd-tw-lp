.class public Lcom/google/android/marvin/talkback/FeedbackItem;
.super Ljava/lang/Object;
.source "FeedbackItem.java"


# instance fields
.field private mCompletedAction:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

.field private mFlags:I

.field private mFragments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/marvin/talkback/FeedbackFragment;",
            ">;"
        }
    .end annotation
.end field

.field private mIsUninterruptible:Z

.field private mUtteranceId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mUtteranceId:Ljava/lang/String;

    .line 67
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mFragments:Ljava/util/List;

    .line 70
    iput-boolean v1, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mIsUninterruptible:Z

    .line 73
    iput v1, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mFlags:I

    return-void
.end method


# virtual methods
.method public addFlag(I)V
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 192
    iget v0, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mFlags:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mFlags:I

    .line 193
    return-void
.end method

.method public addFragment(Lcom/google/android/marvin/talkback/FeedbackFragment;)V
    .locals 1
    .param p1, "fragment"    # Lcom/google/android/marvin/talkback/FeedbackFragment;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mFragments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    return-void
.end method

.method public addFragmentAtPosition(Lcom/google/android/marvin/talkback/FeedbackFragment;I)V
    .locals 1
    .param p1, "fragment"    # Lcom/google/android/marvin/talkback/FeedbackFragment;
    .param p2, "position"    # I

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mFragments:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 138
    return-void
.end method

.method public getAggregateText()Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 113
    iget-object v3, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mFragments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 114
    const/4 v3, 0x0

    .line 124
    :goto_0
    return-object v3

    .line 115
    :cond_0
    iget-object v3, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mFragments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v6, :cond_1

    .line 116
    iget-object v3, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mFragments:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/marvin/talkback/FeedbackFragment;

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/FeedbackFragment;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_0

    .line 119
    :cond_1
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 120
    .local v2, "sb":Landroid/text/SpannableStringBuilder;
    iget-object v3, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mFragments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/marvin/talkback/FeedbackFragment;

    .line 121
    .local v0, "fragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    new-array v3, v6, [Ljava/lang/CharSequence;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/FeedbackFragment;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_1

    .line 124
    .end local v0    # "fragment":Lcom/google/android/marvin/talkback/FeedbackFragment;
    :cond_2
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public getCompletedAction()Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mCompletedAction:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    return-object v0
.end method

.method public getFragments()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/marvin/talkback/FeedbackFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mFragments:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUtteranceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mUtteranceId:Ljava/lang/String;

    return-object v0
.end method

.method public hasFlag(I)Z
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 183
    iget v0, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mFlags:I

    and-int/2addr v0, p1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUninterruptible()Z
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mIsUninterruptible:Z

    return v0
.end method

.method public removeFragment(Lcom/google/android/marvin/talkback/FeedbackFragment;)Z
    .locals 1
    .param p1, "fragment"    # Lcom/google/android/marvin/talkback/FeedbackFragment;

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mFragments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setCompletedAction(Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V
    .locals 0
    .param p1, "action"    # Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mCompletedAction:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .line 209
    return-void
.end method

.method public setUninterruptible(Z)V
    .locals 0
    .param p1, "isUninterruptible"    # Z

    .prologue
    .line 172
    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mIsUninterruptible:Z

    .line 173
    return-void
.end method

.method public setUtteranceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mUtteranceId:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 213
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{utteranceId:\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mUtteranceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\", fragments:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mFragments:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uninterruptible:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mIsUninterruptible:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", flags:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/marvin/talkback/FeedbackItem;->mFlags:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
