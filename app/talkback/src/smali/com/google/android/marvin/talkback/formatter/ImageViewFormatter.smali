.class public Lcom/google/android/marvin/talkback/formatter/ImageViewFormatter;
.super Ljava/lang/Object;
.source "ImageViewFormatter.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# instance fields
.field private ruleNonTextViews:Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;

    invoke-direct {v0}, Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/formatter/ImageViewFormatter;->ruleNonTextViews:Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;

    .line 42
    return-void
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    .line 46
    new-instance v0, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v0, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 47
    .local v0, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 48
    .local v1, "source":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    iget-object v3, p0, Lcom/google/android/marvin/talkback/formatter/ImageViewFormatter;->ruleNonTextViews:Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;

    invoke-virtual {v3, p2, v1, p1}, Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;->format(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 49
    .local v2, "text":Ljava/lang/CharSequence;
    invoke-virtual {p3, v2}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 50
    const/4 v3, 0x1

    return v3
.end method
