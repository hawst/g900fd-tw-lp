.class public Lcom/google/android/marvin/talkback/FullScreenReadController;
.super Ljava/lang/Object;
.source "FullScreenReadController.java"

# interfaces
.implements Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private mCurrentState:I

.field private mCursorController:Lcom/google/android/marvin/talkback/CursorController;

.field private final mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

.field private final mNodeSpokenRunnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 3
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCurrentState:I

    .line 279
    new-instance v0, Lcom/google/android/marvin/talkback/FullScreenReadController$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/FullScreenReadController$1;-><init>(Lcom/google/android/marvin/talkback/FullScreenReadController;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mNodeSpokenRunnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .line 92
    iput-object p1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 93
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    .line 94
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/CachedFeedbackController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    .line 95
    const-string v0, "power"

    invoke-virtual {p1, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const v1, 0x20000006

    const-string v2, "FullScreenReadController"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 97
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/FullScreenReadController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/FullScreenReadController;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->moveForward()V

    return-void
.end method

.method private currentNodeHasWebContent()Z
    .locals 3

    .prologue
    .line 254
    iget-object v2, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 255
    .local v0, "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-nez v0, :cond_0

    .line 256
    const/4 v1, 0x0

    .line 261
    :goto_0
    return v1

    .line 259
    :cond_0
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasLegacyWebContent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    .line 260
    .local v1, "isWebContent":Z
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0
.end method

.method private moveForward()V
    .locals 5

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/marvin/talkback/CursorController;->next(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    const v1, 0x7f050003

    const v2, 0x3fa66666    # 1.3f

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(IFFF)Z

    .line 195
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->interrupt()V

    .line 198
    :cond_0
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->currentNodeHasWebContent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 199
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->moveIntoWebContent()V

    .line 201
    :cond_1
    return-void
.end method

.method private moveIntoWebContent()V
    .locals 4

    .prologue
    const/16 v3, 0x10

    const/4 v2, 0x1

    .line 204
    iget-object v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 205
    .local v0, "webNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-nez v0, :cond_0

    .line 207
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->interrupt()V

    .line 225
    :goto_0
    return-void

    .line 211
    :cond_0
    iget v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCurrentState:I

    if-ne v1, v2, :cond_1

    .line 213
    const/4 v1, -0x1

    invoke-static {v0, v1, v3}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performNavigationAtGranularityAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;II)Z

    .line 218
    :cond_1
    invoke-static {v0, v2, v3}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performNavigationAtGranularityAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;II)Z

    .line 222
    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/FullScreenReadController;->setReadingState(I)V

    .line 224
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0
.end method

.method private setReadingState(I)V
    .locals 7
    .param p1, "newState"    # I

    .prologue
    .line 228
    const-string v1, "FullScreenReadController"

    const/4 v2, 0x2

    const-string v3, "Continuous reading switching to mode: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v1, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 230
    iput p1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCurrentState:I

    .line 232
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getInstance()Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    .line 233
    .local v0, "service":Lcom/google/android/marvin/talkback/TalkBackService;
    if-eqz v0, :cond_0

    .line 234
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->isActive()Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mNodeSpokenRunnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/marvin/talkback/SpeechController;->setShouldInjectAutoReadingCallbacks(ZLcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    .line 237
    :cond_0
    return-void
.end method


# virtual methods
.method public interrupt()V
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->setReadingState(I)V

    .line 187
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 190
    :cond_0
    return-void
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 250
    iget v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCurrentState:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isReadingLegacyWebContent()Z
    .locals 2

    .prologue
    .line 240
    iget v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCurrentState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 266
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->isActive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 272
    :cond_1
    const v0, 0x146297

    invoke-static {p1, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->eventMatchesAnyType(Landroid/view/accessibility/AccessibilityEvent;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->interrupt()V

    goto :goto_0
.end method

.method public shutdown()V
    .locals 0

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->interrupt()V

    .line 105
    return-void
.end method

.method public startReadingFromBeginning()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 143
    const/4 v1, 0x0

    .line 144
    .local v1, "rootNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    const/4 v0, 0x0

    .line 146
    .local v0, "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->isActive()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 179
    :goto_0
    return-void

    .line 151
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v2}, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->getRootInActiveWindow(Landroid/accessibilityservice/AccessibilityService;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 152
    if-nez v1, :cond_1

    .line 177
    new-array v2, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v2, v6

    aput-object v0, v2, v5

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 156
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    sget-object v3, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SHOULD_FOCUS:Lcom/googlecode/eyesfree/utils/NodeFilter;

    const/4 v4, 0x1

    invoke-static {v2, v1, v3, v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->searchFromInOrderTraversal(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 158
    if-nez v0, :cond_2

    .line 177
    new-array v2, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v2, v6

    aput-object v0, v2, v5

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 162
    :cond_2
    const/4 v2, 0x1

    :try_start_2
    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/FullScreenReadController;->setReadingState(I)V

    .line 164
    iget-object v2, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    sget-object v3, Lcom/google/android/marvin/talkback/CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularity;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/marvin/talkback/CursorController;->setGranularity(Lcom/google/android/marvin/talkback/CursorGranularity;Z)Z

    .line 166
    iget-object v2, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-nez v2, :cond_3

    .line 167
    iget-object v2, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 170
    :cond_3
    iget-object v2, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/CursorController;->clearCursor()V

    .line 171
    iget-object v2, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2, v0}, Lcom/google/android/marvin/talkback/CursorController;->setCursor(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    .line 173
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasLegacyWebContent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 174
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->moveIntoWebContent()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 177
    :cond_4
    new-array v2, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v2, v6

    aput-object v0, v2, v5

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    new-array v3, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v3, v6

    aput-object v0, v3, v5

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v2
.end method

.method public startReadingFromNextNode()V
    .locals 4

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->isActive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 116
    .local v0, "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v0, :cond_0

    .line 120
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/FullScreenReadController;->setReadingState(I)V

    .line 122
    iget-object v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    sget-object v2, Lcom/google/android/marvin/talkback/CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularity;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/marvin/talkback/CursorController;->setGranularity(Lcom/google/android/marvin/talkback/CursorGranularity;Z)Z

    .line 124
    iget-object v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_2

    .line 125
    iget-object v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 130
    :cond_2
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasLegacyWebContent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 131
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->moveIntoWebContent()V

    .line 136
    :goto_1
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    .line 133
    :cond_3
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->moveForward()V

    goto :goto_1
.end method
