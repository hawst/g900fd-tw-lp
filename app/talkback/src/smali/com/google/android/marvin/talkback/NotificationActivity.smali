.class public Lcom/google/android/marvin/talkback/NotificationActivity;
.super Landroid/app/Activity;
.source "NotificationActivity.java"


# instance fields
.field private mAlert:Landroid/app/AlertDialog;

.field private mNotificationId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 47
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/android/marvin/talkback/NotificationActivity;->mNotificationId:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/NotificationActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/NotificationActivity;

    .prologue
    .line 16
    iget v0, p0, Lcom/google/android/marvin/talkback/NotificationActivity;->mNotificationId:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/NotificationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/NotificationActivity;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/NotificationActivity;->dismissNotification()V

    return-void
.end method

.method private dismissNotification()V
    .locals 2

    .prologue
    .line 97
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/NotificationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 98
    .local v0, "nm":Landroid/app/NotificationManager;
    iget v1, p0, Lcom/google/android/marvin/talkback/NotificationActivity;->mNotificationId:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 99
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v12, 0x0

    const/4 v11, -0x1

    .line 51
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/NotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 54
    .local v5, "extras":Landroid/os/Bundle;
    if-nez v5, :cond_0

    .line 55
    const/4 v8, 0x5

    const-string v9, "NotificationActivity received an empty extras bundle."

    new-array v10, v12, [Ljava/lang/Object;

    invoke-static {p0, v8, v9, v10}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/NotificationActivity;->finish()V

    .line 59
    :cond_0
    const-string v8, "notificationId"

    const/high16 v9, -0x80000000

    invoke-virtual {v5, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, p0, Lcom/google/android/marvin/talkback/NotificationActivity;->mNotificationId:I

    .line 61
    const-string v8, "title"

    invoke-virtual {v5, v8, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 62
    .local v7, "titleRes":I
    const-string v8, "message"

    invoke-virtual {v5, v8, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 63
    .local v6, "messageRes":I
    const-string v8, "button"

    invoke-virtual {v5, v8, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 65
    .local v2, "buttonRes":I
    if-eq v7, v11, :cond_2

    invoke-virtual {p0, v7}, Lcom/google/android/marvin/talkback/NotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 67
    .local v4, "dialogTitle":Ljava/lang/CharSequence;
    :goto_0
    if-eq v6, v11, :cond_3

    invoke-virtual {p0, v6}, Lcom/google/android/marvin/talkback/NotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 68
    .local v3, "dialogMessage":Ljava/lang/CharSequence;
    :goto_1
    if-eq v2, v11, :cond_4

    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/NotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 71
    .local v1, "acceptButtonText":Ljava/lang/CharSequence;
    :goto_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 73
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/NotificationActivity;->finish()V

    .line 76
    :cond_1
    new-instance v0, Lcom/google/android/marvin/talkback/NotificationActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/NotificationActivity$1;-><init>(Lcom/google/android/marvin/talkback/NotificationActivity;)V

    .line 87
    .local v0, "acceptButtonListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v8, Landroid/app/AlertDialog$Builder;

    invoke-direct {v8, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8, v1, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/marvin/talkback/NotificationActivity;->mAlert:Landroid/app/AlertDialog;

    .line 93
    iget-object v8, p0, Lcom/google/android/marvin/talkback/NotificationActivity;->mAlert:Landroid/app/AlertDialog;

    invoke-virtual {v8}, Landroid/app/AlertDialog;->show()V

    .line 94
    return-void

    .line 65
    .end local v0    # "acceptButtonListener":Landroid/content/DialogInterface$OnClickListener;
    .end local v1    # "acceptButtonText":Ljava/lang/CharSequence;
    .end local v3    # "dialogMessage":Ljava/lang/CharSequence;
    .end local v4    # "dialogTitle":Ljava/lang/CharSequence;
    :cond_2
    const v8, 0x7f060083

    invoke-virtual {p0, v8}, Lcom/google/android/marvin/talkback/NotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 67
    .restart local v4    # "dialogTitle":Ljava/lang/CharSequence;
    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    .line 68
    .restart local v3    # "dialogMessage":Ljava/lang/CharSequence;
    :cond_4
    const v8, 0x104000a

    invoke-virtual {p0, v8}, Lcom/google/android/marvin/talkback/NotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method
