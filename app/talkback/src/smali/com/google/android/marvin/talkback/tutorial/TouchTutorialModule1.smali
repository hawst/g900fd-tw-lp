.class Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;
.super Lcom/google/android/marvin/talkback/tutorial/TutorialModule;
.source "TouchTutorialModule1.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final mAllApps:Landroid/widget/GridView;

.field private final mAppsAdapter:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

.field private final mFirstIconFocusDelegate:Landroid/view/View$AccessibilityDelegate;

.field private final mSwipeFocusChangeDelegate:Landroid/view/View$AccessibilityDelegate;

.field private final mTargetIconClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private final mTargetIconFocusDelegate:Landroid/view/View$AccessibilityDelegate;

.field private final mTargetIconLoseFocusDelegate:Landroid/view/View$AccessibilityDelegate;

.field private final mUntouchedIconFocusDelegate:Landroid/view/View$AccessibilityDelegate;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V
    .locals 6
    .param p1, "parentTutorial"    # Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 180
    const v0, 0x7f03000d

    const v1, 0x7f0601be

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;II)V

    .line 49
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mFirstIconFocusDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 72
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$2;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$2;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mUntouchedIconFocusDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 95
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$3;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$3;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mSwipeFocusChangeDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 119
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$4;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$4;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mTargetIconFocusDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 141
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$5;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$5;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mTargetIconLoseFocusDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 163
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$6;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$6;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mTargetIconClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 183
    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f03000a

    const v3, 0x7f0d0088

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAppsAdapter:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    .line 185
    const v0, 0x7f0d0093

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    .line 186
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAppsAdapter:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 188
    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->setSkipVisible(Z)V

    .line 189
    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->setBackVisible(Z)V

    .line 190
    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->setNextVisible(Z)V

    .line 191
    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->setFinishVisible(Z)V

    .line 192
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)Landroid/widget/GridView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->onTrigger1()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->onTrigger2()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->onTrigger3()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->onTrigger4()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->onTrigger4FocusLost()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->onTrigger5()V

    return-void
.end method

.method private onTrigger0()V
    .locals 3

    .prologue
    .line 227
    const v0, 0x7f0601bf

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 230
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mFirstIconFocusDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 231
    return-void
.end method

.method private onTrigger1()V
    .locals 3

    .prologue
    .line 237
    const v0, 0x7f0601c0

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 243
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mUntouchedIconFocusDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 244
    return-void
.end method

.method private onTrigger2()V
    .locals 3

    .prologue
    .line 250
    const v0, 0x7f0601c1

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 256
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mSwipeFocusChangeDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 257
    return-void
.end method

.method private onTrigger3()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 264
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAppsAdapter:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    invoke-virtual {v1, v3}, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;->getLabel(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 265
    .local v0, "targetName":Ljava/lang/CharSequence;
    const v1, 0x7f0601c2

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v4, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 268
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mTargetIconFocusDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 269
    return-void
.end method

.method private onTrigger4()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 275
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAppsAdapter:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    invoke-virtual {v1, v3}, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;->getLabel(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 276
    .local v0, "targetName":Ljava/lang/CharSequence;
    const v1, 0x7f0601c3

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v4, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 282
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mTargetIconLoseFocusDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 288
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mTargetIconClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 289
    return-void
.end method

.method private onTrigger4FocusLost()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 295
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAppsAdapter:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    invoke-virtual {v1, v3}, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;->getLabel(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 296
    .local v0, "targetName":Ljava/lang/CharSequence;
    const v1, 0x7f0601c4

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v4, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 299
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mTargetIconFocusDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 300
    return-void
.end method

.method private onTrigger5()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 307
    const v0, 0x7f0601c5

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0601b9

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v5, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->addInstruction(IZ[Ljava/lang/Object;)V

    .line 309
    return-void
.end method


# virtual methods
.method public onPause()V
    .locals 0

    .prologue
    .line 206
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 211
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 196
    invoke-super {p0}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->onStart()V

    .line 198
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->setAllowContextMenus(Z)V

    .line 200
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->onTrigger0()V

    .line 201
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 215
    invoke-super {p0}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->onStop()V

    .line 217
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 218
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 220
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->setAllowContextMenus(Z)V

    .line 221
    return-void
.end method
