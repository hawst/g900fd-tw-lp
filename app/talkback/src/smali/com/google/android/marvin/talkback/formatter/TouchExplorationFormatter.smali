.class public final Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;
.super Ljava/lang/Object;
.source "TouchExplorationFormatter.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$ContextBasedRule;
.implements Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;


# static fields
.field private static final DEFAULT_DESCRIPTION:Ljava/lang/CharSequence;

.field private static final SUPPORTS_A11Y_FOCUS:Z

.field private static final SUPPORTS_NODE_CACHE:Z


# instance fields
.field private mContext:Lcom/google/android/marvin/talkback/TalkBackService;

.field private mLastFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private mLastNodeWasScrollable:Z

.field private mNodeProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x10

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->SUPPORTS_A11Y_FOCUS:Z

    .line 52
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->SUPPORTS_NODE_CACHE:Z

    .line 59
    const-string v0, ""

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->DEFAULT_DESCRIPTION:Ljava/lang/CharSequence;

    return-void

    :cond_0
    move v0, v2

    .line 48
    goto :goto_0

    :cond_1
    move v1, v2

    .line 52
    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private addDescription(Lcom/google/android/marvin/talkback/Utterance;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 4
    .param p1, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;
    .param p2, "announcedNode"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p4, "source"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v2, 0x1

    .line 213
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, Lcom/google/android/marvin/talkback/Utterance;->addSpokenFlag(I)V

    .line 215
    iget-object v3, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mNodeProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    invoke-virtual {v3, p2, p3, p4}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->getDescriptionForTree(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 217
    .local v1, "treeDescription":Ljava/lang/CharSequence;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 218
    invoke-virtual {p1, v1}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 232
    :goto_0
    return v2

    .line 222
    :cond_0
    invoke-static {p3}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->getEventTextOrDescription(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 224
    .local v0, "eventDescription":Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 225
    invoke-virtual {p1, v0}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 231
    :cond_1
    sget-object v2, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->DEFAULT_DESCRIPTION:Ljava/lang/CharSequence;

    invoke-virtual {p1, v2}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 232
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private addFeedback(Lcom/google/android/marvin/talkback/Utterance;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 6
    .param p1, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;
    .param p2, "announcedNode"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 242
    if-nez p2, :cond_0

    .line 281
    :goto_0
    return-void

    .line 246
    :cond_0
    iget-object v4, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    sget-object v5, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SCROLLABLE:Lcom/googlecode/eyesfree/utils/NodeFilter;

    invoke-static {v4, p2, v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getSelfOrMatchingAncestor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 249
    .local v0, "scrollableNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v0, :cond_3

    move v1, v2

    .line 251
    .local v1, "userCanScroll":Z
    :goto_1
    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 255
    iget-boolean v2, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mLastNodeWasScrollable:Z

    if-eq v2, v1, :cond_1

    .line 256
    iput-boolean v1, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mLastNodeWasScrollable:Z

    .line 258
    if-eqz v1, :cond_4

    .line 259
    const v2, 0x7f050002

    invoke-virtual {p1, v2}, Lcom/google/android/marvin/talkback/Utterance;->addAuditory(I)V

    .line 268
    :cond_1
    :goto_2
    sget-boolean v2, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->SUPPORTS_NODE_CACHE:Z

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v2, p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isEdgeListItem(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 270
    const v2, 0x7f050015

    invoke-virtual {p1, v2}, Lcom/google/android/marvin/talkback/Utterance;->addAuditory(I)V

    .line 274
    :cond_2
    invoke-static {p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isActionableForAccessibility(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 275
    const v2, 0x7f050005

    invoke-virtual {p1, v2}, Lcom/google/android/marvin/talkback/Utterance;->addAuditory(I)V

    .line 276
    const v2, 0x7f0a0019

    invoke-virtual {p1, v2}, Lcom/google/android/marvin/talkback/Utterance;->addHaptic(I)V

    goto :goto_0

    .end local v1    # "userCanScroll":Z
    :cond_3
    move v1, v3

    .line 249
    goto :goto_1

    .line 261
    .restart local v1    # "userCanScroll":Z
    :cond_4
    const v2, 0x7f050001

    invoke-virtual {p1, v2}, Lcom/google/android/marvin/talkback/Utterance;->addAuditory(I)V

    goto :goto_2

    .line 278
    :cond_5
    const v2, 0x7f050004

    invoke-virtual {p1, v2}, Lcom/google/android/marvin/talkback/Utterance;->addAuditory(I)V

    .line 279
    const v2, 0x7f0a0018

    invoke-virtual {p1, v2}, Lcom/google/android/marvin/talkback/Utterance;->addHaptic(I)V

    goto :goto_0
.end method

.method private getFocusedNode(ILandroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 4
    .param p1, "eventType"    # I
    .param p2, "sourceNode"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v1, 0x0

    .line 149
    if-nez p2, :cond_0

    move-object v0, v1

    .line 196
    :goto_0
    return-object v0

    .line 155
    :cond_0
    sget-boolean v2, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->SUPPORTS_A11Y_FOCUS:Z

    if-eqz v2, :cond_2

    .line 156
    const v2, 0x8000

    if-eq p1, v2, :cond_1

    move-object v0, v1

    .line 157
    goto :goto_0

    .line 160
    :cond_1
    invoke-static {p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0

    .line 165
    :cond_2
    const/16 v2, 0x80

    if-eq p1, v2, :cond_3

    move-object v0, v1

    .line 166
    goto :goto_0

    .line 169
    :cond_3
    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v2, p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->findFocusFromHover(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 173
    .local v0, "focusedNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v0, :cond_4

    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mLastFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 174
    const/4 v2, 0x1

    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move-object v0, v1

    .line 175
    goto :goto_0

    .line 179
    :cond_4
    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mLastFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v2, :cond_5

    .line 180
    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mLastFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 183
    :cond_5
    if-eqz v0, :cond_6

    .line 184
    invoke-static {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mLastFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 194
    :goto_1
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;->updateRecentlyExplored(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    .line 188
    :cond_6
    iput-object v1, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mLastFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    goto :goto_1
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 9
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 109
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v1, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 110
    .local v1, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    .line 111
    .local v2, "sourceNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v5

    invoke-direct {p0, v5, v2}, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->getFocusedNode(ILandroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 116
    .local v0, "focusedNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v2, :cond_0

    if-nez v0, :cond_0

    .line 117
    new-array v4, v4, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v2, v4, v3

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 136
    :goto_0
    return v3

    .line 121
    :cond_0
    const-string v5, "Announcing node: %s"

    new-array v6, v4, [Ljava/lang/Object;

    aput-object v0, v6, v3

    invoke-static {p0, v8, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 124
    invoke-direct {p0, p3, v0, p1, v2}, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->addDescription(Lcom/google/android/marvin/talkback/Utterance;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    .line 125
    invoke-direct {p0, p3, v0}, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->addFeedback(Lcom/google/android/marvin/talkback/Utterance;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 128
    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getMetadata()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "queuing"

    const/4 v7, 0x3

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 132
    const/16 v5, 0x8

    invoke-virtual {p3, v5}, Lcom/google/android/marvin/talkback/Utterance;->addSpokenFlag(I)V

    .line 134
    new-array v5, v8, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v2, v5, v3

    aput-object v0, v5, v4

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v3, v4

    .line 136
    goto :goto_0
.end method

.method public initialize(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 1
    .param p1, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 85
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackService;->addEventListener(Lcom/googlecode/eyesfree/utils/AccessibilityEventListener;)V

    .line 87
    invoke-static {}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->getInstance()Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mNodeProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    .line 88
    return-void
.end method

.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 96
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 102
    :goto_0
    return-void

    .line 99
    :pswitch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mLastNodeWasScrollable:Z

    goto :goto_0

    .line 96
    :pswitch_data_0
    .packed-switch 0x20
        :pswitch_0
    .end packed-switch
.end method
