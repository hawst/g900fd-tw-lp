.class public Lcom/google/android/marvin/talkback/SpeechCleanupUtils;
.super Ljava/lang/Object;
.source "SpeechCleanupUtils.java"


# static fields
.field private static CONSECUTIVE_CHARACTER_PATTERN:Ljava/util/regex/Pattern;

.field private static final UNICODE_MAP:Landroid/util/SparseIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const-string v0, "([\\-\\\\/|!@#$%^&*\\(\\)=_+\\[\\]\\{\\}.?;\'\":<>])\\1{2,}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->CONSECUTIVE_CHARACTER_PATTERN:Ljava/util/regex/Pattern;

    .line 44
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    .line 47
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x26

    const v2, 0x7f060228

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 48
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x3c

    const v2, 0x7f060229

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 49
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x3e

    const v2, 0x7f06022a

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 50
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x27

    const v2, 0x7f060227

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 51
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2a

    const v2, 0x7f06022b

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 52
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x40

    const v2, 0x7f06022c

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 53
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x5c

    const v2, 0x7f06022d

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 54
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2022

    const v2, 0x7f06022e

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 55
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x5e

    const v2, 0x7f06022f

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 56
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xa2

    const v2, 0x7f060230

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 57
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x3a

    const v2, 0x7f060231

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 58
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2c

    const v2, 0x7f060232

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 59
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xa9

    const v2, 0x7f060233

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 60
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x7b

    const v2, 0x7f060234

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 61
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x7d

    const v2, 0x7f060235

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 62
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xb0

    const v2, 0x7f060236

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 63
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xf7

    const v2, 0x7f060237

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 64
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x24

    const v2, 0x7f060238

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 65
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2026

    const v2, 0x7f060239

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 66
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2014

    const v2, 0x7f06023a

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 67
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2013

    const v2, 0x7f06023b

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 68
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x20ac

    const v2, 0x7f06023c

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 69
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x21

    const v2, 0x7f06023d

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 70
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x60

    const v2, 0x7f06023e

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 71
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2d

    const v2, 0x7f06023f

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 72
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x201e

    const v2, 0x7f060240

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 73
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xd7

    const v2, 0x7f060241

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 74
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xa

    const v2, 0x7f060242

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 75
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xb6

    const v2, 0x7f060243

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 76
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x28

    const v2, 0x7f060244

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 77
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x29

    const v2, 0x7f060245

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 78
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x25

    const v2, 0x7f060246

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 79
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2e

    const v2, 0x7f060247

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 80
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x3c0

    const v2, 0x7f060248

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 81
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x23

    const v2, 0x7f060249

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 82
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xa3

    const v2, 0x7f06024a

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 83
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x3f

    const v2, 0x7f06024b

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 84
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x22

    const v2, 0x7f06024c

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 85
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xae

    const v2, 0x7f06024d

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 86
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x3b

    const v2, 0x7f06024e

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 87
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2f

    const v2, 0x7f06024f

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 88
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x20

    const v2, 0x7f060251

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 89
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x5b

    const v2, 0x7f060252

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 90
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x5d

    const v2, 0x7f060253

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 91
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x221a

    const v2, 0x7f060254

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 92
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2122

    const v2, 0x7f060255

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 93
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x5f

    const v2, 0x7f060256

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 94
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x7c

    const v2, 0x7f060257

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 95
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xa5

    const v2, 0x7f060258

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 96
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xac

    const v2, 0x7f060259

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 97
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xa6

    const v2, 0x7f06025a

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 98
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xb5

    const v2, 0x7f06025b

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 99
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2248

    const v2, 0x7f06025c

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 100
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2260

    const v2, 0x7f06025d

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 101
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xa4

    const v2, 0x7f06025e

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 102
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xa7

    const v2, 0x7f06025f

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 103
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2191

    const v2, 0x7f060260

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 104
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2190

    const v2, 0x7f060261

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 105
    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x20b9

    const v2, 0x7f060262

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 106
    return-void
.end method

.method public static cleanUp(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 116
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 120
    .end local p1    # "text":Ljava/lang/CharSequence;
    :cond_0
    :goto_0
    return-object p1

    .restart local p1    # "text":Ljava/lang/CharSequence;
    :cond_1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {p0, v0}, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->getCleanValueFor(Landroid/content/Context;C)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static collapseRepeatedCharacters(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v7, 0x0

    .line 132
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 133
    const/4 v3, 0x0

    .line 148
    :goto_0
    return-object v3

    .line 137
    :cond_0
    sget-object v3, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->CONSECUTIVE_CHARACTER_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 138
    .local v1, "matcher":Ljava/util/regex/Matcher;
    :goto_1
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 139
    const v3, 0x7f06018f

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {p0, v6}, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->getCleanValueFor(Landroid/content/Context;C)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 141
    .local v2, "replacement":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v3

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int v0, v3, v4

    .line 143
    .local v0, "matchFromIndex":I
    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 144
    sget-object v3, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->CONSECUTIVE_CHARACTER_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 145
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-virtual {v1, v0, v3}, Ljava/util/regex/Matcher;->region(II)Ljava/util/regex/Matcher;

    goto :goto_1

    .end local v0    # "matchFromIndex":I
    .end local v2    # "replacement":Ljava/lang/String;
    :cond_1
    move-object v3, p1

    .line 148
    goto :goto_0
.end method

.method public static getCleanValueFor(Landroid/content/Context;C)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # C

    .prologue
    .line 155
    sget-object v1, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 157
    .local v0, "resId":I
    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 165
    :goto_0
    return-object v1

    .line 161
    :cond_0
    invoke-static {p1}, Ljava/lang/Character;->isUpperCase(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 162
    const v1, 0x7f0600bb

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 165
    :cond_1
    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
