.class Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$9;
.super Ljava/lang/Object;
.source "TalkBackPreferencesActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V
    .locals 0

    .prologue
    .line 730
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$9;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 733
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 737
    .local v0, "requestedState":Z
    if-nez v0, :cond_0

    .line 738
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$9;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$9;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    # invokes: Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->createDisableExploreByTouchDialog()Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->access$500(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    # setter for: Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mExploreByTouchDialog:Landroid/app/AlertDialog;
    invoke-static {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->access$002(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 739
    const/4 v1, 0x0

    .line 742
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$9;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    # invokes: Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->setTouchExplorationRequested(Z)Z
    invoke-static {v1, v0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->access$100(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;Z)Z

    move-result v1

    goto :goto_0
.end method
