.class Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$7;
.super Ljava/lang/Object;
.source "LabelDialogActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)V
    .locals 0

    .prologue
    .line 311
    iput-object p1, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$7;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "text"    # Landroid/text/Editable;

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$7;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mPositiveButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->access$400(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$7;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mPositiveButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->access$400(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)Landroid/widget/Button;

    move-result-object v1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 317
    :cond_0
    return-void

    .line 315
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 320
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 323
    return-void
.end method
