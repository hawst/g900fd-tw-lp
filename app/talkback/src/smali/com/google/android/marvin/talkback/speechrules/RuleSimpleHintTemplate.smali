.class public Lcom/google/android/marvin/talkback/speechrules/RuleSimpleHintTemplate;
.super Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;
.source "RuleSimpleHintTemplate.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/speechrules/NodeHintRule;


# instance fields
.field private final mHintResId:I


# direct methods
.method public constructor <init>(Ljava/lang/Class;II)V
    .locals 0
    .param p2, "resId"    # I
    .param p3, "hintResId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;II)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p1, "targetClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;-><init>(Ljava/lang/Class;I)V

    .line 28
    iput p3, p0, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleHintTemplate;->mHintResId:I

    .line 29
    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3, "x2"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 22
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;->accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic format(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3, "x2"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 22
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;->format(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getHintText(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleHintTemplate;->mHintResId:I

    invoke-static {p1, v0}, Lcom/google/android/marvin/talkback/speechrules/NodeHintRule$NodeHintHelper;->getHintString(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
