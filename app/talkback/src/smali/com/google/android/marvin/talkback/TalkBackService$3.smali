.class Lcom/google/android/marvin/talkback/TalkBackService$3;
.super Ljava/lang/Object;
.source "TalkBackService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/marvin/talkback/TalkBackService;->resetFocusedNode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackService;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0

    .prologue
    .line 735
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackService$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 738
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mSavedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->access$200(Lcom/google/android/marvin/talkback/TalkBackService;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    if-nez v1, :cond_1

    .line 758
    :cond_0
    :goto_0
    return-void

    .line 742
    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mSavedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->access$200(Lcom/google/android/marvin/talkback/TalkBackService;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->refreshNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 744
    .local v0, "refreshed":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mSavedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->access$200(Lcom/google/android/marvin/talkback/TalkBackService;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 745
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mSavedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->access$200(Lcom/google/android/marvin/talkback/TalkBackService;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 748
    :cond_2
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/marvin/talkback/TalkBackService;->mSavedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-static {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->access$202(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 750
    if-eqz v0, :cond_0

    .line 751
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isAccessibilityFocused()Z

    move-result v1

    if-nez v1, :cond_3

    .line 752
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    .line 756
    :cond_3
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0
.end method
