.class public Lcom/google/android/marvin/talkback/speechrules/RuleContainer;
.super Ljava/lang/Object;
.source "RuleContainer.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private formatWithChildren(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Ljava/lang/CharSequence;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3, "childCount"    # I

    .prologue
    .line 51
    const-class v1, Landroid/widget/GridView;

    invoke-static {p1, p2, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53
    const v1, 0x7f0600b4

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, "type":Ljava/lang/CharSequence;
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0e0000

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, p3, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 54
    .end local v0    # "type":Ljava/lang/CharSequence;
    :cond_0
    const-class v1, Landroid/widget/TabWidget;

    invoke-static {p1, p2, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 56
    const v1, 0x7f0600b6

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "type":Ljava/lang/CharSequence;
    goto :goto_0

    .line 58
    .end local v0    # "type":Ljava/lang/CharSequence;
    :cond_1
    const v1, 0x7f0600b5

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "type":Ljava/lang/CharSequence;
    goto :goto_0
.end method


# virtual methods
.method public accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3, "ev"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 35
    const-class v0, Landroid/widget/AbsListView;

    invoke-static {p1, p2, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Landroid/widget/TabWidget;

    invoke-static {p1, p2, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public format(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 43
    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v0

    .line 44
    .local v0, "childCount":I
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/marvin/talkback/speechrules/RuleContainer;->formatWithChildren(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Ljava/lang/CharSequence;

    move-result-object v1

    return-object v1
.end method
