.class public Lcom/google/android/marvin/utils/HighlightBoundsView;
.super Landroid/view/View;
.source "HighlightBoundsView.java"


# instance fields
.field private final SCREEN_LOCATION:[I

.field private mHighlightColor:I

.field private final mMatrix:Landroid/graphics/Matrix;

.field private final mNodes:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;"
        }
    .end annotation
.end field

.field private final mPaint:Landroid/graphics/Paint;

.field private final mTemp:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->SCREEN_LOCATION:[I

    .line 40
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mTemp:Landroid/graphics/Rect;

    .line 41
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mPaint:Landroid/graphics/Paint;

    .line 42
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mNodes:Ljava/util/HashSet;

    .line 43
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mMatrix:Landroid/graphics/Matrix;

    .line 56
    iget-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 60
    const/high16 v0, -0x10000

    iput v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mHighlightColor:I

    .line 61
    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    .line 72
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v2

    .line 73
    .local v2, "saveCount":I
    iget-object v3, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->SCREEN_LOCATION:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    neg-int v3, v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->SCREEN_LOCATION:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 74
    iget-object v3, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 76
    iget-object v3, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mHighlightColor:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    iget-object v3, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mNodes:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 79
    .local v1, "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    iget-object v3, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mTemp:Landroid/graphics/Rect;

    invoke-virtual {v1, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getBoundsInScreen(Landroid/graphics/Rect;)V

    .line 80
    iget-object v3, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mTemp:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 83
    .end local v1    # "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_0
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 84
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 65
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 67
    iget-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->SCREEN_LOCATION:[I

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/utils/HighlightBoundsView;->getLocationOnScreen([I)V

    .line 68
    return-void
.end method
