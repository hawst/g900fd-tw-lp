.class abstract Lcom/google/android/marvin/talkback/tutorial/TutorialModule;
.super Landroid/widget/FrameLayout;
.source "TutorialModule.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final mBack:Landroid/widget/Button;

.field private final mDropEventsDelegate:Landroid/view/View$AccessibilityDelegate;

.field private final mFinish:Landroid/widget/Button;

.field private final mHandler:Landroid/os/Handler;

.field private final mInstructions:Landroid/widget/TextView;

.field private final mNext:Landroid/widget/Button;

.field private final mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

.field private final mSkip:Landroid/widget/Button;

.field private final mTitleResId:I


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;I)V
    .locals 1
    .param p1, "parentTutorial"    # Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;
    .param p2, "titleResId"    # I

    .prologue
    .line 123
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;II)V

    .line 124
    return-void
.end method

.method public constructor <init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;II)V
    .locals 6
    .param p1, "parentTutorial"    # Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;
    .param p2, "layoutResId"    # I
    .param p3, "titleResId"    # I

    .prologue
    const/4 v5, 0x1

    .line 80
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 50
    new-instance v4, Lcom/google/android/marvin/talkback/tutorial/TutorialModule$1;

    invoke-direct {v4, p0}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TutorialModule;)V

    iput-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mDropEventsDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 317
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    iput-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mHandler:Landroid/os/Handler;

    .line 82
    iput-object p1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    .line 83
    iput p3, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mTitleResId:I

    .line 85
    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 86
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f03000c

    invoke-virtual {v2, v4, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 89
    .local v0, "container":Landroid/view/View;
    const v4, 0x7f0d0091

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mInstructions:Landroid/widget/TextView;

    .line 90
    const v4, 0x7f0d008b

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mSkip:Landroid/widget/Button;

    .line 91
    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mSkip:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    const v4, 0x7f0d008a

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mBack:Landroid/widget/Button;

    .line 93
    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mBack:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    const v4, 0x7f0d008c

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mNext:Landroid/widget/Button;

    .line 95
    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mNext:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    const v4, 0x7f0d008d

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mFinish:Landroid/widget/Button;

    .line 97
    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mFinish:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    const v4, 0x7f0d008e

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 101
    .local v3, "title":Landroid/widget/TextView;
    if-eqz v3, :cond_0

    .line 102
    invoke-virtual {v3, p3}, Landroid/widget/TextView;->setText(I)V

    .line 105
    :cond_0
    const/4 v4, -0x1

    if-eq p2, v4, :cond_1

    .line 106
    const v4, 0x7f0d0090

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 109
    .local v1, "contentHolder":Landroid/view/ViewGroup;
    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mDropEventsDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 110
    invoke-virtual {v2, p2, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 111
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 113
    .end local v1    # "contentHolder":Landroid/view/ViewGroup;
    :cond_1
    return-void
.end method


# virtual methods
.method protected addGranularityListener(Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;

    .prologue
    .line 298
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getInstance()Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    .line 299
    .local v0, "service":Lcom/google/android/marvin/talkback/TalkBackService;
    if-nez v0, :cond_0

    .line 300
    const/4 v1, 0x0

    .line 304
    :goto_0
    return v1

    .line 303
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/marvin/talkback/CursorController;->addGranularityListener(Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;)V

    .line 304
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected varargs addInstruction(IZ[Ljava/lang/Object;)V
    .locals 3
    .param p1, "resId"    # I
    .param p2, "repeat"    # Z
    .param p3, "formatArgs"    # [Ljava/lang/Object;

    .prologue
    .line 140
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-virtual {v1, p1, p3}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "text":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mInstructions:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 143
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mInstructions:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->speakInstruction(IZ[Ljava/lang/Object;)V

    .line 146
    return-void
.end method

.method protected getGestureDirectionForRequiredAction(Lcom/google/android/marvin/talkback/ShortcutGestureAction;)I
    .locals 8
    .param p1, "action"    # Lcom/google/android/marvin/talkback/ShortcutGestureAction;

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->getParentTutorial()Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    move-result-object v3

    .line 191
    .local v3, "parentActivity":Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;
    invoke-static {v3, p1}, Lcom/google/android/marvin/utils/GesturePreferenceUtils;->getDirectionForAction(Landroid/content/Context;Lcom/google/android/marvin/talkback/ShortcutGestureAction;)I

    move-result v1

    .line 193
    .local v1, "direction":I
    if-gez v1, :cond_0

    .line 194
    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->stopRepeating()V

    .line 196
    const v5, 0x7f0601ef

    invoke-virtual {v3, v5}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 198
    .local v4, "title":Ljava/lang/String;
    invoke-virtual {p1, v3}, Lcom/google/android/marvin/talkback/ShortcutGestureAction;->getLabel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 199
    .local v0, "actionLabel":Ljava/lang/String;
    const v5, 0x7f0601f0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-virtual {v3, v5, v6}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 202
    .local v2, "message":Ljava/lang/String;
    invoke-virtual {v3, v4, v2}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->showAlertDialogAndFinish(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    .end local v0    # "actionLabel":Ljava/lang/String;
    .end local v2    # "message":Ljava/lang/String;
    .end local v4    # "title":Ljava/lang/String;
    :cond_0
    return v1
.end method

.method protected getParentTutorial()Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    return-object v0
.end method

.method protected installTriggerDelayed(Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "trigger"    # Ljava/lang/Runnable;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->stopRepeating()V

    .line 161
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->setTouchGuardActive(Z)V

    .line 162
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->lockOrientation()V

    .line 163
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, p1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 164
    return-void
.end method

.method protected installTriggerDelayedWithFeedback(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "trigger"    # Ljava/lang/Runnable;

    .prologue
    .line 173
    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->installTriggerDelayed(Ljava/lang/Runnable;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->playTriggerSound()V

    .line 175
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 241
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0d008b

    if-ne v0, v1, :cond_1

    .line 242
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->finish()V

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 243
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0d008a

    if-ne v0, v1, :cond_2

    .line 244
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->previous()V

    goto :goto_0

    .line 245
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0d008c

    if-ne v0, v1, :cond_3

    .line 246
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->next()V

    goto :goto_0

    .line 247
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0d008d

    if-ne v0, v1, :cond_0

    .line 248
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->finish()V

    goto :goto_0
.end method

.method public abstract onPause()V
.end method

.method public abstract onResume()V
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mInstructions:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 259
    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    iget v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mTitleResId:I

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->setTitle(I)V

    .line 260
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 279
    return-void
.end method

.method protected registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V
    .locals 2
    .param p1, "receiver"    # Landroid/content/BroadcastReceiver;
    .param p2, "filter"    # Landroid/content/IntentFilter;

    .prologue
    .line 216
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 217
    .local v0, "manager":Landroid/support/v4/content/LocalBroadcastManager;
    if-nez v0, :cond_0

    .line 222
    :goto_0
    return-void

    .line 221
    :cond_0
    invoke-virtual {v0, p1, p2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    goto :goto_0
.end method

.method protected removeGranularityListener(Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;

    .prologue
    .line 308
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getInstance()Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    .line 309
    .local v0, "service":Lcom/google/android/marvin/talkback/TalkBackService;
    if-nez v0, :cond_0

    .line 310
    const/4 v1, 0x0

    .line 314
    :goto_0
    return v1

    .line 313
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/marvin/talkback/CursorController;->removeGranularityListener(Lcom/google/android/marvin/talkback/CursorController$GranularityChangeListener;)V

    .line 314
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected setBackVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 286
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mBack:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 287
    return-void

    .line 286
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected setFinishVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 294
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mFinish:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 295
    return-void

    .line 294
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected setNextVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 290
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mNext:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 291
    return-void

    .line 290
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected setSkipVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 282
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mSkip:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 283
    return-void

    .line 282
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    .locals 2
    .param p1, "receiver"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 231
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 232
    .local v0, "manager":Landroid/support/v4/content/LocalBroadcastManager;
    if-nez v0, :cond_0

    .line 237
    :goto_0
    return-void

    .line 236
    :cond_0
    invoke-virtual {v0, p1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0
.end method
