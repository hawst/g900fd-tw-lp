.class public final Lcom/google/android/marvin/talkback/formatter/LiveViewFormatter;
.super Ljava/lang/Object;
.source "LiveViewFormatter.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    const/4 v2, 0x0

    .line 40
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getSource()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;-><init>(Ljava/lang/Object;)V

    .line 41
    .local v1, "node":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getInfo()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_1

    .line 61
    :cond_0
    :goto_0
    :pswitch_0
    return v2

    .line 45
    :cond_1
    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getLiveRegion()I

    move-result v0

    .line 46
    .local v0, "liveRegion":I
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 59
    :goto_1
    :pswitch_1
    invoke-static {}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->getInstance()Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    move-result-object v2

    invoke-virtual {v2, v1, p1}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->getDescriptionForNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 61
    const/4 v2, 0x1

    goto :goto_0

    .line 50
    :pswitch_2
    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getMetadata()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "queuing"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 46
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
