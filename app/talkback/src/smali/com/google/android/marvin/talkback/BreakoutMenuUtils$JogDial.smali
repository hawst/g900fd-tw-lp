.class public abstract Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;
.super Ljava/lang/Object;
.source "BreakoutMenuUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/BreakoutMenuUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "JogDial"
.end annotation


# instance fields
.field private final mJogListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

.field private final mSegmentCount:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "segmentCount"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial$1;-><init>(Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;->mJogListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    .line 35
    iput p1, p0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;->mSegmentCount:I

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;

    .prologue
    .line 30
    iget v0, p0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;->mSegmentCount:I

    return v0
.end method


# virtual methods
.method protected abstract onFirstTouch()V
.end method

.method protected abstract onNext()V
.end method

.method protected abstract onPrevious()V
.end method

.method public populateMenu(Lcom/googlecode/eyesfree/widget/RadialMenu;)V
    .locals 4
    .param p1, "radialMenu"    # Lcom/googlecode/eyesfree/widget/RadialMenu;

    .prologue
    .line 48
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->clear()V

    .line 50
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;->mSegmentCount:I

    if-ge v0, v2, :cond_0

    .line 51
    const/4 v2, 0x0

    const-string v3, ""

    invoke-virtual {p1, v2, v0, v0, v3}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v1

    .line 52
    .local v1, "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    iget-object v2, p0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;->mJogListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setOnMenuItemSelectionListener(Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;)Landroid/view/MenuItem;

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    .end local v1    # "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    :cond_0
    return-void
.end method
