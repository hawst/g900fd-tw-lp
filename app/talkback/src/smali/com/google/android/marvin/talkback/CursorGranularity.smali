.class public final enum Lcom/google/android/marvin/talkback/CursorGranularity;
.super Ljava/lang/Enum;
.source "CursorGranularity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/marvin/talkback/CursorGranularity;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/marvin/talkback/CursorGranularity;

.field public static final enum CHARACTER:Lcom/google/android/marvin/talkback/CursorGranularity;

.field public static final enum DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularity;

.field public static final enum LINE:Lcom/google/android/marvin/talkback/CursorGranularity;

.field public static final enum PAGE:Lcom/google/android/marvin/talkback/CursorGranularity;

.field public static final enum PARAGRAPH:Lcom/google/android/marvin/talkback/CursorGranularity;

.field public static final enum WEB_CONTROL:Lcom/google/android/marvin/talkback/CursorGranularity;

.field public static final enum WEB_LIST:Lcom/google/android/marvin/talkback/CursorGranularity;

.field public static final enum WEB_SECTION:Lcom/google/android/marvin/talkback/CursorGranularity;

.field public static final enum WORD:Lcom/google/android/marvin/talkback/CursorGranularity;


# instance fields
.field public final resourceId:I

.field public final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 29
    new-instance v0, Lcom/google/android/marvin/talkback/CursorGranularity;

    const-string v1, "DEFAULT"

    const v2, 0x7f060110

    invoke-direct {v0, v1, v5, v2, v5}, Lcom/google/android/marvin/talkback/CursorGranularity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularity;

    .line 30
    new-instance v0, Lcom/google/android/marvin/talkback/CursorGranularity;

    const-string v1, "CHARACTER"

    const v2, 0x7f060111

    invoke-direct {v0, v1, v6, v2, v6}, Lcom/google/android/marvin/talkback/CursorGranularity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/CursorGranularity;->CHARACTER:Lcom/google/android/marvin/talkback/CursorGranularity;

    .line 32
    new-instance v0, Lcom/google/android/marvin/talkback/CursorGranularity;

    const-string v1, "WORD"

    const v2, 0x7f060112

    invoke-direct {v0, v1, v7, v2, v7}, Lcom/google/android/marvin/talkback/CursorGranularity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/CursorGranularity;->WORD:Lcom/google/android/marvin/talkback/CursorGranularity;

    .line 33
    new-instance v0, Lcom/google/android/marvin/talkback/CursorGranularity;

    const-string v1, "LINE"

    const/4 v2, 0x3

    const v3, 0x7f060113

    invoke-direct {v0, v1, v2, v3, v8}, Lcom/google/android/marvin/talkback/CursorGranularity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/CursorGranularity;->LINE:Lcom/google/android/marvin/talkback/CursorGranularity;

    .line 34
    new-instance v0, Lcom/google/android/marvin/talkback/CursorGranularity;

    const-string v1, "PARAGRAPH"

    const v2, 0x7f060114

    invoke-direct {v0, v1, v8, v2, v9}, Lcom/google/android/marvin/talkback/CursorGranularity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/CursorGranularity;->PARAGRAPH:Lcom/google/android/marvin/talkback/CursorGranularity;

    .line 36
    new-instance v0, Lcom/google/android/marvin/talkback/CursorGranularity;

    const-string v1, "PAGE"

    const/4 v2, 0x5

    const v3, 0x7f060115

    const/16 v4, 0x10

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/marvin/talkback/CursorGranularity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/CursorGranularity;->PAGE:Lcom/google/android/marvin/talkback/CursorGranularity;

    .line 37
    new-instance v0, Lcom/google/android/marvin/talkback/CursorGranularity;

    const-string v1, "WEB_SECTION"

    const/4 v2, 0x6

    const v3, 0x7f060116

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/android/marvin/talkback/CursorGranularity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/CursorGranularity;->WEB_SECTION:Lcom/google/android/marvin/talkback/CursorGranularity;

    .line 38
    new-instance v0, Lcom/google/android/marvin/talkback/CursorGranularity;

    const-string v1, "WEB_LIST"

    const/4 v2, 0x7

    const v3, 0x7f060117

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/android/marvin/talkback/CursorGranularity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/CursorGranularity;->WEB_LIST:Lcom/google/android/marvin/talkback/CursorGranularity;

    .line 39
    new-instance v0, Lcom/google/android/marvin/talkback/CursorGranularity;

    const-string v1, "WEB_CONTROL"

    const v2, 0x7f060118

    invoke-direct {v0, v1, v9, v2, v5}, Lcom/google/android/marvin/talkback/CursorGranularity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/CursorGranularity;->WEB_CONTROL:Lcom/google/android/marvin/talkback/CursorGranularity;

    .line 26
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/marvin/talkback/CursorGranularity;

    sget-object v1, Lcom/google/android/marvin/talkback/CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularity;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/marvin/talkback/CursorGranularity;->CHARACTER:Lcom/google/android/marvin/talkback/CursorGranularity;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/marvin/talkback/CursorGranularity;->WORD:Lcom/google/android/marvin/talkback/CursorGranularity;

    aput-object v1, v0, v7

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/marvin/talkback/CursorGranularity;->LINE:Lcom/google/android/marvin/talkback/CursorGranularity;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/marvin/talkback/CursorGranularity;->PARAGRAPH:Lcom/google/android/marvin/talkback/CursorGranularity;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/marvin/talkback/CursorGranularity;->PAGE:Lcom/google/android/marvin/talkback/CursorGranularity;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/marvin/talkback/CursorGranularity;->WEB_SECTION:Lcom/google/android/marvin/talkback/CursorGranularity;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/marvin/talkback/CursorGranularity;->WEB_LIST:Lcom/google/android/marvin/talkback/CursorGranularity;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/marvin/talkback/CursorGranularity;->WEB_CONTROL:Lcom/google/android/marvin/talkback/CursorGranularity;

    aput-object v1, v0, v9

    sput-object v0, Lcom/google/android/marvin/talkback/CursorGranularity;->$VALUES:[Lcom/google/android/marvin/talkback/CursorGranularity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "resourceId"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 59
    iput p3, p0, Lcom/google/android/marvin/talkback/CursorGranularity;->resourceId:I

    .line 60
    iput p4, p0, Lcom/google/android/marvin/talkback/CursorGranularity;->value:I

    .line 61
    return-void
.end method

.method public static extractFromMask(IZLjava/util/List;)V
    .locals 6
    .param p0, "bitmask"    # I
    .param p1, "hasWebContent"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/marvin/talkback/CursorGranularity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p2, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/marvin/talkback/CursorGranularity;>;"
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 92
    sget-object v4, Lcom/google/android/marvin/talkback/CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularity;

    invoke-interface {p2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    invoke-static {}, Lcom/google/android/marvin/talkback/CursorGranularity;->values()[Lcom/google/android/marvin/talkback/CursorGranularity;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/marvin/talkback/CursorGranularity;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 95
    .local v3, "value":Lcom/google/android/marvin/talkback/CursorGranularity;
    iget v4, v3, Lcom/google/android/marvin/talkback/CursorGranularity;->value:I

    if-nez v4, :cond_1

    .line 94
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 99
    :cond_1
    iget v4, v3, Lcom/google/android/marvin/talkback/CursorGranularity;->value:I

    and-int/2addr v4, p0

    iget v5, v3, Lcom/google/android/marvin/talkback/CursorGranularity;->value:I

    if-ne v4, v5, :cond_0

    .line 100
    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 104
    .end local v3    # "value":Lcom/google/android/marvin/talkback/CursorGranularity;
    :cond_2
    if-eqz p1, :cond_3

    .line 105
    sget-object v4, Lcom/google/android/marvin/talkback/CursorGranularity;->WEB_SECTION:Lcom/google/android/marvin/talkback/CursorGranularity;

    invoke-interface {p2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    sget-object v4, Lcom/google/android/marvin/talkback/CursorGranularity;->WEB_LIST:Lcom/google/android/marvin/talkback/CursorGranularity;

    invoke-interface {p2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    sget-object v4, Lcom/google/android/marvin/talkback/CursorGranularity;->WEB_CONTROL:Lcom/google/android/marvin/talkback/CursorGranularity;

    invoke-interface {p2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    :cond_3
    return-void
.end method

.method public static fromResourceId(I)Lcom/google/android/marvin/talkback/CursorGranularity;
    .locals 5
    .param p0, "resourceId"    # I

    .prologue
    .line 70
    invoke-static {}, Lcom/google/android/marvin/talkback/CursorGranularity;->values()[Lcom/google/android/marvin/talkback/CursorGranularity;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/marvin/talkback/CursorGranularity;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 71
    .local v3, "value":Lcom/google/android/marvin/talkback/CursorGranularity;
    iget v4, v3, Lcom/google/android/marvin/talkback/CursorGranularity;->resourceId:I

    if-ne v4, p0, :cond_0

    .line 76
    .end local v3    # "value":Lcom/google/android/marvin/talkback/CursorGranularity;
    :goto_1
    return-object v3

    .line 70
    .restart local v3    # "value":Lcom/google/android/marvin/talkback/CursorGranularity;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 76
    .end local v3    # "value":Lcom/google/android/marvin/talkback/CursorGranularity;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/marvin/talkback/CursorGranularity;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/google/android/marvin/talkback/CursorGranularity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/marvin/talkback/CursorGranularity;

    return-object v0
.end method

.method public static values()[Lcom/google/android/marvin/talkback/CursorGranularity;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/marvin/talkback/CursorGranularity;->$VALUES:[Lcom/google/android/marvin/talkback/CursorGranularity;

    invoke-virtual {v0}, [Lcom/google/android/marvin/talkback/CursorGranularity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/marvin/talkback/CursorGranularity;

    return-object v0
.end method


# virtual methods
.method public isWebGranularity()Z
    .locals 2

    .prologue
    .line 116
    iget v0, p0, Lcom/google/android/marvin/talkback/CursorGranularity;->resourceId:I

    const v1, 0x7f060116

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/marvin/talkback/CursorGranularity;->resourceId:I

    const v1, 0x7f060117

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/marvin/talkback/CursorGranularity;->resourceId:I

    const v1, 0x7f060118

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
