.class Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$1;
.super Ljava/lang/Object;
.source "LabelDialogActivity.java"

# interfaces
.implements Lcom/googlecode/eyesfree/labeling/DirectLabelFetchRequest$OnLabelFetchedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$1;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLabelFetched(Lcom/googlecode/eyesfree/labeling/Label;)V
    .locals 2
    .param p1, "result"    # Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    .line 112
    if-nez p1, :cond_1

    .line 114
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$1;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->finish()V

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    const-string v0, "com.google.android.marvin.talkback.labeling.EDIT_LABEL"

    iget-object v1, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$1;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mStartIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->access$000(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 119
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$1;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    # invokes: Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->showEditLabelDialog(Lcom/googlecode/eyesfree/labeling/Label;)V
    invoke-static {v0, p1}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->access$100(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;Lcom/googlecode/eyesfree/labeling/Label;)V

    goto :goto_0

    .line 120
    :cond_2
    const-string v0, "com.google.android.marvin.talkback.labeling.REMOVE_LABEL"

    iget-object v1, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$1;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    # getter for: Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->mStartIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->access$000(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity$1;->this$0:Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;

    # invokes: Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->showRemoveLabelDialog(Lcom/googlecode/eyesfree/labeling/Label;)V
    invoke-static {v0, p1}, Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;->access$200(Lcom/google/android/marvin/talkback/labeling/LabelDialogActivity;Lcom/googlecode/eyesfree/labeling/Label;)V

    goto :goto_0
.end method
