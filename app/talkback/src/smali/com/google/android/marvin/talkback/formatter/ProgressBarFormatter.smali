.class public Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;
.super Ljava/lang/Object;
.source "ProgressBarFormatter.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# static fields
.field private static sRecentlyExplored:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getProgressPercent(Landroid/view/accessibility/AccessibilityEvent;)F
    .locals 6
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 99
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getItemCount()I

    move-result v0

    .line 100
    .local v0, "maxProgress":I
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getCurrentItemIndex()I

    move-result v2

    .line 101
    .local v2, "progress":I
    int-to-float v3, v2

    int-to-float v4, v0

    div-float v1, v3, v4

    .line 103
    .local v1, "percent":F
    const/high16 v3, 0x42c80000    # 100.0f

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v5, v1}, Ljava/lang/Math;->min(FF)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    mul-float/2addr v3, v4

    return v3
.end method

.method private shouldDropEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v2, 0x0

    .line 76
    new-instance v0, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v0, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    .line 77
    .local v0, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    .line 81
    .local v1, "source":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-nez v1, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v2

    .line 86
    :cond_1
    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isFocused()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isAccessibilityFocused()Z

    move-result v3

    if-nez v3, :cond_0

    .line 91
    sget-object v3, Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;->sRecentlyExplored:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v1, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 95
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static updateRecentlyExplored(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 1
    .param p0, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;->sRecentlyExplored:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v0, :cond_0

    .line 42
    sget-object v0, Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;->sRecentlyExplored:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 45
    :cond_0
    if-eqz p0, :cond_1

    .line 46
    invoke-static {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;->sRecentlyExplored:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 50
    :goto_0
    return-void

    .line 48
    :cond_1
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;->sRecentlyExplored:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    goto :goto_0
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 54
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;->shouldDropEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 55
    const/4 v4, 0x2

    const-string v5, "Dropping unwanted progress bar event"

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {p0, v4, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 72
    :goto_0
    return v3

    .line 59
    :cond_0
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->getEventTextOrDescription(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 60
    .local v2, "text":Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 61
    invoke-virtual {p3, v2}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    move v3, v4

    .line 62
    goto :goto_0

    .line 65
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;->getProgressPercent(Landroid/view/accessibility/AccessibilityEvent;)F

    move-result v0

    .line 66
    .local v0, "percent":F
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    float-to-double v8, v0

    const-wide/high16 v10, 0x4049000000000000L    # 50.0

    div-double/2addr v8, v10

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v8, v10

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    double-to-float v1, v6

    .line 68
    .local v1, "rate":F
    const v3, 0x7f050016

    invoke-virtual {p3, v3}, Lcom/google/android/marvin/talkback/Utterance;->addAuditory(I)V

    .line 69
    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getMetadata()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "earcon_rate"

    invoke-virtual {v3, v5, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 70
    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getMetadata()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "earcon_volume"

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    move v3, v4

    .line 72
    goto :goto_0
.end method
