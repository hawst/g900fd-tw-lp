.class Lcom/google/android/marvin/talkback/CachedFeedbackController$1;
.super Ljava/lang/Object;
.source "CachedFeedbackController.java"

# interfaces
.implements Landroid/media/SoundPool$OnLoadCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(IFFF)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/CachedFeedbackController;

.field final synthetic val$leftVolume:F

.field final synthetic val$rate:F

.field final synthetic val$rightVolume:F


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/CachedFeedbackController;FFF)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController$1;->this$0:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    iput p2, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController$1;->val$leftVolume:F

    iput p3, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController$1;->val$rightVolume:F

    iput p4, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController$1;->val$rate:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadComplete(Landroid/media/SoundPool;II)V
    .locals 7
    .param p1, "soundPool"    # Landroid/media/SoundPool;
    .param p2, "sampleId"    # I
    .param p3, "status"    # I

    .prologue
    const/4 v4, 0x0

    .line 245
    iget v2, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController$1;->val$leftVolume:F

    iget v3, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController$1;->val$rightVolume:F

    iget v6, p0, Lcom/google/android/marvin/talkback/CachedFeedbackController$1;->val$rate:F

    move-object v0, p1

    move v1, p2

    move v5, v4

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 246
    return-void
.end method
