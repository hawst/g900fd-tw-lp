.class Lcom/google/android/marvin/talkback/ProcessorLongHover$1;
.super Ljava/lang/Object;
.source "ProcessorLongHover.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/ProcessorLongHover;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/ProcessorLongHover;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/ProcessorLongHover;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover$1;->this$0:Lcom/google/android/marvin/talkback/ProcessorLongHover;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 201
    const/4 v1, 0x4

    if-eq p1, v1, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover$1;->this$0:Lcom/google/android/marvin/talkback/ProcessorLongHover;

    # getter for: Lcom/google/android/marvin/talkback/ProcessorLongHover;->mPendingLongHoverEvent:Landroid/view/accessibility/AccessibilityEvent;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/ProcessorLongHover;->access$000(Lcom/google/android/marvin/talkback/ProcessorLongHover;)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 206
    .local v0, "event":Landroid/view/accessibility/AccessibilityEvent;
    if-eqz v0, :cond_0

    .line 210
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorLongHover$1;->this$0:Lcom/google/android/marvin/talkback/ProcessorLongHover;

    # getter for: Lcom/google/android/marvin/talkback/ProcessorLongHover;->mHandler:Lcom/google/android/marvin/talkback/ProcessorLongHover$LongHoverHandler;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/ProcessorLongHover;->access$100(Lcom/google/android/marvin/talkback/ProcessorLongHover;)Lcom/google/android/marvin/talkback/ProcessorLongHover$LongHoverHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/marvin/talkback/ProcessorLongHover$LongHoverHandler;->startLongHoverTimeout(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0
.end method
