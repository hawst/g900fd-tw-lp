.class Lcom/google/android/marvin/talkback/KeyboardSearchManager$2;
.super Ljava/lang/Object;
.source "KeyboardSearchManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/KeyboardSearchManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)V
    .locals 0

    .prologue
    .line 365
    iput-object p1, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$2;->this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 368
    iget-object v8, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$2;->this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    # getter for: Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v8}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->access$000(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v8

    const v9, 0x7f06008f

    invoke-virtual {v8, v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 370
    .local v1, "hint":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 372
    iget-object v8, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$2;->this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    # getter for: Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;
    invoke-static {v8}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->access$100(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/googlecode/eyesfree/utils/NodeSearch;

    move-result-object v8

    invoke-virtual {v8}, Lcom/googlecode/eyesfree/utils/NodeSearch;->getCurrentQuery()Ljava/lang/String;

    move-result-object v5

    .line 373
    .local v5, "queryText":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 374
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$2;->this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    # getter for: Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v9}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->access$000(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v9

    const v10, 0x7f060090

    invoke-virtual {v9, v10}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 394
    :goto_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 396
    iget-object v8, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$2;->this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    # getter for: Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mHasNavigated:Z
    invoke-static {v8}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->access$200(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Z

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$2;->this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    # getter for: Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mNodeSearch:Lcom/googlecode/eyesfree/utils/NodeSearch;
    invoke-static {v8}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->access$100(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/googlecode/eyesfree/utils/NodeSearch;

    move-result-object v8

    invoke-virtual {v8}, Lcom/googlecode/eyesfree/utils/NodeSearch;->hasMatch()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 397
    :cond_0
    iget-object v8, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$2;->this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    # getter for: Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v8}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->access$000(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/googlecode/eyesfree/utils/FocusFinder;->getFocusedNode(Landroid/accessibilityservice/AccessibilityService;Z)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v6

    .line 398
    .local v6, "selected":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v6, :cond_4

    .line 399
    iget-object v8, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$2;->this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    # getter for: Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mLabelManager:Lcom/googlecode/eyesfree/labeling/CustomLabelManager;
    invoke-static {v8}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->access$300(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/googlecode/eyesfree/labeling/CustomLabelManager;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getNodeText(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/labeling/CustomLabelManager;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 401
    .local v4, "matchText":Ljava/lang/CharSequence;
    if-eqz v4, :cond_4

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v8

    if-lez v8, :cond_4

    .line 402
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$2;->this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    # getter for: Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v9}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->access$000(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v9

    const v10, 0x7f060093

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v4, v11, v12

    invoke-virtual {v9, v10, v11}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 403
    iget-object v8, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$2;->this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    # getter for: Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;
    invoke-static {v8}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->access$400(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v8

    const/4 v9, 0x3

    const/4 v10, 0x2

    const/4 v11, 0x0

    invoke-virtual {v8, v1, v9, v10, v11}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    .line 413
    .end local v4    # "matchText":Ljava/lang/CharSequence;
    .end local v6    # "selected":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :goto_1
    return-void

    .line 376
    :cond_1
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v3

    .line 377
    .local v3, "length":I
    const-string v7, ""

    .line 379
    .local v7, "separatedQuery":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v3, :cond_3

    .line 380
    invoke-virtual {v5, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    .line 381
    .local v0, "currentChar":Ljava/lang/Character;
    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v8

    invoke-static {v8}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 382
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$2;->this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    # getter for: Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v9}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->access$000(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v9

    const v10, 0x7f060251

    invoke-virtual {v9, v10}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 386
    :goto_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 379
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 384
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_3

    .line 390
    .end local v0    # "currentChar":Ljava/lang/Character;
    :cond_3
    const/4 v8, 0x0

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x2

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 391
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$2;->this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    # getter for: Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v9}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->access$000(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v9

    const v10, 0x7f060091

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v7, v11, v12

    invoke-virtual {v9, v10, v11}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 410
    .end local v2    # "i":I
    .end local v3    # "length":I
    .end local v7    # "separatedQuery":Ljava/lang/String;
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$2;->this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    # getter for: Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v9}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->access$000(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v9

    const v10, 0x7f060092

    invoke-virtual {v9, v10}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 411
    iget-object v8, p0, Lcom/google/android/marvin/talkback/KeyboardSearchManager$2;->this$0:Lcom/google/android/marvin/talkback/KeyboardSearchManager;

    # getter for: Lcom/google/android/marvin/talkback/KeyboardSearchManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;
    invoke-static {v8}, Lcom/google/android/marvin/talkback/KeyboardSearchManager;->access$400(Lcom/google/android/marvin/talkback/KeyboardSearchManager;)Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v8

    const/4 v9, 0x3

    const/4 v10, 0x2

    const/4 v11, 0x0

    invoke-virtual {v8, v1, v9, v10, v11}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    goto/16 :goto_1
.end method
