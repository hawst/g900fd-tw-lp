.class public Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;
.super Ljava/lang/Object;
.source "NodeMenuRuleProcessor.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field private static final mRules:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/marvin/talkback/menurules/NodeMenuRule;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;

.field private final mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;->mRules:Ljava/util/LinkedList;

    .line 50
    sget-object v0, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/menurules/RuleSpannables;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/menurules/RuleSpannables;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 51
    sget-object v0, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/menurules/RuleEditText;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/menurules/RuleEditText;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 52
    sget-object v0, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/menurules/RuleViewPager;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/menurules/RuleViewPager;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 53
    sget-object v0, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/menurules/RuleGranularity;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/menurules/RuleGranularity;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 55
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 56
    sget-object v0, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 59
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_1

    .line 60
    sget-object v0, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/menurules/RuleCustomAction;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/menurules/RuleCustomAction;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_1
    return-void
.end method

.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 1
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 70
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    .line 71
    return-void
.end method

.method private static collapseSubMenus(Lcom/googlecode/eyesfree/widget/RadialMenu;)V
    .locals 6
    .param p0, "menu"    # Lcom/googlecode/eyesfree/widget/RadialMenu;

    .prologue
    .line 134
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getItems(Z)Ljava/util/List;

    move-result-object v2

    .line 135
    .local v2, "menuItems":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/widget/RadialMenuItem;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    .line 136
    .local v1, "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->hasSubMenu()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 137
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getSubMenu()Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v4

    .line 138
    .local v4, "subMenu":Lcom/googlecode/eyesfree/widget/RadialSubMenu;
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->getItems(Z)Ljava/util/List;

    move-result-object v3

    .line 139
    .local v3, "subItems":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/widget/RadialMenuItem;>;"
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getItemId()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/googlecode/eyesfree/widget/RadialMenu;->removeItem(I)V

    .line 140
    invoke-virtual {p0, v3}, Lcom/googlecode/eyesfree/widget/RadialMenu;->addAll(Ljava/lang/Iterable;)V

    goto :goto_0

    .line 143
    .end local v1    # "item":Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .end local v3    # "subItems":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/widget/RadialMenuItem;>;"
    .end local v4    # "subMenu":Lcom/googlecode/eyesfree/widget/RadialSubMenu;
    :cond_1
    return-void
.end method


# virtual methods
.method public prepareMenuForNode(Lcom/googlecode/eyesfree/widget/RadialMenu;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 13
    .param p1, "menu"    # Lcom/googlecode/eyesfree/widget/RadialMenu;
    .param p2, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 82
    if-nez p2, :cond_0

    .line 124
    :goto_0
    return v7

    .line 87
    :cond_0
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->clear()V

    .line 90
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 91
    .local v2, "matchingRules":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/google/android/marvin/talkback/menurules/NodeMenuRule;>;"
    sget-object v9, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;->mRules:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/marvin/talkback/menurules/NodeMenuRule;

    .line 92
    .local v3, "rule":Lcom/google/android/marvin/talkback/menurules/NodeMenuRule;
    iget-object v9, p0, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-interface {v3, v9, p2}, Lcom/google/android/marvin/talkback/menurules/NodeMenuRule;->accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 93
    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 97
    .end local v3    # "rule":Lcom/google/android/marvin/talkback/menurules/NodeMenuRule;
    :cond_2
    const/4 v0, 0x0

    .line 99
    .local v0, "canCollapseMenu":Z
    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/marvin/talkback/menurules/NodeMenuRule;

    .line 100
    .restart local v3    # "rule":Lcom/google/android/marvin/talkback/menurules/NodeMenuRule;
    iget-object v9, p0, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-interface {v3, v9, p2}, Lcom/google/android/marvin/talkback/menurules/NodeMenuRule;->getMenuItemsForNode(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/List;

    move-result-object v4

    .line 101
    .local v4, "ruleResults":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/widget/RadialMenuItem;>;"
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_3

    .line 105
    iget-object v9, p0, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-interface {v3, v9}, Lcom/google/android/marvin/talkback/menurules/NodeMenuRule;->getUserFriendlyMenuName(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 106
    .local v6, "subMenuName":Ljava/lang/CharSequence;
    invoke-virtual {p1, v7, v7, v7, v6}, Lcom/googlecode/eyesfree/widget/RadialMenu;->addSubMenu(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v5

    .line 108
    .local v5, "ruleSubMenu":Lcom/googlecode/eyesfree/widget/RadialSubMenu;
    invoke-virtual {v5, v4}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->addAll(Ljava/lang/Iterable;)V

    .line 110
    invoke-interface {v3}, Lcom/google/android/marvin/talkback/menurules/NodeMenuRule;->canCollapseMenu()Z

    move-result v9

    or-int/2addr v0, v9

    .line 111
    goto :goto_2

    .line 114
    .end local v3    # "rule":Lcom/google/android/marvin/talkback/menurules/NodeMenuRule;
    .end local v4    # "ruleResults":Ljava/util/List;, "Ljava/util/List<Lcom/googlecode/eyesfree/widget/RadialMenuItem;>;"
    .end local v5    # "ruleSubMenu":Lcom/googlecode/eyesfree/widget/RadialSubMenu;
    .end local v6    # "subMenuName":Ljava/lang/CharSequence;
    :cond_4
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->size()I

    move-result v9

    if-ne v9, v8, :cond_5

    if-eqz v0, :cond_5

    .line 115
    invoke-static {p1}, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;->collapseSubMenus(Lcom/googlecode/eyesfree/widget/RadialMenu;)V

    .line 118
    :cond_5
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->size()I

    move-result v9

    if-nez v9, :cond_6

    .line 119
    iget-object v8, p0, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    iget-object v9, p0, Lcom/google/android/marvin/talkback/menurules/NodeMenuRuleProcessor;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v10, 0x7f06012b

    invoke-virtual {v9, v10}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x3

    const/4 v11, 0x2

    const/4 v12, 0x0

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    goto :goto_0

    :cond_6
    move v7, v8

    .line 124
    goto :goto_0
.end method
