.class Lcom/google/android/marvin/talkback/TalkBackService$6;
.super Ljava/lang/Object;
.source "TalkBackService.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/KeyComboManager$KeyComboListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/TalkBackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackService;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0

    .prologue
    .line 1568
    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackService$6;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComboPerformed(I)Z
    .locals 3
    .param p1, "id"    # I

    .prologue
    const/4 v0, 0x1

    .line 1571
    const v1, 0x7f0d0018

    if-ne p1, v1, :cond_0

    .line 1572
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService$6;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->requestSuspendTalkBack()V

    .line 1598
    :goto_0
    return v0

    .line 1574
    :cond_0
    const v1, 0x7f0d000f

    if-ne p1, v1, :cond_1

    .line 1575
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService$6;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v1, v0}, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->performGlobalAction(Landroid/accessibilityservice/AccessibilityService;I)Z

    goto :goto_0

    .line 1578
    :cond_1
    const v1, 0x7f0d0010

    if-ne p1, v1, :cond_2

    .line 1579
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService$6;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->performGlobalAction(Landroid/accessibilityservice/AccessibilityService;I)Z

    goto :goto_0

    .line 1582
    :cond_2
    const v1, 0x7f0d0011

    if-ne p1, v1, :cond_3

    .line 1583
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService$6;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->performGlobalAction(Landroid/accessibilityservice/AccessibilityService;I)Z

    goto :goto_0

    .line 1586
    :cond_3
    const v1, 0x7f0d0012

    if-ne p1, v1, :cond_4

    .line 1587
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService$6;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->performGlobalAction(Landroid/accessibilityservice/AccessibilityService;I)Z

    goto :goto_0

    .line 1590
    :cond_4
    const v1, 0x7f0d0019

    if-ne p1, v1, :cond_5

    .line 1591
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService$6;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->access$500(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/CursorController;->previousGranularity()Z

    goto :goto_0

    .line 1593
    :cond_5
    const v1, 0x7f0d001a

    if-ne p1, v1, :cond_6

    .line 1594
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService$6;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->access$500(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/CursorController;->nextGranularity()Z

    goto :goto_0

    .line 1598
    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method
