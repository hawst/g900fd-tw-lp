.class public final Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;
.super Ljava/lang/Object;
.source "TextFormatters.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/formatter/TextFormatters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ChangedTextFormatter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    return-void
.end method

.method private appendLastWordIfNeeded(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 294
    # invokes: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->getEventText(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$300(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 295
    .local v2, "text":Ljava/lang/CharSequence;
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v1

    .line 297
    .local v1, "fromIndex":I
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-le v1, v6, :cond_1

    .line 298
    const/4 v6, 0x5

    const-string v7, "Received event with invalid fromIndex: %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v4

    invoke-static {p0, v6, v7, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 317
    :cond_0
    :goto_0
    return v4

    .line 303
    :cond_1
    invoke-interface {v2, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 307
    invoke-static {v2, v1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;->getPrecedingWhitespace(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 308
    .local v0, "breakIndex":I
    invoke-interface {v2, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    .line 311
    .local v3, "word":Ljava/lang/CharSequence;
    invoke-static {v3}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v6

    if-eqz v6, :cond_0

    .line 315
    invoke-virtual {p3, v3}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    move v4, v5

    .line 317
    goto :goto_0
.end method

.method private static appendSpellingToUtterance(Landroid/content/Context;Lcom/google/android/marvin/talkback/Utterance;Ljava/lang/CharSequence;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;
    .param p2, "word"    # Ljava/lang/CharSequence;

    .prologue
    .line 323
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    const/4 v4, 0x1

    if-gt v3, v4, :cond_1

    .line 332
    :cond_0
    return-void

    .line 327
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 328
    invoke-interface {p2, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    .line 329
    .local v0, "character":Ljava/lang/CharSequence;
    invoke-static {p0, v0}, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->cleanUp(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 330
    .local v1, "cleaned":Ljava/lang/CharSequence;
    invoke-virtual {p1, v1}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 327
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private formatInternal(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;
    .locals 16
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    .line 196
    invoke-static/range {p2 .. p2}, Lcom/googlecode/eyesfree/compat/provider/SettingsCompatUtils$SecureCompatUtils;->shouldSpeakPasswords(Landroid/content/Context;)Z

    move-result v9

    .line 198
    .local v9, "shouldSpeakPasswords":Z
    invoke-virtual/range {p1 .. p1}, Landroid/view/accessibility/AccessibilityEvent;->isPassword()Z

    move-result v11

    if-eqz v11, :cond_0

    if-nez v9, :cond_0

    .line 199
    invoke-direct/range {p0 .. p3}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;->formatPassword(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Lcom/google/android/marvin/talkback/Utterance;)Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    move-result-object v11

    .line 289
    :goto_0
    return-object v11

    .line 202
    :cond_0
    invoke-direct/range {p0 .. p1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;->passesSanityCheck(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 203
    const/4 v11, 0x6

    const-string v12, "Inconsistent text change event detected"

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v11, v12, v13}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 204
    sget-object v11, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->REJECTED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    goto :goto_0

    .line 208
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/accessibility/AccessibilityEvent;->getRemovedCount()I

    move-result v11

    const/4 v12, 0x1

    if-le v11, v12, :cond_2

    invoke-virtual/range {p1 .. p1}, Landroid/view/accessibility/AccessibilityEvent;->getAddedCount()I

    move-result v11

    if-nez v11, :cond_2

    invoke-virtual/range {p1 .. p1}, Landroid/view/accessibility/AccessibilityEvent;->getBeforeText()Ljava/lang/CharSequence;

    move-result-object v11

    invoke-interface {v11}, Ljava/lang/CharSequence;->length()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Landroid/view/accessibility/AccessibilityEvent;->getRemovedCount()I

    move-result v12

    if-ne v11, v12, :cond_2

    const/4 v10, 0x1

    .line 210
    .local v10, "wasCleared":Z
    :goto_1
    if-eqz v10, :cond_3

    .line 211
    const v11, 0x7f0600c3

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 212
    sget-object v11, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->REMOVED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    goto :goto_0

    .line 208
    .end local v10    # "wasCleared":Z
    :cond_2
    const/4 v10, 0x0

    goto :goto_1

    .line 215
    .restart local v10    # "wasCleared":Z
    :cond_3
    invoke-direct/range {p0 .. p2}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;->getRemovedText(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 216
    .local v7, "removedText":Ljava/lang/CharSequence;
    invoke-direct/range {p0 .. p2}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;->getAddedText(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 219
    .local v3, "addedText":Ljava/lang/CharSequence;
    invoke-static {v3, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 220
    const/4 v11, 0x3

    const-string v12, "Drop event, nothing changed"

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v11, v12, v13}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 221
    sget-object v11, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->REJECTED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    goto :goto_0

    .line 225
    :cond_4
    if-eqz v7, :cond_5

    if-nez v3, :cond_6

    .line 226
    :cond_5
    const/4 v11, 0x3

    const-string v12, "Drop event, either added or removed was null"

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v11, v12, v13}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 227
    sget-object v11, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->REJECTED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    goto :goto_0

    .line 230
    :cond_6
    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v6

    .line 231
    .local v6, "removedLength":I
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 234
    .local v2, "addedLength":I
    if-le v6, v2, :cond_8

    .line 235
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v7, v11, v3, v12, v2}, Landroid/text/TextUtils;->regionMatches(Ljava/lang/CharSequence;ILjava/lang/CharSequence;II)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 236
    invoke-static {v7, v2, v6}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v7

    .line 237
    const-string v3, ""

    .line 248
    :cond_7
    :goto_2
    move-object/from16 v0, p2

    invoke-static {v0, v7}, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->cleanUp(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 249
    .local v5, "cleanRemovedText":Ljava/lang/CharSequence;
    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->cleanUp(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 251
    .local v4, "cleanAddedText":Ljava/lang/CharSequence;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_e

    .line 253
    invoke-direct/range {p0 .. p3}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;->appendLastWordIfNeeded(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Lcom/google/android/marvin/talkback/Utterance;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 278
    :goto_3
    sget-object v11, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->ADDED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    goto/16 :goto_0

    .line 239
    .end local v4    # "cleanAddedText":Ljava/lang/CharSequence;
    .end local v5    # "cleanRemovedText":Ljava/lang/CharSequence;
    :cond_8
    if-le v2, v6, :cond_7

    .line 240
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v7, v11, v3, v12, v6}, Landroid/text/TextUtils;->regionMatches(Ljava/lang/CharSequence;ILjava/lang/CharSequence;II)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 241
    const-string v7, ""

    .line 242
    invoke-static {v3, v6, v2}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 255
    .restart local v4    # "cleanAddedText":Ljava/lang/CharSequence;
    .restart local v5    # "cleanRemovedText":Ljava/lang/CharSequence;
    :cond_9
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_a

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 257
    :cond_a
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 258
    :cond_b
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0b0010

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v11

    if-nez v11, :cond_c

    .line 263
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 265
    :cond_c
    const v11, 0x7f0600a8

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v4, v12, v13

    const/4 v13, 0x1

    aput-object v5, v12, v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 267
    .local v8, "replacedText":Ljava/lang/String;
    move-object/from16 v0, p3

    invoke-virtual {v0, v8}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 271
    invoke-static {}, Lcom/google/android/marvin/talkback/ActionHistory;->getInstance()Lcom/google/android/marvin/talkback/ActionHistory;

    move-result-object v11

    const v12, 0x8000

    invoke-virtual/range {p1 .. p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v14

    invoke-virtual {v11, v12, v14, v15}, Lcom/google/android/marvin/talkback/ActionHistory;->hasActionAtTime(IJ)Z

    move-result v11

    if-nez v11, :cond_d

    .line 273
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v0, v1, v3}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;->appendSpellingToUtterance(Landroid/content/Context;Lcom/google/android/marvin/talkback/Utterance;Ljava/lang/CharSequence;)V

    .line 276
    :cond_d
    sget-object v11, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->REPLACED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    goto/16 :goto_0

    .line 281
    .end local v8    # "replacedText":Ljava/lang/String;
    :cond_e
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_f

    .line 283
    const v11, 0x7f0600a9

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v5, v12, v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 285
    sget-object v11, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->REMOVED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    goto/16 :goto_0

    .line 288
    :cond_f
    const/4 v11, 0x3

    const-string v12, "Drop event, cleaned up text was empty"

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v11, v12, v13}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 289
    sget-object v11, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->REJECTED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    goto/16 :goto_0
.end method

.method private formatPassword(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Lcom/google/android/marvin/talkback/Utterance;)Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;
    .locals 7
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    const v4, 0x7f06022e

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 442
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getRemovedCount()I

    move-result v1

    .line 443
    .local v1, "removed":I
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getAddedCount()I

    move-result v0

    .line 445
    .local v0, "added":I
    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 446
    sget-object v2, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->REJECTED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    .line 457
    :goto_0
    return-object v2

    .line 447
    :cond_0
    if-ne v0, v5, :cond_1

    if-nez v1, :cond_1

    .line 448
    invoke-virtual {p2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 449
    sget-object v2, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->ADDED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    goto :goto_0

    .line 450
    :cond_1
    if-nez v0, :cond_2

    if-ne v1, v5, :cond_2

    .line 451
    const v2, 0x7f0600a9

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {p2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p2, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 453
    sget-object v2, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->REMOVED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    goto :goto_0

    .line 455
    :cond_2
    const v2, 0x7f0600aa

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p2, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/google/android/marvin/talkback/Utterance;->addSpoken(Ljava/lang/CharSequence;)V

    .line 457
    sget-object v2, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->REPLACED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    goto :goto_0
.end method

.method private getAddedText(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 11
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x1

    const/4 v4, 0x0

    const/4 v9, 0x5

    const/4 v8, 0x0

    .line 381
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v3

    .line 382
    .local v3, "textList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v10, :cond_1

    .line 383
    :cond_0
    const-string v5, "getAddedText: Text list was null or bad size"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {p0, v9, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 407
    :goto_0
    return-object v4

    .line 389
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_2

    .line 390
    const-string v4, ""

    goto :goto_0

    .line 393
    :cond_2
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 394
    .local v2, "text":Ljava/lang/CharSequence;
    if-nez v2, :cond_3

    .line 395
    const-string v5, "getAddedText: First text entry was null"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {p0, v9, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 399
    :cond_3
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v0

    .line 400
    .local v0, "addedBegIndex":I
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getAddedCount()I

    move-result v5

    add-int v1, v0, v5

    .line 401
    .local v1, "addedEndIndex":I
    # invokes: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->areValidIndices(Ljava/lang/CharSequence;II)Z
    invoke-static {v2, v0, v1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$400(Ljava/lang/CharSequence;II)Z

    move-result v5

    if-nez v5, :cond_4

    .line 402
    const-string v5, "getAddedText: Invalid indices (%d,%d) for \"%s\""

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    const/4 v7, 0x2

    aput-object v2, v6, v7

    invoke-static {p0, v9, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 407
    :cond_4
    invoke-interface {v2, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    goto :goto_0
.end method

.method private static getPrecedingWhitespace(Ljava/lang/CharSequence;I)I
    .locals 2
    .param p0, "text"    # Ljava/lang/CharSequence;
    .param p1, "fromIndex"    # I

    .prologue
    .line 335
    add-int/lit8 v0, p1, -0x1

    .local v0, "i":I
    :goto_0
    if-lez v0, :cond_1

    .line 336
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 341
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 335
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 341
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private getRemovedText(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 5
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 418
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getBeforeText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 419
    .local v2, "beforeText":Ljava/lang/CharSequence;
    if-nez v2, :cond_1

    .line 429
    :cond_0
    :goto_0
    return-object v3

    .line 423
    :cond_1
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v0

    .line 424
    .local v0, "beforeBegIndex":I
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getRemovedCount()I

    move-result v4

    add-int v1, v0, v4

    .line 425
    .local v1, "beforeEndIndex":I
    # invokes: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->areValidIndices(Ljava/lang/CharSequence;II)Z
    invoke-static {v2, v0, v1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$400(Ljava/lang/CharSequence;II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 429
    invoke-interface {v2, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_0
.end method

.method private passesSanityCheck(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 354
    # invokes: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->getEventText(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$300(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 355
    .local v0, "afterText":Ljava/lang/CharSequence;
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getBeforeText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 360
    .local v1, "beforeText":Ljava/lang/CharSequence;
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getAddedCount()I

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getRemovedCount()I

    move-result v5

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-ne v5, v6, :cond_1

    .line 370
    :cond_0
    :goto_0
    return v3

    .line 364
    :cond_1
    if-eqz v0, :cond_2

    if-nez v1, :cond_3

    :cond_2
    move v3, v4

    .line 365
    goto :goto_0

    .line 368
    :cond_3
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getAddedCount()I

    move-result v5

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getRemovedCount()I

    move-result v6

    sub-int v2, v5, v6

    .line 370
    .local v2, "diff":I
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v5

    add-int/2addr v5, v2

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-eq v5, v6, :cond_0

    move v3, v4

    goto :goto_0
.end method

.method private shouldEchoKeyboard(Landroid/content/Context;Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "changeType"    # Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 169
    sget-object v6, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->REMOVED:Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    if-ne p2, v6, :cond_1

    move v4, v5

    .line 190
    :cond_0
    :goto_0
    :pswitch_0
    return v4

    .line 173
    :cond_1
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 174
    .local v2, "prefs":Landroid/content/SharedPreferences;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 175
    .local v3, "res":Landroid/content/res/Resources;
    const v6, 0x7f06002e

    const v7, 0x7f06006d

    invoke-static {v2, v3, v6, v7}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getIntFromStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)I

    move-result v1

    .line 178
    .local v1, "keyboardPref":I
    packed-switch v1, :pswitch_data_0

    .line 188
    const/4 v6, 0x6

    const-string v7, "Invalid keyboard echo preference value: %d"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v4

    invoke-static {p0, v6, v7, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    move v4, v5

    .line 180
    goto :goto_0

    .line 182
    :pswitch_2
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 183
    .local v0, "config":Landroid/content/res/Configuration;
    iget v6, v0, Landroid/content/res/Configuration;->keyboard:I

    if-eq v6, v5, :cond_2

    iget v6, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    :cond_2
    move v4, v5

    goto :goto_0

    .line 178
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3, "utterance"    # Lcom/google/android/marvin/talkback/Utterance;

    .prologue
    .line 111
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v4

    .line 115
    .local v4, "timestamp":J
    # getter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sAwaitingSelectionCount:I
    invoke-static {}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$000()I

    move-result v6

    if-lez v6, :cond_3

    .line 116
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v6

    # getter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedTimestamp:J
    invoke-static {}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$100()J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x96

    cmp-long v6, v6, v8

    if-ltz v6, :cond_0

    const/4 v1, 0x1

    .line 118
    .local v1, "hasDelayElapsed":Z
    :goto_0
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v6

    # getter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedPackage:Ljava/lang/CharSequence;
    invoke-static {}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$200()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    const/4 v2, 0x1

    .line 124
    .local v2, "hasPackageChanged":Z
    :goto_1
    if-nez v1, :cond_2

    if-nez v2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0010

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 126
    # operator++ for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sAwaitingSelectionCount:I
    invoke-static {}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$008()I

    .line 127
    # setter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedTimestamp:J
    invoke-static {v4, v5}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$102(J)J

    .line 128
    const/4 v6, 0x0

    .line 164
    .end local v1    # "hasDelayElapsed":Z
    .end local v2    # "hasPackageChanged":Z
    :goto_2
    return v6

    .line 116
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 118
    .restart local v1    # "hasDelayElapsed":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 132
    .restart local v2    # "hasPackageChanged":Z
    :cond_2
    const/4 v6, 0x0

    # setter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sAwaitingSelectionCount:I
    invoke-static {v6}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$002(I)I

    .line 135
    .end local v1    # "hasDelayElapsed":Z
    .end local v2    # "hasPackageChanged":Z
    :cond_3
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;->formatInternal(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;

    move-result-object v0

    .line 138
    .local v0, "changeType":Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 139
    .local v3, "params":Landroid/os/Bundle;
    const-string v6, "rate"

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v3, v6, v7}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 140
    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getMetadata()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "speech_params"

    invoke-virtual {v6, v7, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 142
    sget-object v6, Lcom/google/android/marvin/talkback/formatter/TextFormatters$1;->$SwitchMap$com$google$android$marvin$talkback$formatter$TextFormatters$ChangedTextFormatter$ChangeType:[I

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 156
    :goto_3
    const/4 v6, 0x1

    # setter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sAwaitingSelectionCount:I
    invoke-static {v6}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$002(I)I

    .line 157
    # setter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedTimestamp:J
    invoke-static {v4, v5}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$102(J)J

    .line 158
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v6

    # setter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedPackage:Ljava/lang/CharSequence;
    invoke-static {v6}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$202(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 160
    invoke-direct {p0, p2, v0}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;->shouldEchoKeyboard(Landroid/content/Context;Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter$ChangeType;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 161
    const/4 v6, 0x0

    goto :goto_2

    .line 145
    :pswitch_0
    const-string v6, "pitch"

    const v7, 0x3f99999a    # 1.2f

    invoke-virtual {v3, v6, v7}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    goto :goto_3

    .line 149
    :pswitch_1
    const-string v6, "pitch"

    const v7, 0x3f99999a    # 1.2f

    invoke-virtual {v3, v6, v7}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    goto :goto_3

    .line 153
    :pswitch_2
    const/4 v6, 0x0

    goto :goto_2

    .line 164
    :cond_4
    const/4 v6, 0x1

    goto :goto_2

    .line 142
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
