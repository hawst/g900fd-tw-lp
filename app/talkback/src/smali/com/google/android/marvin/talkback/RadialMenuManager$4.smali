.class Lcom/google/android/marvin/talkback/RadialMenuManager$4;
.super Ljava/lang/Object;
.source "RadialMenuManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/RadialMenuManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/RadialMenuManager;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$4;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 266
    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$4;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mService:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$400(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    const v3, 0x7f0600d1

    invoke-virtual {v0, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 268
    .local v1, "hintText":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$4;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # setter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mHintSpeechPending:Z
    invoke-static {v0, v4}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$802(Lcom/google/android/marvin/talkback/RadialMenuManager;Z)Z

    .line 269
    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$4;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$500(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    const/4 v5, 0x2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$4;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mHintSpeechCompleted:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;
    invoke-static {v3}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$900(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    move-result-object v8

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;Ljava/util/Set;Ljava/util/Set;IILandroid/os/Bundle;Landroid/os/Bundle;Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    .line 271
    return-void
.end method
