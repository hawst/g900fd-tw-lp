.class public Lcom/google/android/marvin/talkback/CursorGranularityManager;
.super Ljava/lang/Object;
.source "CursorGranularityManager.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/CursorGranularityManager$1;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mCurrentNodeIndex:I

.field private mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private final mNavigableNodes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;"
        }
    .end annotation
.end field

.field private mRequestedGranularityIndex:I

.field private mSelectionModeActive:Z

.field private final mSupportedGranularities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/marvin/talkback/CursorGranularity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mNavigableNodes:Ljava/util/ArrayList;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mSupportedGranularities:Ljava/util/ArrayList;

    .line 90
    iput-object p1, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mContext:Landroid/content/Context;

    .line 91
    return-void
.end method

.method private clearAndRetainWebState(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 2
    .param p1, "focusedNode"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->getRequestedGranularity()Lcom/google/android/marvin/talkback/CursorGranularity;

    move-result-object v0

    .line 228
    .local v0, "currentGranularity":Lcom/google/android/marvin/talkback/CursorGranularity;
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->clear()V

    .line 229
    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/CursorGranularity;->isWebGranularity()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasNativeWebContent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 231
    invoke-virtual {p0, p1, v0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->setGranularityAt(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/google/android/marvin/talkback/CursorGranularity;)Z

    .line 233
    :cond_0
    return-void
.end method

.method private static extractNavigableNodes(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/util/ArrayList;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "root"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 452
    .local p2, "nodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;>;"
    if-nez p1, :cond_1

    .line 453
    const/4 v3, 0x0

    .line 484
    :cond_0
    return v3

    .line 456
    :cond_1
    if-eqz p2, :cond_2

    .line 457
    invoke-static {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 460
    :cond_2
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getMovementGranularities()I

    move-result v3

    .line 463
    .local v3, "supportedGranularities":I
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 467
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v1

    .line 468
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 469
    invoke-virtual {p1, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    .line 470
    .local v0, "child":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-nez v0, :cond_3

    .line 468
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 474
    :cond_3
    const/high16 v4, 0x20000

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(ILandroid/os/Bundle;)Z

    .line 477
    invoke-static {p0, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 478
    invoke-static {p0, v0, p2}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->extractNavigableNodes(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/util/ArrayList;)I

    move-result v4

    or-int/2addr v3, v4

    .line 481
    :cond_4
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_1
.end method

.method public static getSupportedGranularities(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/List;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "root"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/marvin/talkback/CursorGranularity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 432
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 433
    .local v1, "supported":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/google/android/marvin/talkback/CursorGranularity;>;"
    const/4 v3, 0x0

    invoke-static {p0, p1, v3}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->extractNavigableNodes(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/util/ArrayList;)I

    move-result v2

    .line 434
    .local v2, "supportedMask":I
    invoke-static {p0, p1}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasNavigableWebContent(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    .line 436
    .local v0, "hasWebContent":Z
    invoke-static {v2, v0, v1}, Lcom/google/android/marvin/talkback/CursorGranularity;->extractFromMask(IZLjava/util/List;)V

    .line 438
    return-object v1
.end method

.method private navigateWeb(ILcom/google/android/marvin/talkback/CursorGranularity;)I
    .locals 5
    .param p1, "action"    # I
    .param p2, "granularity"    # Lcom/google/android/marvin/talkback/CursorGranularity;

    .prologue
    const/4 v2, -0x1

    .line 347
    sparse-switch p1, :sswitch_data_0

    .line 378
    :goto_0
    return v2

    .line 349
    :sswitch_0
    const/4 v1, 0x1

    .line 359
    .local v1, "movementType":I
    :goto_1
    sget-object v3, Lcom/google/android/marvin/talkback/CursorGranularityManager$1;->$SwitchMap$com$google$android$marvin$talkback$CursorGranularity:[I

    invoke-virtual {p2}, Lcom/google/android/marvin/talkback/CursorGranularity;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 361
    :pswitch_0
    const-string v0, "SECTION"

    .line 373
    .local v0, "htmlElementType":Ljava/lang/String;
    :goto_2
    iget-object v2, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-static {v2, v1, v0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performNavigationToHtmlElementAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ILjava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 375
    const/4 v2, 0x0

    goto :goto_0

    .line 352
    .end local v0    # "htmlElementType":Ljava/lang/String;
    .end local v1    # "movementType":I
    :sswitch_1
    const/4 v1, -0x1

    .line 353
    .restart local v1    # "movementType":I
    goto :goto_1

    .line 364
    :pswitch_1
    const-string v0, "LIST"

    .line 365
    .restart local v0    # "htmlElementType":Ljava/lang/String;
    goto :goto_2

    .line 367
    .end local v0    # "htmlElementType":Ljava/lang/String;
    :pswitch_2
    const-string v0, "CONTROL"

    .line 368
    .restart local v0    # "htmlElementType":Ljava/lang/String;
    goto :goto_2

    .line 378
    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    .line 347
    :sswitch_data_0
    .sparse-switch
        0x100 -> :sswitch_0
        0x200 -> :sswitch_1
    .end sparse-switch

    .line 359
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setLockedNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 6
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 388
    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v3, p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 389
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->clear()V

    .line 392
    :cond_0
    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez v3, :cond_2

    .line 393
    invoke-static {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 395
    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->shouldClearSelection(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 396
    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/high16 v4, 0x20000

    invoke-virtual {v3, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    .line 400
    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mSupportedGranularities:Ljava/util/ArrayList;

    .line 401
    .local v1, "supported":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/marvin/talkback/CursorGranularity;>;"
    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mNavigableNodes:Ljava/util/ArrayList;

    invoke-static {v3, v4, v5}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->extractNavigableNodes(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/util/ArrayList;)I

    move-result v2

    .line 402
    .local v2, "supportedMask":I
    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-static {v3, v4}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasNavigableWebContent(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    .line 405
    .local v0, "hasWebContent":Z
    invoke-static {v2, v0, v1}, Lcom/google/android/marvin/talkback/CursorGranularity;->extractFromMask(IZLjava/util/List;)V

    .line 407
    .end local v0    # "hasWebContent":Z
    .end local v1    # "supported":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/marvin/talkback/CursorGranularity;>;"
    .end local v2    # "supportedMask":I
    :cond_2
    return-void
.end method

.method private shouldClearSelection(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 5
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 418
    iget-object v2, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mContext:Landroid/content/Context;

    new-array v3, v0, [Ljava/lang/Class;

    const-class v4, Landroid/widget/EditText;

    aput-object v4, v3, v1

    invoke-static {v2, p1, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesAnyClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public adjustGranularityAt(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Z
    .locals 3
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "direction"    # I

    .prologue
    .line 186
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->setLockedNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 188
    iget v1, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    .line 189
    .local v1, "current":I
    iget-object v2, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mSupportedGranularities:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 192
    .local v0, "count":I
    iget v2, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    add-int/2addr v2, p2

    rem-int/2addr v2, v0

    iput v2, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    .line 193
    iget v2, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    if-gez v2, :cond_0

    .line 194
    add-int/lit8 v2, v0, -0x1

    iput v2, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    .line 197
    :cond_0
    iget v2, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    if-eq v2, v1, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public clear()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 207
    iput v2, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    .line 208
    iput v2, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    .line 209
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mSupportedGranularities:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 211
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mNavigableNodes:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mNavigableNodes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 214
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 215
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 217
    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->setSelectionModeActive(Z)V

    .line 218
    return-void
.end method

.method public getRequestedGranularity()Lcom/google/android/marvin/talkback/CursorGranularity;
    .locals 2

    .prologue
    .line 123
    iget v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mSupportedGranularities:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 125
    :cond_0
    sget-object v0, Lcom/google/android/marvin/talkback/CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularity;

    .line 128
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mSupportedGranularities:Ljava/util/ArrayList;

    iget v1, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/marvin/talkback/CursorGranularity;

    goto :goto_0
.end method

.method public isLockedTo(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 2
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v0, 0x0

    .line 110
    iget v1, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    if-nez v1, :cond_1

    .line 114
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v1, p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSelectionModeActive()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mSelectionModeActive:Z

    return v0
.end method

.method public navigate(I)I
    .locals 11
    .param p1, "action"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, -0x1

    .line 262
    iget-object v8, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez v8, :cond_1

    .line 318
    :cond_0
    :goto_0
    return v5

    .line 266
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->getRequestedGranularity()Lcom/google/android/marvin/talkback/CursorGranularity;

    move-result-object v4

    .line 267
    .local v4, "requestedGranularity":Lcom/google/android/marvin/talkback/CursorGranularity;
    if-eqz v4, :cond_0

    sget-object v8, Lcom/google/android/marvin/talkback/CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularity;

    if-eq v4, v8, :cond_0

    .line 272
    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/CursorGranularity;->isWebGranularity()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 273
    invoke-direct {p0, p1, v4}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->navigateWeb(ILcom/google/android/marvin/talkback/CursorGranularity;)I

    move-result v5

    goto :goto_0

    .line 276
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 277
    .local v0, "arguments":Landroid/os/Bundle;
    iget-object v8, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mNavigableNodes:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 280
    .local v1, "count":I
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 282
    :sswitch_0
    const/4 v3, 0x1

    .line 283
    .local v3, "increment":I
    iget v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    if-gez v5, :cond_3

    .line 284
    iget v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    .line 297
    :cond_3
    :goto_1
    iget v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    if-ltz v5, :cond_6

    iget v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    if-ge v5, v1, :cond_6

    .line 298
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->isSelectionModeActive()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 299
    const-string v5, "ACTION_ARGUMENT_EXTEND_SELECTION_BOOLEAN"

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 303
    :cond_4
    const-string v5, "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT"

    iget v8, v4, Lcom/google/android/marvin/talkback/CursorGranularity;->value:I

    invoke-virtual {v0, v5, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 306
    iget-object v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mNavigableNodes:Ljava/util/ArrayList;

    iget v8, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 307
    .local v2, "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-virtual {v2, p1, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(ILandroid/os/Bundle;)Z

    move-result v5

    if-eqz v5, :cond_5

    move v5, v6

    .line 308
    goto :goto_0

    .line 288
    .end local v2    # "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .end local v3    # "increment":I
    :sswitch_1
    const/4 v3, -0x1

    .line 289
    .restart local v3    # "increment":I
    iget v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    if-lt v5, v1, :cond_3

    .line 290
    iget v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    goto :goto_1

    .line 311
    .restart local v2    # "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_5
    const/4 v5, 0x2

    const-string v8, "Failed to move with granularity %s, trying next node"

    new-array v9, v6, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/CursorGranularity;->name()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    invoke-static {p0, v5, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 315
    iget v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    add-int/2addr v5, v3

    iput v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    goto :goto_1

    .end local v2    # "currentNode":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_6
    move v5, v7

    .line 318
    goto :goto_0

    .line 280
    nop

    :sswitch_data_0
    .sparse-switch
        0x100 -> :sswitch_0
        0x200 -> :sswitch_1
    .end sparse-switch
.end method

.method public navigateCurrent()V
    .locals 1

    .prologue
    .line 326
    const/16 v0, 0x100

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->navigate(I)I

    .line 327
    const/16 v0, 0x200

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->navigate(I)I

    .line 328
    return-void
.end method

.method public onNodeFocused(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 2
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 247
    :cond_1
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getWindowId()I

    move-result v0

    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getWindowId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 248
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->clearAndRetainWebState(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0
.end method

.method public setGranularityAt(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/google/android/marvin/talkback/CursorGranularity;)Z
    .locals 3
    .param p1, "node"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "granularity"    # Lcom/google/android/marvin/talkback/CursorGranularity;

    .prologue
    const/4 v1, 0x0

    .line 140
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->setLockedNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 142
    iget-object v2, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mSupportedGranularities:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 143
    .local v0, "index":I
    if-gez v0, :cond_0

    .line 144
    iput v1, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    .line 153
    :goto_0
    return v1

    .line 148
    :cond_0
    iput v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    .line 150
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasNativeWebContent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 151
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->navigateCurrent()V

    .line 153
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setSelectionModeActive(Z)V
    .locals 0
    .param p1, "active"    # Z

    .prologue
    .line 165
    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mSelectionModeActive:Z

    .line 166
    return-void
.end method

.method public shutdown()V
    .locals 0

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->clear()V

    .line 98
    return-void
.end method
