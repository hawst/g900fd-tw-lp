.class Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$LoadDefaultOverridesTask;
.super Landroid/os/AsyncTask;
.source "PreferenceDefaultsProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadDefaultOverridesTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/io/File;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;


# direct methods
.method private constructor <init>(Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$LoadDefaultOverridesTask;->this$0:Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;
    .param p2, "x1"    # Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$1;

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$LoadDefaultOverridesTask;-><init>(Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/io/File;)Ljava/lang/Boolean;
    .locals 17
    .param p1, "params"    # [Ljava/io/File;

    .prologue
    .line 117
    if-eqz p1, :cond_0

    move-object/from16 v0, p1

    array-length v12, v0

    const/4 v13, 0x1

    if-eq v12, v13, :cond_1

    .line 118
    :cond_0
    const/4 v12, 0x6

    const-string v13, "LoadDefaultOverridesTask executed with invalid arguments."

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13, v14}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 120
    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    .line 195
    :goto_0
    return-object v12

    .line 123
    :cond_1
    new-instance v9, Ljava/util/Properties;

    invoke-direct {v9}, Ljava/util/Properties;-><init>()V

    .line 125
    .local v9, "properties":Ljava/util/Properties;
    :try_start_0
    new-instance v10, Ljava/io/InputStreamReader;

    new-instance v12, Ljava/io/FileInputStream;

    const/4 v13, 0x0

    aget-object v13, p1, v13

    invoke-direct {v12, v13}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v13, "UTF-8"

    invoke-direct {v10, v12, v13}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 127
    .local v10, "reader":Ljava/io/InputStreamReader;
    invoke-virtual {v9, v10}, Ljava/util/Properties;->load(Ljava/io/Reader;)V

    .line 128
    invoke-virtual {v10}, Ljava/io/InputStreamReader;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 144
    const/4 v8, 0x0

    .line 145
    .local v8, "prefsProcessed":I
    invoke-virtual {v9}, Ljava/util/Properties;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_8

    .line 146
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$LoadDefaultOverridesTask;->this$0:Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;

    # getter for: Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->mPrefs:Landroid/content/SharedPreferences;
    invoke-static {v12}, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->access$100(Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;)Landroid/content/SharedPreferences;

    move-result-object v12

    invoke-interface {v12}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 147
    .local v7, "prefs":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {v9}, Ljava/util/Properties;->stringPropertyNames()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 149
    .local v6, "prefName":Ljava/lang/String;
    # getter for: Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;
    invoke-static {}, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->access$200()Ljava/util/HashMap;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 150
    const/4 v12, 0x6

    const-string v13, "Unknown pref \"%s\", skipping."

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v6, v14, v15

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13, v14}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 129
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v6    # "prefName":Ljava/lang/String;
    .end local v7    # "prefs":Landroid/content/SharedPreferences$Editor;
    .end local v8    # "prefsProcessed":I
    .end local v10    # "reader":Ljava/io/InputStreamReader;
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Ljava/io/FileNotFoundException;
    const/4 v12, 0x4

    const-string v13, "No default override configuration file found at %s."

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const/16 v16, 0x0

    aget-object v16, p1, v16

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13, v14}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 132
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 133
    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    goto :goto_0

    .line 134
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 135
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v12, 0x4

    const-string v13, "UTF-8 not supported."

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13, v14}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 136
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 137
    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    goto/16 :goto_0

    .line 138
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_2
    move-exception v1

    .line 139
    .local v1, "e":Ljava/io/IOException;
    const/4 v12, 0x4

    const-string v13, "Error reading default override configuration file."

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13, v14}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    .line 140
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 141
    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    goto/16 :goto_0

    .line 154
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v6    # "prefName":Ljava/lang/String;
    .restart local v7    # "prefs":Landroid/content/SharedPreferences$Editor;
    .restart local v8    # "prefsProcessed":I
    .restart local v10    # "reader":Ljava/io/InputStreamReader;
    :cond_2
    # getter for: Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->PREFERENCE_TYPES_MAP:Ljava/util/HashMap;
    invoke-static {}, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->access$200()Ljava/util/HashMap;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 155
    .local v2, "expectedType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v9, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 161
    .local v11, "unparsedValue":Ljava/lang/String;
    :try_start_1
    const-class v12, Ljava/lang/Integer;

    if-ne v2, v12, :cond_3

    .line 162
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 163
    invoke-interface {v7, v6, v11}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_3

    .line 187
    :goto_2
    add-int/lit8 v8, v8, 0x1

    .line 188
    const/4 v12, 0x4

    const-string v13, "Setting override for %s to %s"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v6, v14, v15

    const/4 v15, 0x1

    aput-object v11, v14, v15

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13, v14}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 164
    :cond_3
    :try_start_2
    const-class v12, Ljava/lang/Float;

    if-ne v2, v12, :cond_4

    .line 165
    invoke-static {v11}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 166
    invoke-interface {v7, v6, v11}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_2

    .line 180
    :catch_3
    move-exception v4

    .line 181
    .local v4, "nfe":Ljava/lang/NumberFormatException;
    const/4 v12, 0x6

    const-string v13, "Unable to parse defaults value %s for preference %s. Skipping."

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v11, v14, v15

    const/4 v15, 0x1

    aput-object v6, v14, v15

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13, v14}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 167
    .end local v4    # "nfe":Ljava/lang/NumberFormatException;
    :cond_4
    :try_start_3
    const-class v12, Ljava/lang/Boolean;

    if-ne v2, v12, :cond_5

    .line 168
    invoke-static {v11}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 169
    .local v5, "parsedValue":Z
    invoke-interface {v7, v6, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_2

    .line 170
    .end local v5    # "parsedValue":Z
    :cond_5
    const-class v12, Ljava/lang/String;

    if-ne v2, v12, :cond_6

    .line 171
    invoke-interface {v7, v6, v11}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_2

    .line 173
    :cond_6
    const/4 v12, 0x6

    const-string v13, "Preference type map contained unhandled preference type %s for the preference with key %s"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    aput-object v6, v14, v15

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13, v14}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_1

    .line 192
    .end local v2    # "expectedType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v6    # "prefName":Ljava/lang/String;
    .end local v11    # "unparsedValue":Ljava/lang/String;
    :cond_7
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 195
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v7    # "prefs":Landroid/content/SharedPreferences$Editor;
    :cond_8
    if-lez v8, :cond_9

    const/4 v12, 0x1

    :goto_3
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    goto/16 :goto_0

    :cond_9
    const/4 v12, 0x0

    goto :goto_3
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 113
    check-cast p1, [Ljava/io/File;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$LoadDefaultOverridesTask;->doInBackground([Ljava/io/File;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "overridesLoaded"    # Ljava/lang/Boolean;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$LoadDefaultOverridesTask;->this$0:Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;

    # getter for: Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->mCallback:Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$OnDefaultsLoadedListener;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->access$300(Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;)Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$OnDefaultsLoadedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$LoadDefaultOverridesTask;->this$0:Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;

    # getter for: Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->mCallback:Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$OnDefaultsLoadedListener;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;->access$300(Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy;)Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$OnDefaultsLoadedListener;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$OnDefaultsLoadedListener;->onDefaultsLoaded(Z)V

    .line 204
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 205
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 113
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/PreferenceDefaultsProxy$LoadDefaultOverridesTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
