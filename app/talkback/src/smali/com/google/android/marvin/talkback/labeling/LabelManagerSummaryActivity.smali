.class public Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;
.super Landroid/app/Activity;
.source "LabelManagerSummaryActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$1;,
        Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;,
        Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;
    }
.end annotation


# instance fields
.field private mLabelProviderClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

.field private mNoPackagesMessage:Landroid/widget/TextView;

.field private mPackageLabelInfoAdapter:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;

.field private mPackageList:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 193
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;)Lcom/googlecode/eyesfree/labeling/LabelProviderClient;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->mLabelProviderClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;)Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->mPackageLabelInfoAdapter:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;)Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;
    .param p1, "x1"    # Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->mPackageLabelInfoAdapter:Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$PackageLabelInfoAdapter;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->mPackageList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->mNoPackagesMessage:Landroid/widget/TextView;

    return-object v0
.end method

.method private updatePackageSummary()V
    .locals 3

    .prologue
    .line 109
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "locale":Ljava/lang/String;
    new-instance v1, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;-><init>(Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$1;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity$UpdatePackageSummaryTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 111
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 67
    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->setContentView(I)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 71
    const v0, 0x7f0d0077

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->mPackageList:Landroid/widget/ListView;

    .line 72
    const v0, 0x7f0d0078

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->mNoPackagesMessage:Landroid/widget/TextView;

    .line 73
    new-instance v0, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    const-string v1, "com.google.android.marvin.talkback.providers.LabelProvider"

    invoke-direct {v0, p0, v1}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->mLabelProviderClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    .line 74
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 87
    iget-object v0, p0, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->mLabelProviderClient:Lcom/googlecode/eyesfree/labeling/LabelProviderClient;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/labeling/LabelProviderClient;->shutdown()V

    .line 88
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 95
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 100
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 97
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->finish()V

    .line 98
    const/4 v0, 0x1

    goto :goto_0

    .line 95
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 78
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 80
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/labeling/LabelManagerSummaryActivity;->updatePackageSummary()V

    .line 81
    return-void
.end method
