.class Lcom/google/android/marvin/talkback/RadialMenuManager;
.super Landroid/content/BroadcastReceiver;
.source "RadialMenuManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;
    }
.end annotation


# static fields
.field private static final SCALES:[I


# instance fields
.field private final mCachedRadialMenus:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;",
            ">;"
        }
    .end annotation
.end field

.field private mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

.field private final mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

.field private final mHandler:Landroid/os/Handler;

.field private final mHintSpeechCompleted:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

.field private mHintSpeechPending:Z

.field private mIsRadialMenuShowing:I

.field private final mOnClick:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private final mOnSelection:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

.field private final mOverlayListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

.field private final mRadialMenuHint:Ljava/lang/Runnable;

.field private final mScreenOffFilter:Landroid/content/IntentFilter;

.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;

.field private final mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/marvin/talkback/RadialMenuManager;->SCALES:[I

    return-void

    :array_0
    .array-data 4
        0x7f05000c
        0x7f05000d
        0x7f05000e
        0x7f05000f
        0x7f050010
        0x7f050011
        0x7f050012
        0x7f050013
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 2
    .param p1, "context"    # Lcom/google/android/marvin/talkback/TalkBackService;

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 37
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mCachedRadialMenus:Landroid/util/SparseArray;

    .line 161
    new-instance v0, Lcom/google/android/marvin/talkback/RadialMenuManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/RadialMenuManager$1;-><init>(Lcom/google/android/marvin/talkback/RadialMenuManager;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mOnSelection:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    .line 192
    new-instance v0, Lcom/google/android/marvin/talkback/RadialMenuManager$2;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/RadialMenuManager$2;-><init>(Lcom/google/android/marvin/talkback/RadialMenuManager;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mOnClick:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 217
    new-instance v0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/RadialMenuManager$3;-><init>(Lcom/google/android/marvin/talkback/RadialMenuManager;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mOverlayListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

    .line 263
    new-instance v0, Lcom/google/android/marvin/talkback/RadialMenuManager$4;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/RadialMenuManager$4;-><init>(Lcom/google/android/marvin/talkback/RadialMenuManager;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mRadialMenuHint:Ljava/lang/Runnable;

    .line 277
    new-instance v0, Lcom/google/android/marvin/talkback/RadialMenuManager$5;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/RadialMenuManager$5;-><init>(Lcom/google/android/marvin/talkback/RadialMenuManager;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mHintSpeechCompleted:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    .line 284
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mHandler:Landroid/os/Handler;

    .line 57
    iput-object p1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 58
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    .line 59
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/CachedFeedbackController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    .line 60
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mScreenOffFilter:Landroid/content/IntentFilter;

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/RadialMenuManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mRadialMenuHint:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/RadialMenuManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/CachedFeedbackController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/TalkBackService;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/SpeechController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/marvin/talkback/RadialMenuManager;Landroid/view/Menu;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/RadialMenuManager;
    .param p1, "x1"    # Landroid/view/Menu;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/RadialMenuManager;->playScaleForMenu(Landroid/view/Menu;)V

    return-void
.end method

.method static synthetic access$708(Lcom/google/android/marvin/talkback/RadialMenuManager;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    .prologue
    .line 27
    iget v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mIsRadialMenuShowing:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mIsRadialMenuShowing:I

    return v0
.end method

.method static synthetic access$710(Lcom/google/android/marvin/talkback/RadialMenuManager;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    .prologue
    .line 27
    iget v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mIsRadialMenuShowing:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mIsRadialMenuShowing:I

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/marvin/talkback/RadialMenuManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mHintSpeechPending:Z

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/marvin/talkback/RadialMenuManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/RadialMenuManager;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mHintSpeechPending:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mHintSpeechCompleted:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    return-object v0
.end method

.method private playScaleForMenu(Landroid/view/Menu;)V
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 150
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v0

    .line 151
    .local v0, "size":I
    if-gtz v0, :cond_0

    .line 156
    :goto_0
    return-void

    .line 155
    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    sget-object v2, Lcom/google/android/marvin/talkback/RadialMenuManager;->SCALES:[I

    add-int/lit8 v3, v0, -0x1

    const/4 v4, 0x7

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(I)Z

    goto :goto_0
.end method


# virtual methods
.method public clearCache()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mCachedRadialMenus:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 141
    return-void
.end method

.method public dismissAll()V
    .locals 3

    .prologue
    .line 130
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mCachedRadialMenus:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 131
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mCachedRadialMenus:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;

    .line 133
    .local v1, "menu":Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 134
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->dismiss()V

    .line 130
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    .end local v1    # "menu":Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;
    :cond_1
    return-void
.end method

.method public getFilter()Landroid/content/IntentFilter;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mScreenOffFilter:Landroid/content/IntentFilter;

    return-object v0
.end method

.method public isRadialMenuShowing()Z
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mIsRadialMenuShowing:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 255
    const-string v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/RadialMenuManager;->dismissAll()V

    .line 258
    :cond_0
    return-void
.end method

.method public setClient(Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;)V
    .locals 0
    .param p1, "client"    # Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    .line 72
    return-void
.end method

.method public showRadialMenu(I)Z
    .locals 7
    .param p1, "menuId"    # I

    .prologue
    const/4 v4, 0x0

    .line 82
    invoke-static {}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->isTutorialActive()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->shouldAllowContextMenus()Z

    move-result v5

    if-nez v5, :cond_0

    .line 118
    :goto_0
    return v4

    .line 87
    :cond_0
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mCachedRadialMenus:Landroid/util/SparseArray;

    invoke-virtual {v5, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;

    .line 89
    .local v1, "overlay":Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;
    if-nez v1, :cond_2

    .line 90
    new-instance v1, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;

    .end local v1    # "overlay":Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {v1, v5, p1, v4}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;-><init>(Landroid/content/Context;IZ)V

    .line 91
    .restart local v1    # "overlay":Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mOverlayListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

    invoke-virtual {v1, v5}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->setListener(Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;)V

    .line 93
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->getParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 94
    .local v2, "params":Landroid/view/WindowManager$LayoutParams;
    const/16 v5, 0x7da

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 95
    iget v5, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v5, v5, 0x8

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 96
    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->setParams(Landroid/view/WindowManager$LayoutParams;)V

    .line 98
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->getMenu()Lcom/googlecode/eyesfree/widget/RadialMenu;

    move-result-object v0

    .line 99
    .local v0, "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mOnSelection:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    invoke-virtual {v0, v5}, Lcom/googlecode/eyesfree/widget/RadialMenu;->setDefaultSelectionListener(Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;)V

    .line 100
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mOnClick:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-virtual {v0, v5}, Lcom/googlecode/eyesfree/widget/RadialMenu;->setDefaultListener(Landroid/view/MenuItem$OnMenuItemClickListener;)V

    .line 102
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->getView()Lcom/googlecode/eyesfree/widget/RadialMenuView;

    move-result-object v3

    .line 103
    .local v3, "view":Lcom/googlecode/eyesfree/widget/RadialMenuView;
    sget-object v5, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;->LIFT_TO_ACTIVATE:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    invoke-virtual {v3, v5}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->setSubMenuMode(Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;)V

    .line 105
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    if-eqz v5, :cond_1

    .line 106
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    invoke-interface {v5, p1, v0}, Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;->onCreateRadialMenu(ILcom/googlecode/eyesfree/widget/RadialMenu;)V

    .line 109
    :cond_1
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mCachedRadialMenus:Landroid/util/SparseArray;

    invoke-virtual {v5, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 112
    .end local v0    # "menu":Lcom/googlecode/eyesfree/widget/RadialMenu;
    .end local v2    # "params":Landroid/view/WindowManager$LayoutParams;
    .end local v3    # "view":Lcom/googlecode/eyesfree/widget/RadialMenuView;
    :cond_2
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->getMenu()Lcom/googlecode/eyesfree/widget/RadialMenu;

    move-result-object v6

    invoke-interface {v5, p1, v6}, Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;->onPrepareRadialMenu(ILcom/googlecode/eyesfree/widget/RadialMenu;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 113
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    const v6, 0x7f050003

    invoke-virtual {v5, v6}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(I)Z

    goto :goto_0

    .line 117
    :cond_3
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->showWithDot()V

    .line 118
    const/4 v4, 0x1

    goto :goto_0
.end method
