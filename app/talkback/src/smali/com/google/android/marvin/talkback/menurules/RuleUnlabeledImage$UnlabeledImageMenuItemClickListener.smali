.class Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;
.super Ljava/lang/Object;
.source "RuleUnlabeledImage.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UnlabeledImageMenuItemClickListener"
.end annotation


# instance fields
.field private final mContext:Lcom/google/android/marvin/talkback/TalkBackService;

.field private final mExistingLabel:Lcom/googlecode/eyesfree/labeling/Label;

.field private final mNode:Landroid/view/accessibility/AccessibilityNodeInfo;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/view/accessibility/AccessibilityNodeInfo;Lcom/googlecode/eyesfree/labeling/Label;)V
    .locals 0
    .param p1, "service"    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p2, "node"    # Landroid/view/accessibility/AccessibilityNodeInfo;
    .param p3, "label"    # Lcom/googlecode/eyesfree/labeling/Label;

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    iput-object p1, p0, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    .line 118
    iput-object p2, p0, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;->mNode:Landroid/view/accessibility/AccessibilityNodeInfo;

    .line 119
    iput-object p3, p0, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;->mExistingLabel:Lcom/googlecode/eyesfree/labeling/Label;

    .line 120
    return-void
.end method

.method private canAddLabel()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 153
    iget-object v3, p0, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;->mNode:Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->getViewIdResourceName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/googlecode/eyesfree/labeling/CustomLabelManager;->splitResourceName(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v2

    .line 155
    .local v2, "parsedId":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v2, :cond_1

    move v0, v4

    .line 162
    .local v0, "hasParseableId":Z
    :goto_0
    const/4 v1, 0x0

    .line 163
    .local v1, "isFromKnownApp":Z
    if-eqz v0, :cond_0

    .line 165
    :try_start_0
    iget-object v3, p0, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v6, v3, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    const/4 v1, 0x1

    .line 172
    :cond_0
    :goto_1
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    move v3, v4

    :goto_2
    return v3

    .end local v0    # "hasParseableId":Z
    .end local v1    # "isFromKnownApp":Z
    :cond_1
    move v0, v5

    .line 155
    goto :goto_0

    .restart local v0    # "hasParseableId":Z
    .restart local v1    # "isFromKnownApp":Z
    :cond_2
    move v3, v5

    .line 172
    goto :goto_2

    .line 167
    :catch_0
    move-exception v3

    goto :goto_1
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 124
    if-nez p1, :cond_0

    .line 125
    iget-object v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;->mNode:Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->recycle()V

    .line 149
    :goto_0
    return v1

    .line 129
    :cond_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->saveFocusedNode()V

    .line 130
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 132
    .local v0, "itemId":I
    const v2, 0x7f0d000c

    if-ne v0, v2, :cond_2

    .line 133
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;->canAddLabel()Z

    move-result v1

    if-nez v1, :cond_1

    .line 134
    iget-object v1, p0, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f060177

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    .line 138
    const/4 v1, 0x0

    goto :goto_0

    .line 141
    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;->mNode:Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-static {v1, v2}, Lcom/googlecode/eyesfree/labeling/LabelOperationUtils;->startActivityAddLabelForNode(Landroid/content/Context;Landroid/view/accessibility/AccessibilityNodeInfo;)Z

    move-result v1

    goto :goto_0

    .line 142
    :cond_2
    const v2, 0x7f0d000d

    if-ne v0, v2, :cond_3

    .line 143
    iget-object v1, p0, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;->mExistingLabel:Lcom/googlecode/eyesfree/labeling/Label;

    invoke-static {v1, v2}, Lcom/googlecode/eyesfree/labeling/LabelOperationUtils;->startActivityEditLabel(Landroid/content/Context;Lcom/googlecode/eyesfree/labeling/Label;)Z

    move-result v1

    goto :goto_0

    .line 144
    :cond_3
    const v2, 0x7f0d000e

    if-ne v0, v2, :cond_4

    .line 145
    iget-object v1, p0, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;->mExistingLabel:Lcom/googlecode/eyesfree/labeling/Label;

    invoke-static {v1, v2}, Lcom/googlecode/eyesfree/labeling/LabelOperationUtils;->startActivityRemoveLabel(Landroid/content/Context;Lcom/googlecode/eyesfree/labeling/Label;)Z

    move-result v1

    goto :goto_0

    .line 148
    :cond_4
    iget-object v2, p0, Lcom/google/android/marvin/talkback/menurules/RuleUnlabeledImage$UnlabeledImageMenuItemClickListener;->mNode:Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->recycle()V

    goto :goto_0
.end method
