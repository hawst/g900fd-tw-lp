.class Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;
.super Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;
.source "ProcessorFocusAndSingleTap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FollowFocusHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/utils/WeakReferenceHandler",
        "<",
        "Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;",
        ">;"
    }
.end annotation


# instance fields
.field private mCachedContentRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

.field private mCachedFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private mCachedTouchedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;)V
    .locals 1
    .param p1, "parent"    # Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;

    .prologue
    .line 716
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;-><init>(Ljava/lang/Object;)V

    .line 712
    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->getInstance()Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/CachedFeedbackController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    .line 717
    return-void
.end method


# virtual methods
.method public cancelClearScrollAction()V
    .locals 1

    .prologue
    .line 880
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->removeMessages(I)V

    .line 881
    return-void
.end method

.method public cancelEmptyTouchAreaFeedback()V
    .locals 1

    .prologue
    .line 888
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->removeMessages(I)V

    .line 890
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedTouchedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v0, :cond_0

    .line 891
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedTouchedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 892
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedTouchedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 894
    :cond_0
    return-void
.end method

.method public cancelRefocusTimeout(Z)V
    .locals 2
    .param p1, "shouldRefocus"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 851
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->removeMessages(I)V

    .line 853
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->getParent()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;

    .line 854
    .local v0, "parent":Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;
    if-nez v0, :cond_1

    .line 866
    :cond_0
    :goto_0
    return-void

    .line 858
    :cond_1
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v1, :cond_2

    .line 859
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    # invokes: Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->attemptRefocusNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    invoke-static {v0, v1}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->access$500(Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    .line 862
    :cond_2
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v1, :cond_0

    .line 863
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 864
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    goto :goto_0
.end method

.method public clearScrollActionDelayed()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 839
    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->removeMessages(I)V

    .line 840
    const-wide/16 v0, 0xc8

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 841
    return-void
.end method

.method public followContentChangedDelayed(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)V
    .locals 4
    .param p1, "record"    # Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    .prologue
    const/4 v2, 0x2

    .line 766
    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->removeMessages(I)V

    .line 768
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedContentRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    if-eqz v1, :cond_0

    .line 769
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedContentRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->recycle()V

    .line 770
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedContentRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    .line 773
    :cond_0
    invoke-static {p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedContentRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    .line 775
    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 776
    .local v0, "msg":Landroid/os/Message;
    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 777
    return-void
.end method

.method public followScrollDelayed(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ZZ)V
    .locals 6
    .param p1, "source"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2, "isMovingForward"    # Z
    .param p3, "wasScrollAction"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 785
    invoke-virtual {p0, v3}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->removeMessages(I)V

    .line 787
    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v4, :cond_0

    .line 788
    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 789
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 792
    :cond_0
    invoke-static {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 794
    if-eqz p2, :cond_2

    move v1, v3

    .line 795
    .local v1, "wrapIsMovingForward":I
    :goto_0
    if-eqz p3, :cond_1

    move v2, v3

    .line 797
    .local v2, "wrapWasScrollAction":I
    :cond_1
    invoke-virtual {p0, v3, v1, v2}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 799
    .local v0, "msg":Landroid/os/Message;
    const-wide/16 v4, 0xfa

    invoke-virtual {p0, v0, v4, v5}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 800
    return-void

    .end local v0    # "msg":Landroid/os/Message;
    .end local v1    # "wrapIsMovingForward":I
    .end local v2    # "wrapWasScrollAction":I
    :cond_2
    move v1, v2

    .line 794
    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;
    .param p2, "parent"    # Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 721
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 757
    :cond_0
    :goto_0
    return-void

    .line 723
    :pswitch_0
    iget v4, p1, Landroid/os/Message;->arg1:I

    if-ne v4, v2, :cond_1

    move v0, v2

    .line 724
    .local v0, "isMovingForward":Z
    :goto_1
    iget v4, p1, Landroid/os/Message;->arg2:I

    if-ne v4, v2, :cond_2

    move v1, v2

    .line 726
    .local v1, "wasScrollAction":Z
    :goto_2
    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    # invokes: Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->followScrollEvent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ZZ)Z
    invoke-static {p2, v2, v0, v1}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->access$000(Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ZZ)Z

    .line 728
    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v2, :cond_0

    .line 729
    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 730
    iput-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    goto :goto_0

    .end local v0    # "isMovingForward":Z
    .end local v1    # "wasScrollAction":Z
    :cond_1
    move v0, v3

    .line 723
    goto :goto_1

    .restart local v0    # "isMovingForward":Z
    :cond_2
    move v1, v3

    .line 724
    goto :goto_2

    .line 734
    .end local v0    # "isMovingForward":Z
    :pswitch_1
    # invokes: Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->followContentChangedEvent()Z
    invoke-static {p2}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->access$100(Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;)Z

    .line 736
    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedContentRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    if-eqz v2, :cond_0

    .line 737
    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedContentRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->recycle()V

    .line 738
    iput-object v5, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedContentRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    goto :goto_0

    .line 742
    :pswitch_2
    # invokes: Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->cancelSingleTap()V
    invoke-static {p2}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->access$200(Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;)V

    .line 743
    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->cancelRefocusTimeout(Z)V

    goto :goto_0

    .line 746
    :pswitch_3
    # invokes: Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->setScrollActionImmediately(I)V
    invoke-static {p2, v3}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->access$300(Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;I)V

    goto :goto_0

    .line 749
    :pswitch_4
    # getter for: Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->mService:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {p2}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->access$400(Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedTouchedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-static {v2, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isSelfOrAncestorFocused(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 751
    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    const v3, 0x7f0a0018

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playHaptic(I)Z

    .line 752
    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mFeedbackController:Lcom/google/android/marvin/talkback/CachedFeedbackController;

    const v3, 0x7f050022

    const v4, 0x3fa66666    # 1.3f

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/marvin/talkback/CachedFeedbackController;->playAuditory(IFFF)Z

    goto :goto_0

    .line 721
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic handleMessage(Landroid/os/Message;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Message;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 689
    check-cast p2, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;)V

    return-void
.end method

.method public interruptFollowDelayed()V
    .locals 1

    .prologue
    .line 872
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->removeMessages(I)V

    .line 873
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->removeMessages(I)V

    .line 874
    return-void
.end method

.method public refocusAfterTimeout(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 4
    .param p1, "source"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v2, 0x3

    .line 810
    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->removeMessages(I)V

    .line 812
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v1, :cond_0

    .line 813
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 814
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 817
    :cond_0
    invoke-static {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 819
    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 820
    .local v0, "msg":Landroid/os/Message;
    sget-wide v2, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap;->TAP_TIMEOUT:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 821
    return-void
.end method

.method public sendEmptyTouchAreaFeedbackDelayed(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 4
    .param p1, "touchedNode"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    .line 828
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->cancelEmptyTouchAreaFeedback()V

    .line 829
    invoke-static {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->mCachedTouchedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 831
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 832
    .local v0, "msg":Landroid/os/Message;
    const-wide/16 v2, 0x64

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/marvin/talkback/ProcessorFocusAndSingleTap$FollowFocusHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 833
    return-void
.end method
