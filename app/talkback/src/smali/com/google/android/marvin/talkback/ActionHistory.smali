.class public Lcom/google/android/marvin/talkback/ActionHistory;
.super Ljava/lang/Object;
.source "ActionHistory.java"


# static fields
.field private static sInstance:Lcom/google/android/marvin/talkback/ActionHistory;


# instance fields
.field private final mActionFinishMap:Lcom/googlecode/eyesfree/compat/util/SparseLongArray;

.field private final mActionStartMap:Lcom/googlecode/eyesfree/compat/util/SparseLongArray;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ActionHistory;->mActionStartMap:Lcom/googlecode/eyesfree/compat/util/SparseLongArray;

    .line 45
    new-instance v0, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ActionHistory;->mActionFinishMap:Lcom/googlecode/eyesfree/compat/util/SparseLongArray;

    .line 49
    return-void
.end method

.method public static getInstance()Lcom/google/android/marvin/talkback/ActionHistory;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/marvin/talkback/ActionHistory;->sInstance:Lcom/google/android/marvin/talkback/ActionHistory;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/google/android/marvin/talkback/ActionHistory;

    invoke-direct {v0}, Lcom/google/android/marvin/talkback/ActionHistory;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/ActionHistory;->sInstance:Lcom/google/android/marvin/talkback/ActionHistory;

    .line 38
    :cond_0
    sget-object v0, Lcom/google/android/marvin/talkback/ActionHistory;->sInstance:Lcom/google/android/marvin/talkback/ActionHistory;

    return-object v0
.end method


# virtual methods
.method public after(I)V
    .locals 4
    .param p1, "action"    # I

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ActionHistory;->mActionFinishMap:Lcom/googlecode/eyesfree/compat/util/SparseLongArray;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->put(IJ)V

    .line 69
    return-void
.end method

.method public before(I)V
    .locals 4
    .param p1, "action"    # I

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ActionHistory;->mActionStartMap:Lcom/googlecode/eyesfree/compat/util/SparseLongArray;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->put(IJ)V

    .line 59
    return-void
.end method

.method public hasActionAtTime(IJ)Z
    .locals 10
    .param p1, "action"    # I
    .param p2, "eventTime"    # J

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 81
    iget-object v8, p0, Lcom/google/android/marvin/talkback/ActionHistory;->mActionStartMap:Lcom/googlecode/eyesfree/compat/util/SparseLongArray;

    invoke-virtual {v8, p1}, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->get(I)J

    move-result-wide v4

    .line 82
    .local v4, "startTime":J
    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-lez v8, :cond_1

    move v3, v7

    .line 83
    .local v3, "hasStarted":Z
    :goto_0
    if-eqz v3, :cond_0

    cmp-long v8, v4, p2

    if-lez v8, :cond_2

    .line 95
    :cond_0
    :goto_1
    return v6

    .end local v3    # "hasStarted":Z
    :cond_1
    move v3, v6

    .line 82
    goto :goto_0

    .line 88
    .restart local v3    # "hasStarted":Z
    :cond_2
    iget-object v8, p0, Lcom/google/android/marvin/talkback/ActionHistory;->mActionFinishMap:Lcom/googlecode/eyesfree/compat/util/SparseLongArray;

    invoke-virtual {v8, p1}, Lcom/googlecode/eyesfree/compat/util/SparseLongArray;->get(I)J

    move-result-wide v0

    .line 89
    .local v0, "finishTime":J
    cmp-long v8, v0, v4

    if-ltz v8, :cond_4

    move v2, v7

    .line 90
    .local v2, "hasFinished":Z
    :goto_2
    if-eqz v2, :cond_3

    cmp-long v8, v0, p2

    if-ltz v8, :cond_0

    :cond_3
    move v6, v7

    .line 95
    goto :goto_1

    .end local v2    # "hasFinished":Z
    :cond_4
    move v2, v6

    .line 89
    goto :goto_2
.end method
