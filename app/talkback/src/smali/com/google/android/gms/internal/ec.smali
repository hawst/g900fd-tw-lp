.class public final Lcom/google/android/gms/internal/ec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/ed;


# instance fields
.field public rq:Ljava/lang/String;

.field public rr:I

.field public rs:I

.field public rt:Z

.field public final versionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/ed;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ed;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ec;->CREATOR:Lcom/google/android/gms/internal/ed;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;IIZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/ec;->versionCode:I

    iput-object p2, p0, Lcom/google/android/gms/internal/ec;->rq:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/gms/internal/ec;->rr:I

    iput p4, p0, Lcom/google/android/gms/internal/ec;->rs:I

    iput-boolean p5, p0, Lcom/google/android/gms/internal/ec;->rt:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/ed;->a(Lcom/google/android/gms/internal/ec;Landroid/os/Parcel;I)V

    return-void
.end method
