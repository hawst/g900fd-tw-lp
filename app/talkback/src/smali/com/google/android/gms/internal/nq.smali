.class public final Lcom/google/android/gms/internal/nq;
.super Ljava/lang/Object;


# static fields
.field public static final aoU:[I

.field public static final aoV:[J

.field public static final aoW:[F

.field public static final aoX:[D

.field public static final aoY:[Z

.field public static final aoZ:[Ljava/lang/String;

.field public static final apa:[[B

.field public static final apb:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-array v0, v1, [I

    sput-object v0, Lcom/google/android/gms/internal/nq;->aoU:[I

    new-array v0, v1, [J

    sput-object v0, Lcom/google/android/gms/internal/nq;->aoV:[J

    new-array v0, v1, [F

    sput-object v0, Lcom/google/android/gms/internal/nq;->aoW:[F

    new-array v0, v1, [D

    sput-object v0, Lcom/google/android/gms/internal/nq;->aoX:[D

    new-array v0, v1, [Z

    sput-object v0, Lcom/google/android/gms/internal/nq;->aoY:[Z

    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/internal/nq;->aoZ:[Ljava/lang/String;

    new-array v0, v1, [[B

    sput-object v0, Lcom/google/android/gms/internal/nq;->apa:[[B

    new-array v0, v1, [B

    sput-object v0, Lcom/google/android/gms/internal/nq;->apb:[B

    return-void
.end method

.method static q(II)I
    .locals 1

    shl-int/lit8 v0, p0, 0x3

    or-int/2addr v0, p1

    return v0
.end method
