.class public final Lcom/google/android/gms/internal/dc;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/dd;


# instance fields
.field public final applicationInfo:Landroid/content/pm/ApplicationInfo;

.field public final kK:Ljava/lang/String;

.field public final kN:Lcom/google/android/gms/internal/ec;

.field public final kQ:Lcom/google/android/gms/internal/ap;

.field public final pg:Landroid/os/Bundle;

.field public final ph:Lcom/google/android/gms/internal/am;

.field public final pi:Landroid/content/pm/PackageInfo;

.field public final pj:Ljava/lang/String;

.field public final pk:Ljava/lang/String;

.field public final pl:Ljava/lang/String;

.field public final pm:Landroid/os/Bundle;

.field public final versionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/dd;

    invoke-direct {v0}, Lcom/google/android/gms/internal/dd;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/dc;->CREATOR:Lcom/google/android/gms/internal/dd;

    return-void
.end method

.method constructor <init>(ILandroid/os/Bundle;Lcom/google/android/gms/internal/am;Lcom/google/android/gms/internal/ap;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Landroid/content/pm/PackageInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/ec;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/dc;->versionCode:I

    iput-object p2, p0, Lcom/google/android/gms/internal/dc;->pg:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/google/android/gms/internal/dc;->ph:Lcom/google/android/gms/internal/am;

    iput-object p4, p0, Lcom/google/android/gms/internal/dc;->kQ:Lcom/google/android/gms/internal/ap;

    iput-object p5, p0, Lcom/google/android/gms/internal/dc;->kK:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/internal/dc;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iput-object p7, p0, Lcom/google/android/gms/internal/dc;->pi:Landroid/content/pm/PackageInfo;

    iput-object p8, p0, Lcom/google/android/gms/internal/dc;->pj:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/gms/internal/dc;->pk:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/gms/internal/dc;->pl:Ljava/lang/String;

    iput-object p11, p0, Lcom/google/android/gms/internal/dc;->kN:Lcom/google/android/gms/internal/ec;

    iput-object p12, p0, Lcom/google/android/gms/internal/dc;->pm:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/dd;->a(Lcom/google/android/gms/internal/dc;Landroid/os/Parcel;I)V

    return-void
.end method
