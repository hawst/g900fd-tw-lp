.class public Lcom/google/android/gms/internal/lo$a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/lo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/lp;


# instance fields
.field public afy:J

.field public afz:J

.field public blocked:Z

.field public packageName:Ljava/lang/String;

.field final wv:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/lp;

    invoke-direct {v0}, Lcom/google/android/gms/internal/lp;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/lo$a;->CREATOR:Lcom/google/android/gms/internal/lp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/lo$a;->wv:I

    return-void
.end method

.method constructor <init>(ILjava/lang/String;JZJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/lo$a;->wv:I

    iput-object p2, p0, Lcom/google/android/gms/internal/lo$a;->packageName:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/gms/internal/lo$a;->afy:J

    iput-boolean p5, p0, Lcom/google/android/gms/internal/lo$a;->blocked:Z

    iput-wide p6, p0, Lcom/google/android/gms/internal/lo$a;->afz:J

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/lo$a;->CREATOR:Lcom/google/android/gms/internal/lp;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    sget-object v0, Lcom/google/android/gms/internal/lo$a;->CREATOR:Lcom/google/android/gms/internal/lp;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/lp;->a(Lcom/google/android/gms/internal/lo$a;Landroid/os/Parcel;I)V

    return-void
.end method
