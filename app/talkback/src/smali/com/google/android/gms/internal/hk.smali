.class public Lcom/google/android/gms/internal/hk;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/hk$e;,
        Lcom/google/android/gms/internal/hk$c;,
        Lcom/google/android/gms/internal/hk$d;,
        Lcom/google/android/gms/internal/hk$b;,
        Lcom/google/android/gms/internal/hk$a;
    }
.end annotation


# static fields
.field public static final KT:Lcom/google/android/gms/internal/hk$a;

.field public static final KU:Lcom/google/android/gms/internal/hk$b;

.field public static final KV:Lcom/google/android/gms/internal/hk$d;

.field public static final KW:Lcom/google/android/gms/internal/hk$c;

.field public static final KX:Lcom/google/android/gms/internal/hk$e;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const v3, 0x3e8fa0

    new-instance v0, Lcom/google/android/gms/internal/hk$a;

    const-string v1, "created"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/hk$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hk;->KT:Lcom/google/android/gms/internal/hk$a;

    new-instance v0, Lcom/google/android/gms/internal/hk$b;

    const-string v1, "lastOpenedTime"

    const v2, 0x419ce0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/hk$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hk;->KU:Lcom/google/android/gms/internal/hk$b;

    new-instance v0, Lcom/google/android/gms/internal/hk$d;

    const-string v1, "modified"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/hk$d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hk;->KV:Lcom/google/android/gms/internal/hk$d;

    new-instance v0, Lcom/google/android/gms/internal/hk$c;

    const-string v1, "modifiedByMe"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/hk$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hk;->KW:Lcom/google/android/gms/internal/hk$c;

    new-instance v0, Lcom/google/android/gms/internal/hk$e;

    const-string v1, "sharedWithMe"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/hk$e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hk;->KX:Lcom/google/android/gms/internal/hk$e;

    return-void
.end method
