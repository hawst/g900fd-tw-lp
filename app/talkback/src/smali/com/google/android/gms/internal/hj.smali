.class public Lcom/google/android/gms/internal/hj;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/hj$d;,
        Lcom/google/android/gms/internal/hj$a;,
        Lcom/google/android/gms/internal/hj$e;,
        Lcom/google/android/gms/internal/hj$c;,
        Lcom/google/android/gms/internal/hj$g;,
        Lcom/google/android/gms/internal/hj$b;,
        Lcom/google/android/gms/internal/hj$f;
    }
.end annotation


# static fields
.field public static final KA:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final KB:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final KC:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final KD:Lcom/google/android/gms/internal/hj$a;

.field public static final KE:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final KF:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final KG:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final KH:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final KI:Lcom/google/android/gms/internal/hj$b;

.field public static final KJ:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final KK:Lcom/google/android/gms/drive/metadata/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/b",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final KL:Lcom/google/android/gms/internal/hj$c;

.field public static final KM:Lcom/google/android/gms/internal/hj$d;

.field public static final KN:Lcom/google/android/gms/internal/hj$e;

.field public static final KO:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public static final KP:Lcom/google/android/gms/internal/hj$f;

.field public static final KQ:Lcom/google/android/gms/internal/hj$g;

.field public static final KR:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final KS:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final Ks:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Lcom/google/android/gms/drive/DriveId;",
            ">;"
        }
    .end annotation
.end field

.field public static final Kt:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final Ku:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final Kv:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final Kw:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final Kx:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final Ky:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final Kz:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const v4, 0x432380

    const v3, 0x3e8fa0

    const v2, 0x419ce0

    sget-object v0, Lcom/google/android/gms/internal/hl;->KY:Lcom/google/android/gms/internal/hl;

    sput-object v0, Lcom/google/android/gms/internal/hj;->Ks:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/j;

    const-string v1, "alternateLink"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->Kt:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/j;

    const-string v1, "description"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->Ku:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/j;

    const-string v1, "embedLink"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->Kv:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/j;

    const-string v1, "fileExtension"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->Kw:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/e;

    const-string v1, "fileSize"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->Kx:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a;

    const-string v1, "hasThumbnail"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->Ky:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/j;

    const-string v1, "indexableText"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->Kz:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a;

    const-string v1, "isAppData"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KA:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a;

    const-string v1, "isCopyable"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KB:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a;

    const-string v1, "isEditable"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/drive/metadata/internal/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KC:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/internal/hj$a;

    const-string v1, "isPinned"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/hj$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KD:Lcom/google/android/gms/internal/hj$a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a;

    const-string v1, "isRestricted"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KE:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a;

    const-string v1, "isShared"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KF:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a;

    const-string v1, "isTrashable"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KG:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a;

    const-string v1, "isViewed"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KH:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/internal/hj$b;

    const-string v1, "mimeType"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/hj$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KI:Lcom/google/android/gms/internal/hj$b;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/j;

    const-string v1, "originalFilename"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KJ:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/i;

    const-string v1, "ownerNames"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KK:Lcom/google/android/gms/drive/metadata/b;

    new-instance v0, Lcom/google/android/gms/internal/hj$c;

    const-string v1, "parents"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/hj$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KL:Lcom/google/android/gms/internal/hj$c;

    new-instance v0, Lcom/google/android/gms/internal/hj$d;

    const-string v1, "quotaBytesUsed"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/hj$d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KM:Lcom/google/android/gms/internal/hj$d;

    new-instance v0, Lcom/google/android/gms/internal/hj$e;

    const-string v1, "starred"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/hj$e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KN:Lcom/google/android/gms/internal/hj$e;

    new-instance v0, Lcom/google/android/gms/internal/hj$1;

    const-string v1, "thumbnail"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/internal/hj$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KO:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/internal/hj$f;

    const-string v1, "title"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/hj$f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KP:Lcom/google/android/gms/internal/hj$f;

    new-instance v0, Lcom/google/android/gms/internal/hj$g;

    const-string v1, "trashed"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/hj$g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KQ:Lcom/google/android/gms/internal/hj$g;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/j;

    const-string v1, "webContentLink"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KR:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/j;

    const-string v1, "webViewLink"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/hj;->KS:Lcom/google/android/gms/drive/metadata/MetadataField;

    return-void
.end method
