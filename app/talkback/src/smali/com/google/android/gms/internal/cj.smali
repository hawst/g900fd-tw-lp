.class public final Lcom/google/android/gms/internal/cj;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/ci;


# instance fields
.field public final kN:Lcom/google/android/gms/internal/ec;

.field public final oh:Lcom/google/android/gms/internal/cg;

.field public final oi:Lcom/google/android/gms/internal/z;

.field public final oj:Lcom/google/android/gms/internal/ck;

.field public final ok:Lcom/google/android/gms/internal/ee;

.field public final ol:Lcom/google/android/gms/internal/be;

.field public final om:Ljava/lang/String;

.field public final on:Z

.field public final oo:Ljava/lang/String;

.field public final op:Lcom/google/android/gms/internal/cn;

.field public final oq:I

.field public final or:Lcom/google/android/gms/internal/bh;

.field public final orientation:I

.field public final os:Ljava/lang/String;

.field public final url:Ljava/lang/String;

.field public final versionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/ci;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ci;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/cj;->CREATOR:Lcom/google/android/gms/internal/ci;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/internal/cg;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Ljava/lang/String;ZLjava/lang/String;Landroid/os/IBinder;IILjava/lang/String;Lcom/google/android/gms/internal/ec;Landroid/os/IBinder;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/cj;->versionCode:I

    iput-object p2, p0, Lcom/google/android/gms/internal/cj;->oh:Lcom/google/android/gms/internal/cg;

    invoke-static {p3}, Lcom/google/android/gms/dynamic/d$a;->ag(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/d;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/dynamic/e;->g(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/z;

    iput-object v1, p0, Lcom/google/android/gms/internal/cj;->oi:Lcom/google/android/gms/internal/z;

    invoke-static {p4}, Lcom/google/android/gms/dynamic/d$a;->ag(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/d;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/dynamic/e;->g(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/ck;

    iput-object v1, p0, Lcom/google/android/gms/internal/cj;->oj:Lcom/google/android/gms/internal/ck;

    invoke-static {p5}, Lcom/google/android/gms/dynamic/d$a;->ag(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/d;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/dynamic/e;->g(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/ee;

    iput-object v1, p0, Lcom/google/android/gms/internal/cj;->ok:Lcom/google/android/gms/internal/ee;

    invoke-static {p6}, Lcom/google/android/gms/dynamic/d$a;->ag(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/d;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/dynamic/e;->g(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/be;

    iput-object v1, p0, Lcom/google/android/gms/internal/cj;->ol:Lcom/google/android/gms/internal/be;

    iput-object p7, p0, Lcom/google/android/gms/internal/cj;->om:Ljava/lang/String;

    iput-boolean p8, p0, Lcom/google/android/gms/internal/cj;->on:Z

    iput-object p9, p0, Lcom/google/android/gms/internal/cj;->oo:Ljava/lang/String;

    invoke-static {p10}, Lcom/google/android/gms/dynamic/d$a;->ag(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/d;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/dynamic/e;->g(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/cn;

    iput-object v1, p0, Lcom/google/android/gms/internal/cj;->op:Lcom/google/android/gms/internal/cn;

    iput p11, p0, Lcom/google/android/gms/internal/cj;->orientation:I

    iput p12, p0, Lcom/google/android/gms/internal/cj;->oq:I

    iput-object p13, p0, Lcom/google/android/gms/internal/cj;->url:Ljava/lang/String;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/gms/internal/cj;->kN:Lcom/google/android/gms/internal/ec;

    invoke-static/range {p15 .. p15}, Lcom/google/android/gms/dynamic/d$a;->ag(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/d;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/dynamic/e;->g(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/bh;

    iput-object v1, p0, Lcom/google/android/gms/internal/cj;->or:Lcom/google/android/gms/internal/bh;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/gms/internal/cj;->os:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method aN()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/cj;->oi:Lcom/google/android/gms/internal/z;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->g(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method aO()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/cj;->oj:Lcom/google/android/gms/internal/ck;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->g(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method aP()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/cj;->ok:Lcom/google/android/gms/internal/ee;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->g(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method aQ()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/cj;->ol:Lcom/google/android/gms/internal/be;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->g(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method aR()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/cj;->or:Lcom/google/android/gms/internal/bh;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->g(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method aS()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/cj;->op:Lcom/google/android/gms/internal/cn;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->g(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/ci;->a(Lcom/google/android/gms/internal/cj;Landroid/os/Parcel;I)V

    return-void
.end method
