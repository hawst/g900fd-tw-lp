.class public Lcom/google/android/gms/internal/lo$c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/lo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/lr;


# instance fields
.field public Ut:Lcom/google/android/gms/common/api/Status;

.field public afA:[Lcom/google/android/gms/internal/lo$a;

.field public afB:J

.field public afC:J

.field public afD:J

.field final wv:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/lr;

    invoke-direct {v0}, Lcom/google/android/gms/internal/lr;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/lo$c;->CREATOR:Lcom/google/android/gms/internal/lr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/lo$c;->wv:I

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/api/Status;[Lcom/google/android/gms/internal/lo$a;JJJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/lo$c;->wv:I

    iput-object p2, p0, Lcom/google/android/gms/internal/lo$c;->Ut:Lcom/google/android/gms/common/api/Status;

    iput-object p3, p0, Lcom/google/android/gms/internal/lo$c;->afA:[Lcom/google/android/gms/internal/lo$a;

    iput-wide p4, p0, Lcom/google/android/gms/internal/lo$c;->afB:J

    iput-wide p6, p0, Lcom/google/android/gms/internal/lo$c;->afC:J

    iput-wide p8, p0, Lcom/google/android/gms/internal/lo$c;->afD:J

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/lo$c;->CREATOR:Lcom/google/android/gms/internal/lr;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    sget-object v0, Lcom/google/android/gms/internal/lo$c;->CREATOR:Lcom/google/android/gms/internal/lr;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/lr;->a(Lcom/google/android/gms/internal/lo$c;Landroid/os/Parcel;I)V

    return-void
.end method
