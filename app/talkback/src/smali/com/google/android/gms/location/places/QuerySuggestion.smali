.class public Lcom/google/android/gms/location/places/QuerySuggestion;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/location/places/QuerySuggestion$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/location/places/QuerySuggestion;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final Mt:Ljava/lang/String;

.field final UH:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final UI:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/location/places/QuerySuggestion$a;",
            ">;"
        }
    .end annotation
.end field

.field final Um:Ljava/lang/String;

.field final wv:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/location/places/g;

    invoke-direct {v0}, Lcom/google/android/gms/location/places/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/places/QuerySuggestion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p1, "versionCode"    # I
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "placeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/location/places/QuerySuggestion$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "placeTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p5, "substrings":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/location/places/QuerySuggestion$a;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/location/places/QuerySuggestion;->wv:I

    iput-object p2, p0, Lcom/google/android/gms/location/places/QuerySuggestion;->Mt:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/location/places/QuerySuggestion;->Um:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/location/places/QuerySuggestion;->UH:Ljava/util/List;

    iput-object p5, p0, Lcom/google/android/gms/location/places/QuerySuggestion;->UI:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/google/android/gms/internal/gf;->e(Ljava/lang/Object;)Lcom/google/android/gms/internal/gf$a;

    move-result-object v0

    const-string v1, "mDescription"

    iget-object v2, p0, Lcom/google/android/gms/location/places/QuerySuggestion;->Mt:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/gf$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/gf$a;

    move-result-object v0

    const-string v1, "mPlaceId"

    iget-object v2, p0, Lcom/google/android/gms/location/places/QuerySuggestion;->Um:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/gf$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/gf$a;

    move-result-object v0

    const-string v1, "mPlaceTypes"

    iget-object v2, p0, Lcom/google/android/gms/location/places/QuerySuggestion;->UH:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/gf$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/gf$a;

    move-result-object v0

    const-string v1, "mSubstrings"

    iget-object v2, p0, Lcom/google/android/gms/location/places/QuerySuggestion;->UI:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/gf$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/gf$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gf$a;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/location/places/g;->a(Lcom/google/android/gms/location/places/QuerySuggestion;Landroid/os/Parcel;I)V

    return-void
.end method
