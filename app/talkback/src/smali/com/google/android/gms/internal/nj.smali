.class public abstract Lcom/google/android/gms/internal/nj;
.super Lcom/google/android/gms/internal/nn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Lcom/google/android/gms/internal/nj",
        "<TM;>;>",
        "Lcom/google/android/gms/internal/nn;"
    }
.end annotation


# instance fields
.field protected aoO:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/internal/np;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/nn;-><init>()V

    return-void
.end method


# virtual methods
.method protected nh()I
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/internal/nj;->aoO:Ljava/util/List;

    if-nez v1, :cond_0

    move v1, v0

    :goto_0
    move v2, v0

    move v3, v0

    :goto_1
    if-ge v2, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/nj;->aoO:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/np;

    iget v4, v0, Lcom/google/android/gms/internal/np;->tag:I

    invoke-static {v4}, Lcom/google/android/gms/internal/ni;->fS(I)I

    move-result v4

    add-int/2addr v3, v4

    iget-object v0, v0, Lcom/google/android/gms/internal/np;->aoT:[B

    array-length v0, v0

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/nj;->aoO:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_1
    return v3
.end method

.method public writeTo(Lcom/google/android/gms/internal/ni;)V
    .locals 4
    .param p1, "output"    # Lcom/google/android/gms/internal/ni;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/gms/internal/nj;, "Lcom/google/android/gms/internal/nj<TM;>;"
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/internal/nj;->aoO:Ljava/util/List;

    if-nez v1, :cond_0

    move v1, v0

    :goto_0
    move v2, v0

    :goto_1
    if-ge v2, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/nj;->aoO:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/np;

    iget v3, v0, Lcom/google/android/gms/internal/np;->tag:I

    invoke-virtual {p1, v3}, Lcom/google/android/gms/internal/ni;->fR(I)V

    iget-object v0, v0, Lcom/google/android/gms/internal/np;->aoT:[B

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/ni;->y([B)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/nj;->aoO:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_1
    return-void
.end method
