.class public final Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field account:Landroid/accounts/Account;

.field akF:Ljava/lang/String;

.field akG:I

.field akH:Z

.field akI:Z

.field akJ:Z

.field akK:Z

.field akL:[Lcom/google/android/gms/wallet/CountrySpecification;

.field akM:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/identity/intents/model/CountrySpecification;",
            ">;"
        }
    .end annotation
.end field

.field environment:I

.field pl:Ljava/lang/String;

.field private final wv:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/wallet/h;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->wv:I

    return-void
.end method

.method constructor <init>(IILandroid/accounts/Account;Ljava/lang/String;IZZZLjava/lang/String;Z[Lcom/google/android/gms/wallet/CountrySpecification;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "versionCode"    # I
    .param p2, "environment"    # I
    .param p3, "account"    # Landroid/accounts/Account;
    .param p4, "merchantDomain"    # Ljava/lang/String;
    .param p5, "feature"    # I
    .param p6, "phoneNumberRequired"    # Z
    .param p7, "shippingAddressRequired"    # Z
    .param p8, "useMinimalBillingAddress"    # Z
    .param p9, "sessionId"    # Ljava/lang/String;
    .param p10, "shouldAllowSaveToChromeOption"    # Z
    .param p11, "allowedShippingCountrySpecifications"    # [Lcom/google/android/gms/wallet/CountrySpecification;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            "IZZZ",
            "Ljava/lang/String;",
            "Z[",
            "Lcom/google/android/gms/wallet/CountrySpecification;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/identity/intents/model/CountrySpecification;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p12, "allowedCountrySpecificationsForShipping":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/gms/identity/intents/model/CountrySpecification;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->wv:I

    iput p2, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->environment:I

    iput-object p3, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->account:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->akF:Ljava/lang/String;

    iput p5, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->akG:I

    iput-boolean p6, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->akH:Z

    iput-boolean p7, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->akI:Z

    iput-boolean p8, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->akJ:Z

    iput-object p9, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->pl:Ljava/lang/String;

    iput-boolean p10, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->akK:Z

    iput-object p11, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->akL:[Lcom/google/android/gms/wallet/CountrySpecification;

    iput-object p12, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->akM:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->wv:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/wallet/h;->a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;Landroid/os/Parcel;I)V

    return-void
.end method
