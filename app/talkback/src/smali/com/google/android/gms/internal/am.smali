.class public final Lcom/google/android/gms/internal/am;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/an;


# instance fields
.field public final extras:Landroid/os/Bundle;

.field public final lK:J

.field public final lL:I

.field public final lM:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final lN:Z

.field public final lO:I

.field public final lP:Z

.field public final lQ:Ljava/lang/String;

.field public final lR:Lcom/google/android/gms/internal/ba;

.field public final lS:Landroid/location/Location;

.field public final lT:Ljava/lang/String;

.field public final versionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/an;

    invoke-direct {v0}, Lcom/google/android/gms/internal/an;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/am;->CREATOR:Lcom/google/android/gms/internal/an;

    return-void
.end method

.method public constructor <init>(IJLandroid/os/Bundle;ILjava/util/List;ZIZLjava/lang/String;Lcom/google/android/gms/internal/ba;Landroid/location/Location;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Landroid/os/Bundle;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZIZ",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/internal/ba;",
            "Landroid/location/Location;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/am;->versionCode:I

    iput-wide p2, p0, Lcom/google/android/gms/internal/am;->lK:J

    iput-object p4, p0, Lcom/google/android/gms/internal/am;->extras:Landroid/os/Bundle;

    iput p5, p0, Lcom/google/android/gms/internal/am;->lL:I

    iput-object p6, p0, Lcom/google/android/gms/internal/am;->lM:Ljava/util/List;

    iput-boolean p7, p0, Lcom/google/android/gms/internal/am;->lN:Z

    iput p8, p0, Lcom/google/android/gms/internal/am;->lO:I

    iput-boolean p9, p0, Lcom/google/android/gms/internal/am;->lP:Z

    iput-object p10, p0, Lcom/google/android/gms/internal/am;->lQ:Ljava/lang/String;

    iput-object p11, p0, Lcom/google/android/gms/internal/am;->lR:Lcom/google/android/gms/internal/ba;

    iput-object p12, p0, Lcom/google/android/gms/internal/am;->lS:Landroid/location/Location;

    iput-object p13, p0, Lcom/google/android/gms/internal/am;->lT:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/an;->a(Lcom/google/android/gms/internal/am;Landroid/os/Parcel;I)V

    return-void
.end method
