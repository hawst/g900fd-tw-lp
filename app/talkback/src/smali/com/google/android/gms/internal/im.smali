.class public final Lcom/google/android/gms/internal/im;
.super Lcom/google/android/gms/location/places/Place;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/in;


# instance fields
.field private final UJ:Lcom/google/android/gms/maps/model/LatLng;

.field private final UL:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/location/places/PlaceType;",
            ">;"
        }
    .end annotation
.end field

.field private final UN:Landroid/os/Bundle;

.field private final UO:Lcom/google/android/gms/internal/io;

.field private final UP:F

.field private final UQ:Lcom/google/android/gms/maps/model/LatLngBounds;

.field private final UR:Ljava/lang/String;

.field private final US:Landroid/net/Uri;

.field private final UT:Z

.field private final UU:F

.field private final UV:I

.field private final UW:J

.field private final UX:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/gms/location/places/PlaceType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final UY:Ljava/util/TimeZone;

.field private UZ:Ljava/util/Locale;

.field private final wo:Ljava/lang/String;

.field final wv:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/in;

    invoke-direct {v0}, Lcom/google/android/gms/internal/in;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/im;->CREATOR:Lcom/google/android/gms/internal/in;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/util/List;Landroid/os/Bundle;Lcom/google/android/gms/internal/io;Lcom/google/android/gms/maps/model/LatLng;FLcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;Landroid/net/Uri;ZFIJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/location/places/PlaceType;",
            ">;",
            "Landroid/os/Bundle;",
            "Lcom/google/android/gms/internal/io;",
            "Lcom/google/android/gms/maps/model/LatLng;",
            "F",
            "Lcom/google/android/gms/maps/model/LatLngBounds;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "ZFIJ)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/location/places/Place;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/im;->wv:I

    iput-object p2, p0, Lcom/google/android/gms/internal/im;->wo:Ljava/lang/String;

    invoke-static {p3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/internal/im;->UL:Ljava/util/List;

    iput-object p4, p0, Lcom/google/android/gms/internal/im;->UN:Landroid/os/Bundle;

    iput-object p5, p0, Lcom/google/android/gms/internal/im;->UO:Lcom/google/android/gms/internal/io;

    iput-object p6, p0, Lcom/google/android/gms/internal/im;->UJ:Lcom/google/android/gms/maps/model/LatLng;

    iput p7, p0, Lcom/google/android/gms/internal/im;->UP:F

    iput-object p8, p0, Lcom/google/android/gms/internal/im;->UQ:Lcom/google/android/gms/maps/model/LatLngBounds;

    iput-object p9, p0, Lcom/google/android/gms/internal/im;->UR:Ljava/lang/String;

    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/gms/internal/im;->US:Landroid/net/Uri;

    move/from16 v0, p11

    iput-boolean v0, p0, Lcom/google/android/gms/internal/im;->UT:Z

    move/from16 v0, p12

    iput v0, p0, Lcom/google/android/gms/internal/im;->UU:F

    move/from16 v0, p13

    iput v0, p0, Lcom/google/android/gms/internal/im;->UV:I

    move-wide/from16 v0, p14

    iput-wide v0, p0, Lcom/google/android/gms/internal/im;->UW:J

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/location/places/PlaceType;->create(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v5

    invoke-virtual {p4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/internal/im;->UX:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/gms/internal/im;->UR:Ljava/lang/String;

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/internal/im;->UY:Ljava/util/TimeZone;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/gms/internal/im;->UZ:Ljava/util/Locale;

    return-void
.end method

.method private bk(Ljava/lang/String;)V
    .locals 0

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/im;->CREATOR:Lcom/google/android/gms/internal/in;

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    .end local p1    # "object":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .restart local p1    # "object":Ljava/lang/Object;
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/internal/im;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/gms/internal/im;

    .end local p1    # "object":Ljava/lang/Object;
    iget-object v2, p0, Lcom/google/android/gms/internal/im;->wo:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/internal/im;->wo:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/internal/im;->UZ:Ljava/util/Locale;

    iget-object v3, p1, Lcom/google/android/gms/internal/im;->UZ:Ljava/util/Locale;

    invoke-static {v2, v3}, Lcom/google/android/gms/internal/gf;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lcom/google/android/gms/internal/im;->UW:J

    iget-wide v4, p1, Lcom/google/android/gms/internal/im;->UW:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    const-string v0, "getId"

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/im;->bk(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/im;->wo:Ljava/lang/String;

    return-object v0
.end method

.method public getLatLng()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    const-string v0, "getLatLng"

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/im;->bk(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/im;->UJ:Lcom/google/android/gms/maps/model/LatLng;

    return-object v0
.end method

.method public getLevelNumber()F
    .locals 1

    const-string v0, "getLevelNumber"

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/im;->bk(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/internal/im;->UP:F

    return v0
.end method

.method public getPriceLevel()I
    .locals 1

    const-string v0, "getPriceLevel"

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/im;->bk(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/internal/im;->UV:I

    return v0
.end method

.method public getRating()F
    .locals 1

    const-string v0, "getRating"

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/im;->bk(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/internal/im;->UU:F

    return v0
.end method

.method public getTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/location/places/PlaceType;",
            ">;"
        }
    .end annotation

    const-string v0, "getTypes"

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/im;->bk(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/im;->UL:Ljava/util/List;

    return-object v0
.end method

.method public getViewport()Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 1

    const-string v0, "getViewport"

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/im;->bk(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/im;->UQ:Lcom/google/android/gms/maps/model/LatLngBounds;

    return-object v0
.end method

.method public getWebsiteUri()Landroid/net/Uri;
    .locals 1

    const-string v0, "getWebsiteUri"

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/im;->bk(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/im;->US:Landroid/net/Uri;

    return-object v0
.end method

.method public hM()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/internal/im;->UW:J

    return-wide v0
.end method

.method public hN()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/im;->UN:Landroid/os/Bundle;

    return-object v0
.end method

.method public hO()Lcom/google/android/gms/internal/io;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/im;->UO:Lcom/google/android/gms/internal/io;

    return-object v0
.end method

.method public hP()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/im;->UR:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/internal/im;->wo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/internal/im;->UZ:Ljava/util/Locale;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/internal/im;->UW:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/internal/gf;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isPermanentlyClosed()Z
    .locals 1

    const-string v0, "isPermanentlyClosed"

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/im;->bk(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/internal/im;->UT:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    invoke-static {p0}, Lcom/google/android/gms/internal/gf;->e(Ljava/lang/Object;)Lcom/google/android/gms/internal/gf$a;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lcom/google/android/gms/internal/im;->wo:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/gf$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/gf$a;

    move-result-object v0

    const-string v1, "localization"

    iget-object v2, p0, Lcom/google/android/gms/internal/im;->UO:Lcom/google/android/gms/internal/io;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/gf$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/gf$a;

    move-result-object v0

    const-string v1, "locale"

    iget-object v2, p0, Lcom/google/android/gms/internal/im;->UZ:Ljava/util/Locale;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/gf$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/gf$a;

    move-result-object v0

    const-string v1, "latlng"

    iget-object v2, p0, Lcom/google/android/gms/internal/im;->UJ:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/gf$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/gf$a;

    move-result-object v0

    const-string v1, "levelNumber"

    iget v2, p0, Lcom/google/android/gms/internal/im;->UP:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/gf$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/gf$a;

    move-result-object v0

    const-string v1, "viewport"

    iget-object v2, p0, Lcom/google/android/gms/internal/im;->UQ:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/gf$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/gf$a;

    move-result-object v0

    const-string v1, "timeZone"

    iget-object v2, p0, Lcom/google/android/gms/internal/im;->UR:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/gf$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/gf$a;

    move-result-object v0

    const-string v1, "websiteUri"

    iget-object v2, p0, Lcom/google/android/gms/internal/im;->US:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/gf$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/gf$a;

    move-result-object v0

    const-string v1, "isPermanentlyClosed"

    iget-boolean v2, p0, Lcom/google/android/gms/internal/im;->UT:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/gf$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/gf$a;

    move-result-object v0

    const-string v1, "priceLevel"

    iget v2, p0, Lcom/google/android/gms/internal/im;->UV:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/gf$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/gf$a;

    move-result-object v0

    const-string v1, "timestampSecs"

    iget-wide v2, p0, Lcom/google/android/gms/internal/im;->UW:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/gf$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/gf$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gf$a;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    sget-object v0, Lcom/google/android/gms/internal/im;->CREATOR:Lcom/google/android/gms/internal/in;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/in;->a(Lcom/google/android/gms/internal/im;Landroid/os/Parcel;I)V

    return-void
.end method
