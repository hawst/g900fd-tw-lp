.class public final Lcom/google/android/gms/internal/cg;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/cf;


# instance fields
.field public final mimeType:Ljava/lang/String;

.field public final nP:Ljava/lang/String;

.field public final nQ:Ljava/lang/String;

.field public final nR:Ljava/lang/String;

.field public final nS:Ljava/lang/String;

.field public final packageName:Ljava/lang/String;

.field public final url:Ljava/lang/String;

.field public final versionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/cf;

    invoke-direct {v0}, Lcom/google/android/gms/internal/cf;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/cg;->CREATOR:Lcom/google/android/gms/internal/cf;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/cg;->versionCode:I

    iput-object p2, p0, Lcom/google/android/gms/internal/cg;->nP:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/internal/cg;->url:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/cg;->mimeType:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/internal/cg;->packageName:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/internal/cg;->nQ:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/internal/cg;->nR:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/gms/internal/cg;->nS:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/cf;->a(Lcom/google/android/gms/internal/cg;Landroid/os/Parcel;I)V

    return-void
.end method
