.class public final Lcom/google/android/gms/internal/ba;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/bb;


# instance fields
.field public final backgroundColor:I

.field public final mA:Ljava/lang/String;

.field public final mB:I

.field public final mC:Ljava/lang/String;

.field public final mD:I

.field public final mE:I

.field public final mt:I

.field public final mu:I

.field public final mv:I

.field public final mw:I

.field public final mx:I

.field public final my:I

.field public final mz:I

.field public final query:Ljava/lang/String;

.field public final versionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/bb;

    invoke-direct {v0}, Lcom/google/android/gms/internal/bb;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ba;->CREATOR:Lcom/google/android/gms/internal/bb;

    return-void
.end method

.method constructor <init>(IIIIIIIIILjava/lang/String;ILjava/lang/String;IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/ba;->versionCode:I

    iput p2, p0, Lcom/google/android/gms/internal/ba;->mt:I

    iput p3, p0, Lcom/google/android/gms/internal/ba;->backgroundColor:I

    iput p4, p0, Lcom/google/android/gms/internal/ba;->mu:I

    iput p5, p0, Lcom/google/android/gms/internal/ba;->mv:I

    iput p6, p0, Lcom/google/android/gms/internal/ba;->mw:I

    iput p7, p0, Lcom/google/android/gms/internal/ba;->mx:I

    iput p8, p0, Lcom/google/android/gms/internal/ba;->my:I

    iput p9, p0, Lcom/google/android/gms/internal/ba;->mz:I

    iput-object p10, p0, Lcom/google/android/gms/internal/ba;->mA:Ljava/lang/String;

    iput p11, p0, Lcom/google/android/gms/internal/ba;->mB:I

    iput-object p12, p0, Lcom/google/android/gms/internal/ba;->mC:Ljava/lang/String;

    iput p13, p0, Lcom/google/android/gms/internal/ba;->mD:I

    iput p14, p0, Lcom/google/android/gms/internal/ba;->mE:I

    iput-object p15, p0, Lcom/google/android/gms/internal/ba;->query:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/bb;->a(Lcom/google/android/gms/internal/ba;Landroid/os/Parcel;I)V

    return-void
.end method
