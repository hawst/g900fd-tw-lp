.class public Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/auth/firstparty/proximity/data/a;


# instance fields
.field private final AH:I

.field public final accountId:Ljava/lang/String;

.field public final allowedChannels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final certificate:Lcom/google/android/gms/auth/firstparty/proximity/data/Certificate;

.field public final id:Ljava/lang/String;

.field public final requesters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/auth/firstparty/proximity/data/CapacityRequester;",
            ">;"
        }
    .end annotation
.end field

.field public final type:Ljava/lang/String;

.field final version:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/auth/firstparty/proximity/data/a;

    invoke-direct {v0}, Lcom/google/android/gms/auth/firstparty/proximity/data/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->CREATOR:Lcom/google/android/gms/auth/firstparty/proximity/data/a;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/proximity/data/Certificate;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1, "version"    # I
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "accountId"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;
    .param p5, "certificate"    # Lcom/google/android/gms/auth/firstparty/proximity/data/Certificate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/auth/firstparty/proximity/data/Certificate;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/auth/firstparty/proximity/data/CapacityRequester;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p6, "requesters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/auth/firstparty/proximity/data/CapacityRequester;>;"
    .local p7, "allowedChannels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->version:I

    invoke-static {p2}, Lcom/google/android/gms/internal/gh;->aw(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->id:Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/gms/internal/gh;->aw(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->accountId:Ljava/lang/String;

    invoke-static {p4}, Lcom/google/android/gms/internal/gh;->aw(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->type:Ljava/lang/String;

    invoke-static {p5}, Lcom/google/android/gms/internal/gh;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/proximity/data/Certificate;

    iput-object v0, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->certificate:Lcom/google/android/gms/auth/firstparty/proximity/data/Certificate;

    if-nez p6, :cond_0

    new-instance p6, Ljava/util/ArrayList;

    .end local p6    # "requesters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/auth/firstparty/proximity/data/CapacityRequester;>;"
    invoke-direct {p6}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    iput-object p6, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->requesters:Ljava/util/List;

    if-nez p7, :cond_1

    new-instance p7, Ljava/util/ArrayList;

    .end local p7    # "allowedChannels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p7}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    iput-object p7, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->allowedChannels:Ljava/util/List;

    invoke-direct {p0}, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->dA()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->AH:I

    return-void
.end method

.method private dA()I
    .locals 3

    const/16 v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->id:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v1, v1, 0x20f

    mul-int/2addr v1, v0

    iget-object v2, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->accountId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    mul-int/2addr v1, v0

    iget-object v2, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->type:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    mul-int/2addr v1, v0

    iget-object v2, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->allowedChannels:Ljava/util/List;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    mul-int/2addr v1, v0

    iget-object v2, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->certificate:Lcom/google/android/gms/auth/firstparty/proximity/data/Certificate;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/proximity/data/Certificate;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    mul-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->requesters:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_1

    .end local p1    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    if-ne p1, p0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    instance-of v2, p1, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;

    if-eqz v2, :cond_0

    check-cast p1, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;

    .end local p1    # "obj":Ljava/lang/Object;
    iget-object v2, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->accountId:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->accountId:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->type:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->type:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->allowedChannels:Ljava/util/List;

    iget-object v3, p1, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->allowedChannels:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->allowedChannels:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->allowedChannels:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->certificate:Lcom/google/android/gms/auth/firstparty/proximity/data/Certificate;

    iget-object v3, p1, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->certificate:Lcom/google/android/gms/auth/firstparty/proximity/data/Certificate;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/firstparty/proximity/data/Certificate;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->requesters:Ljava/util/List;

    iget-object v3, p1, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->requesters:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->requesters:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->requesters:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;->AH:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/auth/firstparty/proximity/data/a;->a(Lcom/google/android/gms/auth/firstparty/proximity/data/Capacity;Landroid/os/Parcel;I)V

    return-void
.end method
