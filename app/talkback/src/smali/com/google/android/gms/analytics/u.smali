.class Lcom/google/android/gms/analytics/u;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/analytics/j;


# instance fields
.field so:Ljava/lang/String;

.field sp:Ljava/lang/String;

.field uV:Ljava/lang/String;

.field uW:I

.field uX:I


# direct methods
.method constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/gms/analytics/u;->uW:I

    iput v0, p0, Lcom/google/android/gms/analytics/u;->uX:I

    return-void
.end method


# virtual methods
.method public cA()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/analytics/u;->uV:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cB()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/analytics/u;->uV:Ljava/lang/String;

    return-object v0
.end method

.method public cC()Z
    .locals 1

    iget v0, p0, Lcom/google/android/gms/analytics/u;->uW:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cD()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/analytics/u;->uW:I

    return v0
.end method

.method public cE()Z
    .locals 2

    iget v0, p0, Lcom/google/android/gms/analytics/u;->uX:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cF()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/analytics/u;->uX:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cw()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/analytics/u;->so:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cx()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/analytics/u;->so:Ljava/lang/String;

    return-object v0
.end method

.method public cy()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/analytics/u;->sp:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cz()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/analytics/u;->sp:Ljava/lang/String;

    return-object v0
.end method
