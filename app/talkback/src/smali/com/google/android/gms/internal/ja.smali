.class public Lcom/google/android/gms/internal/ja;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/internal/ja;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final VI:Z

.field private final VJ:Z

.field private final VK:Z

.field private final VL:Z

.field private final VM:Z

.field private final wv:I

.field private final yP:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/jb;

    invoke-direct {v0}, Lcom/google/android/gms/internal/jb;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ja;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;ZZZZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/ja;->wv:I

    iput-object p2, p0, Lcom/google/android/gms/internal/ja;->yP:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/gms/internal/ja;->VI:Z

    iput-boolean p4, p0, Lcom/google/android/gms/internal/ja;->VJ:Z

    iput-boolean p5, p0, Lcom/google/android/gms/internal/ja;->VK:Z

    iput-boolean p6, p0, Lcom/google/android/gms/internal/ja;->VL:Z

    iput-boolean p7, p0, Lcom/google/android/gms/internal/ja;->VM:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ja;->yP:Ljava/lang/String;

    return-object v0
.end method

.method getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/ja;->wv:I

    return v0
.end method

.method public isOptedInForAppUsageCollection()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/ja;->VK:Z

    return v0
.end method

.method public isOptedInForCallAndSmsLogCollection()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/ja;->VL:Z

    return v0
.end method

.method public isOptedInForContactUpload()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/ja;->VM:Z

    return v0
.end method

.method public isOptedInForDeviceData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/ja;->VJ:Z

    return v0
.end method

.method public isOptedInForDeviceState()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/ja;->VI:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/jb;->a(Lcom/google/android/gms/internal/ja;Landroid/os/Parcel;I)V

    return-void
.end method
