.class Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;
.super Landroid/support/v4/widget/ViewDragHelper$Callback;
.source "DrawerLayoutMW.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/widget/DrawerLayoutMW;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewDragCallback"
.end annotation


# instance fields
.field private mDragger:Landroid/support/v4/widget/ViewDragHelper;

.field private final mGravity:I

.field private final mPeekRunnable:Ljava/lang/Runnable;

.field final synthetic this$0:Landroid/support/v4/widget/DrawerLayoutMW;


# direct methods
.method public constructor <init>(Landroid/support/v4/widget/DrawerLayoutMW;I)V
    .locals 1
    .param p2, "gravity"    # I

    .prologue
    .line 1473
    iput-object p1, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-direct {p0}, Landroid/support/v4/widget/ViewDragHelper$Callback;-><init>()V

    .line 1467
    new-instance v0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback$1;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback$1;-><init>(Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;)V

    iput-object v0, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->mPeekRunnable:Ljava/lang/Runnable;

    .line 1474
    iput p2, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->mGravity:I

    .line 1475
    return-void
.end method

.method static synthetic access$000(Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;

    .prologue
    .line 1463
    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->peekDrawer()V

    return-void
.end method

.method private closeOtherDrawer()V
    .locals 3

    .prologue
    const/4 v0, 0x3

    .line 1541
    iget v2, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->mGravity:I

    if-ne v2, v0, :cond_0

    const/4 v0, 0x5

    .line 1542
    .local v0, "otherGrav":I
    :cond_0
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v2, v0}, Landroid/support/v4/widget/DrawerLayoutMW;->findDrawerWithGravity(I)Landroid/view/View;

    move-result-object v1

    .line 1543
    .local v1, "toClose":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 1544
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/DrawerLayoutMW;->closeDrawer(Landroid/view/View;)V

    .line 1546
    :cond_1
    return-void
.end method

.method private peekDrawer()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1591
    iget-object v7, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->mDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v7}, Landroid/support/v4/widget/ViewDragHelper;->getEdgeSize()I

    move-result v3

    .line 1592
    .local v3, "peekDistance":I
    iget v7, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->mGravity:I

    if-ne v7, v8, :cond_4

    move v1, v6

    .line 1593
    .local v1, "leftEdge":Z
    :goto_0
    if-eqz v1, :cond_5

    .line 1594
    iget-object v7, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v7, v8}, Landroid/support/v4/widget/DrawerLayoutMW;->findDrawerWithGravity(I)Landroid/view/View;

    move-result-object v4

    .line 1595
    .local v4, "toCapture":Landroid/view/View;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v5

    neg-int v5, v5

    :cond_0
    add-int v0, v5, v3

    .line 1601
    .local v0, "childLeft":I
    :goto_1
    if-eqz v4, :cond_3

    if-eqz v1, :cond_1

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v5

    if-lt v5, v0, :cond_2

    :cond_1
    if-nez v1, :cond_3

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v5

    if-le v5, v0, :cond_3

    :cond_2
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v5, v4}, Landroid/support/v4/widget/DrawerLayoutMW;->getDrawerLockMode(Landroid/view/View;)I

    move-result v5

    if-nez v5, :cond_3

    .line 1604
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    .line 1605
    .local v2, "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->mDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v7

    invoke-virtual {v5, v4, v0, v7}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    .line 1606
    iput-boolean v6, v2, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->isPeeking:Z

    .line 1607
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v5}, Landroid/support/v4/widget/DrawerLayoutMW;->invalidate()V

    .line 1609
    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->closeOtherDrawer()V

    .line 1611
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v5}, Landroid/support/v4/widget/DrawerLayoutMW;->cancelChildViewTouch()V

    .line 1613
    .end local v2    # "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    :cond_3
    return-void

    .end local v0    # "childLeft":I
    .end local v1    # "leftEdge":Z
    .end local v4    # "toCapture":Landroid/view/View;
    :cond_4
    move v1, v5

    .line 1592
    goto :goto_0

    .line 1597
    .restart local v1    # "leftEdge":Z
    :cond_5
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    const/4 v7, 0x5

    invoke-virtual {v5, v7}, Landroid/support/v4/widget/DrawerLayoutMW;->findDrawerWithGravity(I)Landroid/view/View;

    move-result-object v4

    .line 1598
    .restart local v4    # "toCapture":Landroid/view/View;
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v5}, Landroid/support/v4/widget/DrawerLayoutMW;->getWidth()I

    move-result v5

    sub-int v0, v5, v3

    .restart local v0    # "childLeft":I
    goto :goto_1
.end method


# virtual methods
.method public clampViewPositionHorizontal(Landroid/view/View;II)I
    .locals 3
    .param p1, "child"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "dx"    # I

    .prologue
    .line 1648
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    const/4 v2, 0x3

    invoke-virtual {v1, p1, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->checkDrawerViewGravity(Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1649
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    neg-int v1, v1

    const/4 v2, 0x0

    invoke-static {p2, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1658
    :goto_0
    return v1

    .line 1651
    :cond_0
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v1}, Landroid/support/v4/widget/DrawerLayoutMW;->getWidth()I

    move-result v0

    .line 1654
    .local v0, "width":I
    if-le p2, v0, :cond_1

    .line 1655
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    goto :goto_0

    .line 1658
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int v1, v0, v1

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0
.end method

.method public clampViewPositionVertical(Landroid/view/View;II)I
    .locals 1
    .param p1, "child"    # Landroid/view/View;
    .param p2, "top"    # I
    .param p3, "dy"    # I

    .prologue
    .line 1664
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    return v0
.end method

.method public getViewHorizontalDragRange(Landroid/view/View;)I
    .locals 1
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 1643
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    return v0
.end method

.method public onEdgeDragStarted(II)V
    .locals 3
    .param p1, "edgeFlags"    # I
    .param p2, "pointerId"    # I

    .prologue
    .line 1630
    and-int/lit8 v1, p1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1631
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->findDrawerWithGravity(I)Landroid/view/View;

    move-result-object v0

    .line 1636
    .local v0, "toCapture":Landroid/view/View;
    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/DrawerLayoutMW;->getDrawerLockMode(Landroid/view/View;)I

    move-result v1

    if-nez v1, :cond_0

    .line 1637
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->mDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v1, v0, p2}, Landroid/support/v4/widget/ViewDragHelper;->captureChildView(Landroid/view/View;I)V

    .line 1639
    :cond_0
    return-void

    .line 1633
    .end local v0    # "toCapture":Landroid/view/View;
    :cond_1
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->findDrawerWithGravity(I)Landroid/view/View;

    move-result-object v0

    .restart local v0    # "toCapture":Landroid/view/View;
    goto :goto_0
.end method

.method public onEdgeLock(I)Z
    .locals 1
    .param p1, "edgeFlags"    # I

    .prologue
    .line 1624
    const/4 v0, 0x0

    return v0
.end method

.method public onEdgeTouched(II)V
    .locals 4
    .param p1, "edgeFlags"    # I
    .param p2, "pointerId"    # I

    .prologue
    .line 1585
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->mPeekRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/widget/DrawerLayoutMW;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1586
    return-void
.end method

.method public onViewCaptured(Landroid/view/View;I)V
    .locals 2
    .param p1, "capturedChild"    # Landroid/view/View;
    .param p2, "activePointerId"    # I

    .prologue
    .line 1534
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    .line 1535
    .local v0, "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->isPeeking:Z

    .line 1537
    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->closeOtherDrawer()V

    .line 1538
    return-void
.end method

.method public onViewDragStateChanged(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 1495
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    iget v1, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->mGravity:I

    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->mDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v2}, Landroid/support/v4/widget/ViewDragHelper;->getCapturedView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->updateDrawerState(IILandroid/view/View;)V

    .line 1496
    return-void
.end method

.method public onViewPositionChanged(Landroid/view/View;IIII)V
    .locals 9
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "dx"    # I
    .param p5, "dy"    # I

    .prologue
    const/4 v6, 0x4

    const/4 v8, 0x3

    .line 1501
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 1504
    .local v1, "childWidth":I
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v5, p1, v8}, Landroid/support/v4/widget/DrawerLayoutMW;->checkDrawerViewGravity(Landroid/view/View;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1505
    add-int v5, v1, p2

    int-to-float v5, v5

    int-to-float v7, v1

    div-float v2, v5, v7

    .line 1512
    .local v2, "offset":F
    :goto_0
    const/4 v0, 0x0

    .line 1513
    .local v0, "MWOffSet":F
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    # getter for: Landroid/support/v4/widget/DrawerLayoutMW;->mIsTouchDown:Z
    invoke-static {v5}, Landroid/support/v4/widget/DrawerLayoutMW;->access$100(Landroid/support/v4/widget/DrawerLayoutMW;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    # getter for: Landroid/support/v4/widget/DrawerLayoutMW;->mDrawerState:I
    invoke-static {v5}, Landroid/support/v4/widget/DrawerLayoutMW;->access$200(Landroid/support/v4/widget/DrawerLayoutMW;)I

    move-result v5

    const/4 v7, 0x2

    if-ne v5, v7, :cond_2

    .line 1514
    const/4 v0, 0x0

    .line 1515
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v5, p1, v8}, Landroid/support/v4/widget/DrawerLayoutMW;->checkDrawerViewGravity(Landroid/view/View;I)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    # getter for: Landroid/support/v4/widget/DrawerLayoutMW;->mApplistHeight:I
    invoke-static {v5}, Landroid/support/v4/widget/DrawerLayoutMW;->access$300(Landroid/support/v4/widget/DrawerLayoutMW;)I

    move-result v5

    neg-int v5, v5

    :goto_1
    invoke-virtual {p1, v5}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1521
    :goto_2
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v3

    .line 1522
    .local v3, "vis":I
    if-ne v3, v6, :cond_3

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v5, v0, v5

    if-nez v5, :cond_3

    .line 1530
    :goto_3
    return-void

    .line 1507
    .end local v0    # "MWOffSet":F
    .end local v2    # "offset":F
    .end local v3    # "vis":I
    :cond_0
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v5}, Landroid/support/v4/widget/DrawerLayoutMW;->getWidth()I

    move-result v4

    .line 1508
    .local v4, "width":I
    sub-int v5, v4, p2

    int-to-float v5, v5

    int-to-float v7, v1

    div-float v2, v5, v7

    .restart local v2    # "offset":F
    goto :goto_0

    .line 1515
    .end local v4    # "width":I
    .restart local v0    # "MWOffSet":F
    :cond_1
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    # getter for: Landroid/support/v4/widget/DrawerLayoutMW;->mApplistHeight:I
    invoke-static {v5}, Landroid/support/v4/widget/DrawerLayoutMW;->access$300(Landroid/support/v4/widget/DrawerLayoutMW;)I

    move-result v5

    goto :goto_1

    .line 1517
    :cond_2
    move v0, v2

    goto :goto_2

    .line 1526
    .restart local v3    # "vis":I
    :cond_3
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v5, p1, v0}, Landroid/support/v4/widget/DrawerLayoutMW;->setDrawerViewOffset(Landroid/view/View;F)V

    .line 1527
    const/4 v5, 0x0

    cmpl-float v5, v0, v5

    if-nez v5, :cond_4

    move v5, v6

    :goto_4
    invoke-virtual {p1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1528
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v5}, Landroid/support/v4/widget/DrawerLayoutMW;->invalidate()V

    goto :goto_3

    .line 1527
    :cond_4
    const/4 v5, 0x0

    goto :goto_4
.end method

.method public onViewReleased(Landroid/view/View;FF)V
    .locals 7
    .param p1, "releasedChild"    # Landroid/view/View;
    .param p2, "xvel"    # F
    .param p3, "yvel"    # F

    .prologue
    const/4 v6, 0x0

    .line 1552
    iget-object v4, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v4, p1}, Landroid/support/v4/widget/DrawerLayoutMW;->getDrawerViewOffset(Landroid/view/View;)F

    move-result v2

    .line 1553
    .local v2, "offset":F
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 1556
    .local v0, "childWidth":I
    iget-object v4, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    const/4 v5, 0x3

    invoke-virtual {v4, p1, v5}, Landroid/support/v4/widget/DrawerLayoutMW;->checkDrawerViewGravity(Landroid/view/View;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1557
    cmpl-float v4, p2, v6

    if-gtz v4, :cond_0

    cmpl-float v4, p2, v6

    if-nez v4, :cond_1

    const/high16 v4, 0x3f000000    # 0.5f

    cmpl-float v4, v2, v4

    if-lez v4, :cond_1

    :cond_0
    const/4 v1, 0x0

    .line 1579
    .local v1, "left":I
    :goto_0
    iget-object v4, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->mDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {v4, v1, v5}, Landroid/support/v4/widget/ViewDragHelper;->settleCapturedViewAt(II)Z

    .line 1580
    iget-object v4, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v4}, Landroid/support/v4/widget/DrawerLayoutMW;->invalidate()V

    .line 1581
    return-void

    .line 1557
    .end local v1    # "left":I
    :cond_1
    neg-int v1, v0

    goto :goto_0

    .line 1559
    :cond_2
    iget-object v4, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v4}, Landroid/support/v4/widget/DrawerLayoutMW;->getWidth()I

    move-result v3

    .line 1575
    .local v3, "width":I
    iget-object v4, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    # getter for: Landroid/support/v4/widget/DrawerLayoutMW;->mMovingDirection:I
    invoke-static {v4}, Landroid/support/v4/widget/DrawerLayoutMW;->access$400(Landroid/support/v4/widget/DrawerLayoutMW;)I

    move-result v4

    const/16 v5, 0x3e9

    if-ne v4, v5, :cond_3

    sub-int v1, v3, v0

    .restart local v1    # "left":I
    :goto_1
    goto :goto_0

    .end local v1    # "left":I
    :cond_3
    move v1, v3

    goto :goto_1
.end method

.method public removeCallbacks()V
    .locals 2

    .prologue
    .line 1482
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->mPeekRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayoutMW;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1483
    return-void
.end method

.method public setDragger(Landroid/support/v4/widget/ViewDragHelper;)V
    .locals 0
    .param p1, "dragger"    # Landroid/support/v4/widget/ViewDragHelper;

    .prologue
    .line 1478
    iput-object p1, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->mDragger:Landroid/support/v4/widget/ViewDragHelper;

    .line 1479
    return-void
.end method

.method public tryCaptureView(Landroid/view/View;I)Z
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "pointerId"    # I

    .prologue
    .line 1489
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/DrawerLayoutMW;->isDrawerView(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    iget v1, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->mGravity:I

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/widget/DrawerLayoutMW;->checkDrawerViewGravity(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->this$0:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/DrawerLayoutMW;->getDrawerLockMode(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
