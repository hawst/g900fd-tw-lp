.class public Landroid/support/v4/widget/DrawerLayoutMW;
.super Landroid/view/ViewGroup;
.source "DrawerLayoutMW.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/widget/DrawerLayoutMW$AccessibilityDelegate;,
        Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;,
        Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;,
        Landroid/support/v4/widget/DrawerLayoutMW$SavedState;,
        Landroid/support/v4/widget/DrawerLayoutMW$SimpleDrawerListener;,
        Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;
    }
.end annotation


# static fields
.field private static final LAYOUT_ATTRS:[I


# instance fields
.field private mApplistHeight:I

.field private mBezelDrawerSkipArea:I

.field private mBezelPopoutArea:I

.field private mChildrenCanceledTouch:Z

.field private mDisallowInterceptRequested:Z

.field mDisplay:Landroid/view/Display;

.field mDisplaySize:Landroid/graphics/Point;

.field private mDrawerState:I

.field private mFirstLayout:Z

.field private mInLayout:Z

.field private mInitialMotionX:F

.field private mInitialMotionY:F

.field private mIsTouchDown:Z

.field private mLastTouchPoint:F

.field private final mLeftCallback:Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;

.field private final mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

.field private mListener:Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;

.field private mLockModeLeft:I

.field private mLockModeRight:I

.field private mMinDrawerMargin:I

.field private mMovingDirection:I

.field private final mRightCallback:Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;

.field private final mRightDragger:Landroid/support/v4/widget/ViewDragHelper;

.field private mScrimColor:I

.field private mScrimOpacity:F

.field private mScrimPaint:Landroid/graphics/Paint;

.field private mShadowLeft:Landroid/graphics/drawable/Drawable;

.field private mShadowRight:Landroid/graphics/drawable/Drawable;

.field private mbDrawerSkip:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 143
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100b3

    aput v2, v0, v1

    sput-object v0, Landroid/support/v4/widget/DrawerLayoutMW;->LAYOUT_ATTRS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 249
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v4/widget/DrawerLayoutMW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 250
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 253
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v4/widget/DrawerLayoutMW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 254
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 257
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 149
    const/high16 v2, -0x67000000

    iput v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mScrimColor:I

    .line 151
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mScrimPaint:Landroid/graphics/Paint;

    .line 159
    iput-boolean v5, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mFirstLayout:Z

    .line 174
    iput-boolean v4, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mIsTouchDown:Z

    .line 185
    iput-boolean v4, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mbDrawerSkip:Z

    .line 187
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisplaySize:Landroid/graphics/Point;

    .line 188
    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/hardware/display/DisplayManagerGlobal;->getRealDisplay(I)Landroid/view/Display;

    move-result-object v2

    iput-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisplay:Landroid/view/Display;

    .line 259
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->density:F

    .line 261
    .local v0, "density":F
    iput v4, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mMinDrawerMargin:I

    .line 263
    const/high16 v2, 0x43c80000    # 400.0f

    mul-float v1, v2, v0

    .line 265
    .local v1, "minVel":F
    new-instance v2, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;

    const/4 v3, 0x3

    invoke-direct {v2, p0, v3}, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;-><init>(Landroid/support/v4/widget/DrawerLayoutMW;I)V

    iput-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftCallback:Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;

    .line 266
    new-instance v2, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;

    const/4 v3, 0x5

    invoke-direct {v2, p0, v3}, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;-><init>(Landroid/support/v4/widget/DrawerLayoutMW;I)V

    iput-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightCallback:Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;

    .line 268
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftCallback:Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;

    invoke-static {p0, v6, v2}, Landroid/support/v4/widget/ViewDragHelper;->create(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ViewDragHelper$Callback;)Landroid/support/v4/widget/ViewDragHelper;

    move-result-object v2

    iput-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    .line 269
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v2, v5}, Landroid/support/v4/widget/ViewDragHelper;->setEdgeTrackingEnabled(I)V

    .line 270
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/ViewDragHelper;->setMinVelocity(F)V

    .line 271
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftCallback:Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;

    iget-object v3, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v2, v3}, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->setDragger(Landroid/support/v4/widget/ViewDragHelper;)V

    .line 273
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightCallback:Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;

    invoke-static {p0, v6, v2}, Landroid/support/v4/widget/ViewDragHelper;->create(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ViewDragHelper$Callback;)Landroid/support/v4/widget/ViewDragHelper;

    move-result-object v2

    iput-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightDragger:Landroid/support/v4/widget/ViewDragHelper;

    .line 274
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightDragger:Landroid/support/v4/widget/ViewDragHelper;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/support/v4/widget/ViewDragHelper;->setEdgeTrackingEnabled(I)V

    .line 275
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/ViewDragHelper;->setMinVelocity(F)V

    .line 276
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightCallback:Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;

    iget-object v3, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v2, v3}, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->setDragger(Landroid/support/v4/widget/ViewDragHelper;)V

    .line 279
    invoke-virtual {p0, v5}, Landroid/support/v4/widget/DrawerLayoutMW;->setFocusableInTouchMode(Z)V

    .line 281
    new-instance v2, Landroid/support/v4/widget/DrawerLayoutMW$AccessibilityDelegate;

    invoke-direct {v2, p0}, Landroid/support/v4/widget/DrawerLayoutMW$AccessibilityDelegate;-><init>(Landroid/support/v4/widget/DrawerLayoutMW;)V

    invoke-static {p0, v2}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    .line 282
    invoke-static {p0, v4}, Landroid/support/v4/view/ViewGroupCompat;->setMotionEventSplittingEnabled(Landroid/view/ViewGroup;Z)V

    .line 285
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a001d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mApplistHeight:I

    .line 286
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a013c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mBezelPopoutArea:I

    .line 287
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a013d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mBezelDrawerSkipArea:I

    .line 289
    return-void
.end method

.method static synthetic access$100(Landroid/support/v4/widget/DrawerLayoutMW;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v4/widget/DrawerLayoutMW;

    .prologue
    .line 82
    iget-boolean v0, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mIsTouchDown:Z

    return v0
.end method

.method static synthetic access$200(Landroid/support/v4/widget/DrawerLayoutMW;)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v4/widget/DrawerLayoutMW;

    .prologue
    .line 82
    iget v0, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDrawerState:I

    return v0
.end method

.method static synthetic access$300(Landroid/support/v4/widget/DrawerLayoutMW;)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v4/widget/DrawerLayoutMW;

    .prologue
    .line 82
    iget v0, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mApplistHeight:I

    return v0
.end method

.method static synthetic access$400(Landroid/support/v4/widget/DrawerLayoutMW;)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v4/widget/DrawerLayoutMW;

    .prologue
    .line 82
    iget v0, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mMovingDirection:I

    return v0
.end method

.method static synthetic access$500()[I
    .locals 1

    .prologue
    .line 82
    sget-object v0, Landroid/support/v4/widget/DrawerLayoutMW;->LAYOUT_ATTRS:[I

    return-object v0
.end method

.method private findVisibleDrawer()Landroid/view/View;
    .locals 4

    .prologue
    .line 1337
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildCount()I

    move-result v1

    .line 1338
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 1339
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1340
    .local v0, "child":Landroid/view/View;
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayoutMW;->isDrawerView(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayoutMW;->isDrawerVisible(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1344
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return-object v0

    .line 1338
    .restart local v0    # "child":Landroid/view/View;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1344
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static gravityToString(I)Ljava/lang/String;
    .locals 2
    .param p0, "gravity"    # I

    .prologue
    .line 620
    and-int/lit8 v0, p0, 0x3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 621
    const-string v0, "LEFT"

    .line 626
    :goto_0
    return-object v0

    .line 623
    :cond_0
    and-int/lit8 v0, p0, 0x5

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 624
    const-string v0, "RIGHT"

    goto :goto_0

    .line 626
    :cond_1
    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static hasOpaqueBackground(Landroid/view/View;)Z
    .locals 4
    .param p0, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 824
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 825
    .local v0, "bg":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 826
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 828
    :cond_0
    return v1
.end method

.method private hasPeekingDrawer()Z
    .locals 4

    .prologue
    .line 1298
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildCount()I

    move-result v0

    .line 1299
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 1300
    invoke-virtual {p0, v1}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    .line 1301
    .local v2, "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    iget-boolean v3, v2, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->isPeeking:Z

    if-eqz v3, :cond_0

    .line 1302
    const/4 v3, 0x1

    .line 1305
    .end local v2    # "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    :goto_1
    return v3

    .line 1299
    .restart local v2    # "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1305
    .end local v2    # "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private hasVisibleDrawer()Z
    .locals 1

    .prologue
    .line 1333
    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->findVisibleDrawer()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method cancelChildViewTouch()V
    .locals 11

    .prologue
    const/4 v5, 0x0

    .line 1349
    iget-boolean v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mChildrenCanceledTouch:Z

    if-nez v2, :cond_1

    .line 1350
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1351
    .local v0, "now":J
    const/4 v4, 0x3

    const/4 v7, 0x0

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 1353
    .local v8, "cancelEvent":Landroid/view/MotionEvent;
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildCount()I

    move-result v9

    .line 1354
    .local v9, "childCount":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    if-ge v10, v9, :cond_0

    .line 1355
    invoke-virtual {p0, v10}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1354
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 1357
    :cond_0
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    .line 1358
    const/4 v2, 0x1

    iput-boolean v2, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mChildrenCanceledTouch:Z

    .line 1360
    .end local v0    # "now":J
    .end local v8    # "cancelEvent":Landroid/view/MotionEvent;
    .end local v9    # "childCount":I
    .end local v10    # "i":I
    :cond_1
    return-void
.end method

.method checkDrawerViewGravity(Landroid/view/View;I)Z
    .locals 2
    .param p1, "drawerView"    # Landroid/view/View;
    .param p2, "checkFor"    # I

    .prologue
    .line 574
    invoke-virtual {p0, p1}, Landroid/support/v4/widget/DrawerLayoutMW;->getDrawerViewGravity(Landroid/view/View;)I

    move-result v0

    .line 575
    .local v0, "absGrav":I
    and-int v1, v0, p2

    if-ne v1, p2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 1324
    instance-of v0, p1, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public closeDrawer(Landroid/view/View;)V
    .locals 4
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 1195
    invoke-virtual {p0, p1}, Landroid/support/v4/widget/DrawerLayoutMW;->isDrawerView(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1196
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "View "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not a sliding drawer"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1199
    :cond_0
    iget-boolean v1, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mFirstLayout:Z

    if-eqz v1, :cond_1

    .line 1200
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    .line 1201
    .local v0, "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    const/4 v1, 0x0

    iput v1, v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->onScreen:F

    .line 1202
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->knownOpen:Z

    .line 1211
    .end local v0    # "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->invalidate()V

    .line 1212
    return-void

    .line 1204
    :cond_1
    const/4 v1, 0x3

    invoke-virtual {p0, p1, v1}, Landroid/support/v4/widget/DrawerLayoutMW;->checkDrawerViewGravity(Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1205
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    neg-int v2, v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v1, p1, v2, v3}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    goto :goto_0

    .line 1208
    :cond_2
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v1, p1, v2, v3}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    goto :goto_0
.end method

.method public closeDrawers()V
    .locals 1

    .prologue
    .line 1111
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayoutMW;->closeDrawers(Z)V

    .line 1112
    return-void
.end method

.method closeDrawers(Z)V
    .locals 9
    .param p1, "peekingOnly"    # Z

    .prologue
    .line 1115
    const/4 v5, 0x0

    .line 1116
    .local v5, "needsInvalidate":Z
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildCount()I

    move-result v1

    .line 1117
    .local v1, "childCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_3

    .line 1118
    invoke-virtual {p0, v3}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1119
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    .line 1121
    .local v4, "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayoutMW;->isDrawerView(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_0

    if-eqz p1, :cond_1

    iget-boolean v6, v4, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->isPeeking:Z

    if-nez v6, :cond_1

    .line 1117
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1125
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 1127
    .local v2, "childWidth":I
    const/4 v6, 0x3

    invoke-virtual {p0, v0, v6}, Landroid/support/v4/widget/DrawerLayoutMW;->checkDrawerViewGravity(Landroid/view/View;I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1128
    iget-object v6, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    neg-int v7, v2

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v8

    invoke-virtual {v6, v0, v7, v8}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    move-result v6

    or-int/2addr v5, v6

    .line 1135
    :goto_2
    const/4 v6, 0x0

    iput-boolean v6, v4, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->isPeeking:Z

    goto :goto_1

    .line 1131
    :cond_2
    iget-object v6, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getWidth()I

    move-result v7

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v8

    invoke-virtual {v6, v0, v7, v8}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    move-result v6

    or-int/2addr v5, v6

    goto :goto_2

    .line 1138
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "childWidth":I
    .end local v4    # "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    :cond_3
    iget-object v6, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftCallback:Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;

    invoke-virtual {v6}, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->removeCallbacks()V

    .line 1139
    iget-object v6, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightCallback:Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;

    invoke-virtual {v6}, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->removeCallbacks()V

    .line 1141
    if-eqz v5, :cond_4

    .line 1142
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->invalidate()V

    .line 1144
    :cond_4
    return-void
.end method

.method public computeScroll()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 809
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildCount()I

    move-result v0

    .line 810
    .local v0, "childCount":I
    const/4 v3, 0x0

    .line 811
    .local v3, "scrimOpacity":F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 812
    invoke-virtual {p0, v1}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    iget v2, v4, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->onScreen:F

    .line 813
    .local v2, "onscreen":F
    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 811
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 815
    .end local v2    # "onscreen":F
    :cond_0
    iput v3, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mScrimOpacity:F

    .line 818
    iget-object v4, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v4, v6}, Landroid/support/v4/widget/ViewDragHelper;->continueSettling(Z)Z

    move-result v4

    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v5, v6}, Landroid/support/v4/widget/ViewDragHelper;->continueSettling(Z)Z

    move-result v5

    or-int/2addr v4, v5

    if-eqz v4, :cond_1

    .line 819
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 821
    :cond_1
    return-void
.end method

.method dispatchOnDrawerClosed(Landroid/view/View;)V
    .locals 4
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 514
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v0, v3, Landroid/util/DisplayMetrics;->density:F

    .line 515
    .local v0, "density":F
    const/high16 v3, 0x45fa0000    # 8000.0f

    mul-float v2, v3, v0

    .line 516
    .local v2, "minVel":F
    iget-object v3, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v3, v2}, Landroid/support/v4/widget/ViewDragHelper;->setMinVelocity(F)V

    .line 517
    iget-object v3, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v3, v2}, Landroid/support/v4/widget/ViewDragHelper;->setMinVelocity(F)V

    .line 519
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    .line 520
    .local v1, "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    iget-boolean v3, v1, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->knownOpen:Z

    if-eqz v3, :cond_1

    .line 521
    const/4 v3, 0x0

    iput-boolean v3, v1, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->knownOpen:Z

    .line 522
    iget-object v3, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mListener:Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;

    if-eqz v3, :cond_0

    .line 523
    iget-object v3, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mListener:Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;

    invoke-interface {v3, p1}, Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;->onDrawerClosed(Landroid/view/View;)V

    .line 525
    :cond_0
    const/16 v3, 0x20

    invoke-virtual {p0, v3}, Landroid/support/v4/widget/DrawerLayoutMW;->sendAccessibilityEvent(I)V

    .line 528
    :cond_1
    return-void
.end method

.method dispatchOnDrawerOpened(Landroid/view/View;)V
    .locals 4
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 532
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v0, v3, Landroid/util/DisplayMetrics;->density:F

    .line 533
    .local v0, "density":F
    const/high16 v3, 0x43c80000    # 400.0f

    mul-float v2, v3, v0

    .line 534
    .local v2, "minVel":F
    iget-object v3, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v3, v2}, Landroid/support/v4/widget/ViewDragHelper;->setMinVelocity(F)V

    .line 535
    iget-object v3, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v3, v2}, Landroid/support/v4/widget/ViewDragHelper;->setMinVelocity(F)V

    .line 537
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    .line 538
    .local v1, "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    iget-boolean v3, v1, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->knownOpen:Z

    if-nez v3, :cond_1

    .line 539
    const/4 v3, 0x1

    iput-boolean v3, v1, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->knownOpen:Z

    .line 540
    iget-object v3, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mListener:Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;

    if-eqz v3, :cond_0

    .line 541
    iget-object v3, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mListener:Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;

    invoke-interface {v3, p1}, Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;->onDrawerOpened(Landroid/view/View;)V

    .line 543
    :cond_0
    const/16 v3, 0x20

    invoke-virtual {p1, v3}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 545
    :cond_1
    return-void
.end method

.method dispatchOnDrawerSlide(Landroid/view/View;F)V
    .locals 1
    .param p1, "drawerView"    # Landroid/view/View;
    .param p2, "slideOffset"    # F

    .prologue
    .line 548
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mListener:Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;

    if-eqz v0, :cond_0

    .line 549
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mListener:Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;->onDrawerSlide(Landroid/view/View;F)V

    .line 551
    :cond_0
    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 29
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "drawingTime"    # J

    .prologue
    .line 833
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getHeight()I

    move-result v18

    .line 834
    .local v18, "height":I
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayoutMW;->isContentView(Landroid/view/View;)Z

    move-result v17

    .line 835
    .local v17, "drawingContent":Z
    const/4 v13, 0x0

    .local v13, "clipLeft":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getWidth()I

    move-result v14

    .line 837
    .local v14, "clipRight":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v21

    .line 838
    .local v21, "restoreCount":I
    if-eqz v17, :cond_4

    .line 839
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildCount()I

    move-result v10

    .line 840
    .local v10, "childCount":I
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_0
    move/from16 v0, v19

    if-ge v0, v10, :cond_3

    .line 841
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildAt(I)Landroid/view/View;

    move-result-object v25

    .line 842
    .local v25, "v":Landroid/view/View;
    move-object/from16 v0, v25

    move-object/from16 v1, p2

    if-eq v0, v1, :cond_0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    invoke-static/range {v25 .. v25}, Landroid/support/v4/widget/DrawerLayoutMW;->hasOpaqueBackground(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayoutMW;->isDrawerView(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getHeight()I

    move-result v2

    move/from16 v0, v18

    if-ge v2, v0, :cond_1

    .line 840
    :cond_0
    :goto_1
    add-int/lit8 v19, v19, 0x1

    goto :goto_0

    .line 848
    :cond_1
    const/4 v2, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->checkDrawerViewGravity(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 849
    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getRight()I

    move-result v27

    .line 850
    .local v27, "vright":I
    move/from16 v0, v27

    if-le v0, v13, :cond_0

    move/from16 v13, v27

    goto :goto_1

    .line 852
    .end local v27    # "vright":I
    :cond_2
    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getLeft()I

    move-result v26

    .line 853
    .local v26, "vleft":I
    move/from16 v0, v26

    if-ge v0, v14, :cond_0

    move/from16 v14, v26

    goto :goto_1

    .line 856
    .end local v25    # "v":Landroid/view/View;
    .end local v26    # "vleft":I
    :cond_3
    const/4 v2, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getHeight()I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v2, v14, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 858
    .end local v10    # "childCount":I
    .end local v19    # "i":I
    :cond_4
    invoke-super/range {p0 .. p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v22

    .line 859
    .local v22, "result":Z
    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 861
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mScrimOpacity:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_6

    if-eqz v17, :cond_6

    .line 862
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mScrimColor:I

    const/high16 v3, -0x1000000

    and-int/2addr v2, v3

    ushr-int/lit8 v9, v2, 0x18

    .line 863
    .local v9, "baseAlpha":I
    int-to-float v2, v9

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mScrimOpacity:F

    mul-float/2addr v2, v3

    float-to-int v0, v2

    move/from16 v20, v0

    .line 864
    .local v20, "imag":I
    shl-int/lit8 v2, v20, 0x18

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mScrimColor:I

    const v4, 0xffffff

    and-int/2addr v3, v4

    or-int v15, v2, v3

    .line 865
    .local v15, "color":I
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mScrimPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v15}, Landroid/graphics/Paint;->setColor(I)V

    .line 867
    int-to-float v3, v13

    const/4 v4, 0x0

    int-to-float v5, v14

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getHeight()I

    move-result v2

    int-to-float v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mScrimPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 890
    .end local v9    # "baseAlpha":I
    .end local v15    # "color":I
    .end local v20    # "imag":I
    :cond_5
    :goto_2
    return v22

    .line 868
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mShadowLeft:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_7

    const/4 v2, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->checkDrawerViewGravity(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 869
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mShadowLeft:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v23

    .line 870
    .local v23, "shadowWidth":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getRight()I

    move-result v12

    .line 871
    .local v12, "childRight":I
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v2}, Landroid/support/v4/widget/ViewDragHelper;->getEdgeSize()I

    move-result v16

    .line 872
    .local v16, "drawerPeekDistance":I
    const/4 v2, 0x0

    int-to-float v3, v12

    move/from16 v0, v16

    int-to-float v4, v0

    div-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v8

    .line 874
    .local v8, "alpha":F
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mShadowLeft:Landroid/graphics/drawable/Drawable;

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTop()I

    move-result v3

    add-int v4, v12, v23

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {v2, v12, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 876
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mShadowLeft:Landroid/graphics/drawable/Drawable;

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v3, v8

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 877
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mShadowLeft:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    .line 878
    .end local v8    # "alpha":F
    .end local v12    # "childRight":I
    .end local v16    # "drawerPeekDistance":I
    .end local v23    # "shadowWidth":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mShadowRight:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_5

    const/4 v2, 0x5

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->checkDrawerViewGravity(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 879
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mShadowRight:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v23

    .line 880
    .restart local v23    # "shadowWidth":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getLeft()I

    move-result v11

    .line 881
    .local v11, "childLeft":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getWidth()I

    move-result v2

    sub-int v24, v2, v11

    .line 882
    .local v24, "showing":I
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v2}, Landroid/support/v4/widget/ViewDragHelper;->getEdgeSize()I

    move-result v16

    .line 883
    .restart local v16    # "drawerPeekDistance":I
    const/4 v2, 0x0

    move/from16 v0, v24

    int-to-float v3, v0

    move/from16 v0, v16

    int-to-float v4, v0

    div-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v8

    .line 885
    .restart local v8    # "alpha":F
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mShadowRight:Landroid/graphics/drawable/Drawable;

    sub-int v3, v11, v23

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {v2, v3, v4, v11, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 887
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mShadowRight:Landroid/graphics/drawable/Drawable;

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v3, v8

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 888
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mShadowRight:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_2
.end method

.method findDrawerWithGravity(I)Landroid/view/View;
    .locals 6
    .param p1, "gravity"    # I

    .prologue
    .line 601
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildCount()I

    move-result v1

    .line 602
    .local v1, "childCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_1

    .line 603
    invoke-virtual {p0, v3}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 604
    .local v0, "child":Landroid/view/View;
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayoutMW;->getDrawerViewGravity(Landroid/view/View;)I

    move-result v2

    .line 605
    .local v2, "childGravity":I
    and-int/lit8 v4, v2, 0x7

    and-int/lit8 v5, p1, 0x7

    if-ne v4, v5, :cond_0

    .line 610
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "childGravity":I
    :goto_1
    return-object v0

    .line 602
    .restart local v0    # "child":Landroid/view/View;
    .restart local v2    # "childGravity":I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 610
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "childGravity":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method findOpenDrawer()Landroid/view/View;
    .locals 4

    .prologue
    .line 579
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildCount()I

    move-result v1

    .line 580
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 581
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 582
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    iget-boolean v3, v3, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->knownOpen:Z

    if-eqz v3, :cond_0

    .line 586
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return-object v0

    .line 580
    .restart local v0    # "child":Landroid/view/View;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 586
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1310
    new-instance v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 1329
    new-instance v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 1315
    instance-of v0, p1, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    check-cast p1, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    .end local p1    # "p":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v0, p1}, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;-><init>(Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;)V

    :goto_0
    return-object v0

    .restart local p1    # "p":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    new-instance v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    .end local p1    # "p":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v0, p1}, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_0

    .restart local p1    # "p":Landroid/view/ViewGroup$LayoutParams;
    :cond_1
    new-instance v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public getDrawerLockMode(Landroid/view/View;)I
    .locals 2
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 467
    invoke-virtual {p0, p1}, Landroid/support/v4/widget/DrawerLayoutMW;->getDrawerViewGravity(Landroid/view/View;)I

    move-result v0

    .line 468
    .local v0, "gravity":I
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 469
    iget v1, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLockModeLeft:I

    .line 474
    :goto_0
    return v1

    .line 470
    :cond_0
    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 471
    iget v1, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLockModeRight:I

    goto :goto_0

    .line 474
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getDrawerViewGravity(Landroid/view/View;)I
    .locals 2
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 569
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    iget v0, v1, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->gravity:I

    .line 570
    .local v0, "gravity":I
    invoke-static {p1}, Landroid/support/v4/view/ViewCompat;->getLayoutDirection(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/support/v4/view/GravityCompat;->getAbsoluteGravity(II)I

    move-result v1

    return v1
.end method

.method getDrawerViewOffset(Landroid/view/View;)F
    .locals 1
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 565
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    iget v0, v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->onScreen:F

    return v0
.end method

.method isContentView(Landroid/view/View;)Z
    .locals 1
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 894
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    iget v0, v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->gravity:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isDrawerView(Landroid/view/View;)Z
    .locals 3
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 898
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    iget v1, v2, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->gravity:I

    .line 899
    .local v1, "gravity":I
    invoke-static {p1}, Landroid/support/v4/view/ViewCompat;->getLayoutDirection(Landroid/view/View;)I

    move-result v2

    invoke-static {v1, v2}, Landroid/support/v4/view/GravityCompat;->getAbsoluteGravity(II)I

    move-result v0

    .line 901
    .local v0, "absGravity":I
    and-int/lit8 v2, v0, 0x7

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isDrawerVisible(Landroid/view/View;)Z
    .locals 3
    .param p1, "drawer"    # Landroid/view/View;

    .prologue
    .line 1275
    invoke-virtual {p0, p1}, Landroid/support/v4/widget/DrawerLayoutMW;->isDrawerView(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1276
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "View "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a drawer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1278
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    iget v0, v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->onScreen:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 637
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 638
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mFirstLayout:Z

    .line 639
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 631
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 632
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mFirstLayout:Z

    .line 633
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 906
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v0

    .line 909
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 930
    :pswitch_0
    iget-boolean v8, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mbDrawerSkip:Z

    if-eqz v8, :cond_4

    .line 931
    iput-boolean v6, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mbDrawerSkip:Z

    .line 980
    :cond_0
    :goto_0
    return v6

    .line 911
    :pswitch_1
    new-instance v8, Landroid/graphics/Point;

    invoke-direct {v8}, Landroid/graphics/Point;-><init>()V

    iput-object v8, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisplaySize:Landroid/graphics/Point;

    .line 912
    iget-object v8, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisplay:Landroid/view/Display;

    if-eqz v8, :cond_1

    iget-object v8, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisplay:Landroid/view/Display;

    iget-object v9, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisplaySize:Landroid/graphics/Point;

    invoke-virtual {v8, v9}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 913
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v8

    float-to-int v3, v8

    .line 914
    .local v3, "rawY":I
    iget v8, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mBezelDrawerSkipArea:I

    if-lt v3, v8, :cond_2

    iget-object v8, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisplaySize:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->y:I

    iget v9, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mBezelDrawerSkipArea:I

    sub-int/2addr v8, v9

    if-le v3, v8, :cond_3

    .line 915
    :cond_2
    iput-boolean v7, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mbDrawerSkip:Z

    goto :goto_0

    .line 918
    :cond_3
    iput-boolean v6, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mbDrawerSkip:Z

    .line 939
    .end local v3    # "rawY":I
    :cond_4
    iget-object v8, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v8, p1}, Landroid/support/v4/widget/ViewDragHelper;->shouldInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    iget-object v9, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v9, p1}, Landroid/support/v4/widget/ViewDragHelper;->shouldInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v9

    or-int v1, v8, v9

    .line 942
    .local v1, "interceptForDrag":Z
    const/4 v2, 0x0

    .line 944
    .local v2, "interceptForTap":Z
    packed-switch v0, :pswitch_data_1

    .line 980
    :goto_1
    :pswitch_2
    if-nez v1, :cond_5

    if-nez v2, :cond_5

    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->hasPeekingDrawer()Z

    move-result v8

    if-nez v8, :cond_5

    iget-boolean v8, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mChildrenCanceledTouch:Z

    if-eqz v8, :cond_0

    :cond_5
    move v6, v7

    goto :goto_0

    .line 924
    .end local v1    # "interceptForDrag":Z
    .end local v2    # "interceptForTap":Z
    :pswitch_3
    iget-boolean v8, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mbDrawerSkip:Z

    if-eqz v8, :cond_4

    goto :goto_0

    .line 946
    .restart local v1    # "interceptForDrag":Z
    .restart local v2    # "interceptForTap":Z
    :pswitch_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 947
    .local v4, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    .line 948
    .local v5, "y":F
    iput v4, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mInitialMotionX:F

    .line 949
    iput v5, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mInitialMotionY:F

    .line 950
    iget v8, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mScrimOpacity:F

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-lez v8, :cond_6

    iget-object v8, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    float-to-int v9, v4

    float-to-int v10, v5

    invoke-virtual {v8, v9, v10}, Landroid/support/v4/widget/ViewDragHelper;->findTopChildUnder(II)Landroid/view/View;

    move-result-object v8

    invoke-virtual {p0, v8}, Landroid/support/v4/widget/DrawerLayoutMW;->isContentView(Landroid/view/View;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 952
    const/4 v2, 0x1

    .line 954
    :cond_6
    iput-boolean v6, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisallowInterceptRequested:Z

    .line 955
    iput-boolean v6, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mChildrenCanceledTouch:Z

    goto :goto_1

    .line 974
    .end local v4    # "x":F
    .end local v5    # "y":F
    :pswitch_5
    invoke-virtual {p0, v7}, Landroid/support/v4/widget/DrawerLayoutMW;->closeDrawers(Z)V

    .line 975
    iput-boolean v6, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisallowInterceptRequested:Z

    .line 976
    iput-boolean v6, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mChildrenCanceledTouch:Z

    goto :goto_1

    .line 909
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch

    .line 944
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1364
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->hasVisibleDrawer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1365
    invoke-static {p2}, Landroid/support/v4/view/KeyEventCompat;->startTracking(Landroid/view/KeyEvent;)V

    .line 1366
    const/4 v0, 0x1

    .line 1368
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1373
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    .line 1374
    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->findVisibleDrawer()Landroid/view/View;

    move-result-object v0

    .line 1375
    .local v0, "visibleDrawer":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayoutMW;->getDrawerLockMode(Landroid/view/View;)I

    move-result v1

    if-nez v1, :cond_0

    .line 1376
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->closeDrawers()V

    .line 1378
    :cond_0
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    .line 1380
    .end local v0    # "visibleDrawer":Landroid/view/View;
    :goto_0
    return v1

    .line 1378
    .restart local v0    # "visibleDrawer":Landroid/view/View;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1380
    .end local v0    # "visibleDrawer":Landroid/view/View;
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 23
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 719
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/support/v4/widget/DrawerLayoutMW;->mInLayout:Z

    .line 720
    sub-int v17, p4, p2

    .line 721
    .local v17, "width":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildCount()I

    move-result v6

    .line 722
    .local v6, "childCount":I
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    if-ge v12, v6, :cond_9

    .line 723
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 725
    .local v5, "child":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v18

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    .line 722
    :cond_0
    :goto_1
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 729
    :cond_1
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    .line 731
    .local v13, "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/support/v4/widget/DrawerLayoutMW;->isContentView(Landroid/view/View;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 732
    iget v0, v13, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->leftMargin:I

    move/from16 v18, v0

    iget v0, v13, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->topMargin:I

    move/from16 v19, v0

    iget v0, v13, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->leftMargin:I

    move/from16 v20, v0

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v21

    add-int v20, v20, v21

    iget v0, v13, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->topMargin:I

    move/from16 v21, v0

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v22

    add-int v21, v21, v22

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    goto :goto_1

    .line 736
    :cond_2
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    .line 737
    .local v10, "childWidth":I
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    .line 741
    .local v7, "childHeight":I
    const/16 v18, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v5, v1}, Landroid/support/v4/widget/DrawerLayoutMW;->checkDrawerViewGravity(Landroid/view/View;I)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 742
    neg-int v0, v10

    move/from16 v18, v0

    int-to-float v0, v10

    move/from16 v19, v0

    iget v0, v13, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->onScreen:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    add-int v8, v18, v19

    .line 743
    .local v8, "childLeft":I
    add-int v18, v10, v8

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    int-to-float v0, v10

    move/from16 v19, v0

    div-float v14, v18, v19

    .line 749
    .local v14, "newOffset":F
    :goto_2
    iget v0, v13, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->onScreen:F

    move/from16 v18, v0

    cmpl-float v18, v14, v18

    if-eqz v18, :cond_5

    const/4 v4, 0x1

    .line 751
    .local v4, "changeOffset":Z
    :goto_3
    iget v0, v13, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->gravity:I

    move/from16 v18, v0

    and-int/lit8 v16, v18, 0x70

    .line 753
    .local v16, "vgrav":I
    sparse-switch v16, :sswitch_data_0

    .line 756
    iget v0, v13, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->topMargin:I

    move/from16 v18, v0

    add-int v19, v8, v10

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v5, v8, v0, v1, v7}, Landroid/view/View;->layout(IIII)V

    .line 786
    :goto_4
    if-eqz v4, :cond_3

    .line 787
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Landroid/support/v4/widget/DrawerLayoutMW;->setDrawerViewOffset(Landroid/view/View;F)V

    .line 790
    :cond_3
    iget v0, v13, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->onScreen:F

    move/from16 v18, v0

    const/16 v19, 0x0

    cmpl-float v18, v18, v19

    if-lez v18, :cond_8

    const/4 v15, 0x0

    .line 791
    .local v15, "newVisibility":I
    :goto_5
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v18

    move/from16 v0, v18

    if-eq v0, v15, :cond_0

    .line 792
    invoke-virtual {v5, v15}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 745
    .end local v4    # "changeOffset":Z
    .end local v8    # "childLeft":I
    .end local v14    # "newOffset":F
    .end local v15    # "newVisibility":I
    .end local v16    # "vgrav":I
    :cond_4
    int-to-float v0, v10

    move/from16 v18, v0

    iget v0, v13, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->onScreen:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    sub-int v8, v17, v18

    .line 746
    .restart local v8    # "childLeft":I
    sub-int v18, v17, v8

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    int-to-float v0, v10

    move/from16 v19, v0

    div-float v14, v18, v19

    .restart local v14    # "newOffset":F
    goto :goto_2

    .line 749
    :cond_5
    const/4 v4, 0x0

    goto :goto_3

    .line 761
    .restart local v4    # "changeOffset":Z
    .restart local v16    # "vgrav":I
    :sswitch_0
    sub-int v11, p5, p3

    .line 762
    .local v11, "height":I
    iget v0, v13, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->bottomMargin:I

    move/from16 v18, v0

    sub-int v18, v11, v18

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v19

    sub-int v18, v18, v19

    add-int v19, v8, v10

    iget v0, v13, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->bottomMargin:I

    move/from16 v20, v0

    sub-int v20, v11, v20

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v5, v8, v0, v1, v2}, Landroid/view/View;->layout(IIII)V

    goto :goto_4

    .line 770
    .end local v11    # "height":I
    :sswitch_1
    sub-int v11, p5, p3

    .line 771
    .restart local v11    # "height":I
    sub-int v18, v11, v7

    div-int/lit8 v9, v18, 0x2

    .line 775
    .local v9, "childTop":I
    iget v0, v13, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->topMargin:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v9, v0, :cond_7

    .line 776
    iget v9, v13, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->topMargin:I

    .line 780
    :cond_6
    :goto_6
    add-int v18, v8, v10

    add-int v19, v9, v7

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v5, v8, v9, v0, v1}, Landroid/view/View;->layout(IIII)V

    goto :goto_4

    .line 777
    :cond_7
    add-int v18, v9, v7

    iget v0, v13, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->bottomMargin:I

    move/from16 v19, v0

    sub-int v19, v11, v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_6

    .line 778
    iget v0, v13, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->bottomMargin:I

    move/from16 v18, v0

    sub-int v18, v11, v18

    sub-int v9, v18, v7

    goto :goto_6

    .line 790
    .end local v9    # "childTop":I
    .end local v11    # "height":I
    :cond_8
    const/4 v15, 0x4

    goto :goto_5

    .line 796
    .end local v4    # "changeOffset":Z
    .end local v5    # "child":Landroid/view/View;
    .end local v7    # "childHeight":I
    .end local v8    # "childLeft":I
    .end local v10    # "childWidth":I
    .end local v13    # "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    .end local v14    # "newOffset":F
    .end local v16    # "vgrav":I
    :cond_9
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/support/v4/widget/DrawerLayoutMW;->mInLayout:Z

    .line 797
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/support/v4/widget/DrawerLayoutMW;->mFirstLayout:Z

    .line 798
    return-void

    .line 753
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x50 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .locals 20
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 643
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v15

    .line 644
    .local v15, "widthMode":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v11

    .line 645
    .local v11, "heightMode":I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v16

    .line 646
    .local v16, "widthSize":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v12

    .line 648
    .local v12, "heightSize":I
    const/high16 v17, 0x40000000    # 2.0f

    move/from16 v0, v17

    if-ne v15, v0, :cond_0

    const/high16 v17, 0x40000000    # 2.0f

    move/from16 v0, v17

    if-eq v11, v0, :cond_2

    .line 649
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/DrawerLayoutMW;->isInEditMode()Z

    move-result v17

    if-eqz v17, :cond_5

    .line 654
    const/high16 v17, -0x80000000

    move/from16 v0, v17

    if-ne v15, v0, :cond_3

    .line 655
    const/high16 v15, 0x40000000    # 2.0f

    .line 660
    :cond_1
    :goto_0
    const/high16 v17, -0x80000000

    move/from16 v0, v17

    if-ne v11, v0, :cond_4

    .line 661
    const/high16 v11, 0x40000000    # 2.0f

    .line 673
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1, v12}, Landroid/support/v4/widget/DrawerLayoutMW;->setMeasuredDimension(II)V

    .line 676
    const/4 v10, 0x0

    .line 677
    .local v10, "foundDrawers":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildCount()I

    move-result v4

    .line 678
    .local v4, "childCount":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_2
    if-ge v13, v4, :cond_a

    .line 679
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 681
    .local v3, "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_6

    .line 678
    :goto_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 656
    .end local v3    # "child":Landroid/view/View;
    .end local v4    # "childCount":I
    .end local v10    # "foundDrawers":I
    .end local v13    # "i":I
    :cond_3
    if-nez v15, :cond_1

    .line 657
    const/high16 v15, 0x40000000    # 2.0f

    .line 658
    const/16 v16, 0x12c

    goto :goto_0

    .line 663
    :cond_4
    if-nez v11, :cond_2

    .line 664
    const/high16 v11, 0x40000000    # 2.0f

    .line 665
    const/16 v12, 0x12c

    goto :goto_1

    .line 668
    :cond_5
    new-instance v17, Ljava/lang/IllegalArgumentException;

    const-string v18, "DrawerLayoutMW must be measured with MeasureSpec.EXACTLY."

    invoke-direct/range {v17 .. v18}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 685
    .restart local v3    # "child":Landroid/view/View;
    .restart local v4    # "childCount":I
    .restart local v10    # "foundDrawers":I
    .restart local v13    # "i":I
    :cond_6
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    .line 687
    .local v14, "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/DrawerLayoutMW;->isContentView(Landroid/view/View;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 689
    iget v0, v14, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->leftMargin:I

    move/from16 v17, v0

    sub-int v17, v16, v17

    iget v0, v14, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->rightMargin:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    const/high16 v18, 0x40000000    # 2.0f

    invoke-static/range {v17 .. v18}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 691
    .local v7, "contentWidthSpec":I
    iget v0, v14, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->topMargin:I

    move/from16 v17, v0

    sub-int v17, v12, v17

    iget v0, v14, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->bottomMargin:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    const/high16 v18, 0x40000000    # 2.0f

    invoke-static/range {v17 .. v18}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 693
    .local v6, "contentHeightSpec":I
    invoke-virtual {v3, v7, v6}, Landroid/view/View;->measure(II)V

    goto :goto_3

    .line 694
    .end local v6    # "contentHeightSpec":I
    .end local v7    # "contentWidthSpec":I
    :cond_7
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/DrawerLayoutMW;->isDrawerView(Landroid/view/View;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 695
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/DrawerLayoutMW;->getDrawerViewGravity(Landroid/view/View;)I

    move-result v17

    and-int/lit8 v5, v17, 0x7

    .line 697
    .local v5, "childGravity":I
    and-int v17, v10, v5

    if-eqz v17, :cond_8

    .line 698
    new-instance v17, Ljava/lang/IllegalStateException;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Child drawer has absolute gravity "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-static {v5}, Landroid/support/v4/widget/DrawerLayoutMW;->gravityToString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " but this "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "DrawerLayoutMW"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " already has a "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "drawer view along that edge"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 702
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/DrawerLayoutMW;->mMinDrawerMargin:I

    move/from16 v17, v0

    iget v0, v14, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->leftMargin:I

    move/from16 v18, v0

    add-int v17, v17, v18

    iget v0, v14, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->rightMargin:I

    move/from16 v18, v0

    add-int v17, v17, v18

    iget v0, v14, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->width:I

    move/from16 v18, v0

    move/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildMeasureSpec(III)I

    move-result v9

    .line 705
    .local v9, "drawerWidthSpec":I
    iget v0, v14, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->topMargin:I

    move/from16 v17, v0

    iget v0, v14, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->bottomMargin:I

    move/from16 v18, v0

    add-int v17, v17, v18

    iget v0, v14, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->height:I

    move/from16 v18, v0

    move/from16 v0, p2

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildMeasureSpec(III)I

    move-result v8

    .line 708
    .local v8, "drawerHeightSpec":I
    invoke-virtual {v3, v9, v8}, Landroid/view/View;->measure(II)V

    goto/16 :goto_3

    .line 710
    .end local v5    # "childGravity":I
    .end local v8    # "drawerHeightSpec":I
    .end local v9    # "drawerWidthSpec":I
    :cond_9
    new-instance v17, Ljava/lang/IllegalStateException;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Child "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " at index "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " does not have a valid layout_gravity - must be Gravity.LEFT, "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "Gravity.RIGHT or Gravity.NO_GRAVITY"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 715
    .end local v3    # "child":Landroid/view/View;
    .end local v14    # "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    :cond_a
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 4
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 1385
    move-object v0, p1

    check-cast v0, Landroid/support/v4/widget/DrawerLayoutMW$SavedState;

    .line 1386
    .local v0, "ss":Landroid/support/v4/widget/DrawerLayoutMW$SavedState;
    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayoutMW$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-super {p0, v2}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1388
    iget v2, v0, Landroid/support/v4/widget/DrawerLayoutMW$SavedState;->openDrawerGravity:I

    if-eqz v2, :cond_0

    .line 1389
    iget v2, v0, Landroid/support/v4/widget/DrawerLayoutMW$SavedState;->openDrawerGravity:I

    invoke-virtual {p0, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->findDrawerWithGravity(I)Landroid/view/View;

    move-result-object v1

    .line 1390
    .local v1, "toOpen":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 1391
    invoke-virtual {p0, v1}, Landroid/support/v4/widget/DrawerLayoutMW;->openDrawer(Landroid/view/View;)V

    .line 1395
    .end local v1    # "toOpen":Landroid/view/View;
    :cond_0
    iget v2, v0, Landroid/support/v4/widget/DrawerLayoutMW$SavedState;->lockModeLeft:I

    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, Landroid/support/v4/widget/DrawerLayoutMW;->setDrawerLockMode(II)V

    .line 1396
    iget v2, v0, Landroid/support/v4/widget/DrawerLayoutMW$SavedState;->lockModeRight:I

    const/4 v3, 0x5

    invoke-virtual {p0, v2, v3}, Landroid/support/v4/widget/DrawerLayoutMW;->setDrawerLockMode(II)V

    .line 1397
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 7

    .prologue
    .line 1401
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v5

    .line 1403
    .local v5, "superState":Landroid/os/Parcelable;
    new-instance v4, Landroid/support/v4/widget/DrawerLayoutMW$SavedState;

    invoke-direct {v4, v5}, Landroid/support/v4/widget/DrawerLayoutMW$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1405
    .local v4, "ss":Landroid/support/v4/widget/DrawerLayoutMW$SavedState;
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildCount()I

    move-result v1

    .line 1406
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 1407
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1408
    .local v0, "child":Landroid/view/View;
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayoutMW;->isDrawerView(Landroid/view/View;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1406
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1412
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    .line 1413
    .local v3, "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    iget-boolean v6, v3, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->knownOpen:Z

    if-eqz v6, :cond_0

    .line 1414
    iget v6, v3, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->gravity:I

    iput v6, v4, Landroid/support/v4/widget/DrawerLayoutMW$SavedState;->openDrawerGravity:I

    .line 1420
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    :cond_2
    iget v6, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLockModeLeft:I

    iput v6, v4, Landroid/support/v4/widget/DrawerLayoutMW$SavedState;->lockModeLeft:I

    .line 1421
    iget v6, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLockModeRight:I

    iput v6, v4, Landroid/support/v4/widget/DrawerLayoutMW$SavedState;->lockModeRight:I

    .line 1423
    return-object v4
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 986
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 987
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 1006
    :pswitch_0
    iget-boolean v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mbDrawerSkip:Z

    if-eqz v11, :cond_3

    .line 1007
    const/4 v11, 0x0

    iput-boolean v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mbDrawerSkip:Z

    .line 1008
    const/4 v8, 0x0

    .line 1089
    :goto_0
    return v8

    .line 989
    :pswitch_1
    new-instance v11, Landroid/graphics/Point;

    invoke-direct {v11}, Landroid/graphics/Point;-><init>()V

    iput-object v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisplaySize:Landroid/graphics/Point;

    .line 990
    iget-object v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisplay:Landroid/view/Display;

    if-eqz v11, :cond_0

    iget-object v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisplay:Landroid/view/Display;

    iget-object v12, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisplaySize:Landroid/graphics/Point;

    invoke-virtual {v11, v12}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 991
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v11

    float-to-int v5, v11

    .line 992
    .local v5, "rawY":I
    iget v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mBezelDrawerSkipArea:I

    if-lt v5, v11, :cond_1

    iget-object v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisplaySize:Landroid/graphics/Point;

    iget v11, v11, Landroid/graphics/Point;->y:I

    iget v12, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mBezelDrawerSkipArea:I

    sub-int/2addr v11, v12

    if-le v5, v11, :cond_2

    .line 993
    :cond_1
    const/4 v11, 0x1

    iput-boolean v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mbDrawerSkip:Z

    .line 994
    const/4 v8, 0x0

    goto :goto_0

    .line 996
    :cond_2
    const/4 v11, 0x0

    iput-boolean v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mbDrawerSkip:Z

    .line 1014
    .end local v5    # "rawY":I
    :cond_3
    iget-object v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v11, p1}, Landroid/support/v4/widget/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V

    .line 1015
    iget-object v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v11, p1}, Landroid/support/v4/widget/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V

    .line 1017
    const/4 v8, 0x1

    .line 1019
    .local v8, "wantTouchEvents":Z
    and-int/lit16 v11, v0, 0xff

    packed-switch v11, :pswitch_data_1

    goto :goto_0

    .line 1022
    :pswitch_2
    const/4 v11, 0x1

    iput-boolean v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mIsTouchDown:Z

    .line 1023
    const/16 v11, 0x3e8

    iput v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mMovingDirection:I

    .line 1024
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v11

    iput v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLastTouchPoint:F

    .line 1026
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    .line 1027
    .local v9, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    .line 1028
    .local v10, "y":F
    iput v9, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mInitialMotionX:F

    .line 1029
    iput v10, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mInitialMotionY:F

    .line 1030
    const/4 v11, 0x0

    iput-boolean v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisallowInterceptRequested:Z

    .line 1031
    const/4 v11, 0x0

    iput-boolean v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mChildrenCanceledTouch:Z

    goto :goto_0

    .line 1000
    .end local v8    # "wantTouchEvents":Z
    .end local v9    # "x":F
    .end local v10    # "y":F
    :pswitch_3
    iget-boolean v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mbDrawerSkip:Z

    if-eqz v11, :cond_3

    .line 1001
    const/4 v8, 0x0

    goto :goto_0

    .line 1038
    .restart local v8    # "wantTouchEvents":Z
    :pswitch_4
    iget-object v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    const/4 v12, 0x3

    invoke-virtual {v11, v12}, Landroid/support/v4/widget/ViewDragHelper;->checkTouchSlop(I)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1039
    iget-object v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftCallback:Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;

    invoke-virtual {v11}, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->removeCallbacks()V

    .line 1040
    iget-object v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightCallback:Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;

    invoke-virtual {v11}, Landroid/support/v4/widget/DrawerLayoutMW$ViewDragCallback;->removeCallbacks()V

    .line 1042
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v9

    .line 1043
    .restart local v9    # "x":F
    iget v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLastTouchPoint:F

    sub-float/2addr v11, v9

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    const/high16 v12, 0x40a00000    # 5.0f

    cmpl-float v11, v11, v12

    if-lez v11, :cond_5

    .line 1044
    iget v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLastTouchPoint:F

    cmpg-float v11, v11, v9

    if-gez v11, :cond_6

    .line 1045
    const/16 v11, 0x3ea

    iput v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mMovingDirection:I

    .line 1050
    :cond_5
    :goto_1
    iput v9, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLastTouchPoint:F

    goto/16 :goto_0

    .line 1047
    :cond_6
    const/16 v11, 0x3e9

    iput v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mMovingDirection:I

    goto :goto_1

    .line 1057
    .end local v9    # "x":F
    :pswitch_5
    const/4 v11, 0x0

    iput-boolean v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mIsTouchDown:Z

    .line 1059
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    .line 1060
    .restart local v9    # "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    .line 1061
    .restart local v10    # "y":F
    const/4 v4, 0x1

    .line 1062
    .local v4, "peekingOnly":Z
    iget-object v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    float-to-int v12, v9

    float-to-int v13, v10

    invoke-virtual {v11, v12, v13}, Landroid/support/v4/widget/ViewDragHelper;->findTopChildUnder(II)Landroid/view/View;

    move-result-object v7

    .line 1063
    .local v7, "touchedView":Landroid/view/View;
    if-eqz v7, :cond_7

    invoke-virtual {p0, v7}, Landroid/support/v4/widget/DrawerLayoutMW;->isContentView(Landroid/view/View;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1064
    iget v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mInitialMotionX:F

    sub-float v1, v9, v11

    .line 1065
    .local v1, "dx":F
    iget v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mInitialMotionY:F

    sub-float v2, v10, v11

    .line 1066
    .local v2, "dy":F
    iget-object v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v11}, Landroid/support/v4/widget/ViewDragHelper;->getTouchSlop()I

    move-result v6

    .line 1067
    .local v6, "slop":I
    mul-float v11, v1, v1

    mul-float v12, v2, v2

    add-float/2addr v11, v12

    mul-int v12, v6, v6

    int-to-float v12, v12

    cmpg-float v11, v11, v12

    if-gez v11, :cond_7

    .line 1069
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->findOpenDrawer()Landroid/view/View;

    move-result-object v3

    .line 1070
    .local v3, "openDrawer":Landroid/view/View;
    if-eqz v3, :cond_7

    .line 1071
    invoke-virtual {p0, v3}, Landroid/support/v4/widget/DrawerLayoutMW;->getDrawerLockMode(Landroid/view/View;)I

    move-result v11

    const/4 v12, 0x2

    if-ne v11, v12, :cond_8

    const/4 v4, 0x1

    .line 1075
    .end local v1    # "dx":F
    .end local v2    # "dy":F
    .end local v3    # "openDrawer":Landroid/view/View;
    .end local v6    # "slop":I
    :cond_7
    :goto_2
    invoke-virtual {p0, v4}, Landroid/support/v4/widget/DrawerLayoutMW;->closeDrawers(Z)V

    .line 1076
    const/4 v11, 0x0

    iput-boolean v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisallowInterceptRequested:Z

    goto/16 :goto_0

    .line 1071
    .restart local v1    # "dx":F
    .restart local v2    # "dy":F
    .restart local v3    # "openDrawer":Landroid/view/View;
    .restart local v6    # "slop":I
    :cond_8
    const/4 v4, 0x0

    goto :goto_2

    .line 1081
    .end local v1    # "dx":F
    .end local v2    # "dy":F
    .end local v3    # "openDrawer":Landroid/view/View;
    .end local v4    # "peekingOnly":Z
    .end local v6    # "slop":I
    .end local v7    # "touchedView":Landroid/view/View;
    .end local v9    # "x":F
    .end local v10    # "y":F
    :pswitch_6
    const/4 v11, 0x0

    iput-boolean v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mIsTouchDown:Z

    .line 1082
    const/4 v11, 0x1

    invoke-virtual {p0, v11}, Landroid/support/v4/widget/DrawerLayoutMW;->closeDrawers(Z)V

    .line 1083
    const/4 v11, 0x0

    iput-boolean v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisallowInterceptRequested:Z

    .line 1084
    const/4 v11, 0x0

    iput-boolean v11, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mChildrenCanceledTouch:Z

    goto/16 :goto_0

    .line 987
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 1019
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_6
    .end packed-switch
.end method

.method public openDrawer(Landroid/view/View;)V
    .locals 4
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 1152
    invoke-virtual {p0, p1}, Landroid/support/v4/widget/DrawerLayoutMW;->isDrawerView(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1153
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "View "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not a sliding drawer"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1156
    :cond_0
    iget-boolean v1, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mFirstLayout:Z

    if-eqz v1, :cond_1

    .line 1157
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    .line 1158
    .local v0, "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->onScreen:F

    .line 1159
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->knownOpen:Z

    .line 1168
    .end local v0    # "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->invalidate()V

    .line 1169
    return-void

    .line 1161
    :cond_1
    const/4 v1, 0x3

    invoke-virtual {p0, p1, v1}, Landroid/support/v4/widget/DrawerLayoutMW;->checkDrawerViewGravity(Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1162
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v1, p1, v2, v3}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    goto :goto_0

    .line 1164
    :cond_2
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v1, p1, v2, v3}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    goto :goto_0
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 3
    .param p1, "disallowIntercept"    # Z

    .prologue
    const/4 v2, 0x1

    .line 1093
    iget-boolean v0, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mbDrawerSkip:Z

    if-eqz v0, :cond_1

    .line 1105
    :cond_0
    :goto_0
    return-void

    .line 1096
    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/ViewDragHelper;->isEdgeTouched(I)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightDragger:Landroid/support/v4/widget/ViewDragHelper;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/ViewDragHelper;->isEdgeTouched(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1099
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 1101
    :cond_2
    iput-boolean p1, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDisallowInterceptRequested:Z

    .line 1102
    if-eqz p1, :cond_0

    .line 1103
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->closeDrawers(Z)V

    goto :goto_0
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 802
    iget-boolean v0, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mInLayout:Z

    if-nez v0, :cond_0

    .line 803
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 805
    :cond_0
    return-void
.end method

.method public setDrawerListener(Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;

    .prologue
    .line 345
    iput-object p1, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mListener:Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;

    .line 346
    return-void
.end method

.method public setDrawerLockMode(I)V
    .locals 1
    .param p1, "lockMode"    # I

    .prologue
    .line 362
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/widget/DrawerLayoutMW;->setDrawerLockMode(II)V

    .line 363
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/widget/DrawerLayoutMW;->setDrawerLockMode(II)V

    .line 364
    return-void
.end method

.method public setDrawerLockMode(II)V
    .locals 6
    .param p1, "lockMode"    # I
    .param p2, "edgeGravity"    # I

    .prologue
    const/4 v5, 0x3

    .line 386
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->getLayoutDirection(Landroid/view/View;)I

    move-result v4

    invoke-static {p2, v4}, Landroid/support/v4/view/GravityCompat;->getAbsoluteGravity(II)I

    move-result v0

    .line 388
    .local v0, "absGrav":I
    if-ne v0, v5, :cond_3

    .line 389
    iput p1, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLockModeLeft:I

    .line 393
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    .line 395
    if-ne v0, v5, :cond_4

    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    .line 396
    .local v1, "helper":Landroid/support/v4/widget/ViewDragHelper;
    :goto_1
    invoke-virtual {v1}, Landroid/support/v4/widget/ViewDragHelper;->cancel()V

    .line 398
    .end local v1    # "helper":Landroid/support/v4/widget/ViewDragHelper;
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 413
    :cond_2
    :goto_2
    return-void

    .line 390
    :cond_3
    const/4 v4, 0x5

    if-ne v0, v4, :cond_0

    .line 391
    iput p1, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLockModeRight:I

    goto :goto_0

    .line 395
    :cond_4
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightDragger:Landroid/support/v4/widget/ViewDragHelper;

    goto :goto_1

    .line 400
    :pswitch_0
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayoutMW;->findDrawerWithGravity(I)Landroid/view/View;

    move-result-object v3

    .line 401
    .local v3, "toOpen":Landroid/view/View;
    if-eqz v3, :cond_2

    .line 402
    invoke-virtual {p0, v3}, Landroid/support/v4/widget/DrawerLayoutMW;->openDrawer(Landroid/view/View;)V

    goto :goto_2

    .line 406
    .end local v3    # "toOpen":Landroid/view/View;
    :pswitch_1
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayoutMW;->findDrawerWithGravity(I)Landroid/view/View;

    move-result-object v2

    .line 407
    .local v2, "toClose":Landroid/view/View;
    if-eqz v2, :cond_2

    .line 408
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->closeDrawer(Landroid/view/View;)V

    goto :goto_2

    .line 398
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method setDrawerViewOffset(Landroid/view/View;F)V
    .locals 2
    .param p1, "drawerView"    # Landroid/view/View;
    .param p2, "slideOffset"    # F

    .prologue
    .line 555
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    .line 556
    .local v0, "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    iget v1, v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->onScreen:F

    cmpl-float v1, p2, v1

    if-nez v1, :cond_0

    .line 562
    :goto_0
    return-void

    .line 560
    :cond_0
    iput p2, v0, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->onScreen:F

    .line 561
    invoke-virtual {p0, p1, p2}, Landroid/support/v4/widget/DrawerLayoutMW;->dispatchOnDrawerSlide(Landroid/view/View;F)V

    goto :goto_0
.end method

.method public setScrimColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 334
    iput p1, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mScrimColor:I

    .line 335
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayoutMW;->invalidate()V

    .line 336
    return-void
.end method

.method updateDrawerState(IILandroid/view/View;)V
    .locals 8
    .param p1, "forGravity"    # I
    .param p2, "activeState"    # I
    .param p3, "activeDrawer"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x2

    const/4 v4, 0x1

    .line 482
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mLeftDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v5}, Landroid/support/v4/widget/ViewDragHelper;->getViewDragState()I

    move-result v0

    .line 483
    .local v0, "leftState":I
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mRightDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v5}, Landroid/support/v4/widget/ViewDragHelper;->getViewDragState()I

    move-result v2

    .line 486
    .local v2, "rightState":I
    if-eq v0, v4, :cond_0

    if-ne v2, v4, :cond_3

    .line 487
    :cond_0
    const/4 v3, 0x1

    .line 493
    .local v3, "state":I
    :goto_0
    if-eqz p3, :cond_1

    if-nez p2, :cond_1

    .line 494
    invoke-virtual {p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    .line 495
    .local v1, "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    iget v5, v1, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->onScreen:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-nez v5, :cond_6

    .line 496
    invoke-virtual {p0, p3}, Landroid/support/v4/widget/DrawerLayoutMW;->dispatchOnDrawerClosed(Landroid/view/View;)V

    .line 502
    .end local v1    # "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    :cond_1
    :goto_1
    iget v5, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDrawerState:I

    if-eq v3, v5, :cond_2

    .line 503
    iput v3, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mDrawerState:I

    .line 504
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "user_setup_complete"

    const/4 v7, -0x2

    invoke-static {v5, v6, v4, v7}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v5

    if-ne v5, v4, :cond_7

    .line 506
    .local v4, "userSetupComplete":Z
    :goto_2
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mListener:Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;

    if-eqz v5, :cond_2

    if-eqz v4, :cond_2

    .line 507
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayoutMW;->mListener:Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;

    invoke-interface {v5, v3}, Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;->onDrawerStateChanged(I)V

    .line 510
    .end local v4    # "userSetupComplete":Z
    :cond_2
    return-void

    .line 488
    .end local v3    # "state":I
    :cond_3
    if-eq v0, v6, :cond_4

    if-ne v2, v6, :cond_5

    .line 489
    :cond_4
    const/4 v3, 0x2

    .restart local v3    # "state":I
    goto :goto_0

    .line 491
    .end local v3    # "state":I
    :cond_5
    const/4 v3, 0x0

    .restart local v3    # "state":I
    goto :goto_0

    .line 497
    .restart local v1    # "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    :cond_6
    iget v5, v1, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->onScreen:F

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v5, v5, v6

    if-nez v5, :cond_1

    .line 498
    invoke-virtual {p0, p3}, Landroid/support/v4/widget/DrawerLayoutMW;->dispatchOnDrawerOpened(Landroid/view/View;)V

    goto :goto_1

    .line 504
    .end local v1    # "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    :cond_7
    const/4 v4, 0x0

    goto :goto_2
.end method
