.class public Lcom/samsung/samm/spenscrap/ClipImageLibrary;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static makeImageShadow(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/16 v5, 0x64

    .line 355
    if-nez p0, :cond_0

    .line 356
    const/4 v1, 0x0

    .line 377
    :goto_0
    return-object v1

    .line 358
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 359
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 361
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v3, v3, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 362
    new-instance v3, Landroid/graphics/Rect;

    add-int/2addr v0, p2

    add-int/2addr v1, p3

    invoke-direct {v3, p2, p3, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 365
    iget v0, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, p1

    iput v0, v3, Landroid/graphics/Rect;->top:I

    .line 366
    iget v0, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, p1

    iput v0, v3, Landroid/graphics/Rect;->bottom:I

    .line 367
    iget v0, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, p1

    iput v0, v3, Landroid/graphics/Rect;->left:I

    .line 368
    iget v0, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, p1

    iput v0, v3, Landroid/graphics/Rect;->right:I

    .line 370
    invoke-virtual {v3, v2}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 373
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v6, v5

    move v7, v5

    .line 375
    invoke-static/range {v0 .. v7}, Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngine;->makeImageShadow(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIIIII)Z

    goto :goto_0
.end method
