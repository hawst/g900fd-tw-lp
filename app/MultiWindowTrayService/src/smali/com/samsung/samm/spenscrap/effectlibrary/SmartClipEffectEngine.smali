.class public Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngine;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 216
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 217
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "google_sdk"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "sdk"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    .line 218
    :cond_0
    const-string v0, "SAMSUNG_SAMMFORMAT"

    .line 235
    :goto_0
    return-object v0

    .line 223
    :cond_1
    sget-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 224
    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 225
    if-eqz v2, :cond_2

    if-nez v3, :cond_3

    .line 226
    :cond_2
    const-string v1, "SmartClipEffectEngine"

    const-string v2, "Unknown Brand/Manufacturer Device"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 229
    :cond_3
    sget-object v4, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 230
    const-string v5, "Samsung"

    invoke-virtual {v2, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "Samsung"

    invoke-virtual {v3, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_4

    .line 231
    const-string v5, "SmartClipEffectEngine"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Device("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "), Model("

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "), Brand("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), Manufacturer("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is not a Saumsung device."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 235
    :cond_4
    const-string v0, "SAMSUNG_SAMMFORMAT"

    goto :goto_0
.end method

.method public static makeImageShadow(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIIIII)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/16 v2, 0xff

    .line 28
    invoke-static {}, Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngine;->setCheckSupportingModel()Z

    move-result v1

    if-nez v1, :cond_1

    .line 36
    :cond_0
    :goto_0
    return v0

    .line 31
    :cond_1
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-lez p2, :cond_0

    if-ltz p5, :cond_0

    if-ltz p6, :cond_0

    if-ltz p7, :cond_0

    .line 32
    if-gt p5, v2, :cond_0

    if-gt p6, v2, :cond_0

    if-gt p7, v2, :cond_0

    .line 34
    invoke-static/range {p0 .. p7}, Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngineJNI;->makeImageShadow(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIIIII)V

    .line 36
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static setCheckSupportingModel()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 199
    invoke-static {}, Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngine;->a()Ljava/lang/String;

    move-result-object v1

    .line 200
    if-nez v1, :cond_1

    .line 208
    :cond_0
    :goto_0
    return v0

    .line 202
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 203
    if-eqz v1, :cond_0

    .line 206
    array-length v0, v1

    invoke-static {v1, v0}, Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngineJNI;->setCheckSupportingModel([CI)V

    .line 208
    const/4 v0, 0x1

    goto :goto_0
.end method
