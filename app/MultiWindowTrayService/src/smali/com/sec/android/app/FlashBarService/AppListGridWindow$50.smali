.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$50;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 3716
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$50;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 3718
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 3732
    :cond_0
    :goto_0
    return v1

    .line 3722
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$50;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->cancelCollapseTimer()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    .line 3723
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$50;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$50;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3724
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$50;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dismissHistoryWindow(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)V

    goto :goto_0

    .line 3729
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$50;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startCollapseTimer()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    goto :goto_0

    .line 3718
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
