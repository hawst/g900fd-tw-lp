.class Lcom/sec/android/app/FlashBarService/SwitchView$4;
.super Ljava/lang/Object;
.source "SwitchView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/SwitchView;->animateBack()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/SwitchView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SwitchView;)V
    .locals 0

    .prologue
    .line 579
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$4;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 601
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$4;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    sget-object v1, Lcom/sec/android/app/FlashBarService/SwitchView$State;->NONE:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    # setter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mState:Lcom/sec/android/app/FlashBarService/SwitchView$State;
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1002(Lcom/sec/android/app/FlashBarService/SwitchView;Lcom/sec/android/app/FlashBarService/SwitchView$State;)Lcom/sec/android/app/FlashBarService/SwitchView$State;

    .line 602
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 592
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$4;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    sget-object v1, Lcom/sec/android/app/FlashBarService/SwitchView$State;->NONE:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    # setter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mState:Lcom/sec/android/app/FlashBarService/SwitchView$State;
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1002(Lcom/sec/android/app/FlashBarService/SwitchView;Lcom/sec/android/app/FlashBarService/SwitchView$State;)Lcom/sec/android/app/FlashBarService/SwitchView$State;

    .line 593
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$4;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mHideRequested:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$200(Lcom/sec/android/app/FlashBarService/SwitchView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$4;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateZoomIn()V

    .line 596
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$4;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedView:Landroid/widget/ImageView;
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1402(Lcom/sec/android/app/FlashBarService/SwitchView;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 597
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 606
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 582
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$4;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mHideRequested:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$200(Lcom/sec/android/app/FlashBarService/SwitchView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 583
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$4;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateTouchUnlock()V

    .line 584
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$4;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1300(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 585
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$4;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$4;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1300(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateBorderOut(I)V

    .line 588
    :cond_0
    return-void
.end method
