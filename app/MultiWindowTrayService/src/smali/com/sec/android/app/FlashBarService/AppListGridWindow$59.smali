.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;->hideAppEditList(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 4446
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 9
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v8, 0x1

    const v7, 0x7f080001

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 4448
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeAppListEditWindow()V

    .line 4449
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->initAppListEditWindow()V

    .line 4451
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200bb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 4452
    .local v0, "add":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0029

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v2, v3

    .line 4453
    .local v2, "paddingTop":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a002a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v1, v3

    .line 4455
    .local v1, "paddingBottom":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v6, v0, v6, v6}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 4456
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn_h:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v6, v0, v6, v6}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 4457
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v5, v2, v5, v1}, Landroid/widget/Button;->setPadding(IIII)V

    .line 4458
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn_h:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v5, v2, v5, v1}, Landroid/widget/Button;->setPadding(IIII)V

    .line 4459
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/Button;->setEnabled(Z)V

    .line 4460
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn_h:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/Button;->setEnabled(Z)V

    .line 4461
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 4462
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn_h:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 4463
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/TextView;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 4464
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4465
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditBtn:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v3

    const v4, 0x7f020034

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 4468
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v4, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I
    invoke-static {v3, v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4002(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 4469
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v3, v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3902(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 4470
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListRelayout(Z)V

    .line 4471
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iput-boolean v5, v3, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbAnimating:Z

    .line 4472
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->hideAppEditPost()V

    .line 4473
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListHorizontal:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->updateAppListForScrollView(Ljava/util/List;Z)V

    .line 4474
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListVertical:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->updateAppListForScrollView(Ljava/util/List;Z)V

    .line 4476
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 4477
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 4479
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbAnimating:Z

    .line 4480
    return-void
.end method
