.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 4732
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 13
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v12, 0x0

    const/4 v8, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v9, -0x1

    .line 4734
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    .line 4736
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 4818
    const-string v6, "DragDrop"

    const-string v7, "Unknown action type received by OnDragListener."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4821
    :cond_0
    :goto_0
    :pswitch_0
    return v10

    .line 4741
    :pswitch_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildCount()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dragEnteredChildCount:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13902(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 4742
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListDragMode:Z
    invoke-static {v6, v10}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6202(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 4743
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTopGuideline:Landroid/widget/FrameLayout;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/FrameLayout;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4744
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBottomGuideline:Landroid/widget/FrameLayout;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/FrameLayout;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4745
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    move-result-object v6

    invoke-virtual {v6, v8}, Lcom/sec/android/app/FlashBarService/GuideLineImageView;->setVisibility(I)V

    .line 4746
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 4750
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v6

    float-to-int v5, v6

    .line 4751
    .local v5, "touchY":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0051

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 4753
    .local v4, "scrollDownArea":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ScrollView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/ScrollView;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v6, v4

    if-le v5, v6, :cond_1

    .line 4754
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ScrollView;

    move-result-object v6

    const/16 v7, 0xc

    invoke-virtual {v6, v12, v7}, Landroid/widget/ScrollView;->scrollBy(II)V

    .line 4757
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddEmptyItemPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    if-ne v6, v9, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 4758
    const/4 v3, -0x1

    .line 4759
    .local v3, "position":I
    const/4 v1, 0x0

    .line 4760
    .local v1, "item_tail":Landroid/view/View;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildCount()I

    move-result v6

    if-lez v6, :cond_3

    .line 4761
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dragEnteredChildCount:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    add-int/lit8 v3, v6, -0x1

    .line 4766
    :goto_1
    if-eq v3, v9, :cond_0

    .line 4767
    const/4 v2, 0x0

    .line 4768
    .local v2, "item_v":Landroid/view/View;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 4769
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v7

    const/4 v8, 0x2

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->changlistItemFromEditList(III)Z
    invoke-static {v6, v3, v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;III)Z

    .line 4770
    if-nez v3, :cond_4

    .line 4771
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v6

    invoke-virtual {v6, v12}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 4776
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddEmptyItemPosition:I
    invoke-static {v6, v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6702(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 4779
    :cond_2
    if-eqz v2, :cond_0

    .line 4780
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v6, v11}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 4781
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v6, v11}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 4763
    .end local v2    # "item_v":Landroid/view/View;
    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    .line 4773
    .restart local v2    # "item_v":Landroid/view/View;
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 4774
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->decideScrollMove(Landroid/view/View;Landroid/view/DragEvent;)Z
    invoke-static {v6, v1, p2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/view/View;Landroid/view/DragEvent;)Z

    goto :goto_2

    .line 4789
    .end local v1    # "item_tail":Landroid/view/View;
    .end local v2    # "item_v":Landroid/view/View;
    .end local v3    # "position":I
    .end local v4    # "scrollDownArea":I
    .end local v5    # "touchY":I
    :pswitch_3
    const-string v6, "AppListBezelWindow"

    const-string v7, "BG List Drag Exited"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4790
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 4791
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v6, v9}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 4792
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I
    invoke-static {v6, v9}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5602(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 4793
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddEmptyItemPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    if-eq v6, v9, :cond_5

    .line 4794
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddEmptyItemPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->changlistItemFromEditList(III)Z
    invoke-static {v6, v7, v8, v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;III)Z

    .line 4795
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddEmptyItemPosition:I
    invoke-static {v6, v9}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6702(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 4798
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v6, v10}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListRelayout(Z)V

    goto/16 :goto_0

    .line 4801
    :pswitch_4
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListDragMode:Z
    invoke-static {v6, v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6202(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 4802
    const-string v6, "AppListBezelWindow"

    const-string v7, "Drag Drop"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4803
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 4804
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIvt:[B
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)[B

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v8}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 4805
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddEmptyItemPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->addAndChangelistItem(II)Z
    invoke-static {v6, v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;II)Z

    .line 4806
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddEmptyItemPosition:I
    invoke-static {v6, v9}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6702(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 4807
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setItemIndexFromAppList(I)V

    .line 4808
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v6, v10}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListRelayout(Z)V

    .line 4809
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->updateEditListChanged()V

    .line 4812
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v6, v11}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 4813
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v6, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 4814
    :cond_7
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startCollapseTimer()V
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    goto/16 :goto_0

    .line 4736
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
