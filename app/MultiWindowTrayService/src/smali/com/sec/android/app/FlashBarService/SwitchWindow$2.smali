.class Lcom/sec/android/app/FlashBarService/SwitchWindow$2;
.super Landroid/os/AsyncTask;
.source "SwitchWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/SwitchWindow;->show()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/SwitchWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SwitchWindow;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 90
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/SwitchWindow;->loadScreenshots()Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->access$000(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 92
    .local v0, "b":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 87
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/FlashBarService/SwitchWindow$2;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchWindow;->mHideRequested:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->access$100(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/SwitchWindow;->mHideRequested:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->access$102(Lcom/sec/android/app/FlashBarService/SwitchWindow;Z)Z

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchWindow;->mCallback:Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->access$200(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchWindow;->mCallback:Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->access$200(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;->onHide()V

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchWindow;->mSwitchView:Lcom/sec/android/app/FlashBarService/SwitchView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->access$300(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Lcom/sec/android/app/FlashBarService/SwitchView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/SwitchView;->setImage(Landroid/graphics/Bitmap;)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchWindow;->mViewVisible:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->access$400(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchWindow;->mWindowManager:Landroid/view/WindowManager;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->access$600(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Landroid/view/WindowManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchWindow;->mSwitchView:Lcom/sec/android/app/FlashBarService/SwitchView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->access$300(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Lcom/sec/android/app/FlashBarService/SwitchView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchWindow;->mLp:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->access$500(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/SwitchWindow;->mViewVisible:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->access$402(Lcom/sec/android/app/FlashBarService/SwitchWindow;Z)Z

    .line 109
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchWindow;->mSwitchView:Lcom/sec/android/app/FlashBarService/SwitchView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->access$300(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Lcom/sec/android/app/FlashBarService/SwitchView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->prepareAnimation()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 87
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/FlashBarService/SwitchWindow$2;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
