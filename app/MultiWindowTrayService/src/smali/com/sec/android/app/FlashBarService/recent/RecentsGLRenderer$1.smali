.class Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$1;
.super Ljava/lang/Object;
.source "RecentsGLRenderer.java"

# interfaces
.implements Landroid/animation/TimeAnimator$TimeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeUpdate(Landroid/animation/TimeAnimator;JJ)V
    .locals 5
    .param p1, "timeAnimator"    # Landroid/animation/TimeAnimator;
    .param p2, "l"    # J
    .param p4, "l2"    # J

    .prologue
    .line 88
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScroller:Landroid/widget/Scroller;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->access$000(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)Landroid/widget/Scroller;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Scroller;->isFinished()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScroller:Landroid/widget/Scroller;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->access$000(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)Landroid/widget/Scroller;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 89
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->access$100(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)F

    move-result v1

    .line 90
    .local v1, "oldPos":F
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScroller:Landroid/widget/Scroller;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->access$000(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)Landroid/widget/Scroller;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrX()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollMultiplier:F
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->access$200(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)F

    move-result v4

    div-float/2addr v3, v4

    # setter for: Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->access$102(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;F)F

    .line 91
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    # invokes: Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->adjustPosition()Z
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->access$300(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)Z

    move-result v0

    .line 92
    .local v0, "adjusted":Z
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->access$100(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)F

    move-result v2

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_1

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    # invokes: Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->layoutTasks()V
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->access$400(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)V

    .line 102
    .end local v0    # "adjusted":Z
    .end local v1    # "oldPos":F
    :cond_0
    :goto_0
    return-void

    .line 94
    .restart local v0    # "adjusted":Z
    .restart local v1    # "oldPos":F
    :cond_1
    if-eqz v0, :cond_0

    .line 95
    const-string v2, "MIK"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Finishing scroll, old = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and new = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->access$100(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScroller:Landroid/widget/Scroller;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->access$000(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)Landroid/widget/Scroller;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Scroller;->forceFinished(Z)V

    goto :goto_0

    .line 99
    .end local v0    # "adjusted":Z
    .end local v1    # "oldPos":F
    :cond_2
    const-string v2, "MIK"

    const-string v3, "mFlingAnimator stopped"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mFlingAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v2}, Landroid/animation/TimeAnimator;->cancel()V

    goto :goto_0
.end method
