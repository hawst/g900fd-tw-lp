.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$6;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;->makeHistoryBarDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 833
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 837
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 838
    .local v0, "taskId":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mActivityManager:Landroid/app/ActivityManager;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/ActivityManager;->removeTask(II)Z

    .line 839
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->setRemoveTask(I)V

    .line 840
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v2, 0xcf

    const-wide/16 v4, 0x320

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 841
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dismissHistoryWindow(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)V

    .line 842
    return-void
.end method
