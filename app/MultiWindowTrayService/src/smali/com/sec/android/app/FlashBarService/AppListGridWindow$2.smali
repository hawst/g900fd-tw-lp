.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$2;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;->setWindow(Landroid/view/Window;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 494
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSystemUiVisibilityChange(I)V
    .locals 4
    .param p1, "visibility"    # I

    .prologue
    .line 496
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dismissTemplateDialog()V

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iput p1, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mSystemUiVisibility:I

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mStartFlashBar:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v3

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListPosition(IZZ)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;IZZ)V

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->displayAppListEditWindow(IZ)V

    .line 502
    :cond_0
    return-void
.end method
