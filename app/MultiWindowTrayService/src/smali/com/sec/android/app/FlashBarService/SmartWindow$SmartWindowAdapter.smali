.class Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;
.super Landroid/widget/BaseAdapter;
.source "SmartWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/SmartWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SmartWindowAdapter"
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field private mAppListSmart:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/FlashBarService/SmartWindow;Landroid/content/Context;)V
    .locals 1
    .param p2, "c"    # Landroid/content/Context;

    .prologue
    .line 480
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 476
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->context:Landroid/content/Context;

    .line 478
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->mAppListSmart:Ljava/util/List;

    .line 481
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->context:Landroid/content/Context;

    .line 482
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 483
    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {p1}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$400(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppListForSmartWindow()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->mAppListSmart:Ljava/util/List;

    .line 484
    return-void
.end method


# virtual methods
.method public createView(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 496
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030005

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 497
    .local v0, "convertView":Landroid/view/View;
    new-instance v1, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;

    invoke-direct {v1}, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;-><init>()V

    .line 499
    .local v1, "holder":Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;
    const v2, 0x7f0f0059

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;->iconView:Landroid/widget/ImageView;

    .line 500
    const v2, 0x7f0f005c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;->labelView:Landroid/widget/TextView;

    move-object v2, v0

    .line 501
    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;->contentView:Landroid/widget/LinearLayout;

    .line 502
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowItemTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 503
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowItemDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 504
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 506
    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->mAppListSmart:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 491
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->mAppListSmart:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 609
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 14
    .param p1, "pos"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 511
    if-nez p2, :cond_0

    .line 512
    move-object/from16 v0, p3

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->createView(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p2

    .line 513
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 514
    new-instance v10, Ljava/lang/RuntimeException;

    const-string v11, "Recycled child has parent"

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 516
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 517
    new-instance v10, Ljava/lang/RuntimeException;

    const-string v11, "Recycled child has parent"

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 520
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;

    .line 522
    .local v3, "holder":Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v10}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$400(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, p1, v11}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getSmartWindowItemByIndex(IZ)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v7

    .line 523
    .local v7, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v6

    .line 524
    .local v6, "label":Ljava/lang/CharSequence;
    iget-object v10, v3, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;->labelView:Landroid/widget/TextView;

    invoke-virtual {v10, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 525
    iget-object v10, v3, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;->labelView:Landroid/widget/TextView;

    const v11, -0xb8b8b9

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setTextColor(I)V

    .line 526
    iget-object v10, v3, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v10, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 527
    iget-object v10, v3, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 528
    move v5, p1

    .line 530
    .local v5, "id":I
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->mAppListSmart:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-ge p1, v10, :cond_2

    .line 531
    new-instance v10, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;

    invoke-direct {v10, p0, v5}, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;-><init>(Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;I)V

    invoke-virtual {v3, v10}, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 588
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->mAppListSmart:Ljava/util/List;

    invoke-interface {v10, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    invoke-virtual {v10}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 589
    .local v4, "iconDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v10

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v11

    sget-object v12, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v10, v11, v12}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 590
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 591
    .local v2, "canvas":Landroid/graphics/Canvas;
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v12

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v13

    invoke-virtual {v4, v10, v11, v12, v13}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 592
    invoke-virtual {v4, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 593
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a0148

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 594
    .local v8, "shadowOfIconY":I
    const/4 v10, 0x3

    const/4 v11, 0x0

    invoke-static {v1, v10, v11, v8}, Lcom/samsung/samm/spenscrap/ClipImageLibrary;->makeImageShadow(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 596
    .local v9, "shadowedBitmap":Landroid/graphics/Bitmap;
    iget-object v10, v3, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v10, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 597
    iget-object v10, v3, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;->iconView:Landroid/widget/ImageView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 598
    iget-object v11, v3, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;->labelView:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->mAppListSmart:Ljava/util/List;

    invoke-interface {v10, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    invoke-virtual {v10}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 599
    iget-object v10, v3, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;->labelView:Landroid/widget/TextView;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 601
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 602
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->mAppListSmart:Ljava/util/List;

    invoke-interface {v10, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    invoke-virtual {v10}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 604
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "canvas":Landroid/graphics/Canvas;
    .end local v4    # "iconDrawable":Landroid/graphics/drawable/Drawable;
    .end local v8    # "shadowOfIconY":I
    .end local v9    # "shadowedBitmap":Landroid/graphics/Bitmap;
    :cond_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 605
    return-object p2
.end method
