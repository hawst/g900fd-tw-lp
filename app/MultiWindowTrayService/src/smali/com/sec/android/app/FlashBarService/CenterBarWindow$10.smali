.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$10;
.super Ljava/lang/Object;
.source "CenterBarWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;->makeButtonPopupLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0

    .prologue
    .line 1689
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$10;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x0

    .line 1692
    invoke-virtual {p1, p2}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1693
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_2

    .line 1694
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$10;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarButtonsContainer:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/RippleDrawable;

    .line 1695
    .local v1, "ripple":Landroid/graphics/drawable/RippleDrawable;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$10;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0179

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1696
    .local v2, "rippleSize":I
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    add-int/2addr v3, v2

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v4

    if-le v3, v4, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v0

    .line 1697
    .local v0, "layoutRight":I
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {v1, v3, v4, v0, v5}, Landroid/graphics/drawable/RippleDrawable;->setHotspotBounds(IIII)V

    .line 1698
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$10;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarButtonsContainer:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    add-float/2addr v4, v5

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    add-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/widget/LinearLayout;->drawableHotspotChanged(FF)V

    .line 1699
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$10;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarButtonsContainer:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/widget/LinearLayout;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setPressed(Z)V

    .line 1704
    .end local v0    # "layoutRight":I
    .end local v1    # "ripple":Landroid/graphics/drawable/RippleDrawable;
    .end local v2    # "rippleSize":I
    :cond_0
    :goto_1
    return v7

    .line 1696
    .restart local v1    # "ripple":Landroid/graphics/drawable/RippleDrawable;
    .restart local v2    # "rippleSize":I
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    add-int v0, v3, v2

    goto :goto_0

    .line 1700
    .end local v1    # "ripple":Landroid/graphics/drawable/RippleDrawable;
    .end local v2    # "rippleSize":I
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 1702
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$10;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarButtonsContainer:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setPressed(Z)V

    goto :goto_1
.end method
