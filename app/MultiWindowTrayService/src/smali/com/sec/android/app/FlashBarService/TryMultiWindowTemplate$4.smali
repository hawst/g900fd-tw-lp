.class Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$4;
.super Ljava/lang/Object;
.source "TryMultiWindowTemplate.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->animationTemplateIconMake()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$4;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 358
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$4;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowAppPaired:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$4;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup4:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$4;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup4:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$4;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$4;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mHandle:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$800(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$4;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mFinishRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 362
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 352
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 355
    return-void
.end method
