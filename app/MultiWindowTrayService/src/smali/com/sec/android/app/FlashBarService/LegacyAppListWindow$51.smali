.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$51;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->createResetConfirmDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 3743
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$51;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 3746
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$51;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->resetMultiWindowTray()V

    .line 3747
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$51;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListRelayout(Z)V

    .line 3748
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$51;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissResetConfirmDialog()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$10400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    .line 3749
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$51;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->updateAppListEditContents()V

    .line 3750
    return-void
.end method
