.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;
.super Ljava/lang/Object;
.source "CenterBarWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0

    .prologue
    .line 1274
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v11, 0xce

    const/16 v10, 0xcd

    const/16 v9, 0x1e

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1278
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsCenterBarFixed:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1400(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1400
    :cond_0
    :goto_0
    return v3

    .line 1282
    :cond_1
    invoke-static {}, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;->getSealedState()Z

    move-result v5

    if-eqz v5, :cond_2

    const/16 v5, 0x1b9

    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;->getKnoxSealedMultiWindowFixedState(I)I

    move-result v5

    if-ne v5, v3, :cond_2

    move v3, v4

    .line 1284
    goto :goto_0

    .line 1288
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarGestureDetector:Landroid/view/GestureDetector;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1500(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/view/GestureDetector;

    move-result-object v5

    invoke-virtual {v5, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v3, v4

    .line 1290
    goto :goto_0

    .line 1293
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v5

    float-to-int v1, v5

    .line 1294
    .local v1, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    float-to-int v2, v5

    .line 1296
    .local v2, "y":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-nez v5, :cond_4

    .line 1297
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsInLongPress:Z
    invoke-static {v5, v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1602(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 1299
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayId:I
    invoke-static {v5, v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1702(Lcom/sec/android/app/FlashBarService/CenterBarWindow;I)I

    .line 1301
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsCenterBarPressed:Z
    invoke-static {v5, v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1802(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 1302
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsCenterBarMoved:Z
    invoke-static {v5, v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1902(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 1303
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchStartX:I
    invoke-static {v5, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2002(Lcom/sec/android/app/FlashBarService/CenterBarWindow;I)I

    .line 1304
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchStartY:I
    invoke-static {v5, v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2102(Lcom/sec/android/app/FlashBarService/CenterBarWindow;I)I

    .line 1305
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isSwitchWindowActive()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1308
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getArrangeState()I

    move-result v6

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCurrentArrangeState:I
    invoke-static {v5, v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2302(Lcom/sec/android/app/FlashBarService/CenterBarWindow;I)I

    .line 1309
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Point;->y:I

    # invokes: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawGuideCenterBar(II)V
    invoke-static {v5, v6, v7}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2400(Lcom/sec/android/app/FlashBarService/CenterBarWindow;II)V

    .line 1311
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mStartCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2500(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Point;->y:I

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Point;->set(II)V

    .line 1312
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mActionCancel:Z
    invoke-static {v5, v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2602(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    goto/16 :goto_0

    .line 1313
    :cond_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-eq v5, v3, :cond_5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_f

    .line 1314
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsCrossRect:Z
    invoke-static {v5, v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2702(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 1315
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsCenterBarPressed:Z
    invoke-static {v5, v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1802(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 1316
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideGuideCenterBar()V

    .line 1317
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDoubleTabbed:Z
    invoke-static {v5, v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2802(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 1319
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mActionCancel:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2600(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1320
    const/4 v0, 0x0

    .line 1321
    .local v0, "point":Landroid/graphics/Point;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getCenterBarPoint(I)Landroid/graphics/Point;

    move-result-object v0

    .line 1322
    if-eqz v0, :cond_6

    .line 1323
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v5

    iget v6, v0, Landroid/graphics/Point;->x:I

    iput v6, v5, Landroid/graphics/Point;->x:I

    .line 1324
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v5

    iget v6, v0, Landroid/graphics/Point;->y:I

    iput v6, v5, Landroid/graphics/Point;->y:I

    .line 1326
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Point;->y:I

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawCenterBar(II)V

    .line 1373
    .end local v0    # "point":Landroid/graphics/Point;
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mActionCancel:Z
    invoke-static {v5, v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2602(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 1374
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v4, v11}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1375
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v4, v11}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 1329
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    new-instance v6, Landroid/graphics/Point;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v8

    float-to-int v8, v8

    invoke-direct {v6, v7, v8}, Landroid/graphics/Point;-><init>(II)V

    # invokes: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getCenterBarEdgeArea(Landroid/graphics/Point;)Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;
    invoke-static {v5, v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Landroid/graphics/Point;)Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;->NONE:Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;

    if-eq v5, v6, :cond_8

    .line 1330
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->centerBarFullSizeDocking(Landroid/view/MotionEvent;)Z
    invoke-static {v5, p2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1331
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->findCandidateFocus()I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3100(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->setFocusAppByZone(I)V

    .line 1332
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchStartX:I
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-ge v5, v9, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchStartY:I
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2100(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-ge v5, v9, :cond_0

    .line 1339
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->checkFixedBoundsForDimLayer()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1340
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->findCandidateFocus()I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3100(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->setFocusAppByZone(I)V

    .line 1343
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchStartX:I
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v5

    sub-int/2addr v5, v1

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-ge v5, v9, :cond_d

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchStartY:I
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2100(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v5

    sub-int/2addr v5, v2

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-ge v5, v9, :cond_d

    .line 1345
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsShowButton:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 1346
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v5, v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideButtonsPopupWindow(Z)V

    .line 1348
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mStartCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2500(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mStartCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2500(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Point;->y:I

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawCenterBar(II)V

    .line 1359
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mStartCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2500(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Point;->x:I

    iput v6, v5, Landroid/graphics/Point;->x:I

    .line 1360
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mStartCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2500(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Point;->y:I

    iput v6, v5, Landroid/graphics/Point;->y:I

    goto/16 :goto_1

    .line 1349
    :cond_a
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isSwitchWindowActive()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 1350
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->hideSwitchWindow()V

    goto :goto_2

    .line 1352
    :cond_b
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v5, v10}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 1353
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v5, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 1355
    :cond_c
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-static {v6, v10}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v6

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->DOUBLE_TAP_MIN_TIME:I
    invoke-static {}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3300()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1357
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mStartCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2500(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mStartCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2500(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Point;->y:I

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawCenterBar(II)V

    goto :goto_2

    .line 1362
    :cond_d
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isSwitchWindowActive()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1366
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v5, v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->centerControlBarDocking(Z)Z

    .line 1367
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mStartCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2500(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_e

    .line 1368
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->setCenterBarPoint(ILandroid/graphics/Point;)V

    .line 1370
    :cond_e
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Point;->y:I

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawCenterBar(II)V

    goto/16 :goto_1

    .line 1377
    :cond_f
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 1378
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isSwitchWindowActive()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1382
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsInLongPress:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1600(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1386
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchStartX:I
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v5

    sub-int/2addr v5, v1

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-gt v5, v9, :cond_10

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchStartY:I
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2100(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v5

    sub-int/2addr v5, v2

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-gt v5, v9, :cond_10

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsCenterBarMoved:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1388
    :cond_10
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsCenterBarMoved:Z
    invoke-static {v5, v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1902(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 1389
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v6

    float-to-int v6, v6

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I
    invoke-static {v5, v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$002(Lcom/sec/android/app/FlashBarService/CenterBarWindow;I)I

    .line 1390
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    float-to-int v6, v6

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I
    invoke-static {v5, v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$102(Lcom/sec/android/app/FlashBarService/CenterBarWindow;I)I

    .line 1391
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->setCenterBarPoint()V
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    .line 1392
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v5, v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideButtonsPopupWindow(Z)V

    .line 1393
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->setCenterBarViewsVisibility(I)V

    .line 1396
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Point;->x:I

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Point;->y:I

    # invokes: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawGuideCenterBar(II)V
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2400(Lcom/sec/android/app/FlashBarService/CenterBarWindow;II)V

    goto/16 :goto_0
.end method
