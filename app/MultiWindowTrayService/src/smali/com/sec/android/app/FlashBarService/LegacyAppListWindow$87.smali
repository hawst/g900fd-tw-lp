.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 5270
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 15
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 5272
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v1

    .line 5274
    .local v1, "action":I
    packed-switch v1, :pswitch_data_0

    .line 5372
    const-string v11, "DragDrop"

    const-string v12, "Unknown action type received by OnDragListener."

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5375
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v11, 0x1

    return v11

    .line 5279
    :pswitch_1
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v12, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListDragMode:Z
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6802(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 5280
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideVisibility(ZZZ)V
    invoke-static {v11, v12, v13, v14}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ZZZ)V

    .line 5281
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    move-result-object v11

    const/4 v12, 0x4

    invoke-virtual {v11, v12}, Lcom/sec/android/app/FlashBarService/GuideLineImageView;->setVisibility(I)V

    .line 5282
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x4

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 5286
    :pswitch_2
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListDragMode:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_0

    .line 5289
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddEmptyItemPosition:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v11

    const/4 v12, -0x1

    if-ne v11, v12, :cond_0

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 5290
    const/4 v7, -0x1

    .line 5291
    .local v7, "position":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getY()F

    move-result v11

    float-to-int v10, v11

    .line 5294
    .local v10, "touchY":I
    const/4 v3, 0x0

    .line 5295
    .local v3, "item_head":Landroid/view/View;
    const/4 v4, 0x0

    .line 5296
    .local v4, "item_tail":Landroid/view/View;
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/LinearLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v11

    if-lez v11, :cond_4

    .line 5297
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/LinearLayout;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 5298
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/LinearLayout;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/LinearLayout;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 5299
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    .line 5300
    .local v8, "r":Landroid/graphics/Rect;
    invoke-virtual {v3, v8}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 5301
    iget v2, v8, Landroid/graphics/Rect;->top:I

    .line 5302
    .local v2, "headPosition":I
    invoke-virtual {v4, v8}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 5303
    iget v9, v8, Landroid/graphics/Rect;->bottom:I

    .line 5304
    .local v9, "tailPosition":I
    if-ge v10, v2, :cond_3

    .line 5305
    const/4 v7, 0x0

    .line 5312
    .end local v2    # "headPosition":I
    .end local v8    # "r":Landroid/graphics/Rect;
    .end local v9    # "tailPosition":I
    :cond_1
    :goto_1
    const/4 v11, -0x1

    if-eq v7, v11, :cond_0

    .line 5313
    const/4 v5, 0x0

    .line 5314
    .local v5, "item_v":Landroid/view/View;
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v11}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 5315
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v12, v12, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v12}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v12

    const/4 v13, 0x2

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->changlistItemFromEditList(III)Z
    invoke-static {v11, v7, v12, v13}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;III)Z

    .line 5316
    if-nez v7, :cond_5

    .line 5317
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/LinearLayout;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 5322
    :goto_2
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddEmptyItemPosition:I
    invoke-static {v11, v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 5332
    :cond_2
    :goto_3
    if-eqz v5, :cond_0

    .line 5333
    check-cast v5, Landroid/widget/LinearLayout;

    .end local v5    # "item_v":Landroid/view/View;
    const v11, 0x7f0f0059

    invoke-virtual {v5, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    move-object v6, v11

    check-cast v6, Landroid/widget/ImageView;

    .line 5334
    .local v6, "orgView":Landroid/widget/ImageView;
    const v11, 0x7f02007f

    invoke-virtual {v6, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 5335
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v12, 0x3

    invoke-virtual {v11, v12}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 5336
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v12, 0x3

    invoke-virtual {v11, v12}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 5306
    .end local v6    # "orgView":Landroid/widget/ImageView;
    .restart local v2    # "headPosition":I
    .restart local v8    # "r":Landroid/graphics/Rect;
    .restart local v9    # "tailPosition":I
    :cond_3
    if-le v10, v9, :cond_1

    .line 5307
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v11}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppCnt()I

    move-result v7

    goto :goto_1

    .line 5310
    .end local v2    # "headPosition":I
    .end local v8    # "r":Landroid/graphics/Rect;
    .end local v9    # "tailPosition":I
    :cond_4
    const/4 v7, 0x0

    goto :goto_1

    .line 5319
    .restart local v5    # "item_v":Landroid/view/View;
    :cond_5
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/LinearLayout;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/LinearLayout;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 5320
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v0, p2

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->decideScrollMove(Landroid/view/View;Landroid/view/DragEvent;)Z
    invoke-static {v11, v4, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/view/View;Landroid/view/DragEvent;)Z

    goto :goto_2

    .line 5324
    :cond_6
    if-lez v7, :cond_7

    add-int/lit8 v7, v7, -0x1

    .line 5325
    :goto_4
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v11, v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 5326
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v12

    if-eq v11, v12, :cond_2

    .line 5327
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v12

    iget-object v13, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v13

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->changelistItem(II)Z
    invoke-static {v11, v12, v13}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;II)Z

    .line 5328
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setItemIndexFromAppList(I)V

    .line 5329
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/LinearLayout;

    move-result-object v11

    invoke-virtual {v11, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    goto/16 :goto_3

    .line 5324
    :cond_7
    const/4 v7, 0x0

    goto :goto_4

    .line 5345
    .end local v3    # "item_head":Landroid/view/View;
    .end local v4    # "item_tail":Landroid/view/View;
    .end local v5    # "item_v":Landroid/view/View;
    .end local v7    # "position":I
    .end local v10    # "touchY":I
    :pswitch_3
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v11}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 5346
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v12, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 5347
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v12, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 5348
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddEmptyItemPosition:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v11

    const/4 v12, -0x1

    if-eq v11, v12, :cond_0

    .line 5349
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddEmptyItemPosition:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v12

    iget-object v13, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v13}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v13

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->changlistItemFromEditList(III)Z
    invoke-static {v11, v12, v13, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;III)Z

    .line 5350
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v12, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddEmptyItemPosition:I
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    goto/16 :goto_0

    .line 5355
    :pswitch_4
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v12, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListDragMode:Z
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6802(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 5357
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v11

    if-eqz v11, :cond_8

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v11}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v11

    if-eqz v11, :cond_8

    .line 5358
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mVibrator:Landroid/os/SystemVibrator;

    sget-object v12, Lcom/sec/android/app/FlashBarService/AppListController;->mDefaultIvt:[B

    iget-object v13, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v13}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v13

    invoke-virtual {v11, v12, v13}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 5359
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddEmptyItemPosition:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v12

    iget-object v13, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v13}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v13

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->addAndChangelistItem(II)Z
    invoke-static {v11, v12, v13}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;II)Z

    .line 5360
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v12, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddEmptyItemPosition:I
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 5361
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    const/4 v12, -0x1

    invoke-virtual {v11, v12}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setItemIndexFromAppList(I)V

    .line 5362
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListRelayout(Z)V

    .line 5363
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v11}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->updateEditListChanged()V

    .line 5366
    :cond_8
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v12, 0x3

    invoke-virtual {v11, v12}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 5367
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v12, 0x3

    invoke-virtual {v11, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 5368
    :cond_9
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startCollapseTimer()V
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    goto/16 :goto_0

    .line 5274
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
