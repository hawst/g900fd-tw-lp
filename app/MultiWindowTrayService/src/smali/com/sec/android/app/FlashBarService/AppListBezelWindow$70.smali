.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 4388
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 17
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 4390
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    .line 4391
    .local v4, "l":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v1

    .line 4392
    .local v1, "action":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getX()F

    move-result v12

    float-to-int v10, v12

    .line 4393
    .local v10, "x":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getY()F

    move-result v12

    float-to-int v11, v12

    .line 4394
    .local v11, "y":I
    const/4 v5, 0x0

    .line 4395
    .local v5, "left":I
    const/4 v9, 0x0

    .line 4396
    .local v9, "top":I
    const/4 v8, 0x0

    .line 4397
    .local v8, "right":I
    const/4 v3, 0x0

    .line 4398
    .local v3, "bottom":I
    const/4 v7, 0x0

    .line 4401
    .local v7, "rect":Landroid/graphics/Rect;
    const/4 v12, 0x6

    if-eq v1, v12, :cond_0

    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v12

    if-nez v12, :cond_0

    .line 4402
    const/4 v12, 0x0

    .line 4502
    :goto_0
    return v12

    .line 4405
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLunchBlock:Z
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 4406
    const/4 v12, 0x1

    goto :goto_0

    .line 4409
    :cond_1
    packed-switch v1, :pswitch_data_0

    .line 4502
    :cond_2
    :goto_1
    const/4 v12, 0x1

    goto :goto_0

    .line 4412
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsPenWindowOnly:Z
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v12

    if-eqz v12, :cond_5

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyle(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 4413
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowDefWidth:I

    div-int/lit8 v12, v12, 0x2

    sub-int v5, v10, v12

    .line 4414
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowDefHeight:I

    div-int/lit8 v12, v12, 0x2

    sub-int v9, v11, v12

    .line 4415
    if-gez v9, :cond_3

    const/4 v9, 0x0

    .line 4416
    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowDefWidth:I

    add-int v8, v5, v12

    .line 4417
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowDefHeight:I

    add-int v3, v9, v12

    .line 4418
    new-instance v7, Landroid/graphics/Rect;

    .end local v7    # "rect":Landroid/graphics/Rect;
    invoke-direct {v7, v5, v9, v8, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 4427
    .restart local v7    # "rect":Landroid/graphics/Rect;
    :goto_2
    if-eqz v7, :cond_4

    .line 4428
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4429
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v12

    iget v13, v7, Landroid/graphics/Rect;->left:I

    iget v14, v7, Landroid/graphics/Rect;->top:I

    iget v15, v7, Landroid/graphics/Rect;->right:I

    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    move/from16 v16, v0

    invoke-virtual/range {v12 .. v16}, Landroid/widget/ImageView;->layout(IIII)V

    .line 4431
    :cond_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->closeFlashBar()V
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    goto :goto_1

    .line 4420
    :cond_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v13

    invoke-virtual {v12, v13}, Lcom/sec/android/app/FlashBarService/AppListController;->isTemplateItem(I)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 4421
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v13, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsTemplateItem:Z
    invoke-static {v12, v13}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13702(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 4422
    new-instance v7, Landroid/graphics/Rect;

    .end local v7    # "rect":Landroid/graphics/Rect;
    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I
    invoke-static {v15}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v15

    invoke-direct {v7, v12, v13, v14, v15}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v7    # "rect":Landroid/graphics/Rect;
    goto :goto_2

    .line 4424
    :cond_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v12, v10, v11}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentGuideRect(II)Landroid/graphics/Rect;

    move-result-object v7

    goto :goto_2

    .line 4437
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsPenWindowOnly:Z
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v12

    if-eqz v12, :cond_9

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyle(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 4438
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowDefWidth:I

    div-int/lit8 v12, v12, 0x2

    sub-int v5, v10, v12

    .line 4439
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowDefHeight:I

    div-int/lit8 v12, v12, 0x2

    sub-int v9, v11, v12

    .line 4440
    if-gez v9, :cond_7

    const/4 v9, 0x0

    .line 4441
    :cond_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowDefWidth:I

    add-int v8, v5, v12

    .line 4442
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowDefHeight:I

    add-int v3, v9, v12

    .line 4443
    new-instance v7, Landroid/graphics/Rect;

    .end local v7    # "rect":Landroid/graphics/Rect;
    invoke-direct {v7, v5, v9, v8, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 4449
    .restart local v7    # "rect":Landroid/graphics/Rect;
    :cond_8
    :goto_3
    if-eqz v7, :cond_2

    .line 4450
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4451
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v12

    iget v13, v7, Landroid/graphics/Rect;->left:I

    iget v14, v7, Landroid/graphics/Rect;->top:I

    iget v15, v7, Landroid/graphics/Rect;->right:I

    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    move/from16 v16, v0

    invoke-virtual/range {v12 .. v16}, Landroid/widget/ImageView;->layout(IIII)V

    goto/16 :goto_1

    .line 4445
    :cond_9
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsTemplateItem:Z
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v12

    if-nez v12, :cond_8

    .line 4446
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v12, v10, v11}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentGuideRect(II)Landroid/graphics/Rect;

    move-result-object v7

    goto :goto_3

    .line 4457
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v13, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsTemplateItem:Z
    invoke-static {v12, v13}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13702(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 4458
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/FrameLayout;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 4459
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTopGuideline:Landroid/widget/FrameLayout;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/FrameLayout;

    move-result-object v12

    const/4 v13, 0x4

    invoke-virtual {v12, v13}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4460
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBottomGuideline:Landroid/widget/FrameLayout;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/FrameLayout;

    move-result-object v12

    const/4 v13, 0x4

    invoke-virtual {v12, v13}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4461
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    move-result-object v12

    const/4 v13, 0x4

    invoke-virtual {v12, v13}, Lcom/sec/android/app/FlashBarService/GuideLineImageView;->setVisibility(I)V

    .line 4462
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v12

    const/4 v13, 0x4

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 4467
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v13, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsTemplateItem:Z
    invoke-static {v12, v13}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13702(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 4468
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTopGuideline:Landroid/widget/FrameLayout;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/FrameLayout;

    move-result-object v12

    const/4 v13, 0x4

    invoke-virtual {v12, v13}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4469
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBottomGuideline:Landroid/widget/FrameLayout;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/FrameLayout;

    move-result-object v12

    const/4 v13, 0x4

    invoke-virtual {v12, v13}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4470
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    move-result-object v12

    const/4 v13, 0x4

    invoke-virtual {v12, v13}, Lcom/sec/android/app/FlashBarService/GuideLineImageView;->setVisibility(I)V

    .line 4471
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v12

    const/4 v13, 0x4

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4473
    const/4 v12, 0x0

    iput v12, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    const/4 v12, 0x0

    iput v12, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    const/4 v12, 0x0

    iput v12, v4, Landroid/view/WindowManager$LayoutParams;->width:I

    const/4 v12, 0x0

    iput v12, v4, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 4474
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;

    move-result-object v12

    invoke-virtual {v12, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 4475
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowManager:Landroid/view/WindowManager;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v13

    invoke-interface {v12, v13, v4}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 4477
    const-string v12, "AppListBezelWindow"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "App icon long clicked :: appIconIndex = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconIndex:I
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ",  mcurDstIndex = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4478
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsPenWindowOnly:Z
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v12

    if-eqz v12, :cond_c

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyleLaunch(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 4479
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v12}, Lcom/sec/android/app/FlashBarService/AppListController;->isEnableMakePenWindow()Z

    move-result v12

    if-eqz v12, :cond_b

    .line 4480
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowDefWidth:I

    div-int/lit8 v12, v12, 0x2

    sub-int v5, v10, v12

    .line 4481
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowDefHeight:I

    div-int/lit8 v12, v12, 0x2

    sub-int v9, v11, v12

    .line 4482
    if-gez v9, :cond_a

    const/4 v9, 0x0

    .line 4483
    :cond_a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v12

    add-int/2addr v9, v12

    .line 4484
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowDefWidth:I

    add-int v8, v5, v12

    .line 4485
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowDefHeight:I

    add-int v3, v9, v12

    .line 4486
    new-instance v7, Landroid/graphics/Rect;

    .end local v7    # "rect":Landroid/graphics/Rect;
    invoke-direct {v7, v5, v9, v8, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 4488
    .restart local v7    # "rect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v13

    invoke-virtual {v12, v13}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppResolveInfo(I)Landroid/content/pm/ResolveInfo;

    move-result-object v6

    .line 4489
    .local v6, "r":Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v12

    invoke-virtual {v6, v12}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 4490
    .local v2, "appName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v14, 0x7f08002c

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v2, v15, v16

    invoke-virtual {v13, v14, v15}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    .line 4492
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v13

    invoke-virtual {v12, v13, v7}, Lcom/sec/android/app/FlashBarService/AppListController;->startFreeStyleActivity(ILandroid/graphics/Rect;)V

    .line 4499
    .end local v2    # "appName":Ljava/lang/String;
    .end local v6    # "r":Landroid/content/pm/ResolveInfo;
    :cond_b
    :goto_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v12

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 4495
    :cond_c
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v13

    invoke-virtual {v12, v13, v10, v11}, Lcom/sec/android/app/FlashBarService/AppListController;->startFreeStyleActivity(III)V

    goto :goto_4

    .line 4409
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
