.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$15;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->createAppListOverlayHelp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 1039
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1042
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "do_not_show_app_list_help"

    const/4 v2, 0x1

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 1043
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1044
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->dismissHelpWindow()V

    .line 1045
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;)Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    .line 1047
    :cond_0
    return-void
.end method
