.class Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$6;
.super Landroid/content/BroadcastReceiver;
.source "MultiWindowTrayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$6;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 258
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$6;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$800(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 259
    const-string v1, "mlstatus"

    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 260
    .local v0, "state":I
    const-string v1, "MultiWindowTrayService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mMlReceiver : state="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$6;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isMultiWindowSettingEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 263
    if-ne v0, v5, :cond_1

    .line 264
    # setter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mIsMlActivated:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$902(Z)Z

    .line 265
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$6;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    .line 274
    .end local v0    # "state":I
    :cond_0
    :goto_0
    return-void

    .line 267
    .restart local v0    # "state":I
    :cond_1
    # setter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mIsMlActivated:Z
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$902(Z)Z

    .line 268
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$6;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->getFlashBarEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 269
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$6;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v1

    invoke-virtual {v1, v5, v4}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    goto :goto_0
.end method
