.class Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;
.super Landroid/widget/FrameLayout;
.source "SmartWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/SmartWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GuideView"
.end annotation


# instance fields
.field private mBorderView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mParent:Landroid/view/View;

.field private mShowing:Z

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1, "parentView"    # Landroid/view/View;

    .prologue
    .line 1127
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1128
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mParent:Landroid/view/View;

    .line 1129
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mContext:Landroid/content/Context;

    .line 1130
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mWindowManager:Landroid/view/WindowManager;

    .line 1132
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 1135
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mShowing:Z

    if-eqz v0, :cond_0

    .line 1136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v0, p0}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 1137
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mShowing:Z

    .line 1139
    :cond_0
    return-void
.end method

.method public show(IIII)V
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/4 v3, -0x1

    .line 1142
    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mShowing:Z

    if-nez v2, :cond_2

    .line 1143
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mParent:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mParent:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1144
    const-string v2, "SmartWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parent window removed token = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mParent:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1183
    :goto_0
    return-void

    .line 1147
    :cond_0
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 1149
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    const v2, 0x800033

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1150
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1151
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1152
    const/4 v2, -0x2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 1153
    const/16 v2, 0x3ea

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 1154
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mParent:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    iput-object v2, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 1155
    const/16 v2, 0x318

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1159
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/lit8 v2, v2, 0x8

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    .line 1161
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mBorderView:Landroid/view/View;

    if-nez v2, :cond_1

    .line 1162
    new-instance v2, Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mBorderView:Landroid/view/View;

    .line 1163
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mBorderView:Landroid/view/View;

    const v3, 0x7f02010e

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1165
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, p3, p4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1166
    .local v1, "vlp":Landroid/widget/FrameLayout$LayoutParams;
    iput p1, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1167
    iput p2, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1168
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mBorderView:Landroid/view/View;

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1170
    .end local v1    # "vlp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v2, p0, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1171
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mShowing:Z

    goto :goto_0

    .line 1173
    .end local v0    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mBorderView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 1175
    .restart local v1    # "vlp":Landroid/widget/FrameLayout$LayoutParams;
    iput p1, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1176
    iput p2, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1177
    iput p3, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1178
    iput p4, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1180
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mBorderView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    .line 1181
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;->mBorderView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    goto :goto_0
.end method
