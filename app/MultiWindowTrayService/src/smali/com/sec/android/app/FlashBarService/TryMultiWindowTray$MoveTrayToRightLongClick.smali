.class final Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRightLongClick;
.super Ljava/lang/Object;
.source "TryMultiWindowTray.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MoveTrayToRightLongClick"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)V
    .locals 0

    .prologue
    .line 665
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRightLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;
    .param p2, "x1"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$1;

    .prologue
    .line 665
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRightLongClick;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 669
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRightLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayDragLeft:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 670
    new-instance v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRightLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRightLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayLeft:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 671
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRightLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/os/SystemVibrator;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRightLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mIvt:[B
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRightLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/os/SystemVibrator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 672
    const/4 v0, 0x1

    return v0
.end method
