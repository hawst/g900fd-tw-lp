.class public Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;
.super Ljava/lang/Object;
.source "RecentsGLTask.java"


# static fields
.field private static final alphaInterpolator:Landroid/view/animation/interpolator/SineEaseOut;

.field private static borderTextureHeightPx:I

.field private static borderTextureWidthPx:I

.field private static bottom:F

.field private static bottomBorderHeight:F

.field private static closeHeight:F

.field private static closeMarginRight:F

.field private static closeMarginTop:F

.field private static closeRect:Landroid/graphics/RectF;

.field private static closeWidth:F

.field private static coeff:F

.field private static endBackFadeOutDistance:F

.field private static endFrontFadeOutDistance:F

.field private static iconMarginLeft:F

.field private static iconMarginRight:F

.field private static iconMarginTop:F

.field private static iconSize:F

.field private static left:F

.field private static mCloseButtonHandle:I

.field private static mCloseButtonPressedHandle:I

.field private static mCloseTextureVertices:Ljava/nio/FloatBuffer;

.field private static mCloseVertices:Ljava/nio/FloatBuffer;

.field private static mContext:Landroid/content/Context;

.field private static mGLHeight:F

.field private static mHeight:I

.field private static mIconTextureVertices:Ljava/nio/FloatBuffer;

.field private static mIconVertices:Ljava/nio/FloatBuffer;

.field private static mMVPMatrix:[F

.field private static mModelMatrix:[F

.field private static mTextureVertices:Ljava/nio/FloatBuffer;

.field private static mThumbnailTextureVertices:Ljava/nio/FloatBuffer;

.field private static mThumbnailVertices:Ljava/nio/FloatBuffer;

.field private static mTriangle1Vertices:Ljava/nio/FloatBuffer;

.field private static mWidth:F

.field private static pictureBottomBorderHeight:F

.field private static pictureSideBorderWidth:F

.field private static pictureTopBorderHeight:F

.field private static right:F

.field private static final rotateInterpolator:Landroid/view/animation/interpolator/CubicEaseIn;

.field private static sColorHandle:I

.field private static sMatrixHandle:I

.field private static sPositionHandle:I

.field private static sProgram:I

.field private static sTextureHandle:I

.field private static sTexturePositionHandle:I

.field private static sideBorderWidth:F

.field private static startBackFadeOutDistance:F

.field private static startFrontFadeOutDistance:F

.field private static textMarginTop:F

.field private static thumbnailMarginBottom:F

.field private static thumbnailMarginLeft:F

.field private static thumbnailMarginRight:F

.field private static thumbnailMarginTop:F

.field private static top:F

.field private static topBorderHeight:F


# instance fields
.field private alpha:F

.field private correction:F

.field public drawPosition:F

.field private mClosePressed:Z

.field private mCloseRect:Landroid/graphics/RectF;

.field private mComputeOnce:Z

.field private mIconHandle:I

.field public mIsDisappearing:Z

.field private mParent:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

.field private mTaskDescription:Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

.field private mTextHandle:I

.field private mTextTextureVertices:Ljava/nio/FloatBuffer;

.field private mTextVertices:Ljava/nio/FloatBuffer;

.field private mThumbnailHandle:I

.field public position:F

.field red:F

.field private rot:F

.field private scale:F

.field private zscale:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x10

    const/4 v1, -0x1

    .line 53
    const/high16 v0, -0x40800000    # -1.0f

    sput v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mGLHeight:F

    .line 64
    sput v1, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseButtonHandle:I

    .line 65
    sput v1, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseButtonPressedHandle:I

    .line 68
    new-instance v0, Landroid/view/animation/interpolator/SineEaseOut;

    invoke-direct {v0}, Landroid/view/animation/interpolator/SineEaseOut;-><init>()V

    sput-object v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->alphaInterpolator:Landroid/view/animation/interpolator/SineEaseOut;

    .line 69
    new-instance v0, Landroid/view/animation/interpolator/CubicEaseIn;

    invoke-direct {v0}, Landroid/view/animation/interpolator/CubicEaseIn;-><init>()V

    sput-object v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->rotateInterpolator:Landroid/view/animation/interpolator/CubicEaseIn;

    .line 285
    const v0, -0x40b33333    # -0.8f

    sput v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->startBackFadeOutDistance:F

    .line 286
    const/high16 v0, -0x40400000    # -1.5f

    sput v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->endBackFadeOutDistance:F

    .line 287
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->startFrontFadeOutDistance:F

    .line 288
    const v0, 0x3eb33333    # 0.35f

    sput v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->endFrontFadeOutDistance:F

    .line 290
    new-array v0, v2, [F

    sput-object v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mMVPMatrix:[F

    .line 291
    new-array v0, v2, [F

    sput-object v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mModelMatrix:[F

    return-void
.end method

.method constructor <init>(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)V
    .locals 3
    .param p1, "parent"    # Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    const/high16 v1, 0x3f800000    # 1.0f

    .line 302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->position:F

    .line 28
    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->drawPosition:F

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mComputeOnce:Z

    .line 63
    iput v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mThumbnailHandle:I

    .line 67
    iput v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->zscale:F

    .line 293
    iput v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIconHandle:I

    .line 294
    iput v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTextHandle:I

    .line 295
    iput v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->alpha:F

    .line 296
    iput v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->scale:F

    .line 299
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mParent:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    .line 557
    iput v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->red:F

    .line 303
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mParent:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    .line 304
    return-void
.end method

.method private computeValues(F)V
    .locals 10
    .param p1, "pos"    # F

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v8, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    .line 494
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIsDisappearing:Z

    if-eqz v3, :cond_0

    .line 495
    const-string v3, "MIK"

    const-string v5, "SOMEONE CALLED computeValues!!!"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 498
    :cond_0
    iput v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->rot:F

    .line 499
    iput v7, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->zscale:F

    .line 500
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mParent:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->getZoom()F

    move-result v2

    .line 501
    .local v2, "zoom":F
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIsDisappearing:Z

    if-nez v3, :cond_2

    .line 502
    iput v7, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->alpha:F

    .line 503
    sget v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->startBackFadeOutDistance:F

    mul-float/2addr v3, v2

    cmpg-float v3, p1, v3

    if-gez v3, :cond_1

    .line 504
    neg-float v3, p1

    sget v5, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->startBackFadeOutDistance:F

    mul-float/2addr v5, v2

    add-float/2addr v3, v5

    sget v5, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->startBackFadeOutDistance:F

    mul-float/2addr v5, v2

    sget v6, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->endBackFadeOutDistance:F

    mul-float/2addr v6, v2

    sub-float/2addr v5, v6

    div-float/2addr v3, v5

    sub-float v3, v7, v3

    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->alpha:F

    .line 506
    :cond_1
    sget v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->startFrontFadeOutDistance:F

    mul-float/2addr v3, v2

    cmpl-float v3, p1, v3

    if-lez v3, :cond_2

    .line 507
    sget v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->endFrontFadeOutDistance:F

    mul-float/2addr v3, v2

    cmpg-float v3, p1, v3

    if-gez v3, :cond_5

    .line 508
    sget v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->endFrontFadeOutDistance:F

    mul-float/2addr v3, v2

    div-float v0, p1, v3

    .line 509
    .local v0, "in":F
    sget-object v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->rotateInterpolator:Landroid/view/animation/interpolator/CubicEaseIn;

    invoke-virtual {v3, v0}, Landroid/view/animation/interpolator/CubicEaseIn;->getInterpolation(F)F

    move-result v1

    .line 511
    .local v1, "val":F
    const/high16 v3, 0x42b40000    # 90.0f

    mul-float/2addr v3, v1

    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->rot:F

    .line 513
    sget-object v5, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->alphaInterpolator:Landroid/view/animation/interpolator/SineEaseOut;

    cmpg-float v3, v0, v8

    if-gez v3, :cond_4

    move v3, v4

    :goto_0
    invoke-virtual {v5, v3}, Landroid/view/animation/interpolator/SineEaseOut;->getInterpolation(F)F

    move-result v3

    sub-float v3, v7, v3

    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->alpha:F

    .line 522
    .end local v0    # "in":F
    .end local v1    # "val":F
    :cond_2
    :goto_1
    sget v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    mul-float/2addr v3, p1

    div-float/2addr v3, v2

    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->correction:F

    .line 524
    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->correction:F

    mul-float v5, v9, v2

    div-float v5, p1, v5

    add-float/2addr v3, v5

    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->correction:F

    .line 526
    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->correction:F

    sget v5, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mGLHeight:F

    div-float/2addr v5, v9

    sget v6, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottom:F

    add-float/2addr v5, v6

    add-float/2addr v3, v5

    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->correction:F

    .line 528
    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->alpha:F

    cmpl-float v3, v3, v7

    if-lez v3, :cond_3

    .line 529
    iput v7, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->alpha:F

    .line 532
    :cond_3
    new-instance v3, Landroid/graphics/RectF;

    sget-object v5, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeRect:Landroid/graphics/RectF;

    invoke-direct {v3, v5}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseRect:Landroid/graphics/RectF;

    .line 533
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseRect:Landroid/graphics/RectF;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->correction:F

    neg-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    .line 534
    return-void

    .line 513
    .restart local v0    # "in":F
    .restart local v1    # "val":F
    :cond_4
    sub-float v3, v0, v8

    goto :goto_0

    .line 515
    .end local v0    # "in":F
    .end local v1    # "val":F
    :cond_5
    iput v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->alpha:F

    goto :goto_1
.end method

.method public static initGL(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;FFII)V
    .locals 33
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "helper"    # Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;
    .param p2, "width"    # F
    .param p3, "height"    # F
    .param p4, "displayWidth"    # I
    .param p5, "displayHeight"    # I

    .prologue
    .line 74
    const-string v27, "u_MVPMatrix"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getLocation(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sMatrixHandle:I

    .line 75
    const-string v27, "a_Color"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getLocation(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sColorHandle:I

    .line 76
    const-string v27, "a_Position"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getLocation(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sPositionHandle:I

    .line 77
    const-string v27, "a_TexCoordinate"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getLocation(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sTexturePositionHandle:I

    .line 78
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getTextureHandle()I

    move-result v27

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sTextureHandle:I

    .line 79
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getProgram()I

    move-result v27

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sProgram:I

    .line 81
    sput-object p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mContext:Landroid/content/Context;

    .line 82
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 83
    .local v15, "r":Landroid/content/res/Resources;
    const v27, 0x7f0a0108

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v27

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mWidth:F

    .line 84
    const v27, 0x7f0a0109

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v27

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mHeight:I

    .line 85
    const v27, 0x7f0a01a7

    move/from16 v0, v27

    invoke-static {v15, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getFloatFromResource(Landroid/content/res/Resources;I)F

    move-result v27

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->startFrontFadeOutDistance:F

    .line 86
    const v27, 0x7f0a01a8

    move/from16 v0, v27

    invoke-static {v15, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getFloatFromResource(Landroid/content/res/Resources;I)F

    move-result v27

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->endFrontFadeOutDistance:F

    .line 87
    const v27, 0x7f0a01a9

    move/from16 v0, v27

    invoke-static {v15, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getFloatFromResource(Landroid/content/res/Resources;I)F

    move-result v27

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->startBackFadeOutDistance:F

    .line 88
    const v27, 0x7f0a01aa

    move/from16 v0, v27

    invoke-static {v15, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getFloatFromResource(Landroid/content/res/Resources;I)F

    move-result v27

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->endBackFadeOutDistance:F

    .line 90
    const v27, 0x7f0a0193

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v25

    .line 91
    .local v25, "topBorderHeightPx":I
    const v27, 0x7f0a0194

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 92
    .local v16, "sideBorderWidthPx":I
    const v27, 0x7f0a0195

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 93
    .local v2, "bottomBorderHeightPx":I
    const v27, 0x7f0a0196

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 94
    .local v12, "iconSizePx":I
    const v27, 0x7f0a0197

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 95
    .local v11, "iconMarginTopPx":I
    const v27, 0x7f0a0198

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 96
    .local v9, "iconMarginLeftPx":I
    const v27, 0x7f0a0199

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 97
    .local v10, "iconMarginRightPx":I
    const v27, 0x7f0a019a

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 98
    .local v17, "textMarginTopPx":I
    const v27, 0x7f0a019b

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v22

    .line 99
    .local v22, "thumbnailMarginTopPx":I
    const v27, 0x7f0a019c

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v20

    .line 100
    .local v20, "thumbnailMarginLeftPx":I
    const v27, 0x7f0a019d

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 101
    .local v19, "thumbnailMarginBottomPx":I
    const v27, 0x7f0a019e

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    .line 102
    .local v21, "thumbnailMarginRightPx":I
    const v27, 0x7f0a01a1

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 103
    .local v8, "closeWidthPx":I
    const v27, 0x7f0a01a2

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 104
    .local v3, "closeHeightPx":I
    const v27, 0x7f0a019f

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 105
    .local v4, "closeMarginRightPx":I
    const v27, 0x7f0a01a0

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 107
    .local v5, "closeMarginTopPx":I
    sput p3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mGLHeight:F

    .line 109
    move/from16 v0, p4

    int-to-float v0, v0

    move/from16 v27, v0

    div-float v27, v27, p2

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    .line 111
    sget v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mWidth:F

    move/from16 v0, v27

    neg-float v0, v0

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v27, v27, v28

    const/high16 v28, 0x40000000    # 2.0f

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    .line 112
    sget v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    move/from16 v0, v27

    neg-float v0, v0

    move/from16 v27, v0

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    .line 113
    sget v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mWidth:F

    div-float v27, v27, v28

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mHeight:I

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    mul-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    .line 114
    sget v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mWidth:F

    div-float v27, v27, v28

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mHeight:I

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    mul-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottom:F

    .line 116
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getTextureHeight()I

    move-result v27

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->borderTextureHeightPx:I

    .line 117
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getTextureWidth()I

    move-result v27

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->borderTextureWidthPx:I

    .line 119
    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->topBorderHeight:F

    .line 120
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sideBorderWidth:F

    .line 121
    int-to-float v0, v2

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottomBorderHeight:F

    .line 122
    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->borderTextureHeightPx:I

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureTopBorderHeight:F

    .line 123
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->borderTextureWidthPx:I

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureSideBorderWidth:F

    .line 124
    int-to-float v0, v2

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->borderTextureHeightPx:I

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureBottomBorderHeight:F

    .line 126
    int-to-float v0, v12

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconSize:F

    .line 127
    int-to-float v0, v11

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginTop:F

    .line 128
    int-to-float v0, v9

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginLeft:F

    .line 129
    int-to-float v0, v10

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginRight:F

    .line 130
    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->textMarginTop:F

    .line 132
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->thumbnailMarginTop:F

    .line 133
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->thumbnailMarginLeft:F

    .line 134
    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->thumbnailMarginBottom:F

    .line 135
    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->thumbnailMarginRight:F

    .line 137
    int-to-float v0, v5

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeMarginTop:F

    .line 138
    int-to-float v0, v4

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeMarginRight:F

    .line 139
    int-to-float v0, v8

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeWidth:F

    .line 140
    int-to-float v0, v3

    move/from16 v27, v0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v27, v27, v28

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeHeight:F

    .line 142
    const/16 v27, 0x48

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v26, v0

    const/16 v27, 0x0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    aput v28, v26, v27

    const/16 v27, 0x1

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    aput v28, v26, v27

    const/16 v27, 0x2

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x3

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    aput v28, v26, v27

    const/16 v27, 0x4

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->topBorderHeight:F

    sub-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x5

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x6

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sideBorderWidth:F

    add-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x7

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    aput v28, v26, v27

    const/16 v27, 0x8

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x9

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sideBorderWidth:F

    add-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0xa

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->topBorderHeight:F

    sub-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0xb

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0xc

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sideBorderWidth:F

    sub-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0xd

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    aput v28, v26, v27

    const/16 v27, 0xe

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0xf

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sideBorderWidth:F

    sub-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x10

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->topBorderHeight:F

    sub-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x11

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x12

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    aput v28, v26, v27

    const/16 v27, 0x13

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    aput v28, v26, v27

    const/16 v27, 0x14

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x15

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    aput v28, v26, v27

    const/16 v27, 0x16

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->topBorderHeight:F

    sub-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x17

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x18

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    aput v28, v26, v27

    const/16 v27, 0x19

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->topBorderHeight:F

    sub-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x1a

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x1b

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    aput v28, v26, v27

    const/16 v27, 0x1c

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottom:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottomBorderHeight:F

    add-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x1d

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x1e

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sideBorderWidth:F

    add-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x1f

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->topBorderHeight:F

    sub-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x20

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x21

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sideBorderWidth:F

    add-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x22

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottom:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottomBorderHeight:F

    add-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x23

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x24

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sideBorderWidth:F

    sub-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x25

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->topBorderHeight:F

    sub-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x26

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x27

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sideBorderWidth:F

    sub-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x28

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottom:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottomBorderHeight:F

    add-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x29

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x2a

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    aput v28, v26, v27

    const/16 v27, 0x2b

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->topBorderHeight:F

    sub-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x2c

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x2d

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    aput v28, v26, v27

    const/16 v27, 0x2e

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottom:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottomBorderHeight:F

    add-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x2f

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x30

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    aput v28, v26, v27

    const/16 v27, 0x31

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottom:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottomBorderHeight:F

    add-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x32

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x33

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    aput v28, v26, v27

    const/16 v27, 0x34

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottom:F

    aput v28, v26, v27

    const/16 v27, 0x35

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x36

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sideBorderWidth:F

    add-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x37

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottom:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottomBorderHeight:F

    add-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x38

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x39

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sideBorderWidth:F

    add-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x3a

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottom:F

    aput v28, v26, v27

    const/16 v27, 0x3b

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x3c

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sideBorderWidth:F

    sub-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x3d

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottom:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottomBorderHeight:F

    add-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x3e

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x3f

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sideBorderWidth:F

    sub-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x40

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottom:F

    aput v28, v26, v27

    const/16 v27, 0x41

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x42

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    aput v28, v26, v27

    const/16 v27, 0x43

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottom:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottomBorderHeight:F

    add-float v28, v28, v29

    aput v28, v26, v27

    const/16 v27, 0x44

    const/16 v28, 0x0

    aput v28, v26, v27

    const/16 v27, 0x45

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    aput v28, v26, v27

    const/16 v27, 0x46

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottom:F

    aput v28, v26, v27

    const/16 v27, 0x47

    const/16 v28, 0x0

    aput v28, v26, v27

    .line 181
    .local v26, "triangle1VerticesData":[F
    const/16 v27, 0x30

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v18, v0

    const/16 v27, 0x0

    const/16 v28, 0x0

    aput v28, v18, v27

    const/16 v27, 0x1

    const/16 v28, 0x0

    aput v28, v18, v27

    const/16 v27, 0x2

    const/16 v28, 0x0

    aput v28, v18, v27

    const/16 v27, 0x3

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureTopBorderHeight:F

    aput v28, v18, v27

    const/16 v27, 0x4

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureSideBorderWidth:F

    aput v28, v18, v27

    const/16 v27, 0x5

    const/16 v28, 0x0

    aput v28, v18, v27

    const/16 v27, 0x6

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureSideBorderWidth:F

    aput v28, v18, v27

    const/16 v27, 0x7

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureTopBorderHeight:F

    aput v28, v18, v27

    const/16 v27, 0x8

    const/high16 v28, 0x3f800000    # 1.0f

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureSideBorderWidth:F

    sub-float v28, v28, v29

    aput v28, v18, v27

    const/16 v27, 0x9

    const/16 v28, 0x0

    aput v28, v18, v27

    const/16 v27, 0xa

    const/high16 v28, 0x3f800000    # 1.0f

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureSideBorderWidth:F

    sub-float v28, v28, v29

    aput v28, v18, v27

    const/16 v27, 0xb

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureTopBorderHeight:F

    aput v28, v18, v27

    const/16 v27, 0xc

    const/high16 v28, 0x3f800000    # 1.0f

    aput v28, v18, v27

    const/16 v27, 0xd

    const/16 v28, 0x0

    aput v28, v18, v27

    const/16 v27, 0xe

    const/high16 v28, 0x3f800000    # 1.0f

    aput v28, v18, v27

    const/16 v27, 0xf

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureTopBorderHeight:F

    aput v28, v18, v27

    const/16 v27, 0x10

    const/16 v28, 0x0

    aput v28, v18, v27

    const/16 v27, 0x11

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureTopBorderHeight:F

    aput v28, v18, v27

    const/16 v27, 0x12

    const/16 v28, 0x0

    aput v28, v18, v27

    const/16 v27, 0x13

    const/high16 v28, 0x3f800000    # 1.0f

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureBottomBorderHeight:F

    sub-float v28, v28, v29

    aput v28, v18, v27

    const/16 v27, 0x14

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureSideBorderWidth:F

    aput v28, v18, v27

    const/16 v27, 0x15

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureTopBorderHeight:F

    aput v28, v18, v27

    const/16 v27, 0x16

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureSideBorderWidth:F

    aput v28, v18, v27

    const/16 v27, 0x17

    const/high16 v28, 0x3f800000    # 1.0f

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureBottomBorderHeight:F

    sub-float v28, v28, v29

    aput v28, v18, v27

    const/16 v27, 0x18

    const/high16 v28, 0x3f800000    # 1.0f

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureSideBorderWidth:F

    sub-float v28, v28, v29

    aput v28, v18, v27

    const/16 v27, 0x19

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureTopBorderHeight:F

    aput v28, v18, v27

    const/16 v27, 0x1a

    const/high16 v28, 0x3f800000    # 1.0f

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureSideBorderWidth:F

    sub-float v28, v28, v29

    aput v28, v18, v27

    const/16 v27, 0x1b

    const/high16 v28, 0x3f800000    # 1.0f

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureBottomBorderHeight:F

    sub-float v28, v28, v29

    aput v28, v18, v27

    const/16 v27, 0x1c

    const/high16 v28, 0x3f800000    # 1.0f

    aput v28, v18, v27

    const/16 v27, 0x1d

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureTopBorderHeight:F

    aput v28, v18, v27

    const/16 v27, 0x1e

    const/high16 v28, 0x3f800000    # 1.0f

    aput v28, v18, v27

    const/16 v27, 0x1f

    const/high16 v28, 0x3f800000    # 1.0f

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureBottomBorderHeight:F

    sub-float v28, v28, v29

    aput v28, v18, v27

    const/16 v27, 0x20

    const/16 v28, 0x0

    aput v28, v18, v27

    const/16 v27, 0x21

    const/high16 v28, 0x3f800000    # 1.0f

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureBottomBorderHeight:F

    sub-float v28, v28, v29

    aput v28, v18, v27

    const/16 v27, 0x22

    const/16 v28, 0x0

    aput v28, v18, v27

    const/16 v27, 0x23

    const/high16 v28, 0x3f800000    # 1.0f

    aput v28, v18, v27

    const/16 v27, 0x24

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureSideBorderWidth:F

    aput v28, v18, v27

    const/16 v27, 0x25

    const/high16 v28, 0x3f800000    # 1.0f

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureBottomBorderHeight:F

    sub-float v28, v28, v29

    aput v28, v18, v27

    const/16 v27, 0x26

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureSideBorderWidth:F

    aput v28, v18, v27

    const/16 v27, 0x27

    const/high16 v28, 0x3f800000    # 1.0f

    aput v28, v18, v27

    const/16 v27, 0x28

    const/high16 v28, 0x3f800000    # 1.0f

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureSideBorderWidth:F

    sub-float v28, v28, v29

    aput v28, v18, v27

    const/16 v27, 0x29

    const/high16 v28, 0x3f800000    # 1.0f

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureBottomBorderHeight:F

    sub-float v28, v28, v29

    aput v28, v18, v27

    const/16 v27, 0x2a

    const/high16 v28, 0x3f800000    # 1.0f

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureSideBorderWidth:F

    sub-float v28, v28, v29

    aput v28, v18, v27

    const/16 v27, 0x2b

    const/high16 v28, 0x3f800000    # 1.0f

    aput v28, v18, v27

    const/16 v27, 0x2c

    const/high16 v28, 0x3f800000    # 1.0f

    aput v28, v18, v27

    const/16 v27, 0x2d

    const/high16 v28, 0x3f800000    # 1.0f

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->pictureBottomBorderHeight:F

    sub-float v28, v28, v29

    aput v28, v18, v27

    const/16 v27, 0x2e

    const/high16 v28, 0x3f800000    # 1.0f

    aput v28, v18, v27

    const/16 v27, 0x2f

    const/high16 v28, 0x3f800000    # 1.0f

    aput v28, v18, v27

    .line 219
    .local v18, "textureVerticesData":[F
    const/16 v27, 0xc

    move/from16 v0, v27

    new-array v14, v0, [F

    const/16 v27, 0x0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginLeft:F

    add-float v28, v28, v29

    aput v28, v14, v27

    const/16 v27, 0x1

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginTop:F

    sub-float v28, v28, v29

    aput v28, v14, v27

    const/16 v27, 0x2

    const/16 v28, 0x0

    aput v28, v14, v27

    const/16 v27, 0x3

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginLeft:F

    add-float v28, v28, v29

    aput v28, v14, v27

    const/16 v27, 0x4

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginTop:F

    sub-float v28, v28, v29

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconSize:F

    sub-float v28, v28, v29

    aput v28, v14, v27

    const/16 v27, 0x5

    const/16 v28, 0x0

    aput v28, v14, v27

    const/16 v27, 0x6

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginLeft:F

    add-float v28, v28, v29

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconSize:F

    add-float v28, v28, v29

    aput v28, v14, v27

    const/16 v27, 0x7

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginTop:F

    sub-float v28, v28, v29

    aput v28, v14, v27

    const/16 v27, 0x8

    const/16 v28, 0x0

    aput v28, v14, v27

    const/16 v27, 0x9

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginLeft:F

    add-float v28, v28, v29

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconSize:F

    add-float v28, v28, v29

    aput v28, v14, v27

    const/16 v27, 0xa

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginTop:F

    sub-float v28, v28, v29

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconSize:F

    sub-float v28, v28, v29

    aput v28, v14, v27

    const/16 v27, 0xb

    const/16 v28, 0x0

    aput v28, v14, v27

    .line 225
    .local v14, "iconVerticesData":[F
    const/16 v27, 0x8

    move/from16 v0, v27

    new-array v13, v0, [F

    fill-array-data v13, :array_0

    .line 231
    .local v13, "iconTextureVerticesData":[F
    const/16 v27, 0xc

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v27, 0x0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->thumbnailMarginLeft:F

    add-float v28, v28, v29

    aput v28, v24, v27

    const/16 v27, 0x1

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->thumbnailMarginTop:F

    sub-float v28, v28, v29

    aput v28, v24, v27

    const/16 v27, 0x2

    const/16 v28, 0x0

    aput v28, v24, v27

    const/16 v27, 0x3

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->thumbnailMarginLeft:F

    add-float v28, v28, v29

    aput v28, v24, v27

    const/16 v27, 0x4

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottom:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->thumbnailMarginBottom:F

    add-float v28, v28, v29

    aput v28, v24, v27

    const/16 v27, 0x5

    const/16 v28, 0x0

    aput v28, v24, v27

    const/16 v27, 0x6

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->thumbnailMarginRight:F

    sub-float v28, v28, v29

    aput v28, v24, v27

    const/16 v27, 0x7

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->thumbnailMarginTop:F

    sub-float v28, v28, v29

    aput v28, v24, v27

    const/16 v27, 0x8

    const/16 v28, 0x0

    aput v28, v24, v27

    const/16 v27, 0x9

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->thumbnailMarginRight:F

    sub-float v28, v28, v29

    aput v28, v24, v27

    const/16 v27, 0xa

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottom:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->thumbnailMarginBottom:F

    add-float v28, v28, v29

    aput v28, v24, v27

    const/16 v27, 0xb

    const/16 v28, 0x0

    aput v28, v24, v27

    .line 237
    .local v24, "thumbnailVerticesData":[F
    move-object/from16 v23, v13

    .line 238
    .local v23, "thumbnailTextureVerticesData":[F
    const/16 v27, 0xc

    move/from16 v0, v27

    new-array v7, v0, [F

    const/16 v27, 0x0

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeMarginRight:F

    sub-float v28, v28, v29

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeWidth:F

    sub-float v28, v28, v29

    aput v28, v7, v27

    const/16 v27, 0x1

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeMarginTop:F

    sub-float v28, v28, v29

    aput v28, v7, v27

    const/16 v27, 0x2

    const/16 v28, 0x0

    aput v28, v7, v27

    const/16 v27, 0x3

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeMarginRight:F

    sub-float v28, v28, v29

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeWidth:F

    sub-float v28, v28, v29

    aput v28, v7, v27

    const/16 v27, 0x4

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeMarginTop:F

    sub-float v28, v28, v29

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeHeight:F

    sub-float v28, v28, v29

    aput v28, v7, v27

    const/16 v27, 0x5

    const/16 v28, 0x0

    aput v28, v7, v27

    const/16 v27, 0x6

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeMarginRight:F

    sub-float v28, v28, v29

    aput v28, v7, v27

    const/16 v27, 0x7

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeMarginTop:F

    sub-float v28, v28, v29

    aput v28, v7, v27

    const/16 v27, 0x8

    const/16 v28, 0x0

    aput v28, v7, v27

    const/16 v27, 0x9

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeMarginRight:F

    sub-float v28, v28, v29

    aput v28, v7, v27

    const/16 v27, 0xa

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeMarginTop:F

    sub-float v28, v28, v29

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeHeight:F

    sub-float v28, v28, v29

    aput v28, v7, v27

    const/16 v27, 0xb

    const/16 v28, 0x0

    aput v28, v7, v27

    .line 244
    .local v7, "closeVerticesData":[F
    move-object v6, v13

    .line 246
    .local v6, "closeTextureVerticesData":[F
    new-instance v27, Landroid/graphics/RectF;

    sget v28, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeMarginRight:F

    sub-float v28, v28, v29

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeWidth:F

    sub-float v28, v28, v29

    sget v29, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v30, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeMarginTop:F

    sub-float v29, v29, v30

    sget v30, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeHeight:F

    sub-float v29, v29, v30

    sget v30, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    sget v31, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeMarginRight:F

    sub-float v30, v30, v31

    sget v31, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v32, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeMarginTop:F

    sub-float v31, v31, v32

    invoke-direct/range {v27 .. v31}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->closeRect:Landroid/graphics/RectF;

    .line 248
    invoke-static/range {v26 .. v26}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadToBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v27

    sput-object v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTriangle1Vertices:Ljava/nio/FloatBuffer;

    .line 249
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadToBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v27

    sput-object v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTextureVertices:Ljava/nio/FloatBuffer;

    .line 250
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadToBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v27

    sput-object v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIconVertices:Ljava/nio/FloatBuffer;

    .line 251
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadToBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v27

    sput-object v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIconTextureVertices:Ljava/nio/FloatBuffer;

    .line 252
    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadToBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v27

    sput-object v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mThumbnailVertices:Ljava/nio/FloatBuffer;

    .line 253
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadToBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v27

    sput-object v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mThumbnailTextureVertices:Ljava/nio/FloatBuffer;

    .line 254
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadToBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v27

    sput-object v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseVertices:Ljava/nio/FloatBuffer;

    .line 255
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadToBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v27

    sput-object v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseTextureVertices:Ljava/nio/FloatBuffer;

    .line 257
    sget v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseButtonHandle:I

    const/16 v28, -0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_0

    .line 258
    sget v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseButtonHandle:I

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->deleteTexture(I)V

    .line 259
    const/16 v27, -0x1

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseButtonHandle:I

    .line 261
    :cond_0
    sget v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseButtonPressedHandle:I

    const/16 v28, -0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_1

    .line 262
    sget v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseButtonPressedHandle:I

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->deleteTexture(I)V

    .line 263
    const/16 v27, -0x1

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseButtonPressedHandle:I

    .line 266
    :cond_1
    const v27, 0x7f02005e

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadTexture(Landroid/content/Context;I)I

    move-result v27

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseButtonHandle:I

    .line 267
    const v27, 0x7f02005f

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadTexture(Landroid/content/Context;I)I

    move-result v27

    sput v27, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseButtonPressedHandle:I

    .line 268
    return-void

    .line 225
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public cleanUp()V
    .locals 2

    .prologue
    .line 620
    const-string v0, "MIK"

    const-string v1, "cleanUp"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    return-void
.end method

.method public delayedGLInit()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 636
    const-string v0, "MIK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "delayedGLInit name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTaskDescription:Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", icon = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTaskDescription:Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", th = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTaskDescription:Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getThumbnail()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTextHandle:I

    if-ne v0, v3, :cond_0

    .line 638
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTaskDescription:Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->setText(Ljava/lang/String;)V

    .line 640
    :cond_0
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIconHandle:I

    if-ne v0, v3, :cond_1

    .line 641
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTaskDescription:Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->drawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->setIcon(Landroid/graphics/Bitmap;)V

    .line 643
    :cond_1
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mThumbnailHandle:I

    if-ne v0, v3, :cond_2

    .line 644
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTaskDescription:Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getThumbnail()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->setThumbnail(Landroid/graphics/Bitmap;)V

    .line 646
    :cond_2
    return-void
.end method

.method public draw([F[F)V
    .locals 7
    .param p1, "viewMatrix"    # [F
    .param p2, "projectionMatrix"    # [F

    .prologue
    .line 307
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mParent:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->isSmoothing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->position:F

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->drawPosition:F

    sub-float v6, v0, v1

    .line 309
    .local v6, "drawDiff":F
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x38d1b717    # 1.0E-4f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 310
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->drawPosition:F

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mParent:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->getSmoothingSpeed()F

    move-result v1

    div-float v1, v6, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->drawPosition:F

    .line 311
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->drawPosition:F

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->computeValues(F)V

    .line 315
    .end local v6    # "drawDiff":F
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIsDisappearing:Z

    if-eqz v0, :cond_1

    .line 316
    const-string v0, "MIK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "alpha="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->alpha:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", scale="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->scale:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    :cond_1
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->alpha:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 320
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->position:F

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->drawPosition:F

    .line 413
    :goto_0
    return-void

    .line 326
    :cond_2
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sProgram:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 328
    sget-object v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mModelMatrix:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 330
    sget-object v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mModelMatrix:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->scale:F

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->scale:F

    iget v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->scale:F

    iget v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->zscale:F

    mul-float/2addr v4, v5

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 331
    sget-object v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mModelMatrix:[F

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/high16 v3, -0x40400000    # -1.5f

    iget v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->correction:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->drawPosition:F

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 332
    sget-object v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mModelMatrix:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->rot:F

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 333
    sget-object v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mModelMatrix:[F

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/high16 v3, 0x3fc00000    # 1.5f

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 337
    sget-object v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mMVPMatrix:[F

    const/4 v1, 0x0

    const/4 v3, 0x0

    sget-object v4, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mModelMatrix:[F

    const/4 v5, 0x0

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 341
    sget-object v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mMVPMatrix:[F

    const/4 v1, 0x0

    const/4 v3, 0x0

    sget-object v4, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mMVPMatrix:[F

    const/4 v5, 0x0

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 343
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sTextureHandle:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 344
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sMatrixHandle:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mMVPMatrix:[F

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 345
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sColorHandle:I

    const/4 v1, 0x4

    new-array v1, v1, [F

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->red:F

    aput v3, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput v3, v1, v2

    const/4 v2, 0x2

    const/4 v3, 0x0

    aput v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->alpha:F

    aput v3, v1, v2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glVertexAttrib4fv(I[FI)V

    .line 348
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mThumbnailHandle:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    .line 349
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sPositionHandle:I

    const/4 v1, 0x3

    const/16 v2, 0x1406

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mThumbnailVertices:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 350
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sTexturePositionHandle:I

    const/4 v1, 0x2

    const/16 v2, 0x1406

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mThumbnailTextureVertices:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 352
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sPositionHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 353
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sTexturePositionHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 355
    const/16 v0, 0xde1

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mThumbnailHandle:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 357
    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 361
    :cond_3
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sPositionHandle:I

    const/4 v1, 0x3

    const/16 v2, 0x1406

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTriangle1Vertices:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 362
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sTexturePositionHandle:I

    const/4 v1, 0x2

    const/16 v2, 0x1406

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTextureVertices:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 364
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sPositionHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 365
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sTexturePositionHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 367
    const/16 v0, 0xde1

    sget v1, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sTextureHandle:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 369
    const/4 v0, 0x5

    const/4 v1, 0x0

    const/16 v2, 0x8

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 370
    const/4 v0, 0x5

    const/16 v1, 0x8

    const/16 v2, 0x8

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 371
    const/4 v0, 0x5

    const/16 v1, 0x10

    const/16 v2, 0x8

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 374
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIconHandle:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    .line 375
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sPositionHandle:I

    const/4 v1, 0x3

    const/16 v2, 0x1406

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIconVertices:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 376
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sTexturePositionHandle:I

    const/4 v1, 0x2

    const/16 v2, 0x1406

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIconTextureVertices:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 378
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sPositionHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 379
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sTexturePositionHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 381
    const/16 v0, 0xde1

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIconHandle:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 383
    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 387
    :cond_4
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTextHandle:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    .line 388
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sPositionHandle:I

    const/4 v1, 0x3

    const/16 v2, 0x1406

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTextVertices:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 389
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sTexturePositionHandle:I

    const/4 v1, 0x2

    const/16 v2, 0x1406

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTextTextureVertices:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 391
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sPositionHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 392
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sTexturePositionHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 394
    const/16 v0, 0xde1

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTextHandle:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 396
    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 400
    :cond_5
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sPositionHandle:I

    const/4 v1, 0x3

    const/16 v2, 0x1406

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseVertices:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 401
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sTexturePositionHandle:I

    const/4 v1, 0x2

    const/16 v2, 0x1406

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseTextureVertices:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 403
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sPositionHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 404
    sget v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->sTexturePositionHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 406
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mClosePressed:Z

    if-eqz v0, :cond_6

    .line 407
    const/16 v0, 0xde1

    sget v1, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseButtonPressedHandle:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 412
    :goto_1
    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    goto/16 :goto_0

    .line 409
    :cond_6
    const/16 v0, 0xde1

    sget v1, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseButtonHandle:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    goto :goto_1
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 614
    const-string v0, "MIK"

    const-string v1, "Task: finalize"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->cleanUp()V

    .line 616
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 617
    return-void
.end method

.method public getAlpha()F
    .locals 1

    .prologue
    .line 598
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->alpha:F

    return v0
.end method

.method public getBottom()F
    .locals 2

    .prologue
    .line 542
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->correction:F

    neg-float v0, v0

    sget v1, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->bottom:F

    add-float/2addr v0, v1

    return v0
.end method

.method public getLocalTextures()[I
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 657
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 658
    .local v2, "textures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTextHandle:I

    if-eq v3, v4, :cond_0

    .line 659
    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTextHandle:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 661
    :cond_0
    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIconHandle:I

    if-eq v3, v4, :cond_1

    .line 662
    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIconHandle:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 664
    :cond_1
    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mThumbnailHandle:I

    if-eq v3, v4, :cond_2

    .line 665
    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mThumbnailHandle:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 667
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v1, v3, [I

    .line 668
    .local v1, "res":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 669
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v1, v0

    .line 668
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 671
    :cond_3
    return-object v1
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 606
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->scale:F

    return v0
.end method

.method public getTaskDescription()Lcom/sec/android/app/FlashBarService/recent/TaskDescription;
    .locals 1

    .prologue
    .line 653
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTaskDescription:Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    return-object v0
.end method

.method public getTop()F
    .locals 2

    .prologue
    .line 538
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->correction:F

    neg-float v0, v0

    sget v1, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    add-float/2addr v0, v1

    return v0
.end method

.method public layout(FZ)V
    .locals 1
    .param p1, "pos"    # F
    .param p2, "force"    # Z

    .prologue
    .line 485
    iput p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->position:F

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mParent:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->isSmoothing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mComputeOnce:Z

    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    .line 487
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mComputeOnce:Z

    .line 488
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->position:F

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->drawPosition:F

    .line 489
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->drawPosition:F

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->computeValues(F)V

    .line 491
    :cond_1
    return-void
.end method

.method public onMotionEvent(I)V
    .locals 2
    .param p1, "action"    # I

    .prologue
    const/4 v1, 0x0

    .line 559
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v1, v1, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->onMotionEvent(IFFZ)Z

    .line 561
    return-void
.end method

.method public onMotionEvent(IFFZ)Z
    .locals 5
    .param p1, "action"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "click"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 564
    sget v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    cmpg-float v3, p2, v3

    if-lez v3, :cond_0

    sget v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->right:F

    cmpl-float v3, p2, v3

    if-gez v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->getTop()F

    move-result v3

    cmpl-float v3, p3, v3

    if-gez v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->getBottom()F

    move-result v3

    cmpg-float v3, p3, v3

    if-gtz v3, :cond_2

    :cond_0
    move v1, v2

    .line 594
    :cond_1
    :goto_0
    return v1

    .line 567
    :cond_2
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 569
    :pswitch_1
    const-string v2, "MIK"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rect = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseRect:Landroid/graphics/RectF;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " x = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " y = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseRect:Landroid/graphics/RectF;

    invoke-virtual {v2, p2, p3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 571
    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mClosePressed:Z

    goto :goto_0

    .line 575
    :pswitch_2
    if-eqz p4, :cond_3

    .line 576
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mClosePressed:Z

    if-eqz v3, :cond_4

    .line 577
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mCloseRect:Landroid/graphics/RectF;

    invoke-virtual {v3, p2, p3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 578
    const-string v3, "MIK"

    const-string v4, "Close clicked"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    sget-object v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mContext:Landroid/content/Context;

    const-string v4, "audio"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 580
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 582
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mParent:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->onTaskDismissed(Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;)V

    .line 588
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    :cond_3
    :goto_1
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mClosePressed:Z

    goto :goto_0

    .line 585
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mParent:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->onTaskLaunched(Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;)V

    goto :goto_1

    .line 591
    :pswitch_3
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mClosePressed:Z

    goto :goto_0

    .line 567
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public setAlpha(F)V
    .locals 0
    .param p1, "a"    # F

    .prologue
    .line 602
    iput p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->alpha:F

    .line 603
    return-void
.end method

.method public setIcon(Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "b"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 459
    const-string v0, "MIK"

    const-string v1, "setIcon"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIconHandle:I

    if-eq v0, v2, :cond_0

    .line 461
    new-array v0, v4, [I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIconHandle:I

    aput v1, v0, v3

    invoke-static {v4, v0, v3}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 462
    iput v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIconHandle:I

    .line 464
    :cond_0
    if-eqz p1, :cond_1

    .line 465
    invoke-static {p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadTexture(Landroid/graphics/Bitmap;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIconHandle:I

    .line 467
    :cond_1
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 610
    iput p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->scale:F

    .line 611
    return-void
.end method

.method public setTaskDescription(Lcom/sec/android/app/FlashBarService/recent/TaskDescription;)V
    .locals 0
    .param p1, "td"    # Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    .prologue
    .line 649
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTaskDescription:Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    .line 650
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 16
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 416
    const-string v12, "MIK"

    const-string v13, "setText"

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 418
    .local v5, "p":Landroid/graphics/Paint;
    const v12, -0xb8b8b9

    invoke-virtual {v5, v12}, Landroid/graphics/Paint;->setColor(I)V

    .line 419
    const/high16 v12, 0x42180000    # 38.0f

    invoke-virtual {v5, v12}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 420
    const/4 v12, 0x1

    invoke-virtual {v5, v12}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 421
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 423
    .local v2, "bounds":Landroid/graphics/Rect;
    invoke-virtual {v5}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v4

    .line 424
    .local v4, "fm":Landroid/graphics/Paint$FontMetrics;
    const/4 v12, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v13

    move-object/from16 v0, p1

    invoke-virtual {v5, v0, v12, v13, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 425
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v12

    add-int/lit8 v11, v12, 0x5

    .line 426
    .local v11, "textWidthPx":I
    iget v12, v4, Landroid/graphics/Paint$FontMetrics;->ascent:F

    neg-float v12, v12

    iget v13, v4, Landroid/graphics/Paint$FontMetrics;->descent:F

    add-float/2addr v12, v13

    float-to-int v7, v12

    .line 428
    .local v7, "textHeightPx":I
    sget-object v12, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v7, v12}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 429
    .local v1, "b":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 430
    .local v3, "c":Landroid/graphics/Canvas;
    const/4 v12, 0x0

    iget v13, v4, Landroid/graphics/Paint$FontMetrics;->ascent:F

    neg-float v13, v13

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v12, v13, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 432
    int-to-float v12, v11

    sget v13, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v10, v12, v13

    .line 433
    .local v10, "textWidth":F
    int-to-float v12, v7

    sget v13, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->coeff:F

    div-float v6, v12, v13

    .line 435
    .local v6, "textHeight":F
    const/16 v12, 0xc

    new-array v9, v12, [F

    const/4 v12, 0x0

    sget v13, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    sget v14, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginLeft:F

    add-float/2addr v13, v14

    sget v14, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconSize:F

    add-float/2addr v13, v14

    sget v14, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginRight:F

    add-float/2addr v13, v14

    aput v13, v9, v12

    const/4 v12, 0x1

    sget v13, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v14, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->textMarginTop:F

    sub-float/2addr v13, v14

    aput v13, v9, v12

    const/4 v12, 0x2

    const/4 v13, 0x0

    aput v13, v9, v12

    const/4 v12, 0x3

    sget v13, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    sget v14, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginLeft:F

    add-float/2addr v13, v14

    sget v14, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconSize:F

    add-float/2addr v13, v14

    sget v14, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginRight:F

    add-float/2addr v13, v14

    aput v13, v9, v12

    const/4 v12, 0x4

    sget v13, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sub-float/2addr v13, v6

    sget v14, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->textMarginTop:F

    sub-float/2addr v13, v14

    aput v13, v9, v12

    const/4 v12, 0x5

    const/4 v13, 0x0

    aput v13, v9, v12

    const/4 v12, 0x6

    sget v13, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    sget v14, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginLeft:F

    add-float/2addr v13, v14

    sget v14, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconSize:F

    add-float/2addr v13, v14

    sget v14, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginRight:F

    add-float/2addr v13, v14

    add-float/2addr v13, v10

    aput v13, v9, v12

    const/4 v12, 0x7

    sget v13, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sget v14, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->textMarginTop:F

    sub-float/2addr v13, v14

    aput v13, v9, v12

    const/16 v12, 0x8

    const/4 v13, 0x0

    aput v13, v9, v12

    const/16 v12, 0x9

    sget v13, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->left:F

    sget v14, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginLeft:F

    add-float/2addr v13, v14

    sget v14, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconSize:F

    add-float/2addr v13, v14

    sget v14, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->iconMarginRight:F

    add-float/2addr v13, v14

    add-float/2addr v13, v10

    aput v13, v9, v12

    const/16 v12, 0xa

    sget v13, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->top:F

    sub-float/2addr v13, v6

    sget v14, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->textMarginTop:F

    sub-float/2addr v13, v14

    aput v13, v9, v12

    const/16 v12, 0xb

    const/4 v13, 0x0

    aput v13, v9, v12

    .line 441
    .local v9, "textVerticesData":[F
    const/16 v12, 0x8

    new-array v8, v12, [F

    fill-array-data v8, :array_0

    .line 447
    .local v8, "textTextureVerticesData":[F
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadToBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTextVertices:Ljava/nio/FloatBuffer;

    .line 448
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadToBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTextTextureVertices:Ljava/nio/FloatBuffer;

    .line 450
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTextHandle:I

    const/4 v13, -0x1

    if-eq v12, v13, :cond_0

    .line 451
    const/4 v12, 0x1

    const/4 v13, 0x1

    new-array v13, v13, [I

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTextHandle:I

    aput v15, v13, v14

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 453
    :cond_0
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadTexture(Landroid/graphics/Bitmap;)I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTextHandle:I

    .line 454
    const-string v12, "MIK"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "setText, mTextHandle = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mTextHandle:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 456
    return-void

    .line 441
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "b"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 470
    const-string v0, "MIK"

    const-string v1, "setThumbnail"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mThumbnailHandle:I

    if-eq v0, v2, :cond_0

    .line 472
    new-array v0, v4, [I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mThumbnailHandle:I

    aput v1, v0, v3

    invoke-static {v4, v0, v3}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 473
    iput v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mThumbnailHandle:I

    .line 475
    :cond_0
    if-eqz p1, :cond_1

    .line 476
    invoke-static {p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadTexture(Landroid/graphics/Bitmap;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mThumbnailHandle:I

    .line 478
    :cond_1
    return-void
.end method
