.class final Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "AppListItemViewHolder"
.end annotation


# instance fields
.field badgeIconView:Landroid/widget/ImageView;

.field contentView:Landroid/widget/LinearLayout;

.field iconView:Landroid/widget/ImageView;

.field labelView:Landroid/widget/TextView;

.field removeIconView:Landroid/widget/ImageView;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "appClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->badgeIconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->removeIconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    return-void
.end method

.method public setOnDragListener(Landroid/view/View$OnDragListener;)V
    .locals 1
    .param p1, "appListItemDragListener"    # Landroid/view/View$OnDragListener;

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->contentView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 230
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1
    .param p1, "appTouchListener"    # Landroid/view/View$OnTouchListener;

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 220
    return-void
.end method
