.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 3580
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    const/4 v5, 0x1

    .line 3582
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getItemIndex(Landroid/view/View;)I

    move-result v3

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1802(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 3584
    const-string v2, "AppListGridWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "App icon clicked :: appIconIndex = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mbAnimating = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-boolean v4, v4, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbAnimating:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mbEditmode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3587
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentHistoryIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v2

    if-le v2, v6, :cond_0

    .line 3588
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentHistoryIndex:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 3593
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-boolean v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbAnimating:Z

    if-eqz v2, :cond_3

    .line 3594
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v3

    invoke-virtual {v2, v3, v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->isTemplate(IZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3595
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v3

    invoke-virtual {v2, v3, v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->createConfirmDialog(IZ)V

    .line 3615
    :cond_2
    :goto_0
    return-void

    .line 3600
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v2

    if-le v2, v6, :cond_4

    .line 3601
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getComponentInfo(I)Landroid/content/pm/ComponentInfo;

    move-result-object v0

    .line 3602
    .local v0, "info":Landroid/content/pm/ComponentInfo;
    const-string v2, "AppListGridWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "App icon clicked :: name = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3603
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/FlashBarService/AppListController;->startFreeStyleActivity(I)V

    .line 3604
    sget-boolean v2, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v2, :cond_4

    .line 3605
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v2, v5}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getFrontActivityMultiWindowStyle(I)Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v1

    .line 3607
    .local v1, "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    if-eqz v1, :cond_4

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3608
    invoke-virtual {v1}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v2

    if-nez v2, :cond_4

    .line 3609
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateSplitGuidePosition(ZZ)V
    invoke-static {v2, v5, v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;ZZ)V

    .line 3614
    .end local v0    # "info":Landroid/content/pm/ComponentInfo;
    .end local v1    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v3, 0xcf

    const-wide/16 v4, 0x320

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
