.class Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;
.super Ljava/lang/Object;
.source "TryMultiWindowBezel.java"

# interfaces
.implements Landroid/support/v4/widget/DrawerLayout$DrawerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->setDrawerLayoutListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrawerClosed(Landroid/view/View;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 346
    return-void
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 337
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isApp1Drawn:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup2:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$500(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder2:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 342
    :cond_0
    return-void
.end method

.method public onDrawerSlide(Landroid/view/View;F)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # F

    .prologue
    .line 331
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder1:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 332
    return-void
.end method

.method public onDrawerStateChanged(I)V
    .locals 3
    .param p1, "arg0"    # I

    .prologue
    const/16 v2, 0x8

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->isDrawerVisible(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder1:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 324
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isApp1Drawn:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder1:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 327
    :cond_0
    return-void

    .line 320
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup1:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder1:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method
