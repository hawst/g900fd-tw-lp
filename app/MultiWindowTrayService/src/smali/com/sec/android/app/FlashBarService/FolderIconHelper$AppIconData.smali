.class Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;
.super Ljava/lang/Object;
.source "FolderIconHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/FolderIconHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AppIconData"
.end annotation


# instance fields
.field private APP_ICON_CALIGN:F

.field public APP_ICON_HEIGHT:I

.field private APP_ICON_RALIGN:F

.field public APP_ICON_SCALE_FACTOR:F

.field public APP_ICON_WIDTH:I

.field public EMPTY_BITMAP:Landroid/graphics/Bitmap;

.field private FOLDER_ICON_RECT:Landroid/graphics/Rect;

.field public NORMAL_FOLDER_BITMAP:Landroid/graphics/Bitmap;

.field public OPEN_FOLDER_BITMAP:Landroid/graphics/Bitmap;

.field public OPEN_FOLDER_BITMAP_HIGHLIGHT:Landroid/graphics/Bitmap;

.field public mIconPositions:[[[F


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 6
    .param p1, "r"    # Landroid/content/res/Resources;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const v2, 0x7f0200de

    const/4 v4, 0x1

    .line 279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 280
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->FOLDER_ICON_RECT:Landroid/graphics/Rect;

    .line 281
    iput v3, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_CALIGN:F

    .line 282
    iput v3, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_RALIGN:F

    .line 284
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 285
    .local v0, "d":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->OPEN_FOLDER_BITMAP:Landroid/graphics/Bitmap;

    .line 287
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .end local v0    # "d":Landroid/graphics/drawable/BitmapDrawable;
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 288
    .restart local v0    # "d":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->OPEN_FOLDER_BITMAP_HIGHLIGHT:Landroid/graphics/Bitmap;

    .line 290
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .end local v0    # "d":Landroid/graphics/drawable/BitmapDrawable;
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 291
    .restart local v0    # "d":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->NORMAL_FOLDER_BITMAP:Landroid/graphics/Bitmap;

    .line 293
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v4, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->EMPTY_BITMAP:Landroid/graphics/Bitmap;

    .line 295
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->FOLDER_ICON_RECT:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->OPEN_FOLDER_BITMAP:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->NORMAL_FOLDER_BITMAP:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v1, v5, v5, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 298
    const v1, 0x7f0b0002

    invoke-virtual {p1, v1, v4, v4}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_SCALE_FACTOR:F

    .line 299
    const v1, 0x7f0a0097

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_HEIGHT:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_WIDTH:I

    .line 301
    invoke-virtual {p0, p1}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getAppIconPositions(Landroid/content/res/Resources;)[[[F

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->mIconPositions:[[[F

    .line 302
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    .prologue
    .line 263
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->FOLDER_ICON_RECT:Landroid/graphics/Rect;

    return-object v0
.end method

.method private getIconCordFromRes(Landroid/content/res/Resources;I)F
    .locals 2
    .param p1, "r"    # Landroid/content/res/Resources;
    .param p2, "resID"    # I

    .prologue
    .line 400
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v0, v1

    .line 401
    .local v0, "value":F
    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->resolveValue(F)F

    move-result v0

    .line 402
    return v0
.end method

.method private resolveValue(F)F
    .locals 3
    .param p1, "value"    # F

    .prologue
    const/4 v1, 0x1

    .line 413
    # getter for: Lcom/sec/android/app/FlashBarService/FolderIconHelper;->CENTER_ALIGN_VAL:I
    invoke-static {}, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->access$100()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_2

    .line 414
    iget v0, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_CALIGN:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->FOLDER_ICON_RECT:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_WIDTH:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_SCALE_FACTOR:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_CALIGN:F

    .line 419
    :cond_0
    iget p1, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_CALIGN:F

    .line 429
    :cond_1
    :goto_0
    return p1

    .line 420
    :cond_2
    # getter for: Lcom/sec/android/app/FlashBarService/FolderIconHelper;->RIGHT_ALIGN_VAL:I
    invoke-static {}, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->access$200()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 421
    iget v0, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_RALIGN:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->FOLDER_ICON_RECT:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_WIDTH:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_SCALE_FACTOR:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_RALIGN:F

    .line 426
    :cond_3
    iget p1, p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_RALIGN:F

    goto :goto_0
.end method


# virtual methods
.method public getAppIconPositions(Landroid/content/res/Resources;)[[[F
    .locals 65
    .param p1, "r"    # Landroid/content/res/Resources;

    .prologue
    .line 313
    const v60, 0x7f0a0072

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v3

    .line 314
    .local v3, "c1p1X":F
    const v60, 0x7f0a0073

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v4

    .line 317
    .local v4, "c1p1Y":F
    const v60, 0x7f0a0074

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v5

    .line 318
    .local v5, "c2p1X":F
    const v60, 0x7f0a0075

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v6

    .line 319
    .local v6, "c2p1Y":F
    const v60, 0x7f0a0076

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v7

    .line 320
    .local v7, "c2p2X":F
    const v60, 0x7f0a0077

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v8

    .line 323
    .local v8, "c2p2Y":F
    const v60, 0x7f0a0078

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v9

    .line 324
    .local v9, "c3p1X":F
    const v60, 0x7f0a0079

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v10

    .line 325
    .local v10, "c3p1Y":F
    const v60, 0x7f0a007a

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v11

    .line 326
    .local v11, "c3p2X":F
    const v60, 0x7f0a007b

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v12

    .line 327
    .local v12, "c3p2Y":F
    const v60, 0x7f0a007c

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v13

    .line 328
    .local v13, "c3p3X":F
    const v60, 0x7f0a007d

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v14

    .line 331
    .local v14, "c3p3Y":F
    const v60, 0x7f0a007e

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v15

    .line 332
    .local v15, "c4p1X":F
    const v60, 0x7f0a007f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v16

    .line 333
    .local v16, "c4p1Y":F
    const v60, 0x7f0a0080

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v17

    .line 334
    .local v17, "c4p2X":F
    const v60, 0x7f0a0081

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v18

    .line 335
    .local v18, "c4p2Y":F
    const v60, 0x7f0a0082

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v19

    .line 336
    .local v19, "c4p3X":F
    const v60, 0x7f0a0083

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v20

    .line 337
    .local v20, "c4p3Y":F
    const v60, 0x7f0a0084

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v21

    .line 338
    .local v21, "c4p4X":F
    const v60, 0x7f0a0085

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v22

    .line 344
    .local v22, "c4p4Y":F
    const v60, 0x7f0a007e

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v23

    .line 345
    .local v23, "c5p1X":F
    const v60, 0x7f0a007f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v24

    .line 346
    .local v24, "c5p1Y":F
    const v60, 0x7f0a0080

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v25

    .line 347
    .local v25, "c5p2X":F
    const v60, 0x7f0a0081

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v26

    .line 348
    .local v26, "c5p2Y":F
    const v60, 0x7f0a0082

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v27

    .line 349
    .local v27, "c5p3X":F
    const v60, 0x7f0a0083

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v28

    .line 350
    .local v28, "c5p3Y":F
    const v60, 0x7f0a0084

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v29

    .line 351
    .local v29, "c5p4X":F
    const v60, 0x7f0a0085

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v30

    .line 352
    .local v30, "c5p4Y":F
    const v60, 0x7f0a0084

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v31

    .line 353
    .local v31, "c5p5X":F
    const v60, 0x7f0a0085

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v32

    .line 357
    .local v32, "c5p5Y":F
    const v60, 0x7f0a007e

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v33

    .line 358
    .local v33, "c6p1X":F
    const v60, 0x7f0a007f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v34

    .line 359
    .local v34, "c6p1Y":F
    const v60, 0x7f0a0080

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v35

    .line 360
    .local v35, "c6p2X":F
    const v60, 0x7f0a0081

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v36

    .line 361
    .local v36, "c6p2Y":F
    const v60, 0x7f0a0082

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v37

    .line 362
    .local v37, "c6p3X":F
    const v60, 0x7f0a0083

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v38

    .line 363
    .local v38, "c6p3Y":F
    const v60, 0x7f0a0084

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v39

    .line 364
    .local v39, "c6p4X":F
    const v60, 0x7f0a0085

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v40

    .line 365
    .local v40, "c6p4Y":F
    const v60, 0x7f0a0084

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v41

    .line 366
    .local v41, "c6p5X":F
    const v60, 0x7f0a0085

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v42

    .line 367
    .local v42, "c6p5Y":F
    const v60, 0x7f0a0084

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v43

    .line 368
    .local v43, "c6p6X":F
    const v60, 0x7f0a0085

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v44

    .line 371
    .local v44, "c6p6Y":F
    const v60, 0x7f0a007e

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v45

    .line 372
    .local v45, "c7p1X":F
    const v60, 0x7f0a007f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v46

    .line 373
    .local v46, "c7p1Y":F
    const v60, 0x7f0a0080

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v47

    .line 374
    .local v47, "c7p2X":F
    const v60, 0x7f0a0081

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v48

    .line 375
    .local v48, "c7p2Y":F
    const v60, 0x7f0a0082

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v49

    .line 376
    .local v49, "c7p3X":F
    const v60, 0x7f0a0083

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v50

    .line 377
    .local v50, "c7p3Y":F
    const v60, 0x7f0a0084

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v51

    .line 378
    .local v51, "c7p4X":F
    const v60, 0x7f0a0085

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v52

    .line 379
    .local v52, "c7p4Y":F
    const v60, 0x7f0a0084

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v53

    .line 380
    .local v53, "c7p5X":F
    const v60, 0x7f0a0085

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v54

    .line 381
    .local v54, "c7p5Y":F
    const v60, 0x7f0a0084

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v55

    .line 382
    .local v55, "c7p6X":F
    const v60, 0x7f0a0085

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v56

    .line 383
    .local v56, "c7p6Y":F
    const v60, 0x7f0a0084

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v57

    .line 384
    .local v57, "c7p7X":F
    const v60, 0x7f0a0085

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v60

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->getIconCordFromRes(Landroid/content/res/Resources;I)F

    move-result v58

    .line 386
    .local v58, "c7p7Y":F
    const/16 v60, 0x7

    move/from16 v0, v60

    new-array v0, v0, [[[F

    move-object/from16 v59, v0

    const/16 v60, 0x0

    const/16 v61, 0x1

    move/from16 v0, v61

    new-array v0, v0, [[F

    move-object/from16 v61, v0

    const/16 v62, 0x0

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v3, v63, v64

    const/16 v64, 0x1

    aput v4, v63, v64

    aput-object v63, v61, v62

    aput-object v61, v59, v60

    const/16 v60, 0x1

    const/16 v61, 0x2

    move/from16 v0, v61

    new-array v0, v0, [[F

    move-object/from16 v61, v0

    const/16 v62, 0x0

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v5, v63, v64

    const/16 v64, 0x1

    aput v6, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x1

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v7, v63, v64

    const/16 v64, 0x1

    aput v8, v63, v64

    aput-object v63, v61, v62

    aput-object v61, v59, v60

    const/16 v60, 0x2

    const/16 v61, 0x3

    move/from16 v0, v61

    new-array v0, v0, [[F

    move-object/from16 v61, v0

    const/16 v62, 0x0

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v9, v63, v64

    const/16 v64, 0x1

    aput v10, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x1

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v11, v63, v64

    const/16 v64, 0x1

    aput v12, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x2

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v13, v63, v64

    const/16 v64, 0x1

    aput v14, v63, v64

    aput-object v63, v61, v62

    aput-object v61, v59, v60

    const/16 v60, 0x3

    const/16 v61, 0x4

    move/from16 v0, v61

    new-array v0, v0, [[F

    move-object/from16 v61, v0

    const/16 v62, 0x0

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v15, v63, v64

    const/16 v64, 0x1

    aput v16, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x1

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v17, v63, v64

    const/16 v64, 0x1

    aput v18, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x2

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v19, v63, v64

    const/16 v64, 0x1

    aput v20, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x3

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v21, v63, v64

    const/16 v64, 0x1

    aput v22, v63, v64

    aput-object v63, v61, v62

    aput-object v61, v59, v60

    const/16 v60, 0x4

    const/16 v61, 0x5

    move/from16 v0, v61

    new-array v0, v0, [[F

    move-object/from16 v61, v0

    const/16 v62, 0x0

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v23, v63, v64

    const/16 v64, 0x1

    aput v24, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x1

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v25, v63, v64

    const/16 v64, 0x1

    aput v26, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x2

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v27, v63, v64

    const/16 v64, 0x1

    aput v28, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x3

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v29, v63, v64

    const/16 v64, 0x1

    aput v30, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x4

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v31, v63, v64

    const/16 v64, 0x1

    aput v32, v63, v64

    aput-object v63, v61, v62

    aput-object v61, v59, v60

    const/16 v60, 0x5

    const/16 v61, 0x6

    move/from16 v0, v61

    new-array v0, v0, [[F

    move-object/from16 v61, v0

    const/16 v62, 0x0

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v33, v63, v64

    const/16 v64, 0x1

    aput v34, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x1

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v35, v63, v64

    const/16 v64, 0x1

    aput v36, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x2

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v37, v63, v64

    const/16 v64, 0x1

    aput v38, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x3

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v39, v63, v64

    const/16 v64, 0x1

    aput v40, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x4

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v41, v63, v64

    const/16 v64, 0x1

    aput v42, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x5

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v43, v63, v64

    const/16 v64, 0x1

    aput v44, v63, v64

    aput-object v63, v61, v62

    aput-object v61, v59, v60

    const/16 v60, 0x6

    const/16 v61, 0x7

    move/from16 v0, v61

    new-array v0, v0, [[F

    move-object/from16 v61, v0

    const/16 v62, 0x0

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v45, v63, v64

    const/16 v64, 0x1

    aput v46, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x1

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v47, v63, v64

    const/16 v64, 0x1

    aput v48, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x2

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v49, v63, v64

    const/16 v64, 0x1

    aput v50, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x3

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v51, v63, v64

    const/16 v64, 0x1

    aput v52, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x4

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v53, v63, v64

    const/16 v64, 0x1

    aput v54, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x5

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v55, v63, v64

    const/16 v64, 0x1

    aput v56, v63, v64

    aput-object v63, v61, v62

    const/16 v62, 0x6

    const/16 v63, 0x2

    move/from16 v0, v63

    new-array v0, v0, [F

    move-object/from16 v63, v0

    const/16 v64, 0x0

    aput v57, v63, v64

    const/16 v64, 0x1

    aput v58, v63, v64

    aput-object v63, v61, v62

    aput-object v61, v59, v60

    .line 396
    .local v59, "iconPositions":[[[F
    return-object v59
.end method
