.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$11;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->makeHistoryBarDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 1013
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1017
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1018
    .local v0, "taskId":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mActivityManager:Landroid/app/ActivityManager;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/ActivityManager;->removeTask(II)Z

    .line 1019
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->setRemoveTask(I)V

    .line 1020
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v2, 0xcf

    const-wide/16 v4, 0x320

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1021
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissHistoryWindow(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)V

    .line 1022
    return-void
.end method
