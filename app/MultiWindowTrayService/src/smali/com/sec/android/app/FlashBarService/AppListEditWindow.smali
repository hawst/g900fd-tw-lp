.class public Lcom/sec/android/app/FlashBarService/AppListEditWindow;
.super Ljava/lang/Object;
.source "AppListEditWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;,
        Lcom/sec/android/app/FlashBarService/AppListEditWindow$ViewHolder;
    }
.end annotation


# instance fields
.field private final APPLIST_DRAG_ZONE:I

.field private final APPLIST_LONG_PRESS:I

.field private final APPLIST_TIMER_LONG_PRESS:I

.field mAppClickListener:Landroid/view/View$OnClickListener;

.field mAppEditListDragListener:Landroid/view/View$OnDragListener;

.field private mAppListDragIndex:I

.field mAppListEditKeyListener:Landroid/view/View$OnKeyListener;

.field private mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

.field mAppLongClickListener:Landroid/view/View$OnLongClickListener;

.field mAppTouchListener:Landroid/view/View$OnTouchListener;

.field private mContext:Landroid/content/Context;

.field private mCurAppListPos:I

.field private mCurrentDownEvent:Landroid/view/MotionEvent;

.field private mCurrentDownView:Landroid/view/View;

.field private mDisplayHeight:I

.field private mDisplayOrientation:I

.field private mDisplayWidth:I

.field private mEditDragIndex:I

.field private mEditGridView:Landroid/widget/GridView;

.field private mEditListAdapter:Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;

.field private mEditingDragView:Landroid/widget/ImageView;

.field private mEmptyText:Landroid/widget/TextView;

.field private mHelpBtn:Landroid/widget/Button;

.field private mIvt:[B

.field private mMainFrame:Landroid/widget/FrameLayout;

.field private mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

.field private mResetBtn:Landroid/widget/Button;

.field private mResetClickListener:Landroid/view/View$OnClickListener;

.field mTimerHandler:Landroid/os/Handler;

.field private mVibrator:Landroid/os/SystemVibrator;

.field private mWindowAppEditList:Landroid/view/Window;

.field private mWindowManager:Landroid/view/WindowManager;

.field private mbStartDragFromEdit:Z

.field private res:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listwindow"    # Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    .param p3, "appInfo"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .prologue
    const/4 v2, -0x1

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->APPLIST_TIMER_LONG_PRESS:I

    .line 73
    const/16 v0, 0xca

    iput v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->APPLIST_LONG_PRESS:I

    .line 74
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->APPLIST_DRAG_ZONE:I

    .line 85
    const/16 v0, 0x26

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mIvt:[B

    .line 103
    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListDragIndex:I

    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mbStartDragFromEdit:Z

    .line 106
    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditDragIndex:I

    .line 107
    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mCurAppListPos:I

    .line 454
    new-instance v0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow$3;-><init>(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mTimerHandler:Landroid/os/Handler;

    .line 575
    new-instance v0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;-><init>(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppEditListDragListener:Landroid/view/View$OnDragListener;

    .line 609
    new-instance v0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow$5;-><init>(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppClickListener:Landroid/view/View$OnClickListener;

    .line 619
    new-instance v0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow$6;-><init>(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppTouchListener:Landroid/view/View$OnTouchListener;

    .line 648
    new-instance v0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow$7;-><init>(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 682
    new-instance v0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow$8;-><init>(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListEditKeyListener:Landroid/view/View$OnKeyListener;

    .line 692
    new-instance v0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow$9;-><init>(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mResetClickListener:Landroid/view/View$OnClickListener;

    .line 135
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mContext:Landroid/content/Context;

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowManager:Landroid/view/WindowManager;

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    .line 139
    new-instance v0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;-><init>(Lcom/sec/android/app/FlashBarService/AppListEditWindow;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditListAdapter:Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;

    .line 140
    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditDragIndex:I

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mContext:Landroid/content/Context;

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/SystemVibrator;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mVibrator:Landroid/os/SystemVibrator;

    .line 143
    iput-object p3, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .line 144
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    .line 145
    return-void

    .line 85
    nop

    :array_0
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0xat
        0x0t
        0x14t
        0x0t
        0x0t
        0x0t
        0x20t
        0x1t
        0x0t
        0x0t
        0x7ft
        0x0t
        0x0t
        0x42t
        0x0t
        0x0t
        0x4dt
        0x0t
        0x61t
        0x0t
        0x67t
        0x0t
        0x53t
        0x0t
        0x77t
        0x0t
        0x65t
        0x0t
        0x65t
        0x0t
        0x70t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditListAdapter:Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/widget/GridView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/view/MotionEvent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/FlashBarService/AppListEditWindow;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mCurrentDownView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/FlashBarService/AppListEditWindow;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mCurrentDownView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditingDragView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/FlashBarService/AppListEditWindow;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditingDragView:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditDragIndex:I

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/FlashBarService/AppListEditWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    .param p1, "x1"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditDragIndex:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mbStartDragFromEdit:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/FlashBarService/AppListEditWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mbStartDragFromEdit:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListDragIndex:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/FlashBarService/AppListEditWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    .param p1, "x1"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListDragIndex:I

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mIvt:[B

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/os/SystemVibrator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mVibrator:Landroid/os/SystemVibrator;

    return-object v0
.end method

.method private displayEmptyText()V
    .locals 2

    .prologue
    .line 415
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditListAdapter:Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEmptyText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 420
    :goto_0
    return-void

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEmptyText:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private makeAppListEditLayout(Z)V
    .locals 13
    .param p1, "more"    # Z

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/16 v10, 0x8

    .line 165
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowAppEditList:Landroid/view/Window;

    const v8, 0x7f0f0051

    invoke-virtual {v7, v8}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    .line 166
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowAppEditList:Landroid/view/Window;

    const v8, 0x7f0f0053

    invoke-virtual {v7, v8}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/GridView;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    .line 167
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowAppEditList:Landroid/view/Window;

    const v8, 0x7f0f0052

    invoke-virtual {v7, v8}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEmptyText:Landroid/widget/TextView;

    .line 168
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowAppEditList:Landroid/view/Window;

    const v8, 0x7f0f0056

    invoke-virtual {v7, v8}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mHelpBtn:Landroid/widget/Button;

    .line 169
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowAppEditList:Landroid/view/Window;

    const v8, 0x7f0f0055

    invoke-virtual {v7, v8}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mResetBtn:Landroid/widget/Button;

    .line 171
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 173
    .local v5, "pm":Landroid/content/pm/PackageManager;
    const/4 v4, 0x0

    .line 176
    .local v4, "mHelphubVersion":I
    :try_start_0
    const-string v7, "com.android.vending"

    const/4 v8, 0x1

    invoke-virtual {v5, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 177
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const-string v8, "com.samsung.helphub"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 178
    .local v2, "info":Landroid/content/pm/PackageInfo;
    if-eqz v2, :cond_0

    .line 179
    iget v7, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    rem-int/lit8 v4, v7, 0xa
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    .end local v2    # "info":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    if-ne v4, v12, :cond_4

    .line 188
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mHelpBtn:Landroid/widget/Button;

    invoke-virtual {v7, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 228
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowAppEditList:Landroid/view/Window;

    const v8, 0x7f0f0054

    invoke-virtual {v7, v8}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 229
    .local v1, "helpBtnLayout":Landroid/widget/LinearLayout;
    if-eqz p1, :cond_b

    .line 230
    const-string v7, "com.android.vending"

    invoke-virtual {v5, v7}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v7

    const/4 v8, 0x2

    if-eq v7, v8, :cond_1

    const-string v7, "com.android.vending"

    invoke-virtual {v5, v7}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v7

    const/4 v8, 0x3

    if-ne v7, v8, :cond_9

    .line 232
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mResetBtn:Landroid/widget/Button;

    if-nez v7, :cond_8

    if-eqz v1, :cond_8

    .line 233
    invoke-virtual {v1, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 258
    :cond_2
    :goto_2
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/GridView;->setVisibility(I)V

    .line 260
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppEditListDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v7, v8}, Landroid/widget/GridView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 261
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppEditListDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 262
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListEditKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 264
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowAppEditList:Landroid/view/Window;

    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 265
    .local v3, "l":Landroid/view/WindowManager$LayoutParams;
    iput v11, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v11, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v11, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v11, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 266
    const/16 v7, 0x33

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 267
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowAppEditList:Landroid/view/Window;

    invoke-virtual {v7, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 268
    const-string v7, "AppListEdit"

    invoke-virtual {v3, v7}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 270
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    iget v7, v7, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mSystemUiVisibility:I

    if-nez v7, :cond_3

    .line 271
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v8, 0x1050010

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 272
    .local v6, "statusBarHeight":I
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v8}, Landroid/widget/FrameLayout;->getPaddingLeft()I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getPaddingTop()I

    move-result v9

    add-int/2addr v9, v6

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v10}, Landroid/widget/FrameLayout;->getPaddingRight()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v11}, Landroid/widget/FrameLayout;->getPaddingBottom()I

    move-result v11

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 276
    .end local v6    # "statusBarHeight":I
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowAppEditList:Landroid/view/Window;

    invoke-virtual {v8}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v8

    invoke-interface {v7, v8, v3}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 277
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    invoke-virtual {v7}, Landroid/widget/GridView;->requestFocus()Z

    .line 278
    return-void

    .line 182
    .end local v1    # "helpBtnLayout":Landroid/widget/LinearLayout;
    .end local v3    # "l":Landroid/view/WindowManager$LayoutParams;
    :catch_0
    move-exception v0

    .line 183
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 p1, 0x0

    .line 184
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mHelpBtn:Landroid/widget/Button;

    invoke-virtual {v7, v10}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    .line 189
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_4
    if-eqz p1, :cond_6

    .line 190
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mResetBtn:Landroid/widget/Button;

    if-eqz v7, :cond_5

    .line 191
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mResetBtn:Landroid/widget/Button;

    invoke-virtual {v7, v11}, Landroid/widget/Button;->setVisibility(I)V

    .line 192
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mResetBtn:Landroid/widget/Button;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mContext:Landroid/content/Context;

    const v9, 0x7f08004a

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 193
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mResetBtn:Landroid/widget/Button;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mResetClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mHelpBtn:Landroid/widget/Button;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mContext:Landroid/content/Context;

    const v9, 0x7f080005

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mHelpBtn:Landroid/widget/Button;

    new-instance v8, Lcom/sec/android/app/FlashBarService/AppListEditWindow$1;

    invoke-direct {v8, p0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow$1;-><init>(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)V

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 212
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mResetBtn:Landroid/widget/Button;

    if-eqz v7, :cond_7

    .line 213
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mResetBtn:Landroid/widget/Button;

    invoke-virtual {v7, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 215
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mHelpBtn:Landroid/widget/Button;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mContext:Landroid/content/Context;

    const v9, 0x7f080004

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mHelpBtn:Landroid/widget/Button;

    new-instance v8, Lcom/sec/android/app/FlashBarService/AppListEditWindow$2;

    invoke-direct {v8, p0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow$2;-><init>(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)V

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 235
    .restart local v1    # "helpBtnLayout":Landroid/widget/LinearLayout;
    :cond_8
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mHelpBtn:Landroid/widget/Button;

    invoke-virtual {v7, v10}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_2

    .line 238
    :cond_9
    if-eq v4, v12, :cond_2

    .line 239
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mResetBtn:Landroid/widget/Button;

    if-nez v7, :cond_a

    if-eqz v1, :cond_a

    .line 240
    invoke-virtual {v1, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 242
    :cond_a
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mHelpBtn:Landroid/widget/Button;

    invoke-virtual {v7, v10}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_2

    .line 248
    :cond_b
    :try_start_1
    const-string v7, "com.samsung.helphub"

    const/4 v8, 0x1

    invoke-virtual {v5, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    .line 249
    :catch_1
    move-exception v0

    .line 250
    .restart local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mResetBtn:Landroid/widget/Button;

    if-nez v7, :cond_c

    if-eqz v1, :cond_c

    .line 251
    invoke-virtual {v1, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 253
    :cond_c
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mHelpBtn:Landroid/widget/Button;

    invoke-virtual {v7, v10}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_2
.end method


# virtual methods
.method public displayAppListEditWindow(IZ)V
    .locals 12
    .param p1, "appListPosition"    # I
    .param p2, "bAni"    # Z

    .prologue
    .line 297
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowAppEditList:Landroid/view/Window;

    if-eqz v6, :cond_4

    .line 299
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowAppEditList:Landroid/view/Window;

    invoke-virtual {v6}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 300
    .local v2, "l":Landroid/view/WindowManager$LayoutParams;
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mCurAppListPos:I

    .line 301
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 302
    .local v1, "fullscreen":Landroid/graphics/Point;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 303
    iget v6, v1, Landroid/graphics/Point;->x:I

    iput v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mDisplayWidth:I

    .line 304
    iget v6, v1, Landroid/graphics/Point;->y:I

    iput v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mDisplayHeight:I

    .line 306
    const/4 v6, 0x0

    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 307
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->getApplistIndicatorSize()I

    move-result v6

    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 308
    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mDisplayWidth:I

    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 309
    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mDisplayHeight:I

    iget v7, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    sub-int/2addr v6, v7

    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 311
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    iput v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mDisplayOrientation:I

    .line 313
    const/16 v6, 0x67

    if-eq p1, v6, :cond_0

    const/16 v6, 0x68

    if-ne p1, v6, :cond_8

    .line 315
    :cond_0
    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mDisplayOrientation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_5

    .line 316
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v8, 0x7f070009

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 317
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    const v7, 0x7f02007c

    invoke-virtual {v6, v7}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 323
    :goto_0
    const/16 v6, 0x67

    if-ne p1, v6, :cond_6

    .line 324
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v8, 0x7f0a001d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 325
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v8, 0x7f0a003f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v9, 0x7f0a0041

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v10, 0x7f0a0040

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v11, 0x7f0a0042

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/widget/GridView;->setPadding(IIII)V

    .line 339
    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mResetBtn:Landroid/widget/Button;

    if-eqz v6, :cond_2

    .line 340
    new-instance v3, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mResetBtn:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 341
    .local v3, "resetBtnLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v7, 0x7f0a0026

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 343
    .local v4, "shadowMargin":I
    const/16 v6, 0x67

    if-ne p1, v6, :cond_7

    .line 344
    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 348
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mResetBtn:Landroid/widget/Button;

    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v7, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 371
    .end local v3    # "resetBtnLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v4    # "shadowMargin":I
    :cond_2
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    iget v6, v6, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mSystemUiVisibility:I

    if-nez v6, :cond_3

    .line 372
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v7, 0x1050010

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 373
    .local v5, "statusBarHeight":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getPaddingLeft()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v8}, Landroid/widget/FrameLayout;->getPaddingTop()I

    move-result v8

    add-int/2addr v8, v5

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getPaddingRight()I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v10}, Landroid/widget/FrameLayout;->getPaddingBottom()I

    move-result v10

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 377
    .end local v5    # "statusBarHeight":I
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowAppEditList:Landroid/view/Window;

    invoke-virtual {v6, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 378
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowAppEditList:Landroid/view/Window;

    invoke-virtual {v7}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v7

    invoke-interface {v6, v7, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 379
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/GridView;->setVisibility(I)V

    .line 380
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditListAdapter:Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;

    invoke-virtual {v6, v7}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 381
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->displayEmptyText()V

    .line 383
    const/4 v0, 0x0

    .line 384
    .local v0, "ani":Landroid/view/animation/Animation;
    const/16 v6, 0x67

    if-ne p1, v6, :cond_c

    .line 385
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mContext:Landroid/content/Context;

    const v7, 0x7f040047

    invoke-static {v6, v7}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 391
    :goto_4
    if-eqz p2, :cond_4

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v6, v0}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 393
    .end local v0    # "ani":Landroid/view/animation/Animation;
    .end local v1    # "fullscreen":Landroid/graphics/Point;
    .end local v2    # "l":Landroid/view/WindowManager$LayoutParams;
    :cond_4
    return-void

    .line 319
    .restart local v1    # "fullscreen":Landroid/graphics/Point;
    .restart local v2    # "l":Landroid/view/WindowManager$LayoutParams;
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v8, 0x7f070008

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 320
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    const v7, 0x7f02007b

    invoke-virtual {v6, v7}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 329
    :cond_6
    const/16 v6, 0x68

    if-ne p1, v6, :cond_1

    .line 330
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v8, 0x7f0a0025

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v10, 0x7f0a001d

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v11, 0x7f0a0025

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    sub-int/2addr v9, v10

    const/4 v10, 0x0

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 333
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v8, 0x7f0a0040

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v9, 0x7f0a0041

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v10, 0x7f0a003f

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v11, 0x7f0a0042

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/widget/GridView;->setPadding(IIII)V

    goto/16 :goto_1

    .line 346
    .restart local v3    # "resetBtnLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v4    # "shadowMargin":I
    :cond_7
    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto/16 :goto_2

    .line 351
    .end local v3    # "resetBtnLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v4    # "shadowMargin":I
    :cond_8
    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mDisplayOrientation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_a

    .line 352
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 353
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    const v7, 0x7f02007c

    invoke-virtual {v6, v7}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 359
    :goto_5
    const/16 v6, 0x65

    if-ne p1, v6, :cond_b

    .line 360
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v9, 0x7f0a001d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 365
    :cond_9
    :goto_6
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v8, 0x7f0a0043

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v9, 0x7f0a0045

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v10, 0x7f0a0044

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v11, 0x7f0a0046

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/widget/GridView;->setPadding(IIII)V

    goto/16 :goto_3

    .line 355
    :cond_a
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 356
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    const v7, 0x7f02007b

    invoke-virtual {v6, v7}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    goto :goto_5

    .line 361
    :cond_b
    const/16 v6, 0x66

    if-ne p1, v6, :cond_9

    .line 362
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    const v11, 0x7f0a001d

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    goto :goto_6

    .line 386
    .restart local v0    # "ani":Landroid/view/animation/Animation;
    :cond_c
    const/16 v6, 0x68

    if-ne p1, v6, :cond_d

    .line 387
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mContext:Landroid/content/Context;

    const v7, 0x7f040048

    invoke-static {v6, v7}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto/16 :goto_4

    .line 389
    :cond_d
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mContext:Landroid/content/Context;

    const v7, 0x7f040046

    invoke-static {v6, v7}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto/16 :goto_4
.end method

.method public getEditGridViewFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 427
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v0

    .line 430
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getImageViewByIndex(I)Landroid/widget/ImageView;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 447
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getEditGridViewFirstVisiblePosition()I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 448
    .local v0, "tmpView":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 449
    const v1, 0x7f0f0059

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 452
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItemIndex(Landroid/view/View;)I
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 433
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    invoke-virtual {v3}, Landroid/widget/GridView;->getCount()I

    move-result v0

    .line 434
    .local v0, "cnt":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_2

    .line 435
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    invoke-virtual {v3, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 436
    .local v2, "tmpView":Landroid/widget/LinearLayout;
    if-eqz v2, :cond_1

    .line 437
    const v3, 0x7f0f0059

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-ne p1, v3, :cond_0

    .line 438
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getEditGridViewFirstVisiblePosition()I

    move-result v3

    add-int/2addr v3, v1

    .line 443
    .end local v2    # "tmpView":Landroid/widget/LinearLayout;
    :goto_1
    return v3

    .line 439
    .restart local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_0
    const v3, 0x7f0f005c

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-ne p1, v3, :cond_1

    .line 440
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getEditGridViewFirstVisiblePosition()I

    move-result v3

    add-int/2addr v3, v1

    goto :goto_1

    .line 434
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 443
    .end local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_2
    const/4 v3, -0x1

    goto :goto_1
.end method

.method public getItemIndexFromEditList()I
    .locals 1

    .prologue
    .line 404
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditDragIndex:I

    return v0
.end method

.method public getMainFrame()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMainFrame:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public getStartDragFromEdit()Z
    .locals 1

    .prologue
    .line 408
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mbStartDragFromEdit:Z

    return v0
.end method

.method public initAppListEditWindow()V
    .locals 1

    .prologue
    .line 293
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mbStartDragFromEdit:Z

    .line 294
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowAppEditList:Landroid/view/Window;

    .line 295
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 282
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowAppEditList:Landroid/view/Window;

    if-eqz v1, :cond_0

    .line 283
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 284
    .local v0, "fullscreen":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 285
    iget v1, v0, Landroid/graphics/Point;->x:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mDisplayWidth:I

    .line 286
    iget v1, v0, Landroid/graphics/Point;->y:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mDisplayHeight:I

    .line 288
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mCurAppListPos:I

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->displayAppListEditWindow(IZ)V

    .line 290
    .end local v0    # "fullscreen":Landroid/graphics/Point;
    :cond_0
    return-void
.end method

.method public setItemIndexFromAppList(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 396
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListDragIndex:I

    .line 397
    return-void
.end method

.method public setStartDragFromEdit(Z)V
    .locals 0
    .param p1, "bEnd"    # Z

    .prologue
    .line 411
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mbStartDragFromEdit:Z

    .line 412
    return-void
.end method

.method public setWindow(Landroid/view/Window;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Z)V
    .locals 2
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "info"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    .param p3, "more"    # Z

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowAppEditList:Landroid/view/Window;

    .line 150
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 151
    .local v0, "fullscreen":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 152
    iget v1, v0, Landroid/graphics/Point;->x:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mDisplayWidth:I

    .line 153
    iget v1, v0, Landroid/graphics/Point;->y:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mDisplayHeight:I

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->res:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mDisplayOrientation:I

    .line 157
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .line 159
    invoke-direct {p0, p3}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->makeAppListEditLayout(Z)V

    .line 161
    return-void
.end method

.method public updateAppListEditContents()V
    .locals 2

    .prologue
    .line 699
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->updateEditListChanged()V

    .line 700
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditListAdapter:Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 701
    return-void
.end method

.method public updateEditListChanged()V
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditListAdapter:Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;->notifyDataSetChanged()V

    .line 424
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->displayEmptyText()V

    .line 425
    return-void
.end method
