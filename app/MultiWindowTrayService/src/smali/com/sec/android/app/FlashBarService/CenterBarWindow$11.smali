.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$11;
.super Ljava/lang/Object;
.source "CenterBarWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;->makeButtonPopupLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0

    .prologue
    .line 1712
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v3, 0xcd

    .line 1714
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportStyleTransition(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1724
    :cond_0
    :goto_0
    return-void

    .line 1717
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3700(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/os/SystemVibrator;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIvt:[B
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3600(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3700(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/os/SystemVibrator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 1718
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1719
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1720
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3800(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->changeTaskToCascade(Landroid/graphics/Point;IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1723
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideButtonsPopupWindow(Z)V

    goto :goto_0
.end method
