.class Lcom/sec/android/app/FlashBarService/AppListEditWindow$2;
.super Ljava/lang/Object;
.source "AppListEditWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListEditWindow;->makeAppListEditLayout(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 218
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$000(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->hideAppEditList()V

    .line 219
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$000(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->updateFlashBarState(ZZ)Z

    .line 220
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.helphub.HELP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 221
    .local v0, "helpIntent":Landroid/content/Intent;
    const/high16 v1, 0x30200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 222
    const-string v1, "helphub:section"

    const-string v2, "multi_window"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 223
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$100(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 224
    return-void
.end method
