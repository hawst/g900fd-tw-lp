.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 1696
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 11
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 1698
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    .line 1699
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 1848
    const-string v7, "DragDrop"

    const-string v8, "Unknown action type received by OnDragListener."

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1851
    .end local p1    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    const/4 v7, 0x1

    return v7

    .line 1701
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_0
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->findeCurIndex(Landroid/view/View;)I
    invoke-static {v8, p1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/view/View;)I

    move-result v8

    if-ne v7, v8, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v7

    if-nez v7, :cond_0

    .line 1703
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v7, 0x7f0f0059

    invoke-virtual {p1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    move-object v5, v7

    check-cast v5, Landroid/widget/ImageView;

    .line 1705
    .local v5, "orgView":Landroid/widget/ImageView;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4202(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 1706
    const v7, 0x7f02007f

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1707
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIvt:[B
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)[B

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v9}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 1709
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setItemIndexFromAppList(I)V

    .line 1710
    sget-boolean v7, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v7, :cond_1

    .line 1712
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 1713
    .local v3, "l":Landroid/view/WindowManager$LayoutParams;
    const/4 v7, 0x0

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1714
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v7

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1715
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1716
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v8

    sub-int/2addr v7, v8

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 1717
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1718
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/Window;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v8

    invoke-interface {v7, v8, v3}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1719
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1721
    .end local v3    # "l":Landroid/view/WindowManager$LayoutParams;
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->cancelCollapseTimer()V
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    goto/16 :goto_0

    .line 1726
    .end local v5    # "orgView":Landroid/widget/ImageView;
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListDragMode:Z
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4602(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 1729
    sget-boolean v7, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v7, :cond_2

    .line 1730
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopGuideline:Landroid/widget/FrameLayout;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/FrameLayout;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1731
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBottomGuideline:Landroid/widget/FrameLayout;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/FrameLayout;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1733
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/GuideLineImageView;->setVisibility(I)V

    .line 1734
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1735
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddEmptyItemPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1736
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppCnt()I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v9

    const/4 v10, 0x6

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->changlistItemFromEditList(III)Z
    invoke-static {v7, v8, v9, v10}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;III)Z

    .line 1737
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddEmptyItemPosition:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5102(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    goto/16 :goto_0

    .line 1742
    :pswitch_2
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1743
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->decideScrollMove(Landroid/view/View;Landroid/view/DragEvent;)Z
    invoke-static {v7, p1, p2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/view/View;Landroid/view/DragEvent;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1747
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->findeCurIndex(Landroid/view/View;)I
    invoke-static {v8, p1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/view/View;)I

    move-result v8

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 1750
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_0

    .line 1751
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v7

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1753
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v8

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddToListItem:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 1754
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppCnt()I

    move-result v8

    if-ge v7, v8, :cond_4

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v8

    if-eq v7, v8, :cond_4

    .line 1755
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->changlistItemFromEditList(III)Z
    invoke-static {v7, v8, v9, v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;III)Z

    .line 1768
    :cond_3
    :goto_1
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v7, 0x7f0f0059

    invoke-virtual {p1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    move-object v1, v7

    check-cast v1, Landroid/widget/ImageView;

    .line 1769
    .local v1, "blankView":Landroid/widget/ImageView;
    const v7, 0x7f02007f

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 1758
    .end local v1    # "blankView":Landroid/widget/ImageView;
    .restart local p1    # "view":Landroid/view/View;
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppCnt()I

    move-result v8

    if-ne v7, v8, :cond_3

    .line 1759
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->changlistItemFromEditList(III)Z
    invoke-static {v7, v8, v9, v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;III)Z

    goto :goto_1

    .line 1763
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->changelistItem(II)Z
    invoke-static {v7, v8, v9}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;II)Z

    .line 1764
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setItemIndexFromAppList(I)V

    goto :goto_1

    .line 1775
    :pswitch_3
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v7

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->checkAppListLayout()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1776
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListRelayout(Z)V

    .line 1777
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4002(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 1778
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setStartDragFromEdit(Z)V

    .line 1779
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4202(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 1780
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 1783
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddToListItem:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_7

    .line 1784
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddToListItem:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddToListItem:I
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v9

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v10}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->changeApplistItem(IIZ)V

    .line 1785
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddToListItem:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 1786
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListRelayout(Z)V

    .line 1789
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->findeCurIndex(Landroid/view/View;)I
    invoke-static {v7, p1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/view/View;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v8

    if-ne v7, v8, :cond_9

    .line 1790
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v7, 0x7f0f0059

    invoke-virtual {p1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    move-object v4, v7

    check-cast v4, Landroid/widget/ImageView;

    .line 1791
    .local v4, "org":Landroid/widget/ImageView;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    if-eqz v7, :cond_8

    .line 1792
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1793
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4202(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 1795
    :cond_8
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4002(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 1796
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 1799
    .end local v4    # "org":Landroid/widget/ImageView;
    :cond_9
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1800
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 1805
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_4
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v7

    if-eqz v7, :cond_b

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v7

    if-eqz v7, :cond_a

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->doOneTimeAfterDragStarts:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1806
    :cond_a
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->doOneTimeAfterDragStarts:Z
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5602(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 1807
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v7, 0x7f0f0059

    invoke-virtual {p1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    move-object v1, v7

    check-cast v1, Landroid/widget/ImageView;

    .line 1808
    .restart local v1    # "blankView":Landroid/widget/ImageView;
    const v7, 0x7f02007f

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1810
    .end local v1    # "blankView":Landroid/widget/ImageView;
    :cond_b
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1813
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->changlistItemFromEditList(III)Z
    invoke-static {v7, v8, v9, v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;III)Z

    .line 1814
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 1815
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4002(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    goto/16 :goto_0

    .line 1819
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_5
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListDragMode:Z
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4602(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 1821
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v7

    if-eqz v7, :cond_c

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_c

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v7

    if-eqz v7, :cond_c

    .line 1822
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->addAndChangelistItem(II)Z
    invoke-static {v7, v8, v9}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;II)Z

    .line 1823
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIvt:[B
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)[B

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v9}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 1824
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    const/4 v8, -0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setItemIndexFromAppList(I)V

    .line 1825
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddToListItem:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 1826
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->updateEditListChanged()V

    .line 1828
    :cond_c
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_d

    .line 1829
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f02007f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1830
    .local v2, "curIcon":Landroid/graphics/drawable/Drawable;
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v7, 0x7f0f0059

    invoke-virtual {p1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    move-object v6, v7

    check-cast v6, Landroid/widget/ImageView;

    .line 1832
    .local v6, "tmpiv":Landroid/widget/ImageView;
    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    if-ne v7, v2, :cond_d

    .line 1833
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1834
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4202(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 1837
    .end local v2    # "curIcon":Landroid/graphics/drawable/Drawable;
    .end local v6    # "tmpiv":Landroid/widget/ImageView;
    :cond_d
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 1838
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 1840
    :cond_e
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v7

    if-eqz v7, :cond_f

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v7

    if-eqz v7, :cond_f

    .line 1841
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 1842
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4002(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 1844
    :cond_f
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startCollapseTimer()V
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    goto/16 :goto_0

    .line 1699
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method
