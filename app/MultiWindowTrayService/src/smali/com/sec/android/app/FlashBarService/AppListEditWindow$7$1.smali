.class Lcom/sec/android/app/FlashBarService/AppListEditWindow$7$1;
.super Landroid/view/View$DragShadowBuilder;
.source "AppListEditWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListEditWindow$7;->onLongClick(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/FlashBarService/AppListEditWindow$7;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListEditWindow$7;Landroid/view/View;)V
    .locals 0
    .param p2, "x0"    # Landroid/view/View;

    .prologue
    .line 660
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$7$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListEditWindow$7;

    invoke-direct {p0, p2}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onDrawShadow(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 668
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$7$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListEditWindow$7;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListEditWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditingDragView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$400(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/widget/ImageView;

    move-result-object v0

    .line 669
    .local v0, "view":Landroid/view/View;
    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 670
    return-void
.end method

.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 4
    .param p1, "shadowSize"    # Landroid/graphics/Point;
    .param p2, "shadowTouchPoint"    # Landroid/graphics/Point;

    .prologue
    .line 662
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$7$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListEditWindow$7;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListEditWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditingDragView:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$400(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    .line 663
    .local v1, "ShadowWidth":I
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$7$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListEditWindow$7;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListEditWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditingDragView:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$400(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    .line 664
    .local v0, "ShadowHeight":I
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Point;->set(II)V

    .line 665
    mul-int/lit8 v2, v1, 0x3

    div-int/lit8 v2, v2, 0x4

    mul-int/lit8 v3, v0, 0x3

    div-int/lit8 v3, v3, 0x4

    invoke-virtual {p2, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 666
    return-void
.end method
