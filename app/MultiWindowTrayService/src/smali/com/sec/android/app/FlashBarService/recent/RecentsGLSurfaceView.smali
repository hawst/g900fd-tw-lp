.class public Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;
.super Landroid/opengl/GLSurfaceView;
.source "RecentsGLSurfaceView.java"

# interfaces
.implements Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

.field private mRecentCallback:Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;

.field mRenderer:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->setEGLContextClientVersion(I)V

    .line 29
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->setEGLConfigChooser(Z)V

    .line 31
    new-instance v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-direct {v0, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mRenderer:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mRenderer:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 34
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->setRenderMode(I)V

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->update()V

    return-void
.end method

.method public static drawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v5, 0x0

    .line 71
    if-nez p0, :cond_0

    .line 72
    const/4 v0, 0x0

    .line 83
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    .local v1, "canvas":Landroid/graphics/Canvas;
    :goto_0
    return-object v0

    .line 74
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    :cond_0
    instance-of v2, p0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_1

    .line 75
    check-cast p0, Landroid/graphics/drawable/BitmapDrawable;

    .end local p0    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 78
    .restart local p0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 79
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 80
    .restart local v1    # "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    invoke-virtual {p0, v5, v5, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 81
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private update()V
    .locals 6

    .prologue
    .line 87
    const-string v3, "MIK"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "update: list size = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->getCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mRenderer:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->clearTaskList()V

    .line 89
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->getCount()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 90
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    .line 92
    .local v2, "td":Lcom/sec/android/app/FlashBarService/recent/TaskDescription;
    const-string v3, "MIK"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "update: i = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", name = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", icon = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", th = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getThumbnail()Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    new-instance v1, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mRenderer:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-direct {v1, v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;-><init>(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)V

    .line 95
    .local v1, "t":Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;
    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->setTaskDescription(Lcom/sec/android/app/FlashBarService/recent/TaskDescription;)V

    .line 97
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mRenderer:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->addTask(Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;)V

    .line 98
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mRenderer:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->requestTaskInit(Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;)V

    .line 89
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 100
    .end local v1    # "t":Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;
    .end local v2    # "td":Lcom/sec/android/app/FlashBarService/recent/TaskDescription;
    :cond_0
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mRenderer:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 40
    const/4 v0, 0x1

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 138
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onAttachedToWindow()V

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->onResume()V

    .line 140
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 144
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onDetachedFromWindow()V

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->onPause()V

    .line 146
    return-void
.end method

.method public onTasksLoaded()V
    .locals 2

    .prologue
    .line 45
    const-string v0, "MIK"

    const-string v1, "onTasksLoaded"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->update()V

    .line 47
    return-void
.end method

.method public setAdapter(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    new-instance v1, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView$1;-><init>(Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 68
    return-void
.end method

.method public setCallback(Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mRecentCallback:Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mRenderer:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->setCallback(Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;)V

    .line 106
    return-void
.end method

.method public setMinSwipeAlpha(F)V
    .locals 0
    .param p1, "minAlpha"    # F

    .prologue
    .line 111
    return-void
.end method

.method public updateIcon(Lcom/sec/android/app/FlashBarService/recent/TaskDescription;)V
    .locals 4
    .param p1, "td"    # Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    .prologue
    .line 119
    const-string v1, "MIK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateIcon:  name = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", icon = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", th = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getThumbnail()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mRenderer:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->getTaskForDescription(Lcom/sec/android/app/FlashBarService/recent/TaskDescription;)Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    move-result-object v0

    .line 121
    .local v0, "t":Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;
    if-eqz v0, :cond_0

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mRenderer:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->requestTaskInit(Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;)V

    .line 123
    const-string v1, "MIK"

    const-string v2, "updateIcon done"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_0
    return-void
.end method

.method public updateThumbnail(Lcom/sec/android/app/FlashBarService/recent/TaskDescription;)V
    .locals 4
    .param p1, "td"    # Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    .prologue
    .line 128
    const-string v1, "MIK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateThumbnail:  name = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", icon = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", th = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getThumbnail()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mRenderer:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->getTaskForDescription(Lcom/sec/android/app/FlashBarService/recent/TaskDescription;)Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    move-result-object v0

    .line 130
    .local v0, "t":Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;
    if-eqz v0, :cond_0

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->mRenderer:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->requestTaskInit(Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;)V

    .line 132
    const-string v1, "MIK"

    const-string v2, "updateThumbnail done"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    :cond_0
    return-void
.end method
