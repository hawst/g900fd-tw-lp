.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;->createTemplateDialog(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 2967
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 12
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v11, -0x2

    const/4 v8, 0x0

    .line 2970
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-interface {v7}, Landroid/text/Editable;->length()I

    move-result v7

    if-nez v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2971
    .local v4, "title":Ljava/lang/String;
    :goto_0
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-interface {v7}, Landroid/text/Editable;->length()I

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/EditText;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateIconIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$6900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    if-ne v7, v11, :cond_4

    :cond_1
    const/4 v5, 0x1

    .line 2974
    .local v5, "titleChanged":Z
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v10, v10, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResumedInfos:Ljava/util/List;

    invoke-virtual {v9, v10, v4, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addTemplate(Ljava/util/List;Ljava/lang/String;Z)I

    move-result v9

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateIconIndex:I
    invoke-static {v7, v9}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$6902(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 2975
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateIconIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$6900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    if-gez v7, :cond_5

    .line 2976
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateIconIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$6900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    if-ne v7, v11, :cond_2

    .line 2977
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v9, 0xcd

    invoke-static {v8, v9}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3000
    :cond_2
    :goto_2
    return-void

    .line 2970
    .end local v4    # "title":Ljava/lang/String;
    .end local v5    # "titleChanged":Z
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .restart local v4    # "title":Ljava/lang/String;
    :cond_4
    move v5, v8

    .line 2971
    goto :goto_1

    .line 2981
    .restart local v5    # "titleChanged":Z
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->prepareAnimationIcon()V
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    .line 2982
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListController;->updateWindowRects()V

    .line 2983
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_3
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResumedInfos:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v2, v7, :cond_6

    .line 2984
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationIcon:Ljava/util/List;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2986
    .local v0, "animationIcon":Landroid/widget/ImageView;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResumedInfos:Ljava/util/List;

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v7, v7, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v7}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v6

    .line 2987
    .local v6, "zoneInfo":I
    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    invoke-direct {v1, v7}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2989
    .local v1, "animationIconMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v7, v6}, Lcom/sec/android/app/FlashBarService/AppListController;->getRectByZone(I)Landroid/graphics/Rect;

    move-result-object v3

    .line 2990
    .local v3, "splitRect":Landroid/graphics/Rect;
    iget v7, v3, Landroid/graphics/Rect;->left:I

    iget v9, v3, Landroid/graphics/Rect;->right:I

    iget v10, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v7, v9

    iget v9, v3, Landroid/graphics/Rect;->top:I

    iget v10, v3, Landroid/graphics/Rect;->bottom:I

    iget v11, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v10, v11

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v9, v10

    invoke-virtual {v1, v7, v9, v8, v8}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2993
    new-instance v7, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v7, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2983
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 2996
    .end local v0    # "animationIcon":Landroid/widget/ImageView;
    .end local v1    # "animationIconMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v3    # "splitRect":Landroid/graphics/Rect;
    .end local v6    # "zoneInfo":I
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateIconIndex:I
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$6900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->templateAnimationIcon:Landroid/view/View;
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7202(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/view/View;)Landroid/view/View;

    .line 2997
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationTemplateStart()V
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    .line 2998
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dismissTemplateDialog()V

    .line 2999
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->openFlashBar()V
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    goto/16 :goto_2
.end method
