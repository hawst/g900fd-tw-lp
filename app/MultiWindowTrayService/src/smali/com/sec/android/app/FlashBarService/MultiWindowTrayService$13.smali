.class Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$13;
.super Landroid/content/BroadcastReceiver;
.source "MultiWindowTrayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 0

    .prologue
    .line 375
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$13;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 378
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.action.NOTIFY_STOP_DRAG_MODE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$13;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 380
    const-string v0, "MultiWindowTrayService"

    const-string v1, "SmartClipService Stoped"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$13;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideDragAndDropHelpDialog()V

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$13;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->removeDragAndDrop()V

    .line 385
    :cond_0
    return-void
.end method
