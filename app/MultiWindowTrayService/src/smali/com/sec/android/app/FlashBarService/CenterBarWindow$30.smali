.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$30;
.super Ljava/lang/Object;
.source "CenterBarWindow.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;->makeDragAndDropHelpPopupLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 3101
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$30;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$30;->val$checkBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v6, 0x1

    .line 3104
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$30;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3105
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$30;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsDragAndDropCheckBoxChecked:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$5102(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 3106
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$30;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "do_not_show_help_popup_drag_and_drop"

    const/4 v5, -0x2

    invoke-static {v3, v4, v6, v5}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 3108
    :cond_0
    const/4 v2, 0x0

    .line 3110
    .local v2, "info":Landroid/content/pm/PackageInfo;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$30;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.samsung.helphub"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 3115
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.samsung.helphub.HELP"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3117
    .local v0, "helpIntent":Landroid/content/Intent;
    iget v3, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    rem-int/lit8 v1, v3, 0xa

    .line 3118
    .local v1, "helphubVersion":I
    if-eq v1, v6, :cond_1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_2

    .line 3119
    :cond_1
    const-string v3, "helphub:section"

    const-string v4, "multi_window"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3126
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$30;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/content/Context;

    move-result-object v3

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v3, v0, v4}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 3127
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$30;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideDragAndDropHelpDialog()V

    .line 3128
    return-void

    .line 3120
    :cond_2
    const/4 v3, 0x3

    if-ne v1, v3, :cond_3

    .line 3121
    const-string v3, "helphub:appid"

    const-string v4, "multi_window"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 3123
    :cond_3
    const-string v3, "CenterBarWindow"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mHelphubVersion = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 3112
    .end local v0    # "helpIntent":Landroid/content/Intent;
    .end local v1    # "helphubVersion":I
    :catch_0
    move-exception v3

    goto :goto_0
.end method
