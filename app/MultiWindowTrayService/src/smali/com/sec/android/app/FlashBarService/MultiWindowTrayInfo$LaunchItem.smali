.class public Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
.super Ljava/lang/Object;
.source "MultiWindowTrayInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LaunchItem"
.end annotation


# instance fields
.field private mDefaultTitle:Z

.field private mIcon:Landroid/graphics/drawable/BitmapDrawable;

.field mLists:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private mTaskId:I

.field private mTitle:Ljava/lang/CharSequence;

.field private mTumbnail:Landroid/graphics/Bitmap;

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)V
    .locals 5
    .param p2, "ri"    # Landroid/content/pm/ResolveInfo;

    .prologue
    const/4 v2, 0x0

    .line 2476
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2468
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    .line 2469
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mTitle:Ljava/lang/CharSequence;

    .line 2470
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mTumbnail:Landroid/graphics/Bitmap;

    .line 2471
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mDefaultTitle:Z

    .line 2472
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mTaskId:I

    .line 2474
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mLists:Ljava/util/List;

    .line 2477
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mLists:Ljava/util/List;

    new-instance v3, Landroid/util/Pair;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v3, p2, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2478
    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->access$100(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mTitle:Ljava/lang/CharSequence;

    .line 2480
    # invokes: Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->loadIconForResolve(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    invoke-static {p1, p2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->access$200(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 2482
    .local v0, "icon":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {p1, v2, v3, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->createShadowIcon(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2483
    .local v1, "shadowBitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->access$100(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    .line 2484
    return-void
.end method

.method constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Ljava/util/List;Ljava/lang/String;Z)V
    .locals 6
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "defaultTitle"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .local p2, "plists":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    const/4 v4, 0x0

    .line 2486
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2468
    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    .line 2469
    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mTitle:Ljava/lang/CharSequence;

    .line 2470
    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mTumbnail:Landroid/graphics/Bitmap;

    .line 2471
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mDefaultTitle:Z

    .line 2472
    const/4 v4, -0x1

    iput v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mTaskId:I

    .line 2474
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mLists:Ljava/util/List;

    .line 2487
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2488
    .local v0, "drs":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/drawable/Drawable;>;"
    const/4 v2, 0x0

    .line 2489
    .local v2, "lastWinMode":I
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    .line 2490
    .local v3, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mLists:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2491
    iget-object v4, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-le v4, v2, :cond_0

    .line 2492
    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Landroid/content/pm/ResolveInfo;

    # invokes: Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->loadIconForResolve(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    invoke-static {p1, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->access$200(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2496
    :goto_1
    iget-object v4, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2497
    goto :goto_0

    .line 2494
    :cond_0
    const/4 v5, 0x0

    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Landroid/content/pm/ResolveInfo;

    # invokes: Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->loadIconForResolve(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    invoke-static {p1, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->access$200(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-interface {v0, v5, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 2499
    .end local v3    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    :cond_1
    if-eqz p3, :cond_2

    .line 2500
    iput-object p3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mTitle:Ljava/lang/CharSequence;

    .line 2504
    :goto_2
    # invokes: Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->generateMultiIcon(Ljava/util/List;)Landroid/graphics/drawable/BitmapDrawable;
    invoke-static {p1, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->access$300(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Ljava/util/List;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    .line 2505
    iput-boolean p4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mDefaultTitle:Z

    .line 2506
    return-void

    .line 2502
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAvailableTemplateText()Ljava/lang/CharSequence;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mTitle:Ljava/lang/CharSequence;

    goto :goto_2
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .prologue
    .line 2467
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mTitle:Ljava/lang/CharSequence;

    return-object v0
.end method


# virtual methods
.method getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 2509
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    return-object v0
.end method

.method getLists()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2560
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mLists:Ljava/util/List;

    return-object v0
.end method

.method getResolveInfo()Landroid/content/pm/ResolveInfo;
    .locals 2

    .prologue
    .line 2537
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mLists:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/content/pm/ResolveInfo;

    return-object v0
.end method

.method getResolveInfo2()Landroid/content/pm/ResolveInfo;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2551
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 2552
    const/4 v0, 0x0

    .line 2556
    :goto_0
    return-object v0

    .line 2555
    :cond_0
    const-string v1, "MultiWindowTrayInfo"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getResolveInfo2() return. mLists.get(1).first="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mLists:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2556
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mLists:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/content/pm/ResolveInfo;

    goto :goto_0
.end method

.method public getTaskId()I
    .locals 1

    .prologue
    .line 2601
    iget v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mTaskId:I

    return v0
.end method

.method public getThumbnail()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2593
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mTumbnail:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method getTitle()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 2517
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x2

    if-ge v3, v4, :cond_1

    .line 2518
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->access$100(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 2531
    :cond_0
    :goto_0
    return-object v2

    .line 2520
    :cond_1
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mDefaultTitle:Z

    if-eqz v3, :cond_3

    .line 2521
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 2522
    .local v1, "size":I
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 2523
    .local v2, "title":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_0

    .line 2524
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Landroid/content/pm/ResolveInfo;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->access$100(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2525
    add-int/lit8 v3, v1, -0x1

    if-eq v0, v3, :cond_2

    .line 2526
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2523
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2531
    .end local v0    # "i":I
    .end local v1    # "size":I
    .end local v2    # "title":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mTitle:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method isDefaultTitle()Z
    .locals 1

    .prologue
    .line 2564
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mDefaultTitle:Z

    return v0
.end method

.method refreshItemIcon()V
    .locals 10

    .prologue
    .line 2568
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x1

    if-le v6, v7, :cond_2

    .line 2569
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2570
    .local v0, "drs":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/drawable/Drawable;>;"
    const/4 v3, 0x0

    .line 2571
    .local v3, "lastWinMode":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mLists:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    .line 2572
    .local v4, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    iget-object v6, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-le v6, v3, :cond_0

    .line 2573
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v6, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Landroid/content/pm/ResolveInfo;

    # invokes: Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->loadIconForResolve(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    invoke-static {v7, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->access$200(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2577
    :goto_1
    iget-object v6, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 2578
    goto :goto_0

    .line 2575
    :cond_0
    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v6, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Landroid/content/pm/ResolveInfo;

    # invokes: Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->loadIconForResolve(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    invoke-static {v8, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->access$200(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-interface {v0, v7, v6}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 2579
    .end local v4    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    # invokes: Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->generateMultiIcon(Ljava/util/List;)Landroid/graphics/drawable/BitmapDrawable;
    invoke-static {v6, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->access$300(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Ljava/util/List;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    .line 2586
    .end local v0    # "drs":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/drawable/Drawable;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "lastWinMode":I
    :goto_2
    return-void

    .line 2581
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v7

    # invokes: Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->loadIconForResolve(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->access$200(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .line 2583
    .local v2, "icon":Landroid/graphics/drawable/BitmapDrawable;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v8

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v9

    invoke-virtual {v6, v7, v8, v9}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->createShadowIcon(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 2584
    .local v5, "shadowBitmap":Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->access$100(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {v6, v7, v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    goto :goto_2
.end method

.method updateResolveInfo(Landroid/content/pm/ResolveInfo;)V
    .locals 5
    .param p1, "ri"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 2541
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mLists:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2542
    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mLists:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/content/pm/ResolveInfo;

    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2543
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mLists:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2544
    .local v1, "winMode":I
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mLists:Ljava/util/List;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mLists:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2545
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mLists:Ljava/util/List;

    new-instance v3, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v3, p1, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v2, v0, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2541
    .end local v1    # "winMode":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2548
    :cond_1
    return-void
.end method
