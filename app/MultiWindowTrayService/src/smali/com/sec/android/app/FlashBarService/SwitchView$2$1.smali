.class Lcom/sec/android/app/FlashBarService/SwitchView$2$1;
.super Ljava/lang/Object;
.source "SwitchView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/SwitchView$2;->onAnimationEnd(Landroid/animation/Animator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/FlashBarService/SwitchView$2;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SwitchView$2;)V
    .locals 0

    .prologue
    .line 460
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2$1;->this$1:Lcom/sec/android/app/FlashBarService/SwitchView$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 463
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2$1;->this$1:Lcom/sec/android/app/FlashBarService/SwitchView$2;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$900(Lcom/sec/android/app/FlashBarService/SwitchView;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2$1;->this$1:Lcom/sec/android/app/FlashBarService/SwitchView$2;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedAppZone:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$800(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    .line 464
    .local v0, "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2$1;->this$1:Lcom/sec/android/app/FlashBarService/SwitchView$2;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->border:Landroid/view/View;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/FlashBarService/SwitchView;->bringChildToBack(Landroid/view/View;Z)V

    .line 465
    const-string v1, "MIK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "animateZoom: onAnimationEnd: movingBack "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->border:Landroid/view/View;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2$1;->this$1:Lcom/sec/android/app/FlashBarService/SwitchView$2;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->requestLayout()V

    .line 467
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2$1;->this$1:Lcom/sec/android/app/FlashBarService/SwitchView$2;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2$1;->this$1:Lcom/sec/android/app/FlashBarService/SwitchView$2;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedAppZone:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$800(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v2

    # setter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1302(Lcom/sec/android/app/FlashBarService/SwitchView;I)I

    .line 468
    return-void
.end method
