.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 4445
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v3, -0x1

    .line 4449
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getCurrentAppList()Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4302(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 4450
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 4476
    :cond_0
    :goto_0
    :pswitch_0
    return v5

    .line 4453
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    if-le v1, v3, :cond_1

    .line 4454
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 4456
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    if-le v1, v3, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 4457
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-boolean v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSupportMultiInstance:Z

    const/16 v4, 0x64

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v0

    .line 4458
    .local v0, "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissHistoryWindow(Z)V
    invoke-static {v1, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)V

    .line 4459
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4463
    .end local v0    # "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->cancelCollapseTimer()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    goto :goto_0

    .line 4466
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    if-le v1, v3, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 4467
    const v1, 0x7f020092

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 4469
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryTimerHandler:Landroid/os/Handler;

    const/16 v2, 0x12c

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 4470
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->cancelCollapseTimer()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    goto/16 :goto_0

    .line 4473
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startCollapseTimer()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    goto/16 :goto_0

    .line 4450
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
