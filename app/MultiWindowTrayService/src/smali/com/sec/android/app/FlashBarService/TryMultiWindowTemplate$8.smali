.class Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$8;
.super Ljava/lang/Object;
.source "TryMultiWindowTemplate.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)V
    .locals 0

    .prologue
    .line 432
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$8;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 437
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 449
    :goto_0
    :pswitch_0
    return-void

    .line 440
    :pswitch_1
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketOpenBtnPushed:Z

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$8;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup2Parent:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$2800(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$8;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->startPocketOpenAnimation()V

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$8;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketBody:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 444
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$8;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # invokes: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->updatePocketTrayLayout(IZZ)V
    invoke-static {v0, v2, v2, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$3000(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;IZZ)V

    goto :goto_0

    .line 437
    nop

    :pswitch_data_0
    .packed-switch 0x7f0f0143
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
