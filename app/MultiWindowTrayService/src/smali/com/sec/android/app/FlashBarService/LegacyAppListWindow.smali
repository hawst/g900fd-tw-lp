.class public Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.super Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
.source "LegacyAppListWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;,
        Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$GridViewAdapter;,
        Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;,
        Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;
    }
.end annotation


# static fields
.field private static SCROLL_MOVE_DELAY:I


# instance fields
.field private final APPLIST_LONG_PRESS:I

.field private final APPLIST_RECENT_HOMEKEY:I

.field private final APPLIST_RECENT_REMOVE:I

.field private final APPLIST_TIMER_COLLAPSE:I

.field private final APPLIST_TIMER_LONG_PRESS:I

.field private final APPLIST_TIMER_MESSAGE:I

.field private final APPLIST_VIEW_PAGER_MOVE_LEFT:I

.field private final APPLIST_VIEW_PAGER_MOVE_RIGHT:I

.field private final ESTIMATE_INVALID_VALUE:S

.field private final HISTORYBAR_REMAKE:I

.field private final HISTORY_TIMER_MESSAGE:I

.field private final MAX_NAME_LENGTH:I

.field private final MAX_TASKS:I

.field private final PAIREDWINDOW_DIALOG_SHOW:I

.field private final THUMBNAIL_TYPE_APP:I

.field private final THUMBNAIL_TYPE_RECENT:I

.field private final UPDATE_APPLIST_POSITION:I

.field private final UPDATE_APPLIST_POSITION_DELAY:I

.field private final UPDATE_SPLIT_GUIDE_VIEW:I

.field private final UPDATE_SPLIT_GUIDE_VIEW_DELAY:I

.field private final VIEWPAGER_TIMEOUT_MESSAGE:I

.field private final WINDOW_PORTRAIT_MODE:S

.field private doOneTimeAfterDragStarts:Z

.field private mAddEmptyItemPosition:I

.field private mAddToListItem:I

.field private mAnimationBackground:Landroid/widget/ImageView;

.field private mAnimationCapture1:Landroid/widget/ImageView;

.field private mAnimationCapture2:Landroid/widget/ImageView;

.field private mAnimationIcon1:Landroid/widget/ImageView;

.field private mAnimationIcon2:Landroid/widget/ImageView;

.field mAppDefHeight:I

.field mAppDefWidth:I

.field mAppIconClickListener:Landroid/view/View$OnClickListener;

.field mAppIconHoverListener:Landroid/view/View$OnHoverListener;

.field private mAppIconIndex:I

.field mAppIconLongClickListener:Landroid/view/View$OnLongClickListener;

.field mAppIconTouchListener:Landroid/view/View$OnTouchListener;

.field mAppListDragListener:Landroid/view/View$OnDragListener;

.field private mAppListDragMode:Z

.field private mAppListDragZone:I

.field protected mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

.field private mAppListHelpDefaultMargin:I

.field mAppListItemDragListener:Landroid/view/View$OnDragListener;

.field private mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

.field private mAppListPosition:I

.field private mAppListPositonFromCSC:Ljava/lang/String;

.field private mAppListVertical:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private mAppListViewPageCount:I

.field private mAppListViewPager:Landroid/support/v4/view/ViewPager;

.field private mAppListViewPagerAdapter:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;

.field private mAppListViewPagerDialog:Landroid/app/Dialog;

.field mAppListViewPagerHoverListener:Landroid/view/View$OnHoverListener;

.field private mAppListViewPagerSelectedPage:I

.field private mAppListViewPagerShadow:Landroid/widget/ImageView;

.field private mAppListViewPagerShadowLand:Landroid/widget/ImageView;

.field private mAppListViewPagerTrayShow:Z

.field private mAppListViewPagerView:Landroid/view/View;

.field private mAppListViewPagerWrapper:Landroid/widget/FrameLayout;

.field private mAppTrayBG:Landroid/widget/ImageView;

.field private mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

.field private mAvailableAppCnt:I

.field private mBeforeAppListPosition:I

.field private mBottomGuideline:Landroid/widget/FrameLayout;

.field private mCancelDrawable:Landroid/graphics/drawable/Drawable;

.field private mCenterBarPoint:Landroid/graphics/Point;

.field private mChangedPosition:I

.field private mCloseApplist:Z

.field private mConfigChanged:Z

.field mConfirmDialog:Landroid/app/AlertDialog;

.field private mCurrentAppPosition:I

.field private mCurrentDownEvent:Landroid/view/MotionEvent;

.field private mCurrentDownView:Landroid/view/View;

.field private mCurrentHistoryIndex:I

.field private mDelIndex:I

.field private mDisplayHeight:I

.field private mDisplayWidth:I

.field mDragHandler:Landroid/os/Handler;

.field private mEditBtn:Landroid/widget/Button;

.field private mEditLayout:Landroid/widget/RelativeLayout;

.field private mEditTextView:Landroid/widget/EditText;

.field private mExpandAppList:Z

.field private mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

.field private mFadingAnimation:Landroid/view/animation/Animation;

.field private mFadingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field mFlashBarEditListener:Landroid/view/View$OnClickListener;

.field mFlashBarHelpListener:Landroid/view/View$OnClickListener;

.field mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

.field mFlashBarOpenListener:Landroid/view/View$OnClickListener;

.field mFlashBarTemplateListener:Landroid/view/View$OnClickListener;

.field mFlashBarTouchListener:Landroid/view/View$OnTouchListener;

.field private mFlashBarVertical:Landroid/widget/ScrollView;

.field private mFlashingAnimation:Landroid/view/animation/Animation;

.field private mFlashingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mFocusGuideline:Landroid/widget/ImageView;

.field private mFocusVisible:Z

.field private mFrequentlyAppListWindowKeyListener:Landroid/view/View$OnKeyListener;

.field mFrequentlyIconTouchListener:Landroid/view/View$OnTouchListener;

.field mFrequentlyPageIconClickListener:Landroid/view/View$OnClickListener;

.field private mFrequentlyUsedAppCnt:I

.field private mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

.field private mGoalAnimationView:Landroid/view/View;

.field mGuidelineDragListener:Landroid/view/View$OnDragListener;

.field private mGuidelineLayout:Landroid/widget/FrameLayout;

.field private mHandlePosition:I

.field private mHandler:Landroid/os/Handler;

.field protected mHasCocktailBar:Z

.field private mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

.field private mHelphubVersion:I

.field private mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

.field private mHistoryBarDialog:Landroid/app/Dialog;

.field private mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

.field private mHistoryBarDialogScrollView:Landroid/widget/ScrollView;

.field private mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

.field private mHistoryBarDialogView:Landroid/view/View;

.field mHistoryBarLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mHistoryBarNewThumbIcon:Landroid/widget/ImageView;

.field private mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

.field mHistoryTimerHandler:Landroid/os/Handler;

.field private mHistotyBarTopMargin:I

.field private mInputMethodChanged:Z

.field private mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mIntent:Landroid/content/Intent;

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mIntents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private mIsAvailableMove:Z

.field private mIsHoverType:Z

.field private mIsInputMethodShown:Z

.field private mIsNewHelpPopup:Z

.field private mIsPenWindowOnly:Z

.field private mIsPressMoveBtn:Z

.field private mIsTemplateItem:Z

.field private mLastConfig:Landroid/content/res/Configuration;

.field private mLastDegreesForPosition:I

.field private mLastHandlePosition:I

.field private mLastMultiWindowTypeArray:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mLastPostAnimationRunnable:Ljava/lang/Runnable;

.field private mLegacyAppListWindowKeyListener:Landroid/view/View$OnKeyListener;

.field private mLunchBlock:Z

.field private mMainFrame:Landroid/view/ViewGroup;

.field private mMakeInstanceAniRunning:Z

.field private mMoveBtn:Landroid/widget/ImageView;

.field private mMoveBtnBoundary:I

.field mMoveBtnDragListener:Landroid/view/View$OnDragListener;

.field mMoveBtnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mMoveBtnOverlapMargin:I

.field private mMoveBtnShadowMargin:I

.field mMoveBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mMultiWindowItemNum:I

.field private mNeedHelpPage:Z

.field private mNeedUpdatePocketTrayButtonText:Z

.field private mNewInstanceIntent:Landroid/content/Intent;

.field private mOffsetForAnim:I

.field private mOpenPocketTray:Z

.field private mOrientationListenerForPosition:Landroid/view/OrientationEventListener;

.field private mPocketEditBtn:Landroid/widget/ImageButton;

.field mPocketEditBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mPocketEditText:Landroid/widget/TextView;

.field private mPocketHelpBtn:Landroid/widget/ImageButton;

.field mPocketHelpBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mPocketHelpText:Landroid/widget/TextView;

.field private mPocketLayout:Landroid/widget/RelativeLayout;

.field private mPocketOpenBtn:Landroid/widget/ImageButton;

.field private mPocketTemplateBtn:Landroid/widget/ImageButton;

.field mPocketTemplateBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mPocketTemplateText:Landroid/widget/TextView;

.field private mPocketTrayBody:Landroid/widget/RelativeLayout;

.field private mPocketTrayHeader:Landroid/widget/RelativeLayout;

.field mPocketTrayHeaderTouchListener:Landroid/view/View$OnTouchListener;

.field private mPositionGuideline:Landroid/widget/ImageView;

.field private mPositionX:I

.field private mPositionY:I

.field private mPrevExpandAppList:Z

.field mPreviewFocusedRect:Landroid/graphics/Rect;

.field private mProcessObserver:Landroid/app/IProcessObserver;

.field private mRecentIcon:Landroid/widget/ImageView;

.field mRecentIconClickListener:Landroid/view/View$OnClickListener;

.field private mRecentLayout:Landroid/widget/LinearLayout;

.field mRecentsAppStartReceiver:Landroid/content/BroadcastReceiver;

.field mResetConfirmDialog:Landroid/app/AlertDialog;

.field private mResources:Landroid/content/res/Resources;

.field mResumedInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningTaskInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mScrollDirection:I

.field private mSelectedAppPackage:Ljava/lang/String;

.field private mSelectedTaskId:I

.field private mShadowHeight:I

.field private mShadowWidth:I

.field private mShowAnimation:Landroid/view/animation/Animation;

.field private mShowAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mShowViewPagerTray:Z

.field private mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;

.field private mSplitGuideOpenAnimation:Landroid/view/animation/Animation;

.field private mSplitGuidelineShadow:I

.field private mStartFlashBar:Z

.field private mTemplateBtn:Landroid/widget/Button;

.field mTemplateDialog:Landroid/app/AlertDialog;

.field private mTemplateIconIndex:I

.field private mThbumNailCancelImageSize:I

.field private mThbumNailShadowMargin:I

.field private mThumbNailImageHeight:I

.field private mThumbNailImageWidth:I

.field private mThumbNailLayoutHeight:I

.field private mThumbNailLayoutPadding:I

.field private mThumbNailLayoutWidth:I

.field private mThumbnailTaskId:I

.field private mThumbnailTaskInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningTaskInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mThumbnailType:I

.field mTimerHandler:Landroid/os/Handler;

.field mToolkitRect:Landroid/graphics/Rect;

.field mToolkitZone:I

.field private mTopGuideline:Landroid/widget/FrameLayout;

.field private mTopLayoutVertical:Landroid/widget/LinearLayout;

.field mTrayOpenDialog:Landroid/app/AlertDialog;

.field mUnableDialog:Landroid/app/AlertDialog;

.field private mUserId:I

.field private mUserStoppedReceiver:Landroid/content/BroadcastReceiver;

.field private mViewPagerHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

.field mViewPagerIconClickListener:Landroid/view/View$OnClickListener;

.field mViewPagerIconTouchListener:Landroid/view/View$OnTouchListener;

.field private mViewPagerMark:Landroid/widget/LinearLayout;

.field private mViewPagerMarkBottomMarginLand:I

.field private mViewPagerMarkBottomMarginPort:I

.field private mViewPagerMarkerMarginLeft:I

.field private mViewPagerMarkerSize:I

.field private mViewPagerMoveLeft:Z

.field private mViewPagerMoveRight:Z

.field private mViewPagerScrollHoverMargin:I

.field private mViewPagerTopPaddingLand:I

.field private mViewPagerTopPaddingPort:I

.field private mWindowAppList:Landroid/view/Window;

.field mWindowDefHeight:I

.field mWindowDefWidth:I

.field private mWindowGuideline:Landroid/view/Window;

.field private mbEditmode:Z

.field private mbFromEdit:Z

.field private mbSynchronizeConfirmDialog:Z

.field private mcurDstIndex:I

.field private mcurSrcIndex:I

.field private orgIcon:Landroid/graphics/drawable/Drawable;

.field private templateAnimationIcon:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 271
    const/16 v0, 0x32

    sput v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->SCROLL_MOVE_DELAY:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p3, "trayinfo"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .prologue
    .line 449
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V

    .line 145
    const/4 v0, -0x1

    iput-short v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->ESTIMATE_INVALID_VALUE:S

    .line 146
    const/4 v0, 0x1

    iput-short v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->WINDOW_PORTRAIT_MODE:S

    .line 147
    const/16 v0, 0x1388

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->APPLIST_TIMER_COLLAPSE:I

    .line 148
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->APPLIST_TIMER_LONG_PRESS:I

    .line 149
    const/16 v0, 0xc9

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->APPLIST_TIMER_MESSAGE:I

    .line 150
    const/16 v0, 0xca

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->APPLIST_LONG_PRESS:I

    .line 151
    const/16 v0, 0xcb

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->APPLIST_RECENT_HOMEKEY:I

    .line 152
    const/16 v0, 0xcc

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->APPLIST_RECENT_REMOVE:I

    .line 153
    const/16 v0, 0xcd

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->PAIREDWINDOW_DIALOG_SHOW:I

    .line 154
    const/16 v0, 0xce

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->UPDATE_APPLIST_POSITION:I

    .line 155
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->UPDATE_APPLIST_POSITION_DELAY:I

    .line 156
    const/16 v0, 0xcf

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->UPDATE_SPLIT_GUIDE_VIEW:I

    .line 157
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->UPDATE_SPLIT_GUIDE_VIEW_DELAY:I

    .line 158
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->HISTORYBAR_REMAKE:I

    .line 159
    const/16 v0, 0xd0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->APPLIST_VIEW_PAGER_MOVE_LEFT:I

    .line 160
    const/16 v0, 0xd1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->APPLIST_VIEW_PAGER_MOVE_RIGHT:I

    .line 161
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->HISTORY_TIMER_MESSAGE:I

    .line 162
    const/16 v0, 0x12d

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->VIEWPAGER_TIMEOUT_MESSAGE:I

    .line 163
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->MAX_TASKS:I

    .line 164
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->MAX_NAME_LENGTH:I

    .line 166
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .line 174
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnOverlapMargin:I

    .line 175
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnShadowMargin:I

    .line 176
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnBoundary:I

    .line 189
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsPressMoveBtn:Z

    .line 190
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    .line 193
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    .line 195
    const/16 v0, 0x67

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    .line 196
    const-string v0, "RIGHT"

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPositonFromCSC:Ljava/lang/String;

    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsAvailableMove:Z

    .line 201
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPrevExpandAppList:Z

    .line 202
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z

    .line 203
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOpenPocketTray:Z

    .line 204
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mStartFlashBar:Z

    .line 206
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbSynchronizeConfirmDialog:Z

    .line 207
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListDragMode:Z

    .line 220
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerTrayShow:Z

    .line 234
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSelectedTaskId:I

    .line 235
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    .line 236
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    .line 237
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    .line 238
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    .line 239
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListHelpDefaultMargin:I

    .line 240
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShowViewPagerTray:Z

    .line 264
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;

    .line 267
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I

    .line 268
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I

    .line 272
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mScrollDirection:I

    .line 278
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;

    .line 281
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    .line 282
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddEmptyItemPosition:I

    .line 286
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddToListItem:I

    .line 287
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDelIndex:I

    .line 288
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbFromEdit:Z

    .line 296
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResumedInfos:Ljava/util/List;

    .line 303
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    .line 307
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentAppPosition:I

    .line 309
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskInfos:Ljava/util/List;

    .line 310
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->THUMBNAIL_TYPE_APP:I

    .line 311
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->THUMBNAIL_TYPE_RECENT:I

    .line 318
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I

    .line 352
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsHoverType:Z

    .line 353
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNeedUpdatePocketTrayButtonText:Z

    .line 355
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppDefWidth:I

    .line 356
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppDefHeight:I

    .line 357
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefWidth:I

    .line 358
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefHeight:I

    .line 359
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mToolkitZone:I

    .line 360
    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v0, v1, v2, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mToolkitRect:Landroid/graphics/Rect;

    .line 363
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsInputMethodShown:Z

    .line 364
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mInputMethodChanged:Z

    .line 365
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mConfigChanged:Z

    .line 366
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mChangedPosition:I

    .line 371
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastHandlePosition:I

    .line 377
    const/16 v0, 0x67

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBeforeAppListPosition:I

    .line 380
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandler:Landroid/os/Handler;

    .line 384
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->doOneTimeAfterDragStarts:Z

    .line 385
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLunchBlock:Z

    .line 387
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    .line 388
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mProcessObserver:Landroid/app/IProcessObserver;

    .line 391
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHelphubVersion:I

    .line 394
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsPenWindowOnly:Z

    .line 395
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsTemplateItem:Z

    .line 398
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHasCocktailBar:Z

    .line 402
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastMultiWindowTypeArray:Landroid/util/SparseArray;

    .line 403
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mUserId:I

    .line 405
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$1;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLegacyAppListWindowKeyListener:Landroid/view/View$OnKeyListener;

    .line 422
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$2;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyAppListWindowKeyListener:Landroid/view/View$OnKeyListener;

    .line 634
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$5;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mUserStoppedReceiver:Landroid/content/BroadcastReceiver;

    .line 644
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$6;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 668
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$7;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mRecentsAppStartReceiver:Landroid/content/BroadcastReceiver;

    .line 777
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    .line 859
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$10;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryTimerHandler:Landroid/os/Handler;

    .line 1186
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerHoverListener:Landroid/view/View$OnHoverListener;

    .line 1374
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 2113
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListItemDragListener:Landroid/view/View$OnDragListener;

    .line 2303
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$32;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$32;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDragHandler:Landroid/os/Handler;

    .line 3205
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastPostAnimationRunnable:Ljava/lang/Runnable;

    .line 3334
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$36;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$36;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFadingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 3347
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$37;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$37;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 3360
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$38;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$38;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShowAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 4277
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 4377
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$71;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$71;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 4386
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$72;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$72;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnDragListener:Landroid/view/View$OnDragListener;

    .line 4401
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconClickListener:Landroid/view/View$OnClickListener;

    .line 4445
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$74;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconHoverListener:Landroid/view/View$OnHoverListener;

    .line 4480
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 4534
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$76;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$76;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconTouchListener:Landroid/view/View$OnTouchListener;

    .line 4568
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$77;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$77;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarTouchListener:Landroid/view/View$OnTouchListener;

    .line 4591
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$78;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$78;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

    .line 4611
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$79;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$79;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTrayHeaderTouchListener:Landroid/view/View$OnTouchListener;

    .line 4640
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarOpenListener:Landroid/view/View$OnClickListener;

    .line 4685
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarEditListener:Landroid/view/View$OnClickListener;

    .line 4744
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarTemplateListener:Landroid/view/View$OnClickListener;

    .line 4812
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$83;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$83;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarHelpListener:Landroid/view/View$OnClickListener;

    .line 4912
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineDragListener:Landroid/view/View$OnDragListener;

    .line 5270
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$87;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListDragListener:Landroid/view/View$OnDragListener;

    .line 5615
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mRecentIconClickListener:Landroid/view/View$OnClickListener;

    .line 5781
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$91;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$91;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyPageIconClickListener:Landroid/view/View$OnClickListener;

    .line 5790
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$92;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$92;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyIconTouchListener:Landroid/view/View$OnTouchListener;

    .line 5802
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$93;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$93;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerIconClickListener:Landroid/view/View$OnClickListener;

    .line 5817
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$94;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$94;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerIconTouchListener:Landroid/view/View$OnTouchListener;

    .line 6093
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$95;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$95;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    .line 6122
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$96;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$96;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 6138
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$97;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$97;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 6154
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$98;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$98;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 450
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getWindowDefSize()V

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    .line 453
    new-instance v0, Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-direct {v0, v1, p0, v2}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .line 456
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Framework_ConfigDefTrayPosition"

    const-string v2, "RIGHT"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPositonFromCSC:Ljava/lang/String;

    .line 457
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getFlashBarCurPosition()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    .line 458
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getFlashBarHandlePosition()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f020079

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;

    .line 460
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 461
    invoke-static {}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->getInstance()Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    .line 462
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$3;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$3;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOrientationListenerForPosition:Landroid/view/OrientationEventListener;

    .line 478
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->helpTapAnimationInit(Landroid/content/Context;)V

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a0017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListDragZone:I

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnOverlapMargin:I

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnShadowMargin:I

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a001c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnBoundary:I

    .line 485
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a0059

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailLayoutWidth:I

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a005a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailLayoutHeight:I

    .line 487
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a005c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailLayoutPadding:I

    .line 488
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a0065

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistotyBarTopMargin:I

    .line 489
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a005d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailImageWidth:I

    .line 490
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a005e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailImageHeight:I

    .line 491
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a0061

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThbumNailCancelImageSize:I

    .line 492
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a0064

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThbumNailShadowMargin:I

    .line 493
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a0166

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerTopPaddingPort:I

    .line 494
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a0168

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerTopPaddingLand:I

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a016c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkerSize:I

    .line 496
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a016d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkerMarginLeft:I

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a016a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkBottomMarginPort:I

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a016b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkBottomMarginLand:I

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a016e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerScrollHoverMargin:I

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f07000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowItemNum:I

    .line 501
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskId:I

    .line 503
    new-instance v0, Landroid/content/res/Configuration;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastConfig:Landroid/content/res/Configuration;

    .line 504
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsNewHelpPopup:Z

    .line 506
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f070001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x64

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mChangedPosition:I

    .line 508
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 509
    .local v3, "intentFilter":Landroid/content/IntentFilter;
    const-string v0, "ResponseAxT9Info"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 510
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 511
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v6, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    new-instance v7, Landroid/content/IntentFilter;

    const-string v0, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v7, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 513
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v0, :cond_0

    .line 514
    new-instance v7, Landroid/content/IntentFilter;

    const-string v0, "com.android.systemui.recent.RECENTSPANEL_OPEN"

    invoke-direct {v7, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 515
    .local v7, "recentsAppStartReceiver":Landroid/content/IntentFilter;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mRecentsAppStartReceiver:Landroid/content/BroadcastReceiver;

    sget-object v6, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 519
    .end local v7    # "recentsAppStartReceiver":Landroid/content/IntentFilter;
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->initFolderResources(Landroid/content/res/Resources;)V

    .line 521
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastHandlePosition:I

    .line 523
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mProcessObserver:Landroid/app/IProcessObserver;

    .line 587
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.samsung.helphub"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v10

    .line 588
    .local v10, "info":Landroid/content/pm/PackageInfo;
    iget v0, v10, Landroid/content/pm/PackageInfo;->versionCode:I

    rem-int/lit8 v0, v0, 0xa

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHelphubVersion:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 592
    .end local v10    # "info":Landroid/content/pm/PackageInfo;
    :goto_0
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHelphubVersion:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 593
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "do_not_show_help_popup_open_traybar"

    const/4 v2, 0x1

    const/4 v4, -0x2

    invoke-static {v0, v1, v2, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "do_not_show_help_popup_drag_and_drop"

    const/4 v2, 0x1

    const/4 v4, -0x2

    invoke-static {v0, v1, v2, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 597
    :cond_1
    new-instance v11, Landroid/content/IntentFilter;

    const-string v0, "android.intent.action.USER_STOPPED"

    invoke-direct {v11, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 598
    .local v11, "userStoppedFilter":Landroid/content/IntentFilter;
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mUserStoppedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1, v11}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 599
    return-void

    .line 589
    .end local v11    # "userStoppedFilter":Landroid/content/IntentFilter;
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private GetCaptureImage(Landroid/graphics/Rect;)Landroid/graphics/Bitmap;
    .locals 22
    .param p1, "splitRect"    # Landroid/graphics/Rect;

    .prologue
    .line 3842
    new-instance v16, Landroid/graphics/Matrix;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Matrix;-><init>()V

    .line 3844
    .local v16, "displayMatrix":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v15

    .line 3845
    .local v15, "display":Landroid/view/Display;
    new-instance v17, Landroid/util/DisplayMetrics;

    invoke-direct/range {v17 .. v17}, Landroid/util/DisplayMetrics;-><init>()V

    .line 3846
    .local v17, "displayMetrics":Landroid/util/DisplayMetrics;
    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 3848
    const/4 v3, 0x2

    new-array v14, v3, [F

    const/4 v3, 0x0

    move-object/from16 v0, v17

    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v4, v4

    aput v4, v14, v3

    const/4 v3, 0x1

    move-object/from16 v0, v17

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v4, v4

    aput v4, v14, v3

    .line 3849
    .local v14, "dims":[F
    invoke-virtual {v15}, Landroid/view/Display;->getRotation()I

    move-result v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getDegreesForRotation(I)F

    move-result v13

    .line 3851
    .local v13, "degrees":F
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Matrix;->reset()V

    .line 3852
    neg-float v3, v13

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->preRotate(F)Z

    .line 3853
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 3854
    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, v14, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    aput v4, v14, v3

    .line 3855
    const/4 v3, 0x1

    const/4 v4, 0x1

    aget v4, v14, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    aput v4, v14, v3

    .line 3857
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    const/4 v4, 0x0

    aget v4, v14, v4

    float-to-int v4, v4

    const/4 v5, 0x1

    aget v5, v14, v5

    float-to-int v5, v5

    const/16 v6, 0x4e20

    const v7, 0x270ff

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v3 .. v9}, Landroid/view/SurfaceControl;->screenshot(Landroid/graphics/Rect;IIIIZI)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 3858
    .local v20, "screenCapturedTemp":Landroid/graphics/Bitmap;
    move-object/from16 v0, v17

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move-object/from16 v0, v17

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 3859
    .local v19, "screenCaptured":Landroid/graphics/Bitmap;
    new-instance v11, Landroid/graphics/Canvas;

    move-object/from16 v0, v19

    invoke-direct {v11, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 3861
    .local v11, "canvasTemp":Landroid/graphics/Canvas;
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v11, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3862
    invoke-virtual {v11, v13}, Landroid/graphics/Canvas;->rotate(F)V

    .line 3863
    const/4 v3, 0x0

    aget v3, v14, v3

    neg-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    const/4 v4, 0x1

    aget v4, v14, v4

    neg-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v11, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3864
    if-eqz v20, :cond_0

    .line 3865
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v11, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 3867
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v11, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 3868
    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 3870
    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    const/4 v4, 0x1

    if-lt v3, v4, :cond_1

    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    const/4 v4, 0x1

    if-ge v3, v4, :cond_2

    .line 3871
    :cond_1
    const/4 v12, 0x0

    .line 3879
    :goto_0
    return-object v12

    .line 3873
    :cond_2
    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 3874
    .local v12, "cropBitmap":Landroid/graphics/Bitmap;
    new-instance v10, Landroid/graphics/Canvas;

    invoke-direct {v10, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 3875
    .local v10, "canvasFinal":Landroid/graphics/Canvas;
    new-instance v21, Landroid/graphics/Rect;

    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p1

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, v21

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3876
    .local v21, "srcRectForCrop":Landroid/graphics/Rect;
    new-instance v18, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p1

    iget v6, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v5, v6

    move-object/from16 v0, p1

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p1

    iget v7, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v6, v7

    move-object/from16 v0, v18

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3877
    .local v18, "dstRectForCrop":Landroid/graphics/Rect;
    const/4 v3, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v18

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSelectedAppPackage:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastDegreesForPosition:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFocusVisible:Z

    return v0
.end method

.method static synthetic access$10000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationTemplateStart()V

    return-void
.end method

.method static synthetic access$10100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->openFlashBar()V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastDegreesForPosition:I

    return p1
.end method

.method static synthetic access$10200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDelIndex:I

    return v0
.end method

.method static synthetic access$10300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbFromEdit:Z

    return v0
.end method

.method static synthetic access$10400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissResetConfirmDialog()V

    return-void
.end method

.method static synthetic access$10502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbSynchronizeConfirmDialog:Z

    return p1
.end method

.method static synthetic access$10600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$10700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$10800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationTemplateIconAppear()V

    return-void
.end method

.method static synthetic access$10900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationTemplateIconMove()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ZZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 141
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideVisibility(ZZZ)V

    return-void
.end method

.method static synthetic access$11000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->endAnimationIcon()V

    return-void
.end method

.method static synthetic access$11100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationTemplateIcon()V

    return-void
.end method

.method static synthetic access$11200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->templateAnimationIcon:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$11400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationRecentIconMoveByHomeKey()V

    return-void
.end method

.method static synthetic access$11500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mRecentIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$11600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->enablePocketButtons(Z)V

    return-void
.end method

.method static synthetic access$11702(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsPressMoveBtn:Z

    return p1
.end method

.method static synthetic access$11800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setImageToMoveButton(IZ)V

    return-void
.end method

.method static synthetic access$11900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPrevExpandAppList:Z

    return v0
.end method

.method static synthetic access$11902(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPrevExpandAppList:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$12000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsAvailableMove:Z

    return v0
.end method

.method static synthetic access$12002(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsAvailableMove:Z

    return p1
.end method

.method static synthetic access$12100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionX:I

    return v0
.end method

.method static synthetic access$12102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionX:I

    return p1
.end method

.method static synthetic access$12200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z

    .prologue
    .line 141
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListPosition(IZZZ)V

    return-void
.end method

.method static synthetic access$12300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBeforeAppListPosition:I

    return v0
.end method

.method static synthetic access$12302(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBeforeAppListPosition:I

    return p1
.end method

.method static synthetic access$12400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideTextPadding()V

    return-void
.end method

.method static synthetic access$12500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getCurrentAppList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$12600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I

    return v0
.end method

.method static synthetic access$12602(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I

    return p1
.end method

.method static synthetic access$12700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$12800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsPenWindowOnly:Z

    return v0
.end method

.method static synthetic access$12802(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsPenWindowOnly:Z

    return p1
.end method

.method static synthetic access$12900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->isLaunchingBlockedItem(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mUserId:I

    return v0
.end method

.method static synthetic access$13000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/MotionEvent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$13002(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;

    return-object p1
.end method

.method static synthetic access$13100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/view/MotionEvent;Landroid/view/View;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Landroid/view/MotionEvent;
    .param p2, "x2"    # Landroid/view/View;
    .param p3, "x3"    # I

    .prologue
    .line 141
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->applyShadePressEffect(Landroid/view/MotionEvent;Landroid/view/View;I)V

    return-void
.end method

.method static synthetic access$13200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListDragZone:I

    return v0
.end method

.method static synthetic access$13300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$13400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTrayHeader:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$13500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOpenPocketTray:Z

    return v0
.end method

.method static synthetic access$13502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOpenPocketTray:Z

    return p1
.end method

.method static synthetic access$13600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHelphubVersion:I

    return v0
.end method

.method static synthetic access$13700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 141
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updatePocketTrayLayout(IZZ)V

    return-void
.end method

.method static synthetic access$13802(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNeedUpdatePocketTrayButtonText:Z

    return p1
.end method

.method static synthetic access$13900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/util/SparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastMultiWindowTypeArray:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic access$14000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$14100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$14200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$14300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$14400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$14500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$14600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->openHelpHub()V

    return-void
.end method

.method static synthetic access$14700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$14800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsTemplateItem:Z

    return v0
.end method

.method static synthetic access$14802(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsTemplateItem:Z

    return p1
.end method

.method static synthetic access$14900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    return-object v0
.end method

.method static synthetic access$15002(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentAppPosition:I

    return p1
.end method

.method static synthetic access$15100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailLayoutWidth:I

    return v0
.end method

.method static synthetic access$15200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyUsedAppCnt:I

    return v0
.end method

.method static synthetic access$15202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyUsedAppCnt:I

    return p1
.end method

.method static synthetic access$15300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowItemNum:I

    return v0
.end method

.method static synthetic access$15400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    return-object v0
.end method

.method static synthetic access$15500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNeedHelpPage:Z

    return v0
.end method

.method static synthetic access$15502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNeedHelpPage:Z

    return p1
.end method

.method static synthetic access$15602(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPageCount:I

    return p1
.end method

.method static synthetic access$15700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/res/Configuration;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastConfig:Landroid/content/res/Configuration;

    return-object v0
.end method

.method static synthetic access$15800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/View$OnKeyListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyAppListWindowKeyListener:Landroid/view/View$OnKeyListener;

    return-object v0
.end method

.method static synthetic access$15900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/View$OnKeyListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLegacyAppListWindowKeyListener:Landroid/view/View$OnKeyListener;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShowViewPagerTray:Z

    return v0
.end method

.method static synthetic access$16000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShowViewPagerTray:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSelectedTaskId:I

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ILjava/lang/String;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I

    .prologue
    .line 141
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->checkPackageName(ILjava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    return v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsInputMethodShown:Z

    return p1
.end method

.method static synthetic access$2102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mInputMethodChanged:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionY:I

    return v0
.end method

.method static synthetic access$2202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionY:I

    return p1
.end method

.method static synthetic access$2300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setAppListHandlePosition(I)V

    return-void
.end method

.method static synthetic access$2400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;)Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;)Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mStartFlashBar:Z

    return v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentDownView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentDownView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationRecentIconAppearByHomeKey()V

    return-void
.end method

.method static synthetic access$2900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationRecentIcon()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getAppListPosition(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailType:I

    return v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->makeHistoryBarDialog(I)V

    return-void
.end method

.method static synthetic access$3200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditTextView:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissHistoryWindow(Z)V

    return-void
.end method

.method static synthetic access$3400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerSelectedPage:I

    return v0
.end method

.method static synthetic access$3402(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerSelectedPage:I

    return p1
.end method

.method static synthetic access$3500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMark:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkerSize:I

    return v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    return-object v0
.end method

.method static synthetic access$3702(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;)Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    return-object p1
.end method

.method static synthetic access$3800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMoveLeft:Z

    return v0
.end method

.method static synthetic access$3802(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMoveLeft:Z

    return p1
.end method

.method static synthetic access$3900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMoveRight:Z

    return v0
.end method

.method static synthetic access$3902(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMoveRight:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 141
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListPosition(IZZ)V

    return-void
.end method

.method static synthetic access$4000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerScrollHoverMargin:I

    return v0
.end method

.method static synthetic access$4100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/support/v4/view/ViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLunchBlock:Z

    return v0
.end method

.method static synthetic access$4202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLunchBlock:Z

    return p1
.end method

.method static synthetic access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I

    return v0
.end method

.method static synthetic access$4302(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I

    return p1
.end method

.method static synthetic access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$4402(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic access$4502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntents:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$4600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskId:I

    return v0
.end method

.method static synthetic access$4702(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskId:I

    return p1
.end method

.method static synthetic access$4800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskInfos:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShadowWidth:I

    return v0
.end method

.method static synthetic access$4902(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShadowWidth:I

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    return v0
.end method

.method static synthetic access$5000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShadowHeight:I

    return v0
.end method

.method static synthetic access$5002(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShadowHeight:I

    return p1
.end method

.method static synthetic access$502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    return p1
.end method

.method static synthetic access$5100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I

    return v0
.end method

.method static synthetic access$5102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I

    return p1
.end method

.method static synthetic access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I

    return v0
.end method

.method static synthetic access$5202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I

    return p1
.end method

.method static synthetic access$5300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$5400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->isLaunchingBlockedTask(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->cancelCollapseTimer()V

    return-void
.end method

.method static synthetic access$5600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startCollapseTimer()V

    return-void
.end method

.method static synthetic access$5700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/content/Intent;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Landroid/content/Intent;
    .param p2, "x2"    # Z

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V

    return-void
.end method

.method static synthetic access$5800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationHistoryBarDisappear()V

    return-void
.end method

.method static synthetic access$5902(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMakeInstanceAniRunning:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerTrayShow:Z

    return v0
.end method

.method static synthetic access$6000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNewInstanceIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$6002(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNewInstanceIntent:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic access$602(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerTrayShow:Z

    return p1
.end method

.method static synthetic access$6100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$6200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$6300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationHistoryBarThumbNailNew()V

    return-void
.end method

.method static synthetic access$6400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/view/View;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->findeCurIndex(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method static synthetic access$6500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->orgIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$6502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->orgIcon:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method static synthetic access$6600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    return v0
.end method

.method static synthetic access$6700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    return v0
.end method

.method static synthetic access$6800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListDragMode:Z

    return v0
.end method

.method static synthetic access$6802(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListDragMode:Z

    return p1
.end method

.method static synthetic access$6900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/GuideLineImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->closeFlashBar()V

    return-void
.end method

.method static synthetic access$7000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionGuideline:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$7100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddEmptyItemPosition:I

    return v0
.end method

.method static synthetic access$7102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddEmptyItemPosition:I

    return p1
.end method

.method static synthetic access$7200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;III)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 141
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->changlistItemFromEditList(III)Z

    move-result v0

    return v0
.end method

.method static synthetic access$7300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/view/DragEvent;

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->decideScrollMove(Landroid/view/View;Landroid/view/DragEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$7400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddToListItem:I

    return v0
.end method

.method static synthetic access$7402(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddToListItem:I

    return p1
.end method

.method static synthetic access$7500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->changelistItem(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$7600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->doOneTimeAfterDragStarts:Z

    return v0
.end method

.method static synthetic access$7602(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->doOneTimeAfterDragStarts:Z

    return p1
.end method

.method static synthetic access$7700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->addAndChangelistItem(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$7800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$7900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mScrollDirection:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/Window;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    return-object v0
.end method

.method static synthetic access$8000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$8100()I
    .locals 1

    .prologue
    .line 141
    sget v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->SCROLL_MOVE_DELAY:I

    return v0
.end method

.method static synthetic access$8120(I)I
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 141
    sget v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->SCROLL_MOVE_DELAY:I

    sub-int/2addr v0, p0

    sput v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->SCROLL_MOVE_DELAY:I

    return v0
.end method

.method static synthetic access$8200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$8300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$8400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->hideGuidelineWindow()V

    return-void
.end method

.method static synthetic access$8500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$8600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuidePosition(ZZ)V

    return-void
.end method

.method static synthetic access$8700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$8800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/Window;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    return-object v0
.end method

.method static synthetic access$8900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashingAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z

    return v0
.end method

.method static synthetic access$9000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGoalAnimationView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z

    return p1
.end method

.method static synthetic access$9100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFadingAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$9200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissTrayOpenDialog()V

    return-void
.end method

.method static synthetic access$9300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateIconIndex:I

    return v0
.end method

.method static synthetic access$9302(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateIconIndex:I

    return p1
.end method

.method static synthetic access$9400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->prepareAnimationIcon()V

    return-void
.end method

.method static synthetic access$9500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$9502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
    .param p1, "x1"    # Landroid/graphics/Point;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;

    return-object p1
.end method

.method static synthetic access$9600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$9700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$9800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->isPortrait()Z

    move-result v0

    return v0
.end method

.method static synthetic access$9900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOffsetForAnim:I

    return v0
.end method

.method private addAndChangelistItem(II)Z
    .locals 7
    .param p1, "dstIndex"    # I
    .param p2, "editIndex"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2328
    const-string v0, "LegacyAppListWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addAndChangelistItem src="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "dst=%d"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2330
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->moveToAppListItem(II)V

    .line 2331
    invoke-virtual {p0, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListRelayout(Z)V

    .line 2332
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I

    .line 2333
    return v6
.end method

.method private animationHistoryBarDisappear()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x12c

    const/4 v5, 0x0

    .line 1880
    const/4 v2, 0x0

    .line 1881
    .local v2, "translateAnimation":Landroid/view/animation/Animation;
    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    const/16 v4, 0x67

    if-ne v3, v4, :cond_0

    .line 1882
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    .end local v2    # "translateAnimation":Landroid/view/animation/Animation;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    invoke-direct {v2, v5, v3, v5, v5}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1886
    .restart local v2    # "translateAnimation":Landroid/view/animation/Animation;
    :goto_0
    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1887
    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1888
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1889
    new-instance v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$28;

    invoke-direct {v3, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$28;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1908
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v3, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1909
    .local v0, "alphaAnimation1":Landroid/view/animation/Animation;
    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1910
    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1912
    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1913
    .local v1, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1914
    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1915
    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v4, v5}, Landroid/view/animation/AnimationSet;->setStartOffset(J)V

    .line 1917
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 1918
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1919
    return-void

    .line 1884
    .end local v0    # "alphaAnimation1":Landroid/view/animation/Animation;
    .end local v1    # "animationSet":Landroid/view/animation/AnimationSet;
    :cond_0
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    .end local v2    # "translateAnimation":Landroid/view/animation/Animation;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v2, v5, v3, v5, v5}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v2    # "translateAnimation":Landroid/view/animation/Animation;
    goto :goto_0
.end method

.method private animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V
    .locals 13
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "animation"    # Z

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    const/4 v12, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 1965
    move-object v11, p1

    .line 1966
    .local v11, "sendIntent":Landroid/content/Intent;
    const/4 v0, 0x0

    .line 1967
    .local v0, "scaleAnimation":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    .end local v0    # "scaleAnimation":Landroid/view/animation/Animation;
    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 1969
    .restart local v0    # "scaleAnimation":Landroid/view/animation/Animation;
    const-wide/16 v6, 0x1f4

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1970
    invoke-virtual {v0, v5}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1972
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v9, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1973
    .local v9, "alphaAnimation1":Landroid/view/animation/Animation;
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v9, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1974
    const-wide/16 v2, 0x1f4

    invoke-virtual {v9, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1976
    new-instance v10, Landroid/view/animation/AnimationSet;

    invoke-direct {v10, v12}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1977
    .local v10, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1978
    invoke-virtual {v10, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1979
    new-instance v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$30;

    invoke-direct {v1, p0, v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$30;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/content/Intent;)V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1997
    if-eqz p2, :cond_0

    .line 1998
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 1999
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v10}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2007
    :goto_0
    return-void

    .line 2000
    :cond_0
    if-eqz v11, :cond_1

    .line 2001
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v1, v11}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitySafe(Landroid/content/Intent;)V

    .line 2002
    invoke-virtual {p0, v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateListStatusForIntent(Landroid/content/Intent;)V

    .line 2003
    invoke-direct {p0, v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissHistoryWindow(Z)V

    goto :goto_0

    .line 2005
    :cond_1
    invoke-direct {p0, v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissHistoryWindow(Z)V

    goto :goto_0
.end method

.method private animationHistoryBarThumbNailNew()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x1f4

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1851
    const/4 v0, 0x0

    .line 1852
    .local v0, "scaleAnimation":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    .end local v0    # "scaleAnimation":Landroid/view/animation/Animation;
    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 1854
    .restart local v0    # "scaleAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v0, v12, v13}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1855
    invoke-virtual {v0, v5}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1857
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v9, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1858
    .local v9, "alphaAnimation1":Landroid/view/animation/Animation;
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v9, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1859
    invoke-virtual {v9, v12, v13}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1861
    new-instance v10, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x0

    invoke-direct {v10, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1863
    .local v10, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v10, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1864
    new-instance v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$27;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$27;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1875
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 1876
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v10}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1877
    return-void
.end method

.method private animationHistoryBarUI()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x12c

    const/4 v6, 0x0

    .line 1922
    const/4 v3, 0x0

    .line 1923
    .local v3, "translateAnimation":Landroid/view/animation/Animation;
    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    packed-switch v4, :pswitch_data_0

    .line 1933
    :goto_0
    invoke-virtual {v3, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1934
    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1936
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v0, v6, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1937
    .local v0, "alphaAnimation1":Landroid/view/animation/Animation;
    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1938
    invoke-virtual {v0, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1940
    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v4, 0x0

    invoke-direct {v1, v4}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1941
    .local v1, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1942
    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1944
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 1945
    new-instance v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$29;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$29;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v1, v4}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1961
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1962
    return-void

    .line 1925
    .end local v0    # "alphaAnimation1":Landroid/view/animation/Animation;
    .end local v1    # "animationSet":Landroid/view/animation/AnimationSet;
    :pswitch_0
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    .end local v3    # "translateAnimation":Landroid/view/animation/Animation;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    invoke-direct {v3, v4, v6, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1926
    .restart local v3    # "translateAnimation":Landroid/view/animation/Animation;
    goto :goto_0

    .line 1928
    :pswitch_1
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1929
    .local v2, "outMetrics":Landroid/util/DisplayMetrics;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1930
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    .end local v3    # "translateAnimation":Landroid/view/animation/Animation;
    iget v4, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-direct {v3, v4, v6, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v3    # "translateAnimation":Landroid/view/animation/Animation;
    goto :goto_0

    .line 1923
    nop

    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private animationRecentIcon()V
    .locals 3

    .prologue
    .line 4215
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04003b

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 4216
    .local v0, "bubbleAnimation":Landroid/view/animation/Animation;
    new-instance v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$69;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$69;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4224
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mRecentIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 4225
    return-void
.end method

.method private animationRecentIconAppearByHomeKey()V
    .locals 5

    .prologue
    const v4, 0x7f04003a

    const/4 v3, 0x0

    .line 4130
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4131
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4133
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 4134
    .local v0, "animation1":Landroid/view/animation/Animation;
    new-instance v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$66;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$66;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4142
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 4143
    .local v1, "animation2":Landroid/view/animation/Animation;
    new-instance v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$67;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$67;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4152
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 4153
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 4155
    return-void
.end method

.method private animationRecentIconMoveByHomeKey()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x12c

    const/4 v1, 0x1

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 4159
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    div-float v11, v4, v6

    .line 4160
    .local v11, "distance":F
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    int-to-float v4, v4

    div-float/2addr v4, v6

    neg-float v4, v4

    neg-float v8, v11

    move v5, v1

    move v6, v2

    move v7, v3

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 4164
    .local v0, "translateAnimation1":Landroid/view/animation/Animation;
    invoke-virtual {v0, v12, v13}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 4165
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 4167
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3e4ccccd    # 0.2f

    invoke-direct {v9, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 4168
    .local v9, "alphaAnimation":Landroid/view/animation/Animation;
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v9, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 4169
    invoke-virtual {v9, v12, v13}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 4171
    new-instance v10, Landroid/view/animation/AnimationSet;

    invoke-direct {v10, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 4172
    .local v10, "animationSet1":Landroid/view/animation/AnimationSet;
    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 4173
    invoke-virtual {v10, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 4175
    new-instance v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$68;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$68;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4211
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 4212
    return-void
.end method

.method private animationTemplateIcon()V
    .locals 3

    .prologue
    .line 4040
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListRelayout(Z)V

    .line 4041
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateScrollPositionTemplate()V

    .line 4042
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04003b

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 4043
    .local v0, "bubbleAnimation":Landroid/view/animation/Animation;
    new-instance v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$63;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$63;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4051
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->templateAnimationIcon:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 4052
    return-void
.end method

.method private animationTemplateIconAppear()V
    .locals 5

    .prologue
    const v4, 0x7f04003a

    const/4 v3, 0x0

    .line 3939
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3940
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3942
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 3943
    .local v0, "animation1":Landroid/view/animation/Animation;
    new-instance v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$59;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$59;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3951
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 3952
    .local v1, "animation2":Landroid/view/animation/Animation;
    new-instance v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$60;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$60;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3962
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3963
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3964
    return-void
.end method

.method private animationTemplateIconMove()V
    .locals 23

    .prologue
    .line 3967
    const/4 v6, 0x0

    .line 3968
    .local v6, "distance1X":F
    const/4 v15, 0x0

    .line 3969
    .local v15, "distance2X":F
    const/4 v10, 0x0

    .line 3970
    .local v10, "distance1Y":F
    const/16 v19, 0x0

    .line 3972
    .local v19, "distance2Y":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    packed-switch v3, :pswitch_data_0

    .line 3983
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 3984
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getY()F

    move-result v3

    neg-float v10, v3

    .line 3985
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getY()F

    move-result v3

    neg-float v0, v3

    move/from16 v19, v0

    .line 3988
    :cond_0
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 3992
    .local v2, "translateAnimation1":Landroid/view/animation/Animation;
    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 3994
    new-instance v11, Landroid/view/animation/TranslateAnimation;

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-direct/range {v11 .. v19}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 3998
    .local v11, "translateAnimation2":Landroid/view/animation/Animation;
    const-wide/16 v4, 0x1f4

    invoke-virtual {v11, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 4001
    new-instance v20, Landroid/view/animation/AlphaAnimation;

    const/high16 v3, 0x3f800000    # 1.0f

    const v4, 0x3e4ccccd    # 0.2f

    move-object/from16 v0, v20

    invoke-direct {v0, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 4002
    .local v20, "alphaAnimation":Landroid/view/animation/Animation;
    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 4003
    const-wide/16 v4, 0x1f4

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 4005
    new-instance v21, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-direct {v0, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 4006
    .local v21, "animationSet1":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 4007
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 4009
    new-instance v22, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-direct {v0, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 4010
    .local v22, "animationSet2":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 4011
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 4013
    new-instance v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$61;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$61;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4024
    new-instance v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$62;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$62;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4035
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 4036
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 4037
    return-void

    .line 3974
    .end local v2    # "translateAnimation1":Landroid/view/animation/Animation;
    .end local v11    # "translateAnimation2":Landroid/view/animation/Animation;
    .end local v20    # "alphaAnimation":Landroid/view/animation/Animation;
    .end local v21    # "animationSet1":Landroid/view/animation/AnimationSet;
    .end local v22    # "animationSet2":Landroid/view/animation/AnimationSet;
    :pswitch_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->isPortrait()Z

    move-result v3

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    int-to-float v3, v3

    neg-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v6, v3, v4

    .line 3975
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->isPortrait()Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    int-to-float v3, v3

    neg-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v15, v3, v4

    .line 3976
    :goto_2
    goto/16 :goto_0

    .line 3974
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getX()F

    move-result v3

    neg-float v6, v3

    goto :goto_1

    .line 3975
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getX()F

    move-result v3

    neg-float v15, v3

    goto :goto_2

    .line 3978
    :pswitch_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->isPortrait()Z

    move-result v3

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v6, v3, v4

    .line 3979
    :goto_3
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->isPortrait()Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v15, v3, v4

    :goto_4
    goto/16 :goto_0

    .line 3978
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getX()F

    move-result v4

    sub-float v6, v3, v4

    goto :goto_3

    .line 3979
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getX()F

    move-result v4

    sub-float v15, v3, v4

    goto :goto_4

    .line 3972
    nop

    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private animationTemplateStart()V
    .locals 11

    .prologue
    const v10, 0x7f040035

    const/4 v9, 0x0

    .line 3883
    const/4 v6, 0x0

    .line 3884
    .local v6, "splitRect":Landroid/graphics/Rect;
    const/4 v4, 0x0

    .line 3885
    .local v4, "captureImage1":Landroid/graphics/Bitmap;
    const/4 v5, 0x0

    .line 3887
    .local v5, "captureImage2":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture1:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    invoke-direct {v2, v7}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3888
    .local v2, "animationCapture1Margin":Landroid/view/ViewGroup$MarginLayoutParams;
    new-instance v3, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture2:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    invoke-direct {v3, v7}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3890
    .local v3, "animationCapture2Margin":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListController;->updateWindowRects()V

    .line 3892
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListController;->getRectByZone(I)Landroid/graphics/Rect;

    move-result-object v6

    .line 3893
    invoke-direct {p0, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->GetCaptureImage(Landroid/graphics/Rect;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 3894
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    const/16 v8, 0xc

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListController;->getRectByZone(I)Landroid/graphics/Rect;

    move-result-object v6

    .line 3895
    invoke-direct {p0, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->GetCaptureImage(Landroid/graphics/Rect;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 3896
    if-eqz v4, :cond_0

    if-nez v5, :cond_1

    .line 3897
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->endAnimationIcon()V

    .line 3898
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationTemplateIcon()V

    .line 3936
    :goto_0
    return-void

    .line 3901
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture1:Landroid/widget/ImageView;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 3902
    iget v7, v6, Landroid/graphics/Rect;->left:I

    iget v8, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2, v7, v8, v9, v9}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 3903
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture2:Landroid/widget/ImageView;

    invoke-virtual {v7, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 3904
    iget v7, v6, Landroid/graphics/Rect;->left:I

    iget v8, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3, v7, v8, v9, v9}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 3906
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture1:Landroid/widget/ImageView;

    new-instance v8, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v8, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3907
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture2:Landroid/widget/ImageView;

    new-instance v8, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v8, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3909
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture1:Landroid/widget/ImageView;

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3910
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture2:Landroid/widget/ImageView;

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3912
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-static {v7, v10}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 3913
    .local v0, "animation1":Landroid/view/animation/Animation;
    new-instance v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$57;

    invoke-direct {v7, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$57;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v0, v7}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3922
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-static {v7, v10}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 3923
    .local v1, "animation2":Landroid/view/animation/Animation;
    new-instance v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$58;

    invoke-direct {v7, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$58;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v1, v7}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3933
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture1:Landroid/widget/ImageView;

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3934
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture2:Landroid/widget/ImageView;

    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private applyShadePressEffect(Landroid/view/MotionEvent;Landroid/view/View;I)V
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "type"    # I

    .prologue
    const/16 v7, 0x66

    .line 5726
    const/4 v0, 0x0

    .line 5727
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    const/16 v4, 0x64

    if-ne p3, v4, :cond_3

    .line 5729
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getCurrentAppList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, p2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 5730
    .local v2, "idxOfAppList":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_2

    .line 5731
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getIconByIndex(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 5758
    .end local v2    # "idxOfAppList":I
    :goto_0
    if-eqz v0, :cond_1

    .line 5759
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_6

    .line 5760
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 5765
    :cond_1
    :goto_1
    invoke-virtual {p2}, Landroid/view/View;->invalidate()V

    .line 5766
    :cond_2
    return-void

    .line 5737
    :cond_3
    const/4 v3, 0x0

    .line 5738
    .local v3, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    const/4 v1, 0x0

    .line 5740
    .local v1, "iconIndex":I
    if-ne p3, v7, :cond_5

    .line 5741
    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyUsedAppCnt:I

    if-lez v4, :cond_4

    .line 5742
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerSelectedPage:I

    add-int/lit8 v5, v5, -0x1

    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowItemNum:I

    mul-int/2addr v5, v6

    add-int v1, v4, v5

    .line 5746
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v4, v1, v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getViewPagerAppListItem(II)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v3

    .line 5752
    :goto_3
    if-eqz v3, :cond_2

    .line 5753
    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 5744
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerSelectedPage:I

    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowItemNum:I

    mul-int/2addr v5, v6

    add-int v1, v4, v5

    goto :goto_2

    .line 5748
    :cond_5
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 5749
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    const/16 v5, 0x68

    invoke-virtual {v4, v1, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getViewPagerAppListItem(II)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v3

    goto :goto_3

    .line 5762
    .end local v1    # "iconIndex":I
    .end local v3    # "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_6
    new-instance v4, Landroid/graphics/PorterDuffColorFilter;

    const/high16 v5, 0x44000000    # 512.0f

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v5, v6}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_1
.end method

.method private cancelCollapseTimer()V
    .locals 2

    .prologue
    const/16 v1, 0xc9

    .line 773
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 775
    :cond_0
    return-void
.end method

.method private changelistItem(II)Z
    .locals 4
    .param p1, "srcIndex"    # I
    .param p2, "dstIndex"    # I

    .prologue
    .line 2338
    if-ne p1, p2, :cond_0

    .line 2339
    const/4 v0, 0x0

    .line 2346
    :goto_0
    return v0

    .line 2341
    :cond_0
    const-string v0, "LegacyAppListWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChangeItem src=("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") dst=( "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2343
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0, p2, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->reorderApplist(II)V

    .line 2344
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZI)V

    .line 2345
    iput p2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I

    .line 2346
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private changlistItemFromEditList(III)Z
    .locals 9
    .param p1, "appListIndex"    # I
    .param p2, "editListIndex"    # I
    .param p3, "action"    # I

    .prologue
    const/4 v8, 0x1

    .line 2351
    const-string v4, "LegacyAppListWindow"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "changlistItemFromEditList action="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2352
    const-string v4, "LegacyAppListWindow"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "changlistItemFromEditList app="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " edit ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2354
    const/4 v4, 0x6

    if-ne p3, v4, :cond_1

    .line 2356
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    const/4 v5, 0x0

    invoke-virtual {v4, p1, p2, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->changeApplistItem(IIZ)V

    .line 2359
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->checkAppListLayout()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2360
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 2361
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2388
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    iget-boolean v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    iget v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZI)V

    .line 2389
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I

    .line 2390
    return v8

    .line 2364
    :cond_1
    const/4 v4, 0x2

    if-ne p3, v4, :cond_0

    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListDragMode:Z

    if-ne v4, v8, :cond_0

    .line 2366
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v4, p1, p2, v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->changeApplistItem(IIZ)V

    .line 2370
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2372
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030005

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4, v5, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2374
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2376
    .local v2, "item_v":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListItemDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 2377
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2378
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2380
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 2381
    const v4, 0x7f0f0057

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2382
    .local v0, "applistItem":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 2383
    .local v3, "lp":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v4, -0x2

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2384
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2386
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    check-cast v2, Landroid/widget/LinearLayout;

    .end local v2    # "item_v":Landroid/view/View;
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private checkPackageName(ILjava/lang/String;I)Z
    .locals 11
    .param p1, "pid"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "taskId"    # I

    .prologue
    .line 602
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mActivityManager:Landroid/app/ActivityManager;

    invoke-virtual {v8}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v5

    .line 603
    .local v5, "runningAppProcesses":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/16 v9, 0x64

    const/4 v10, 0x2

    invoke-virtual {v8, v9, v10}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v6

    .line 605
    .local v6, "runningTaskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v7, 0x0

    .line 606
    .local v7, "selectedTaskPackage":Ljava/lang/String;
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 607
    .local v2, "info":Landroid/app/ActivityManager$RunningTaskInfo;
    iget v8, v2, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    if-ne v8, p3, :cond_0

    .line 608
    iget-object v8, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-eqz v8, :cond_0

    .line 609
    iget-object v8, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    .line 615
    .end local v2    # "info":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_1
    if-eqz v5, :cond_5

    .line 616
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 618
    .local v4, "runningAppProcessInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :try_start_0
    iget v8, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v8, p1, :cond_2

    .line 619
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v8, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    array-length v8, v8

    if-ge v0, v8, :cond_2

    .line 620
    iget-object v8, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    aget-object v3, v8, v0

    .line 621
    .local v3, "processPackage":Ljava/lang/String;
    if-eqz v3, :cond_4

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    if-eqz v7, :cond_4

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    if-eqz v8, :cond_4

    .line 623
    :cond_3
    const/4 v8, 0x1

    .line 631
    .end local v0    # "i":I
    .end local v3    # "processPackage":Ljava/lang/String;
    .end local v4    # "runningAppProcessInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :goto_2
    return v8

    .line 619
    .restart local v0    # "i":I
    .restart local v3    # "processPackage":Ljava/lang/String;
    .restart local v4    # "runningAppProcessInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 631
    .end local v0    # "i":I
    .end local v3    # "processPackage":Ljava/lang/String;
    .end local v4    # "runningAppProcessInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_5
    const/4 v8, 0x0

    goto :goto_2

    .line 627
    .restart local v4    # "runningAppProcessInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :catch_0
    move-exception v8

    goto :goto_0
.end method

.method private clearTemplateAnimation()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x4

    .line 2722
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationBackground:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 2723
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationBackground:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2725
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->templateAnimationIcon:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 2726
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->templateAnimationIcon:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2727
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->templateAnimationIcon:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2729
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->templateAnimationIcon:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 2731
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture1:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 2732
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture1:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2733
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture1:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2735
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture1:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 2736
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture1:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2738
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture2:Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    .line 2739
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture2:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2740
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture2:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2742
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture2:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 2743
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2745
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    .line 2746
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2747
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2749
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 2750
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2752
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    if-eqz v0, :cond_a

    .line 2753
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 2754
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2756
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 2757
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2759
    :cond_a
    return-void
.end method

.method private closeFlashBar()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 3161
    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z

    if-nez v2, :cond_0

    .line 3186
    :goto_0
    return-void

    .line 3164
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    .line 3165
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v4, 0x7f08001a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3166
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->sendAccessibilityEvent(I)V

    .line 3168
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    .line 3169
    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3170
    .local v1, "moveBtnLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    const/16 v2, -0x2710

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 3171
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3174
    .end local v1    # "moveBtnLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->hideTraybarHelpPopup()V

    .line 3175
    invoke-direct {p0, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissHistoryWindow(Z)V

    .line 3177
    const/4 v0, 0x0

    .line 3178
    .local v0, "ani":Landroid/view/animation/Animation;
    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    const/16 v3, 0x68

    if-ne v2, v3, :cond_2

    .line 3179
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f040038

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 3183
    :goto_1
    invoke-direct {p0, v0, v5, v5, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setFlashBarAnimation(Landroid/view/animation/Animation;ZZZ)V

    .line 3184
    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    const/4 v3, 0x1

    invoke-direct {p0, v2, v5, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updatePocketTrayLayout(IZZ)V

    .line 3185
    invoke-virtual {p0, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setSemiTransStatusMode(Z)V

    goto :goto_0

    .line 3181
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f040037

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1
.end method

.method private decideScrollMove(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v6, 0x3

    .line 2265
    const/4 v2, -0x1

    .line 2266
    .local v2, "rawEnd":I
    const/4 v3, 0x0

    .line 2268
    .local v3, "scroll":I
    const/4 v0, 0x0

    .line 2270
    .local v0, "bRet":Z
    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    if-eqz v4, :cond_1

    .line 2271
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v4

    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v5

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    invoke-virtual {v5}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr v4, v5

    float-to-int v2, v4

    .line 2272
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOpenPocketTray:Z

    if-eqz v4, :cond_3

    .line 2273
    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    add-int/lit8 v4, v4, -0x28

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getEditAreaHeight()I

    move-result v5

    sub-int v3, v4, v5

    .line 2280
    :goto_0
    add-int/lit8 v4, v2, 0x64

    if-le v4, v3, :cond_4

    .line 2281
    const/4 v4, 0x4

    iput v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mScrollDirection:I

    .line 2282
    const/4 v0, 0x1

    .line 2292
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2293
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 2294
    .local v1, "msg":Landroid/os/Message;
    const/16 v4, 0x32

    sput v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->SCROLL_MOVE_DELAY:I

    .line 2295
    iput v6, v1, Landroid/os/Message;->what:I

    .line 2296
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v4, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2297
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->doOneTimeAfterDragStarts:Z

    .line 2298
    const/4 v0, 0x0

    .line 2300
    .end local v0    # "bRet":Z
    .end local v1    # "msg":Landroid/os/Message;
    :cond_2
    return v0

    .line 2275
    .restart local v0    # "bRet":Z
    :cond_3
    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    add-int/lit8 v4, v4, -0x28

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v5

    sub-int v3, v4, v5

    goto :goto_0

    .line 2283
    :cond_4
    const/16 v4, 0x64

    if-ge v2, v4, :cond_5

    .line 2284
    iput v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mScrollDirection:I

    .line 2285
    const/4 v0, 0x1

    goto :goto_1

    .line 2287
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2288
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_1
.end method

.method private dismissHistoryWindow(Z)V
    .locals 4
    .param p1, "reDraw"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 834
    if-nez p1, :cond_0

    .line 835
    iput v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentAppPosition:I

    .line 836
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I

    .line 838
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMakeInstanceAniRunning:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNewInstanceIntent:Landroid/content/Intent;

    if-eqz v0, :cond_1

    .line 839
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMakeInstanceAniRunning:Z

    .line 840
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNewInstanceIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitySafe(Landroid/content/Intent;)V

    .line 841
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNewInstanceIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateListStatusForIntent(Landroid/content/Intent;)V

    .line 842
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNewInstanceIntent:Landroid/content/Intent;

    .line 844
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_5

    .line 845
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 846
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 848
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 849
    :cond_2
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    .line 850
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v0, :cond_4

    .line 851
    :cond_3
    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListRelayout(Z)V

    .line 853
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 854
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 857
    :cond_5
    return-void
.end method

.method private dismissResetConfirmDialog()V
    .locals 2

    .prologue
    .line 3780
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$53;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$53;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3790
    return-void
.end method

.method private dismissTrayOpenDialog()V
    .locals 2

    .prologue
    .line 3567
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$43;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$43;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3577
    return-void
.end method

.method private enablePocketButtons(Z)V
    .locals 1
    .param p1, "clickable"    # Z

    .prologue
    .line 4264
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 4265
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 4266
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 4267
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 4268
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTrayHeader:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 4269
    if-nez p1, :cond_0

    .line 4270
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 4271
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 4272
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 4273
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 4275
    :cond_0
    return-void
.end method

.method private endAnimationIcon()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x4

    .line 4253
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationBackground:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4254
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4255
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4257
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 4258
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 4260
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->hideGuidelineWindow()V

    .line 4261
    return-void
.end method

.method private findHistoryBarPosisition(I)I
    .locals 9
    .param p1, "cnt"    # I

    .prologue
    const/4 v5, 0x0

    .line 1807
    const/4 v0, 0x0

    .line 1808
    .local v0, "barHeight":I
    const/4 v4, 0x0

    .line 1809
    .local v4, "top":I
    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailLayoutHeight:I

    iget v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailLayoutPadding:I

    add-int v2, v6, v7

    .line 1810
    .local v2, "thumbNailsHeight":I
    mul-int v3, v2, p1

    .line 1812
    .local v3, "thumbNailsLayoutHeight":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskInfos:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListController;->getAvailableMultiInstanceCnt()I

    move-result v7

    if-lt v6, v7, :cond_1

    .line 1813
    move v0, v3

    .line 1814
    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentAppPosition:I

    iget v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailImageHeight:I

    mul-int/2addr v7, p1

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v6, v7

    iget v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistotyBarTopMargin:I

    add-int v4, v6, v7

    .line 1819
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v7, 0x1050010

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1820
    .local v1, "statusBarHeight":I
    if-gez v4, :cond_3

    .line 1821
    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSystemUiVisibility:I

    if-nez v6, :cond_2

    move v4, v1

    .line 1830
    :cond_0
    :goto_1
    return v4

    .line 1816
    .end local v1    # "statusBarHeight":I
    :cond_1
    add-int/lit8 v6, p1, 0x1

    mul-int v0, v2, v6

    .line 1817
    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentAppPosition:I

    iget v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailImageHeight:I

    add-int/lit8 v8, p1, 0x1

    mul-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v6, v7

    iget v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistotyBarTopMargin:I

    add-int v4, v6, v7

    goto :goto_0

    .restart local v1    # "statusBarHeight":I
    :cond_2
    move v4, v5

    .line 1821
    goto :goto_1

    .line 1823
    :cond_3
    add-int v6, v4, v0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getHeight()I

    move-result v7

    if-le v6, v7, :cond_0

    .line 1824
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getHeight()I

    move-result v6

    sub-int v4, v6, v0

    .line 1825
    if-gez v4, :cond_0

    .line 1826
    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSystemUiVisibility:I

    if-nez v6, :cond_4

    move v4, v1

    :goto_2
    goto :goto_1

    :cond_4
    move v4, v5

    goto :goto_2
.end method

.method private findeCurIndex(Landroid/view/View;)I
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, -0x1

    .line 2394
    instance-of v4, p1, Landroid/widget/LinearLayout;

    if-nez v4, :cond_1

    move v1, v3

    .line 2403
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v2, p1

    .line 2397
    check-cast v2, Landroid/widget/LinearLayout;

    .line 2398
    .local v2, "item":Landroid/widget/LinearLayout;
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getCurrentAppList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    .line 2399
    .local v0, "appListSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 2400
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getCurrentAppList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eq v2, v4, :cond_0

    .line 2399
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v1, v3

    .line 2403
    goto :goto_0
.end method

.method private getAppListHandlePosition()I
    .locals 5

    .prologue
    const/high16 v4, 0x42c80000    # 100.0f

    .line 5234
    const/4 v1, 0x0

    .line 5235
    .local v1, "position":I
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a0018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    .line 5237
    .local v0, "moveBtnWidth":I
    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    packed-switch v2, :pswitch_data_0

    .line 5259
    :goto_0
    return v1

    .line 5239
    :pswitch_0
    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    int-to-float v3, v3

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 5240
    div-int/lit8 v2, v0, 0x2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnBoundary:I

    add-int/2addr v2, v3

    if-gt v1, v2, :cond_0

    .line 5241
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnBoundary:I

    goto :goto_0

    .line 5242
    :cond_0
    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnBoundary:I

    sub-int/2addr v2, v3

    if-lt v1, v2, :cond_1

    .line 5243
    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v2, v0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnBoundary:I

    sub-int v1, v2, v3

    goto :goto_0

    .line 5245
    :cond_1
    div-int/lit8 v2, v0, 0x2

    sub-int/2addr v1, v2

    .line 5247
    goto :goto_0

    .line 5249
    :pswitch_1
    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    int-to-float v3, v3

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 5250
    div-int/lit8 v2, v0, 0x2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnBoundary:I

    add-int/2addr v2, v3

    if-gt v1, v2, :cond_2

    .line 5251
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnBoundary:I

    goto :goto_0

    .line 5252
    :cond_2
    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnBoundary:I

    sub-int/2addr v2, v3

    if-lt v1, v2, :cond_3

    .line 5253
    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v2, v0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnBoundary:I

    sub-int v1, v2, v3

    goto :goto_0

    .line 5255
    :cond_3
    div-int/lit8 v2, v0, 0x2

    sub-int/2addr v1, v2

    goto :goto_0

    .line 5237
    nop

    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getAppListPosition(I)I
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/16 v2, 0x68

    const/16 v1, 0x67

    .line 3467
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHasCocktailBar:Z

    if-eqz v3, :cond_1

    .line 3468
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 3469
    .local v0, "rotation":I
    packed-switch v0, :pswitch_data_0

    .line 3483
    :goto_0
    iget p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    .line 3491
    .end local v0    # "rotation":I
    .end local p1    # "position":I
    :cond_0
    :goto_1
    return p1

    .line 3471
    .restart local v0    # "rotation":I
    .restart local p1    # "position":I
    :pswitch_0
    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    goto :goto_0

    .line 3474
    :pswitch_1
    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    goto :goto_0

    .line 3477
    :pswitch_2
    iput v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    goto :goto_0

    .line 3480
    :pswitch_3
    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    goto :goto_0

    .line 3484
    .end local v0    # "rotation":I
    :cond_1
    sget-boolean v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v3, :cond_0

    .line 3485
    const-string v3, "LEFT"

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPositonFromCSC:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move p1, v1

    .line 3486
    goto :goto_1

    :cond_2
    move p1, v2

    .line 3488
    goto :goto_1

    .line 3469
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getCurrentAppList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1371
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    return-object v0
.end method

.method private getDegreesForRotation(I)F
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 2015
    packed-switch p1, :pswitch_data_0

    .line 2023
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2017
    :pswitch_0
    const/high16 v0, 0x43870000    # 270.0f

    goto :goto_0

    .line 2019
    :pswitch_1
    const/high16 v0, 0x43340000    # 180.0f

    goto :goto_0

    .line 2021
    :pswitch_2
    const/high16 v0, 0x42b40000    # 90.0f

    goto :goto_0

    .line 2015
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getEditAreaHeight()I
    .locals 2

    .prologue
    .line 5535
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 5536
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 5538
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method private getFlashBarCurPosition()I
    .locals 4

    .prologue
    .line 2420
    new-instance v1, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    const-string v2, "FlashBarPosition"

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 2421
    .local v1, "prefs":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    const-string v2, "FlashBarPosition"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2422
    .local v0, "flashBarState":I
    const/16 v2, 0x67

    if-lt v0, v2, :cond_0

    const/16 v2, 0x68

    if-le v0, v2, :cond_1

    .line 2423
    :cond_0
    const-string v2, "LEFT"

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPositonFromCSC:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2424
    const/16 v0, 0x67

    .line 2429
    :cond_1
    :goto_0
    return v0

    .line 2426
    :cond_2
    const/16 v0, 0x68

    goto :goto_0
.end method

.method private getFlashBarHandlePosition()I
    .locals 3

    .prologue
    .line 2433
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    const-string v1, "FlashBarHandlePosition"

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 2439
    .local v0, "prefs":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    const-string v1, "FlashBarHandlePosition"

    const/16 v2, 0x32

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method private getWindowDefSize()V
    .locals 5

    .prologue
    const v4, 0x3f19999a    # 0.6f

    .line 6114
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 6115
    .local v1, "displaySize":Landroid/graphics/Point;
    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/hardware/display/DisplayManagerGlobal;->getRealDisplay(I)Landroid/view/Display;

    move-result-object v0

    .line 6116
    .local v0, "display":Landroid/view/Display;
    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 6118
    :cond_0
    iget v2, v1, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefWidth:I

    .line 6119
    iget v2, v1, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefHeight:I

    .line 6120
    return-void
.end method

.method private helpTapAnimationInit(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 3326
    const v0, 0x7f040050

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShowAnimation:Landroid/view/animation/Animation;

    .line 3327
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShowAnimation:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShowAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3328
    const v0, 0x7f04004e

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFadingAnimation:Landroid/view/animation/Animation;

    .line 3329
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFadingAnimation:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFadingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3330
    const v0, 0x7f04004f

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashingAnimation:Landroid/view/animation/Animation;

    .line 3331
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashingAnimation:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3332
    return-void
.end method

.method private hideGuidelineWindow()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2711
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->clearTemplateAnimation()V

    .line 2712
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    if-eqz v1, :cond_0

    .line 2713
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 2714
    .local v0, "l":Landroid/view/WindowManager$LayoutParams;
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2715
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 2716
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2717
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2719
    .end local v0    # "l":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    return-void
.end method

.method private initPocketTrayLayout()V
    .locals 9

    .prologue
    const v8, 0x7f0a006a

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 5557
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v4, 0x7f0f0046

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    .line 5558
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v4, 0x7f0f0047

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTrayHeader:Landroid/widget/RelativeLayout;

    .line 5559
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v4, 0x7f0f004a

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTrayBody:Landroid/widget/RelativeLayout;

    .line 5560
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v4, 0x7f0f0049

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    .line 5561
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v4, 0x7f0f004d

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditBtn:Landroid/widget/ImageButton;

    .line 5562
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v4, 0x7f0f004b

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;

    .line 5563
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v4, 0x7f0f004f

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpBtn:Landroid/widget/ImageButton;

    .line 5564
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v4, 0x7f0f004e

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditText:Landroid/widget/TextView;

    .line 5565
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditText:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->enableMultiSelection(Z)V

    .line 5566
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v4, 0x7f0f004c

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateText:Landroid/widget/TextView;

    .line 5567
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateText:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->enableMultiSelection(Z)V

    .line 5568
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v4, 0x7f0f0050

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpText:Landroid/widget/TextView;

    .line 5569
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpText:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->enableMultiSelection(Z)V

    .line 5570
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_2

    .line 5571
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    if-eqz v3, :cond_3

    .line 5572
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080002

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5576
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTrayHeader:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarOpenListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5577
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTrayHeader:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTrayHeaderTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 5578
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarOpenListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5579
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTrayHeaderTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 5580
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditBtn:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarEditListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5581
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditBtn:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 5582
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarTemplateListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5583
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 5584
    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHelphubVersion:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-static {}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isUsaFeature()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHelphubVersion:I

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isKnoxMode()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 5585
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v0, v3

    .line 5586
    .local v0, "pocketTrayFullHeight":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0a0069

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v1, v3

    .line 5587
    .local v1, "pocketTrayHelpButtonHeight":I
    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5588
    .local v2, "pocketTrayLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    sub-int v3, v0, v1

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 5589
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5590
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 5591
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpText:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 5605
    .end local v0    # "pocketTrayFullHeight":I
    .end local v1    # "pocketTrayHelpButtonHeight":I
    .end local v2    # "pocketTrayLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    :goto_1
    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    invoke-direct {p0, v3, v6, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updatePocketTrayLayout(IZZ)V

    .line 5607
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080003

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5608
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080004

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5610
    :cond_2
    return-void

    .line 5574
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080001

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 5593
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v3

    if-ne v3, v7, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    if-ne v3, v7, :cond_5

    .line 5595
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v0, v3

    .line 5596
    .restart local v0    # "pocketTrayFullHeight":I
    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5597
    .restart local v2    # "pocketTrayLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v0, v2, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 5598
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5599
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 5600
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpText:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 5602
    .end local v0    # "pocketTrayFullHeight":I
    .end local v2    # "pocketTrayLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpBtn:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarHelpListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5603
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpBtn:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_1
.end method

.method private initRecentLayout()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 5542
    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mRecentLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5543
    .local v1, "recentLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v4, 0x7f0f0010

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 5544
    .local v0, "recentLabel":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v4, 0x7f0f0011

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mRecentIcon:Landroid/widget/ImageView;

    .line 5545
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0a001e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v2, v3

    .line 5547
    .local v2, "topPadding":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mRecentLayout:Landroid/widget/LinearLayout;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 5549
    invoke-virtual {v1, v5, v2, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 5550
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mRecentLayout:Landroid/widget/LinearLayout;

    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5551
    const-string v3, "Recent"

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5552
    const/16 v3, 0x7f

    invoke-static {v3, v5, v5, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 5553
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mRecentIcon:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mRecentIconClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5554
    return-void
.end method

.method private initViewPageMark()V
    .locals 11

    .prologue
    const v10, 0x7f020130

    const/4 v9, -0x2

    const/high16 v8, 0x40000000    # 2.0f

    const v7, 0x3f99999a    # 1.2f

    .line 1293
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMark:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1294
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPageCount:I

    if-ge v0, v4, :cond_2

    .line 1295
    new-instance v1, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1296
    .local v1, "iv":Landroid/widget/ImageView;
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1297
    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 1299
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    if-nez v0, :cond_1

    .line 1300
    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyUsedAppCnt:I

    if-lez v4, :cond_0

    .line 1301
    const v4, 0x7f020131

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1305
    :goto_1
    invoke-virtual {v1}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    .line 1306
    .local v3, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v3, v7, v7}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1307
    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkerSize:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkerSize:I

    int-to-float v5, v5

    mul-float/2addr v5, v7

    sub-float/2addr v4, v5

    div-float/2addr v4, v8

    iget v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkerSize:I

    int-to-float v5, v5

    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkerSize:I

    int-to-float v6, v6

    mul-float/2addr v6, v7

    sub-float/2addr v5, v6

    div-float/2addr v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1309
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 1315
    .end local v3    # "matrix":Landroid/graphics/Matrix;
    :goto_2
    sget-object v4, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1316
    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkerSize:I

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1317
    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkerSize:I

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1318
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1320
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMark:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1321
    new-instance v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$20;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$20;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1294
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1303
    :cond_0
    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 1311
    :cond_1
    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1312
    const/16 v4, 0x66

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 1313
    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkerMarginLeft:I

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_2

    .line 1329
    .end local v1    # "iv":Landroid/widget/ImageView;
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerSelectedPage:I

    .line 1330
    return-void
.end method

.method private isLaunchingBlockedItem(I)Z
    .locals 1
    .param p1, "curDstIndex"    # I

    .prologue
    .line 5460
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/AppListController;->isLaunchingBlockedItem(I)Z

    move-result v0

    return v0
.end method

.method private isLaunchingBlockedTask(I)Z
    .locals 1
    .param p1, "taskId"    # I

    .prologue
    .line 5464
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/AppListController;->isLaunchingBlockedTask(I)Z

    move-result v0

    return v0
.end method

.method private isPortrait()Z
    .locals 2

    .prologue
    .line 439
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeFlashBarLayout(Z)V
    .locals 11
    .param p1, "bAni"    # Z

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2028
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v7, 0x7f0f0008

    invoke-virtual {v6, v7}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    .line 2029
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v7, 0x7f0f000d

    invoke-virtual {v6, v7}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppTrayBG:Landroid/widget/ImageView;

    .line 2030
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v7, 0x7f0f000b

    invoke-virtual {v6, v7}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ScrollView;

    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    .line 2031
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v7, 0x7f0f000c

    invoke-virtual {v6, v7}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    .line 2032
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v7, 0x7f0f0013

    invoke-virtual {v6, v7}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    .line 2033
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v7, 0x7f0f0015

    invoke-virtual {v6, v7}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditBtn:Landroid/widget/Button;

    .line 2034
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v7, 0x7f0f0016

    invoke-virtual {v6, v7}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateBtn:Landroid/widget/Button;

    .line 2035
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v7, 0x7f0f000f

    invoke-virtual {v6, v7}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mRecentLayout:Landroid/widget/LinearLayout;

    .line 2036
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->initRecentLayout()V

    .line 2037
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->initPocketTrayLayout()V

    .line 2039
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditBtn:Landroid/widget/Button;

    const/16 v7, 0x11

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setGravity(I)V

    .line 2040
    iget-boolean v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    if-eqz v6, :cond_3

    .line 2041
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditBtn:Landroid/widget/Button;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v8, 0x7f080002

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2047
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v6, v7}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2048
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v6, v7}, Landroid/widget/ScrollView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 2050
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditBtn:Landroid/widget/Button;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarEditListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2052
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateBtn:Landroid/widget/Button;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarTemplateListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2053
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateBtn:Landroid/widget/Button;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v8, 0x7f080003

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2055
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateBtn:Landroid/widget/Button;

    invoke-virtual {v6, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 2057
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 2058
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2059
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 2061
    sget-boolean v6, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-nez v6, :cond_0

    .line 2062
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    const v7, 0x7f0f000e

    invoke-virtual {v6, v7}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    .line 2064
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v6, :cond_1

    .line 2065
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2066
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 2067
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2068
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 2072
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 2074
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppCnt()I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAvailableAppCnt:I

    .line 2075
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 2077
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAvailableAppCnt:I

    if-ge v2, v6, :cond_4

    .line 2078
    const v6, 0x7f030005

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6, v7, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2080
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 2082
    .local v4, "item_v":Landroid/view/View;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListItemDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 2083
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2084
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2086
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 2088
    const v6, 0x7f0f0057

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2089
    .local v1, "applistItem":Landroid/widget/LinearLayout;
    const v6, 0x7f0f005c

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2090
    .local v0, "applabel":Landroid/widget/TextView;
    invoke-virtual {v0, v9}, Landroid/widget/TextView;->enableMultiSelection(Z)V

    .line 2091
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 2092
    .local v5, "lp":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v6, -0x2

    iput v6, v5, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2093
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2095
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    move-object v6, v4

    check-cast v6, Landroid/widget/LinearLayout;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2097
    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateIconIndex:I

    if-ne v2, v6, :cond_2

    .line 2098
    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->templateAnimationIcon:Landroid/view/View;

    .line 2077
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2044
    .end local v0    # "applabel":Landroid/widget/TextView;
    .end local v1    # "applistItem":Landroid/widget/LinearLayout;
    .end local v2    # "i":I
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    .end local v4    # "item_v":Landroid/view/View;
    .end local v5    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditBtn:Landroid/widget/Button;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v8, 0x7f080001

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2102
    .restart local v2    # "i":I
    .restart local v3    # "inflater":Landroid/view/LayoutInflater;
    :cond_4
    if-eqz p1, :cond_5

    .line 2103
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getFlashBarState()Z

    move-result v6

    if-nez v6, :cond_6

    .line 2104
    invoke-direct {p0, v9, v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startFlashBarAnimation(ZZ)V

    .line 2110
    :cond_5
    :goto_2
    return-void

    .line 2106
    :cond_6
    invoke-virtual {p0, v10}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setFlashBarState(Z)V

    .line 2107
    invoke-direct {p0, v10, v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startFlashBarAnimation(ZZ)V

    goto :goto_2
.end method

.method private makeGuideLineLayout()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 4842
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f008d

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    .line 4843
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f008e

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    .line 4844
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f0091

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    .line 4845
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f0094

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    .line 4846
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f0095

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionGuideline:Landroid/widget/ImageView;

    .line 4847
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f0096

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFocusGuideline:Landroid/widget/ImageView;

    .line 4848
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f0097

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationBackground:Landroid/widget/ImageView;

    .line 4849
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f0098

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture1:Landroid/widget/ImageView;

    .line 4850
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f0099

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture2:Landroid/widget/ImageView;

    .line 4851
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f009c

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    .line 4852
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f009d

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    .line 4853
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4854
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4855
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/FlashBarService/GuideLineImageView;->setVisibility(I)V

    .line 4856
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionGuideline:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4857
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFocusGuideline:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4858
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationBackground:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4859
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture1:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4860
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationCapture2:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4861
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4862
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4863
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0a0056

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    .line 4865
    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v1, :cond_0

    .line 4866
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionGuideline:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1080669

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 4869
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 4871
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 4872
    .local v0, "l":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 4873
    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 4874
    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 4875
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FlashBarService/GuideLine "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 4876
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 4877
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 4878
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 4880
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$84;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$84;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 4889
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040043

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuideOpenAnimation:Landroid/view/animation/Animation;

    .line 4890
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuideOpenAnimation:Landroid/view/animation/Animation;

    new-instance v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$85;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$85;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4910
    return-void
.end method

.method private makeHistoryBarDialog(I)V
    .locals 37
    .param p1, "type"    # I

    .prologue
    .line 1435
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    move-object/from16 v33, v0

    const-string v34, "layout_inflater"

    invoke-virtual/range {v33 .. v34}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/view/LayoutInflater;

    .line 1437
    .local v15, "layoutInflater":Landroid/view/LayoutInflater;
    const/16 v30, 0x0

    .line 1438
    .local v30, "thumbNailCnt":I
    const/4 v10, 0x0

    .line 1439
    .local v10, "hasThumb":Z
    const/4 v13, 0x0

    .line 1440
    .local v13, "isFailedGetThumbnail":Z
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailType:I

    .line 1442
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-object/from16 v33, v0

    const/16 v34, 0x64

    const/16 v35, 0x2

    invoke-virtual/range {v33 .. v35}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v26

    .line 1444
    .local v26, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastConfig:Landroid/content/res/Configuration;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v33, v0

    const/16 v34, 0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_5

    .line 1445
    const/high16 v33, 0x7f030000

    const/16 v34, 0x0

    move/from16 v0, v33

    move-object/from16 v1, v34

    invoke-virtual {v15, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogView:Landroid/view/View;

    .line 1449
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v33, v0

    const v34, 0x7f0f0001

    invoke-virtual/range {v33 .. v34}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/ScrollView;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogScrollView:Landroid/widget/ScrollView;

    .line 1450
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogScrollView:Landroid/widget/ScrollView;

    move-object/from16 v33, v0

    new-instance v34, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$22;

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$22;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual/range {v33 .. v34}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1468
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v33, v0

    const v34, 0x7f0f0002

    invoke-virtual/range {v33 .. v34}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    .line 1469
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v33, v0

    const v34, 0x7f0f0004

    invoke-virtual/range {v33 .. v34}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    .line 1470
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v33, v0

    const v34, 0x7f0f0005

    invoke-virtual/range {v33 .. v34}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    .line 1471
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v33, v0

    const v34, 0x7f0f0007

    invoke-virtual/range {v33 .. v34}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    .line 1472
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v33, v0

    const v34, 0x7f0f0006

    invoke-virtual/range {v33 .. v34}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/ImageView;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarNewThumbIcon:Landroid/widget/ImageView;

    .line 1474
    new-instance v33, Landroid/app/Dialog;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-direct/range {v33 .. v34}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    .line 1475
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    move-object/from16 v33, v0

    const/16 v34, 0x1

    invoke-virtual/range {v33 .. v34}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 1476
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v34, v0

    invoke-virtual/range {v33 .. v34}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 1478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v32

    .line 1479
    .local v32, "w":Landroid/view/Window;
    const/high16 v33, 0x10000

    invoke-virtual/range {v32 .. v33}, Landroid/view/Window;->addFlags(I)V

    .line 1480
    const/16 v33, 0x8

    invoke-virtual/range {v32 .. v33}, Landroid/view/Window;->addFlags(I)V

    .line 1481
    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1482
    const/16 v33, -0x3

    invoke-virtual/range {v32 .. v33}, Landroid/view/Window;->setFormat(I)V

    .line 1483
    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/view/Window;->setDimAmount(F)V

    .line 1485
    invoke-virtual/range {v32 .. v32}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v22

    .line 1486
    .local v22, "p":Landroid/view/WindowManager$LayoutParams;
    const/16 v33, 0x3e8

    move/from16 v0, v33

    move-object/from16 v1, v22

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 1487
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v22

    iput-object v0, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 1489
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskInfos:Ljava/util/List;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Ljava/util/List;->clear()V

    .line 1490
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I

    move/from16 v33, v0

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I

    .line 1491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I

    move/from16 v34, v0

    invoke-virtual/range {v33 .. v34}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getComponentInfo(I)Landroid/content/pm/ComponentInfo;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ActivityInfo;

    .line 1492
    .local v7, "activityInfo":Landroid/content/pm/ActivityInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSupportMultiInstance:Z

    move/from16 v35, v0

    const/16 v36, 0x64

    invoke-virtual/range {v33 .. v36}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v14

    .line 1495
    .local v14, "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I

    move-result v33

    add-int/lit8 v11, v33, -0x1

    .local v11, "i":I
    :goto_1
    if-ltz v11, :cond_4

    .line 1496
    move-object/from16 v0, v26

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1497
    .local v25, "t":Landroid/app/ActivityManager$RunningTaskInfo;
    if-eqz v7, :cond_0

    iget-object v0, v7, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v33, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-nez v33, :cond_2

    :cond_0
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v33

    if-lez v33, :cond_1

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    move-object/from16 v33, v0

    if-eqz v33, :cond_1

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v34

    const/16 v33, 0x0

    move/from16 v0, v33

    invoke-interface {v14, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Landroid/content/Intent;

    invoke-virtual/range {v33 .. v33}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-nez v33, :cond_2

    :cond_1
    const/16 v33, 0x2

    move/from16 v0, p1

    move/from16 v1, v33

    if-ne v0, v1, :cond_b

    .line 1502
    :cond_2
    :try_start_0
    move-object/from16 v0, v25

    iget v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    move/from16 v28, v0

    .line 1503
    .local v28, "taskid":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mActivityManager:Landroid/app/ActivityManager;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getTaskThumbnail(I)Landroid/app/ActivityManager$TaskThumbnail;

    move-result-object v27

    .line 1504
    .local v27, "taskThumbnail":Landroid/app/ActivityManager$TaskThumbnail;
    if-eqz v27, :cond_6

    move-object/from16 v0, v27

    iget-object v0, v0, Landroid/app/ActivityManager$TaskThumbnail;->mainThumbnail:Landroid/graphics/Bitmap;

    move-object/from16 v29, v0

    .line 1506
    .local v29, "thumb":Landroid/graphics/Bitmap;
    :goto_2
    if-nez v29, :cond_3

    .line 1507
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I

    move/from16 v34, v0

    invoke-virtual/range {v33 .. v34}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getIconByIndex(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    check-cast v8, Landroid/graphics/drawable/BitmapDrawable;

    .line 1508
    .local v8, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    const/4 v13, 0x1

    .line 1509
    invoke-virtual {v8}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v29

    .line 1512
    .end local v8    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    :cond_3
    if-eqz v29, :cond_b

    .line 1513
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskInfos:Ljava/util/List;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v33

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/app/FlashBarService/AppListController;->getAvailableMultiInstanceCnt()I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v34

    move/from16 v0, v33

    move/from16 v1, v34

    if-lt v0, v1, :cond_7

    .line 1756
    .end local v25    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v27    # "taskThumbnail":Landroid/app/ActivityManager$TaskThumbnail;
    .end local v28    # "taskid":I
    .end local v29    # "thumb":Landroid/graphics/Bitmap;
    :cond_4
    if-eqz v10, :cond_d

    .line 1757
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v24

    .line 1758
    .local v24, "size":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/sec/android/app/FlashBarService/AppListController;->getAvailableMultiInstanceCnt()I

    move-result v33

    move/from16 v0, v24

    move/from16 v1, v33

    if-lt v0, v1, :cond_c

    .line 1759
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v33, v0

    const/16 v34, 0x8

    invoke-virtual/range {v33 .. v34}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1760
    const/4 v11, 0x0

    :goto_3
    move/from16 v0, v24

    if-ge v11, v0, :cond_c

    .line 1761
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    invoke-virtual {v0, v11}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/RelativeLayout;

    .line 1762
    .local v23, "r":Landroid/widget/RelativeLayout;
    new-instance v16, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual/range {v23 .. v23}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v33

    move-object/from16 v0, v16

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1763
    .local v16, "layoutMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailLayoutHeight:I

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailLayoutPadding:I

    move/from16 v34, v0

    add-int v33, v33, v34

    sub-int v34, v24, v11

    add-int/lit8 v34, v34, -0x1

    mul-int v19, v33, v34

    .line 1764
    .local v19, "margin":I
    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v33

    move/from16 v2, v19

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1765
    new-instance v33, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v33

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1760
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 1447
    .end local v7    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .end local v11    # "i":I
    .end local v14    # "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .end local v16    # "layoutMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v19    # "margin":I
    .end local v22    # "p":Landroid/view/WindowManager$LayoutParams;
    .end local v23    # "r":Landroid/widget/RelativeLayout;
    .end local v24    # "size":I
    .end local v32    # "w":Landroid/view/Window;
    :cond_5
    const v33, 0x7f030001

    const/16 v34, 0x0

    move/from16 v0, v33

    move-object/from16 v1, v34

    invoke-virtual {v15, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogView:Landroid/view/View;

    goto/16 :goto_0

    .line 1504
    .restart local v7    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .restart local v11    # "i":I
    .restart local v14    # "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .restart local v22    # "p":Landroid/view/WindowManager$LayoutParams;
    .restart local v25    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .restart local v27    # "taskThumbnail":Landroid/app/ActivityManager$TaskThumbnail;
    .restart local v28    # "taskid":I
    .restart local v32    # "w":Landroid/view/Window;
    :cond_6
    const/16 v29, 0x0

    goto/16 :goto_2

    .line 1515
    .restart local v29    # "thumb":Landroid/graphics/Bitmap;
    :cond_7
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskInfos:Ljava/util/List;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1517
    new-instance v6, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    invoke-direct {v6, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1518
    .local v6, "AppThumbnailLayout":Landroid/widget/RelativeLayout;
    new-instance v17, Landroid/view/ViewGroup$LayoutParams;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailLayoutWidth:I

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailLayoutHeight:I

    move/from16 v34, v0

    move-object/from16 v0, v17

    move/from16 v1, v33

    move/from16 v2, v34

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1519
    .local v17, "layoutParam":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1520
    const v33, 0x7f020073

    move/from16 v0, v33

    invoke-virtual {v6, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 1522
    new-instance v5, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    invoke-direct {v5, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1523
    .local v5, "AppThumbnailImageView":Landroid/widget/ImageView;
    sget-object v33, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    move-object/from16 v0, v33

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1524
    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1525
    if-eqz v13, :cond_8

    .line 1526
    const v33, 0x3f19999a    # 0.6f

    move/from16 v0, v33

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1528
    :cond_8
    new-instance v18, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailImageWidth:I

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailImageHeight:I

    move/from16 v34, v0

    move-object/from16 v0, v18

    move/from16 v1, v33

    move/from16 v2, v34

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1529
    .local v18, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v33, 0xd

    move-object/from16 v0, v18

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1530
    move-object/from16 v0, v18

    invoke-virtual {v6, v5, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1532
    new-instance v12, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    invoke-direct {v12, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1533
    .local v12, "imageView":Landroid/widget/ImageView;
    const v33, 0x7f0200ee

    move/from16 v0, v33

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1534
    new-instance v18, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v18    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThbumNailCancelImageSize:I

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThbumNailCancelImageSize:I

    move/from16 v34, v0

    move-object/from16 v0, v18

    move/from16 v1, v33

    move/from16 v2, v34

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1535
    .restart local v18    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    move-object/from16 v33, v0

    const v34, 0x7f0a0062

    invoke-virtual/range {v33 .. v34}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v33

    move/from16 v0, v33

    float-to-int v0, v0

    move/from16 v21, v0

    .line 1536
    .local v21, "minusIconTopMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    move-object/from16 v33, v0

    const v34, 0x7f0a0063

    invoke-virtual/range {v33 .. v34}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v33

    move/from16 v0, v33

    float-to-int v0, v0

    move/from16 v20, v0

    .line 1537
    .local v20, "minusIconRightMargin":I
    move/from16 v0, v21

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1538
    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1539
    const/16 v33, 0xb

    move-object/from16 v0, v18

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1540
    const/16 v33, 0xa

    move-object/from16 v0, v18

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1541
    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1542
    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1543
    new-instance v33, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$23;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$23;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    move-object/from16 v0, v33

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1563
    invoke-virtual {v6, v12}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1565
    if-nez v10, :cond_a

    .line 1566
    const/4 v10, 0x1

    .line 1567
    const/16 v33, 0x2

    move/from16 v0, p1

    move/from16 v1, v33

    if-eq v0, v1, :cond_a

    .line 1568
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarNewThumbIcon:Landroid/widget/ImageView;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I

    move/from16 v35, v0

    invoke-virtual/range {v34 .. v35}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getIconByIndex(I)Landroid/graphics/drawable/Drawable;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1569
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v33

    if-nez v33, :cond_9

    .line 1570
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v33, v0

    const/16 v34, 0x8

    invoke-virtual/range {v33 .. v34}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1571
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarLongClickListener:Landroid/view/View$OnLongClickListener;

    move-object/from16 v34, v0

    invoke-virtual/range {v33 .. v34}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1572
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v33, v0

    new-instance v34, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual/range {v33 .. v34}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1685
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarLongClickListener:Landroid/view/View$OnLongClickListener;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    invoke-virtual {v6, v0}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1686
    new-instance v33, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    move-object/from16 v0, v33

    invoke-virtual {v6, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1725
    new-instance v33, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$26;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$26;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    move-object/from16 v0, v33

    invoke-virtual {v6, v0}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1743
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1744
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v30

    .line 1745
    new-instance v31, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v33

    move-object/from16 v0, v31

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1746
    .local v31, "thumbnailMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailLayoutHeight:I

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailLayoutPadding:I

    move/from16 v34, v0

    add-int v33, v33, v34

    mul-int v19, v33, v30

    .line 1747
    .restart local v19    # "margin":I
    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    move-object/from16 v0, v31

    move/from16 v1, v33

    move/from16 v2, v19

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1748
    new-instance v33, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v33

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    move-object/from16 v0, v33

    invoke-virtual {v6, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1495
    .end local v5    # "AppThumbnailImageView":Landroid/widget/ImageView;
    .end local v6    # "AppThumbnailLayout":Landroid/widget/RelativeLayout;
    .end local v12    # "imageView":Landroid/widget/ImageView;
    .end local v17    # "layoutParam":Landroid/view/ViewGroup$LayoutParams;
    .end local v18    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v19    # "margin":I
    .end local v20    # "minusIconRightMargin":I
    .end local v21    # "minusIconTopMargin":I
    .end local v27    # "taskThumbnail":Landroid/app/ActivityManager$TaskThumbnail;
    .end local v28    # "taskid":I
    .end local v29    # "thumb":Landroid/graphics/Bitmap;
    .end local v31    # "thumbnailMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_b
    :goto_4
    add-int/lit8 v11, v11, -0x1

    goto/16 :goto_1

    .line 1750
    :catch_0
    move-exception v9

    .line 1751
    .local v9, "e":Ljava/lang/SecurityException;
    invoke-virtual {v9}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_4

    .line 1768
    .end local v9    # "e":Ljava/lang/SecurityException;
    .end local v25    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .restart local v24    # "size":I
    :cond_c
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->showHistoryBarUi(II)V

    .line 1769
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startCollapseTimer()V

    .line 1773
    .end local v24    # "size":I
    :goto_5
    return-void

    .line 1771
    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->closeFlashBar()V

    goto :goto_5
.end method

.method private openFlashBar()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 3140
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z

    if-eqz v1, :cond_0

    .line 3158
    :goto_0
    return-void

    .line 3143
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 3144
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f080019

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3145
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->sendAccessibilityEvent(I)V

    .line 3148
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getGlobalSystemUiVisibility()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSystemUiVisibility:I

    .line 3150
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListRelayout(Z)V

    .line 3152
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AppListController;->updateWindowRects()V

    .line 3153
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentGuideRect()Landroid/graphics/Rect;

    move-result-object v0

    .line 3154
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 3155
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget v2, v2, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->setPreviewFullAppZone(I)V

    .line 3157
    invoke-direct {p0, v4, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startFlashBarAnimation(ZZ)V

    goto :goto_0
.end method

.method private openHelpHub()V
    .locals 4

    .prologue
    const/high16 v3, 0x30200000

    .line 4822
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHelphubVersion:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHelphubVersion:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 4824
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.helphub.HELP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4825
    .local v0, "helpIntent":Landroid/content/Intent;
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 4826
    const-string v1, "helphub:section"

    const-string v2, "multi_window"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4827
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 4837
    .end local v0    # "helpIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 4828
    :cond_1
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHelphubVersion:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 4830
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.helphub.HELP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4831
    .restart local v0    # "helpIntent":Landroid/content/Intent;
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 4832
    const-string v1, "helphub:appid"

    const-string v2, "multi_window"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4833
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0

    .line 4835
    .end local v0    # "helpIntent":Landroid/content/Intent;
    :cond_2
    const-string v1, "LegacyAppListWindow"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mHelphubVersion = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHelphubVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private prepareAnimationIcon()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 4228
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 4229
    .local v0, "l":Landroid/view/WindowManager$LayoutParams;
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    rsub-int/lit8 v1, v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 4230
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    rsub-int/lit8 v1, v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 4231
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 4232
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 4233
    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v1, :cond_1

    .line 4234
    :cond_0
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 4235
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 4237
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 4238
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 4239
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 4241
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationBackground:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 4242
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->isPortrait()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4243
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationBackground:Landroid/widget/ImageView;

    const v2, 0x7f02007b

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 4247
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationBackground:Landroid/widget/ImageView;

    const/16 v2, 0x5a

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 4248
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationBackground:Landroid/widget/ImageView;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v1, v5, v5, v2, v3}, Landroid/widget/ImageView;->layout(IIII)V

    .line 4249
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationBackground:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4250
    return-void

    .line 4245
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationBackground:Landroid/widget/ImageView;

    const v2, 0x7f02007c

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private saveFlashBarCurPosition(I)V
    .locals 3
    .param p1, "flashBarPosition"    # I

    .prologue
    .line 2408
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    const-string v1, "FlashBarPosition"

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 2409
    .local v0, "prefs":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    const-string v1, "FlashBarPosition"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putInt(Ljava/lang/String;I)V

    .line 2410
    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->commit()V

    .line 2411
    return-void
.end method

.method private saveFlashBarHandlePosition(I)V
    .locals 3
    .param p1, "handlePosition"    # I

    .prologue
    .line 2414
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    const-string v1, "FlashBarHandlePosition"

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 2415
    .local v0, "prefs":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    const-string v1, "FlashBarHandlePosition"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putInt(Ljava/lang/String;I)V

    .line 2416
    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->commit()V

    .line 2417
    return-void
.end method

.method private setAppListHandlePosition(I)V
    .locals 8
    .param p1, "position"    # I

    .prologue
    const/high16 v7, 0x425c0000    # 55.0f

    const/high16 v4, 0x42340000    # 45.0f

    const/4 v6, 0x0

    const/high16 v3, 0x42c80000    # 100.0f

    const/high16 v5, 0x42480000    # 50.0f

    .line 5168
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0a0018

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v0, v1

    .line 5169
    .local v0, "moveBtnWidth":I
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    packed-switch v1, :pswitch_data_0

    .line 5231
    :goto_0
    return-void

    .line 5171
    :pswitch_0
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mConfigChanged:Z

    if-eqz v1, :cond_1

    .line 5172
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastHandlePosition:I

    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    if-le v1, v2, :cond_0

    .line 5173
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mChangedPosition:I

    int-to-float v2, v0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v2, v5

    float-to-int v2, v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    .line 5177
    :goto_1
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mConfigChanged:Z

    .line 5198
    :goto_2
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->saveFlashBarHandlePosition(I)V

    goto :goto_0

    .line 5175
    :cond_0
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastHandlePosition:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    goto :goto_1

    .line 5178
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mInputMethodChanged:Z

    if-eqz v1, :cond_4

    .line 5179
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsInputMethodShown:Z

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    if-le v1, v2, :cond_3

    .line 5180
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mChangedPosition:I

    int-to-float v2, v0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v2, v5

    float-to-int v2, v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    .line 5184
    :cond_2
    :goto_3
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mInputMethodChanged:Z

    goto :goto_2

    .line 5181
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsInputMethodShown:Z

    if-nez v1, :cond_2

    .line 5182
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastHandlePosition:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    goto :goto_3

    .line 5186
    :cond_4
    div-int/lit8 v1, v0, 0x2

    sub-int v1, p1, v1

    if-gtz v1, :cond_5

    .line 5187
    iput v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    .line 5196
    :goto_4
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastHandlePosition:I

    goto :goto_2

    .line 5188
    :cond_5
    div-int/lit8 v1, v0, 0x2

    add-int/2addr v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    if-le v1, v2, :cond_6

    .line 5189
    const/16 v1, 0x64

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    goto :goto_4

    .line 5191
    :cond_6
    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    cmpl-float v1, v1, v4

    if-lez v1, :cond_7

    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    cmpg-float v1, v1, v7

    if-gez v1, :cond_7

    .line 5192
    const/16 v1, 0x32

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    goto :goto_4

    .line 5194
    :cond_7
    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    goto :goto_4

    .line 5201
    :pswitch_1
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mConfigChanged:Z

    if-eqz v1, :cond_9

    .line 5202
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastHandlePosition:I

    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    if-le v1, v2, :cond_8

    .line 5203
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mChangedPosition:I

    int-to-float v2, v0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v2, v5

    float-to-int v2, v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    .line 5207
    :goto_5
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mConfigChanged:Z

    .line 5228
    :goto_6
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->saveFlashBarHandlePosition(I)V

    goto/16 :goto_0

    .line 5205
    :cond_8
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastHandlePosition:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    goto :goto_5

    .line 5208
    :cond_9
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mInputMethodChanged:Z

    if-eqz v1, :cond_c

    .line 5209
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsInputMethodShown:Z

    if-eqz v1, :cond_b

    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    if-le v1, v2, :cond_b

    .line 5210
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mChangedPosition:I

    int-to-float v2, v0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v2, v5

    float-to-int v2, v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    .line 5214
    :cond_a
    :goto_7
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mInputMethodChanged:Z

    goto :goto_6

    .line 5211
    :cond_b
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsInputMethodShown:Z

    if-nez v1, :cond_a

    .line 5212
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastHandlePosition:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    goto :goto_7

    .line 5216
    :cond_c
    div-int/lit8 v1, v0, 0x2

    sub-int v1, p1, v1

    if-gtz v1, :cond_d

    .line 5217
    iput v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    .line 5226
    :goto_8
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastHandlePosition:I

    goto :goto_6

    .line 5218
    :cond_d
    div-int/lit8 v1, v0, 0x2

    add-int/2addr v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    if-le v1, v2, :cond_e

    .line 5219
    const/16 v1, 0x64

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    goto :goto_8

    .line 5221
    :cond_e
    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    cmpl-float v1, v1, v4

    if-lez v1, :cond_f

    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    cmpg-float v1, v1, v7

    if-gez v1, :cond_f

    .line 5222
    const/16 v1, 0x32

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    goto :goto_8

    .line 5224
    :cond_f
    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandlePosition:I

    goto :goto_8

    .line 5169
    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setFlashBarAnimation(Landroid/view/animation/Animation;ZZZ)V
    .locals 7
    .param p1, "ani"    # Landroid/view/animation/Animation;
    .param p2, "expand"    # Z
    .param p3, "finishtray"    # Z
    .param p4, "bStart"    # Z

    .prologue
    const/16 v6, 0xca

    const/16 v3, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3208
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    if-nez v2, :cond_0

    .line 3324
    :goto_0
    return-void

    .line 3211
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v2, p2}, Lcom/sec/android/app/FlashBarService/AppListController;->setMWTrayOpenState(Z)V

    .line 3215
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

    invoke-interface {v2, p2}, Lcom/android/internal/statusbar/IStatusBarService;->setMultiWindowBg(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3220
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 3221
    .local v0, "currentAni":Landroid/view/animation/Animation;
    if-eqz v0, :cond_1

    .line 3222
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3223
    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 3225
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-ne v2, v3, :cond_1

    .line 3226
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3230
    :cond_1
    if-eqz p2, :cond_4

    .line 3231
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 3232
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3235
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->checkCreateTemplateEnable()Z

    move-result v2

    if-nez v2, :cond_3

    .line 3236
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;

    const v3, 0x7f0200c4

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3237
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setSoundEffectsEnabled(Z)V

    .line 3238
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 3239
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateText:Landroid/widget/TextView;

    const v3, 0x3f19999a    # 0.6f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 3247
    :goto_2
    new-instance v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;

    invoke-direct {v2, p0, p2, p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ZLandroid/view/animation/Animation;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastPostAnimationRunnable:Ljava/lang/Runnable;

    .line 3278
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastPostAnimationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 3216
    .end local v0    # "currentAni":Landroid/view/animation/Animation;
    :catch_0
    move-exception v1

    .line 3217
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 3241
    .end local v1    # "e":Landroid/os/RemoteException;
    .restart local v0    # "currentAni":Landroid/view/animation/Animation;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;

    const v3, 0x7f02002e

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3242
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setSoundEffectsEnabled(Z)V

    .line 3243
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 3244
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateText:Landroid/widget/TextView;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_2

    .line 3280
    :cond_4
    iput-boolean p2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z

    .line 3281
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissAllDialog()V

    .line 3283
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 3284
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 3287
    :cond_5
    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuidePosition(ZZ)V

    .line 3288
    invoke-direct {p0, v4, v4, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideVisibility(ZZZ)V

    .line 3290
    if-eqz p4, :cond_6

    .line 3291
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 3294
    :cond_6
    new-instance v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;

    invoke-direct {v2, p0, p3, p4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ZZ)V

    invoke-virtual {p1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3322
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v2, p1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0
.end method

.method private setImageToMoveButton(IZ)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "expand"    # Z

    .prologue
    const/4 v2, 0x1

    .line 2444
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-nez v1, :cond_1

    .line 2483
    :cond_0
    :goto_0
    return-void

    .line 2447
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsPressMoveBtn:Z

    .line 2448
    .local v0, "press":Z
    packed-switch p1, :pswitch_data_0

    .line 2480
    :goto_1
    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 2481
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz p2, :cond_8

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_0

    .line 2450
    :pswitch_0
    if-ne p2, v2, :cond_3

    .line 2451
    if-ne v0, v2, :cond_2

    .line 2452
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200eb

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 2454
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200e8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 2457
    :cond_3
    if-ne v0, v2, :cond_4

    .line 2458
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200ea

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 2460
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200e9

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 2465
    :pswitch_1
    if-ne p2, v2, :cond_6

    .line 2466
    if-ne v0, v2, :cond_5

    .line 2467
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f020100

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 2469
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200fd

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 2472
    :cond_6
    if-ne v0, v2, :cond_7

    .line 2473
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200ff

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 2475
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200fe

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 2481
    :cond_8
    const/high16 v1, 0x3f400000    # 0.75f

    goto :goto_2

    .line 2448
    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setOffsetForAnim()V
    .locals 2

    .prologue
    const v1, 0x7f0a001d

    .line 3129
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    packed-switch v0, :pswitch_data_0

    .line 3137
    :goto_0
    return-void

    .line 3131
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOffsetForAnim:I

    goto :goto_0

    .line 3134
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    neg-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOffsetForAnim:I

    goto :goto_0

    .line 3129
    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private showHistoryBarUi(II)V
    .locals 8
    .param p1, "type"    # I
    .param p2, "cnt"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v5, 0x0

    .line 1776
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    if-eqz v6, :cond_1

    .line 1777
    const/4 v0, 0x0

    .line 1778
    .local v0, "gravity":I
    const/4 v3, 0x0

    .local v3, "x":I
    const/4 v4, 0x0

    .line 1779
    .local v4, "y":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v6

    div-int/lit8 v2, v6, 0xa

    .line 1780
    .local v2, "padding":I
    :goto_0
    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    packed-switch v6, :pswitch_data_0

    .line 1792
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    invoke-virtual {v6}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 1793
    .local v1, "l":Landroid/view/WindowManager$LayoutParams;
    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1794
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1795
    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1796
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v7

    invoke-virtual {v6, v5, v5, v5, v7}, Landroid/widget/ScrollView;->setPaddingRelative(IIII)V

    .line 1797
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getVisibility()I

    move-result v5

    if-nez v5, :cond_0

    .line 1798
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1800
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    .line 1801
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationHistoryBarUI()V

    .line 1803
    .end local v0    # "gravity":I
    .end local v1    # "l":Landroid/view/WindowManager$LayoutParams;
    .end local v2    # "padding":I
    .end local v3    # "x":I
    .end local v4    # "y":I
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->cancelCollapseTimer()V

    .line 1804
    return-void

    .restart local v0    # "gravity":I
    .restart local v3    # "x":I
    .restart local v4    # "y":I
    :cond_2
    move v2, v5

    .line 1779
    goto :goto_0

    .line 1782
    .restart local v2    # "padding":I
    :pswitch_0
    const/16 v0, 0x33

    .line 1783
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    invoke-virtual {v6}, Landroid/widget/ScrollView;->getWidth()I

    move-result v6

    add-int v3, v6, v2

    .line 1784
    if-ne p1, v7, :cond_3

    move v4, v5

    .line 1785
    :goto_2
    goto :goto_1

    .line 1784
    :cond_3
    invoke-direct {p0, p2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->findHistoryBarPosisition(I)I

    move-result v4

    goto :goto_2

    .line 1787
    :pswitch_1
    const/16 v0, 0x35

    .line 1788
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    invoke-virtual {v6}, Landroid/widget/ScrollView;->getWidth()I

    move-result v6

    add-int v3, v6, v2

    .line 1789
    if-ne p1, v7, :cond_4

    move v4, v5

    :goto_3
    goto :goto_1

    :cond_4
    invoke-direct {p0, p2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->findHistoryBarPosisition(I)I

    move-result v4

    goto :goto_3

    .line 1780
    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private startCollapseTimer()V
    .locals 4

    .prologue
    const/16 v2, 0xc9

    .line 764
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 765
    .local v0, "msg":Landroid/os/Message;
    iput v2, v0, Landroid/os/Message;->what:I

    .line 766
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 767
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 768
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    if-nez v1, :cond_2

    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v1, :cond_1

    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v1, :cond_2

    .line 769
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 770
    :cond_2
    return-void
.end method

.method private startFlashBarAnimation(ZZ)V
    .locals 3
    .param p1, "bStart"    # Z
    .param p2, "bExpand"    # Z

    .prologue
    const/16 v2, 0x68

    .line 3189
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mStartFlashBar:Z

    .line 3190
    const/4 v0, 0x0

    .line 3191
    .local v0, "ani":Landroid/view/animation/Animation;
    if-eqz p1, :cond_1

    .line 3192
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    if-ne v1, v2, :cond_0

    .line 3193
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04004b

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 3202
    :goto_0
    if-nez p1, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-direct {p0, v0, p2, v1, p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setFlashBarAnimation(Landroid/view/animation/Animation;ZZZ)V

    .line 3203
    return-void

    .line 3195
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040049

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 3197
    :cond_1
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    if-ne v1, v2, :cond_2

    .line 3198
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040033

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 3200
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040031

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 3202
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private updateAppListPosition(IZZ)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "sticky"    # Z
    .param p3, "expand"    # Z

    .prologue
    .line 2762
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListPosition(IZZZ)V

    .line 2763
    return-void
.end method

.method private updateAppListPosition(IZZZ)V
    .locals 27
    .param p1, "position"    # I
    .param p2, "sticky"    # Z
    .param p3, "expand"    # Z
    .param p4, "savePosition"    # Z

    .prologue
    .line 2766
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHasCocktailBar:Z

    move/from16 v23, v0

    if-eqz v23, :cond_1

    .line 2767
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z

    move/from16 v23, v0

    if-nez v23, :cond_0

    .line 2768
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowManager:Landroid/view/WindowManager;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/view/Display;->getRotation()I

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastDegreesForPosition:I

    .line 2770
    :cond_0
    if-eqz p3, :cond_7

    .line 2771
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOrientationListenerForPosition:Landroid/view/OrientationEventListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/OrientationEventListener;->enable()V

    .line 2776
    :cond_1
    :goto_0
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getAppListPosition(I)I

    move-result v5

    .line 2777
    .local v5, "appListPosition":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v14

    .line 2778
    .local v14, "l":Landroid/view/WindowManager$LayoutParams;
    new-instance v4, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-direct {v4, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2779
    .local v4, "appListMarginVertical":Landroid/view/ViewGroup$MarginLayoutParams;
    new-instance v6, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-direct {v6, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2780
    .local v6, "appTrayBGLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    const/16 v16, 0x0

    .line 2781
    .local v16, "moveBtnLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    if-eqz v23, :cond_2

    .line 2782
    new-instance v16, Landroid/view/ViewGroup$MarginLayoutParams;

    .end local v16    # "moveBtnLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2785
    .restart local v16    # "moveBtnLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2787
    .local v10, "editLayout":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a0018

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v17, v0

    .line 2788
    .local v17, "moveBtnWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a0019

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v15, v0

    .line 2789
    .local v15, "moveBtnHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a001d

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v7, v0

    .line 2790
    .local v7, "applistHeight":I
    move v12, v7

    .line 2791
    .local v12, "flashBarHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a0019

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnOverlapMargin:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnShadowMargin:I

    move/from16 v25, v0

    add-int v24, v24, v25

    sub-int v23, v23, v24

    add-int v12, v12, v23

    .line 2794
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a0020

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v19, v0

    .line 2795
    .local v19, "scrollerMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a0053

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v8, v0

    .line 2796
    .local v8, "applistSidePadding":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a0021

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v18, v0

    .line 2797
    .local v18, "scrollerLeftMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a0022

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v20, v0

    .line 2799
    .local v20, "scrollerRightFocusMargin":I
    const/4 v9, 0x0

    .line 2800
    .local v9, "applistTopMargin":I
    const/16 v23, 0x67

    move/from16 v0, v23

    if-ne v5, v0, :cond_8

    const/4 v11, 0x0

    .line 2801
    .local v11, "editMargin":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a0052

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v22

    .line 2803
    .local v22, "topPadding":I
    new-instance v13, Landroid/graphics/Point;

    invoke-direct {v13}, Landroid/graphics/Point;-><init>()V

    .line 2804
    .local v13, "fullscreen":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2805
    iget v0, v13, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    .line 2806
    iget v0, v13, Landroid/graphics/Point;->y:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    .line 2808
    packed-switch v5, :pswitch_data_0

    .line 3001
    :cond_3
    :goto_2
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v5, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setImageToMoveButton(IZ)V

    .line 3002
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    if-eqz v23, :cond_4

    .line 3003
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    new-instance v24, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3005
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    new-instance v24, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v24

    invoke-direct {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3009
    sget-boolean v23, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v23, :cond_5

    if-nez p3, :cond_5

    .line 3010
    iget v0, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    move/from16 v23, v0

    move/from16 v0, v23

    neg-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 3012
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 3013
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowManager:Landroid/view/WindowManager;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-interface {v0, v1, v14}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 3015
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/view/View;->setVisibility(I)V

    .line 3017
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->updateForeground()V

    .line 3018
    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    .line 3019
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z

    .line 3020
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setOffsetForAnim()V

    .line 3021
    const/16 v23, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissHistoryWindow(Z)V

    .line 3022
    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v5, v1, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updatePocketTrayLayout(IZZ)V

    .line 3023
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->setMWTrayOpenState(Z)V

    .line 3025
    if-eqz p4, :cond_6

    .line 3026
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->saveFlashBarCurPosition(I)V

    .line 3028
    :cond_6
    return-void

    .line 2773
    .end local v4    # "appListMarginVertical":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v5    # "appListPosition":I
    .end local v6    # "appTrayBGLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v7    # "applistHeight":I
    .end local v8    # "applistSidePadding":I
    .end local v9    # "applistTopMargin":I
    .end local v10    # "editLayout":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v11    # "editMargin":I
    .end local v12    # "flashBarHeight":I
    .end local v13    # "fullscreen":Landroid/graphics/Point;
    .end local v14    # "l":Landroid/view/WindowManager$LayoutParams;
    .end local v15    # "moveBtnHeight":I
    .end local v16    # "moveBtnLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v17    # "moveBtnWidth":I
    .end local v18    # "scrollerLeftMargin":I
    .end local v19    # "scrollerMargin":I
    .end local v20    # "scrollerRightFocusMargin":I
    .end local v22    # "topPadding":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOrientationListenerForPosition:Landroid/view/OrientationEventListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/OrientationEventListener;->disable()V

    goto/16 :goto_0

    .line 2800
    .restart local v4    # "appListMarginVertical":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v5    # "appListPosition":I
    .restart local v6    # "appTrayBGLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v7    # "applistHeight":I
    .restart local v8    # "applistSidePadding":I
    .restart local v9    # "applistTopMargin":I
    .restart local v10    # "editLayout":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v12    # "flashBarHeight":I
    .restart local v14    # "l":Landroid/view/WindowManager$LayoutParams;
    .restart local v15    # "moveBtnHeight":I
    .restart local v16    # "moveBtnLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v17    # "moveBtnWidth":I
    .restart local v18    # "scrollerLeftMargin":I
    .restart local v19    # "scrollerMargin":I
    .restart local v20    # "scrollerRightFocusMargin":I
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnOverlapMargin:I

    move/from16 v23, v0

    sub-int v11, v15, v23

    goto/16 :goto_1

    .line 2810
    .restart local v11    # "editMargin":I
    .restart local v13    # "fullscreen":Landroid/graphics/Point;
    .restart local v22    # "topPadding":I
    :pswitch_0
    const/16 v23, 0x1

    move/from16 v0, p2

    move/from16 v1, v23

    if-ne v0, v1, :cond_e

    .line 2811
    const/16 v23, 0x1

    move/from16 v0, p3

    move/from16 v1, v23

    if-ne v0, v1, :cond_c

    .line 2812
    const/16 v23, 0x0

    move/from16 v0, v23

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2813
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v23

    move/from16 v0, v23

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2814
    iput v12, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2815
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v24

    sub-int v23, v23, v24

    move/from16 v0, v23

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2816
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    if-eqz v23, :cond_9

    .line 2817
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getAppListHandlePosition()I

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2818
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnOverlapMargin:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnShadowMargin:I

    move/from16 v24, v0

    add-int v23, v23, v24

    sub-int v23, v7, v23

    move/from16 v0, v23

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2819
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    const v24, 0x7f0200e8

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2820
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x7f08001a

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2822
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2823
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2824
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    if-eqz v23, :cond_a

    .line 2825
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2826
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x3

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 2863
    :cond_a
    :goto_3
    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v10, v0, v1, v11, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2864
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2865
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    if-eqz v23, :cond_10

    .line 2866
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2871
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastConfig:Landroid/content/res/Configuration;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v23, v0

    const/16 v24, 0x2

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_11

    .line 2872
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a001f

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v9, v0

    .line 2877
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getGlobalSystemUiVisibility()I

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSystemUiVisibility:I

    .line 2878
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSystemUiVisibility:I

    move/from16 v23, v0

    if-nez v23, :cond_b

    .line 2879
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x1050010

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    .line 2880
    .local v21, "statusbarHeight":I
    add-int v9, v9, v21

    .line 2883
    .end local v21    # "statusbarHeight":I
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    move/from16 v23, v0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getEditAreaHeight()I

    move-result v24

    sub-int v23, v23, v24

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v24

    sub-int v23, v23, v24

    sub-int v23, v23, v9

    move/from16 v0, v23

    iput v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2887
    const/16 v23, 0x0

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v23

    invoke-virtual {v4, v0, v9, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2889
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v23, v0

    new-instance v24, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v24

    invoke-direct {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2890
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v24

    move-object/from16 v0, v23

    move/from16 v1, v22

    move/from16 v2, v24

    invoke-virtual {v0, v8, v1, v8, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 2893
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    const v24, 0x7f0200e6

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 2894
    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2895
    iput v7, v6, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2896
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v24

    sub-int v23, v23, v24

    move/from16 v0, v23

    iput v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2897
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    if-eqz v23, :cond_3

    .line 2898
    move-object/from16 v0, v16

    iput v15, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2899
    move/from16 v0, v17

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    goto/16 :goto_2

    .line 2829
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnOverlapMargin:I

    move/from16 v23, v0

    rsub-int/lit8 v23, v23, 0x0

    move/from16 v0, v23

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2830
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v23

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getAppListHandlePosition()I

    move-result v24

    add-int v23, v23, v24

    move/from16 v0, v23

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2831
    iput v15, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2832
    move/from16 v0, v17

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2833
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    if-eqz v23, :cond_d

    .line 2834
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2835
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2836
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x7f080019

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2838
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v23, v0

    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2839
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2840
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    if-eqz v23, :cond_a

    .line 2841
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2842
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x3

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setGravity(I)V

    goto/16 :goto_3

    .line 2846
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionX:I

    move/from16 v23, v0

    div-int/lit8 v24, v17, 0x2

    sub-int v24, v12, v24

    sub-int v23, v23, v24

    move/from16 v0, v23

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2847
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionY:I

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v24

    add-int v23, v23, v24

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getAppListHandlePosition()I

    move-result v24

    sub-int v23, v23, v24

    div-int/lit8 v24, v17, 0x2

    sub-int v23, v23, v24

    move/from16 v0, v23

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2848
    iput v12, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2849
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v24

    sub-int v23, v23, v24

    move/from16 v0, v23

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2850
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    if-eqz v23, :cond_f

    .line 2851
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getAppListHandlePosition()I

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2852
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnOverlapMargin:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnShadowMargin:I

    move/from16 v24, v0

    add-int v23, v23, v24

    sub-int v23, v7, v23

    move/from16 v0, v23

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2853
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    const v24, 0x7f0200e8

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2855
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2856
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2857
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    if-eqz v23, :cond_a

    .line 2858
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2859
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x3

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setGravity(I)V

    goto/16 :goto_3

    .line 2868
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_4

    .line 2874
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a001e

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v9, v0

    goto/16 :goto_5

    .line 2903
    :pswitch_1
    const/16 v23, 0x1

    move/from16 v0, p2

    move/from16 v1, v23

    if-ne v0, v1, :cond_17

    .line 2904
    const/16 v23, 0x1

    move/from16 v0, p3

    move/from16 v1, v23

    if-ne v0, v1, :cond_15

    .line 2905
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    move/from16 v23, v0

    sub-int v23, v23, v12

    move/from16 v0, v23

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2906
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v23

    move/from16 v0, v23

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2907
    iput v12, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2908
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v24

    sub-int v23, v23, v24

    move/from16 v0, v23

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2909
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    if-eqz v23, :cond_12

    .line 2910
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getAppListHandlePosition()I

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2911
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2912
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    const v24, 0x7f0200fd

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2913
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x7f08001a

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2915
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2916
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2917
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    if-eqz v23, :cond_13

    .line 2918
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2919
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x5

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 2920
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2958
    :cond_13
    :goto_6
    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v10, v11, v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2959
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2960
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    if-eqz v23, :cond_19

    .line 2961
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2966
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastConfig:Landroid/content/res/Configuration;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v23, v0

    const/16 v24, 0x2

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_1a

    .line 2967
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a001f

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v9, v0

    .line 2972
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getGlobalSystemUiVisibility()I

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSystemUiVisibility:I

    .line 2974
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSystemUiVisibility:I

    move/from16 v23, v0

    if-nez v23, :cond_14

    .line 2975
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x1050010

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    .line 2976
    .restart local v21    # "statusbarHeight":I
    add-int v9, v9, v21

    .line 2979
    .end local v21    # "statusbarHeight":I
    :cond_14
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    move/from16 v23, v0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getEditAreaHeight()I

    move-result v24

    sub-int v23, v23, v24

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v24

    sub-int v23, v23, v24

    sub-int v23, v23, v9

    move/from16 v0, v23

    iput v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2983
    add-int v23, v15, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnShadowMargin:I

    move/from16 v24, v0

    sub-int v23, v23, v24

    add-int v23, v23, v20

    const/16 v24, 0x0

    const/16 v25, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v4, v0, v9, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2985
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v23, v0

    new-instance v24, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v24

    invoke-direct {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2986
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v24

    move-object/from16 v0, v23

    move/from16 v1, v22

    move/from16 v2, v24

    invoke-virtual {v0, v8, v1, v8, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 2989
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    const v24, 0x7f0200fb

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 2990
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnOverlapMargin:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnShadowMargin:I

    move/from16 v24, v0

    add-int v23, v23, v24

    sub-int v23, v15, v23

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2991
    iput v7, v6, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2992
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v24

    sub-int v23, v23, v24

    move/from16 v0, v23

    iput v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2993
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    if-eqz v23, :cond_3

    .line 2994
    move-object/from16 v0, v16

    iput v15, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2995
    move/from16 v0, v17

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    goto/16 :goto_2

    .line 2923
    :cond_15
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    move/from16 v23, v0

    sub-int v23, v23, v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnOverlapMargin:I

    move/from16 v24, v0

    add-int v23, v23, v24

    move/from16 v0, v23

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2924
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v23

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getAppListHandlePosition()I

    move-result v24

    add-int v23, v23, v24

    move/from16 v0, v23

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2925
    iput v15, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2926
    move/from16 v0, v17

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2927
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    if-eqz v23, :cond_16

    .line 2928
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2929
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2930
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x7f080019

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2932
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v23, v0

    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2933
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2934
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    if-eqz v23, :cond_13

    .line 2935
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2936
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x5

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setGravity(I)V

    goto/16 :goto_6

    .line 2940
    :cond_17
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionX:I

    move/from16 v23, v0

    div-int/lit8 v24, v15, 0x2

    sub-int v23, v23, v24

    move/from16 v0, v23

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2941
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionY:I

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v24

    add-int v23, v23, v24

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getAppListHandlePosition()I

    move-result v24

    sub-int v23, v23, v24

    div-int/lit8 v24, v17, 0x2

    sub-int v23, v23, v24

    move/from16 v0, v23

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2942
    iput v12, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2943
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v24

    sub-int v23, v23, v24

    move/from16 v0, v23

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2944
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    if-eqz v23, :cond_18

    .line 2945
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getAppListHandlePosition()I

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2946
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2947
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    const v24, 0x7f0200fd

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2949
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2950
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2951
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    if-eqz v23, :cond_13

    .line 2952
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2953
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x5

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 2954
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_6

    .line 2963
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_7

    .line 2969
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a001e

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v9, v0

    goto/16 :goto_8

    .line 2808
    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updatePocketTrayButtonText()V
    .locals 3

    .prologue
    .line 3031
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNeedUpdatePocketTrayButtonText:Z

    if-nez v0, :cond_1

    .line 3047
    :cond_0
    :goto_0
    return-void

    .line 3035
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 3040
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    if-eqz v0, :cond_2

    const v0, 0x7f080002

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3041
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080003

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3042
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080004

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3044
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->invalidate()V

    .line 3045
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->invalidate()V

    .line 3046
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->invalidate()V

    goto :goto_0

    .line 3040
    :cond_2
    const v0, 0x7f080001

    goto :goto_1
.end method

.method private updatePocketTrayLayout(IZZ)V
    .locals 13
    .param p1, "position"    # I
    .param p2, "isStarting"    # Z
    .param p3, "forceClose"    # Z

    .prologue
    .line 3051
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    if-nez v10, :cond_0

    .line 3126
    :goto_0
    return-void

    .line 3055
    :cond_0
    new-instance v6, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v10}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    invoke-direct {v6, v10}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3056
    .local v6, "pocketTrayLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    invoke-virtual {v10}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 3058
    .local v7, "scrollLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v11, 0x7f0a0019

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v1, v10

    .line 3059
    .local v1, "moveBtnHeight":I
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v11, 0x7f0a006b

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v2, v10

    .line 3060
    .local v2, "openButtonHeight":I
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v11, 0x7f0a0068

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v3, v10

    .line 3061
    .local v3, "openPocketTrayHeght":I
    iget v8, v7, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 3062
    .local v8, "scrollViewHeight":I
    move v4, v8

    .line 3064
    .local v4, "pocketTopMargin":I
    iget v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHelphubVersion:I

    const/4 v11, 0x3

    if-ne v10, v11, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-static {}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isUsaFeature()Z

    move-result v10

    if-nez v10, :cond_2

    :cond_1
    iget v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHelphubVersion:I

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v10}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isKnoxMode()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 3065
    :cond_2
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v11, 0x7f0a0069

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v5, v10

    .line 3066
    .local v5, "pocketTrayHelpButtonHeight":I
    sub-int/2addr v3, v5

    .line 3069
    .end local v5    # "pocketTrayHelpButtonHeight":I
    :cond_3
    if-eqz p3, :cond_5

    .line 3070
    iget v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v11

    add-int/2addr v11, v2

    sub-int v4, v10, v11

    .line 3071
    move v8, v4

    .line 3072
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    const v11, 0x7f02003a

    invoke-virtual {v10, v11}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 3073
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOpenPocketTray:Z

    .line 3093
    :goto_1
    packed-switch p1, :pswitch_data_0

    .line 3104
    :goto_2
    const/4 v0, 0x0

    .line 3105
    .local v0, "applistTopMargin":I
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget v10, v10, Landroid/content/res/Configuration;->orientation:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_9

    .line 3106
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v11, 0x7f0a001f

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v0, v10

    .line 3111
    :goto_3
    iget v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSystemUiVisibility:I

    if-nez v10, :cond_4

    .line 3112
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v11, 0x1050010

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 3113
    .local v9, "statusbarHeight":I
    add-int/2addr v0, v9

    .line 3116
    .end local v9    # "statusbarHeight":I
    :cond_4
    sub-int v10, v8, v0

    iput v10, v7, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 3117
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    new-instance v11, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v11, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v10, v11}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3118
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    new-instance v11, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v11, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3120
    if-nez p2, :cond_a

    iget-boolean v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOpenPocketTray:Z

    if-nez v10, :cond_a

    .line 3121
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTrayBody:Landroid/widget/RelativeLayout;

    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 3075
    .end local v0    # "applistTopMargin":I
    :cond_5
    iget-boolean v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOpenPocketTray:Z

    if-eqz v10, :cond_7

    .line 3076
    iget v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    add-int v11, v2, v3

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v12

    add-int/2addr v11, v12

    sub-int v4, v10, v11

    .line 3077
    if-eqz p2, :cond_6

    .line 3078
    add-int v8, v4, v3

    .line 3082
    :goto_4
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    const v11, 0x7f02003b

    invoke-virtual {v10, v11}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_1

    .line 3080
    :cond_6
    move v8, v4

    goto :goto_4

    .line 3084
    :cond_7
    iget v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v11

    add-int/2addr v11, v2

    sub-int v8, v10, v11

    .line 3085
    if-eqz p2, :cond_8

    .line 3086
    sub-int v4, v8, v3

    .line 3090
    :goto_5
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    const v11, 0x7f02003a

    invoke-virtual {v10, v11}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_1

    .line 3088
    :cond_8
    move v4, v8

    goto :goto_5

    .line 3095
    :pswitch_0
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    const v11, 0x7f020105

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 3096
    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v6, v10, v4, v11, v12}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto/16 :goto_2

    .line 3099
    :pswitch_1
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    const v11, 0x7f020106

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 3100
    iget v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnOverlapMargin:I

    iget v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtnShadowMargin:I

    add-int/2addr v10, v11

    sub-int v10, v1, v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v6, v10, v4, v11, v12}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto/16 :goto_2

    .line 3108
    .restart local v0    # "applistTopMargin":I
    :cond_9
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v11, 0x7f0a001e

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v0, v10

    goto/16 :goto_3

    .line 3123
    :cond_a
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updatePocketTrayButtonText()V

    .line 3124
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTrayBody:Landroid/widget/RelativeLayout;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 3093
    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateScrollPositionTemplate()V
    .locals 2

    .prologue
    .line 5522
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    new-instance v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$89;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$89;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 5532
    return-void
.end method

.method private updateSplitGuidePosition(ZZ)V
    .locals 10
    .param p1, "expand"    # Z
    .param p2, "requestPreview"    # Z

    .prologue
    .line 2486
    sget-boolean v5, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v5, :cond_0

    sget-boolean v5, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v5, :cond_1

    .line 2530
    :cond_0
    :goto_0
    return-void

    .line 2489
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_0

    .line 2493
    if-eqz p1, :cond_2

    .line 2494
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 2495
    .local v1, "l":Landroid/view/WindowManager$LayoutParams;
    iget v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    rsub-int/lit8 v5, v5, 0x0

    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2496
    iget v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    rsub-int/lit8 v5, v5, 0x0

    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2497
    iget v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2498
    iget v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2499
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v5, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 2500
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v6}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    invoke-interface {v5, v6, v1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2501
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2503
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    const/16 v6, 0xc

    invoke-virtual {v5, v6}, Lcom/sec/android/app/FlashBarService/AppListController;->getRectByZone(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 2504
    .local v0, "bottomRect":Landroid/graphics/Rect;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 2505
    .local v3, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v5

    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    iput v5, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2506
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v5

    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    iput v5, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2507
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v3}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2508
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2509
    .local v2, "lp":Landroid/widget/FrameLayout$LayoutParams;
    iget v5, v0, Landroid/graphics/Rect;->left:I

    iget v6, v0, Landroid/graphics/Rect;->top:I

    iget v7, v0, Landroid/graphics/Rect;->right:I

    iget v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    mul-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    iget v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    mul-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    invoke-virtual {v2, v5, v6, v7, v8}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2512
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2514
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Lcom/sec/android/app/FlashBarService/AppListController;->getRectByZone(I)Landroid/graphics/Rect;

    move-result-object v4

    .line 2515
    .local v4, "topRect":Landroid/graphics/Rect;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 2516
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v5

    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    iput v5, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2517
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v5

    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    iput v5, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2518
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v3}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2519
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    .end local v2    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    invoke-direct {v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2520
    .restart local v2    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    iget v5, v4, Landroid/graphics/Rect;->left:I

    iget v6, v4, Landroid/graphics/Rect;->top:I

    iget v7, v4, Landroid/graphics/Rect;->right:I

    iget v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    mul-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    iget v8, v4, Landroid/graphics/Rect;->bottom:I

    iget v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I

    mul-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    invoke-virtual {v2, v5, v6, v7, v8}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2523
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2525
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideTextPadding()V

    .line 2528
    .end local v0    # "bottomRect":Landroid/graphics/Rect;
    .end local v1    # "l":Landroid/view/WindowManager$LayoutParams;
    .end local v2    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    .end local v3    # "params":Landroid/view/ViewGroup$LayoutParams;
    .end local v4    # "topRect":Landroid/graphics/Rect;
    :cond_2
    if-eqz p2, :cond_0

    .line 2529
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v5, p1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->requestSplitPreview(Z)V

    goto/16 :goto_0
.end method

.method private updateSplitGuideTextPadding()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const v5, 0x7f0f0092

    const v4, 0x7f0f008f

    const/4 v3, 0x0

    .line 2688
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0a001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v0, v1

    .line 2689
    .local v0, "applistHeight":I
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    const/16 v2, 0x67

    if-ne v1, v2, :cond_2

    .line 2690
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v6, :cond_1

    .line 2691
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 2692
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 2706
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->enableMultiSelection(Z)V

    .line 2707
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->enableMultiSelection(Z)V

    .line 2708
    return-void

    .line 2694
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 2695
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    .line 2697
    :cond_2
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    const/16 v2, 0x68

    if-ne v1, v2, :cond_0

    .line 2698
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v6, :cond_3

    .line 2699
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3, v3, v0, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 2700
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3, v3, v0, v3}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    .line 2702
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 2703
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3, v3, v0, v3}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method

.method private updateSplitGuideVisibility(ZZZ)V
    .locals 11
    .param p1, "visible"    # Z
    .param p2, "focusVisible"    # Z
    .param p3, "animating"    # Z

    .prologue
    .line 2533
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    if-nez v9, :cond_1

    .line 2685
    :cond_0
    :goto_0
    return-void

    .line 2537
    :cond_1
    sget-boolean v9, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v9, :cond_2

    sget-boolean v9, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v9, :cond_3

    .line 2538
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2539
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    .line 2543
    :cond_3
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v8

    .line 2544
    .local v8, "topVisibility":I
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v3

    .line 2546
    .local v3, "bottomVisibility":I
    if-eqz p1, :cond_f

    if-eqz p2, :cond_f

    const/4 v9, 0x1

    :goto_1
    iput-boolean v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFocusVisible:Z

    .line 2548
    if-eqz p1, :cond_16

    .line 2549
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2550
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2553
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getFrontActivityMultiWindowStyle(I)Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v5

    .line 2555
    .local v5, "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v9

    if-nez v9, :cond_6

    .line 2556
    :cond_4
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2557
    if-eqz v5, :cond_5

    const/4 v9, 0x2

    invoke-virtual {v5, v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v9

    if-eqz v9, :cond_5

    const/16 v9, 0x1000

    invoke-virtual {v5, v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v9

    if-nez v9, :cond_5

    const/high16 v9, 0x200000

    invoke-virtual {v5, v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 2560
    :cond_5
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2565
    :cond_6
    if-eqz v5, :cond_7

    const/4 v9, 0x2

    invoke-virtual {v5, v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v9

    if-eqz v9, :cond_7

    const/16 v9, 0x1000

    invoke-virtual {v5, v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v9

    if-nez v9, :cond_7

    invoke-virtual {v5}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v9

    if-nez v9, :cond_7

    const/high16 v9, 0x200000

    invoke-virtual {v5, v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v9

    if-nez v9, :cond_7

    .line 2569
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget v9, v9, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    const/4 v10, 0x3

    if-ne v9, v10, :cond_10

    .line 2570
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2571
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2581
    :cond_7
    :goto_2
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    if-eqz v9, :cond_8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuideOpenAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v9, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v9

    if-eqz v9, :cond_8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v9

    if-nez v9, :cond_8

    .line 2585
    const/4 v8, 0x4

    .line 2586
    const/4 v3, 0x4

    .line 2589
    :cond_8
    if-eqz p3, :cond_c

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    if-eqz v9, :cond_9

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuideOpenAnimation:Landroid/view/animation/Animation;

    if-eq v9, v10, :cond_c

    :cond_9
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v9

    if-eq v8, v9, :cond_a

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v9

    if-eqz v9, :cond_b

    :cond_a
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v9

    if-eq v3, v9, :cond_c

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v9

    if-nez v9, :cond_c

    .line 2596
    :cond_b
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuideOpenAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v9}, Landroid/view/animation/Animation;->reset()V

    .line 2597
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuideOpenAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v9, v10}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2600
    :cond_c
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    const v10, 0x7f0f0090

    invoke-virtual {v9, v10}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 2601
    .local v6, "topFocus":Landroid/view/View;
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    const v10, 0x7f0f0093

    invoke-virtual {v9, v10}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2602
    .local v1, "bottomFocus":Landroid/view/View;
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v10, 0x7f040042

    invoke-static {v9, v10}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 2604
    .local v0, "a":Landroid/view/animation/Animation;
    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v7

    .line 2605
    .local v7, "topFocusVisibility":I
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    .line 2606
    .local v2, "bottomFocusVisibility":I
    const/4 v9, 0x4

    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    .line 2607
    const/4 v9, 0x4

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    .line 2609
    if-eqz p2, :cond_14

    .line 2610
    if-eqz v5, :cond_e

    .line 2611
    const/4 v4, 0x0

    .line 2612
    .local v4, "focusedZone":I
    const/4 v9, 0x2

    invoke-virtual {v5, v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v9

    if-eqz v9, :cond_d

    const/16 v9, 0x1000

    invoke-virtual {v5, v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v9

    if-nez v9, :cond_d

    const/high16 v9, 0x200000

    invoke-virtual {v5, v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 2615
    :cond_d
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget v4, v9, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    .line 2620
    :goto_3
    const/4 v9, 0x3

    if-ne v4, v9, :cond_13

    .line 2621
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    .line 2622
    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v9

    if-eq v7, v9, :cond_e

    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v9

    if-nez v9, :cond_e

    invoke-virtual {v6}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    if-nez v9, :cond_e

    .line 2625
    invoke-virtual {v6, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2637
    .end local v4    # "focusedZone":I
    :cond_e
    :goto_4
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v9

    if-eqz v9, :cond_0

    .line 2639
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionGuideline:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2546
    .end local v0    # "a":Landroid/view/animation/Animation;
    .end local v1    # "bottomFocus":Landroid/view/View;
    .end local v2    # "bottomFocusVisibility":I
    .end local v5    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    .end local v6    # "topFocus":Landroid/view/View;
    .end local v7    # "topFocusVisibility":I
    :cond_f
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 2572
    .restart local v5    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_10
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget v9, v9, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    const/16 v10, 0xc

    if-ne v9, v10, :cond_7

    .line 2573
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2574
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 2617
    .restart local v0    # "a":Landroid/view/animation/Animation;
    .restart local v1    # "bottomFocus":Landroid/view/View;
    .restart local v2    # "bottomFocusVisibility":I
    .restart local v4    # "focusedZone":I
    .restart local v6    # "topFocus":Landroid/view/View;
    .restart local v7    # "topFocusVisibility":I
    :cond_11
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget v9, v9, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    const/4 v10, 0x3

    if-ne v9, v10, :cond_12

    const/16 v4, 0xc

    :goto_5
    goto :goto_3

    :cond_12
    const/4 v4, 0x3

    goto :goto_5

    .line 2627
    :cond_13
    const/16 v9, 0xc

    if-ne v4, v9, :cond_e

    .line 2628
    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    .line 2629
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v9

    if-eq v2, v9, :cond_e

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v9

    if-nez v9, :cond_e

    invoke-virtual {v1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    if-nez v9, :cond_e

    .line 2632
    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_4

    .line 2642
    .end local v4    # "focusedZone":I
    :cond_14
    invoke-virtual {v6}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    if-eqz v9, :cond_15

    .line 2643
    invoke-virtual {v6}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2644
    invoke-virtual {v6}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/animation/Animation;->cancel()V

    .line 2645
    invoke-virtual {v6}, Landroid/view/View;->clearAnimation()V

    .line 2647
    :cond_15
    invoke-virtual {v1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 2648
    invoke-virtual {v1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2649
    invoke-virtual {v1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/animation/Animation;->cancel()V

    .line 2650
    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    goto/16 :goto_0

    .line 2654
    .end local v0    # "a":Landroid/view/animation/Animation;
    .end local v1    # "bottomFocus":Landroid/view/View;
    .end local v2    # "bottomFocusVisibility":I
    .end local v5    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    .end local v6    # "topFocus":Landroid/view/View;
    .end local v7    # "topFocusVisibility":I
    :cond_16
    if-eqz p3, :cond_19

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v9

    if-eqz v9, :cond_17

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v9

    if-nez v9, :cond_19

    .line 2657
    :cond_17
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    if-eqz v9, :cond_18

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuideOpenAnimation:Landroid/view/animation/Animation;

    if-ne v9, v10, :cond_0

    .line 2659
    :cond_18
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v10, 0x7f040041

    invoke-static {v9, v10}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 2660
    .restart local v0    # "a":Landroid/view/animation/Animation;
    new-instance v9, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$33;

    invoke-direct {v9, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$33;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v0, v9}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2672
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v9, v0}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 2675
    .end local v0    # "a":Landroid/view/animation/Animation;
    :cond_19
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2676
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2677
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->hideGuidelineWindow()V

    .line 2678
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 2679
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2680
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/animation/Animation;->cancel()V

    .line 2681
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->clearAnimation()V

    goto/16 :goto_0
.end method

.method private updateViewPagerLayout()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 1262
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerAdapter:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;

    invoke-virtual {v4, v5}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 1263
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPager:Landroid/support/v4/view/ViewPager;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerSelectedPage:I

    invoke-virtual {v4, v5}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1264
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerWrapper:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1265
    .local v2, "params":Landroid/view/ViewGroup$LayoutParams;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMark:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout$LayoutParams;

    .line 1266
    .local v3, "viewPagerMarkParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerShadow:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 1267
    .local v1, "lp":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v8, :cond_0

    .line 1268
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerShadow:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1269
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerShadowLand:Landroid/widget/ImageView;

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1270
    iput v6, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1271
    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    div-int/lit8 v4, v4, 0x2

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1272
    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkBottomMarginPort:I

    iput v4, v3, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 1273
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/16 v5, 0x50

    invoke-virtual {v4, v5}, Landroid/view/Window;->setGravity(I)V

    .line 1287
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerWrapper:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1288
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerShadow:Landroid/widget/ImageView;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1289
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMark:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1290
    return-void

    .line 1275
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerShadow:Landroid/widget/ImageView;

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1276
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerShadowLand:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1277
    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    div-int/lit8 v4, v4, 0x2

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1278
    iput v6, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1279
    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkBottomMarginLand:I

    iput v4, v3, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 1280
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Landroid/view/Window;->setGravity(I)V

    .line 1282
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Lcom/android/internal/statusbar/IStatusBarService;->setMultiWindowBg(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1283
    :catch_0
    move-exception v0

    .line 1284
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public animationRecentIconByHomeKey()V
    .locals 0

    .prologue
    .line 4127
    return-void
.end method

.method public canCloseWindow()Z
    .locals 1

    .prologue
    .line 5663
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mUnableDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mUnableDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5664
    const/4 v0, 0x0

    .line 5666
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cancelAllMessages()V
    .locals 8

    .prologue
    const/16 v7, 0xcd

    const/16 v6, 0xcc

    const/16 v5, 0xcb

    const/16 v4, 0xca

    const/16 v3, 0xc9

    .line 5468
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5469
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 5471
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 5472
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 5474
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5475
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 5477
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 5478
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 5480
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v3, 0x12c

    invoke-virtual {v2, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 5481
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v3, 0x12c

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 5483
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 5484
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 5486
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v3, 0xce

    invoke-virtual {v2, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 5487
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v3, 0xce

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 5489
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastPostAnimationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5490
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 5492
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 5493
    sget-boolean v2, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v2, :cond_7

    .line 5494
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mRecentsAppStartReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_7

    .line 5495
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mRecentsAppStartReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 5498
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 5502
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mProcessObserver:Landroid/app/IProcessObserver;

    if-eqz v2, :cond_8

    .line 5503
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    .line 5505
    .local v0, "am":Landroid/app/IActivityManager;
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mProcessObserver:Landroid/app/IProcessObserver;

    invoke-interface {v0, v2}, Landroid/app/IActivityManager;->unregisterProcessObserver(Landroid/app/IProcessObserver;)V

    .line 5506
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mProcessObserver:Landroid/app/IProcessObserver;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 5512
    .end local v0    # "am":Landroid/app/IActivityManager;
    :cond_8
    :goto_1
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mUserStoppedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 5515
    :goto_2
    return-void

    .line 5507
    .restart local v0    # "am":Landroid/app/IActivityManager;
    :catch_0
    move-exception v1

    .line 5508
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "LegacyAppListWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed to unregisterProcessObserver, e = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 5513
    .end local v0    # "am":Landroid/app/IActivityManager;
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v2

    goto :goto_2

    .line 5499
    :catch_2
    move-exception v2

    goto :goto_0
.end method

.method public checkAppListLayout()Z
    .locals 2

    .prologue
    .line 5380
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppCnt()I

    move-result v0

    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getCurrentAppList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 5381
    const/4 v0, 0x1

    .line 5382
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkCreateTemplateEnable()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 4729
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v6, 0x10e00b6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    add-int/lit8 v1, v5, 0x1

    .line 4730
    .local v1, "maxTask":I
    if-gtz v1, :cond_0

    .line 4731
    const/4 v1, 0x1

    .line 4733
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/16 v6, 0x64

    const/4 v7, 0x3

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v2

    .line 4734
    .local v2, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 4735
    .local v3, "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v5, v3, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    if-eqz v5, :cond_1

    iget-object v5, v3, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v5}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getType()I

    move-result v5

    if-ne v5, v4, :cond_1

    iget-object v5, v3, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v5}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v5

    if-eqz v5, :cond_1

    .line 4741
    .end local v3    # "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    :goto_0
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public closePocketTray()V
    .locals 4

    .prologue
    .line 4634
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v0, :cond_0

    .line 4635
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZI)V

    .line 4637
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarOpenListener:Landroid/view/View$OnClickListener;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTrayHeader:Landroid/widget/RelativeLayout;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 4638
    return-void
.end method

.method public createAppListOverlayHelp()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1024
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "do_not_show_app_list_help"

    const/4 v4, -0x2

    invoke-static {v2, v3, v6, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 1055
    :cond_0
    :goto_0
    return-void

    .line 1027
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const-string v3, "keyguard"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 1028
    .local v0, "km":Landroid/app/KeyguardManager;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1032
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->cancelCollapseTimer()V

    .line 1033
    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    const/16 v3, 0x68

    if-ne v2, v3, :cond_4

    .line 1034
    new-instance v2, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    const v5, 0x7f03001c

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;-><init>(Landroid/content/Context;Landroid/os/IBinder;I)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    .line 1038
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    const v3, 0x7f0f00c1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 1039
    .local v1, "ok_btn":Landroid/widget/Button;
    new-instance v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$15;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$15;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1049
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    invoke-virtual {v2, v6, v6}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 1050
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListOverlayHelpLocation()V

    .line 1051
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyleLaunch(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1052
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    const v3, 0x7f0f00c0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1054
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->show()V

    goto :goto_0

    .line 1036
    .end local v1    # "ok_btn":Landroid/widget/Button;
    :cond_4
    new-instance v2, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    const v5, 0x7f03001b

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;-><init>(Landroid/content/Context;Landroid/os/IBinder;I)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    goto :goto_1
.end method

.method public createConfirmDialog(IZ)V
    .locals 11
    .param p1, "index"    # I
    .param p2, "bFromEdit"    # Z

    .prologue
    const v10, 0x7f080015

    const/4 v6, 0x1

    .line 3694
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbSynchronizeConfirmDialog:Z

    if-eqz v3, :cond_0

    .line 3734
    :goto_0
    return-void

    .line 3696
    :cond_0
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbSynchronizeConfirmDialog:Z

    .line 3697
    iput-boolean p2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbFromEdit:Z

    .line 3698
    iput p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDelIndex:I

    .line 3699
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080017

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDelIndex:I

    invoke-virtual {v8, v9, p2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppTitle(IZ)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$49;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$49;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080012

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$48;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$48;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$47;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$47;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 3727
    .local v0, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    .line 3728
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 3729
    .local v2, "w":Landroid/view/Window;
    const/16 v3, 0x300

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 3730
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 3731
    .local v1, "p":Landroid/view/WindowManager$LayoutParams;
    const/16 v3, 0x3e8

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 3732
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    iput-object v3, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 3733
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0
.end method

.method public createGestureHelp()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    .line 1111
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "do_not_show_gesture_help"

    const/4 v12, -0x2

    invoke-static {v10, v11, v14, v12}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_1

    .line 1177
    :cond_0
    :goto_0
    return-void

    .line 1114
    :cond_1
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    if-nez v10, :cond_0

    .line 1117
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v10}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 1120
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const-string v11, "keyguard"

    invoke-virtual {v10, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/KeyguardManager;

    .line 1121
    .local v4, "km":Landroid/app/KeyguardManager;
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v10

    if-nez v10, :cond_0

    .line 1124
    :cond_2
    new-instance v10, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v12}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v12

    const v13, 0x7f03001d

    invoke-direct {v10, v11, v12, v13}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;-><init>(Landroid/content/Context;Landroid/os/IBinder;I)V

    iput-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    .line 1125
    iget-boolean v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHasCocktailBar:Z

    if-eqz v10, :cond_3

    .line 1126
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    const v11, 0x7f0f00c5

    invoke-virtual {v10, v11}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 1127
    .local v6, "leftCorner":Landroid/widget/ImageView;
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    const v11, 0x7f0f00c6

    invoke-virtual {v10, v11}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 1128
    .local v8, "rightCorner":Landroid/widget/ImageView;
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    const v11, 0x7f0f00c7

    invoke-virtual {v10, v11}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1129
    .local v3, "gestureTextView":Landroid/widget/TextView;
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v11, 0x7f08005d

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1130
    .local v2, "gestureText":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v10}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 1131
    .local v1, "degrees":I
    packed-switch v1, :pswitch_data_0

    .line 1145
    .end local v1    # "degrees":I
    .end local v2    # "gestureText":Ljava/lang/String;
    .end local v3    # "gestureTextView":Landroid/widget/TextView;
    .end local v6    # "leftCorner":Landroid/widget/ImageView;
    .end local v8    # "rightCorner":Landroid/widget/ImageView;
    :cond_3
    :goto_1
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    const v11, 0x7f0f00c8

    invoke-virtual {v10, v11}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 1147
    .local v5, "learnMore_btn":Landroid/widget/Button;
    invoke-virtual {v5}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1148
    .local v0, "btnStr":Ljava/lang/CharSequence;
    new-instance v9, Landroid/text/SpannableString;

    invoke-direct {v9, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1149
    .local v9, "underlineStr":Landroid/text/SpannableString;
    new-instance v10, Landroid/text/style/UnderlineSpan;

    invoke-direct {v10}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {v9}, Landroid/text/SpannableString;->length()I

    move-result v11

    invoke-virtual {v9, v10, v14, v11, v14}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1150
    invoke-virtual {v5, v9}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1152
    new-instance v10, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$17;

    invoke-direct {v10, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$17;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v5, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1167
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    const v11, 0x7f0f00c1

    invoke-virtual {v10, v11}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    .line 1168
    .local v7, "ok_btn":Landroid/widget/Button;
    new-instance v10, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$18;

    invoke-direct {v10, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$18;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v7, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1176
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    invoke-virtual {v10}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->show()V

    goto/16 :goto_0

    .line 1133
    .end local v0    # "btnStr":Ljava/lang/CharSequence;
    .end local v5    # "learnMore_btn":Landroid/widget/Button;
    .end local v7    # "ok_btn":Landroid/widget/Button;
    .end local v9    # "underlineStr":Landroid/text/SpannableString;
    .restart local v1    # "degrees":I
    .restart local v2    # "gestureText":Ljava/lang/String;
    .restart local v3    # "gestureTextView":Landroid/widget/TextView;
    .restart local v6    # "leftCorner":Landroid/widget/ImageView;
    .restart local v8    # "rightCorner":Landroid/widget/ImageView;
    :pswitch_0
    const/16 v10, 0x8

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1134
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1137
    :pswitch_1
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    goto/16 :goto_0

    .line 1131
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public createResetConfirmDialog()V
    .locals 6

    .prologue
    .line 3737
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbSynchronizeConfirmDialog:Z

    if-eqz v3, :cond_0

    .line 3777
    :goto_0
    return-void

    .line 3739
    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbSynchronizeConfirmDialog:Z

    .line 3740
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f08004a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f08004b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080011

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$51;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$51;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080012

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$50;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$50;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 3758
    .local v0, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    .line 3759
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 3760
    .local v2, "w":Landroid/view/Window;
    const/16 v3, 0x300

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 3761
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 3762
    .local v1, "p":Landroid/view/WindowManager$LayoutParams;
    const/16 v3, 0x3e8

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 3763
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    iput-object v3, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 3764
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 3765
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    new-instance v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$52;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$52;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    goto :goto_0
.end method

.method public createTemplateDialog(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 8
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "defaultTitle"    # Ljava/lang/CharSequence;

    .prologue
    .line 3581
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 3582
    .local v2, "layoutInflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030010

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 3583
    .local v0, "createDialogView":Landroid/view/View;
    const v5, 0x7f0f0085

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditTextView:Landroid/widget/EditText;

    .line 3584
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v5, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 3585
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v5, p2}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    .line 3586
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->selectAll()V

    .line 3587
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditTextView:Landroid/widget/EditText;

    const/16 v6, 0x4000

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setInputType(I)V

    .line 3588
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->requestFocus()Z

    .line 3589
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditTextView:Landroid/widget/EditText;

    const-string v6, "disablePrediction=true;disableEmoticonInput=true;disableVoiceInput=true;noMicrophoneKey=true;inputType=DisableOCR"

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 3590
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditTextView:Landroid/widget/EditText;

    new-instance v6, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$44;

    invoke-direct {v6, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$44;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 3619
    new-instance v5, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v7, 0x7f080013

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v7, 0x7f080011

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;

    invoke-direct {v7, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v7, 0x7f080012

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$45;

    invoke-direct {v7, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$45;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 3669
    .local v1, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    .line 3670
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 3671
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    .line 3672
    .local v4, "w":Landroid/view/Window;
    const v5, 0x10200

    invoke-virtual {v4, v5}, Landroid/view/Window;->addFlags(I)V

    .line 3675
    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 3676
    .local v3, "p":Landroid/view/WindowManager$LayoutParams;
    const/16 v5, 0x7d8

    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 3677
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v5

    iput-object v5, v3, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 3678
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    .line 3679
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->closeFlashBar()V

    .line 3680
    const/16 v5, 0x15

    invoke-virtual {v4, v5}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 3681
    return-void
.end method

.method public createUnableDialog(Ljava/lang/CharSequence;)V
    .locals 6
    .param p1, "message"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v4, 0x0

    .line 3806
    sget-boolean v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v3, :cond_0

    .line 3807
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-static {v3, p1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 3827
    :goto_0
    return-void

    .line 3810
    :cond_0
    invoke-virtual {p0, v4, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateFlashBarState(ZZ)Z

    .line 3811
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080013

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080011

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$55;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$55;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 3820
    .local v0, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mUnableDialog:Landroid/app/AlertDialog;

    .line 3821
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mUnableDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 3822
    .local v2, "w":Landroid/view/Window;
    const/16 v3, 0x300

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 3823
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 3824
    .local v1, "p":Landroid/view/WindowManager$LayoutParams;
    const/16 v3, 0x3e8

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 3825
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    iput-object v3, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 3826
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mUnableDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method public createViewPagerHelp()V
    .locals 10

    .prologue
    .line 1077
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "do_not_show_view_pager_help"

    const/4 v8, 0x0

    const/4 v9, -0x2

    invoke-static {v6, v7, v8, v9}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_1

    .line 1108
    :cond_0
    :goto_0
    return-void

    .line 1080
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const-string v7, "keyguard"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    .line 1081
    .local v2, "km":Landroid/app/KeyguardManager;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1085
    :cond_2
    new-instance v6, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v8

    const v9, 0x7f03001e

    invoke-direct {v6, v7, v8, v9}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;-><init>(Landroid/content/Context;Landroid/os/IBinder;I)V

    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    .line 1086
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    const v7, 0x7f0f00cb

    invoke-virtual {v6, v7}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1087
    .local v0, "helpLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1088
    .local v1, "hlp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->isPortrait()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1089
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerWrapper:Landroid/widget/FrameLayout;

    invoke-virtual {v6}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 1090
    .local v4, "params":Landroid/view/ViewGroup$LayoutParams;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v6}, Landroid/support/v4/view/ViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1091
    .local v5, "viewPagerParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget v6, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    iget v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerTopPaddingPort:I

    mul-int/lit8 v8, v8, 0x4

    div-int/lit8 v8, v8, 0x3

    add-int/2addr v7, v8

    sub-int/2addr v6, v7

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1095
    .end local v4    # "params":Landroid/view/ViewGroup$LayoutParams;
    .end local v5    # "viewPagerParams":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1096
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    const v7, 0x7f0f00c1

    invoke-virtual {v6, v7}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 1097
    .local v3, "ok_btn":Landroid/widget/Button;
    new-instance v6, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$16;

    invoke-direct {v6, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$16;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1107
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->show()V

    goto :goto_0

    .line 1093
    .end local v3    # "ok_btn":Landroid/widget/Button;
    :cond_3
    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerTopPaddingLand:I

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto :goto_1
.end method

.method public createViewPagerTray()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 910
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 912
    .local v0, "layoutInflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissViewPagerAppList()V

    .line 913
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerTrayShow:Z

    .line 914
    const v3, 0x7f03000a

    invoke-virtual {v0, v3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerView:Landroid/view/View;

    .line 915
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerView:Landroid/view/View;

    const v4, 0x7f0f0063

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/ViewPager;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPager:Landroid/support/v4/view/ViewPager;

    .line 916
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerView:Landroid/view/View;

    const v4, 0x7f0f0060

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerWrapper:Landroid/widget/FrameLayout;

    .line 917
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerView:Landroid/view/View;

    const v4, 0x7f0f0061

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerShadow:Landroid/widget/ImageView;

    .line 918
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerView:Landroid/view/View;

    const v4, 0x7f0f0062

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerShadowLand:Landroid/widget/ImageView;

    .line 919
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerView:Landroid/view/View;

    const v4, 0x7f0f0064

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMark:Landroid/widget/LinearLayout;

    .line 921
    new-instance v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {v3, p0, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerAdapter:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;

    .line 922
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerAdapter:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 923
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 924
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMark:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 926
    new-instance v3, Landroid/app/Dialog;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerDialog:Landroid/app/Dialog;

    .line 927
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerDialog:Landroid/app/Dialog;

    invoke-virtual {v3, v6}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 928
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerDialog:Landroid/app/Dialog;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerView:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 929
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerDialog:Landroid/app/Dialog;

    invoke-virtual {v3, v6}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 930
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerDialog:Landroid/app/Dialog;

    new-instance v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$11;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$11;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 941
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->initViewPageMark()V

    .line 943
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 944
    .local v2, "w":Landroid/view/Window;
    invoke-virtual {v2, v8}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 945
    const/4 v3, -0x3

    invoke-virtual {v2, v3}, Landroid/view/Window;->setFormat(I)V

    .line 946
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/Window;->setDimAmount(F)V

    .line 947
    const/16 v3, 0x100

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 949
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 950
    .local v1, "p":Landroid/view/WindowManager$LayoutParams;
    const/16 v3, 0x3e8

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 951
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    iput-object v3, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 952
    iput v7, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 953
    iput v7, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 956
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateViewPagerLayout()V

    .line 958
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerWrapper:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 959
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v5}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 961
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$12;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$12;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 980
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$13;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$13;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v3, v5, v4}, Landroid/support/v4/view/ViewPager;->setPageTransformer(ZLandroid/support/v4/view/ViewPager$PageTransformer;)V

    .line 990
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerView:Landroid/view/View;

    new-instance v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$14;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$14;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V

    .line 1018
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    .line 1020
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getCurrentFocus()Landroid/view/View;

    .line 1021
    return-void
.end method

.method public dismissAllDialog()V
    .locals 0

    .prologue
    .line 6107
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissResetConfirmDialog()V

    .line 6108
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissTemplateDialog()V

    .line 6109
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissConfirmDialog()V

    .line 6110
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissUnableDialog()V

    .line 6111
    return-void
.end method

.method public dismissConfirmDialog()V
    .locals 2

    .prologue
    .line 3793
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$54;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$54;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3803
    return-void
.end method

.method public dismissGestureOverlayHelp()V
    .locals 1

    .prologue
    .line 1180
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    if-eqz v0, :cond_0

    .line 1181
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->dismissHelpWindow()V

    .line 1182
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    .line 1184
    :cond_0
    return-void
.end method

.method public dismissTemplateDialog()V
    .locals 2

    .prologue
    .line 3684
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3685
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    .line 3686
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 3687
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 3689
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 3690
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    .line 3692
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_1
    return-void
.end method

.method public dismissUnableDialog()V
    .locals 2

    .prologue
    .line 3830
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$56;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$56;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3839
    return-void
.end method

.method public dismissViewPagerAppList()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1251
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 1252
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 1254
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    if-eqz v0, :cond_1

    .line 1255
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->dismissHelpWindow()V

    .line 1256
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    .line 1258
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerDialog:Landroid/app/Dialog;

    .line 1259
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerTrayShow:Z

    .line 1260
    return-void
.end method

.method public getFlashBarEnabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 896
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    if-eqz v1, :cond_0

    .line 897
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 899
    :cond_0
    :goto_0
    return v0

    .line 897
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getFlashBarState()Z
    .locals 1

    .prologue
    .line 889
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    if-eqz v0, :cond_0

    .line 890
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarState()Z

    move-result v0

    .line 892
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGuidelineWindow()Landroid/view/Window;
    .locals 1

    .prologue
    .line 727
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    return-object v0
.end method

.method public hideAppEditList()V
    .locals 1

    .prologue
    .line 5387
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->hideAppEditList(Z)V

    .line 5388
    return-void
.end method

.method public hideAppEditList(Z)V
    .locals 6
    .param p1, "bForceCloseAppList"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 5399
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbAnimating:Z

    if-nez v1, :cond_2

    .line 5400
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->saveFlashBarList()V

    .line 5401
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCloseApplist:Z

    .line 5404
    const/4 v0, 0x0

    .line 5405
    .local v0, "ani":Landroid/view/animation/Animation;
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    const/16 v2, 0x67

    if-ne v1, v2, :cond_3

    .line 5406
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04002f

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 5413
    :goto_0
    new-instance v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 5444
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getMainFrame()Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 5445
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 5446
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5448
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->checkCreateTemplateEnable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5449
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;

    const v2, 0x7f02002e

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 5450
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setSoundEffectsEnabled(Z)V

    .line 5451
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateText:Landroid/widget/TextView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 5454
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    invoke-virtual {v1, v2, v4, v3, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZIZ)V

    .line 5456
    .end local v0    # "ani":Landroid/view/animation/Animation;
    :cond_2
    return-void

    .line 5407
    .restart local v0    # "ani":Landroid/view/animation/Animation;
    :cond_3
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    const/16 v2, 0x68

    if-ne v1, v2, :cond_4

    .line 5408
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040030

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 5410
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04002e

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0
.end method

.method public hideAppEditPost()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5392
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCloseApplist:Z

    if-eqz v0, :cond_0

    .line 5393
    invoke-virtual {p0, v1, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateFlashBarState(ZZ)Z

    .line 5397
    :goto_0
    return-void

    .line 5395
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startCollapseTimer()V

    goto :goto_0
.end method

.method public hideTraybarHelpPopup()V
    .locals 0

    .prologue
    .line 2010
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissTrayOpenDialog()V

    .line 2011
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startCollapseTimer()V

    .line 2012
    return-void
.end method

.method public isPocketTrayOpen()Z
    .locals 1

    .prologue
    .line 4630
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOpenPocketTray:Z

    return v0
.end method

.method public makeRecentApp()V
    .locals 0

    .prologue
    .line 5613
    return-void
.end method

.method public moveViewPagerLeft()V
    .locals 2

    .prologue
    .line 1225
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMoveLeft:Z

    .line 1226
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerSelectedPage:I

    if-nez v0, :cond_0

    .line 1231
    :goto_0
    return-void

    .line 1229
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPager:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerSelectedPage:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method

.method public moveViewPagerRight()V
    .locals 2

    .prologue
    .line 1234
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMoveRight:Z

    .line 1235
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerSelectedPage:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerSelectedPage:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1240
    :goto_0
    return-void

    .line 1238
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPager:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerSelectedPage:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 8
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const v7, 0x7f080002

    const v3, 0x7f080001

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 3375
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p1, Landroid/content/res/Configuration;->fontScale:F

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget v2, v2, Landroid/content/res/Configuration;->fontScale:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_5

    .line 3382
    :cond_0
    invoke-virtual {p0, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListRelayout(Z)V

    .line 3383
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNeedUpdatePocketTrayButtonText:Z

    .line 3385
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    if-eqz v1, :cond_10

    .line 3386
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditBtn:Landroid/widget/Button;

    if-eqz v1, :cond_1

    .line 3387
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditBtn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3389
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditText:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 3390
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3400
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateText:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpText:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 3401
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f080003

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3402
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketHelpText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f080004

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3404
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_4

    .line 3405
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    const v2, 0x7f0f008f

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f080052

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 3406
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_5

    .line 3407
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    const v2, 0x7f0f0092

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f080052

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 3410
    :cond_5
    new-instance v1, Landroid/content/res/Configuration;

    invoke-direct {v1, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastConfig:Landroid/content/res/Configuration;

    .line 3411
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/FlashBarService/AppListController;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 3413
    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v1, :cond_6

    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-nez v1, :cond_6

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_6

    .line 3415
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideTextPadding()V

    .line 3418
    :cond_6
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 3419
    .local v0, "fullscreen":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 3420
    iget v1, v0, Landroid/graphics/Point;->x:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    .line 3421
    iget v1, v0, Landroid/graphics/Point;->y:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    .line 3423
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mStartFlashBar:Z

    if-eqz v1, :cond_8

    .line 3424
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsAvailableMove:Z

    if-nez v1, :cond_7

    .line 3425
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z

    invoke-direct {p0, v1, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuidePosition(ZZ)V

    .line 3426
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFocusVisible:Z

    invoke-direct {p0, v1, v2, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideVisibility(ZZZ)V

    .line 3428
    :cond_7
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z

    invoke-direct {p0, v1, v4, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListPosition(IZZ)V

    .line 3431
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    if-eqz v1, :cond_9

    .line 3432
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 3434
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f070001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    rsub-int/lit8 v1, v1, 0x64

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mChangedPosition:I

    .line 3435
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getFlashBarEnabled()Z

    move-result v1

    if-eqz v1, :cond_a

    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsInputMethodShown:Z

    if-eqz v1, :cond_a

    .line 3436
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mConfigChanged:Z

    .line 3437
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionY:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setAppListHandlePosition(I)V

    .line 3438
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z

    invoke-direct {p0, v1, v4, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListPosition(IZZ)V

    .line 3441
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_b

    .line 3442
    invoke-direct {p0, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissHistoryWindow(Z)V

    .line 3445
    :cond_b
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerTrayShow:Z

    if-eqz v1, :cond_c

    .line 3446
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateViewPagerLayout()V

    .line 3449
    :cond_c
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getWindowDefSize()V

    .line 3450
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 3451
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->dismissHelpWindow()V

    .line 3452
    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    .line 3453
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->createViewPagerHelp()V

    .line 3455
    :cond_d
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 3456
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->dismissHelpWindow()V

    .line 3457
    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    .line 3458
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->createAppListOverlayHelp()V

    .line 3460
    :cond_e
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    if-eqz v1, :cond_f

    .line 3461
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->dismissHelpWindow()V

    .line 3462
    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    .line 3464
    :cond_f
    return-void

    .line 3393
    .end local v0    # "fullscreen":Landroid/graphics/Point;
    :cond_10
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditBtn:Landroid/widget/Button;

    if-eqz v1, :cond_11

    .line 3394
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditBtn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3396
    :cond_11
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditText:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 3397
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 442
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->releaseAppListBitmap(Ljava/util/List;)V

    .line 443
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    .line 444
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 445
    return-void
.end method

.method public pkgManagerList(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 5138
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0, p1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->pkgManagerList(Landroid/content/Intent;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5139
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isMultiWindowSettingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5140
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListRelayout(Z)V

    .line 5141
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    if-eqz v0, :cond_0

    .line 5142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->displayAppListEditWindow(IZ)V

    .line 5146
    :cond_0
    return-void
.end method

.method public prepareClosing()V
    .locals 0

    .prologue
    .line 5670
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissUnableDialog()V

    .line 5671
    return-void
.end method

.method public removeListener(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;)V
    .locals 2
    .param p1, "holder"    # Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;

    .prologue
    const/4 v1, 0x0

    .line 6007
    if-eqz p1, :cond_0

    .line 6008
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 6009
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 6010
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->labelView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 6011
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->labelView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 6013
    :cond_0
    return-void
.end method

.method public reviveMultiWindowTray()V
    .locals 3

    .prologue
    .line 5710
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5711
    iget v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListPosition(IZZ)V

    .line 5712
    :cond_0
    return-void
.end method

.method public setAppEditListWindow(Landroid/view/Window;)V
    .locals 3
    .param p1, "window"    # Landroid/view/Window;

    .prologue
    .line 5264
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 5265
    .local v0, "more":Z
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v1, p1, v2, v0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setWindow(Landroid/view/Window;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Z)V

    .line 5266
    return-void

    .line 5264
    .end local v0    # "more":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFlashBarState(Z)V
    .locals 1
    .param p1, "enableState"    # Z

    .prologue
    .line 883
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    if-eqz v0, :cond_0

    .line 884
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->setFlashBarState(Z)V

    .line 886
    :cond_0
    return-void
.end method

.method public setGuidelineWindow(Landroid/view/Window;)V
    .locals 0
    .param p1, "window"    # Landroid/view/Window;

    .prologue
    .line 722
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;

    .line 723
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->makeGuideLineLayout()V

    .line 724
    return-void
.end method

.method public setHoverIcon(I)V
    .locals 4
    .param p1, "type"    # I

    .prologue
    .line 1244
    const/4 v1, -0x1

    :try_start_0
    invoke-static {p1, v1}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1248
    :goto_0
    return-void

    .line 1245
    :catch_0
    move-exception v0

    .line 1246
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "LegacyAppListWindow"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to change PointerIcon to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setSemiTransStatusMode(Z)V
    .locals 3
    .param p1, "on"    # Z

    .prologue
    .line 5715
    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-nez v1, :cond_0

    .line 5716
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const-string v2, "statusbar"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    .line 5718
    .local v0, "s":Landroid/app/StatusBarManager;
    if-eqz p1, :cond_0

    .line 5722
    .end local v0    # "s":Landroid/app/StatusBarManager;
    :cond_0
    return-void
.end method

.method public setWindow(Landroid/view/Window;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 7
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "service"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 683
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    .line 684
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeFlashBarList()V

    .line 686
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .line 688
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 689
    .local v2, "fullscreen":Landroid/graphics/Point;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 690
    iget v3, v2, Landroid/graphics/Point;->x:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I

    .line 691
    iget v3, v2, Landroid/graphics/Point;->y:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I

    .line 693
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    .line 695
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->makeFlashBarLayout(Z)V

    .line 696
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    iget-boolean v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    iget v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZI)V

    .line 698
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v3}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getGlobalSystemUiVisibility()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSystemUiVisibility:I

    .line 699
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$8;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$8;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 713
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    .line 715
    .local v0, "am":Landroid/app/IActivityManager;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mProcessObserver:Landroid/app/IProcessObserver;

    invoke-interface {v0, v3}, Landroid/app/IActivityManager;->registerProcessObserver(Landroid/app/IProcessObserver;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 719
    :goto_0
    return-void

    .line 716
    :catch_0
    move-exception v1

    .line 717
    .local v1, "e":Landroid/os/RemoteException;
    const-string v3, "LegacyAppListWindow"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed to registerProcessObserver, e = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public showFlashBar(ZZ)V
    .locals 7
    .param p1, "show"    # Z
    .param p2, "expand"    # Z

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 1334
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissViewPagerAppList()V

    .line 1337
    invoke-static {}, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;->getSealedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1338
    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setFlashBarState(Z)V

    .line 1339
    const/4 p1, 0x0

    .line 1342
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->cancelCollapseTimer()V

    .line 1344
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AppListController;->updateWindowRects()V

    .line 1345
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentGuideRect()Landroid/graphics/Rect;

    move-result-object v0

    .line 1346
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1347
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget v2, v2, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->setPreviewFullAppZone(I)V

    .line 1349
    if-eqz p1, :cond_3

    .line 1350
    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v1, :cond_1

    .line 1351
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZI)V

    .line 1353
    :cond_1
    invoke-direct {p0, v5, p2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startFlashBarAnimation(ZZ)V

    .line 1354
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 1355
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f080019

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1356
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->sendAccessibilityEvent(I)V

    .line 1367
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getGlobalSystemUiVisibility()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSystemUiVisibility:I

    .line 1368
    return-void

    .line 1359
    :cond_3
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    invoke-direct {p0, v1, v3, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updatePocketTrayLayout(IZZ)V

    .line 1360
    invoke-direct {p0, v3, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startFlashBarAnimation(ZZ)V

    .line 1361
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 1362
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f08001a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1363
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method

.method public showHistoryBarDialog(I)V
    .locals 8
    .param p1, "index"    # I

    .prologue
    const v5, 0x7f020092

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 5675
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    invoke-virtual {v3, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 5676
    .local v2, "view":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 5677
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsHoverType:Z

    if-eqz v3, :cond_0

    .line 5678
    invoke-virtual {v2, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 5707
    :goto_0
    return-void

    .line 5681
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->dismiss()V

    .line 5682
    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I

    if-ne v3, p1, :cond_2

    .line 5683
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_1

    .line 5684
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5686
    :cond_1
    iput v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentAppPosition:I

    .line 5687
    const/4 v3, -0x1

    iput v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I

    goto :goto_0

    .line 5692
    :cond_2
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 5693
    .local v1, "outMetrics":Landroid/util/DisplayMetrics;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 5694
    const/4 v3, 0x2

    new-array v0, v3, [I

    .line 5695
    .local v0, "loc":[I
    invoke-virtual {v2, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 5696
    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    packed-switch v3, :pswitch_data_0

    .line 5703
    :goto_1
    invoke-virtual {v2, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 5704
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mVibrator:Landroid/os/SystemVibrator;

    sget-object v4, Lcom/sec/android/app/FlashBarService/AppListController;->mDefaultIvt:[B

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v5}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 5705
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsHoverType:Z

    .line 5706
    invoke-direct {p0, v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->makeHistoryBarDialog(I)V

    goto :goto_0

    .line 5700
    :pswitch_0
    aget v3, v0, v7

    iget v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThbumNailShadowMargin:I

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentAppPosition:I

    goto :goto_1

    .line 5696
    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public showTraybarHelpPopup()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 3496
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "do_not_show_help_popup_open_traybar"

    const/4 v8, 0x0

    const/4 v9, -0x2

    invoke-static {v6, v7, v8, v9}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    if-eqz v6, :cond_1

    .line 3564
    :cond_0
    :goto_0
    return-void

    .line 3502
    :cond_1
    invoke-static {}, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;->getSealedState()Z

    move-result v6

    if-nez v6, :cond_0

    .line 3507
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->cancelCollapseTimer()V

    .line 3509
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 3510
    .local v2, "layoutInflater":Landroid/view/LayoutInflater;
    const v6, 0x7f030016

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 3512
    .local v4, "view":Landroid/view/View;
    const v6, 0x7f0f00a9

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 3513
    .local v0, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {v0, v10}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 3514
    new-instance v6, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$39;

    invoke-direct {v6, p0, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$39;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 3527
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v8, 0x7f080056

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v8, 0x7f080011

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$40;

    invoke-direct {v8, p0, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$40;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/widget/CheckBox;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 3540
    .local v1, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTrayOpenDialog:Landroid/app/AlertDialog;

    .line 3541
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTrayOpenDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 3542
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTrayOpenDialog:Landroid/app/AlertDialog;

    new-instance v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$41;

    invoke-direct {v7, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$41;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 3552
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTrayOpenDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    .line 3553
    .local v5, "window":Landroid/view/Window;
    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 3554
    .local v3, "params":Landroid/view/WindowManager$LayoutParams;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v6

    iput-object v6, v3, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 3555
    const/16 v6, 0x7d8

    iput v6, v3, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 3557
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTrayOpenDialog:Landroid/app/AlertDialog;

    new-instance v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$42;

    invoke-direct {v7, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$42;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 3563
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTrayOpenDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0
.end method

.method public showViewPagerTray(Ljava/lang/String;I)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "taskId"    # I

    .prologue
    const/16 v1, 0x12d

    .line 903
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSelectedAppPackage:Ljava/lang/String;

    .line 904
    iput p2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSelectedTaskId:I

    .line 905
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShowViewPagerTray:Z

    .line 906
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 907
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 908
    return-void
.end method

.method public updateAppListOverlayHelpLocation()V
    .locals 5

    .prologue
    .line 1058
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    if-nez v2, :cond_0

    .line 1074
    :goto_0
    return-void

    .line 1062
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    const v3, 0x7f0f00bf

    invoke-virtual {v2, v3}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1063
    .local v0, "aboveLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1064
    .local v1, "alp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->isPortrait()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1065
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a0105

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1070
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v2}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getGlobalSystemUiVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 1071
    iget v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v4, 0x1050010

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1073
    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1067
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a0106

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto :goto_1
.end method

.method public updateAppListRelayout(Z)V
    .locals 5
    .param p1, "bClear"    # Z

    .prologue
    const/4 v4, 0x0

    .line 5150
    if-eqz p1, :cond_1

    .line 5151
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 5152
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 5153
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 5155
    :cond_0
    invoke-direct {p0, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->makeFlashBarLayout(Z)V

    .line 5158
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    iget v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZI)V

    .line 5160
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mStartFlashBar:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z

    if-eqz v0, :cond_2

    .line 5161
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuidePosition(ZZ)V

    .line 5162
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z

    invoke-direct {p0, v0, v4, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideVisibility(ZZZ)V

    .line 5164
    :cond_2
    return-void
.end method

.method public updateFlashBarState(ZZ)Z
    .locals 4
    .param p1, "expand"    # Z
    .param p2, "pressedHelpBtn"    # Z

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 731
    if-nez p1, :cond_1

    sget-boolean v2, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v2, :cond_1

    .line 733
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->hideGuidelineWindow()V

    .line 737
    :cond_1
    invoke-static {}, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;->getSealedState()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 738
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v2, :cond_2

    .line 739
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 749
    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z

    if-ne v2, p1, :cond_4

    move v0, v1

    .line 760
    :goto_0
    return v0

    .line 741
    :cond_3
    if-ne p1, v0, :cond_2

    if-ne p2, v0, :cond_2

    .line 742
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-ne v2, v3, :cond_2

    .line 743
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 751
    :cond_4
    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z

    if-eqz v2, :cond_5

    if-nez p2, :cond_5

    move v0, v1

    .line 752
    goto :goto_0

    .line 754
    :cond_5
    if-eqz p1, :cond_6

    .line 755
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->openFlashBar()V

    goto :goto_0

    .line 757
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->closeFlashBar()V

    goto :goto_0
.end method

.method public updateListStatusForIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 1834
    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v1, :cond_3

    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-nez v1, :cond_3

    .line 1835
    invoke-virtual {p1}, Landroid/content/Intent;->getMultiWindowStyle()Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v0

    .line 1836
    .local v0, "requestZone":I
    const/16 v1, 0xf

    if-eq v0, v1, :cond_0

    if-nez v0, :cond_2

    .line 1838
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateFlashBarState(ZZ)Z

    .line 1842
    :goto_0
    iget v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I

    const/4 v2, -0x1

    if-le v1, v2, :cond_1

    .line 1843
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1848
    .end local v0    # "requestZone":I
    :cond_1
    :goto_1
    return-void

    .line 1840
    .restart local v0    # "requestZone":I
    :cond_2
    invoke-virtual {p0, v2, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateFlashBarState(ZZ)Z

    goto :goto_0

    .line 1846
    .end local v0    # "requestZone":I
    :cond_3
    invoke-virtual {p0, v2, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateFlashBarState(ZZ)Z

    goto :goto_1
.end method

.method public userSwitched()V
    .locals 2

    .prologue
    .line 5650
    const-string v0, "LegacyAppListWindow"

    const-string v1, "userSwitched"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5651
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;

    if-nez v0, :cond_0

    .line 5660
    :goto_0
    return-void

    .line 5654
    :cond_0
    const-wide/16 v0, 0x3e8

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5657
    :goto_1
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mUserId:I

    .line 5658
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeFlashBarList()V

    .line 5659
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListRelayout(Z)V

    goto :goto_0

    .line 5655
    :catch_0
    move-exception v0

    goto :goto_1
.end method
