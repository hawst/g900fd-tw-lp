.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$6;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->setWindow(Landroid/view/Window;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 611
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSystemUiVisibilityChange(I)V
    .locals 4
    .param p1, "visibility"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 613
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissTemplateDialog()V

    .line 614
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iput p1, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mSystemUiVisibility:I

    .line 615
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mStartFlashBar:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 616
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mStatusBarUpdate:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 617
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelSliding:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelOpened:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 618
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v1

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListPosition(IZZZ)V
    invoke-static {v0, v1, v2, v2, v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;IZZZ)V

    .line 619
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v1

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->displayAppListEditWindow(IZ)V

    .line 622
    :cond_1
    return-void
.end method
