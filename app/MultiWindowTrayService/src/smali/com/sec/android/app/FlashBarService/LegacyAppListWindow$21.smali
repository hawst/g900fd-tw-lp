.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 1374
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 13
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1377
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v10, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLunchBlock:Z
    invoke-static {v9, v10}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 1378
    const/4 v9, 0x1

    new-array v6, v9, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "text/uri-list"

    aput-object v10, v6, v9

    .line 1379
    .local v6, "strs":[Ljava/lang/String;
    new-instance v5, Landroid/content/ClipData$Item;

    const-string v9, ""

    invoke-direct {v5, v9}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;)V

    .line 1380
    .local v5, "item":Landroid/content/ClipData$Item;
    new-instance v1, Landroid/content/ClipData;

    const-string v9, "historybar"

    invoke-direct {v1, v9, v6, v5}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    .line 1382
    .local v1, "dragData":Landroid/content/ClipData;
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v9

    const/4 v10, -0x1

    if-le v9, v10, :cond_2

    .line 1383
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v10}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-boolean v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSupportMultiInstance:Z

    const/16 v12, 0x64

    invoke-virtual {v9, v10, v11, v12}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v4

    .line 1384
    .local v4, "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1385
    const/4 v9, 0x0

    .line 1429
    .end local v4    # "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    :goto_0
    return v9

    .line 1387
    .restart local v4    # "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    :cond_0
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v9, 0x0

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Intent;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v10, v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4402(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/content/Intent;)Landroid/content/Intent;

    .line 1388
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntents:Ljava/util/List;
    invoke-static {v9, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Ljava/util/List;)Ljava/util/List;

    .line 1389
    const-string v9, "LegacyAppListWindow"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "App icon long clicked :: packageName: = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1390
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v9}, Lcom/sec/android/app/FlashBarService/AppListController;->updateWindowRects()V

    .line 1393
    if-eqz v1, :cond_2

    .line 1395
    move-object v7, p1

    .line 1396
    .local v7, "thumbnailview":Landroid/view/View;
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/RelativeLayout;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v0

    .line 1397
    .local v0, "childCnt":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v0, :cond_1

    .line 1398
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/RelativeLayout;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    .line 1399
    .local v8, "thumnail":Landroid/widget/RelativeLayout;
    invoke-virtual {v8, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1400
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskInfos:Ljava/util/List;
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/ActivityManager$RunningTaskInfo;

    iget v9, v9, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskId:I
    invoke-static {v10, v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4702(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 1406
    .end local v8    # "thumnail":Landroid/widget/RelativeLayout;
    :cond_1
    new-instance v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;

    invoke-direct {v2, p0, v7, v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;Landroid/view/View;Landroid/view/View;)V

    .line 1425
    .local v2, "dragShadow":Landroid/view/View$DragShadowBuilder;
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 1426
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v1, v2, v9, v10}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 1429
    .end local v0    # "childCnt":I
    .end local v2    # "dragShadow":Landroid/view/View$DragShadowBuilder;
    .end local v3    # "i":I
    .end local v4    # "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .end local v7    # "thumbnailview":Landroid/view/View;
    :cond_2
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 1403
    .restart local v0    # "childCnt":I
    .restart local v3    # "i":I
    .restart local v4    # "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .restart local v7    # "thumbnailview":Landroid/view/View;
    .restart local v8    # "thumnail":Landroid/widget/RelativeLayout;
    :cond_3
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v10, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskId:I
    invoke-static {v9, v10}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4702(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 1397
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method
