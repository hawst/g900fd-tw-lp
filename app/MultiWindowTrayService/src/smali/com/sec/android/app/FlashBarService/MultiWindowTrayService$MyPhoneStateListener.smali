.class Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;
.super Landroid/telephony/PhoneStateListener;
.source "MultiWindowTrayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MyPhoneStateListener"
.end annotation


# instance fields
.field private mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 1
    .param p1, "service"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 1361
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    .line 1359
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .line 1362
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .line 1363
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p2, "x1"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$1;

    .prologue
    .line 1358
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;

    .prologue
    .line 1358
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;->dispose()V

    return-void
.end method

.method private dispose()V
    .locals 1

    .prologue
    .line 1366
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .line 1367
    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 1
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 1370
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1372
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    if-eqz v0, :cond_0

    .line 1373
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->onRining()V

    .line 1376
    :cond_0
    return-void
.end method
