.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$20;
.super Ljava/lang/Object;
.source "CenterBarWindow.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;->animateCenterBarOpen(Landroid/view/View;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

.field final synthetic val$autoCloseAnim:Z

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;ZLandroid/view/View;)V
    .locals 0

    .prologue
    .line 2105
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$20;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iput-boolean p2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$20;->val$autoCloseAnim:Z

    iput-object p3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$20;->val$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 2114
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$20;->val$autoCloseAnim:Z

    if-eqz v0, :cond_0

    .line 2115
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$20;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/FlashBarService/CenterBarWindow$20$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$20$1;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow$20;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2122
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$20;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtonOpenAnimating:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$4702(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 2123
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$20;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsShowButton:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3202(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 2124
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 2111
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 2108
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$20;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsShowButton:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3202(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 2109
    return-void
.end method
