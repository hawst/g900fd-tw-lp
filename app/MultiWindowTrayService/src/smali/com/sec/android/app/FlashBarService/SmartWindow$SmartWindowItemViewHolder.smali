.class final Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;
.super Ljava/lang/Object;
.source "SmartWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/SmartWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SmartWindowItemViewHolder"
.end annotation


# instance fields
.field contentView:Landroid/widget/LinearLayout;

.field iconView:Landroid/widget/ImageView;

.field labelView:Landroid/widget/TextView;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "smartWindowItemClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;->contentView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    return-void
.end method

.method public setOnDragListener(Landroid/view/View$OnDragListener;)V
    .locals 1
    .param p1, "smartWindowItemDragListener"    # Landroid/view/View$OnDragListener;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;->contentView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 143
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1
    .param p1, "smartWindowItemTouchListener"    # Landroid/view/View$OnTouchListener;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;->contentView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 147
    return-void
.end method
