.class Lcom/sec/android/app/FlashBarService/SmartEditWindow$4;
.super Ljava/lang/Object;
.source "SmartEditWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/SmartEditWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/SmartEditWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)V
    .locals 0

    .prologue
    .line 369
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    .line 371
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 391
    :cond_0
    :goto_0
    return v4

    .line 373
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mPressArrowbutton:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->access$302(Lcom/sec/android/app/FlashBarService/SmartEditWindow;Z)Z

    .line 374
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 375
    .local v0, "rId":I
    const v1, 0x7f0f00f2

    if-ne v0, v1, :cond_1

    .line 376
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 377
    :cond_1
    const v1, 0x7f0f00f3

    if-ne v0, v1, :cond_0

    .line 378
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 382
    .end local v0    # "rId":I
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/SmartEditWindow;->checkCanScroll()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->access$400(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)V

    goto :goto_0

    .line 386
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mPressArrowbutton:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->access$302(Lcom/sec/android/app/FlashBarService/SmartEditWindow;Z)Z

    .line 387
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 388
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 371
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
