.class Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$2;
.super Ljava/lang/Object;
.source "TryMultiWindowBezel.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->removeListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)V
    .locals 0

    .prologue
    .line 925
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$2;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 929
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 946
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 931
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$2;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$2;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTrayBarDisabledFunctions:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$4000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "AppSwitching"

    # invokes: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->checkFunctionState(Ljava/util/List;Ljava/lang/String;)Z
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$4100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Ljava/util/List;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 932
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$2;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # invokes: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->startAnimationFor4SubButton()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$4200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)V

    .line 936
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$2;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup6:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$4400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$2;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 937
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$2;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup6:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$4400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 938
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$2;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHandle:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$2;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFinishRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$4500(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 934
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$2;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # invokes: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->startAnimationFor5SubButton()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$4300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)V

    goto :goto_1

    .line 941
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$2;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0

    .line 929
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
