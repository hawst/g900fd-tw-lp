.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$77;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 4568
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$77;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 4570
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 4587
    :cond_0
    :goto_0
    return v2

    .line 4574
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$77;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->cancelCollapseTimer()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    .line 4575
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$77;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$77;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4576
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$77;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v0

    const/4 v1, -0x1

    if-le v0, v1, :cond_1

    .line 4577
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$77;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$77;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 4579
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$77;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissHistoryWindow(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)V

    goto :goto_0

    .line 4584
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$77;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startCollapseTimer()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    goto :goto_0

    .line 4570
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
