.class public Lcom/sec/android/app/FlashBarService/AppListController;
.super Ljava/lang/Object;
.source "AppListController.java"


# static fields
.field public static final mAppListItemDropIvt:[B

.field public static final mDefaultIvt:[B


# instance fields
.field private final MAX_TASKS:I

.field private final WINDOW_PORTRAIT_MODE:S

.field private mActivityManager:Landroid/app/ActivityManager;

.field private mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

.field private mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

.field private mArrangeState:I

.field private mContext:Landroid/content/Context;

.field private mDisplayOrientation:I

.field private mDockingArea:Landroid/graphics/Rect;

.field private mIWindowManager:Landroid/view/IWindowManager;

.field private mIntent:Landroid/content/Intent;

.field private mIsMWTrayOpen:Z

.field private final mLock:Ljava/lang/Object;

.field private mMultiInstanceMaxCnt:I

.field private mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

.field private mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

.field private mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

.field private mMultiWindowTrayInfoUsageStatus:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;

.field mMultiWindowTrayService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

.field public mPreviewFullAppZone:I

.field private mSupportMultiInstance:Z

.field mSupportQuadView:Z

.field private mWindowManager:Landroid/view/WindowManager;

.field private mZoneInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mZoneRect:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const/16 v0, 0x26

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/FlashBarService/AppListController;->mDefaultIvt:[B

    .line 104
    const/16 v0, 0x38

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/FlashBarService/AppListController;->mAppListItemDropIvt:[B

    return-void

    .line 95
    nop

    :array_0
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0xat
        0x0t
        0x14t
        0x0t
        0x0t
        0x0t
        0x20t
        0x1t
        0x0t
        0x0t
        0x7ft
        0x0t
        0x0t
        0x42t
        0x0t
        0x0t
        0x4dt
        0x0t
        0x61t
        0x0t
        0x67t
        0x0t
        0x53t
        0x0t
        0x77t
        0x0t
        0x65t
        0x0t
        0x65t
        0x0t
        0x70t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 104
    nop

    :array_1
    .array-data 1
        0x1t
        0x0t
        0x3t
        0x0t
        0x30t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x12t
        0x0t
        0x1at
        0x0t
        -0xft
        -0x20t
        0x2t
        -0x1et
        0x0t
        0x0t
        -0x30t
        0x0t
        0x21t
        -0xft
        -0x20t
        0x1t
        -0x1et
        0x0t
        0x26t
        -0x2ft
        0x5ft
        -0x1t
        0x20t
        0x0t
        0x0t
        0x0t
        0x7ft
        0x0t
        0x0t
        -0xft
        0x30t
        0x28t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x20t
        0x4t
        0x0t
        0x0t
        0x21t
        0x0t
        0x0t
        0x31t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "applist"    # Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    .param p3, "trayinfo"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    .param p4, "service"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/4 v1, 0x1

    iput-short v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->WINDOW_PORTRAIT_MODE:S

    .line 62
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mLock:Ljava/lang/Object;

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportMultiInstance:Z

    .line 65
    const/16 v1, 0x64

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->MAX_TASKS:I

    .line 78
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    .line 79
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    .line 80
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    .line 81
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mDockingArea:Landroid/graphics/Rect;

    .line 84
    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mArrangeState:I

    .line 85
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    .line 87
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportQuadView:Z

    .line 88
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIsMWTrayOpen:Z

    .line 91
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    .line 604
    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    .line 116
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    .line 117
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    .line 118
    iput-object p3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .line 119
    iput-object p4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mActivityManager:Landroid/app/ActivityManager;

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mWindowManager:Landroid/view/WindowManager;

    .line 122
    const-string v1, "window"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIWindowManager:Landroid/view/IWindowManager;

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    const-string v2, "multiwindow_facade"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/multiwindow/MultiWindowFacade;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mDisplayOrientation:I

    .line 125
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportMultiInstance(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportMultiInstance:Z

    .line 127
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mDockingArea:Landroid/graphics/Rect;

    .line 130
    :try_start_0
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    .line 131
    .local v0, "pm":Landroid/content/pm/IPackageManager;
    const-string v1, "com.sec.feature.multiwindow.quadview"

    invoke-interface {v0, v1}, Landroid/content/pm/IPackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportQuadView:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    .end local v0    # "pm":Landroid/content/pm/IPackageManager;
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getAvailableMultiInstanceCnt()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiInstanceMaxCnt:I

    .line 137
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListController;->updateWindowRects()V

    .line 138
    invoke-static {}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->getInstance()Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    .line 139
    new-instance v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfoUsageStatus:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;

    .line 140
    return-void

    .line 132
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private getExecutePriority(I)I
    .locals 6
    .param p1, "zoneInfo"    # I

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v3, 0x3

    .line 151
    if-ne p1, v3, :cond_1

    .line 152
    sget-boolean v2, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    if-ne v2, v3, :cond_0

    .line 165
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 152
    goto :goto_0

    .line 153
    :cond_1
    const/16 v5, 0xc

    if-ne p1, v5, :cond_3

    .line 154
    sget-boolean v2, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    if-ne v2, v3, :cond_2

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1

    .line 155
    :cond_3
    if-ne p1, v0, :cond_4

    move v0, v2

    .line 156
    goto :goto_0

    .line 157
    :cond_4
    if-ne p1, v4, :cond_5

    move v0, v3

    .line 158
    goto :goto_0

    .line 159
    :cond_5
    if-ne p1, v2, :cond_6

    move v0, v4

    .line 160
    goto :goto_0

    .line 161
    :cond_6
    const/16 v0, 0x8

    if-ne p1, v0, :cond_7

    .line 162
    const/4 v0, 0x5

    goto :goto_0

    .line 165
    :cond_7
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private getRunningTaskCnt(ZZZ)I
    .locals 12
    .param p1, "resume"    # Z
    .param p2, "withScale"    # Z
    .param p3, "showHidden"    # Z

    .prologue
    const/16 v11, 0x64

    const/16 v10, 0x800

    .line 791
    const/4 v1, 0x0

    .line 792
    .local v1, "cnt":I
    const/4 v5, 0x0

    .line 794
    .local v5, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz p1, :cond_3

    .line 795
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/4 v9, 0x3

    invoke-virtual {v8, v11, v9}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v5

    .line 800
    :goto_0
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    invoke-virtual {v8}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v3

    .line 801
    .local v3, "selectedAffinity":Ljava/lang/String;
    if-eqz v3, :cond_8

    .line 802
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 803
    .local v4, "t":Landroid/app/ActivityManager$RunningTaskInfo;
    iget v8, v4, Landroid/app/ActivityManager$RunningTaskInfo;->userId:I

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v9

    if-ne v8, v9, :cond_0

    .line 806
    iget-object v8, v4, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v0

    .line 807
    .local v0, "baseRunningAffinity":Ljava/lang/String;
    iget-object v8, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v7

    .line 808
    .local v7, "topRunningAffinity":Ljava/lang/String;
    iget-object v6, v4, Landroid/app/ActivityManager$RunningTaskInfo;->taskAffinity:Ljava/lang/String;

    .line 809
    .local v6, "taskRecordAffinity":Ljava/lang/String;
    iget-object v8, v4, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v8

    if-eqz v8, :cond_1

    if-eqz p3, :cond_0

    .line 810
    :cond_1
    if-eqz v0, :cond_4

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 811
    iget-object v8, v4, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v8, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v8

    if-eqz v8, :cond_2

    if-eqz p2, :cond_0

    .line 812
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 797
    .end local v0    # "baseRunningAffinity":Ljava/lang/String;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "selectedAffinity":Ljava/lang/String;
    .end local v4    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v6    # "taskRecordAffinity":Ljava/lang/String;
    .end local v7    # "topRunningAffinity":Ljava/lang/String;
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/4 v9, 0x2

    invoke-virtual {v8, v11, v9}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v5

    goto :goto_0

    .line 814
    .restart local v0    # "baseRunningAffinity":Ljava/lang/String;
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "selectedAffinity":Ljava/lang/String;
    .restart local v4    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .restart local v6    # "taskRecordAffinity":Ljava/lang/String;
    .restart local v7    # "topRunningAffinity":Ljava/lang/String;
    :cond_4
    if-eqz v7, :cond_6

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 815
    iget-object v8, v4, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v8, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v8

    if-eqz v8, :cond_5

    if-eqz p2, :cond_0

    .line 816
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 818
    :cond_6
    if-eqz v6, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 819
    iget-object v8, v4, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v8, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v8

    if-eqz v8, :cond_7

    if-eqz p2, :cond_0

    .line 820
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 826
    .end local v0    # "baseRunningAffinity":Ljava/lang/String;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v6    # "taskRecordAffinity":Ljava/lang/String;
    .end local v7    # "topRunningAffinity":Ljava/lang/String;
    :cond_8
    return v1
.end method


# virtual methods
.method public focusToSelectedApp(I)Z
    .locals 12
    .param p1, "curDstIndex"    # I

    .prologue
    const/16 v11, 0x64

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 924
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-boolean v10, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportMultiInstance:Z

    invoke-virtual {v7, p1, v10, v11}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v2

    .line 926
    .local v2, "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_4

    .line 927
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    if-ne v7, v8, :cond_5

    .line 928
    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Intent;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    .line 929
    invoke-direct {p0, v8, v8, v9}, Lcom/sec/android/app/FlashBarService/AppListController;->getRunningTaskCnt(ZZZ)I

    move-result v0

    .line 930
    .local v0, "cnt":I
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    invoke-virtual {v7}, Landroid/content/Intent;->getFlags()I

    move-result v7

    const/high16 v10, 0x8000000

    and-int/2addr v7, v10

    if-eqz v7, :cond_0

    .line 931
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiInstanceMaxCnt:I

    if-lt v0, v7, :cond_5

    move v7, v8

    .line 959
    .end local v0    # "cnt":I
    :goto_0
    return v7

    .line 935
    .restart local v0    # "cnt":I
    :cond_0
    if-lez v0, :cond_5

    .line 936
    if-ne v0, v8, :cond_3

    .line 937
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/4 v10, 0x3

    invoke-virtual {v7, v11, v10}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v4

    .line 938
    .local v4, "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 939
    .local v6, "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-eqz v7, :cond_1

    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    invoke-virtual {v10}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 941
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v5

    .line 942
    .local v5, "selectedAffinity":Ljava/lang/String;
    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v3

    .line 943
    .local v3, "runningAffinity":Ljava/lang/String;
    if-eqz v5, :cond_2

    if-eqz v3, :cond_2

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    move v7, v9

    .line 944
    goto :goto_0

    .line 946
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mActivityManager:Landroid/app/ActivityManager;

    iget v10, v6, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v9, v11}, Landroid/app/ActivityManager;->moveTaskToFront(IILandroid/os/Bundle;)V

    goto :goto_1

    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "runningAffinity":Ljava/lang/String;
    .end local v4    # "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v5    # "selectedAffinity":Ljava/lang/String;
    .end local v6    # "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_3
    move v7, v8

    .line 951
    goto :goto_0

    .end local v0    # "cnt":I
    :cond_4
    move v7, v9

    .line 956
    goto :goto_0

    :cond_5
    move v7, v9

    .line 959
    goto :goto_0
.end method

.method public getAvailableMultiInstanceCnt()I
    .locals 1

    .prologue
    .line 916
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiInstanceMaxCnt:I

    return v0
.end method

.method public getCurrentGuideRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 606
    invoke-virtual {p0, v0, v0}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentGuideRect(II)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentGuideRect(II)Landroid/graphics/Rect;
    .locals 12
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 609
    if-ltz p1, :cond_0

    if-gez p2, :cond_2

    :cond_0
    const/4 v1, 0x1

    .line 610
    .local v1, "autoSelect":Z
    :goto_0
    const/4 v8, 0x0

    iput v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    .line 612
    const/4 v5, 0x0

    .line 613
    .local v5, "retRect":Landroid/graphics/Rect;
    const/4 v6, 0x0

    .line 615
    .local v6, "retZone":I
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    if-nez v8, :cond_1

    .line 616
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListController;->updateWindowRects()V

    .line 619
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    if-nez v8, :cond_3

    if-nez v1, :cond_3

    .line 620
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    new-instance v10, Ljava/lang/Integer;

    const/4 v11, 0x0

    invoke-direct {v10, v11}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Rect;

    .line 718
    :goto_1
    return-object v8

    .line 609
    .end local v1    # "autoSelect":Z
    .end local v5    # "retRect":Landroid/graphics/Rect;
    .end local v6    # "retZone":I
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 623
    .restart local v1    # "autoSelect":Z
    .restart local v5    # "retRect":Landroid/graphics/Rect;
    .restart local v6    # "retZone":I
    :cond_3
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mLock:Ljava/lang/Object;

    monitor-enter v9

    .line 624
    :try_start_0
    sget-boolean v8, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v8, :cond_4

    sget-boolean v8, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v8, :cond_6

    :cond_4
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v10, 0x2

    invoke-virtual {v8, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v8

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v10, 0x1000

    invoke-virtual {v8, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v8

    if-nez v8, :cond_5

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/high16 v10, 0x200000

    invoke-virtual {v8, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 630
    :cond_5
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Rect;

    monitor-exit v9

    goto :goto_1

    .line 702
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v8

    .line 631
    :cond_6
    if-eqz v1, :cond_b

    .line 632
    :try_start_1
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    if-nez v8, :cond_8

    const/4 v7, 0x0

    .line 633
    .local v7, "zone":I
    :goto_2
    if-eqz v7, :cond_7

    if-gez v7, :cond_9

    .line 636
    :cond_7
    const/4 v8, 0x3

    iput v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    .line 637
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    const/4 v11, 0x3

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Rect;

    monitor-exit v9

    goto :goto_1

    .line 632
    .end local v7    # "zone":I
    :cond_8
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v8}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v7

    goto :goto_2

    .line 639
    .restart local v7    # "zone":I
    :cond_9
    move v6, v7

    .line 640
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Landroid/graphics/Rect;

    move-object v5, v0

    .line 702
    :cond_a
    :goto_3
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 703
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    if-eqz v8, :cond_18

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v8

    if-eqz v8, :cond_18

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v9, 0x1000

    invoke-virtual {v8, v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v8

    if-nez v8, :cond_18

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/high16 v9, 0x200000

    invoke-virtual {v8, v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v8

    if-nez v8, :cond_18

    .line 708
    const/4 v8, 0x3

    if-ne v6, v8, :cond_17

    .line 709
    const/16 v8, 0xc

    iput v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    :goto_4
    move-object v8, v5

    .line 718
    goto/16 :goto_1

    .line 643
    .end local v7    # "zone":I
    :cond_b
    :try_start_2
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    if-nez v8, :cond_c

    .line 644
    monitor-exit v9

    move-object v8, v5

    goto/16 :goto_1

    .line 647
    :cond_c
    sget-boolean v8, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v8, :cond_d

    sget-boolean v8, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v8, :cond_f

    :cond_d
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v10, 0x2

    invoke-virtual {v8, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v8

    if-eqz v8, :cond_e

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v10, 0x1000

    invoke-virtual {v8, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v8

    if-eqz v8, :cond_f

    .line 650
    :cond_e
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Rect;

    monitor-exit v9

    goto/16 :goto_1

    .line 653
    :cond_f
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v8}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v7

    .line 654
    .restart local v7    # "zone":I
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_5
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v3, v8, :cond_15

    .line 655
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Rect;

    .line 656
    .local v4, "rect":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 657
    invoke-virtual {v4, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v8

    if-eqz v8, :cond_14

    .line 658
    if-nez v7, :cond_12

    .line 659
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    if-eqz v8, :cond_11

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v10, 0x2

    invoke-virtual {v8, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v8

    if-eqz v8, :cond_11

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v10, 0x1000

    invoke-virtual {v8, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v8

    if-nez v8, :cond_11

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/high16 v10, 0x200000

    invoke-virtual {v8, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v8

    if-nez v8, :cond_11

    .line 664
    const/4 v8, 0x3

    if-ne v6, v8, :cond_10

    .line 665
    const/16 v8, 0xc

    iput v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    .line 674
    :goto_6
    monitor-exit v9

    move-object v8, v4

    goto/16 :goto_1

    .line 667
    :cond_10
    const/4 v8, 0x3

    iput v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    goto :goto_6

    .line 672
    :cond_11
    iput v6, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_6

    .line 677
    :cond_12
    :try_start_3
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIWindowManager:Landroid/view/IWindowManager;

    invoke-interface {v8}, Landroid/view/IWindowManager;->isNavigationBarVisible()Z

    move-result v8

    if-eqz v8, :cond_13

    .line 678
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v8, v6}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getZoneBounds(I)Landroid/graphics/Rect;
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v5

    .line 679
    :try_start_4
    monitor-exit v9

    move-object v8, v5

    goto/16 :goto_1

    .line 681
    :catch_0
    move-exception v2

    .line 682
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    .line 684
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_13
    move-object v5, v4

    .line 654
    :cond_14
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 690
    .end local v4    # "rect":Landroid/graphics/Rect;
    :cond_15
    if-eqz v5, :cond_a

    iget v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mArrangeState:I

    const/4 v10, 0x2

    if-ge v8, v10, :cond_a

    .line 691
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mDockingArea:Landroid/graphics/Rect;

    invoke-virtual {v8, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 692
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    const/4 v11, 0x3

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Rect;

    invoke-virtual {v8, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v8

    if-eqz v8, :cond_16

    .line 693
    const/4 v6, 0x3

    .line 694
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    const/4 v11, 0x3

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Landroid/graphics/Rect;

    move-object v5, v0

    goto/16 :goto_3

    .line 695
    :cond_16
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    const/16 v11, 0xc

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Rect;

    invoke-virtual {v8, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 696
    const/16 v6, 0xc

    .line 697
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    const/16 v11, 0xc

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Landroid/graphics/Rect;

    move-object v5, v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_3

    .line 711
    .end local v3    # "i":I
    :cond_17
    const/4 v8, 0x3

    iput v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    goto/16 :goto_4

    .line 716
    :cond_18
    iput v6, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    goto/16 :goto_4
.end method

.method public getCurrentZone(II)I
    .locals 10
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/16 v6, 0xc

    const/4 v5, 0x3

    const/4 v9, 0x2

    const/4 v4, 0x0

    .line 722
    const/4 v2, 0x0

    .line 723
    .local v2, "resultZone":I
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    if-nez v7, :cond_1

    move v4, v2

    .line 759
    :cond_0
    :goto_0
    return v4

    .line 726
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v7, v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v8, 0x1000

    invoke-virtual {v7, v8}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v7

    if-nez v7, :cond_0

    .line 730
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/high16 v8, 0x200000

    invoke-virtual {v7, v8}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v7

    if-nez v7, :cond_0

    .line 734
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v4}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v3

    .line 736
    .local v3, "zone":I
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mLock:Ljava/lang/Object;

    monitor-enter v7

    .line 737
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_4

    .line 738
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 739
    .local v1, "rect":Landroid/graphics/Rect;
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 740
    if-nez v3, :cond_2

    .line 741
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    monitor-exit v7

    goto :goto_0

    .line 758
    .end local v1    # "rect":Landroid/graphics/Rect;
    :catchall_0
    move-exception v4

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 743
    .restart local v1    # "rect":Landroid/graphics/Rect;
    :cond_2
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 737
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 749
    .end local v1    # "rect":Landroid/graphics/Rect;
    :cond_4
    if-eqz v2, :cond_6

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mArrangeState:I

    if-ge v4, v9, :cond_6

    .line 750
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mDockingArea:Landroid/graphics/Rect;

    invoke-virtual {v4, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 751
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    const/4 v9, 0x3

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Rect;

    invoke-virtual {v4, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 752
    monitor-exit v7

    move v4, v5

    goto/16 :goto_0

    .line 753
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    const/16 v8, 0xc

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Rect;

    invoke-virtual {v4, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 754
    monitor-exit v7

    move v4, v6

    goto/16 :goto_0

    .line 758
    :cond_6
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v4, v2

    .line 759
    goto/16 :goto_0
.end method

.method public getRectByZone(I)Landroid/graphics/Rect;
    .locals 4
    .param p1, "zone"    # I

    .prologue
    .line 763
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 764
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    monitor-exit v1

    return-object v0

    .line 765
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getSelectedTaskAffinity()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 884
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 885
    .local v3, "pm":Landroid/content/pm/PackageManager;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    invoke-virtual {v5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 886
    .local v1, "componentName":Landroid/content/ComponentName;
    if-nez v1, :cond_0

    .line 893
    .end local v1    # "componentName":Landroid/content/ComponentName;
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return-object v4

    .line 889
    .restart local v1    # "componentName":Landroid/content/ComponentName;
    .restart local v3    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    const/4 v5, 0x0

    invoke-virtual {v3, v1, v5}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 890
    .local v0, "Info":Landroid/content/pm/ActivityInfo;
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 892
    .end local v0    # "Info":Landroid/content/pm/ActivityInfo;
    .end local v1    # "componentName":Landroid/content/ComponentName;
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v2

    .line 893
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method public getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;
    .locals 5
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    const/4 v3, 0x0

    .line 899
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 900
    .local v2, "pm":Landroid/content/pm/PackageManager;
    if-nez p1, :cond_0

    .line 907
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return-object v3

    .line 903
    .restart local v2    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v2, p1, v4}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 904
    .local v0, "Info":Landroid/content/pm/ActivityInfo;
    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 906
    .end local v0    # "Info":Landroid/content/pm/ActivityInfo;
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v1

    .line 907
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method public isEnableMakePenWindow()Z
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v0}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->isEnableMakePenWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    const/4 v0, 0x1

    .line 498
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFullGuideLine(Landroid/content/Intent;)Z
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 977
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v7, v5}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getFrontActivityMultiWindowStyle(I)Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    .line 978
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/high16 v8, 0x200000

    invoke-virtual {v7, v8}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1007
    :cond_0
    :goto_0
    return v5

    .line 983
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    .line 985
    const/4 v2, 0x0

    .line 986
    .local v2, "isRunning":Z
    invoke-direct {p0, v6, v5, v6}, Lcom/sec/android/app/FlashBarService/AppListController;->getRunningTaskCnt(ZZZ)I

    move-result v0

    .line 987
    .local v0, "cnt":I
    if-lez v0, :cond_2

    .line 988
    const/4 v2, 0x1

    .line 991
    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v7

    const/high16 v8, 0x8000000

    and-int/2addr v7, v8

    if-eqz v7, :cond_3

    move v5, v6

    .line 993
    goto :goto_0

    .line 995
    :cond_3
    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportQuadView:Z

    if-nez v7, :cond_5

    .line 996
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/16 v8, 0x64

    const/4 v9, 0x3

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v3

    .line 997
    .local v3, "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 998
    .local v4, "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v7, v4, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v7}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getType()I

    move-result v7

    if-nez v7, :cond_4

    .line 1001
    iget-object v7, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-eqz v7, :cond_4

    iget-object v7, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    goto :goto_0

    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v4    # "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_5
    move v5, v6

    .line 1007
    goto :goto_0
.end method

.method public isLaunchingBlockedItem(I)Z
    .locals 12
    .param p1, "curDstIndex"    # I

    .prologue
    const/16 v11, 0x64

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 830
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-boolean v10, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportMultiInstance:Z

    invoke-virtual {v7, p1, v10, v11}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v2

    .line 832
    .local v2, "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_7

    .line 833
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    if-ne v7, v8, :cond_8

    .line 834
    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Intent;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    .line 835
    invoke-direct {p0, v8, v8, v9}, Lcom/sec/android/app/FlashBarService/AppListController;->getRunningTaskCnt(ZZZ)I

    move-result v0

    .line 836
    .local v0, "cnt":I
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    invoke-virtual {v7}, Landroid/content/Intent;->getFlags()I

    move-result v7

    const/high16 v10, 0x8000000

    and-int/2addr v7, v10

    if-eqz v7, :cond_0

    .line 837
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiInstanceMaxCnt:I

    if-lt v0, v7, :cond_8

    move v7, v8

    .line 869
    .end local v0    # "cnt":I
    :goto_0
    return v7

    .line 841
    .restart local v0    # "cnt":I
    :cond_0
    if-lez v0, :cond_8

    .line 842
    if-ne v0, v8, :cond_6

    .line 843
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/4 v10, 0x3

    invoke-virtual {v7, v11, v10}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v4

    .line 844
    .local v4, "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 845
    .local v6, "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-eqz v7, :cond_2

    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    invoke-virtual {v10}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    :cond_2
    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    if-eqz v7, :cond_4

    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    invoke-virtual {v10}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 847
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v5

    .line 848
    .local v5, "selectedAffinity":Ljava/lang/String;
    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v3

    .line 849
    .local v3, "runningAffinity":Ljava/lang/String;
    if-eqz v5, :cond_1

    if-eqz v3, :cond_1

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    move v7, v8

    .line 850
    goto :goto_0

    .line 852
    .end local v3    # "runningAffinity":Ljava/lang/String;
    .end local v5    # "selectedAffinity":Ljava/lang/String;
    :cond_4
    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->taskAffinity:Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 853
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v5

    .line 854
    .restart local v5    # "selectedAffinity":Ljava/lang/String;
    if-eqz v5, :cond_1

    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->taskAffinity:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    move v7, v8

    .line 855
    goto/16 :goto_0

    .end local v5    # "selectedAffinity":Ljava/lang/String;
    .end local v6    # "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_5
    move v7, v9

    .line 859
    goto/16 :goto_0

    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v4    # "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_6
    move v7, v8

    .line 861
    goto/16 :goto_0

    .end local v0    # "cnt":I
    :cond_7
    move v7, v9

    .line 866
    goto/16 :goto_0

    :cond_8
    move v7, v9

    .line 869
    goto/16 :goto_0
.end method

.method public isLaunchingBlockedTask(I)Z
    .locals 6
    .param p1, "taskId"    # I

    .prologue
    .line 873
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/16 v4, 0x64

    const/4 v5, 0x3

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v1

    .line 874
    .local v1, "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 875
    .local v2, "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    iget v3, v2, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    if-ne v3, p1, :cond_0

    .line 876
    const/4 v3, 0x1

    .line 879
    .end local v2    # "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public isMultiWindowTrayOpen()Z
    .locals 1

    .prologue
    .line 912
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIsMWTrayOpen:Z

    return v0
.end method

.method public isPenWindowOnly(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 963
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->isPenWindowOnly(I)Z

    move-result v0

    return v0
.end method

.method public isRunningScaleWindow(I)Z
    .locals 6
    .param p1, "taskId"    # I

    .prologue
    .line 1012
    const/4 v2, 0x0

    .line 1013
    .local v2, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/16 v4, 0x64

    const/4 v5, 0x3

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v2

    .line 1014
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1015
    .local v1, "t":Landroid/app/ActivityManager$RunningTaskInfo;
    iget v3, v1, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    if-ne v3, p1, :cond_0

    iget-object v3, v1, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v3}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getType()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 1016
    const/4 v3, 0x1

    .line 1019
    .end local v1    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public isSupportScaleApp(Landroid/content/pm/ActivityInfo;)Z
    .locals 1
    .param p1, "activityInfo"    # Landroid/content/pm/ActivityInfo;

    .prologue
    .line 920
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    invoke-virtual {v0, p1}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->isSupportScaleApp(Landroid/content/pm/ActivityInfo;)Z

    move-result v0

    return v0
.end method

.method public isTemplateItem(I)Z
    .locals 5
    .param p1, "mcurDstIndex"    # I

    .prologue
    const/4 v1, 0x1

    .line 967
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportMultiInstance:Z

    const/16 v4, 0x64

    invoke-virtual {v2, p1, v3, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v0

    .line 968
    .local v0, "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 969
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v1, :cond_0

    .line 973
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 143
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mDisplayOrientation:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_0

    .line 144
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mDisplayOrientation:I

    .line 146
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListController;->updateWindowRects()V

    .line 148
    :cond_0
    return-void
.end method

.method public setMWTrayOpenState(Z)V
    .locals 5
    .param p1, "open"    # Z

    .prologue
    const/4 v1, 0x1

    .line 775
    if-ne p1, v1, :cond_0

    .line 776
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 777
    .local v0, "intentForSendBroadcast":Landroid/content/Intent;
    const-string v2, "com.sec.android.intent.action.APPLIST_OPENED"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 778
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 781
    .end local v0    # "intentForSendBroadcast":Landroid/content/Intent;
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIsMWTrayOpen:Z

    if-eq v2, p1, :cond_1

    .line 782
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "multi_window_expanded"

    if-eqz p1, :cond_2

    :goto_0
    const/4 v4, -0x2

    invoke-static {v2, v3, v1, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 785
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIsMWTrayOpen:Z

    .line 787
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v1, p1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->setMultiWindowTrayOpenState(Z)V

    .line 788
    return-void

    .line 782
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public startActivitiesSafe([Landroid/content/Intent;)V
    .locals 8
    .param p1, "intents"    # [Landroid/content/Intent;

    .prologue
    .line 513
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    sget-object v7, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v5, p1, v6, v7}, Landroid/content/Context;->startActivitiesAsUser([Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 522
    :cond_0
    return-void

    .line 515
    :catch_0
    move-exception v1

    .line 517
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "MultiWindowTrayController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception in startActivitiesSafe() mContext="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    move-object v0, p1

    .local v0, "arr$":[Landroid/content/Intent;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v3, v0, v2

    .line 519
    .local v3, "intent":Landroid/content/Intent;
    const-string v5, "MultiWindowTrayController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    intent="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public startActivity(I)V
    .locals 1
    .param p1, "mcurDstIndex"    # I

    .prologue
    const/4 v0, -0x1

    .line 363
    invoke-virtual {p0, p1, v0, v0}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivity(III)V

    .line 364
    return-void
.end method

.method public startActivity(III)V
    .locals 6
    .param p1, "mcurDstIndex"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 199
    const/4 v4, 0x0

    const/4 v5, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivity(IIIZI)V

    .line 200
    return-void
.end method

.method public startActivity(IIIZI)V
    .locals 35
    .param p1, "mcurDstIndex"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "forceStart"    # Z
    .param p5, "specificTaskId"    # I

    .prologue
    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportMultiInstance:Z

    move/from16 v33, v0

    const/16 v34, 0x64

    move-object/from16 v0, v32

    move/from16 v1, p1

    move/from16 v2, v33

    move/from16 v3, v34

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v18

    .line 204
    .local v18, "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    const/16 v16, 0x0

    .line 206
    .local v16, "isRunning":Z
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v32

    if-nez v32, :cond_1

    .line 208
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v32

    const/16 v33, 0x1

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_10

    .line 209
    const/16 v32, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v32

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Landroid/content/Intent;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    .line 211
    const/16 v32, 0x0

    const/16 v33, 0x1

    const/16 v34, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v32

    move/from16 v2, v33

    move/from16 v3, v34

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/AppListController;->getRunningTaskCnt(ZZZ)I

    move-result v6

    .line 212
    .local v6, "cnt":I
    if-lez v6, :cond_0

    .line 213
    const/16 v16, 0x1

    .line 216
    :cond_0
    if-eqz v16, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/content/Intent;->getFlags()I

    move-result v32

    const/high16 v33, 0x8000000

    and-int v32, v32, v33

    if-eqz v32, :cond_2

    const/16 v32, -0x1

    move/from16 v0, p2

    move/from16 v1, v32

    if-ne v0, v1, :cond_2

    const/16 v32, -0x1

    move/from16 v0, p3

    move/from16 v1, v32

    if-ne v0, v1, :cond_2

    if-nez p4, :cond_2

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showHistoryBarDialog(I)V

    .line 360
    .end local v6    # "cnt":I
    :cond_1
    :goto_0
    return-void

    .line 221
    .restart local v6    # "cnt":I
    :cond_2
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/FlashBarService/AppListController;->isLaunchingBlockedItem(I)Z

    move-result v32

    if-nez v32, :cond_1

    .line 224
    const/4 v8, 0x1

    .line 225
    .local v8, "expand":Z
    new-instance v31, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v32, 0x1

    invoke-direct/range {v31 .. v32}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    .line 226
    .local v31, "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    const/16 v32, -0x1

    move/from16 v0, p2

    move/from16 v1, v32

    if-eq v0, v1, :cond_9

    const/16 v32, -0x1

    move/from16 v0, p3

    move/from16 v1, v32

    if-eq v0, v1, :cond_9

    .line 227
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentZone(II)I

    move-result v7

    .line 228
    .local v7, "currentZone":I
    const/16 v32, 0xf

    move/from16 v0, v32

    if-eq v7, v0, :cond_3

    if-nez v7, :cond_8

    .line 230
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    move-object/from16 v0, v32

    move/from16 v1, v33

    invoke-virtual {v0, v8, v1}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->updateFlashBarState(ZZ)Z

    .line 236
    :cond_4
    :goto_1
    move-object/from16 v0, v31

    invoke-virtual {v0, v7}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 260
    .end local v7    # "currentZone":I
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->initCenterBarIfNeed()V

    .line 262
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 263
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    const/16 v34, 0x3

    invoke-virtual/range {v32 .. v34}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v28

    .line 268
    .local v28, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitySafe(Landroid/content/Intent;)V

    .line 270
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v32

    const/16 v33, 0x3

    move/from16 v0, v32

    move/from16 v1, v33

    if-eq v0, v1, :cond_6

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v32

    const/16 v33, 0xc

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_1

    .line 271
    :cond_6
    if-eqz v28, :cond_f

    .line 272
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_7
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-eqz v32, :cond_f

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 273
    .local v27, "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    move-object/from16 v0, v27

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getType()I

    move-result v32

    const/16 v33, 0x1

    move/from16 v0, v32

    move/from16 v1, v33

    if-eq v0, v1, :cond_7

    move-object/from16 v0, v27

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    move-object/from16 v32, v0

    if-eqz v32, :cond_7

    move-object/from16 v0, v27

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v32

    if-eqz v32, :cond_7

    .line 275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v32, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->findResolveInfoByPackage(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v4

    .line 276
    .local v4, "appResolveInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v4, :cond_7

    .line 277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfoUsageStatus:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;->storeAppUsageCount(Landroid/content/pm/ResolveInfo;)V

    goto :goto_3

    .line 231
    .end local v4    # "appResolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v27    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v28    # "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .restart local v7    # "currentZone":I
    :cond_8
    sget-boolean v32, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v32, :cond_4

    .line 233
    const/4 v8, 0x0

    .line 234
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    move-object/from16 v0, v32

    move/from16 v1, v33

    invoke-virtual {v0, v8, v1}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->updateFlashBarState(ZZ)Z

    goto/16 :goto_1

    .line 238
    .end local v7    # "currentZone":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getFrontActivityMultiWindowStyle(I)Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v9

    .line 239
    .local v9, "frontActivityMultiWindowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    if-eqz v9, :cond_5

    const/16 v32, 0x2

    move/from16 v0, v32

    invoke-virtual {v9, v0}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v32

    if-eqz v32, :cond_5

    const/high16 v32, 0x200000

    move/from16 v0, v32

    invoke-virtual {v9, v0}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v32

    if-nez v32, :cond_5

    const/16 v32, 0x1000

    move/from16 v0, v32

    invoke-virtual {v9, v0}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v32

    if-nez v32, :cond_5

    .line 242
    const/16 v32, 0x1

    invoke-virtual/range {v31 .. v32}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setType(I)V

    .line 243
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    invoke-static/range {v32 .. v32}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportQuadView(Landroid/content/Context;)Z

    move-result v32

    if-nez v32, :cond_e

    .line 244
    invoke-virtual {v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v32

    const/16 v33, 0xc

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_b

    .line 245
    const/16 v32, 0x3

    invoke-virtual/range {v31 .. v32}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 256
    :cond_a
    :goto_4
    const/4 v8, 0x0

    .line 257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    move-object/from16 v0, v32

    move/from16 v1, v33

    invoke-virtual {v0, v8, v1}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->updateFlashBarState(ZZ)Z

    goto/16 :goto_2

    .line 246
    :cond_b
    invoke-virtual {v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v32

    const/16 v33, 0x3

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_c

    .line 247
    const/16 v32, 0xc

    invoke-virtual/range {v31 .. v32}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    goto :goto_4

    .line 248
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    move/from16 v32, v0

    const/16 v33, 0xc

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_d

    .line 249
    const/16 v32, 0x3

    invoke-virtual/range {v31 .. v32}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    goto :goto_4

    .line 250
    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    move/from16 v32, v0

    const/16 v33, 0x3

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_a

    .line 251
    const/16 v32, 0xc

    invoke-virtual/range {v31 .. v32}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    goto :goto_4

    .line 254
    :cond_e
    invoke-virtual {v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v32

    invoke-virtual/range {v31 .. v32}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    goto :goto_4

    .line 282
    .end local v9    # "frontActivityMultiWindowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    .restart local v28    # "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfoUsageStatus:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppResolveInfo(I)Landroid/content/pm/ResolveInfo;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;->storeAppUsageCount(Landroid/content/pm/ResolveInfo;)V

    goto/16 :goto_0

    .line 287
    .end local v6    # "cnt":I
    .end local v8    # "expand":Z
    .end local v28    # "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v31    # "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_10
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v32

    move/from16 v0, v32

    new-array v0, v0, [Landroid/content/Intent;

    move-object/from16 v32, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v32

    check-cast v32, [Landroid/content/Intent;

    move-object/from16 v15, v32

    check-cast v15, [Landroid/content/Intent;

    .line 290
    .local v15, "intents":[Landroid/content/Intent;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_5
    array-length v0, v15

    move/from16 v32, v0

    move/from16 v0, v32

    if-ge v10, v0, :cond_13

    .line 291
    aget-object v32, v15, v10

    invoke-virtual/range {v32 .. v32}, Landroid/content/Intent;->getMultiWindowStyle()Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v32

    const/16 v33, 0x1

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_12

    .line 292
    aget-object v32, v15, v10

    invoke-virtual/range {v32 .. v32}, Landroid/content/Intent;->getMultiWindowStyle()Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v32

    const/16 v33, 0x3

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 290
    :cond_11
    :goto_6
    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    .line 293
    :cond_12
    aget-object v32, v15, v10

    invoke-virtual/range {v32 .. v32}, Landroid/content/Intent;->getMultiWindowStyle()Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v32

    const/16 v33, 0x4

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_11

    .line 294
    aget-object v32, v15, v10

    invoke-virtual/range {v32 .. v32}, Landroid/content/Intent;->getMultiWindowStyle()Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v32

    const/16 v33, 0xc

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    goto :goto_6

    .line 300
    :cond_13
    array-length v0, v15

    move/from16 v21, v0

    .line 301
    .local v21, "matchCount":I
    array-length v0, v15

    move/from16 v32, v0

    add-int/lit8 v14, v32, -0x1

    .line 302
    .local v14, "intentCnt":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportQuadView:Z

    move/from16 v32, v0

    if-nez v32, :cond_19

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-object/from16 v32, v0

    const/16 v33, 0x64

    const/16 v34, 0x1

    invoke-virtual/range {v32 .. v34}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v24

    .line 305
    .local v24, "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_14
    :goto_7
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-eqz v32, :cond_17

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 306
    .local v30, "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    move-object/from16 v0, v30

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getType()I

    move-result v32

    const/16 v33, 0x1

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_14

    .line 308
    move-object v5, v15

    .local v5, "arr$":[Landroid/content/Intent;
    array-length v0, v5

    move/from16 v20, v0

    .local v20, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_8
    move/from16 v0, v20

    if-ge v12, v0, :cond_14

    aget-object v13, v5, v12

    .line 309
    .local v13, "intent":Landroid/content/Intent;
    const-string v32, "isResumed"

    const/16 v33, 0x0

    move-object/from16 v0, v32

    move/from16 v1, v33

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v32

    if-eqz v32, :cond_16

    .line 308
    :cond_15
    add-int/lit8 v12, v12, 0x1

    goto :goto_8

    .line 311
    :cond_16
    move-object/from16 v0, v30

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    move-object/from16 v32, v0

    if-eqz v32, :cond_15

    move-object/from16 v0, v30

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v32

    invoke-virtual {v13}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_15

    .line 313
    invoke-virtual {v13}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v32

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v25

    .line 314
    .local v25, "selectedAffinity":Ljava/lang/String;
    move-object/from16 v0, v30

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v23

    .line 315
    .local v23, "runningAffinity":Ljava/lang/String;
    if-eqz v25, :cond_15

    if-eqz v23, :cond_15

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_15

    .line 316
    add-int/lit8 v21, v21, -0x1

    .line 317
    const-string v32, "isResumed"

    const/16 v33, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v33

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_7

    .line 323
    .end local v5    # "arr$":[Landroid/content/Intent;
    .end local v12    # "i$":I
    .end local v13    # "intent":Landroid/content/Intent;
    .end local v20    # "len$":I
    .end local v23    # "runningAffinity":Ljava/lang/String;
    .end local v25    # "selectedAffinity":Ljava/lang/String;
    .end local v30    # "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_17
    const/4 v10, 0x1

    :goto_9
    array-length v0, v15

    move/from16 v32, v0

    move/from16 v0, v32

    if-ge v10, v0, :cond_19

    .line 324
    add-int/lit8 v32, v10, -0x1

    aget-object v32, v15, v32

    invoke-virtual/range {v32 .. v32}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v32

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v25

    .line 325
    .restart local v25    # "selectedAffinity":Ljava/lang/String;
    aget-object v32, v15, v10

    invoke-virtual/range {v32 .. v32}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v32

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v26

    .line 326
    .local v26, "selectedAffinity2":Ljava/lang/String;
    if-eqz v25, :cond_18

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_18

    .line 327
    add-int/lit8 v14, v14, -0x1

    .line 323
    :cond_18
    add-int/lit8 v10, v10, 0x1

    goto :goto_9

    .line 331
    .end local v24    # "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v25    # "selectedAffinity":Ljava/lang/String;
    .end local v26    # "selectedAffinity2":Ljava/lang/String;
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->initCenterBarIfNeed()V

    .line 333
    if-nez v21, :cond_1b

    if-nez v14, :cond_1b

    .line 357
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    const/16 v34, 0x0

    invoke-virtual/range {v32 .. v34}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->updateFlashBarState(ZZ)Z

    goto/16 :goto_0

    .line 338
    :cond_1b
    const/4 v10, 0x0

    :goto_a
    array-length v0, v15

    move/from16 v32, v0

    add-int/lit8 v32, v32, -0x1

    move/from16 v0, v32

    if-ge v10, v0, :cond_1e

    .line 339
    add-int/lit8 v17, v10, 0x1

    .local v17, "j":I
    :goto_b
    array-length v0, v15

    move/from16 v32, v0

    move/from16 v0, v17

    move/from16 v1, v32

    if-ge v0, v1, :cond_1d

    .line 340
    aget-object v32, v15, v10

    invoke-virtual/range {v32 .. v32}, Landroid/content/Intent;->getMultiWindowStyle()Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->getExecutePriority(I)I

    move-result v32

    aget-object v33, v15, v17

    invoke-virtual/range {v33 .. v33}, Landroid/content/Intent;->getMultiWindowStyle()Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v33

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->getExecutePriority(I)I

    move-result v33

    move/from16 v0, v32

    move/from16 v1, v33

    if-le v0, v1, :cond_1c

    .line 343
    aget-object v29, v15, v10

    .line 344
    .local v29, "tempIntent":Landroid/content/Intent;
    aget-object v32, v15, v17

    aput-object v32, v15, v10

    .line 345
    aput-object v29, v15, v17

    .line 339
    .end local v29    # "tempIntent":Landroid/content/Intent;
    :cond_1c
    add-int/lit8 v17, v17, 0x1

    goto :goto_b

    .line 338
    :cond_1d
    add-int/lit8 v10, v10, 0x1

    goto :goto_a

    .line 349
    .end local v17    # "j":I
    :cond_1e
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitiesSafe([Landroid/content/Intent;)V

    .line 350
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    move-object/from16 v0, v32

    move/from16 v1, p1

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getListItemByIndex(IZ)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v19

    .line 351
    .local v19, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v32

    invoke-interface/range {v32 .. v32}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .restart local v11    # "i$":Ljava/util/Iterator;
    :cond_1f
    :goto_c
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-eqz v32, :cond_1a

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/util/Pair;

    .line 352
    .local v22, "r":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    move-object/from16 v0, v22

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v32, v0

    if-eqz v32, :cond_1f

    .line 353
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfoUsageStatus:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;

    move-object/from16 v33, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v32, v0

    check-cast v32, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;->storeAppUsageCount(Landroid/content/pm/ResolveInfo;)V

    goto :goto_c
.end method

.method public startActivitySafe(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 503
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 504
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    const-string v2, "TRAY"

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/FlashBarService/LoggingHelper;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 509
    :goto_0
    return-void

    .line 506
    :catch_0
    move-exception v0

    .line 507
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MultiWindowTrayController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception in startActivityAsUserSafe() mContext="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", intent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startActivityViewPagerAppList(IILjava/lang/String;)V
    .locals 9
    .param p1, "index"    # I
    .param p2, "type"    # I
    .param p3, "selectedAppPackage"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x68

    const/4 v7, 0x0

    const/16 v6, 0x66

    .line 170
    const/4 v0, 0x0

    .line 171
    .local v0, "multiWindowAppIntentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    if-ne p2, v6, :cond_1

    .line 172
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportMultiInstance:Z

    invoke-virtual {v4, p1, v5, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v0

    .line 177
    :goto_0
    new-instance v3, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    .line 178
    .local v3, "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 179
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 180
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Intent;

    invoke-virtual {v4, v3}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 181
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Intent;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitySafe(Landroid/content/Intent;)V

    .line 182
    const/4 v1, 0x0

    .line 183
    .local v1, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v4, p3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->findResolveInfoByPackage(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 184
    .local v2, "selectedAppResolveInfo":Landroid/content/pm/ResolveInfo;
    if-ne p2, v6, :cond_2

    .line 185
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v4, p1, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getViewPagerAppListItem(II)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 189
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfoUsageStatus:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;

    invoke-virtual {v4, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;->storeAppUsageCount(Landroid/content/pm/ResolveInfo;)V

    .line 190
    if-eqz v2, :cond_0

    .line 191
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfoUsageStatus:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;->storeAppUsageCount(Landroid/content/pm/ResolveInfo;)V

    .line 194
    .end local v1    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v2    # "selectedAppResolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_0
    return-void

    .line 174
    .end local v3    # "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportMultiInstance:Z

    invoke-virtual {v4, p1, v5, v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 187
    .restart local v1    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .restart local v2    # "selectedAppResolveInfo":Landroid/content/pm/ResolveInfo;
    .restart local v3    # "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v4, p1, v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getViewPagerAppListItem(II)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v1

    goto :goto_1
.end method

.method public startFreeStyleActivity(I)V
    .locals 1
    .param p1, "mcurDstIndex"    # I

    .prologue
    const/4 v0, -0x1

    .line 367
    invoke-virtual {p0, p1, v0, v0}, Lcom/sec/android/app/FlashBarService/AppListController;->startFreeStyleActivity(III)V

    .line 368
    return-void
.end method

.method public startFreeStyleActivity(III)V
    .locals 23
    .param p1, "mcurDstIndex"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 371
    const/4 v11, 0x0

    .line 372
    .local v11, "isRunning":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportMultiInstance:Z

    move/from16 v19, v0

    const/16 v20, 0x64

    move-object/from16 v0, v18

    move/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v13

    .line 373
    .local v13, "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_1

    .line 374
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 376
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/content/Intent;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    .line 378
    const/16 v18, 0x0

    const/16 v19, 0x1

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/AppListController;->getRunningTaskCnt(ZZZ)I

    move-result v6

    .line 379
    .local v6, "cnt":I
    if-lez v6, :cond_0

    .line 380
    const/4 v11, 0x1

    .line 383
    :cond_0
    if-eqz v11, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Intent;->getFlags()I

    move-result v18

    const/high16 v19, 0x8000000

    and-int v18, v18, v19

    if-eqz v18, :cond_2

    const/16 v18, -0x1

    move/from16 v0, p2

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    const/16 v18, -0x1

    move/from16 v0, p3

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    .line 385
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showHistoryBarDialog(I)V

    .line 457
    .end local v6    # "cnt":I
    :cond_1
    :goto_0
    return-void

    .line 388
    .restart local v6    # "cnt":I
    :cond_2
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/FlashBarService/AppListController;->isLaunchingBlockedItem(I)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 389
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/FlashBarService/AppListController;->focusToSelectedApp(I)Z

    goto :goto_0

    .line 394
    :cond_3
    const/16 v18, -0x1

    move/from16 v0, p2

    move/from16 v1, v18

    if-eq v0, v1, :cond_6

    const/16 v18, -0x1

    move/from16 v0, p3

    move/from16 v1, v18

    if-eq v0, v1, :cond_6

    .line 395
    new-instance v17, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v18, 0x1

    invoke-direct/range {v17 .. v18}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    .line 396
    .local v17, "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentZone(II)I

    move-result v18

    const/16 v19, 0xf

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_4

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentZone(II)I

    move-result v18

    if-nez v18, :cond_5

    .line 398
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->updateFlashBarState(ZZ)Z

    .line 400
    :cond_5
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentZone(II)I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 401
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->initCenterBarIfNeed()V

    .line 402
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 403
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitySafe(Landroid/content/Intent;)V

    goto :goto_0

    .line 406
    .end local v17    # "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_6
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Intent;

    .line 407
    .local v9, "intent":Landroid/content/Intent;
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 408
    .local v7, "displayFrame":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v18

    const/16 v19, 0x80

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v4

    .line 409
    .local v4, "aInfo":Landroid/content/pm/ActivityInfo;
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/FlashBarService/AppListController;->isSupportScaleApp(Landroid/content/pm/ActivityInfo;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 410
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListController;->isEnableMakePenWindow()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 411
    new-instance v14, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v18, 0x2

    move/from16 v0, v18

    invoke-direct {v14, v0}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    .line 412
    .local v14, "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    const/16 v18, 0x800

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setOption(IZ)V

    .line 413
    invoke-virtual {v14, v7}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setBounds(Landroid/graphics/Rect;)V

    .line 414
    invoke-virtual {v9, v14}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 415
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitySafe(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 418
    .end local v14    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppResolveInfo(I)Landroid/content/pm/ResolveInfo;

    move-result-object v15

    .line 419
    .local v15, "r":Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 420
    .local v5, "appName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const v20, 0x7f08002b

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v5, v21, v22

    invoke-virtual/range {v19 .. v21}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x0

    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 426
    .end local v4    # "aInfo":Landroid/content/pm/ActivityInfo;
    .end local v5    # "appName":Ljava/lang/String;
    .end local v6    # "cnt":I
    .end local v7    # "displayFrame":Landroid/graphics/Rect;
    .end local v9    # "intent":Landroid/content/Intent;
    .end local v15    # "r":Landroid/content/pm/ResolveInfo;
    :cond_8
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v0, v0, [Landroid/content/Intent;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v13, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v18

    check-cast v18, [Landroid/content/Intent;

    move-object/from16 v10, v18

    check-cast v10, [Landroid/content/Intent;

    .line 429
    .local v10, "intents":[Landroid/content/Intent;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    array-length v0, v10

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v8, v0, :cond_b

    .line 430
    aget-object v18, v10, v8

    invoke-virtual/range {v18 .. v18}, Landroid/content/Intent;->getMultiWindowStyle()Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_a

    .line 431
    aget-object v18, v10, v8

    invoke-virtual/range {v18 .. v18}, Landroid/content/Intent;->getMultiWindowStyle()Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v18

    const/16 v19, 0x3

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 429
    :cond_9
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 432
    :cond_a
    aget-object v18, v10, v8

    invoke-virtual/range {v18 .. v18}, Landroid/content/Intent;->getMultiWindowStyle()Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v18

    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_9

    .line 433
    aget-object v18, v10, v8

    invoke-virtual/range {v18 .. v18}, Landroid/content/Intent;->getMultiWindowStyle()Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v18

    const/16 v19, 0xc

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    goto :goto_2

    .line 438
    :cond_b
    const/4 v8, 0x0

    :goto_3
    array-length v0, v10

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    if-ge v8, v0, :cond_e

    .line 439
    add-int/lit8 v12, v8, 0x1

    .local v12, "j":I
    :goto_4
    array-length v0, v10

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v12, v0, :cond_d

    .line 440
    aget-object v18, v10, v8

    invoke-virtual/range {v18 .. v18}, Landroid/content/Intent;->getMultiWindowStyle()Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->getExecutePriority(I)I

    move-result v18

    aget-object v19, v10, v12

    invoke-virtual/range {v19 .. v19}, Landroid/content/Intent;->getMultiWindowStyle()Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v19

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->getExecutePriority(I)I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_c

    .line 441
    aget-object v16, v10, v8

    .line 442
    .local v16, "tempIntent":Landroid/content/Intent;
    aget-object v18, v10, v12

    aput-object v18, v10, v8

    .line 443
    aput-object v16, v10, v12

    .line 439
    .end local v16    # "tempIntent":Landroid/content/Intent;
    :cond_c
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    .line 438
    :cond_d
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 448
    .end local v12    # "j":I
    :cond_e
    const/16 v18, 0x0

    aget-object v18, v10, v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Intent;->getMultiWindowStyle()Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v17

    .line 449
    .restart local v17    # "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setType(I)V

    .line 450
    const/16 v18, 0x0

    aget-object v18, v10, v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 452
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitiesSafe([Landroid/content/Intent;)V

    .line 453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->updateFlashBarState(ZZ)Z

    goto/16 :goto_0
.end method

.method public startFreeStyleActivity(ILandroid/graphics/Rect;)V
    .locals 9
    .param p1, "mcurDstIndex"    # I
    .param p2, "rect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 460
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportMultiInstance:Z

    const/16 v6, 0x64

    invoke-virtual {v4, p1, v5, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v2

    .line 461
    .local v2, "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 462
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v8, :cond_0

    .line 463
    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Intent;

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    .line 464
    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 465
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 467
    .local v0, "displayFrame":Landroid/graphics/Rect;
    new-instance v3, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    .line 468
    .local v3, "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    const/16 v4, 0x800

    invoke-virtual {v3, v4, v8}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setOption(IZ)V

    .line 469
    invoke-virtual {v3, v0}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setBounds(Landroid/graphics/Rect;)V

    .line 470
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 471
    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitySafe(Landroid/content/Intent;)V

    .line 474
    .end local v0    # "displayFrame":Landroid/graphics/Rect;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v3    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_0
    return-void
.end method

.method public startFreeStyleActivity(IIIII)Z
    .locals 6
    .param p1, "mcurDstIndex"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "w"    # I
    .param p5, "h"    # I

    .prologue
    const/4 v5, 0x1

    .line 477
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 478
    .local v0, "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportMultiInstance:Z

    const/16 v4, 0x67

    invoke-virtual {v2, p1, v3, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v0

    .line 479
    new-instance v1, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    .line 480
    .local v1, "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    const/16 v2, 0x800

    invoke-virtual {v1, v2, v5}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setOption(IZ)V

    .line 481
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, p2, p3, p4, p5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setBounds(Landroid/graphics/Rect;)V

    .line 483
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 485
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v5, :cond_0

    .line 486
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    .line 487
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 488
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mIntent:Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitySafe(Landroid/content/Intent;)V

    .line 491
    :cond_0
    return v5
.end method

.method public startRunningApp(I)V
    .locals 4
    .param p1, "runningAppIndex"    # I

    .prologue
    .line 525
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getRunningAppTaskIdByIndex(I)I

    move-result v0

    .line 526
    .local v0, "taskId":I
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mActivityManager:Landroid/app/ActivityManager;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/app/ActivityManager;->moveTaskToFront(IILandroid/os/Bundle;)V

    .line 527
    return-void
.end method

.method public updateWindowRects()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 530
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 531
    .local v1, "fullScreen":Landroid/graphics/Point;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 533
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->getApplistIndicatorSize()I

    move-result v2

    .line 534
    .local v2, "screenTop":I
    sget-boolean v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v3, :cond_0

    .line 535
    const/4 v2, 0x0

    .line 537
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v3, v10}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getFrontActivityMultiWindowStyle(I)Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    .line 538
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v3}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getArrangeState()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mArrangeState:I

    .line 541
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowTrayService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->initCenterBarIfNeed()V

    .line 542
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v3, v6}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getCenterBarPoint(I)Landroid/graphics/Point;

    move-result-object v0

    .line 543
    .local v0, "centerBarPoint":Landroid/graphics/Point;
    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->getCenterBarPoint()Landroid/graphics/Point;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 544
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->updateCenterBarPoint(Landroid/graphics/Point;)V

    .line 555
    :cond_1
    :goto_0
    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mDisplayOrientation:I

    if-ne v3, v10, :cond_7

    .line 556
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mDockingArea:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Point;->x:I

    iget v5, v1, Landroid/graphics/Point;->x:I

    div-int/lit8 v5, v5, 0xa

    sub-int/2addr v4, v5

    iput v4, v3, Landroid/graphics/Rect;->left:I

    .line 557
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mDockingArea:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Point;->x:I

    iget v5, v1, Landroid/graphics/Point;->x:I

    div-int/lit8 v5, v5, 0xa

    add-int/2addr v4, v5

    iput v4, v3, Landroid/graphics/Rect;->right:I

    .line 558
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mDockingArea:Landroid/graphics/Rect;

    iput v6, v3, Landroid/graphics/Rect;->top:I

    .line 559
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mDockingArea:Landroid/graphics/Rect;

    iget v4, v1, Landroid/graphics/Point;->y:I

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    .line 567
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 568
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 569
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 571
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget v8, v1, Landroid/graphics/Point;->x:I

    iget v9, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v9, v2

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 572
    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mDisplayOrientation:I

    if-ne v3, v10, :cond_8

    .line 573
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget v8, v1, Landroid/graphics/Point;->x:I

    iget v9, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 574
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    iget v7, v0, Landroid/graphics/Point;->y:I

    iget v8, v1, Landroid/graphics/Point;->x:I

    iget v9, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v9, v2

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 575
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportQuadView:Z

    if-eqz v3, :cond_2

    .line 576
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget v8, v0, Landroid/graphics/Point;->x:I

    iget v9, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 577
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    new-instance v5, Landroid/graphics/Rect;

    iget v6, v0, Landroid/graphics/Point;->x:I

    const/4 v7, 0x0

    iget v8, v1, Landroid/graphics/Point;->x:I

    iget v9, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 578
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    iget v7, v0, Landroid/graphics/Point;->y:I

    iget v8, v0, Landroid/graphics/Point;->x:I

    iget v9, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v9, v2

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 579
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    new-instance v5, Landroid/graphics/Rect;

    iget v6, v0, Landroid/graphics/Point;->x:I

    iget v7, v0, Landroid/graphics/Point;->y:I

    iget v8, v1, Landroid/graphics/Point;->x:I

    iget v9, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v9, v2

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 592
    :cond_2
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Integer;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 593
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Integer;

    const/4 v6, 0x3

    invoke-direct {v5, v6}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 594
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Integer;

    const/16 v6, 0xc

    invoke-direct {v5, v6}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 595
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportQuadView:Z

    if-eqz v3, :cond_3

    .line 596
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Integer;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 597
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Integer;

    const/4 v6, 0x2

    invoke-direct {v5, v6}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 598
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Integer;

    const/4 v6, 0x4

    invoke-direct {v5, v6}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 599
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Integer;

    const/16 v6, 0x8

    invoke-direct {v5, v6}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 601
    :cond_3
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 602
    :cond_4
    return-void

    .line 545
    :cond_5
    if-nez v0, :cond_1

    .line 547
    const-string v3, "MultiWindowTrayController"

    const-string v4, "mMultiWindowFacade.getCenterBarPoint() is null. keep old rects. if this is coming first time to call, error."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneInfo:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_4

    .line 549
    :cond_6
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "centerBarPoint":Landroid/graphics/Point;
    iget v3, v1, Landroid/graphics/Point;->x:I

    div-int/lit8 v3, v3, 0x2

    iget v4, v1, Landroid/graphics/Point;->y:I

    div-int/lit8 v4, v4, 0x2

    invoke-direct {v0, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "centerBarPoint":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 561
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mDockingArea:Landroid/graphics/Rect;

    iput v6, v3, Landroid/graphics/Rect;->left:I

    .line 562
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mDockingArea:Landroid/graphics/Rect;

    iget v4, v1, Landroid/graphics/Point;->x:I

    iput v4, v3, Landroid/graphics/Rect;->right:I

    .line 563
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mDockingArea:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Point;->y:I

    iget v5, v1, Landroid/graphics/Point;->y:I

    div-int/lit8 v5, v5, 0xa

    sub-int/2addr v4, v5

    iput v4, v3, Landroid/graphics/Rect;->top:I

    .line 564
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mDockingArea:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Point;->y:I

    iget v5, v1, Landroid/graphics/Point;->y:I

    div-int/lit8 v5, v5, 0xa

    add-int/2addr v4, v5

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_1

    .line 582
    :cond_8
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget v8, v0, Landroid/graphics/Point;->x:I

    iget v9, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v9, v2

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 583
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    new-instance v5, Landroid/graphics/Rect;

    iget v6, v0, Landroid/graphics/Point;->x:I

    const/4 v7, 0x0

    iget v8, v1, Landroid/graphics/Point;->x:I

    iget v9, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v9, v2

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 584
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mSupportQuadView:Z

    if-eqz v3, :cond_2

    .line 585
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget v8, v0, Landroid/graphics/Point;->x:I

    iget v9, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 586
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    iget v7, v0, Landroid/graphics/Point;->y:I

    iget v8, v0, Landroid/graphics/Point;->x:I

    iget v9, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v9, v2

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 587
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    new-instance v5, Landroid/graphics/Rect;

    iget v6, v0, Landroid/graphics/Point;->x:I

    const/4 v7, 0x0

    iget v8, v1, Landroid/graphics/Point;->x:I

    iget v9, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 588
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListController;->mZoneRect:Ljava/util/ArrayList;

    new-instance v5, Landroid/graphics/Rect;

    iget v6, v0, Landroid/graphics/Point;->x:I

    iget v7, v0, Landroid/graphics/Point;->y:I

    iget v8, v1, Landroid/graphics/Point;->x:I

    iget v9, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v9, v2

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 601
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method
