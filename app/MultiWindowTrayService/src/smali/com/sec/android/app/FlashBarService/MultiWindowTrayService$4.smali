.class Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$4;
.super Landroid/content/BroadcastReceiver;
.source "MultiWindowTrayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$4;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 220
    const-string v1, "MultiWindowTrayService"

    const-string v2, "mCenterBarFocusDisplayReceiver : onReceive"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 223
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.sec.android.action.NOTIFY_FOCUS_WINDOWS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 224
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$4;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    const-string v2, "com.sec.android.extra.ARRAGE_TYPE"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    # setter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mFocusZoneInfo:I
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$502(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;I)I

    .line 225
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$4;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 226
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$4;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$4;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mFocusZoneInfo:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$500(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->setFocusedZoneInfo(I)V

    .line 228
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$4;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSwitchWindow:Lcom/sec/android/app/FlashBarService/SwitchWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$600(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/SwitchWindow;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 229
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$4;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSwitchWindow:Lcom/sec/android/app/FlashBarService/SwitchWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$600(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/SwitchWindow;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$4;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mFocusZoneInfo:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$500(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->setFocusedZone(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 232
    :catch_0
    move-exception v1

    goto :goto_0
.end method
