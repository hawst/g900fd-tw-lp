.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$65;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 4180
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$65;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 4185
    const/4 v0, 0x0

    .line 4186
    .local v0, "anim":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$65;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOpenPocketTray:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4187
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$65;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04004a

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 4188
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$65;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOpenPocketTray:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 4193
    :goto_0
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$65$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$65$1;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow$65;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4207
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$65;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 4208
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$65;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startCollapseTimer()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    .line 4209
    return-void

    .line 4190
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$65;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040032

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 4191
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$65;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOpenPocketTray:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    goto :goto_0
.end method
