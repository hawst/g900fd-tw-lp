.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$6;
.super Landroid/content/BroadcastReceiver;
.source "LegacyAppListWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 644
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    const/16 v6, 0xce

    .line 647
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 649
    .local v0, "action":Ljava/lang/String;
    const-string v2, "ResponseAxT9Info"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 650
    const-string v2, "AxT9IME.isVisibleWindow"

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 652
    .local v1, "isInputMethodShown":Z
    const-string v2, "AxT9IME.isMovableKeypad"

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 665
    .end local v1    # "isInputMethodShown":Z
    :cond_0
    :goto_0
    return-void

    .line 655
    .restart local v1    # "isInputMethodShown":Z
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getFlashBarEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 658
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsInputMethodShown:Z
    invoke-static {v2, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2002(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 659
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v3, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mInputMethodChanged:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 660
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionY:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v3

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setAppListHandlePosition(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)V

    .line 661
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 662
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 663
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
