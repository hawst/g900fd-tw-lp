.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 2136
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 11
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 2138
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    .line 2139
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 2281
    const-string v7, "DragDrop"

    const-string v8, "Unknown action type received by OnDragListener."

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2284
    .end local p1    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    const/4 v7, 0x1

    return v7

    .line 2141
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_0
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->findeCurIndex(Landroid/view/View;)I
    invoke-static {v8, p1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/view/View;)I

    move-result v8

    if-ne v7, v8, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v7

    if-nez v7, :cond_0

    move-object v7, p1

    .line 2143
    check-cast v7, Landroid/widget/LinearLayout;

    const v8, 0x7f0f0059

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    move-object v5, v7

    check-cast v5, Landroid/widget/ImageView;

    .line 2144
    .local v5, "orgView":Landroid/widget/ImageView;
    const-string v8, "AppListBezelWindow"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Drag Start "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v7, 0x7f0f005c

    invoke-virtual {p1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    check-cast v7, Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2145
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5802(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 2146
    const v7, 0x7f02007f

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2147
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIvt:[B
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)[B

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v9}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 2149
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setItemIndexFromAppList(I)V

    .line 2151
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 2152
    .local v3, "l":Landroid/view/WindowManager$LayoutParams;
    const/4 v7, 0x0

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2153
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v7

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2154
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2155
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v8

    sub-int/2addr v7, v8

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2156
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 2157
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v8

    invoke-interface {v7, v8, v3}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2158
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2159
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->cancelCollapseTimer()V
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    goto/16 :goto_0

    .line 2164
    .end local v3    # "l":Landroid/view/WindowManager$LayoutParams;
    .end local v5    # "orgView":Landroid/widget/ImageView;
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListDragMode:Z
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6202(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 2165
    const-string v8, "AppListBezelWindow"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Drag Entered"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v7, 0x7f0f005c

    invoke-virtual {p1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    check-cast v7, Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2166
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTopGuideline:Landroid/widget/FrameLayout;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/FrameLayout;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2167
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBottomGuideline:Landroid/widget/FrameLayout;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/FrameLayout;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2168
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/GuideLineImageView;->setVisibility(I)V

    .line 2169
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2170
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddEmptyItemPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2171
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppCnt()I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v9

    const/4 v10, 0x6

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->changlistItemFromEditList(III)Z
    invoke-static {v7, v8, v9, v10}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;III)Z

    .line 2172
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddEmptyItemPosition:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6702(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    goto/16 :goto_0

    .line 2177
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_2
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2178
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->decideScrollMove(Landroid/view/View;Landroid/view/DragEvent;)Z
    invoke-static {v7, p1, p2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/view/View;Landroid/view/DragEvent;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 2181
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->findeCurIndex(Landroid/view/View;)I
    invoke-static {v8, p1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/view/View;)I

    move-result v8

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 2182
    const-string v7, "AppListBezelWindow"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Item dstIndex ="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "srcIndex="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2183
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_0

    .line 2184
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2186
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v8

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddToListItem:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 2187
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppCnt()I

    move-result v8

    if-ge v7, v8, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v8

    if-eq v7, v8, :cond_2

    .line 2188
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->changlistItemFromEditList(III)Z
    invoke-static {v7, v8, v9, v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;III)Z

    .line 2199
    :cond_1
    :goto_1
    const-string v8, "AppListBezelWindow"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Drag Location set grid item"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object v7, p1

    check-cast v7, Landroid/widget/LinearLayout;

    const v10, 0x7f0f005c

    invoke-virtual {v7, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    check-cast v7, Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2201
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v7, 0x7f0f0059

    invoke-virtual {p1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    move-object v1, v7

    check-cast v1, Landroid/widget/ImageView;

    .line 2202
    .local v1, "blankView":Landroid/widget/ImageView;
    const v7, 0x7f02007f

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 2191
    .end local v1    # "blankView":Landroid/widget/ImageView;
    .restart local p1    # "view":Landroid/view/View;
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppCnt()I

    move-result v8

    if-ne v7, v8, :cond_1

    .line 2192
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->changlistItemFromEditList(III)Z
    invoke-static {v7, v8, v9, v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;III)Z

    goto :goto_1

    .line 2196
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->changelistItem(II)Z
    invoke-static {v7, v8, v9}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;II)Z

    .line 2197
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setItemIndexFromAppList(I)V

    goto/16 :goto_1

    .line 2208
    :pswitch_3
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v7

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_4

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->checkAppListLayout()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2209
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListRelayout(Z)V

    .line 2210
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5602(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 2211
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setStartDragFromEdit(Z)V

    .line 2212
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5802(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 2213
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 2216
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddToListItem:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_5

    .line 2217
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddToListItem:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddToListItem:I
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v9

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v10}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->changeApplistItem(IIZ)V

    .line 2218
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddToListItem:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 2219
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListRelayout(Z)V

    .line 2222
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->findeCurIndex(Landroid/view/View;)I
    invoke-static {v7, p1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/view/View;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v8

    if-ne v7, v8, :cond_7

    .line 2223
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v7, 0x7f0f0059

    invoke-virtual {p1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    move-object v4, v7

    check-cast v4, Landroid/widget/ImageView;

    .line 2224
    .local v4, "org":Landroid/widget/ImageView;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    if-eqz v7, :cond_6

    .line 2225
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2226
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5802(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 2228
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5602(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 2229
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 2232
    .end local v4    # "org":Landroid/widget/ImageView;
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2233
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 2237
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_4
    const-string v8, "AppListBezelWindow"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Drag Exited : "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object v7, p1

    check-cast v7, Landroid/widget/LinearLayout;

    const v10, 0x7f0f005c

    invoke-virtual {v7, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    check-cast v7, Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2238
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v7

    if-eqz v7, :cond_9

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v7

    if-eqz v7, :cond_8

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->doOneTimeAfterDragStarts:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 2239
    :cond_8
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->doOneTimeAfterDragStarts:Z
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7202(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 2240
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v7, 0x7f0f0059

    invoke-virtual {p1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    move-object v1, v7

    check-cast v1, Landroid/widget/ImageView;

    .line 2241
    .restart local v1    # "blankView":Landroid/widget/ImageView;
    const v7, 0x7f02007f

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2243
    .end local v1    # "blankView":Landroid/widget/ImageView;
    :cond_9
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2246
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->changlistItemFromEditList(III)Z
    invoke-static {v7, v8, v9, v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;III)Z

    .line 2247
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 2248
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5602(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    goto/16 :goto_0

    .line 2252
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_5
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListDragMode:Z
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6202(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 2253
    const-string v7, "AppListBezelWindow"

    const-string v8, "Drag Drop"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2254
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v7

    if-eqz v7, :cond_a

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_a

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v7

    if-eqz v7, :cond_a

    .line 2255
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->addAndChangelistItem(II)Z
    invoke-static {v7, v8, v9}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;II)Z

    .line 2256
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIvt:[B
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)[B

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v9}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 2257
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v7

    const/4 v8, -0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setItemIndexFromAppList(I)V

    .line 2258
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddToListItem:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 2259
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->updateEditListChanged()V

    .line 2261
    :cond_a
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_b

    .line 2262
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f02007f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2263
    .local v2, "curIcon":Landroid/graphics/drawable/Drawable;
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v7, 0x7f0f0059

    invoke-virtual {p1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    move-object v6, v7

    check-cast v6, Landroid/widget/ImageView;

    .line 2265
    .local v6, "tmpiv":Landroid/widget/ImageView;
    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    if-ne v7, v2, :cond_b

    .line 2266
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2267
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5802(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 2270
    .end local v2    # "curIcon":Landroid/graphics/drawable/Drawable;
    .end local v6    # "tmpiv":Landroid/widget/ImageView;
    :cond_b
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 2271
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 2273
    :cond_c
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v7

    if-eqz v7, :cond_d

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v7

    if-eqz v7, :cond_d

    .line 2274
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 2275
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5602(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 2277
    :cond_d
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startCollapseTimer()V
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    goto/16 :goto_0

    .line 2139
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method
