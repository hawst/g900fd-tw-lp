.class public Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;
.super Landroid/app/Activity;
.source "TryMultiWindowTray.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;,
        Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRightLongClick;,
        Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayLongClick;,
        Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;,
        Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$CloseTrayOnClickListener;
    }
.end annotation


# instance fields
.field private mBubbleAnimation:Landroid/view/animation/Animation;

.field private mDisplayHeight:I

.field private mHandler:Landroid/os/Handler;

.field private mInvalidToast:Landroid/widget/Toast;

.field private mIvt:[B

.field private mMWDialogPopup1:Landroid/widget/RelativeLayout;

.field private mMWDialogPopup2:Landroid/widget/RelativeLayout;

.field private mMWFeatureLevel:Z

.field private mMWTryPocketButton:Landroid/widget/RelativeLayout;

.field private mMWTryPocketDivider:Landroid/widget/RelativeLayout;

.field private mOrientation:I

.field private mResource:Landroid/content/res/Resources;

.field private mRotated:Z

.field private mTrayClosed:Z

.field private mTrayInLeft:Z

.field private mTrayInRight:Z

.field private mTryMWTrayBarShadow:Landroid/widget/ImageView;

.field private mTryMWTrayContent1:Landroid/widget/RelativeLayout;

.field private mTryMWTrayContent2:Landroid/widget/RelativeLayout;

.field private mTryMWTrayContent3:Landroid/widget/RelativeLayout;

.field private mTryMWTrayDragHandler:Landroid/widget/RelativeLayout;

.field private mTryMWTrayDragLeft:Landroid/widget/RelativeLayout;

.field private mTryMWTrayGuideCue1:Landroid/widget/ImageView;

.field private mTryMWTrayGuideCue2:Landroid/widget/ImageView;

.field private mTryMWTrayGuideCue3:Landroid/widget/ImageView;

.field private mTryMWTrayHandleLeft:Landroid/widget/ImageView;

.field private mTryMWTrayHandleRight:Landroid/widget/ImageView;

.field private mTryMWTrayHandleRightMove:Landroid/widget/ImageView;

.field private mTryMWTrayHandleRightMoveClicked:Z

.field private mTryMWTrayHandleRightMoveHeightRate:F

.field private mTryMWTrayHandleRightShadow:Landroid/widget/ImageView;

.field private mTryMWTrayImageContainerLeft:Landroid/widget/LinearLayout;

.field private mTryMWTrayImageContainerRight:Landroid/widget/LinearLayout;

.field private mTryMWTrayLeft:Landroid/widget/RelativeLayout;

.field private mTryMWTrayPopUpArrow1:Landroid/widget/ImageView;

.field private mTryMWTrayPopUpArrow2:Landroid/widget/ImageView;

.field private mTryMWTrayPopUpArrow3:Landroid/widget/ImageView;

.field private mTryMWTrayPopup1:Landroid/widget/RelativeLayout;

.field private mTryMWTrayPopup2:Landroid/widget/RelativeLayout;

.field private mTryMWTrayPopup3:Landroid/widget/RelativeLayout;

.field private mTryMWTrayPopup4:Landroid/widget/RelativeLayout;

.field private mTryMWTrayRight:Landroid/widget/RelativeLayout;

.field private mTryMWTrayRightMove:Landroid/widget/RelativeLayout;

.field private mTryMWTrayText1:Landroid/widget/TextView;

.field private mTryMWTrayText2:Landroid/widget/TextView;

.field private mTryMWTrayText3:Landroid/widget/TextView;

.field private mTryMWTrayWindowHelper:Landroid/widget/RelativeLayout;

.field private mTryMWTryPop1Text:Landroid/widget/TextView;

.field private mTryMWTryPop2Text:Landroid/widget/TextView;

.field private mTryMWTryPop3Text:Landroid/widget/TextView;

.field private mTryMWTryPop4Text:Landroid/widget/TextView;

.field private mVibrator:Landroid/os/SystemVibrator;

.field private mWindowManager:Landroid/view/WindowManager;

.field private r:Ljava/lang/Runnable;

.field private rightHandleMovementStarted:Z

.field private trayToRight:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 39
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleLeft:Landroid/widget/ImageView;

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRight:Landroid/widget/ImageView;

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTryPop1Text:Landroid/widget/TextView;

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTryPop2Text:Landroid/widget/TextView;

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTryPop3Text:Landroid/widget/TextView;

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTryPop4Text:Landroid/widget/TextView;

    .line 55
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayLeft:Landroid/widget/RelativeLayout;

    .line 56
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayRight:Landroid/widget/RelativeLayout;

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayDragLeft:Landroid/widget/RelativeLayout;

    .line 58
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayDragHandler:Landroid/widget/RelativeLayout;

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayRightMove:Landroid/widget/RelativeLayout;

    .line 60
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayWindowHelper:Landroid/widget/RelativeLayout;

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWDialogPopup1:Landroid/widget/RelativeLayout;

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWDialogPopup2:Landroid/widget/RelativeLayout;

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWTryPocketButton:Landroid/widget/RelativeLayout;

    .line 72
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWTryPocketDivider:Landroid/widget/RelativeLayout;

    .line 78
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayImageContainerLeft:Landroid/widget/LinearLayout;

    .line 79
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayImageContainerRight:Landroid/widget/LinearLayout;

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mInvalidToast:Landroid/widget/Toast;

    .line 83
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mHandler:Landroid/os/Handler;

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWFeatureLevel:Z

    .line 92
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTrayInRight:Z

    .line 93
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTrayInLeft:Z

    .line 94
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->trayToRight:Z

    .line 95
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->rightHandleMovementStarted:Z

    .line 96
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mRotated:Z

    .line 97
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTrayClosed:Z

    .line 98
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMoveClicked:Z

    .line 99
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMoveHeightRate:F

    .line 103
    const/16 v0, 0x26

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mIvt:[B

    .line 825
    new-instance v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$1;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->r:Ljava/lang/Runnable;

    return-void

    .line 103
    nop

    :array_0
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0xat
        0x0t
        0x14t
        0x0t
        0x0t
        0x0t
        0x20t
        0x1t
        0x0t
        0x0t
        0x7ft
        0x0t
        0x0t
        0x42t
        0x0t
        0x0t
        0x4dt
        0x0t
        0x61t
        0x0t
        0x67t
        0x0t
        0x53t
        0x0t
        0x77t
        0x0t
        0x65t
        0x0t
        0x65t
        0x0t
        0x70t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method static synthetic access$1000(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->setHandleToCenter()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mDisplayHeight:I

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup4:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->r:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMoveClicked:Z

    return p1
.end method

.method static synthetic access$1802(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMoveHeightRate:F

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->rightHandleMovementStarted:Z

    return p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTrayClosed:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayDragHandler:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mIvt:[B

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/os/SystemVibrator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mVibrator:Landroid/os/SystemVibrator;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayDragLeft:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayLeft:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup1:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopUpArrow1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayRight:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTrayInRight:Z

    return v0
.end method

.method static synthetic access$3002(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTrayInRight:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayImageContainerLeft:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleLeft:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup2:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3402(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->trayToRight:Z

    return p1
.end method

.method static synthetic access$3500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightShadow:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mRotated:Z

    return v0
.end method

.method static synthetic access$3602(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mRotated:Z

    return p1
.end method

.method static synthetic access$3700(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayWindowHelper:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3802(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTrayInLeft:Z

    return p1
.end method

.method static synthetic access$3900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayBarShadow:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayImageContainerRight:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWTryPocketButton:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWTryPocketDivider:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mOrientation:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRight:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWFeatureLevel:Z

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mBubbleAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup3:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayRightMove:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private getStatusBarHeight()I
    .locals 2

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1050010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method private init()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const v8, 0x7f0f016e

    const v7, 0x7f0f0166

    const v6, 0x7f0f015d

    const v5, 0x7f040052

    .line 142
    const v3, 0x7f0f0154

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayLeft:Landroid/widget/RelativeLayout;

    .line 143
    const v3, 0x7f0f0158

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayImageContainerLeft:Landroid/widget/LinearLayout;

    .line 144
    const v3, 0x7f0f0157

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleLeft:Landroid/widget/ImageView;

    .line 145
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleLeft:Landroid/widget/ImageView;

    new-instance v4, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRightLongClick;

    invoke-direct {v4, p0, v9}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRightLongClick;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$1;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 147
    const v3, 0x7f0f0160

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayRight:Landroid/widget/RelativeLayout;

    .line 148
    const v3, 0x7f0f0163

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayImageContainerRight:Landroid/widget/LinearLayout;

    .line 149
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayImageContainerRight:Landroid/widget/LinearLayout;

    const/16 v4, 0x35

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 150
    const v3, 0x7f0f0162

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRight:Landroid/widget/ImageView;

    .line 151
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRight:Landroid/widget/ImageView;

    new-instance v4, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$CloseTrayOnClickListener;

    invoke-direct {v4, p0, v9}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$CloseTrayOnClickListener;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$1;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    const v3, 0x7f0f0169

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayRightMove:Landroid/widget/RelativeLayout;

    .line 154
    const v3, 0x7f0f016a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;

    .line 155
    const v3, 0x7f0f015a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayDragLeft:Landroid/widget/RelativeLayout;

    .line 156
    const v3, 0x7f0f016b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayDragHandler:Landroid/widget/RelativeLayout;

    .line 158
    const v3, 0x7f0f0153

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayWindowHelper:Landroid/widget/RelativeLayout;

    .line 160
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayWindowHelper:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, p0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 162
    const v3, 0x7f0f015b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup1:Landroid/widget/RelativeLayout;

    .line 163
    const v3, 0x7f0f0159

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWTryPocketButton:Landroid/widget/RelativeLayout;

    .line 164
    const v3, 0x7f0f0143

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWTryPocketDivider:Landroid/widget/RelativeLayout;

    .line 167
    const v3, 0x7f0f015f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue1:Landroid/widget/ImageView;

    .line 168
    const v3, 0x7f0f015e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopUpArrow1:Landroid/widget/ImageView;

    .line 169
    invoke-virtual {p0, v6}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayText1:Landroid/widget/TextView;

    .line 170
    const v3, 0x7f0f0164

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup2:Landroid/widget/RelativeLayout;

    .line 171
    const v3, 0x7f0f0168

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue2:Landroid/widget/ImageView;

    .line 172
    const v3, 0x7f0f0167

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopUpArrow2:Landroid/widget/ImageView;

    .line 173
    invoke-virtual {p0, v7}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayText2:Landroid/widget/TextView;

    .line 174
    const v3, 0x7f0f016c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup3:Landroid/widget/RelativeLayout;

    .line 175
    const v3, 0x7f0f0170

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue3:Landroid/widget/ImageView;

    .line 176
    const v3, 0x7f0f016f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopUpArrow3:Landroid/widget/ImageView;

    .line 177
    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayText3:Landroid/widget/TextView;

    .line 178
    const v3, 0x7f0f0155

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightShadow:Landroid/widget/ImageView;

    .line 179
    const v3, 0x7f0f0156

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayBarShadow:Landroid/widget/ImageView;

    .line 181
    const v3, 0x7f0f0171

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup4:Landroid/widget/RelativeLayout;

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mOrientation:I

    .line 185
    const v3, 0x7f040024

    invoke-static {p0, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 187
    const-string v3, "vibrator"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/SystemVibrator;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mVibrator:Landroid/os/SystemVibrator;

    .line 189
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWFeatureLevel:Z

    if-eqz v3, :cond_0

    .line 190
    invoke-static {p0, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 191
    .local v1, "myFadeAnimation":Landroid/view/animation/Animation;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue1:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 192
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue2:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 193
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue3:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 194
    const v3, 0x7f0f015c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayContent1:Landroid/widget/RelativeLayout;

    .line 195
    const v3, 0x7f0f0165

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayContent2:Landroid/widget/RelativeLayout;

    .line 196
    const v3, 0x7f0f016d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayContent3:Landroid/widget/RelativeLayout;

    .line 199
    .end local v1    # "myFadeAnimation":Landroid/view/animation/Animation;
    :cond_0
    invoke-virtual {p0, v6}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTryPop1Text:Landroid/widget/TextView;

    .line 200
    invoke-virtual {p0, v7}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTryPop2Text:Landroid/widget/TextView;

    .line 201
    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTryPop3Text:Landroid/widget/TextView;

    .line 202
    const v3, 0x7f0f0133

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTryPop4Text:Landroid/widget/TextView;

    .line 203
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTryPop1Text:Landroid/widget/TextView;

    new-instance v4, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v4}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 204
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTryPop2Text:Landroid/widget/TextView;

    new-instance v4, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v4}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 205
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTryPop3Text:Landroid/widget/TextView;

    new-instance v4, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v4}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 206
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTryPop4Text:Landroid/widget/TextView;

    new-instance v4, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v4}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 208
    const-string v3, "window"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mWindowManager:Landroid/view/WindowManager;

    .line 209
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 210
    .local v0, "displaySize":Landroid/graphics/Point;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 211
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 213
    :cond_1
    iget v3, v0, Landroid/graphics/Point;->y:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mDisplayHeight:I

    .line 215
    invoke-static {p0, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 216
    .local v2, "myFadeInAnimation":Landroid/view/animation/Animation;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue1:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 217
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue2:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 218
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue3:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 219
    return-void
.end method

.method private setHandleToCenter()V
    .locals 3

    .prologue
    .line 594
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;

    iget v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mDisplayHeight:I

    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->getStatusBarHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setY(F)V

    .line 595
    return-void
.end method


# virtual methods
.method public invalidTouch()V
    .locals 2

    .prologue
    .line 851
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mInvalidToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 852
    const v0, 0x7f08003a

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mInvalidToast:Landroid/widget/Toast;

    .line 854
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mInvalidToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 855
    return-void
.end method

.method public makeScreenForLandscape()V
    .locals 27

    .prologue
    .line 439
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mRotated:Z

    .line 440
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWFeatureLevel:Z

    move/from16 v24, v0

    if-eqz v24, :cond_2

    .line 441
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v24, -0x2

    const/16 v25, -0x2

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v4, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 442
    .local v4, "mTryMWTrayContent1Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a00ff

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 443
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a00f5

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 444
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayContent1:Landroid/widget/RelativeLayout;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 446
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayContent2:Landroid/widget/RelativeLayout;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 447
    .local v5, "mTryMWTrayContent2Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a00ff

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a00f3

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 449
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayContent2:Landroid/widget/RelativeLayout;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 451
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayContent3:Landroid/widget/RelativeLayout;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 452
    .local v6, "mTryMWTrayContent3Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a00ff

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayContent3:Landroid/widget/RelativeLayout;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 463
    .end local v4    # "mTryMWTrayContent1Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v5    # "mTryMWTrayContent2Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v6    # "mTryMWTrayContent3Params":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_0
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v24, -0x2

    const/16 v25, -0x2

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v7, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 465
    .local v7, "mTryMWTrayGuideCue1Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a0100

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 467
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a0101

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 469
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue1:Landroid/widget/ImageView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 471
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v24, -0x2

    const/16 v25, -0x2

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v8, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 473
    .local v8, "mTryMWTrayGuideCue2Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a0100

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 475
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a00fc

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v8, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 477
    const/16 v24, 0xb

    move/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue2:Landroid/widget/ImageView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 480
    new-instance v11, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v24, -0x2

    const/16 v25, -0x2

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v11, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 482
    .local v11, "mTryMWTrayPopUpArrow1Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a0102

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 484
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a00f8

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 486
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopUpArrow1:Landroid/widget/ImageView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 488
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v24, -0x2

    const/16 v25, -0x2

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v12, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 490
    .local v12, "mTryMWTrayPopUpArrow2Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a0102

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v12, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 492
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a00fb

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v12, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 494
    const/16 v24, 0xb

    move/from16 v0, v24

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 495
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopUpArrow2:Landroid/widget/ImageView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 497
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWFeatureLevel:Z

    move/from16 v24, v0

    if-nez v24, :cond_0

    .line 498
    new-instance v15, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v24, -0x2

    const/16 v25, -0x2

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v15, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 500
    .local v15, "mTryMWTrayPopup3Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a00ff

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v15, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 502
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a0103

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v15, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 504
    const/16 v24, 0xb

    move/from16 v0, v24

    invoke-virtual {v15, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 505
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup3:Landroid/widget/RelativeLayout;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 507
    .end local v15    # "mTryMWTrayPopup3Params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    new-instance v13, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v24, -0x2

    const/16 v25, -0x2

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v13, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 509
    .local v13, "mTryMWTrayPopUpArrow3Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a0102

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v13, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 511
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a0104

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v13, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 513
    const/16 v24, 0xb

    move/from16 v0, v24

    invoke-virtual {v13, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 514
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopUpArrow3:Landroid/widget/ImageView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 516
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v24, -0x2

    const/16 v25, -0x2

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v9, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 518
    .local v9, "mTryMWTrayGuideCue3Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a0100

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 520
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a0103

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 522
    const/16 v24, 0xb

    move/from16 v0, v24

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 523
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue3:Landroid/widget/ImageView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 525
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a00bd

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v21, v0

    .line 526
    .local v21, "textViewWidth1":I
    new-instance v17, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v24, -0x2

    move-object/from16 v0, v17

    move/from16 v1, v21

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 528
    .local v17, "mTryMWTrayText1LandParams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayText1:Landroid/widget/TextView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 530
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a00be

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v22, v0

    .line 531
    .local v22, "textViewWidth2":I
    new-instance v18, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v24, -0x2

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 533
    .local v18, "mTryMWTrayText2LandParams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayText2:Landroid/widget/TextView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 535
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a00bf

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v23, v0

    .line 536
    .local v23, "textViewWidth3":I
    new-instance v19, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v24, -0x2

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 538
    .local v19, "mTryMWTrayText3LandParams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayText3:Landroid/widget/TextView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 540
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup4:Landroid/widget/RelativeLayout;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    check-cast v16, Landroid/widget/RelativeLayout$LayoutParams;

    .line 541
    .local v16, "mTryMWTrayPopup4Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a00c3

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v16

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 542
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup4:Landroid/widget/RelativeLayout;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 544
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v24, -0x2

    const/16 v25, -0x2

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 546
    .local v10, "mTryMWTrayPocketParams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a00cb

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 547
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a00cc

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 548
    const/16 v24, 0xc

    move/from16 v0, v24

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 549
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWTryPocketButton:Landroid/widget/RelativeLayout;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 550
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTrayInRight:Z

    move/from16 v24, v0

    if-eqz v24, :cond_3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTrayClosed:Z

    move/from16 v24, v0

    if-nez v24, :cond_3

    .line 551
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayLeft:Landroid/widget/RelativeLayout;

    move-object/from16 v24, v0

    const/16 v25, 0x8

    invoke-virtual/range {v24 .. v25}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 552
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayRight:Landroid/widget/RelativeLayout;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 557
    :goto_1
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 558
    .local v3, "displaySize":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mWindowManager:Landroid/view/WindowManager;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v24

    if-eqz v24, :cond_1

    .line 559
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mWindowManager:Landroid/view/WindowManager;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 561
    :cond_1
    iget v0, v3, Landroid/graphics/Point;->y:I

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mDisplayHeight:I

    .line 562
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMoveClicked:Z

    move/from16 v24, v0

    if-eqz v24, :cond_4

    .line 563
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMoveHeightRate:F

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mDisplayHeight:I

    move/from16 v25, v0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->getStatusBarHeight()I

    move-result v26

    sub-int v25, v25, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/widget/ImageView;->getHeight()I

    move-result v26

    sub-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    mul-float v24, v24, v25

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v20, v0

    .line 564
    .local v20, "resetHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;

    move-object/from16 v24, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/widget/ImageView;->setY(F)V

    .line 568
    .end local v20    # "resetHeight":I
    :goto_2
    return-void

    .line 456
    .end local v3    # "displaySize":Landroid/graphics/Point;
    .end local v7    # "mTryMWTrayGuideCue1Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v8    # "mTryMWTrayGuideCue2Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v9    # "mTryMWTrayGuideCue3Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v10    # "mTryMWTrayPocketParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v11    # "mTryMWTrayPopUpArrow1Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v12    # "mTryMWTrayPopUpArrow2Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v13    # "mTryMWTrayPopUpArrow3Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v16    # "mTryMWTrayPopup4Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v17    # "mTryMWTrayText1LandParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v18    # "mTryMWTrayText2LandParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v19    # "mTryMWTrayText3LandParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v21    # "textViewWidth1":I
    .end local v22    # "textViewWidth2":I
    .end local v23    # "textViewWidth3":I
    :cond_2
    new-instance v14, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v24, -0x2

    const/16 v25, -0x2

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v14, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 457
    .local v14, "mTryMWTrayPopup1Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a00ff

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v14, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 458
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f0a00a0

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v14, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 459
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup1:Landroid/widget/RelativeLayout;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 460
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup2:Landroid/widget/RelativeLayout;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 554
    .end local v14    # "mTryMWTrayPopup1Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v7    # "mTryMWTrayGuideCue1Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v8    # "mTryMWTrayGuideCue2Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v9    # "mTryMWTrayGuideCue3Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v10    # "mTryMWTrayPocketParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v11    # "mTryMWTrayPopUpArrow1Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v12    # "mTryMWTrayPopUpArrow2Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v13    # "mTryMWTrayPopUpArrow3Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v16    # "mTryMWTrayPopup4Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v17    # "mTryMWTrayText1LandParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v18    # "mTryMWTrayText2LandParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v19    # "mTryMWTrayText3LandParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v21    # "textViewWidth1":I
    .restart local v22    # "textViewWidth2":I
    .restart local v23    # "textViewWidth3":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayLeft:Landroid/widget/RelativeLayout;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 555
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayLeft:Landroid/widget/RelativeLayout;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Landroid/widget/RelativeLayout;->setY(F)V

    goto/16 :goto_1

    .line 566
    .restart local v3    # "displaySize":Landroid/graphics/Point;
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->setHandleToCenter()V

    goto :goto_2
.end method

.method public makeScreenForPortrait()V
    .locals 26

    .prologue
    .line 307
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mRotated:Z

    .line 308
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWFeatureLevel:Z

    move/from16 v23, v0

    if-eqz v23, :cond_2

    .line 309
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v23, -0x2

    const/16 v24, -0x2

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v4, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 310
    .local v4, "mTryMWTrayContent1Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00f6

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00f5

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 312
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayContent1:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayContent2:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 315
    .local v5, "mTryMWTrayContent2Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00f6

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 316
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00f3

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 317
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayContent2:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayContent3:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 320
    .local v6, "mTryMWTrayContent3Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00f6

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 321
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayContent3:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 334
    .end local v4    # "mTryMWTrayContent1Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v5    # "mTryMWTrayContent2Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v6    # "mTryMWTrayContent3Params":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_0
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v23, -0x2

    const/16 v24, -0x2

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v7, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 337
    .local v7, "mTryMWTrayGuideCue1Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00f9

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 339
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00fa

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue1:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 343
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v23, -0x2

    const/16 v24, -0x2

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v8, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 346
    .local v8, "mTryMWTrayGuideCue2Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00f9

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 348
    const/16 v23, 0xb

    move/from16 v0, v23

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 349
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00fc

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v8, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 351
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue2:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 353
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v23, -0x2

    const/16 v24, -0x2

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 355
    .local v10, "mTryMWTrayPopUpArrow1Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00f7

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 357
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00f8

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 359
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopUpArrow1:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 361
    new-instance v11, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v23, -0x2

    const/16 v24, -0x2

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v11, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 363
    .local v11, "mTryMWTrayPopUpArrow2Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00f7

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 365
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00fb

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 368
    const/16 v23, 0xb

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 369
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopUpArrow2:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 370
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWFeatureLevel:Z

    move/from16 v23, v0

    if-nez v23, :cond_0

    .line 371
    new-instance v14, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v23, -0x2

    const/16 v24, -0x2

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v14, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 373
    .local v14, "mTryMWTrayPopup3Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00f6

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v14, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 375
    const/16 v23, 0xb

    move/from16 v0, v23

    invoke-virtual {v14, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 376
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup3:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 378
    .end local v14    # "mTryMWTrayPopup3Params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v23, -0x2

    const/16 v24, -0x2

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v12, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 380
    .local v12, "mTryMWTrayPopUpArrow3Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00f7

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v12, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00fe

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v12, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 384
    const/16 v23, 0xb

    move/from16 v0, v23

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 385
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopUpArrow3:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v12}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 387
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v23, -0x2

    const/16 v24, -0x2

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v9, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 389
    .local v9, "mTryMWTrayGuideCue3Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00f9

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 392
    const/16 v23, 0xb

    move/from16 v0, v23

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 393
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue3:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 395
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00ba

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v20, v0

    .line 396
    .local v20, "textViewWidth1":I
    new-instance v16, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v23, -0x2

    move-object/from16 v0, v16

    move/from16 v1, v20

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 398
    .local v16, "mTryMWTrayText1Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayText1:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 400
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00bb

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v21, v0

    .line 401
    .local v21, "textViewWidth2":I
    new-instance v17, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v23, -0x2

    move-object/from16 v0, v17

    move/from16 v1, v21

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 403
    .local v17, "mTryMWTrayText2Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayText2:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 405
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00bc

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v22, v0

    .line 406
    .local v22, "textViewWidth3":I
    new-instance v18, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v23, -0x2

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 408
    .local v18, "mTryMWTrayText3Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayText3:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 409
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup4:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    check-cast v15, Landroid/widget/RelativeLayout$LayoutParams;

    .line 410
    .local v15, "mTryMWTrayPopup4Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00c2

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v15, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup4:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 412
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTrayInRight:Z

    move/from16 v23, v0

    if-eqz v23, :cond_3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTrayClosed:Z

    move/from16 v23, v0

    if-nez v23, :cond_3

    .line 413
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayLeft:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 414
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayRight:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 420
    :goto_1
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 421
    .local v3, "displaySize":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mWindowManager:Landroid/view/WindowManager;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v23

    if-eqz v23, :cond_1

    .line 422
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mWindowManager:Landroid/view/WindowManager;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 424
    :cond_1
    iget v0, v3, Landroid/graphics/Point;->y:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mDisplayHeight:I

    .line 426
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMoveClicked:Z

    move/from16 v23, v0

    if-eqz v23, :cond_4

    .line 427
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMoveHeightRate:F

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mDisplayHeight:I

    move/from16 v24, v0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->getStatusBarHeight()I

    move-result v25

    sub-int v24, v24, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/ImageView;->getHeight()I

    move-result v25

    sub-int v24, v24, v25

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v19, v0

    .line 428
    .local v19, "resetHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setY(F)V

    .line 432
    .end local v19    # "resetHeight":I
    :goto_2
    return-void

    .line 324
    .end local v3    # "displaySize":Landroid/graphics/Point;
    .end local v7    # "mTryMWTrayGuideCue1Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v8    # "mTryMWTrayGuideCue2Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v9    # "mTryMWTrayGuideCue3Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v10    # "mTryMWTrayPopUpArrow1Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v11    # "mTryMWTrayPopUpArrow2Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v12    # "mTryMWTrayPopUpArrow3Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v15    # "mTryMWTrayPopup4Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v16    # "mTryMWTrayText1Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v17    # "mTryMWTrayText2Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v18    # "mTryMWTrayText3Params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v20    # "textViewWidth1":I
    .end local v21    # "textViewWidth2":I
    .end local v22    # "textViewWidth3":I
    :cond_2
    new-instance v13, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v23, -0x2

    const/16 v24, -0x2

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v13, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 327
    .local v13, "mTryMWTrayPopup1Params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00f6

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v13, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 329
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f0a00a0

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v13, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 331
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup1:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 332
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup2:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 416
    .end local v13    # "mTryMWTrayPopup1Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v7    # "mTryMWTrayGuideCue1Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v8    # "mTryMWTrayGuideCue2Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v9    # "mTryMWTrayGuideCue3Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v10    # "mTryMWTrayPopUpArrow1Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v11    # "mTryMWTrayPopUpArrow2Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v12    # "mTryMWTrayPopUpArrow3Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v15    # "mTryMWTrayPopup4Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v16    # "mTryMWTrayText1Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v17    # "mTryMWTrayText2Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v18    # "mTryMWTrayText3Params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v20    # "textViewWidth1":I
    .restart local v21    # "textViewWidth2":I
    .restart local v22    # "textViewWidth3":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayLeft:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 417
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayLeft:Landroid/widget/RelativeLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setY(F)V

    goto/16 :goto_1

    .line 430
    .restart local v3    # "displaySize":Landroid/graphics/Point;
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->setHandleToCenter()V

    goto :goto_2
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 227
    const/16 v0, 0x65

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->setResult(I)V

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->finish()V

    .line 229
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 289
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 292
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mOrientation:I

    .line 293
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mOrientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 294
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->makeScreenForPortrait()V

    .line 297
    :cond_0
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mOrientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 298
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->makeScreenForLandscape()V

    .line 300
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 114
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 117
    .local v1, "w":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 118
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    .line 119
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 120
    const v2, 0x7f03002e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->setContentView(I)V

    .line 121
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->init()V

    .line 123
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mOrientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->makeScreenForPortrait()V

    .line 127
    :cond_0
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mOrientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->makeScreenForLandscape()V

    .line 131
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWFeatureLevel:Z

    if-eqz v2, :cond_2

    .line 132
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup1:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 134
    :cond_2
    return-void
.end method

.method public onPause()V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v9, -0x1

    const/4 v8, 0x0

    const/4 v7, -0x2

    const/4 v6, 0x0

    .line 233
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 234
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->trayToRight:Z

    if-nez v3, :cond_0

    .line 237
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightShadow:Landroid/widget/ImageView;

    invoke-virtual {v3, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 238
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleLeft:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0200eb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 239
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleLeft:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 240
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayBarShadow:Landroid/widget/ImageView;

    const v4, 0x7f0200e6

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 241
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 244
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f0a00b3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 245
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayImageContainerLeft:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 247
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWTryPocketButton:Landroid/widget/RelativeLayout;

    const v4, 0x7f0200ec

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 248
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 251
    .local v1, "lp2":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f0a00cb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 252
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f0a00cc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 253
    const/16 v3, 0xc

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 254
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWTryPocketButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 255
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 258
    .local v2, "lp3":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWTryPocketDivider:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 261
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayLeft:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 262
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayImageContainerLeft:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 263
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleLeft:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 264
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayDragLeft:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 265
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayLeft:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 266
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayLeft:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 267
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mIvt:[B

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v5}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 268
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleLeft:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 269
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleLeft:Landroid/widget/ImageView;

    new-instance v4, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRightLongClick;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRightLongClick;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$1;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 270
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightShadow:Landroid/widget/ImageView;

    invoke-virtual {v3, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 273
    .end local v0    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v1    # "lp2":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v2    # "lp3":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 277
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 279
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->rightHandleMovementStarted:Z

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup4:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup4:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->r:Ljava/lang/Runnable;

    const-wide/16 v2, 0xdac

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 285
    :cond_0
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 835
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 847
    :cond_0
    :goto_0
    return v1

    .line 837
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTryPop1Text:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTryPop2Text:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTryPop3Text:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTryPop4Text:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 839
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayWindowHelper:Landroid/widget/RelativeLayout;

    if-ne p1, v0, :cond_0

    .line 841
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->invalidTouch()V

    goto :goto_0

    .line 835
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
