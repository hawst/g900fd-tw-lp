.class Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;
.super Ljava/lang/Object;
.source "SmartWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

.field final synthetic val$id:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;I)V
    .locals 0

    .prologue
    .line 531
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iput p2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->val$id:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 534
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mHasEditWindow:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 586
    :cond_0
    :goto_0
    return-void

    .line 538
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->isEnableMakePenWindow()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 542
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$400(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->val$id:I

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mSupportMultiInstance:Z
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$500(Lcom/sec/android/app/FlashBarService/SmartWindow;)Z

    move-result v9

    const/16 v10, 0x65

    invoke-virtual {v7, v8, v9, v10}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v3

    .line 543
    .local v3, "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 547
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    if-ne v7, v11, :cond_0

    .line 548
    invoke-interface {v3, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    .line 549
    .local v2, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 550
    .local v1, "displayFrame":Landroid/graphics/Rect;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$600(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    .line 552
    .local v4, "lp":Landroid/view/WindowManager$LayoutParams;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$700(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-virtual {v2, v7, v12}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 553
    .local v0, "aInfo":Landroid/content/pm/ActivityInfo;
    new-instance v5, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-direct {v5, v13}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    .line 554
    .local v5, "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    if-eqz v0, :cond_4

    iget v7, v0, Landroid/content/pm/ActivityInfo;->screenOrientation:I

    if-ne v7, v11, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$700(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v7, v7, Landroid/content/res/Configuration;->orientation:I

    if-eq v7, v13, :cond_3

    :cond_2
    iget v7, v0, Landroid/content/pm/ActivityInfo;->screenOrientation:I

    if-nez v7, :cond_4

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$700(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v7, v7, Landroid/content/res/Configuration;->orientation:I

    if-ne v7, v11, :cond_4

    .line 557
    :cond_3
    const/4 v6, 0x0

    .line 558
    .local v6, "temp":I
    iget v6, v4, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 559
    iget v7, v4, Landroid/view/WindowManager$LayoutParams;->height:I

    iput v7, v4, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 560
    iput v6, v4, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 562
    iget v6, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 563
    iget v7, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v7, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 564
    iput v6, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 566
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayHeight:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$800(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v6

    .line 567
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayWidth:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$900(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v8

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayHeight:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$802(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 568
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayWidth:I
    invoke-static {v7, v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$902(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 571
    .end local v6    # "temp":I
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-virtual {v7, v4}, Lcom/sec/android/app/FlashBarService/SmartWindow;->keepLaunchedWindowRatio(Landroid/view/WindowManager$LayoutParams;)V

    .line 573
    iget v7, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1000(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/graphics/Rect;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Rect;->left:I

    add-int/2addr v7, v8

    iput v7, v1, Landroid/graphics/Rect;->left:I

    .line 574
    iget v7, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1000(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/graphics/Rect;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Rect;->top:I

    add-int/2addr v7, v8

    iput v7, v1, Landroid/graphics/Rect;->top:I

    .line 575
    iget v7, v1, Landroid/graphics/Rect;->left:I

    iget v8, v4, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1000(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/graphics/Rect;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Rect;->left:I

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1000(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/graphics/Rect;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Rect;->right:I

    add-int/2addr v8, v9

    sub-int/2addr v7, v8

    iput v7, v1, Landroid/graphics/Rect;->right:I

    .line 576
    iget v7, v1, Landroid/graphics/Rect;->top:I

    iget v8, v4, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1000(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/graphics/Rect;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Rect;->top:I

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1000(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/graphics/Rect;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v8, v9

    sub-int/2addr v7, v8

    iput v7, v1, Landroid/graphics/Rect;->bottom:I

    .line 578
    const/16 v7, 0x800

    invoke-virtual {v5, v7, v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setOption(IZ)V

    .line 579
    invoke-virtual {v5, v1}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setBounds(Landroid/graphics/Rect;)V

    .line 581
    invoke-virtual {v2, v5}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 582
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$700(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 584
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->closeSmartWindow()V

    goto/16 :goto_0
.end method
