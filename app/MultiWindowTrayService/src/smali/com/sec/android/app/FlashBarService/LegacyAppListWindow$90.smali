.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 5615
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 5618
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/app/Dialog;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-ne v2, v5, :cond_1

    .line 5619
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->dismiss()V

    .line 5620
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_0

    .line 5621
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5623
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 5624
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentAppPosition:I
    invoke-static {v2, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15002(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 5625
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v3, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12602(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 5645
    :goto_0
    return-void

    .line 5630
    :cond_1
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 5631
    .local v1, "outMetrics":Landroid/util/DisplayMetrics;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 5632
    new-array v0, v6, [I

    .line 5633
    .local v0, "loc":[I
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 5634
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 5642
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mVibrator:Landroid/os/SystemVibrator;

    sget-object v3, Lcom/sec/android/app/FlashBarService/AppListController;->mDefaultIvt:[B

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v4}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 5643
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->makeHistoryBarDialog(I)V
    invoke-static {v2, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)V

    .line 5644
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startCollapseTimer()V
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    goto :goto_0

    .line 5639
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    aget v3, v0, v5

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$90;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbNailLayoutWidth:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentAppPosition:I
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15002(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    goto :goto_1

    .line 5634
    nop

    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
