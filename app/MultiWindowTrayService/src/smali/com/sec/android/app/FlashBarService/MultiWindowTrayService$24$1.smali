.class Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24$1;
.super Ljava/lang/Object;
.source "MultiWindowTrayService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;->onArrangeStateUpdate(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;)V
    .locals 0

    .prologue
    .line 1524
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24$1;->this$1:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1527
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24$1;->this$1:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$2400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1538
    :goto_0
    return-void

    .line 1531
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24$1;->this$1:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$1600(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24$1;->this$1:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1532
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24$1;->this$1:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # invokes: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->makeCenterBarWindow(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$2500(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;I)V

    .line 1534
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24$1;->this$1:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$1600(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24$1;->this$1:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1535
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24$1;->this$1:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->refreshWindow(I)V

    .line 1537
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24$1;->this$1:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->updateForeground()V

    goto :goto_0
.end method
