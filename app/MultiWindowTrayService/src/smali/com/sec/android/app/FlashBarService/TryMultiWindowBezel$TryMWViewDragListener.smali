.class Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;
.super Ljava/lang/Object;
.source "TryMultiWindowBezel.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TryMWViewDragListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)V
    .locals 0

    .prologue
    .line 535
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 18
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 539
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v4

    .line 540
    .local v4, "action":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getX()F

    move-result v13

    float-to-int v2, v13

    .line 541
    .local v2, "X":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getY()F

    move-result v13

    float-to-int v3, v13

    .line 543
    .local v3, "Y":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v13

    div-int/lit8 v11, v13, 0x2

    .line 544
    .local v11, "tryMWViewContainerHeightCenter":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v13

    div-int/lit8 v12, v13, 0x2

    .line 546
    .local v12, "tryMWViewContainerWidthCenter":I
    packed-switch v4, :pswitch_data_0

    .line 682
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v13, 0x1

    return v13

    .line 554
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isFirstTipShown:Z
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z

    move-result v13

    if-eqz v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isApp1DrawnSuccess:Z
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z

    move-result v13

    if-eqz v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDisplayWidth:I
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWTrayWidth:I
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1600(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v14

    sub-int/2addr v13, v14

    if-gt v2, v13, :cond_1

    .line 556
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    const/4 v14, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->app1DragArea:Z
    invoke-static {v13, v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1202(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Z)Z

    .line 557
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v13

    invoke-virtual {v13}, Landroid/support/v4/widget/DrawerLayout;->closeDrawers()V

    .line 558
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    .line 560
    .local v7, "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v13, 0x0

    iput v13, v7, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 561
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v13

    iput v13, v7, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 562
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 563
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 564
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder2:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v13

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 567
    .end local v7    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isSecondTipShown:Z
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z

    move-result v13

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isApp1Drawn:Z
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z

    move-result v13

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDisplayWidth:I
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWTrayWidth:I
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1600(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v14

    sub-int/2addr v13, v14

    if-ge v2, v13, :cond_0

    .line 568
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    const/4 v14, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->app2DragArea:Z
    invoke-static {v13, v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1802(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Z)Z

    .line 569
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v13

    invoke-virtual {v13}, Landroid/support/v4/widget/DrawerLayout;->closeDrawers()V

    .line 570
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder3:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v13

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 571
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_2

    .line 572
    if-le v3, v11, :cond_3

    .line 573
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    .line 575
    .restart local v7    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    iput v11, v7, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 576
    iput v11, v7, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 577
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 578
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 579
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_DOWN:I
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v14

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFrameLocation:I
    invoke-static {v13, v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2102(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;I)I

    .line 591
    .end local v7    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v13

    const/4 v14, 0x2

    if-ne v13, v14, :cond_0

    .line 592
    if-le v2, v12, :cond_4

    .line 593
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    .line 595
    .restart local v7    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v13, 0x0

    iput v13, v7, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 596
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v13

    iput v13, v7, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 597
    iput v12, v7, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 598
    iput v12, v7, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 599
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 600
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 601
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_DOWN:I
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v14

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFrameLocation:I
    invoke-static {v13, v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2102(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;I)I

    goto/16 :goto_0

    .line 581
    .end local v7    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    .line 583
    .restart local v7    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v13, 0x0

    iput v13, v7, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 584
    iput v11, v7, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 585
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 586
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 587
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_UP:I
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v14

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFrameLocation:I
    invoke-static {v13, v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2102(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;I)I

    goto/16 :goto_1

    .line 603
    .end local v7    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 605
    .local v5, "l":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v13, 0x0

    iput v13, v5, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 606
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v13

    iput v13, v5, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 607
    const/4 v13, 0x0

    iput v13, v5, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 608
    iput v12, v5, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 609
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 610
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 611
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_UP:I
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v14

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFrameLocation:I
    invoke-static {v13, v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2102(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;I)I

    goto/16 :goto_0

    .line 618
    .end local v5    # "l":Landroid/widget/FrameLayout$LayoutParams;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 619
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isApp1DrawnSuccess:Z
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 620
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->app1DragArea:Z
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 621
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    const/4 v14, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isApp1DrawnSuccess:Z
    invoke-static {v13, v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1402(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Z)Z

    .line 622
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    const v14, 0x7f020084

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 624
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelGridView:Landroid/widget/GridView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2500(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/GridView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 625
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v13

    const/4 v14, 0x4

    invoke-virtual {v13, v14}, Landroid/support/v4/widget/DrawerLayout;->setVisibility(I)V

    .line 626
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHandle:Landroid/os/Handler;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/os/Handler;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->openDrawerRunnable:Ljava/lang/Runnable;
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2600(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Ljava/lang/Runnable;

    move-result-object v14

    const-wide/16 v16, 0x320

    move-wide/from16 v0, v16

    invoke-virtual {v13, v14, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 629
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isSecondTipShown:Z
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z

    move-result v13

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isApp1Drawn:Z
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 630
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    const/4 v14, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isApp2Drawn:Z
    invoke-static {v13, v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2802(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Z)Z

    .line 631
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->app2DragArea:Z
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1800(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 632
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewApp2:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 633
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v13

    invoke-virtual {v13}, Landroid/support/v4/widget/DrawerLayout;->closeDrawers()V

    .line 634
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v13

    const/4 v14, 0x2

    if-ne v13, v14, :cond_6

    .line 635
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewAppViewer:Landroid/widget/LinearLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/LinearLayout;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 636
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrameLinearLayout:Landroid/widget/LinearLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/LinearLayout;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 638
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout$LayoutParams;

    .line 640
    .local v10, "mTryMWViewHomeParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v13

    iput v13, v10, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 641
    iput v12, v10, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 642
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13, v10}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 644
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewApp2:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout$LayoutParams;

    .line 646
    .local v6, "mTryMWViewApp2Params":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v13

    iput v13, v6, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 647
    iput v12, v6, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 648
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewApp2:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13, v6}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 650
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout$LayoutParams;

    .line 652
    .local v9, "mTryMWViewFocusFrameParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v13

    iput v13, v9, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 653
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainerWidthCenter:I
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v13

    iput v13, v9, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 654
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13, v9}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 656
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout$LayoutParams;

    .line 658
    .local v8, "mTryMWViewFocusFrame2Params":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v13

    iput v13, v8, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 660
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainerWidthCenter:I
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v13

    iput v13, v8, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 661
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13, v8}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 664
    .end local v6    # "mTryMWViewApp2Params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v8    # "mTryMWViewFocusFrame2Params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v9    # "mTryMWViewFocusFrameParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v10    # "mTryMWViewHomeParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFrameLocation:I
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_UP:I
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v14

    if-ne v13, v14, :cond_8

    .line 665
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 666
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mBroderThickness:I
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3600(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v14

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlAdjustMargin:I
    invoke-static {v13, v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3502(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;I)I

    .line 671
    :cond_7
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewApp2:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 672
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 673
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup4:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3800(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/view/animation/Animation;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 674
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder4:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3900(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 675
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v13

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/support/v4/widget/DrawerLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 667
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFrameLocation:I
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_DOWN:I
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v14

    if-ne v13, v14, :cond_7

    .line 668
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 669
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mBroderThickness:I
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3600(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v14

    neg-int v14, v14

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlAdjustMargin:I
    invoke-static {v13, v14}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$3502(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;I)I

    goto :goto_2

    .line 546
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
