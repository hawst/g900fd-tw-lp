.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->hideAppEditList(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 4860
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 9
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    const v8, 0x7f080001

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 4862
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeAppListEditWindow()V

    .line 4863
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->initAppListEditWindow()V

    .line 4865
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200bb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 4866
    .local v0, "add":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0029

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v2, v3

    .line 4867
    .local v2, "paddingTop":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a002a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v1, v3

    .line 4869
    .local v1, "paddingBottom":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v7, v0, v7, v7}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 4870
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn_h:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v7, v0, v7, v7}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 4871
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v6, v2, v6, v1}, Landroid/widget/Button;->setPadding(IIII)V

    .line 4872
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn_h:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v6, v2, v6, v1}, Landroid/widget/Button;->setPadding(IIII)V

    .line 4873
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 4874
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn_h:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 4875
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 4876
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn_h:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 4877
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/TextView;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 4878
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4879
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditBtn:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v3

    const v4, 0x7f020034

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 4881
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOnlyEditBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 4882
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOnlyEditBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 4885
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 4886
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateItem:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 4887
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateBtn:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 4889
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->misAvailableHelpHub:Z
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$14000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 4890
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 4891
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpItem:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 4892
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpBtn:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 4895
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v4, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I
    invoke-static {v3, v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5602(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 4896
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2402(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 4897
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListRelayout(Z)V

    .line 4898
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iput-boolean v6, v3, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbAnimating:Z

    .line 4899
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->hideAppEditPost()V

    .line 4900
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListHorizontal:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->updateAppListForScrollView(Ljava/util/List;Z)V

    .line 4901
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListVertical:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->updateAppListForScrollView(Ljava/util/List;Z)V

    .line 4903
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 4904
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 4906
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbAnimating:Z

    .line 4907
    return-void
.end method
