.class final Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$ViewHolder;
.super Ljava/lang/Object;
.source "PopupViewEditWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ViewHolder"
.end annotation


# instance fields
.field iconView:Landroid/widget/ImageView;

.field labelView:Landroid/widget/TextView;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "mAppClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$ViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$ViewHolder;->labelView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1
    .param p1, "mAppTouchListener"    # Landroid/view/View$OnTouchListener;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$ViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$ViewHolder;->labelView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 107
    return-void
.end method
