.class Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$15;
.super Landroid/content/BroadcastReceiver;
.source "MultiWindowTrayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 0

    .prologue
    .line 403
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$15;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 406
    const-string v3, "MultiWindowTrayService"

    const-string v4, "Knox Setup Complete"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 409
    .local v0, "intentExtra":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 410
    const-string v3, "userid"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 412
    .local v1, "knoxUserId":I
    const/16 v3, 0x64

    if-lt v1, v3, :cond_0

    .line 413
    new-instance v2, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    const/4 v3, 0x0

    invoke-direct {v2, v3, p1}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 414
    .local v2, "prefs":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    invoke-virtual {v2, v1}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->removeAllUserData(I)V

    .line 417
    .end local v1    # "knoxUserId":I
    .end local v2    # "prefs":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    :cond_0
    return-void
.end method
