.class Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$1;
.super Ljava/lang/Object;
.source "MultiWindowTrayInfo.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->generateFlashBarList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V
    .locals 0

    .prologue
    .line 499
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$1;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)I
    .locals 5
    .param p1, "o1"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .param p2, "o2"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .prologue
    .line 501
    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    .line 502
    .local v0, "t1":Ljava/lang/CharSequence;
    invoke-virtual {p2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    .line 503
    .local v1, "t2":Ljava/lang/CharSequence;
    sget-object v2, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    return v2
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 499
    check-cast p1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$1;->compare(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)I

    move-result v0

    return v0
.end method
