.class Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$2;
.super Landroid/os/Handler;
.source "PopupViewEditWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)V
    .locals 0

    .prologue
    .line 302
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v4, 0xf

    const/4 v2, 0x0

    .line 304
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 323
    :cond_0
    :goto_0
    return-void

    .line 306
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mAppLongClickListener:Landroid/view/View$OnLongClickListener;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mCurrentDownView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$200(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/View$OnLongClickListener;->onLongClick(Landroid/view/View;)Z

    goto :goto_0

    .line 309
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mPressArrowbutton:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$300(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mEditScrollView:Landroid/widget/HorizontalScrollView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$100(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)Landroid/widget/HorizontalScrollView;

    move-result-object v0

    const/4 v1, -0x5

    invoke-virtual {v0, v1, v2}, Landroid/widget/HorizontalScrollView;->scrollBy(II)V

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mTimerHandler:Landroid/os/Handler;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->checkCanScroll()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$400(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)V

    goto :goto_0

    .line 316
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mPressArrowbutton:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$300(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mEditScrollView:Landroid/widget/HorizontalScrollView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$100(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)Landroid/widget/HorizontalScrollView;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/widget/HorizontalScrollView;->scrollBy(II)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mTimerHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->checkCanScroll()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$400(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)V

    goto :goto_0

    .line 304
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_1
        0x1 -> :sswitch_2
        0xca -> :sswitch_0
    .end sparse-switch
.end method
