.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;
.super Landroid/view/View$DragShadowBuilder;
.source "LegacyAppListWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->onLongClick(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;Landroid/view/View;)V
    .locals 0
    .param p2, "x0"    # Landroid/view/View;

    .prologue
    .line 4507
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    invoke-direct {p0, p2}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onDrawShadow(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 4515
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getCurrentAppList()Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const v2, 0x7f0f0059

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 4516
    .local v0, "view":Landroid/view/View;
    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 4517
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 4518
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 4519
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4520
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->isLaunchingBlockedItem(I)Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4521
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 4522
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLunchBlock:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 4525
    :cond_0
    return-void
.end method

.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 2
    .param p1, "shadowSize"    # Landroid/graphics/Point;
    .param p2, "shadowTouchPoint"    # Landroid/graphics/Point;

    .prologue
    .line 4509
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShadowWidth:I
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4902(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 4510
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShadowHeight:I
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5002(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 4511
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShadowWidth:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShadowHeight:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 4512
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShadowWidth:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v0

    div-int/lit8 v0, v0, 0x4

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShadowHeight:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    div-int/lit8 v1, v1, 0x4

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 4513
    return-void
.end method
