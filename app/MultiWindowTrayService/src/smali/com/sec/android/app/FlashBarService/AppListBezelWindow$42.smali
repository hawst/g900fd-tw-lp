.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$42;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->createConfirmDialog(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 3458
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 3461
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDelIndex:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbFromEdit:Z
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->removeTemplate(IZ)V

    .line 3462
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbFromEdit:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3463
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->updateEditListChanged()V

    .line 3467
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissConfirmDialog()V

    .line 3468
    return-void

    .line 3465
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListRelayout(Z)V

    goto :goto_0
.end method
