.class public Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;
.super Ljava/lang/Object;
.source "RecentTasksLoader.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDefaultIconBackground:Landroid/graphics/Bitmap;

.field private mDefaultThumbnailBackground:Landroid/graphics/Bitmap;

.field private mFirstScreenful:Z

.field private mFirstTaskLock:Ljava/lang/Object;

.field private mFocusedZone:I

.field private mHandler:Landroid/os/Handler;

.field private mIconDpi:I

.field private mLoadResumeOnly:Z

.field private mLoadedTasks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/FlashBarService/recent/TaskDescription;",
            ">;"
        }
    .end annotation
.end field

.field private mNumTasksInFirstScreenful:I

.field mPreloadTasksRunnable:Ljava/lang/Runnable;

.field private mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

.field private mState:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;

.field private mTaskLoader:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/FlashBarService/recent/TaskDescription;",
            ">;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mThumbnailLoader:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Lcom/sec/android/app/FlashBarService/recent/TaskDescription;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mFirstTaskLock:Ljava/lang/Object;

    .line 86
    const v3, 0x7fffffff

    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mNumTasksInFirstScreenful:I

    .line 92
    sget-object v3, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;->CANCELLED:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mState:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;

    .line 93
    iput v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mFocusedZone:I

    .line 94
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mLoadResumeOnly:Z

    .line 267
    new-instance v3, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$1;-><init>(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mPreloadTasksRunnable:Ljava/lang/Runnable;

    .line 105
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mContext:Landroid/content/Context;

    .line 106
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mHandler:Landroid/os/Handler;

    .line 108
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 111
    .local v2, "res":Landroid/content/res/Resources;
    const v3, 0x7f090002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 112
    .local v1, "isTablet":Z
    if-eqz v1, :cond_0

    .line 113
    const-string v3, "activity"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 115
    .local v0, "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getLauncherLargeIconDensity()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mIconDpi:I

    .line 122
    .end local v0    # "activityManager":Landroid/app/ActivityManager;
    :goto_0
    const v3, 0x7f090005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mLoadResumeOnly:Z

    .line 123
    return-void

    .line 117
    :cond_0
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mIconDpi:I

    goto :goto_0
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mFirstScreenful:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;Landroid/content/ComponentName;Landroid/content/pm/ActivityInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;
    .param p1, "x1"    # Landroid/content/ComponentName;
    .param p2, "x2"    # Landroid/content/pm/ActivityInfo;

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->isCurrentHomeActivity(Landroid/content/ComponentName;Landroid/content/pm/ActivityInfo;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mNumTasksInFirstScreenful:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;)Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;
    .param p1, "x1"    # Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mState:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;

    return-object p1
.end method

.method private cancelLoadingThumbnailsAndIcons()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mTaskLoader:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mTaskLoader:Landroid/os/AsyncTask;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 314
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mTaskLoader:Landroid/os/AsyncTask;

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mThumbnailLoader:Landroid/os/AsyncTask;

    if-eqz v0, :cond_1

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mThumbnailLoader:Landroid/os/AsyncTask;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 318
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mThumbnailLoader:Landroid/os/AsyncTask;

    .line 320
    :cond_1
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mLoadedTasks:Ljava/util/ArrayList;

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    if-eqz v0, :cond_2

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->onTaskLoadingCancelled()V

    .line 324
    :cond_2
    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mFirstScreenful:Z

    .line 325
    sget-object v0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;->CANCELLED:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mState:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;

    .line 326
    return-void
.end method

.method private getFullResIcon(Landroid/content/pm/ResolveInfo;Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "info"    # Landroid/content/pm/ResolveInfo;
    .param p2, "packageManager"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 253
    :try_start_0
    iget-object v3, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p2, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 258
    .local v2, "resources":Landroid/content/res/Resources;
    :goto_0
    if-eqz v2, :cond_0

    .line 259
    iget-object v3, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v3}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    move-result v1

    .line 260
    .local v1, "iconId":I
    if-eqz v1, :cond_0

    .line 261
    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->getFullResIcon(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 264
    .end local v1    # "iconId":I
    :goto_1
    return-object v3

    .line 255
    .end local v2    # "resources":Landroid/content/res/Resources;
    :catch_0
    move-exception v0

    .line 256
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    .restart local v2    # "resources":Landroid/content/res/Resources;
    goto :goto_0

    .line 264
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->getFullResDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    goto :goto_1
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 98
    const-class v1, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->sInstance:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    if-nez v0, :cond_0

    .line 99
    new-instance v0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->sInstance:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    .line 101
    :cond_0
    sget-object v0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->sInstance:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private isCurrentHomeActivity(Landroid/content/ComponentName;Landroid/content/pm/ActivityInfo;)Z
    .locals 4
    .param p1, "component"    # Landroid/content/ComponentName;
    .param p2, "homeInfo"    # Landroid/content/pm/ActivityInfo;

    .prologue
    const/4 v1, 0x0

    .line 172
    if-nez p2, :cond_0

    .line 173
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 174
    .local v0, "pm":Landroid/content/pm/PackageManager;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "android.intent.category.HOME"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object p2

    .line 177
    .end local v0    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    if-eqz p2, :cond_1

    iget-object v2, p2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private loadThumbnailsAndIconsInBackground(Ljava/util/concurrent/BlockingQueue;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/sec/android/app/FlashBarService/recent/TaskDescription;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 563
    .local p1, "tasksWaitingForThumbnails":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Lcom/sec/android/app/FlashBarService/recent/TaskDescription;>;"
    new-instance v0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$4;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$4;-><init>(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mThumbnailLoader:Landroid/os/AsyncTask;

    .line 606
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mThumbnailLoader:Landroid/os/AsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 607
    return-void
.end method


# virtual methods
.method public cancelLoadingThumbnailsAndIcons(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)V
    .locals 1
    .param p1, "caller"    # Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    .prologue
    .line 305
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    if-ne v0, p1, :cond_0

    .line 306
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->cancelLoadingThumbnailsAndIcons()V

    .line 308
    :cond_0
    return-void
.end method

.method public cancelPreloadingRecentTasksList()V
    .locals 2

    .prologue
    .line 298
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->cancelLoadingThumbnailsAndIcons()V

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mPreloadTasksRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 300
    return-void
.end method

.method createTaskDescription(IILandroid/content/Intent;Landroid/content/ComponentName;Ljava/lang/CharSequence;)Lcom/sec/android/app/FlashBarService/recent/TaskDescription;
    .locals 11
    .param p1, "taskId"    # I
    .param p2, "persistentTaskId"    # I
    .param p3, "baseIntent"    # Landroid/content/Intent;
    .param p4, "origActivity"    # Landroid/content/ComponentName;
    .param p5, "description"    # Ljava/lang/CharSequence;

    .prologue
    .line 185
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8, p3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 186
    .local v8, "intent":Landroid/content/Intent;
    if-eqz p4, :cond_0

    .line 187
    invoke-virtual {v8, p4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 189
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    .line 190
    .local v9, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v8}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const v2, -0x200001

    and-int/2addr v1, v2

    const/high16 v2, 0x10000000

    or-int/2addr v1, v2

    invoke-virtual {v8, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 192
    const/4 v1, 0x0

    invoke-virtual {v9, v8, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    .line 193
    .local v3, "resolveInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v3, :cond_1

    .line 194
    iget-object v7, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 195
    .local v7, "info":Landroid/content/pm/ActivityInfo;
    invoke-virtual {v7, v9}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    .line 197
    .local v10, "title":Ljava/lang/String;
    if-eqz v10, :cond_1

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 201
    new-instance v0, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    iget-object v5, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move v1, p1

    move v2, p2

    move-object v4, p3

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;-><init>(IILandroid/content/pm/ResolveInfo;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 204
    .local v0, "item":Lcom/sec/android/app/FlashBarService/recent/TaskDescription;
    invoke-virtual {v0, v10}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->setLabel(Ljava/lang/CharSequence;)V

    .line 211
    .end local v0    # "item":Lcom/sec/android/app/FlashBarService/recent/TaskDescription;
    .end local v7    # "info":Landroid/content/pm/ActivityInfo;
    .end local v10    # "title":Ljava/lang/String;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDefaultIcon()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 154
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mDefaultIconBackground:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x1050000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 157
    .local v0, "defaultIconSize":I
    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mIconDpi:I

    mul-int/2addr v2, v0

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    div-int v1, v2, v3

    .line 158
    .local v1, "iconSize":I
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mDefaultIconBackground:Landroid/graphics/Bitmap;

    .line 160
    .end local v0    # "defaultIconSize":I
    .end local v1    # "iconSize":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mDefaultIconBackground:Landroid/graphics/Bitmap;

    return-object v2
.end method

.method public getDefaultThumbnail()Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    .line 137
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mDefaultThumbnailBackground:Landroid/graphics/Bitmap;

    if-nez v4, :cond_0

    .line 139
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x1050002

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 141
    .local v3, "thumbnailWidth":I
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x1050001

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 143
    .local v2, "thumbnailHeight":I
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060002

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 145
    .local v1, "color":I
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v2, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mDefaultThumbnailBackground:Landroid/graphics/Bitmap;

    .line 147
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mDefaultThumbnailBackground:Landroid/graphics/Bitmap;

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 148
    .local v0, "c":Landroid/graphics/Canvas;
    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 150
    .end local v0    # "c":Landroid/graphics/Canvas;
    .end local v1    # "color":I
    .end local v2    # "thumbnailHeight":I
    .end local v3    # "thumbnailWidth":I
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mDefaultThumbnailBackground:Landroid/graphics/Bitmap;

    return-object v4
.end method

.method getFullResDefaultActivityIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 238
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10d0000

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->getFullResIcon(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method getFullResIcon(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "iconId"    # I

    .prologue
    .line 244
    :try_start_0
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mIconDpi:I

    invoke-virtual {p1, p2, v1}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 246
    :goto_0
    return-object v1

    .line 245
    :catch_0
    move-exception v0

    .line 246
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->getFullResDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0
.end method

.method public getLoadedTasks()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/FlashBarService/recent/TaskDescription;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mLoadedTasks:Ljava/util/ArrayList;

    return-object v0
.end method

.method getRunningTasks()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/ActivityManager$RunningTaskInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 612
    .local v1, "focusedRunningTasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v0, 0x0

    .line 613
    .local v0, "flag":I
    iget-boolean v6, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mLoadResumeOnly:Z

    if-eqz v6, :cond_0

    .line 614
    const/4 v0, 0x1

    .line 616
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mContext:Landroid/content/Context;

    const-string v7, "multiwindow_facade"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 617
    .local v3, "multiWindowFacade":Lcom/samsung/android/multiwindow/MultiWindowFacade;
    const/16 v6, 0x3e8

    invoke-virtual {v3, v6, v0}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 619
    .local v5, "runningTasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-nez v5, :cond_2

    .line 620
    const/4 v1, 0x0

    .line 631
    .end local v1    # "focusedRunningTasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_1
    return-object v1

    .line 623
    .restart local v1    # "focusedRunningTasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 624
    .local v4, "rt":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-boolean v6, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mLoadResumeOnly:Z

    if-eqz v6, :cond_4

    iget-boolean v6, v4, Landroid/app/ActivityManager$RunningTaskInfo;->isHomeType:Z

    if-nez v6, :cond_1

    .line 627
    :cond_4
    iget v6, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mFocusedZone:I

    iget-object v7, v4, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v7}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v7

    if-ne v6, v7, :cond_3

    .line 628
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public isFirstScreenful()Z
    .locals 1

    .prologue
    .line 168
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mFirstScreenful:Z

    return v0
.end method

.method public loadTasksInBackground()V
    .locals 1

    .prologue
    .line 425
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->loadTasksInBackground(Z)V

    .line 426
    return-void
.end method

.method public loadTasksInBackground(Z)V
    .locals 4
    .param p1, "zeroeth"    # Z

    .prologue
    .line 428
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mState:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;

    sget-object v2, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;->CANCELLED:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;

    if-eq v1, v2, :cond_0

    .line 556
    :goto_0
    return-void

    .line 431
    :cond_0
    sget-object v1, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;->LOADING:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mState:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;

    .line 432
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mFirstScreenful:Z

    .line 434
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    .line 436
    .local v0, "tasksWaitingForThumbnails":Ljava/util/concurrent/LinkedBlockingQueue;, "Ljava/util/concurrent/LinkedBlockingQueue<Lcom/sec/android/app/FlashBarService/recent/TaskDescription;>;"
    new-instance v1, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;-><init>(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;Ljava/util/concurrent/LinkedBlockingQueue;)V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mTaskLoader:Landroid/os/AsyncTask;

    .line 554
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mTaskLoader:Landroid/os/AsyncTask;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v3}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 555
    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->loadThumbnailsAndIconsInBackground(Ljava/util/concurrent/BlockingQueue;)V

    goto :goto_0
.end method

.method loadThumbnailAndIcon(Lcom/sec/android/app/FlashBarService/recent/TaskDescription;)V
    .locals 7
    .param p1, "td"    # Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    .prologue
    .line 215
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mContext:Landroid/content/Context;

    const-string v6, "activity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 217
    .local v0, "am":Landroid/app/ActivityManager;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 218
    .local v2, "pm":Landroid/content/pm/PackageManager;
    iget v5, p1, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->persistentTaskId:I

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getTaskThumbnail(I)Landroid/app/ActivityManager$TaskThumbnail;

    move-result-object v3

    .line 219
    .local v3, "taskThumbnail":Landroid/app/ActivityManager$TaskThumbnail;
    if-eqz v3, :cond_1

    iget-object v4, v3, Landroid/app/ActivityManager$TaskThumbnail;->mainThumbnail:Landroid/graphics/Bitmap;

    .line 220
    .local v4, "thumbnail":Landroid/graphics/Bitmap;
    :goto_0
    iget-object v5, p1, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->resolveInfo:Landroid/content/pm/ResolveInfo;

    invoke-direct {p0, v5, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->getFullResIcon(Landroid/content/pm/ResolveInfo;Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 224
    .local v1, "icon":Landroid/graphics/drawable/Drawable;
    monitor-enter p1

    .line 225
    if-eqz v4, :cond_2

    .line 226
    :try_start_0
    invoke-virtual {p1, v4}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->setThumbnail(Landroid/graphics/Bitmap;)V

    .line 230
    :goto_1
    if-eqz v1, :cond_0

    .line 231
    invoke-virtual {p1, v1}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 233
    :cond_0
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->setLoaded(Z)V

    .line 234
    monitor-exit p1

    .line 235
    return-void

    .line 219
    .end local v1    # "icon":Landroid/graphics/drawable/Drawable;
    .end local v4    # "thumbnail":Landroid/graphics/Bitmap;
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 228
    .restart local v1    # "icon":Landroid/graphics/drawable/Drawable;
    .restart local v4    # "thumbnail":Landroid/graphics/Bitmap;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->getDefaultThumbnail()Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->setThumbnail(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 234
    :catchall_0
    move-exception v5

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 277
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v0, v1, 0xff

    .line 278
    .local v0, "action":I
    if-nez v0, :cond_1

    .line 279
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->preloadRecentTasksList()V

    .line 290
    :cond_0
    :goto_0
    const/4 v1, 0x0

    return v1

    .line 280
    :cond_1
    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 281
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->cancelPreloadingRecentTasksList()V

    goto :goto_0

    .line 282
    :cond_2
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 284
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mPreloadTasksRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 285
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 286
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->cancelLoadingThumbnailsAndIcons()V

    goto :goto_0
.end method

.method public preloadRecentTasksList()V
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mPreloadTasksRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 295
    return-void
.end method

.method public setFocusedZone(I)V
    .locals 0
    .param p1, "focusZone"    # I

    .prologue
    .line 133
    iput p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mFocusedZone:I

    .line 134
    return-void
.end method

.method public setRecentsPanel(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)V
    .locals 1
    .param p1, "newRecentsPanel"    # Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;
    .param p2, "caller"    # Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    .prologue
    .line 127
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    if-ne v0, p2, :cond_1

    .line 128
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    .line 130
    :cond_1
    return-void
.end method
