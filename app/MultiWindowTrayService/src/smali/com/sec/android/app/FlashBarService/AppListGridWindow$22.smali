.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$22;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;->setFlashBarAnimation(Landroid/view/animation/Animation;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

.field final synthetic val$finishtray:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)V
    .locals 0

    .prologue
    .line 2795
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$22;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iput-boolean p2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$22;->val$finishtray:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 2797
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$22;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->hideTraybarHelpPopup()V

    .line 2798
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$22;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$6500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2799
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$22;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->updateForeground()V

    .line 2800
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$22;->val$finishtray:Z

    if-nez v0, :cond_0

    .line 2801
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$22;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$22;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListPosition(IZZ)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;IZZ)V

    .line 2802
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 2803
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 2804
    return-void
.end method
