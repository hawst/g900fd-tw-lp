.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 4640
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 4645
    const/4 v0, 0x0

    .line 4646
    .local v0, "anim":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOpenPocketTray:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 4647
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHelphubVersion:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-static {}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isUsaFeature()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHelphubVersion:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    if-nez v1, :cond_2

    .line 4648
    :cond_1
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .end local v0    # "anim":Landroid/view/animation/Animation;
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const v6, 0x3f4ccccd    # 0.8f

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 4652
    .restart local v0    # "anim":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 4656
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOpenPocketTray:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 4669
    :goto_1
    new-instance v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80$1;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4680
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 4681
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startCollapseTimer()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    .line 4682
    return-void

    .line 4654
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04004a

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 4658
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHelphubVersion:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-static {}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isUsaFeature()Z

    move-result v1

    if-nez v1, :cond_5

    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHelphubVersion:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    if-nez v1, :cond_6

    .line 4659
    :cond_5
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .end local v0    # "anim":Landroid/view/animation/Animation;
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const v8, 0x3f4ccccd    # 0.8f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 4663
    .restart local v0    # "anim":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 4667
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOpenPocketTray:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    goto :goto_1

    .line 4665
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040032

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_2
.end method
