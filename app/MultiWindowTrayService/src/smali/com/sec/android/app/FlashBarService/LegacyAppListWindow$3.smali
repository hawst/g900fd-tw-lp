.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$3;
.super Landroid/view/OrientationEventListener;
.source "LegacyAppListWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 462
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 5
    .param p1, "angle"    # I

    .prologue
    const/4 v4, 0x1

    .line 465
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 466
    .local v1, "display":Landroid/view/Display;
    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 467
    .local v0, "degrees":I
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastDegreesForPosition:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    if-eq v2, v0, :cond_1

    .line 468
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v3

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getAppListPosition(I)I
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 469
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v3

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListPosition(IZZ)V
    invoke-static {v2, v3, v4, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZZ)V

    .line 470
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 471
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->displayAppListEditWindow(IZ)V

    .line 473
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastDegreesForPosition:I
    invoke-static {v2, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 475
    :cond_1
    return-void
.end method
