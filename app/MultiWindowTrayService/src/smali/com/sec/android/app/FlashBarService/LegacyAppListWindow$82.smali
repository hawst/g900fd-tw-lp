.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 4744
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v11, 0x0

    .line 4747
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 4808
    :cond_0
    :goto_0
    return-void

    .line 4750
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->cancelCollapseTimer()V
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    .line 4751
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/16 v8, 0x64

    invoke-virtual {v7, v8, v10}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v5

    .line 4752
    .local v5, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 4753
    .local v4, "resumedInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 4754
    .local v6, "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    if-eqz v7, :cond_2

    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v7}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getType()I

    move-result v7

    if-ne v7, v9, :cond_2

    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v7}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v7

    if-eqz v7, :cond_2

    .line 4757
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4758
    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v7}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v7

    if-ne v7, v10, :cond_3

    .line 4759
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v8, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getIconByRunningTaskInfo(Landroid/app/ActivityManager$RunningTaskInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 4760
    :cond_3
    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v7}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v7

    const/16 v8, 0xc

    if-ne v7, v8, :cond_2

    .line 4761
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v8, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getIconByRunningTaskInfo(Landroid/app/ActivityManager$RunningTaskInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 4766
    .end local v6    # "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    if-le v7, v9, :cond_7

    .line 4767
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iput-object v4, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResumedInfos:Ljava/util/List;

    .line 4768
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->canAddTemplate()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 4769
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResumedInfos:Ljava/util/List;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getDefaultTemplateText(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 4770
    .local v2, "defaultTitle":Ljava/lang/CharSequence;
    if-eqz v2, :cond_6

    .line 4771
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResumedInfos:Ljava/util/List;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10, v11}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addTemplate(Ljava/util/List;Ljava/lang/String;Z)I

    move-result v8

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateIconIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9302(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 4772
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateIconIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    if-ltz v7, :cond_0

    .line 4775
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->prepareAnimationIcon()V
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    .line 4776
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v8, v11}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getCenterBarPoint(I)Landroid/graphics/Point;

    move-result-object v8

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 4778
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    invoke-direct {v0, v7}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4779
    .local v0, "animationIcon1Margin":Landroid/view/ViewGroup$MarginLayoutParams;
    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    invoke-direct {v1, v7}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4781
    .local v1, "animationIcon2Margin":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->isPortrait()Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 4782
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/Point;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Point;->y:I

    add-int/2addr v8, v9

    div-int/lit8 v8, v8, 0x2

    invoke-virtual {v0, v7, v8, v11, v11}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 4783
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->y:I

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v9

    add-int/2addr v8, v9

    div-int/lit8 v8, v8, 0x2

    invoke-virtual {v1, v7, v8, v11, v11}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 4796
    :goto_2
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v7

    new-instance v8, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v8, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4797
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v7

    new-instance v8, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v8, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4799
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationTemplateStart()V
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$10000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    .line 4800
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->openFlashBar()V
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$10100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    goto/16 :goto_0

    .line 4785
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    packed-switch v7, :pswitch_data_0

    goto :goto_2

    .line 4787
    :pswitch_0
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/Point;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Point;->x:I

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOffsetForAnim:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    add-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    invoke-virtual {v0, v7, v8, v11, v11}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 4788
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/Point;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Point;->x:I

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    add-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    invoke-virtual {v1, v7, v8, v11, v11}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_2

    .line 4791
    :pswitch_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/Point;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Point;->x:I

    div-int/lit8 v7, v7, 0x2

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    invoke-virtual {v0, v7, v8, v11, v11}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 4792
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/Point;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Point;->x:I

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOffsetForAnim:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    add-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    invoke-virtual {v1, v7, v8, v11, v11}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto/16 :goto_2

    .line 4802
    .end local v0    # "animationIcon1Margin":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v1    # "animationIcon2Margin":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v9, 0x7f080020

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->createUnableDialog(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 4806
    .end local v2    # "defaultTitle":Ljava/lang/CharSequence;
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$82;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const v9, 0x7f080021

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->createUnableDialog(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 4785
    nop

    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
