.class Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;
.super Ljava/lang/Object;
.source "PopupViewWindow.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/PopupViewWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V
    .locals 0

    .prologue
    .line 357
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const-wide/16 v6, 0x1f4

    const/16 v5, 0xcc

    const/16 v2, 0xcb

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 360
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 392
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 362
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMoveLeft:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$802(Lcom/sec/android/app/FlashBarService/PopupViewWindow;Z)Z

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMoveRight:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$902(Lcom/sec/android/app/FlashBarService/PopupViewWindow;Z)Z

    goto :goto_0

    .line 366
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerScrollHoverMargin:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1000(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->setHoverIcon(I)V

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMoveLeft:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$800(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMoveLeft:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$802(Lcom/sec/android/app/FlashBarService/PopupViewWindow;Z)Z

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 373
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMark:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$500(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerScrollHoverMargin:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1000(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->setHoverIcon(I)V

    .line 375
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMoveRight:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$900(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMoveRight:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$902(Lcom/sec/android/app/FlashBarService/PopupViewWindow;Z)Z

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 381
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->setHoverIcon(I)V

    goto :goto_0

    .line 385
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMoveLeft:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$802(Lcom/sec/android/app/FlashBarService/PopupViewWindow;Z)Z

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMoveRight:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$902(Lcom/sec/android/app/FlashBarService/PopupViewWindow;Z)Z

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 388
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->setHoverIcon(I)V

    goto/16 :goto_0

    .line 360
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
