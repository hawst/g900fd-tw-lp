.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 4317
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 13
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v8, 0x4

    const/4 v10, 0x3

    const/4 v9, -0x1

    .line 4319
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    .line 4321
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 4404
    const-string v6, "DragDrop"

    const-string v7, "Unknown action type received by OnDragListener."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4407
    :cond_0
    :goto_0
    :pswitch_0
    return v12

    .line 4326
    :pswitch_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildCount()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dragEnteredChildCount:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11902(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 4327
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListDragMode:Z
    invoke-static {v6, v12}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4602(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 4328
    sget-boolean v6, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v6, :cond_1

    .line 4329
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopGuideline:Landroid/widget/FrameLayout;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/FrameLayout;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4330
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBottomGuideline:Landroid/widget/FrameLayout;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/FrameLayout;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4332
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    move-result-object v6

    invoke-virtual {v6, v8}, Lcom/sec/android/app/FlashBarService/GuideLineImageView;->setVisibility(I)V

    .line 4333
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 4337
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v6

    float-to-int v5, v6

    .line 4338
    .local v5, "touchY":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0051

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 4340
    .local v4, "scrollDownArea":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ScrollView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/ScrollView;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v6, v4

    if-le v5, v6, :cond_2

    .line 4341
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ScrollView;

    move-result-object v6

    const/16 v7, 0xc

    invoke-virtual {v6, v11, v7}, Landroid/widget/ScrollView;->scrollBy(II)V

    .line 4344
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddEmptyItemPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v6

    if-ne v6, v9, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 4345
    const/4 v3, -0x1

    .line 4346
    .local v3, "position":I
    const/4 v1, 0x0

    .line 4347
    .local v1, "item_tail":Landroid/view/View;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildCount()I

    move-result v6

    if-lez v6, :cond_4

    .line 4348
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dragEnteredChildCount:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v6

    add-int/lit8 v3, v6, -0x1

    .line 4353
    :goto_1
    if-eq v3, v9, :cond_0

    .line 4354
    const/4 v2, 0x0

    .line 4355
    .local v2, "item_v":Landroid/view/View;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 4356
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v7

    const/4 v8, 0x2

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->changlistItemFromEditList(III)Z
    invoke-static {v6, v3, v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;III)Z

    .line 4357
    if-nez v3, :cond_5

    .line 4358
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v6

    invoke-virtual {v6, v11}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 4363
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddEmptyItemPosition:I
    invoke-static {v6, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5102(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 4366
    :cond_3
    if-eqz v2, :cond_0

    .line 4367
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v6, v10}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 4368
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v6, v10}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 4350
    .end local v2    # "item_v":Landroid/view/View;
    :cond_4
    const/4 v3, 0x0

    goto :goto_1

    .line 4360
    .restart local v2    # "item_v":Landroid/view/View;
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 4361
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->decideScrollMove(Landroid/view/View;Landroid/view/DragEvent;)Z
    invoke-static {v6, v1, p2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/view/View;Landroid/view/DragEvent;)Z

    goto :goto_2

    .line 4377
    .end local v1    # "item_tail":Landroid/view/View;
    .end local v2    # "item_v":Landroid/view/View;
    .end local v3    # "position":I
    .end local v4    # "scrollDownArea":I
    .end local v5    # "touchY":I
    :pswitch_3
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 4378
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v6, v9}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 4379
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I
    invoke-static {v6, v9}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4002(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 4380
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddEmptyItemPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v6

    if-eq v6, v9, :cond_0

    .line 4381
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddEmptyItemPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->changlistItemFromEditList(III)Z
    invoke-static {v6, v7, v8, v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;III)Z

    .line 4382
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddEmptyItemPosition:I
    invoke-static {v6, v9}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5102(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    goto/16 :goto_0

    .line 4387
    :pswitch_4
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListDragMode:Z
    invoke-static {v6, v11}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4602(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 4389
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 4390
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIvt:[B
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)[B

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v8}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 4391
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddEmptyItemPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->addAndChangelistItem(II)Z
    invoke-static {v6, v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;II)Z

    .line 4392
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddEmptyItemPosition:I
    invoke-static {v6, v9}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5102(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 4393
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v6, v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setItemIndexFromAppList(I)V

    .line 4394
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v6, v12}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListRelayout(Z)V

    .line 4395
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->updateEditListChanged()V

    .line 4398
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v6, v10}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 4399
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v6, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 4400
    :cond_7
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startCollapseTimer()V
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    goto/16 :goto_0

    .line 4321
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
