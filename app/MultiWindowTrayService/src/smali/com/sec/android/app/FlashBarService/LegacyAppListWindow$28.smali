.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$28;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationHistoryBarDisappear()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 1889
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v2, 0x0

    .line 1896
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMakeInstanceAniRunning:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5902(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 1897
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNewInstanceIntent:Landroid/content/Intent;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1898
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNewInstanceIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitySafe(Landroid/content/Intent;)V

    .line 1899
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNewInstanceIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateListStatusForIntent(Landroid/content/Intent;)V

    .line 1900
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissHistoryWindow(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)V

    .line 1901
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNewInstanceIntent:Landroid/content/Intent;
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6002(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/content/Intent;)Landroid/content/Intent;

    .line 1905
    :goto_0
    return-void

    .line 1903
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$28;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissHistoryWindow(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1893
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1891
    return-void
.end method
