.class public Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;
.super Landroid/widget/BaseAdapter;
.source "AppListBezelWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AppListAdapter"
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1993
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1989
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->context:Landroid/content/Context;

    .line 1994
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->context:Landroid/content/Context;

    .line 1995
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 1996
    return-void
.end method


# virtual methods
.method public createView(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 2011
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030005

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2013
    .local v0, "convertView":Landroid/view/View;
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;

    invoke-direct {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;-><init>()V

    .line 2015
    .local v1, "holder":Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;
    const v2, 0x7f0f0059

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->iconView:Landroid/widget/ImageView;

    .line 2016
    const v2, 0x7f0f005c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->labelView:Landroid/widget/TextView;

    .line 2017
    const v2, 0x7f0f005a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->removeIconView:Landroid/widget/ImageView;

    .line 2018
    const v2, 0x7f0f005b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->badgeIconView:Landroid/widget/ImageView;

    move-object v2, v0

    .line 2019
    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->contentView:Landroid/widget/LinearLayout;

    .line 2021
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2022
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2023
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListItemDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 2025
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2026
    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 1999
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppCnt()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 2005
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getListItemByIndex(IZ)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 2067
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v8, 0x8

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 2030
    if-nez p2, :cond_0

    .line 2031
    invoke-virtual {p0, p3, p1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->createView(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p2

    .line 2032
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 2033
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Recycled child has parent"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2035
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 2036
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Recycled child has parent"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2039
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;

    .line 2041
    .local v0, "holder":Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v5, p1, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getListItemByIndex(IZ)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v3

    .line 2042
    .local v3, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    .line 2043
    .local v2, "label":Ljava/lang/CharSequence;
    iget-object v5, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->labelView:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2044
    iget-object v5, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2045
    iget-object v5, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2047
    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v1, :cond_2

    .line 2049
    .local v1, "isTemplateItem":Z
    :goto_0
    if-eqz v1, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2050
    iget-object v5, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->removeIconView:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2055
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v6, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->badgeIconView:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    invoke-virtual {v5, v6, p1, v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeInstancebadgeForGridView(Landroid/widget/ImageView;II)V

    .line 2056
    if-eqz v1, :cond_4

    .line 2057
    iget-object v4, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->badgeIconView:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2062
    :goto_2
    return-object p2

    .end local v1    # "isTemplateItem":Z
    :cond_2
    move v1, v4

    .line 2047
    goto :goto_0

    .line 2052
    .restart local v1    # "isTemplateItem":Z
    :cond_3
    iget-object v5, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->removeIconView:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 2059
    :cond_4
    iget-object v5, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->badgeIconView:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2
.end method
