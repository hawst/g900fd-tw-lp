.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 3840
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v10, 0x68

    const/16 v9, 0x67

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3842
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0019

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v2, v6

    .line 3843
    .local v2, "moveBtnHeight":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 3929
    :cond_0
    :goto_0
    return v4

    .line 3846
    :pswitch_0
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->cancelCollapseTimer()V
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    .line 3847
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsPressMoveBtn:Z
    invoke-static {v6, v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$9702(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 3848
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mExpandAppList:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$9800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v7

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->setImageToMoveButton(IZ)V
    invoke-static {v5, v6, v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$9900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;IZ)V

    .line 3849
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isRecentsWindowShowing()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3850
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeRecentsWindow()V

    goto :goto_0

    .line 3854
    :pswitch_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsAvailableMove:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 3855
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v7

    float-to-int v7, v7

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionX:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 3856
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v7

    float-to-int v7, v7

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionY:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10202(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 3857
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionX:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v1

    .line 3858
    .local v1, "left":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionX:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    sub-int v3, v6, v7

    .line 3860
    .local v3, "right":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    packed-switch v6, :pswitch_data_1

    .line 3871
    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mExpandAppList:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$9800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 3872
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    if-eq v6, v9, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    if-ne v6, v10, :cond_3

    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionY:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->setAppListHandlePosition(I)V
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)V

    .line 3874
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mExpandAppList:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$9800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListPosition(IZZZ)V
    invoke-static {v6, v7, v5, v8, v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;IZZZ)V

    :goto_3
    move v4, v5

    .line 3892
    goto/16 :goto_0

    .line 3862
    :pswitch_2
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionX:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    if-lt v6, v2, :cond_1

    .line 3863
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mExpandAppList:Z
    invoke-static {v6, v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$9802(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    goto :goto_1

    .line 3866
    :pswitch_3
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionX:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    sub-int/2addr v7, v2

    if-gt v6, v7, :cond_1

    .line 3867
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mExpandAppList:Z
    invoke-static {v6, v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$9802(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    goto :goto_1

    .line 3873
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionX:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->setAppListHandlePosition(I)V
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)V

    goto :goto_2

    .line 3876
    :cond_4
    if-ge v1, v3, :cond_6

    .line 3877
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBeforeAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    if-ne v6, v10, :cond_5

    .line 3878
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBeforeAppListPosition:I
    invoke-static {v6, v9}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10402(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 3879
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListHorizontal:Ljava/util/List;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v8

    invoke-virtual {v6, v7, v8, v9, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZIZ)V

    .line 3880
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListVertical:Ljava/util/List;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v8

    invoke-virtual {v6, v7, v8, v9, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZIZ)V

    .line 3882
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mExpandAppList:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$9800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v7

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListPosition(IZZZ)V
    invoke-static {v6, v9, v4, v7, v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;IZZZ)V

    goto :goto_3

    .line 3884
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBeforeAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    if-ne v6, v9, :cond_7

    .line 3885
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBeforeAppListPosition:I
    invoke-static {v6, v10}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10402(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 3886
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListHorizontal:Ljava/util/List;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v8

    invoke-virtual {v6, v7, v8, v10, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZIZ)V

    .line 3887
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListVertical:Ljava/util/List;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v8

    invoke-virtual {v6, v7, v8, v10, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZIZ)V

    .line 3889
    :cond_7
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mExpandAppList:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$9800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v7

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListPosition(IZZZ)V
    invoke-static {v6, v10, v4, v7, v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;IZZZ)V

    goto/16 :goto_3

    .line 3895
    .end local v1    # "left":I
    .end local v3    # "right":I
    :pswitch_4
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsPressMoveBtn:Z
    invoke-static {v6, v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$9702(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 3896
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mExpandAppList:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$9800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->setImageToMoveButton(IZ)V
    invoke-static {v6, v7, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$9900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;IZ)V

    .line 3897
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->playSoundEffect(I)V

    .line 3898
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsAvailableMove:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v6

    if-ne v6, v5, :cond_a

    .line 3899
    const/4 v0, 0x0

    .line 3900
    .local v0, "hide":Z
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsAvailableMove:Z
    invoke-static {v6, v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 3901
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    packed-switch v6, :pswitch_data_2

    .line 3911
    :cond_8
    :goto_4
    if-eqz v0, :cond_9

    .line 3912
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListPosition(IZZ)V
    invoke-static {v6, v7, v5, v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;IZZ)V

    .line 3916
    :goto_5
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIvt:[B
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)[B

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v7}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 3917
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startCollapseTimer()V
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    .line 3925
    .end local v0    # "hide":Z
    :goto_6
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    invoke-virtual {v5, v6, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->setGlowEffect(IZ)V

    goto/16 :goto_0

    .line 3903
    .restart local v0    # "hide":Z
    :pswitch_5
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionX:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    if-gt v6, v2, :cond_8

    .line 3904
    const/4 v0, 0x1

    goto :goto_4

    .line 3907
    :pswitch_6
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionX:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    sub-int/2addr v7, v2

    if-lt v6, v7, :cond_8

    .line 3908
    const/4 v0, 0x1

    goto :goto_4

    .line 3914
    :cond_9
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mExpandAppList:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$9800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListPosition(IZZ)V
    invoke-static {v6, v7, v5, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;IZZ)V

    goto :goto_5

    .line 3919
    .end local v0    # "hide":Z
    :cond_a
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mExpandAppList:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$9800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 3920
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->closeFlashBar()V
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    goto :goto_6

    .line 3922
    :cond_b
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->openFlashBar()V
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    goto :goto_6

    .line 3843
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_4
    .end packed-switch

    .line 3860
    :pswitch_data_1
    .packed-switch 0x67
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 3901
    :pswitch_data_2
    .packed-switch 0x67
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
