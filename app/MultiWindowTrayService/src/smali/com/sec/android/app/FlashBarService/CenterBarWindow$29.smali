.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$29;
.super Ljava/lang/Object;
.source "CenterBarWindow.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;->makeDragAndDropHelpPopupLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 3089
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$29;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$29;->val$checkBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v3, 0x1

    .line 3092
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$29;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3093
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$29;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsDragAndDropCheckBoxChecked:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$5102(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 3094
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$29;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "do_not_show_help_popup_drag_and_drop"

    const/4 v2, -0x2

    invoke-static {v0, v1, v3, v2}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 3096
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$29;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsDragAndDropStart:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$5202(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 3097
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$29;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawDragAndDrop()V

    .line 3098
    return-void
.end method
