.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$12;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->createViewPagerTray()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 961
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 962
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 963
    return-void
.end method

.method public onPageSelected(I)V
    .locals 8
    .param p1, "position"    # I

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const v6, 0x3f99999a    # 1.2f

    .line 965
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMark:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/LinearLayout;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerSelectedPage:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 966
    .local v2, "prevIcon":Landroid/widget/ImageView;
    const/16 v3, 0x66

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 967
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 969
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMark:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 970
    .local v0, "curIcon":Landroid/widget/ImageView;
    invoke-virtual {v0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 971
    .local v1, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v1, v6, v6}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 972
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkerSize:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkerSize:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v6

    sub-float/2addr v3, v4

    div-float/2addr v3, v7

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkerSize:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMarkerSize:I
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    div-float/2addr v4, v7

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 974
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 975
    const/16 v3, 0xff

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 976
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerSelectedPage:I
    invoke-static {v3, p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3402(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 977
    return-void
.end method
