.class Lcom/sec/android/app/FlashBarService/GuideLine$1;
.super Ljava/lang/Object;
.source "GuideLine.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/GuideLine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/GuideLine;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/GuideLine;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 24
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 142
    new-instance v7, Landroid/util/DisplayMetrics;

    invoke-direct {v7}, Landroid/util/DisplayMetrics;-><init>()V

    .line 143
    .local v7, "displayMetrix":Landroid/util/DisplayMetrics;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowManager:Landroid/view/WindowManager;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    .line 144
    .local v6, "display":Landroid/view/Display;
    invoke-virtual {v6, v7}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 145
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    iget v0, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayWidth:I

    .line 146
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    iget v0, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayHeight:I

    .line 148
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v3

    .line 149
    .local v3, "action":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getX()F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v19, v0

    .line 150
    .local v19, "x":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getY()F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v20, v0

    .line 151
    .local v20, "y":I
    new-instance v16, Landroid/graphics/Rect;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Rect;-><init>()V

    .line 152
    .local v16, "rect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayWidth:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mFloatingMinimumSizeRatio:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v18, v0

    .line 153
    .local v18, "width":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayHeight:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mFloatingMinimumSizeRatio:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v11, v0

    .line 154
    .local v11, "height":I
    div-int/lit8 v21, v18, 0x2

    sub-int v14, v19, v21

    .line 155
    .local v14, "rawX":I
    div-int/lit8 v21, v11, 0x2

    sub-int v15, v20, v21

    .line 157
    .local v15, "rawY":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 158
    .local v5, "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v10

    .line 159
    .local v10, "headerWidth":I
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    .line 161
    .local v9, "headerHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/widget/FrameLayout$LayoutParams;

    .line 162
    .local v12, "lpBody":Landroid/widget/FrameLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/widget/FrameLayout$LayoutParams;

    .line 163
    .local v13, "lpHeader":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v21

    packed-switch v21, :pswitch_data_0

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/FlashBarService/GuideLine;->hide()V

    .line 317
    :cond_0
    :goto_0
    :pswitch_0
    const/16 v21, 0x1

    :goto_1
    return v21

    .line 169
    :pswitch_1
    const-string v21, "GuideLine"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "mGuideLineDragListener event = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mHandler:Landroid/os/Handler;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v21

    if-eqz v21, :cond_1

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mHandler:Landroid/os/Handler;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Landroid/os/Handler;->removeMessages(I)V

    .line 173
    :cond_1
    move/from16 v0, v18

    iput v0, v12, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 174
    iput v11, v12, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 175
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    div-int/lit8 v22, v18, 0x2

    sub-int v22, v19, v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setX(F)V

    .line 177
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    div-int/lit8 v22, v11, 0x2

    sub-int v22, v20, v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setY(F)V

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const v22, 0x1080674

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 179
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 181
    iput v10, v13, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 182
    iput v9, v13, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 183
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    div-int/lit8 v22, v10, 0x2

    sub-int v22, v19, v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setX(F)V

    .line 185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    div-int/lit8 v22, v11, 0x2

    sub-int v22, v20, v22

    div-int/lit8 v23, v9, 0x2

    sub-int v22, v22, v23

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setY(F)V

    .line 186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mSupportMultiWindow:Z

    move/from16 v21, v0

    if-eqz v21, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mIntent:Landroid/content/Intent;

    move-object/from16 v23, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->isFullGuideLine(Landroid/content/Intent;)Z

    move-result v21

    :goto_2
    move/from16 v0, v21

    move-object/from16 v1, v22

    iput-boolean v0, v1, Lcom/sec/android/app/FlashBarService/GuideLine;->mFullGuideRect:Z

    .line 190
    const-string v21, "GuideLine"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "mGuideLineDragListener mFullGuideRect = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mFullGuideRect:Z

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mFullGuideRect:Z

    move/from16 v21, v0

    if-nez v21, :cond_3

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentGuideRect(II)Landroid/graphics/Rect;

    move-result-object v16

    .line 196
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput-boolean v0, v1, Lcom/sec/android/app/FlashBarService/GuideLine;->mDocking:Z

    goto/16 :goto_0

    .line 188
    :cond_2
    const/16 v21, 0x0

    goto :goto_2

    .line 194
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/FlashBarService/AppListController;->getRectByZone(I)Landroid/graphics/Rect;

    move-result-object v16

    goto :goto_3

    .line 199
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mFullGuideRect:Z

    move/from16 v21, v0

    if-nez v21, :cond_6

    .line 200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentGuideRect(II)Landroid/graphics/Rect;

    move-result-object v16

    .line 205
    :goto_4
    if-eqz v16, :cond_0

    .line 206
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayOrientation:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_9

    .line 207
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v21, v0

    const v22, 0x3d8f5c29    # 0.07f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayHeight:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v22, v22, v23

    cmpg-float v21, v21, v22

    if-ltz v21, :cond_4

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v21, v0

    const v22, 0x3f6e147b    # 0.93f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayHeight:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v22, v22, v23

    cmpl-float v21, v21, v22

    if-lez v21, :cond_7

    .line 208
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDocking:Z

    move/from16 v21, v0

    if-nez v21, :cond_5

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const v22, 0x1080669

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 210
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setX(F)V

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setY(F)V

    .line 212
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v21

    move/from16 v0, v21

    iput v0, v12, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 213
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v21

    move/from16 v0, v21

    iput v0, v12, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 215
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mVibrator:Landroid/os/SystemVibrator;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/android/app/FlashBarService/GuideLine;->mIvt:[B
    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/FlashBarService/GuideLine;->access$000(Lcom/sec/android/app/FlashBarService/GuideLine;)[B

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mVibrator:Landroid/os/SystemVibrator;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v23

    invoke-virtual/range {v21 .. v23}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput-boolean v0, v1, Lcom/sec/android/app/FlashBarService/GuideLine;->mDocking:Z

    .line 259
    :cond_5
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 202
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/FlashBarService/AppListController;->getRectByZone(I)Landroid/graphics/Rect;

    move-result-object v16

    goto/16 :goto_4

    .line 219
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const v22, 0x1080674

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    div-int/lit8 v22, v18, 0x2

    sub-int v22, v19, v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setX(F)V

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    div-int/lit8 v22, v11, 0x2

    sub-int v22, v20, v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setY(F)V

    .line 222
    move/from16 v0, v18

    iput v0, v12, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 223
    iput v11, v12, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 225
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    div-int/lit8 v22, v10, 0x2

    sub-int v22, v19, v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setX(F)V

    .line 226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    div-int/lit8 v22, v11, 0x2

    sub-int v22, v20, v22

    div-int/lit8 v23, v9, 0x2

    sub-int v22, v22, v23

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setY(F)V

    .line 227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/ImageView;->getVisibility()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_8

    .line 228
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 230
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput-boolean v0, v1, Lcom/sec/android/app/FlashBarService/GuideLine;->mDocking:Z

    goto/16 :goto_5

    .line 233
    :cond_9
    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v21, v0

    const v22, 0x3d8f5c29    # 0.07f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayWidth:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v22, v22, v23

    cmpg-float v21, v21, v22

    if-ltz v21, :cond_a

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v21, v0

    const v22, 0x3f6e147b    # 0.93f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayWidth:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v22, v22, v23

    cmpl-float v21, v21, v22

    if-lez v21, :cond_b

    .line 234
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDocking:Z

    move/from16 v21, v0

    if-nez v21, :cond_5

    .line 235
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const v22, 0x1080669

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 236
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setX(F)V

    .line 237
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setY(F)V

    .line 238
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v21

    move/from16 v0, v21

    iput v0, v12, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 239
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v21

    move/from16 v0, v21

    iput v0, v12, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 241
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mVibrator:Landroid/os/SystemVibrator;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/android/app/FlashBarService/GuideLine;->mIvt:[B
    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/FlashBarService/GuideLine;->access$000(Lcom/sec/android/app/FlashBarService/GuideLine;)[B

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mVibrator:Landroid/os/SystemVibrator;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v23

    invoke-virtual/range {v21 .. v23}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 242
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput-boolean v0, v1, Lcom/sec/android/app/FlashBarService/GuideLine;->mDocking:Z

    goto/16 :goto_5

    .line 245
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const v22, 0x1080674

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    div-int/lit8 v22, v18, 0x2

    sub-int v22, v19, v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setX(F)V

    .line 247
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    div-int/lit8 v22, v11, 0x2

    sub-int v22, v20, v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setY(F)V

    .line 248
    move/from16 v0, v18

    iput v0, v12, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 249
    iput v11, v12, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    div-int/lit8 v22, v10, 0x2

    sub-int v22, v19, v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setX(F)V

    .line 252
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    div-int/lit8 v22, v11, 0x2

    sub-int v22, v20, v22

    div-int/lit8 v23, v9, 0x2

    sub-int v22, v22, v23

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setY(F)V

    .line 253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/ImageView;->getVisibility()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_c

    .line 254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 256
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput-boolean v0, v1, Lcom/sec/android/app/FlashBarService/GuideLine;->mDocking:Z

    goto/16 :goto_5

    .line 264
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/FlashBarService/GuideLine;->hide()V

    goto/16 :goto_0

    .line 267
    :pswitch_4
    const-string v21, "GuideLine"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "mGuideLineDragListener event = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    new-instance v17, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>()V

    .line 269
    .local v17, "style":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayOrientation:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_d

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v21, v0

    const v22, 0x3d8f5c29    # 0.07f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayHeight:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v22, v22, v23

    cmpg-float v21, v21, v22

    if-ltz v21, :cond_e

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v21, v0

    const v22, 0x3f6e147b    # 0.93f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayHeight:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v22, v22, v23

    cmpl-float v21, v21, v22

    if-gtz v21, :cond_e

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayOrientation:I

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_10

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v21, v0

    const v22, 0x3d8f5c29    # 0.07f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayWidth:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v22, v22, v23

    cmpg-float v21, v21, v22

    if-ltz v21, :cond_e

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v21, v0

    const v22, 0x3f6e147b    # 0.93f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayWidth:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v22, v22, v23

    cmpl-float v21, v21, v22

    if-lez v21, :cond_10

    .line 273
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getFrontActivityMultiWindowStyle(I)Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v8

    .line 274
    .local v8, "frontActivityMultiWindowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    if-eqz v8, :cond_f

    const/16 v21, 0x2

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v21

    if-eqz v21, :cond_f

    const/high16 v21, 0x200000

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v21

    if-nez v21, :cond_f

    const/16 v21, 0x1000

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v21

    if-nez v21, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mSupportMultiWindow:Z

    move/from16 v21, v0

    if-eqz v21, :cond_f

    .line 279
    const/16 v21, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setType(I)V

    .line 280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentZone(II)I

    move-result v4

    .line 281
    .local v4, "currentZone":I
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 297
    .end local v4    # "currentZone":I
    .end local v8    # "frontActivityMultiWindowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_f
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mTaskId:I

    move/from16 v21, v0

    if-ltz v21, :cond_13

    .line 308
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 309
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 310
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/FlashBarService/GuideLine;->hide()V

    .line 311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/FlashBarService/GuideLine;->mContext:Landroid/content/Context;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/FlashBarService/GuideLine;->access$100(Lcom/sec/android/app/FlashBarService/GuideLine;)Landroid/content/Context;

    move-result-object v21

    const-string v22, "RCNT"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mIntent:Landroid/content/Intent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v21 .. v23}, Lcom/sec/android/app/FlashBarService/LoggingHelper;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 284
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mStatusBarHeight:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v15, v0, :cond_11

    .line 285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v15, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mStatusBarHeight:I

    .line 288
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mTaskId:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/FlashBarService/AppListController;->isRunningScaleWindow(I)Z

    move-result v21

    if-nez v21, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->isEnableMakePenWindow()Z

    move-result v21

    if-nez v21, :cond_12

    .line 289
    const/16 v21, 0x1

    goto/16 :goto_1

    .line 292
    :cond_12
    new-instance v17, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    .end local v17    # "style":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    const/16 v21, 0x2

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    .line 293
    .restart local v17    # "style":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    const/16 v21, 0x800

    const/16 v22, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setOption(IZ)V

    .line 294
    new-instance v21, Landroid/graphics/Rect;

    add-int v22, v14, v18

    add-int v23, v15, v11

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v14, v15, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setBounds(Landroid/graphics/Rect;)V

    goto/16 :goto_6

    .line 301
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mIntent:Landroid/content/Intent;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 303
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/FlashBarService/GuideLine;->mContext:Landroid/content/Context;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/FlashBarService/GuideLine;->access$100(Lcom/sec/android/app/FlashBarService/GuideLine;)Landroid/content/Context;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;->this$0:Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/GuideLine;->mIntent:Landroid/content/Intent;

    move-object/from16 v22, v0

    sget-object v23, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual/range {v21 .. v23}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_7

    .line 305
    :catch_0
    move-exception v21

    goto/16 :goto_7

    .line 163
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
