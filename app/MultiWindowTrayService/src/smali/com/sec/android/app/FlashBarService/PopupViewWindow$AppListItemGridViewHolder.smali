.class final Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;
.super Ljava/lang/Object;
.source "PopupViewWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/PopupViewWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "AppListItemGridViewHolder"
.end annotation


# instance fields
.field iconView:Landroid/widget/ImageView;

.field labelView:Landroid/widget/TextView;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "appClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 816
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 817
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1
    .param p1, "appTouchListener"    # Landroid/view/View$OnTouchListener;

    .prologue
    .line 812
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 813
    return-void
.end method
