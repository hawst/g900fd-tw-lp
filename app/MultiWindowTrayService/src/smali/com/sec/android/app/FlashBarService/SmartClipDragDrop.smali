.class public Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;
.super Landroid/app/SallyService;
.source "SmartClipDragDrop.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$SaveImageFileTask;,
        Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$ShadowBuilder;
    }
.end annotation


# static fields
.field private static DRAG_SHADOW_BORDER_LINE_THICK:I

.field private static DRAG_SHADOW_MAX_TEXT_LENGTH:I


# instance fields
.field mAm:Landroid/app/IActivityManager;

.field private mAppContext:Landroid/content/Context;

.field mAppListExpandReceiver:Landroid/content/BroadcastReceiver;

.field final mForegroundToken:Landroid/os/IBinder;

.field private final mHandler:Landroid/os/Handler;

.field private mHasCocktailBar:Z

.field mHomeResumeReceiver:Landroid/content/BroadcastReceiver;

.field private mIsDragging:Z

.field private mIvt:[B

.field private mMainFrame:Landroid/widget/FrameLayout;

.field mMultiWindowStatusReceiver:Landroid/content/BroadcastReceiver;

.field private volatile mServiceHandler:Landroid/os/Handler;

.field private volatile mServiceLooper:Landroid/os/Looper;

.field private mServiceRunning:Z

.field private mSmartClipNewFileName:Lcom/sec/android/app/FlashBarService/SmartClipNewFileName;

.field private mThread:Landroid/os/HandlerThread;

.field private mVibrator:Landroid/os/SystemVibrator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const/16 v0, 0x14

    sput v0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->DRAG_SHADOW_MAX_TEXT_LENGTH:I

    .line 81
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->DRAG_SHADOW_BORDER_LINE_THICK:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 64
    invoke-direct {p0}, Landroid/app/SallyService;-><init>()V

    .line 83
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mMainFrame:Landroid/widget/FrameLayout;

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    .line 85
    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mServiceRunning:Z

    .line 86
    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mIsDragging:Z

    .line 87
    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mHasCocktailBar:Z

    .line 89
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mForegroundToken:Landroid/os/IBinder;

    .line 90
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAm:Landroid/app/IActivityManager;

    .line 92
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mSmartClipNewFileName:Lcom/sec/android/app/FlashBarService/SmartClipNewFileName;

    .line 94
    const/16 v0, 0x26

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mIvt:[B

    .line 149
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$1;-><init>(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mHomeResumeReceiver:Landroid/content/BroadcastReceiver;

    .line 159
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$2;-><init>(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppListExpandReceiver:Landroid/content/BroadcastReceiver;

    .line 166
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$3;-><init>(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mMultiWindowStatusReceiver:Landroid/content/BroadcastReceiver;

    .line 191
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "SmartClipDragDropThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mThread:Landroid/os/HandlerThread;

    .line 192
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mHandler:Landroid/os/Handler;

    .line 805
    return-void

    .line 94
    nop

    :array_0
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0xat
        0x0t
        0x14t
        0x0t
        0x0t
        0x0t
        0x20t
        0x1t
        0x0t
        0x0t
        0x7ft
        0x0t
        0x0t
        0x42t
        0x0t
        0x0t
        0x4dt
        0x0t
        0x61t
        0x0t
        0x67t
        0x0t
        0x53t
        0x0t
        0x77t
        0x0t
        0x65t
        0x0t
        0x65t
        0x0t
        0x70t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 64
    sget v0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->DRAG_SHADOW_BORDER_LINE_THICK:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->notifyStartDragMode()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/view/View;Ljava/lang/String;Landroid/graphics/Rect;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Landroid/graphics/Rect;
    .param p4, "x4"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->startTextDrag(Landroid/view/View;Ljava/lang/String;Landroid/graphics/Rect;I)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/view/View;Landroid/graphics/Rect;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/graphics/Rect;
    .param p3, "x3"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->startImageDrag(Landroid/view/View;Landroid/graphics/Rect;I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->stopSmartClipDragDropThread()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/graphics/Rect;I)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;
    .param p1, "x1"    # Landroid/graphics/Rect;
    .param p2, "x2"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->cropScreen(Landroid/graphics/Rect;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/graphics/Bitmap;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->saveImageFile(Landroid/graphics/Bitmap;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->minimize()V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mIvt:[B

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)Landroid/os/SystemVibrator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mVibrator:Landroid/os/SystemVibrator;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;
    .param p1, "x1"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mIsDragging:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)Landroid/os/HandlerThread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mThread:Landroid/os/HandlerThread;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)Landroid/os/Looper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mServiceLooper:Landroid/os/Looper;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/os/Looper;)Landroid/os/Looper;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;
    .param p1, "x1"    # Landroid/os/Looper;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mServiceLooper:Landroid/os/Looper;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mServiceHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mServiceHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/view/View;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getSmartClipData(Landroid/view/View;II)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;
    .param p1, "x1"    # Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getTextMetaData(Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;
    .param p1, "x1"    # Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getUrlMetaData(Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;
    .param p1, "x1"    # Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getTextSelectionMetaData(Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private adjustCropRectForReduceScreen(Landroid/graphics/Rect;)V
    .locals 9
    .param p1, "cropRect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 508
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "any_screen_running"

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_1

    .line 509
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "any_screen_running_scale"

    invoke-static {v5, v6, v8}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v3

    .line 510
    .local v3, "scale":F
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "any_screen_running_offset_x"

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 511
    .local v1, "offsetX":I
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "any_screen_running_offset_y"

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 512
    .local v2, "offsetY":I
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v4

    .line 513
    .local v4, "w":I
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 515
    .local v0, "h":I
    cmpl-float v5, v3, v8

    if-lez v5, :cond_0

    const/high16 v5, 0x3f800000    # 1.0f

    cmpg-float v5, v3, v5

    if-gtz v5, :cond_0

    .line 516
    int-to-float v5, v1

    iget v6, p1, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    mul-float/2addr v6, v3

    add-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, p1, Landroid/graphics/Rect;->left:I

    .line 517
    int-to-float v5, v2

    iget v6, p1, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    mul-float/2addr v6, v3

    add-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, p1, Landroid/graphics/Rect;->top:I

    .line 518
    iget v5, p1, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    int-to-float v6, v4

    mul-float/2addr v6, v3

    add-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, p1, Landroid/graphics/Rect;->right:I

    .line 519
    iget v5, p1, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    int-to-float v6, v0

    mul-float/2addr v6, v3

    add-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 521
    :cond_0
    const-string v5, "SmartClipDragDrop"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "any_screen_running : Crope rect : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", scale="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", offsetX="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", offsetY="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    .end local v0    # "h":I
    .end local v1    # "offsetX":I
    .end local v2    # "offsetY":I
    .end local v3    # "scale":F
    .end local v4    # "w":I
    :cond_1
    return-void
.end method

.method private cropScreen(Landroid/graphics/Rect;I)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "cropRect"    # Landroid/graphics/Rect;
    .param p2, "layerToCapture"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v11, 0x0

    .line 527
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    const-string v9, "window"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    .line 528
    .local v6, "windowManager":Landroid/view/WindowManager;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 529
    .local v2, "displaySize":Landroid/graphics/Point;
    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 530
    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 532
    :cond_0
    iget v5, v2, Landroid/graphics/Point;->x:I

    .line 533
    .local v5, "scrWidth":I
    iget v4, v2, Landroid/graphics/Point;->y:I

    .line 536
    .local v4, "scrHeight":I
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v8

    if-lez v8, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v8

    if-gtz v8, :cond_2

    :cond_1
    move-object v1, v7

    .line 555
    :goto_0
    return-object v1

    .line 538
    :cond_2
    invoke-virtual {p1, v11, v11, v5, v4}, Landroid/graphics/Rect;->intersect(IIII)Z

    .line 539
    const-string v8, "SmartClipDragDrop"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "cropScreen : Crope rect : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    invoke-direct {p0, p2}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getFullScreenShotBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 543
    .local v3, "fullscreenBitmap":Landroid/graphics/Bitmap;
    if-nez v3, :cond_3

    .line 544
    const-string v8, "SmartClipDragDrop"

    const-string v9, "Cannot capture the screen!"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v7

    .line 545
    goto :goto_0

    .line 548
    :cond_3
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->adjustCropRectForReduceScreen(Landroid/graphics/Rect;)V

    .line 551
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v9

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 552
    .local v1, "croppedBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 553
    .local v0, "canvasForCrop":Landroid/graphics/Canvas;
    new-instance v8, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v9

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v10

    invoke-direct {v8, v11, v11, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v3, p1, v8, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private getDegreesForRotation(I)F
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 656
    packed-switch p1, :pswitch_data_0

    .line 664
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 658
    :pswitch_0
    const/high16 v0, 0x43870000    # 270.0f

    goto :goto_0

    .line 660
    :pswitch_1
    const/high16 v0, 0x43340000    # 180.0f

    goto :goto_0

    .line 662
    :pswitch_2
    const/high16 v0, 0x42b40000    # 90.0f

    goto :goto_0

    .line 656
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getFullScreenShotBitmap(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "layer"    # I

    .prologue
    .line 559
    const/16 v0, 0x4e20

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getFullScreenShotBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private getFullScreenShotBitmap(II)Landroid/graphics/Bitmap;
    .locals 24
    .param p1, "minLayer"    # I
    .param p2, "maxLayer"    # I

    .prologue
    .line 563
    const-string v2, "SmartClipDragDrop"

    const-string v5, "getFullScreenShotBitmap"

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    new-instance v14, Landroid/util/DisplayMetrics;

    invoke-direct {v14}, Landroid/util/DisplayMetrics;-><init>()V

    .line 567
    .local v14, "displayMetrics":Landroid/util/DisplayMetrics;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    const-string v5, "window"

    invoke-virtual {v2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v17

    .line 568
    .local v17, "mDisplay":Landroid/view/Display;
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 570
    invoke-virtual/range {v17 .. v17}, Landroid/view/Display;->getRotation()I

    move-result v13

    .line 573
    .local v13, "currentRotation":I
    iget v2, v14, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v22

    .line 574
    .local v22, "screenWidth":I
    iget v2, v14, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v21

    .line 575
    .local v21, "screenHeight":I
    move/from16 v10, v22

    .line 576
    .local v10, "appWidth":I
    move/from16 v9, v21

    .line 577
    .local v9, "appHeight":I
    const/4 v12, 0x0

    .line 579
    .local v12, "cocktailBarSize":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mHasCocktailBar:Z

    if-eqz v2, :cond_0

    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/hardware/display/DisplayManagerGlobal;->getDisplayInfo(I)Landroid/view/DisplayInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 580
    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/hardware/display/DisplayManagerGlobal;->getDisplayInfo(I)Landroid/view/DisplayInfo;

    move-result-object v2

    iget v10, v2, Landroid/view/DisplayInfo;->appWidth:I

    .line 581
    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/hardware/display/DisplayManagerGlobal;->getDisplayInfo(I)Landroid/view/DisplayInfo;

    move-result-object v2

    iget v9, v2, Landroid/view/DisplayInfo;->appHeight:I

    .line 583
    move/from16 v0, v22

    if-eq v0, v10, :cond_4

    .line 584
    sub-int v12, v22, v10

    .line 590
    :cond_0
    :goto_0
    move/from16 v3, v22

    .line 591
    .local v3, "lcdWidth":I
    move/from16 v4, v21

    .line 593
    .local v4, "lcdHeight":I
    packed-switch v13, :pswitch_data_0

    .line 604
    :goto_1
    :pswitch_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mHasCocktailBar:Z

    if-eqz v2, :cond_1

    .line 605
    move/from16 v22, v10

    .line 606
    move/from16 v21, v9

    .line 610
    :cond_1
    const/16 v20, 0x0

    .line 613
    .local v20, "screenBitmap":Landroid/graphics/Bitmap;
    if-gez p1, :cond_2

    .line 614
    const/16 p1, 0x4e20

    .line 617
    :cond_2
    if-gez p2, :cond_3

    .line 618
    const p2, 0x30d40

    .line 624
    :cond_3
    :try_start_0
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    const/4 v7, 0x0

    const/4 v8, 0x0

    move/from16 v5, p1

    move/from16 v6, p2

    invoke-static/range {v2 .. v8}, Landroid/view/SurfaceControl;->screenshot(Landroid/graphics/Rect;IIIIZI)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v20

    .line 631
    :goto_2
    if-nez v20, :cond_5

    .line 632
    const-string v2, "SmartClipDragDrop"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not capture the screen! lcdWidth="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " lcdHeight="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " rotation="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    const/4 v2, 0x0

    .line 652
    :goto_3
    return-object v2

    .line 586
    .end local v3    # "lcdWidth":I
    .end local v4    # "lcdHeight":I
    .end local v20    # "screenBitmap":Landroid/graphics/Bitmap;
    :cond_4
    sub-int v12, v21, v9

    goto :goto_0

    .line 596
    .restart local v3    # "lcdWidth":I
    .restart local v4    # "lcdHeight":I
    :pswitch_1
    move/from16 v3, v21

    .line 597
    move/from16 v4, v22

    .line 598
    goto :goto_1

    .line 627
    .restart local v20    # "screenBitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v16

    .line 628
    .local v16, "e":Ljava/lang/Exception;
    const-string v2, "SmartClipDragDrop"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getFullScreenShotBitmap : failed to invoke screenshot() method : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 637
    .end local v16    # "e":Ljava/lang/Exception;
    :cond_5
    if-nez v13, :cond_6

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mHasCocktailBar:Z

    if-eqz v2, :cond_7

    .line 638
    :cond_6
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getDegreesForRotation(I)F

    move-result v19

    .line 639
    .local v19, "rotationDegrees":F
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v22

    move/from16 v1, v21

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 640
    .local v18, "rotatedBitmap":Landroid/graphics/Bitmap;
    new-instance v11, Landroid/graphics/Canvas;

    move-object/from16 v0, v18

    invoke-direct {v11, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 641
    .local v11, "c":Landroid/graphics/Canvas;
    new-instance v23, Landroid/graphics/Point;

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    move-object/from16 v0, v23

    invoke-direct {v0, v2, v5}, Landroid/graphics/Point;-><init>(II)V

    .line 642
    .local v23, "srcBmpCenter":Landroid/graphics/Point;
    new-instance v15, Landroid/graphics/Point;

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    invoke-direct {v15, v2, v5}, Landroid/graphics/Point;-><init>(II)V

    .line 644
    .local v15, "dstBmpCenter":Landroid/graphics/Point;
    iget v2, v15, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v5, v15, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    move/from16 v0, v19

    invoke-virtual {v11, v0, v2, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 645
    iget v2, v15, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v23

    iget v5, v0, Landroid/graphics/Point;->x:I

    sub-int/2addr v2, v5

    div-int/lit8 v5, v12, 0x2

    add-int/2addr v2, v5

    int-to-float v2, v2

    iget v5, v15, Landroid/graphics/Point;->y:I

    move-object/from16 v0, v23

    iget v6, v0, Landroid/graphics/Point;->y:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v11, v0, v2, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 649
    move-object/from16 v20, v18

    .end local v11    # "c":Landroid/graphics/Canvas;
    .end local v15    # "dstBmpCenter":Landroid/graphics/Point;
    .end local v18    # "rotatedBitmap":Landroid/graphics/Bitmap;
    .end local v19    # "rotationDegrees":F
    .end local v23    # "srcBmpCenter":Landroid/graphics/Point;
    :cond_7
    move-object/from16 v2, v20

    .line 652
    goto/16 :goto_3

    .line 593
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getSmartClipData(Landroid/view/View;II)V
    .locals 6
    .param p1, "touchView"    # Landroid/view/View;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 732
    move-object v2, p1

    .line 733
    .local v2, "view":Landroid/view/View;
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mServiceRunning:Z

    if-nez v3, :cond_0

    .line 734
    const-string v3, "SmartClipDragDrop"

    const-string v4, "onTouch : SmartClipDragDrop service is already stopped"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 735
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->stopSmartClipDragDropThread()V

    .line 793
    :goto_0
    return-void

    .line 740
    :cond_0
    const-string v3, "spengestureservice"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/smartclip/SpenGestureManager;

    .line 741
    .local v1, "spenGestureManager":Lcom/samsung/android/smartclip/SpenGestureManager;
    new-instance v3, Landroid/graphics/Rect;

    add-int/lit8 v4, p2, 0x1

    add-int/lit8 v5, p3, 0x1

    invoke-direct {v3, p2, p3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v1, v3, v4, v5}, Lcom/samsung/android/smartclip/SpenGestureManager;->getSmartClipDataByScreenRect(Landroid/graphics/Rect;Landroid/os/IBinder;I)Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    move-result-object v0

    .line 742
    .local v0, "repository":Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;

    invoke-direct {v4, p0, v0, v2}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;-><init>(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;Landroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private getTextMetaData(Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;)Ljava/lang/String;
    .locals 8
    .param p1, "repository"    # Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    .prologue
    const/4 v5, 0x0

    .line 453
    const-string v6, "plain_text"

    invoke-virtual {p1, v6}, Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;->getMetaTag(Ljava/lang/String;)Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTagArray;

    move-result-object v3

    .line 454
    .local v3, "tags":Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTagArray;
    const-string v4, ""

    .line 456
    .local v4, "textMetaValue":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTagArray;->size()I

    move-result v6

    if-lez v6, :cond_1

    .line 457
    invoke-virtual {v3}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTagArray;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;

    .line 458
    .local v2, "metaTag":Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 459
    .local v0, "curTextValue":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 460
    goto :goto_0

    .line 461
    .end local v0    # "curTextValue":Ljava/lang/String;
    .end local v2    # "metaTag":Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;
    :cond_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_2

    .line 467
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    return-object v5

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    move-object v5, v4

    .line 464
    goto :goto_1
.end method

.method private getTextSelectionMetaData(Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;)Ljava/lang/String;
    .locals 8
    .param p1, "repository"    # Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    .prologue
    const/4 v5, 0x0

    .line 472
    const-string v6, "text_selection"

    invoke-virtual {p1, v6}, Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;->getMetaTag(Ljava/lang/String;)Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTagArray;

    move-result-object v3

    .line 473
    .local v3, "tags":Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTagArray;
    const-string v4, ""

    .line 475
    .local v4, "textSelectionMetaValue":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTagArray;->size()I

    move-result v6

    if-lez v6, :cond_1

    .line 476
    invoke-virtual {v3}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTagArray;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;

    .line 477
    .local v2, "metaTag":Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 478
    .local v0, "curTextValue":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 479
    goto :goto_0

    .line 480
    .end local v0    # "curTextValue":Ljava/lang/String;
    .end local v2    # "metaTag":Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;
    :cond_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_2

    .line 486
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    return-object v5

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    move-object v5, v4

    .line 483
    goto :goto_1
.end method

.method private getTextThumbnailBuilder(Ljava/lang/CharSequence;)Landroid/view/View$DragShadowBuilder;
    .locals 7
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v6, -0x2

    const/4 v5, 0x0

    .line 704
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    const v3, 0x10900fc

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 707
    .local v0, "shadowView":Landroid/widget/TextView;
    if-nez v0, :cond_0

    .line 708
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Unable to inflate text drag thumbnail"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 711
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    sget v3, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->DRAG_SHADOW_MAX_TEXT_LENGTH:I

    if-le v2, v3, :cond_1

    .line 712
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget v3, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->DRAG_SHADOW_MAX_TEXT_LENGTH:I

    invoke-interface {p1, v5, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 714
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 715
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 717
    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 718
    const v2, -0xbbbbbc

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 720
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 723
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 724
    .local v1, "size":I
    invoke-virtual {v0, v1, v1}, Landroid/widget/TextView;->measure(II)V

    .line 726
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v0, v5, v5, v2, v3}, Landroid/widget/TextView;->layout(IIII)V

    .line 727
    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 728
    new-instance v2, Landroid/view/View$DragShadowBuilder;

    invoke-direct {v2, v0}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    return-object v2
.end method

.method private getUrlMetaData(Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;)Ljava/lang/String;
    .locals 2
    .param p1, "repository"    # Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    .prologue
    .line 491
    const-string v1, "url"

    invoke-virtual {p1, v1}, Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;->getMetaTag(Ljava/lang/String;)Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTagArray;

    move-result-object v0

    .line 493
    .local v0, "tags":Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTagArray;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTagArray;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 494
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTagArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 496
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private minimize()V
    .locals 6

    .prologue
    .line 434
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 437
    .local v1, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v3, v3, 0x8

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 438
    const/4 v3, 0x0

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 439
    const/4 v3, 0x0

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 440
    const/4 v3, 0x0

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 441
    const/4 v3, 0x0

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 442
    const/16 v3, 0x33

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 443
    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 445
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    const-string v4, "window"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 446
    .local v2, "windowManager":Landroid/view/WindowManager;
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 450
    .end local v1    # "layoutParams":Landroid/view/WindowManager$LayoutParams;
    .end local v2    # "windowManager":Landroid/view/WindowManager;
    :goto_0
    return-void

    .line 447
    :catch_0
    move-exception v0

    .line 448
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "SmartClipDragDrop"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IllegalArgumentException "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private notifyStartDragMode()V
    .locals 2

    .prologue
    .line 359
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 360
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.action.NOTIFY_START_DRAG_MODE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 361
    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->sendBroadcast(Landroid/content/Intent;)V

    .line 362
    return-void
.end method

.method private saveImageFile(Landroid/graphics/Bitmap;)Ljava/lang/String;
    .locals 17
    .param p1, "bitmapToSave"    # Landroid/graphics/Bitmap;

    .prologue
    .line 668
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    sget-object v13, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-virtual {v12, v13}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 669
    .local v2, "availableFilePhth":Ljava/io/File;
    if-nez v2, :cond_0

    .line 670
    const/4 v8, 0x0

    .line 700
    :goto_0
    return-object v8

    .line 672
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    .line 673
    .local v6, "imageDir":Ljava/lang/String;
    const-string v12, "DragImage[%d].png"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mSmartClipNewFileName:Lcom/sec/android/app/FlashBarService/SmartClipNewFileName;

    sget-object v16, Lcom/sec/android/app/FlashBarService/SmartClipNewFileName$IndexMode;->DRAGDRAP:Lcom/sec/android/app/FlashBarService/SmartClipNewFileName$IndexMode;

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/app/FlashBarService/SmartClipNewFileName;->getIndex(Lcom/sec/android/app/FlashBarService/SmartClipNewFileName$IndexMode;)I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 674
    .local v7, "imageFileName":Ljava/lang/String;
    const-string v12, "%s/%s"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v6, v13, v14

    const/4 v14, 0x1

    aput-object v7, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 677
    .local v8, "imageFilePath":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 678
    .local v10, "savingStartTime":J
    const-string v12, "SmartClipDragDrop"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "saveImageFile : Saving the image file to "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 680
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 681
    .local v3, "dir":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_1

    .line 682
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 685
    :cond_1
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 686
    .local v5, "file":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->createNewFile()Z

    .line 687
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 688
    .local v9, "out":Ljava/io/FileOutputStream;
    sget-object v12, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v13, 0x64

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13, v9}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 689
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->flush()V

    .line 690
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 692
    const-string v12, "SmartClipDragDrop"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "saveImageFile : Encoding and file save finished. elapsed time = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    sub-long/2addr v14, v10

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "ms"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 693
    .end local v3    # "dir":Ljava/io/File;
    .end local v5    # "file":Ljava/io/File;
    .end local v9    # "out":Ljava/io/FileOutputStream;
    .end local v10    # "savingStartTime":J
    :catch_0
    move-exception v4

    .line 694
    .local v4, "e":Ljava/lang/Exception;
    const-string v12, "SmartClipDragDrop"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "saveImageFile : File saving failed : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 697
    const/4 v8, 0x0

    goto/16 :goto_0
.end method

.method private startImageDrag(Landroid/view/View;Landroid/graphics/Rect;I)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "rectToCrop"    # Landroid/graphics/Rect;
    .param p3, "layerToCapture"    # I

    .prologue
    .line 427
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$SaveImageFileTask;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$SaveImageFileTask;-><init>(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/view/View;Landroid/graphics/Rect;I)V

    .line 428
    .local v0, "saveImageTread":Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$SaveImageFileTask;
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$SaveImageFileTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 430
    return-void
.end method

.method private startTextDrag(Landroid/view/View;Ljava/lang/String;Landroid/graphics/Rect;I)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "textToDrag"    # Ljava/lang/String;
    .param p3, "rectToCrop"    # Landroid/graphics/Rect;
    .param p4, "layerToCapture"    # I

    .prologue
    const v5, 0x7f080053

    const/4 v7, 0x0

    .line 380
    const-string v4, "Multiwindow drag and drop text"

    invoke-static {v4, p2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    .line 382
    .local v1, "dragData":Landroid/content/ClipData;
    if-eqz v1, :cond_0

    .line 383
    const/4 v3, 0x0

    .line 385
    .local v3, "useTextViewShadow":Z
    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 386
    invoke-direct {p0, p4}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getFullScreenShotBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 388
    .local v2, "fullScreenBitmap":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_1

    .line 389
    invoke-virtual {p1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 390
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->minimize()V

    .line 391
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mIvt:[B

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v6}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 392
    invoke-direct {p0, p2}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getTextThumbnailBuilder(Ljava/lang/CharSequence;)Landroid/view/View$DragShadowBuilder;

    move-result-object v4

    invoke-virtual {p1, v1, v4, p1, v7}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 423
    .end local v2    # "fullScreenBitmap":Landroid/graphics/Bitmap;
    .end local v3    # "useTextViewShadow":Z
    :cond_0
    :goto_0
    return-void

    .line 394
    .restart local v2    # "fullScreenBitmap":Landroid/graphics/Bitmap;
    .restart local v3    # "useTextViewShadow":Z
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    invoke-static {v4, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 398
    .end local v2    # "fullScreenBitmap":Landroid/graphics/Bitmap;
    :cond_2
    const/4 v0, 0x0

    .line 400
    .local v0, "dragBitmap":Landroid/graphics/Bitmap;
    if-eqz p3, :cond_3

    .line 401
    invoke-direct {p0, p3, p4}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->cropScreen(Landroid/graphics/Rect;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 403
    if-nez v0, :cond_3

    .line 404
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v4

    if-lez v4, :cond_3

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v4

    if-lez v4, :cond_3

    .line 405
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    invoke-static {v4, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 411
    :cond_3
    invoke-virtual {p1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 412
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->minimize()V

    .line 414
    if-eqz v0, :cond_4

    .line 415
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mIvt:[B

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v6}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 416
    new-instance v4, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$ShadowBuilder;

    invoke-direct {v4, p0, p1, v0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$ShadowBuilder;-><init>(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/view/View;Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v1, v4, p1, v7}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    goto :goto_0

    .line 418
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mIvt:[B

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v6}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 419
    invoke-direct {p0, p2}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getTextThumbnailBuilder(Ljava/lang/CharSequence;)Landroid/view/View$DragShadowBuilder;

    move-result-object v4

    invoke-virtual {p1, v1, v4, p1, v7}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    goto :goto_0
.end method

.method private stopSmartClipDragDropThread()V
    .locals 2

    .prologue
    .line 796
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mServiceLooper:Landroid/os/Looper;

    if-eqz v0, :cond_0

    .line 797
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mServiceLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 799
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_1

    .line 800
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->interrupt()V

    .line 802
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 803
    return-void
.end method


# virtual methods
.method public initDragDrop()V
    .locals 2

    .prologue
    .line 277
    const v0, 0x7f0f0086

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mMainFrame:Landroid/widget/FrameLayout;

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mMainFrame:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$4;-><init>(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mMainFrame:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$5;-><init>(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 356
    return-void
.end method

.method public onCreate()V
    .locals 9

    .prologue
    const/4 v7, 0x1

    .line 196
    invoke-super {p0}, Landroid/app/SallyService;->onCreate()V

    .line 198
    const-string v5, "SmartClipDragDrop"

    const-string v6, "onCreate()"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    .line 201
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    const-string v6, "window"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mWindowManager:Landroid/view/WindowManager;

    .line 202
    iput-boolean v7, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mServiceRunning:Z

    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 205
    .local v3, "l":Landroid/view/WindowManager$LayoutParams;
    const/16 v5, 0x89c

    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getWindow()Landroid/view/Window;

    move-result-object v5

    const v6, 0x106000d

    invoke-virtual {v5, v6}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 209
    const v5, 0x7f030011

    invoke-virtual {p0, v5}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->setContentView(I)V

    .line 211
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->initDragDrop()V

    .line 215
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAm:Landroid/app/IActivityManager;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mForegroundToken:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v7

    const/4 v8, 0x1

    invoke-interface {v5, v6, v7, v8}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    :goto_0
    new-instance v2, Landroid/content/IntentFilter;

    const-string v5, "com.sec.android.intent.action.HOME_RESUME"

    invoke-direct {v2, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 222
    .local v2, "homeResumeReceiver":Landroid/content/IntentFilter;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mHomeResumeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v2}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 225
    new-instance v0, Landroid/content/IntentFilter;

    const-string v5, "com.sec.android.intent.action.APPLIST_OPENED"

    invoke-direct {v0, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 226
    .local v0, "appListExpandFilter":Landroid/content/IntentFilter;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppListExpandReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 229
    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "com.sec.android.action.NOTIFY_MULTIWINDOW_STATUS"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 230
    .local v4, "multiwindowStatusFilter":Landroid/content/IntentFilter;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mMultiWindowStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v4}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 232
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    const-string v6, "vibrator"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/SystemVibrator;

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mVibrator:Landroid/os/SystemVibrator;

    .line 234
    new-instance v5, Lcom/sec/android/app/FlashBarService/SmartClipNewFileName;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/sec/android/app/FlashBarService/SmartClipNewFileName;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mSmartClipNewFileName:Lcom/sec/android/app/FlashBarService/SmartClipNewFileName;

    .line 238
    return-void

    .line 216
    .end local v0    # "appListExpandFilter":Landroid/content/IntentFilter;
    .end local v2    # "homeResumeReceiver":Landroid/content/IntentFilter;
    .end local v4    # "multiwindowStatusFilter":Landroid/content/IntentFilter;
    :catch_0
    move-exception v1

    .line 217
    .local v1, "e":Landroid/os/RemoteException;
    const-string v5, "SmartClipDragDrop"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onCreate : Exception - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    .line 244
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mHomeResumeReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 245
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mHomeResumeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 246
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mHomeResumeReceiver:Landroid/content/BroadcastReceiver;

    .line 249
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppListExpandReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 250
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppListExpandReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 251
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAppListExpandReceiver:Landroid/content/BroadcastReceiver;

    .line 254
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mMultiWindowStatusReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_2

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mMultiWindowStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 256
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mMultiWindowStatusReceiver:Landroid/content/BroadcastReceiver;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 264
    :cond_2
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mAm:Landroid/app/IActivityManager;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mForegroundToken:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 269
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mSmartClipNewFileName:Lcom/sec/android/app/FlashBarService/SmartClipNewFileName;

    sget-object v2, Lcom/sec/android/app/FlashBarService/SmartClipNewFileName$IndexMode;->DRAGDRAP:Lcom/sec/android/app/FlashBarService/SmartClipNewFileName$IndexMode;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/SmartClipNewFileName;->saveIndex(Lcom/sec/android/app/FlashBarService/SmartClipNewFileName$IndexMode;)V

    .line 271
    invoke-super {p0}, Landroid/app/SallyService;->onDestroy()V

    .line 272
    return-void

    .line 258
    :catch_0
    move-exception v0

    .line 259
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SmartClipDragDrop"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDestroy : Exception = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 265
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 266
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SmartClipDragDrop"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDestroy : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 502
    invoke-super {p0, p1, p2, p3}, Landroid/app/SallyService;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method

.method public stopService()V
    .locals 3

    .prologue
    .line 367
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mIsDragging:Z

    if-nez v1, :cond_0

    .line 368
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 369
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.action.NOTIFY_STOP_DRAG_MODE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 370
    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->sendBroadcast(Landroid/content/Intent;)V

    .line 372
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mServiceRunning:Z

    .line 374
    invoke-super {p0}, Landroid/app/SallyService;->stopService()V

    .line 375
    const-string v1, "SmartClipDragDrop"

    const-string v2, "SmartClipDragDrop service finished."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
