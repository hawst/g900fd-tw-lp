.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;
.super Landroid/view/View$DragShadowBuilder;
.source "LegacyAppListWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->onLongClick(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

.field final synthetic val$thumbnailview:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;Landroid/view/View;Landroid/view/View;)V
    .locals 0
    .param p2, "x0"    # Landroid/view/View;

    .prologue
    .line 1406
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iput-object p3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->val$thumbnailview:Landroid/view/View;

    invoke-direct {p0, p2}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onDrawShadow(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1414
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->val$thumbnailview:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 1415
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 1416
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 1417
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1418
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskId:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->isLaunchingBlockedTask(I)Z
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskId:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1419
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1420
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLunchBlock:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 1423
    :cond_0
    return-void
.end method

.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 2
    .param p1, "shadowSize"    # Landroid/graphics/Point;
    .param p2, "shadowTouchPoint"    # Landroid/graphics/Point;

    .prologue
    .line 1408
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShadowWidth:I
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4902(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 1409
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShadowHeight:I
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5002(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 1410
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShadowWidth:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShadowHeight:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 1411
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShadowWidth:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShadowHeight:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 1412
    return-void
.end method
