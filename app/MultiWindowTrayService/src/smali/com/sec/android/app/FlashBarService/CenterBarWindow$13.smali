.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$13;
.super Ljava/lang/Object;
.source "CenterBarWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;->makeButtonPopupLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0

    .prologue
    .line 1746
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 1748
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "multiwindow_facade"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 1750
    .local v1, "mwf":Lcom/samsung/android/multiwindow/MultiWindowFacade;
    invoke-virtual {v1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getArrangeState()I

    move-result v0

    .line 1751
    .local v0, "currentState":I
    if-eqz v0, :cond_0

    .line 1752
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 1753
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideButtonsPopupWindow(Z)V

    .line 1754
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3800(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3800(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v4

    xor-int/lit8 v4, v4, -0x1

    and-int/lit8 v4, v4, 0xf

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->exchangeTopTaskToZone(II)Z

    .line 1756
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3800(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v3

    xor-int/lit8 v3, v3, -0x1

    and-int/lit8 v3, v3, 0xf

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3802(Lcom/sec/android/app/FlashBarService/CenterBarWindow;I)I

    .line 1758
    sget-boolean v2, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v2, :cond_0

    .line 1759
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    new-instance v3, Lcom/sec/android/app/FlashBarService/CenterBarWindow$13$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$13$1;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow$13;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1772
    :cond_0
    :goto_0
    return-void

    .line 1768
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->showSwitchWindow()V

    .line 1769
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideButtonsPopupWindow(Z)V

    goto :goto_0
.end method
