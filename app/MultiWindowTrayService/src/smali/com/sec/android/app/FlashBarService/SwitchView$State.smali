.class final enum Lcom/sec/android/app/FlashBarService/SwitchView$State;
.super Ljava/lang/Enum;
.source "SwitchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/SwitchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/FlashBarService/SwitchView$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/FlashBarService/SwitchView$State;

.field public static final enum ANIMATING:Lcom/sec/android/app/FlashBarService/SwitchView$State;

.field public static final enum ANIMATING_BACK:Lcom/sec/android/app/FlashBarService/SwitchView$State;

.field public static final enum ANIMATING_SWITCH:Lcom/sec/android/app/FlashBarService/SwitchView$State;

.field public static final enum ANIMATING_ZOOM:Lcom/sec/android/app/FlashBarService/SwitchView$State;

.field public static final enum MOVING:Lcom/sec/android/app/FlashBarService/SwitchView$State;

.field public static final enum NONE:Lcom/sec/android/app/FlashBarService/SwitchView$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 124
    new-instance v0, Lcom/sec/android/app/FlashBarService/SwitchView$State;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/FlashBarService/SwitchView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/FlashBarService/SwitchView$State;->NONE:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    .line 125
    new-instance v0, Lcom/sec/android/app/FlashBarService/SwitchView$State;

    const-string v1, "MOVING"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/FlashBarService/SwitchView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/FlashBarService/SwitchView$State;->MOVING:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    .line 126
    new-instance v0, Lcom/sec/android/app/FlashBarService/SwitchView$State;

    const-string v1, "ANIMATING"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/FlashBarService/SwitchView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/FlashBarService/SwitchView$State;->ANIMATING:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    .line 127
    new-instance v0, Lcom/sec/android/app/FlashBarService/SwitchView$State;

    const-string v1, "ANIMATING_SWITCH"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/FlashBarService/SwitchView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/FlashBarService/SwitchView$State;->ANIMATING_SWITCH:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    .line 128
    new-instance v0, Lcom/sec/android/app/FlashBarService/SwitchView$State;

    const-string v1, "ANIMATING_BACK"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/FlashBarService/SwitchView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/FlashBarService/SwitchView$State;->ANIMATING_BACK:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    .line 129
    new-instance v0, Lcom/sec/android/app/FlashBarService/SwitchView$State;

    const-string v1, "ANIMATING_ZOOM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/SwitchView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/FlashBarService/SwitchView$State;->ANIMATING_ZOOM:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    .line 123
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/FlashBarService/SwitchView$State;

    sget-object v1, Lcom/sec/android/app/FlashBarService/SwitchView$State;->NONE:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/FlashBarService/SwitchView$State;->MOVING:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/FlashBarService/SwitchView$State;->ANIMATING:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/FlashBarService/SwitchView$State;->ANIMATING_SWITCH:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/FlashBarService/SwitchView$State;->ANIMATING_BACK:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/FlashBarService/SwitchView$State;->ANIMATING_ZOOM:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/FlashBarService/SwitchView$State;->$VALUES:[Lcom/sec/android/app/FlashBarService/SwitchView$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 123
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/FlashBarService/SwitchView$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 123
    const-class v0, Lcom/sec/android/app/FlashBarService/SwitchView$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/SwitchView$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/FlashBarService/SwitchView$State;
    .locals 1

    .prologue
    .line 123
    sget-object v0, Lcom/sec/android/app/FlashBarService/SwitchView$State;->$VALUES:[Lcom/sec/android/app/FlashBarService/SwitchView$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/FlashBarService/SwitchView$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/FlashBarService/SwitchView$State;

    return-object v0
.end method
