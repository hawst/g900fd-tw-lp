.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$49;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->createConfirmDialog(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 3702
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$49;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 3705
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$49;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$49;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDelIndex:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$10200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$49;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbFromEdit:Z
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$10300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->removeTemplate(IZ)V

    .line 3706
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$49;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbFromEdit:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$10300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3707
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$49;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->updateEditListChanged()V

    .line 3711
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$49;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissConfirmDialog()V

    .line 3712
    return-void

    .line 3709
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$49;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListRelayout(Z)V

    goto :goto_0
.end method
