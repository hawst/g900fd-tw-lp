.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->hideAppEditList(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 5413
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 9
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    const v8, 0x7f080001

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 5415
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeAppListEditWindow()V

    .line 5416
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->initAppListEditWindow()V

    .line 5418
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200bb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 5419
    .local v0, "add":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0029

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v2, v3

    .line 5420
    .local v2, "paddingTop":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a002a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v1, v3

    .line 5422
    .local v1, "paddingBottom":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v6, v0, v6, v6}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 5423
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v5, v2, v5, v1}, Landroid/widget/Button;->setPadding(IIII)V

    .line 5424
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 5425
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setSoundEffectsEnabled(Z)V

    .line 5426
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 5427
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/TextView;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 5428
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5429
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditBtn:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageButton;

    move-result-object v3

    const v4, 0x7f020034

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 5432
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v4, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v3, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 5433
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v3, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 5434
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iput-boolean v5, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbAnimating:Z

    .line 5435
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->hideAppEditPost()V

    .line 5436
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->updateAppListForScrollView(Ljava/util/List;Z)V

    .line 5438
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 5439
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 5441
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$88;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbAnimating:Z

    .line 5442
    return-void
.end method
