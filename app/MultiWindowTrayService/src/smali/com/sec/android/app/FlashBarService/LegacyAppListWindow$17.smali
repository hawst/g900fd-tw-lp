.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$17;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->createGestureHelp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 1152
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1155
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "do_not_show_gesture_help"

    const/4 v3, 0x1

    const/4 v4, -0x2

    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 1156
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->dismissHelpWindow()V

    .line 1157
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGestureOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2402(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;)Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    .line 1159
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.helphub.HELP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1160
    .local v0, "helpIntent":Landroid/content/Intent;
    const/high16 v1, 0x30200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1162
    const-string v1, "helphub:section"

    const-string v2, "multi_window"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1163
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 1164
    return-void
.end method
