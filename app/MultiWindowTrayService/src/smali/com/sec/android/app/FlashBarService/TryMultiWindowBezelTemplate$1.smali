.class Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$1;
.super Ljava/lang/Object;
.source "TryMultiWindowBezelTemplate.java"

# interfaces
.implements Landroid/support/v4/widget/DrawerLayout$DrawerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->setDrawerLayoutListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrawerClosed(Landroid/view/View;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 342
    return-void
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 334
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopupHolder2:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopup2:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$500(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWTemplateCue2:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mFadeInAnimation:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDrawerBezelTemplateLayout:Landroid/support/v4/widget/DrawerLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 338
    return-void
.end method

.method public onDrawerSlide(Landroid/view/View;F)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # F

    .prologue
    .line 329
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopupHolder1:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 330
    return-void
.end method

.method public onDrawerStateChanged(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 318
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDrawerBezelTemplateLayout:Landroid/support/v4/widget/DrawerLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->isDrawerVisible(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopupHolder1:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 325
    :goto_0
    return-void

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopup1:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$1;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopupHolder1:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method
