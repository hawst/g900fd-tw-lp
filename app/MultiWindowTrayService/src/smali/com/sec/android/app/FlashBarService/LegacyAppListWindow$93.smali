.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$93;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 5802
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$93;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 5805
    const/4 v1, 0x0

    .line 5806
    .local v1, "iconIndex":I
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 5807
    .local v0, "count":I
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$93;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyUsedAppCnt:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    if-lez v2, :cond_0

    .line 5808
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$93;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerSelectedPage:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$93;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowItemNum:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v3

    mul-int/2addr v2, v3

    add-int v1, v0, v2

    .line 5812
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$93;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    const/16 v3, 0x66

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$93;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSelectedAppPackage:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivityViewPagerAppList(IILjava/lang/String;)V

    .line 5813
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$93;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissViewPagerAppList()V

    .line 5814
    return-void

    .line 5810
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$93;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerSelectedPage:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$93;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowItemNum:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v3

    mul-int/2addr v2, v3

    add-int v1, v0, v2

    goto :goto_0
.end method
