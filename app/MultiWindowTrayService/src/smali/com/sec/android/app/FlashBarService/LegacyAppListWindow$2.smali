.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$2;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 422
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 425
    const/16 v1, 0x42

    if-ne p2, v1, :cond_1

    .line 426
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v2, :cond_0

    move-object v1, p1

    .line 427
    check-cast v1, Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getSelectedItemId()J

    move-result-wide v4

    long-to-int v0, v4

    .line 428
    .local v0, "id":I
    check-cast p1, Landroid/widget/GridView;

    .end local p1    # "view":Landroid/view/View;
    invoke-virtual {p1}, Landroid/widget/GridView;->getSelectedItemPosition()I

    move-result v1

    if-ltz v1, :cond_0

    if-ltz v0, :cond_0

    .line 429
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    const/16 v3, 0x68

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSelectedAppPackage:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v3, v4}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivityViewPagerAppList(IILjava/lang/String;)V

    .line 430
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissViewPagerAppList()V

    .end local v0    # "id":I
    :cond_0
    move v1, v2

    .line 435
    :goto_0
    return v1

    .restart local p1    # "view":Landroid/view/View;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
