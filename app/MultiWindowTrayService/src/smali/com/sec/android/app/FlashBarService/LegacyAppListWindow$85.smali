.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$85;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->makeGuideLineLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 4890
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$85;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v1, 0x0

    .line 4903
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$85;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4905
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$85;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideVisibility(ZZZ)V
    invoke-static {v0, v1, v1, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ZZZ)V

    .line 4907
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$85;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->clearAnimation()V

    .line 4908
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 4900
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v1, 0x0

    .line 4893
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$85;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4895
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$85;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideVisibility(ZZZ)V
    invoke-static {v0, v1, v1, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ZZZ)V

    .line 4897
    :cond_0
    return-void
.end method
