.class Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24$2;
.super Ljava/lang/Object;
.source "MultiWindowTrayService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;->onArrangeStateUpdate(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;)V
    .locals 0

    .prologue
    .line 1542
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24$2;->this$1:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1545
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24$2;->this$1:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1546
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24$2;->this$1:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->dismissTemplateDialog()V

    .line 1547
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24$2;->this$1:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->dismissConfirmDialog()V

    .line 1549
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24$2;->this$1:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1550
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24$2;->this$1:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideButtonsPopupWindow(Z)V

    .line 1552
    :cond_1
    return-void
.end method
