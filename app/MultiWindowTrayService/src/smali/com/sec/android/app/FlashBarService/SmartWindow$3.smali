.class Lcom/sec/android/app/FlashBarService/SmartWindow$3;
.super Ljava/lang/Object;
.source "SmartWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/SmartWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SmartWindow;)V
    .locals 0

    .prologue
    .line 322
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mHasEditWindow:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/SmartWindow;->setEditMode(Z)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/FlashBarService/SmartWindow;->updateSmartWindowPosition(Z)V

    .line 332
    :goto_0
    return-void

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/FlashBarService/SmartWindow;->setEditMode(Z)V

    goto :goto_0
.end method
