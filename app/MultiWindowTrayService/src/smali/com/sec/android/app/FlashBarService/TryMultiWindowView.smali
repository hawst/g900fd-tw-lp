.class public Lcom/sec/android/app/FlashBarService/TryMultiWindowView;
.super Landroid/app/Activity;
.source "TryMultiWindowView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;,
        Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;
    }
.end annotation


# static fields
.field private static mTryMWViewSplitControlY:F


# instance fields
.field private FRAME_LOCATION_DOWN:I

.field private FRAME_LOCATION_UP:I

.field public GUI_SCALE_PREVIEW:Z

.field private app1DragArea:Z

.field private app2DragArea:Z

.field private isApp1Drawn:Z

.field private isApp1DrawnSuccess:Z

.field private isApp2Drawn:Z

.field private isFirstTipShown:Z

.field private isFourthTipShown:Z

.field private isSecondTipShown:Z

.field private mApp1ToggleForVibrate:Z

.field private mApp2ToggleForVibrate:Z

.field private mAppListPosition:I

.field private mAppTag1:Ljava/lang/String;

.field private mBottomGuideline:Landroid/widget/FrameLayout;

.field private mBubbleAnimation:Landroid/view/animation/Animation;

.field private mCenterBarGuideSize:I

.field private mCenterBarMaxPositionH:I

.field private mCenterBarMaxPositionW:I

.field private mCenterBarMinPositionH:I

.field private mCenterBarMinPositionW:I

.field private mCenterBarSize:I

.field mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mDisplayHeight:I

.field private mFrameLocation:I

.field private mHandle:Landroid/os/Handler;

.field mInvalidToast:Landroid/widget/Toast;

.field private mIvt:[B

.field private mMWDialogPopup1:Landroid/widget/RelativeLayout;

.field private mMWDialogPopup2:Landroid/widget/RelativeLayout;

.field private mMWFeatureLevel:Z

.field private mOrientation:I

.field private mResource:Landroid/content/res/Resources;

.field private mTopGuideline:Landroid/widget/FrameLayout;

.field private mTryMWApp1:Landroid/widget/ImageView;

.field private mTryMWApp2:Landroid/widget/ImageView;

.field private mTryMWViewAnimTip1:Landroid/widget/ImageView;

.field private mTryMWViewAnimTip2:Landroid/widget/ImageView;

.field private mTryMWViewAnimTip3:Landroid/widget/ImageView;

.field private mTryMWViewAnimTip3Params:Landroid/widget/RelativeLayout$LayoutParams;

.field private mTryMWViewApp2:Landroid/widget/ImageView;

.field private mTryMWViewAppViewer:Landroid/widget/LinearLayout;

.field private mTryMWViewContainer:Landroid/widget/RelativeLayout;

.field private mTryMWViewContainerWidthCenter:I

.field private mTryMWViewDragApp1:Landroid/widget/RelativeLayout;

.field private mTryMWViewDragApp2:Landroid/widget/RelativeLayout;

.field private mTryMWViewDragApp3:Landroid/widget/RelativeLayout;

.field private mTryMWViewEdit:Landroid/widget/ImageView;

.field private mTryMWViewFlashBar:Landroid/widget/ImageView;

.field private mTryMWViewFocusFrame:Landroid/widget/ImageView;

.field private mTryMWViewFocusFrame2:Landroid/widget/ImageView;

.field private mTryMWViewFocusFrameLinearLayout:Landroid/widget/LinearLayout;

.field private mTryMWViewHandle:Landroid/widget/ImageView;

.field private mTryMWViewHandleClose:Landroid/widget/ImageView;

.field private mTryMWViewHome:Landroid/widget/ImageView;

.field private mTryMWViewImageContainer:Landroid/widget/LinearLayout;

.field private mTryMWViewPocketButton:Landroid/widget/RelativeLayout;

.field private mTryMWViewPop1Text:Landroid/widget/TextView;

.field private mTryMWViewPop2Text:Landroid/widget/TextView;

.field private mTryMWViewPop3Text:Landroid/widget/TextView;

.field private mTryMWViewPop4Text:Landroid/widget/TextView;

.field private mTryMWViewPopUp3:Landroid/widget/RelativeLayout;

.field private mTryMWViewPopUp3Params:Landroid/widget/RelativeLayout$LayoutParams;

.field private mTryMWViewPopUp4:Landroid/widget/RelativeLayout;

.field private mTryMWViewPopUp4Params:Landroid/widget/RelativeLayout$LayoutParams;

.field private mTryMWViewSplitArrow:Landroid/widget/ImageView;

.field private mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

.field private mTryMWViewSplitArrowParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private mTryMWViewSplitControl:Landroid/widget/ImageView;

.field private mTryMWViewSplitControlLand:Landroid/widget/ImageView;

.field private mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

.field private mTryMWdivider:Landroid/widget/ImageView;

.field private mVibrator:Landroid/os/SystemVibrator;

.field protected mWindowManager:Landroid/view/WindowManager;

.field private r:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlY:F

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 43
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isFirstTipShown:Z

    .line 44
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isSecondTipShown:Z

    .line 45
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isFourthTipShown:Z

    .line 46
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp1Drawn:Z

    .line 47
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp1DrawnSuccess:Z

    .line 48
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp2Drawn:Z

    .line 49
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->app1DragArea:Z

    .line 50
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->app2DragArea:Z

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPop1Text:Landroid/widget/TextView;

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPop2Text:Landroid/widget/TextView;

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPop3Text:Landroid/widget/TextView;

    .line 54
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPop4Text:Landroid/widget/TextView;

    .line 58
    const/16 v0, 0x67

    iput v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mAppListPosition:I

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTopGuideline:Landroid/widget/FrameLayout;

    .line 66
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBottomGuideline:Landroid/widget/FrameLayout;

    .line 68
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWdivider:Landroid/widget/ImageView;

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    .line 72
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    .line 74
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    .line 75
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    .line 76
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFlashBar:Landroid/widget/ImageView;

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandle:Landroid/widget/ImageView;

    .line 78
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandleClose:Landroid/widget/ImageView;

    .line 79
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;

    .line 80
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWApp1:Landroid/widget/ImageView;

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWApp2:Landroid/widget/ImageView;

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    .line 83
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    .line 85
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewImageContainer:Landroid/widget/LinearLayout;

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAppViewer:Landroid/widget/LinearLayout;

    .line 87
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrameLinearLayout:Landroid/widget/LinearLayout;

    .line 89
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    .line 90
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewDragApp1:Landroid/widget/RelativeLayout;

    .line 91
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewDragApp2:Landroid/widget/RelativeLayout;

    .line 92
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewDragApp3:Landroid/widget/RelativeLayout;

    .line 93
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPocketButton:Landroid/widget/RelativeLayout;

    .line 95
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWDialogPopup1:Landroid/widget/RelativeLayout;

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWDialogPopup2:Landroid/widget/RelativeLayout;

    .line 98
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3:Landroid/widget/RelativeLayout;

    .line 99
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp4:Landroid/widget/RelativeLayout;

    .line 100
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 101
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3Params:Landroid/widget/RelativeLayout$LayoutParams;

    .line 102
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3Params:Landroid/widget/RelativeLayout$LayoutParams;

    .line 103
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp4Params:Landroid/widget/RelativeLayout$LayoutParams;

    .line 105
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip1:Landroid/widget/ImageView;

    .line 106
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip2:Landroid/widget/ImageView;

    .line 107
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3:Landroid/widget/ImageView;

    .line 114
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mInvalidToast:Landroid/widget/Toast;

    .line 116
    const-string v0, "100"

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mAppTag1:Ljava/lang/String;

    .line 118
    iput v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mOrientation:I

    .line 120
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mHandle:Landroid/os/Handler;

    .line 123
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    .line 127
    iput v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_UP:I

    .line 128
    iput v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_DOWN:I

    .line 132
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 136
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->GUI_SCALE_PREVIEW:Z

    .line 138
    const/16 v0, 0x26

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mIvt:[B

    .line 146
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mApp1ToggleForVibrate:Z

    .line 147
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mApp2ToggleForVibrate:Z

    .line 1122
    new-instance v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$1;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->r:Ljava/lang/Runnable;

    return-void

    .line 138
    nop

    :array_0
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0xat
        0x0t
        0x14t
        0x0t
        0x0t
        0x0t
        0x20t
        0x1t
        0x0t
        0x0t
        0x7ft
        0x0t
        0x0t
        0x42t
        0x0t
        0x0t
        0x4dt
        0x0t
        0x61t
        0x0t
        0x67t
        0x0t
        0x53t
        0x0t
        0x77t
        0x0t
        0x65t
        0x0t
        0x65t
        0x0t
        0x70t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mAppTag1:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->app1DragArea:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->app1DragArea:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mApp1ToggleForVibrate:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mApp1ToggleForVibrate:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mOrientation:I

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I

    return v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;
    .param p1, "x1"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I

    return p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_DOWN:I

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_UP:I

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewDragApp1:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isSecondTipShown:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFlashBar:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isSecondTipShown:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPocketButton:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewImageContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandle:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandleClose:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->app2DragArea:Z

    return v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->app2DragArea:Z

    return p1
.end method

.method static synthetic access$2600(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mApp2ToggleForVibrate:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mApp2ToggleForVibrate:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewDragApp2:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBottomGuideline:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isFirstTipShown:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTopGuideline:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isFirstTipShown:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBubbleAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWDialogPopup2:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWDialogPopup1:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWApp2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3602(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp2Drawn:Z

    return p1
.end method

.method static synthetic access$3700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAppViewer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrameLinearLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp1Drawn:Z

    return v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainerWidthCenter:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp1Drawn:Z

    return p1
.end method

.method static synthetic access$4100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->setMarginsForViewInLandMode()V

    return-void
.end method

.method static synthetic access$4700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->setMarginsForViewInPortMode()V

    return-void
.end method

.method static synthetic access$4800(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewDragApp3:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mIvt:[B

    return-object v0
.end method

.method static synthetic access$5000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWApp1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/os/SystemVibrator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mVibrator:Landroid/os/SystemVibrator;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp1DrawnSuccess:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp1DrawnSuccess:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mAppListPosition:I

    return v0
.end method

.method private init()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 183
    const v1, 0x7f0f00fd

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;

    .line 184
    const v1, 0x7f0f00fe

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;

    .line 185
    const v1, 0x7f0f0100

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    .line 186
    const v1, 0x7f0f0101

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;

    .line 187
    const v1, 0x7f0f0103

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    .line 188
    const v1, 0x7f0f0173

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    .line 189
    const v1, 0x7f0f0104

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    .line 190
    const v1, 0x7f0f0175

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFlashBar:Landroid/widget/ImageView;

    .line 191
    const v1, 0x7f0f0176

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandle:Landroid/widget/ImageView;

    .line 192
    const v1, 0x7f0f0177

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandleClose:Landroid/widget/ImageView;

    .line 194
    const v1, 0x7f0f0178

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewImageContainer:Landroid/widget/LinearLayout;

    .line 196
    const v1, 0x7f0f0102

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;

    .line 197
    const v1, 0x7f0f017a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewDragApp1:Landroid/widget/RelativeLayout;

    .line 198
    const v1, 0x7f0f017e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewDragApp2:Landroid/widget/RelativeLayout;

    .line 199
    const v1, 0x7f0f0182

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewDragApp3:Landroid/widget/RelativeLayout;

    .line 200
    const v1, 0x7f0f0179

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPocketButton:Landroid/widget/RelativeLayout;

    .line 202
    const v1, 0x7f0f017b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWDialogPopup1:Landroid/widget/RelativeLayout;

    .line 203
    const v1, 0x7f0f017f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWDialogPopup2:Landroid/widget/RelativeLayout;

    .line 205
    const v1, 0x7f0f0183

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3:Landroid/widget/RelativeLayout;

    .line 206
    const v1, 0x7f0f0187

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp4:Landroid/widget/RelativeLayout;

    .line 207
    const v1, 0x7f0f00fb

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    .line 208
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 209
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 210
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 211
    const v1, 0x7f0f0185

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    .line 212
    const v1, 0x7f0f0174

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    .line 214
    const v1, 0x7f0f00f9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWApp1:Landroid/widget/ImageView;

    .line 215
    const v1, 0x7f0f00fa

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWApp2:Landroid/widget/ImageView;

    .line 216
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWApp1:Landroid/widget/ImageView;

    new-instance v2, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;

    invoke-direct {v2, p0, v4}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Lcom/sec/android/app/FlashBarService/TryMultiWindowView$1;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 217
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWApp2:Landroid/widget/ImageView;

    new-instance v2, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;

    invoke-direct {v2, p0, v4}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Lcom/sec/android/app/FlashBarService/TryMultiWindowView$1;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 219
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 220
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWApp2:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 222
    const v1, 0x7f0f017d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip1:Landroid/widget/ImageView;

    .line 223
    const v1, 0x7f0f0181

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip2:Landroid/widget/ImageView;

    .line 224
    const v1, 0x7f0f0186

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3:Landroid/widget/ImageView;

    .line 226
    const v1, 0x7f040052

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 227
    .local v0, "myFadeInAnimation":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip1:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip2:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 229
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 230
    const v1, 0x7f0f00fc

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAppViewer:Landroid/widget/LinearLayout;

    .line 231
    const v1, 0x7f0f00ff

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrameLinearLayout:Landroid/widget/LinearLayout;

    .line 232
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mOrientation:I

    .line 233
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a0001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarSize:I

    .line 234
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a0002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarGuideSize:I

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainerWidthCenter:I

    .line 237
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 238
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3Params:Landroid/widget/RelativeLayout$LayoutParams;

    .line 240
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3Params:Landroid/widget/RelativeLayout$LayoutParams;

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp4:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp4Params:Landroid/widget/RelativeLayout$LayoutParams;

    .line 242
    const v1, 0x7f040024

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 244
    const-string v1, "vibrator"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/SystemVibrator;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mVibrator:Landroid/os/SystemVibrator;

    .line 246
    const v1, 0x7f0f017c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPop1Text:Landroid/widget/TextView;

    .line 247
    const v1, 0x7f0f0180

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPop2Text:Landroid/widget/TextView;

    .line 248
    const v1, 0x7f0f0184

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPop3Text:Landroid/widget/TextView;

    .line 249
    const v1, 0x7f0f011c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPop4Text:Landroid/widget/TextView;

    .line 250
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPop1Text:Landroid/widget/TextView;

    new-instance v2, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v2}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 251
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPop2Text:Landroid/widget/TextView;

    new-instance v2, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v2}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 252
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPop3Text:Landroid/widget/TextView;

    new-instance v2, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v2}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 253
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPop4Text:Landroid/widget/TextView;

    new-instance v2, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v2}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 255
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->GUI_SCALE_PREVIEW:Z

    if-eqz v1, :cond_0

    .line 256
    const v1, 0x7f0f008e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTopGuideline:Landroid/widget/FrameLayout;

    .line 257
    const v1, 0x7f0f0091

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBottomGuideline:Landroid/widget/FrameLayout;

    .line 258
    const v1, 0x7f0f0172

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWdivider:Landroid/widget/ImageView;

    .line 259
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandle:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 260
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandleClose:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 262
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->initCenterbarFlingSize()V

    .line 263
    return-void
.end method

.method private initCenterbarFlingSize()V
    .locals 5

    .prologue
    .line 266
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 267
    .local v2, "displaySize":Landroid/graphics/Point;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 268
    iget v3, v2, Landroid/graphics/Point;->x:I

    mul-int/lit8 v3, v3, 0x19

    div-int/lit8 v1, v3, 0x64

    .line 269
    .local v1, "centerBarFlingSizeW":I
    iget v3, v2, Landroid/graphics/Point;->y:I

    mul-int/lit8 v3, v3, 0x19

    div-int/lit8 v0, v3, 0x64

    .line 271
    .local v0, "centerBarFlingSizeH":I
    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v1, v3

    iput v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarMinPositionW:I

    .line 272
    iget v3, v2, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v1

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarSize:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarMaxPositionW:I

    .line 273
    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    iput v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarMinPositionH:I

    .line 274
    iget v3, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarSize:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarMaxPositionH:I

    .line 275
    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarMaxPositionH:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarMinPositionH:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sput v3, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlY:F

    .line 276
    return-void
.end method

.method private makeScreenForLandscape()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 381
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->initCenterbarFlingSize()V

    .line 382
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 383
    .local v0, "displaySize":Landroid/graphics/Point;
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 384
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    invoke-virtual {v9, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 386
    :cond_0
    iget v9, v0, Landroid/graphics/Point;->y:I

    iput v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mDisplayHeight:I

    .line 387
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAppViewer:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v11}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 388
    iget-boolean v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v9, :cond_1

    .line 389
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrameLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v11}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 391
    :cond_1
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    const v10, 0x7f0200a5

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 392
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    const v10, 0x7f0200a6

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 395
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout$LayoutParams;

    .line 397
    .local v7, "mTryMWViewHomeParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAppViewer:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v9

    iput v9, v7, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 398
    iget v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainerWidthCenter:I

    iput v9, v7, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 399
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v9, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 401
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 403
    .local v4, "mTryMWViewApp2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAppViewer:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v9

    iput v9, v4, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 404
    iget v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainerWidthCenter:I

    iput v9, v4, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 405
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;

    invoke-virtual {v9, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 407
    iget-boolean v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->GUI_SCALE_PREVIEW:Z

    if-eqz v9, :cond_4

    .line 408
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 410
    .local v3, "mTopGuidelineParams":Landroid/widget/LinearLayout$LayoutParams;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 411
    .local v1, "displaySizeTemp":Landroid/graphics/Point;
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 412
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    invoke-virtual {v9, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 414
    :cond_2
    iget v9, v1, Landroid/graphics/Point;->y:I

    iput v9, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 415
    iget v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainerWidthCenter:I

    iput v9, v3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 416
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v9, v3}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 418
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 420
    .local v2, "mBottomGuidelineParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 421
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    invoke-virtual {v9, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 423
    :cond_3
    iget v9, v1, Landroid/graphics/Point;->y:I

    iput v9, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 424
    iget v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainerWidthCenter:I

    iput v9, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 425
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v9, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 427
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWdivider:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout$LayoutParams;

    .line 429
    .local v8, "mTryMWdividerParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v9, v8, Landroid/widget/LinearLayout$LayoutParams;->height:I

    iput v9, v8, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 430
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v9}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v9

    iput v9, v8, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 431
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWdivider:Landroid/widget/ImageView;

    invoke-virtual {v9, v8}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 434
    .end local v1    # "displaySizeTemp":Landroid/graphics/Point;
    .end local v2    # "mBottomGuidelineParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "mTopGuidelineParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v8    # "mTryMWdividerParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_4
    iget-boolean v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v9, :cond_5

    .line 435
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout$LayoutParams;

    .line 437
    .local v6, "mTryMWViewFocusFrameParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mDisplayHeight:I

    iput v9, v6, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 438
    iget v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainerWidthCenter:I

    iput v9, v6, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 439
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v9, v6}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 441
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 443
    .local v5, "mTryMWViewFocusFrame2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mDisplayHeight:I

    iput v9, v5, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 444
    iget v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainerWidthCenter:I

    iput v9, v5, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 445
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;

    invoke-virtual {v9, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 448
    .end local v5    # "mTryMWViewFocusFrame2Params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v6    # "mTryMWViewFocusFrameParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_5
    iget-boolean v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp2Drawn:Z

    if-eqz v9, :cond_6

    .line 449
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 450
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 451
    iget-boolean v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v9, :cond_6

    .line 452
    iget v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I

    iget v10, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_UP:I

    if-ne v9, v10, :cond_7

    .line 453
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 459
    :cond_6
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->setMarginsForViewInLandMode()V

    .line 460
    return-void

    .line 454
    :cond_7
    iget v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I

    iget v10, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_DOWN:I

    if-ne v9, v10, :cond_6

    .line 455
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private makeScreenForPortrait()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 283
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->initCenterbarFlingSize()V

    .line 284
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v7, v8}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 285
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAppViewer:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v10}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 286
    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v7, :cond_0

    .line 287
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrameLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v10}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 289
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    const v8, 0x7f0200a7

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 290
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    const v8, 0x7f0200a8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 292
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 294
    .local v5, "mTryMWViewHomeParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainerWidthCenter:I

    iput v7, v5, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 295
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v7, v5, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 296
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v7, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 298
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 300
    .local v2, "mTryMWViewApp2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainerWidthCenter:I

    iput v7, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 301
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v7, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 302
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;

    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 303
    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->GUI_SCALE_PREVIEW:Z

    if-eqz v7, :cond_1

    .line 304
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 306
    .local v1, "mTopGuidelineParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainerWidthCenter:I

    iput v7, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 307
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v7, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 308
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v7, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 310
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 312
    .local v0, "mBottomGuidelineParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainerWidthCenter:I

    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 313
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 314
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v7, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 316
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWdivider:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout$LayoutParams;

    .line 318
    .local v6, "mTryMWdividerParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, v6, Landroid/widget/LinearLayout$LayoutParams;->width:I

    iput v7, v6, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 319
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v7, v6, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 320
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWdivider:Landroid/widget/ImageView;

    invoke-virtual {v7, v6}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 322
    .end local v0    # "mBottomGuidelineParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v1    # "mTopGuidelineParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v6    # "mTryMWdividerParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_1
    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v7, :cond_2

    .line 323
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 325
    .local v4, "mTryMWViewFocusFrameParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainerWidthCenter:I

    iput v7, v4, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 326
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v7, v4, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 327
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 329
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 331
    .local v3, "mTryMWViewFocusFrame2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    iput v7, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 332
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v7, v4, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 333
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;

    invoke-virtual {v7, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 336
    .end local v3    # "mTryMWViewFocusFrame2Params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v4    # "mTryMWViewFocusFrameParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp2Drawn:Z

    if-eqz v7, :cond_4

    .line 337
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 338
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 339
    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v7, :cond_3

    .line 340
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I

    iget v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_UP:I

    if-ne v7, v8, :cond_5

    .line 341
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 346
    :cond_3
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->setMarginsForViewInPortMode()V

    .line 349
    :cond_4
    return-void

    .line 342
    :cond_5
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I

    iget v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_DOWN:I

    if-ne v7, v8, :cond_3

    .line 343
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setMarginsForViewInLandMode()V
    .locals 3

    .prologue
    .line 474
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a00f0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 476
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a00ef

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 478
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 480
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3Params:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3Params:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 492
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp4Params:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a00c3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 493
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp4:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp4Params:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 494
    return-void

    .line 484
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3Params:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a00f2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 485
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3Params:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a00f1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3Params:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 488
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3Params:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a00ed

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 489
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3Params:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a00ee

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 490
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3Params:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private setMarginsForViewInPortMode()V
    .locals 3

    .prologue
    .line 356
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a00ec

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a00eb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 360
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3Params:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3Params:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 372
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp4Params:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a00c2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp4:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp4Params:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 374
    return-void

    .line 364
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3Params:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a00e5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3Params:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a00e6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 366
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3Params:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3Params:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a00e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 369
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3Params:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a00e8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3Params:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public invalidTouch()V
    .locals 2

    .prologue
    .line 883
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mInvalidToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 884
    const v0, 0x7f08003a

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mInvalidToast:Landroid/widget/Toast;

    .line 886
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mInvalidToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 887
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 511
    const/16 v0, 0x65

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->setResult(I)V

    .line 512
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->finish()V

    .line 513
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 498
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 499
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mOrientation:I

    .line 500
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mOrientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 501
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->makeScreenForLandscape()V

    .line 504
    :cond_0
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mOrientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 505
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->makeScreenForPortrait()V

    .line 507
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 153
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 161
    .local v1, "w":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 162
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    .line 163
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    .line 166
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mAppListPosition:I

    const/16 v3, 0x68

    if-ne v2, v3, :cond_0

    .line 167
    const v2, 0x7f030030

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->setContentView(I)V

    .line 171
    :goto_0
    const-string v2, "window"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mWindowManager:Landroid/view/WindowManager;

    .line 172
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const/high16 v3, 0x7f090000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->GUI_SCALE_PREVIEW:Z

    .line 174
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->init()V

    .line 175
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWDialogPopup1:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 176
    return-void

    .line 169
    :cond_0
    const v2, 0x7f03002f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->setContentView(I)V

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 892
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    float-to-int v0, v4

    .line 893
    .local v0, "X":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    float-to-int v1, v4

    .line 895
    .local v1, "Y":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    and-int/lit16 v4, v4, 0xff

    packed-switch v4, :pswitch_data_0

    .line 1119
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v4, 0x1

    return v4

    .line 897
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPop1Text:Landroid/widget/TextView;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPop2Text:Landroid/widget/TextView;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPop3Text:Landroid/widget/TextView;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPop4Text:Landroid/widget/TextView;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 898
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mIvt:[B

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v6}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 899
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 900
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3:Landroid/widget/RelativeLayout;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 901
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 903
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->invalidTouch()V

    goto :goto_0

    .line 908
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 909
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarMaxPositionW:I

    if-le v0, v4, :cond_b

    .line 910
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarMaxPositionW:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setX(F)V

    .line 914
    :cond_3
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mIvt:[B

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v6}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 915
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 916
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v4, :cond_c

    .line 917
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 918
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    const v5, 0x7f02006a

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 919
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 924
    :goto_2
    const/4 v4, 0x7

    invoke-virtual {p0, v4}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->setRequestedOrientation(I)V

    .line 926
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f0a00ac

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarSize:I

    add-int/2addr v4, v5

    if-ge v1, v4, :cond_e

    .line 927
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v4, :cond_4

    .line 928
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f0a00ac

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setY(F)V

    .line 929
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getY()F

    move-result v4

    sput v4, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlY:F

    .line 930
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_UP:I

    if-ne v4, v5, :cond_d

    .line 931
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    sget v7, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlY:F

    float-to-int v7, v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a00af

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    add-int/2addr v7, v8

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 937
    :cond_4
    :goto_3
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f0a00ac

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setY(F)V

    .line 938
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    sget v8, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlY:F

    float-to-int v8, v8

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a00af

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    sub-int/2addr v7, v8

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 970
    :cond_5
    :goto_4
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isFourthTipShown:Z

    if-nez v4, :cond_6

    .line 971
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isFourthTipShown:Z

    .line 972
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp4:Landroid/widget/RelativeLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 973
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp4:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 975
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mHandle:Landroid/os/Handler;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->r:Ljava/lang/Runnable;

    const-wide/16 v6, 0xdac

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 978
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 979
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarMaxPositionH:I

    if-le v1, v4, :cond_13

    .line 980
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarMaxPositionH:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setY(F)V

    .line 984
    :cond_8
    :goto_5
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v4, :cond_14

    .line 985
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 986
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    const v5, 0x7f02006a

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 987
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 992
    :goto_6
    const/4 v4, 0x6

    invoke-virtual {p0, v4}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->setRequestedOrientation(I)V

    .line 993
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f0a00ac

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    if-ge v0, v4, :cond_15

    .line 994
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f0a00ac

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setX(F)V

    .line 995
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0a00ac

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    iget v7, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 996
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v4, :cond_9

    .line 997
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f0a00ac

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarSize:I

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setX(F)V

    .line 998
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0a00ac

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    iget v7, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1014
    :cond_9
    :goto_7
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isFourthTipShown:Z

    if-nez v4, :cond_a

    .line 1015
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isFourthTipShown:Z

    .line 1016
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp4:Landroid/widget/RelativeLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1017
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp4:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1019
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mHandle:Landroid/os/Handler;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->r:Ljava/lang/Runnable;

    const-wide/16 v6, 0xdac

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 911
    :cond_b
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarMinPositionW:I

    if-ge v0, v4, :cond_3

    .line 912
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarMinPositionW:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setX(F)V

    goto/16 :goto_1

    .line 921
    :cond_c
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 922
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    const v5, 0x7f0200a9

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_2

    .line 933
    :cond_d
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    sget v8, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlY:F

    float-to-int v8, v8

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a00af

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    sub-int/2addr v7, v8

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_3

    .line 939
    :cond_e
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f0a00ad

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    sub-int/2addr v4, v5

    if-le v1, v4, :cond_10

    .line 940
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0a00ad

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setY(F)V

    .line 941
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0a00ad

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setY(F)V

    .line 942
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getY()F

    move-result v4

    sput v4, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlY:F

    .line 943
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    sget v8, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlY:F

    float-to-int v8, v8

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a00af

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    sub-int/2addr v7, v8

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 945
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v4, :cond_5

    .line 946
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0a00ad

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarSize:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setY(F)V

    .line 947
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_UP:I

    if-ne v4, v5, :cond_f

    .line 948
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    sget v7, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlY:F

    float-to-int v7, v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a00af

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    add-int/2addr v7, v8

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_4

    .line 950
    :cond_f
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    sget v8, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlY:F

    float-to-int v8, v8

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a00af

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    sub-int/2addr v7, v8

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_4

    .line 955
    :cond_10
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    sget v8, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlY:F

    float-to-int v8, v8

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a00af

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    sub-int/2addr v7, v8

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 956
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v4, :cond_12

    .line 957
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarSize:I

    sub-int v5, v1, v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setY(F)V

    .line 958
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_UP:I

    if-ne v4, v5, :cond_11

    .line 959
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    sget v7, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlY:F

    float-to-int v7, v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a00af

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    add-int/2addr v7, v8

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_4

    .line 961
    :cond_11
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    sget v8, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlY:F

    float-to-int v8, v8

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a00af

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    sub-int/2addr v7, v8

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_4

    .line 966
    :cond_12
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    int-to-float v5, v1

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setY(F)V

    goto/16 :goto_4

    .line 981
    :cond_13
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarMinPositionH:I

    if-ge v1, v4, :cond_8

    .line 982
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarMinPositionH:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setY(F)V

    goto/16 :goto_5

    .line 989
    :cond_14
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 990
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    const v5, 0x7f0200ab

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_6

    .line 1000
    :cond_15
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f0a00ad

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    sub-int/2addr v4, v5

    if-le v0, v4, :cond_16

    .line 1001
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0a00ad

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setX(F)V

    .line 1002
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v8, 0x7f0a00ad

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    iget v7, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1003
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v4, :cond_9

    .line 1004
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0a00ad

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarSize:I

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setX(F)V

    .line 1005
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v8, 0x7f0a00ad

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    iget v7, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_7

    .line 1008
    :cond_16
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;

    int-to-float v5, v0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setX(F)V

    .line 1009
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v5, v0, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1010
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v4, :cond_9

    .line 1011
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v5, v0, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_7

    .line 1029
    :pswitch_3
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 1030
    const/4 v4, 0x7

    invoke-virtual {p0, v4}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->setRequestedOrientation(I)V

    .line 1031
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 1033
    .local v2, "l":Landroid/widget/FrameLayout$LayoutParams;
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v4, :cond_19

    .line 1034
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarSize:I

    sub-int v4, v1, v4

    iput v4, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1039
    :goto_8
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f0a00ef

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1041
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1042
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v4, :cond_1a

    .line 1043
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1044
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1045
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarGuideSize:I

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v1, v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setY(F)V

    .line 1050
    :goto_9
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v4, :cond_1b

    .line 1051
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarSize:I

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v1, v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setY(F)V

    .line 1067
    :goto_a
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v4, :cond_17

    .line 1068
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarGuideSize:I

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v0, v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setX(F)V

    .line 1069
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarSize:I

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v0, v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setX(F)V

    .line 1073
    .end local v2    # "l":Landroid/widget/FrameLayout$LayoutParams;
    :cond_17
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 1074
    const/4 v4, 0x6

    invoke-virtual {p0, v4}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->setRequestedOrientation(I)V

    .line 1075
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout$LayoutParams;

    .line 1076
    .local v3, "l1":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f0a00ef

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, v3, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1077
    iput v0, v3, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1078
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1079
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v4, :cond_1e

    .line 1080
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1081
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    const v5, 0x7f02006a

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1086
    :goto_b
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v4, :cond_1f

    .line 1087
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    const v5, 0x7f02006d

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1088
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1089
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarGuideSize:I

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v0, v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setX(F)V

    .line 1090
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarSize:I

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v0, v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setX(F)V

    .line 1105
    :goto_c
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z

    if-eqz v4, :cond_18

    .line 1106
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarGuideSize:I

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v1, v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setY(F)V

    .line 1107
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mCenterBarSize:I

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v1, v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setY(F)V

    .line 1110
    .end local v3    # "l1":Landroid/widget/FrameLayout$LayoutParams;
    :cond_18
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWApp2:Landroid/widget/ImageView;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWApp1:Landroid/widget/ImageView;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1111
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getY()F

    move-result v4

    sput v4, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlY:F

    .line 1112
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1113
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAnimTip3:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1037
    .restart local v2    # "l":Landroid/widget/FrameLayout$LayoutParams;
    :cond_19
    iput v1, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    goto/16 :goto_8

    .line 1047
    :cond_1a
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1048
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    const v5, 0x7f0200aa

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_9

    .line 1055
    :cond_1b
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f0a00ac

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    if-ge v1, v4, :cond_1c

    .line 1056
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f0a00ac

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setY(F)V

    .line 1057
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f0a00ac

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1058
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_a

    .line 1059
    :cond_1c
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f0a00ad

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    sub-int/2addr v4, v5

    if-le v1, v4, :cond_1d

    .line 1060
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0a00ad

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setY(F)V

    .line 1061
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f0a00ad

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    sub-int/2addr v4, v5

    iput v4, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1062
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_a

    .line 1064
    :cond_1d
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    int-to-float v5, v1

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setY(F)V

    goto/16 :goto_a

    .line 1083
    .end local v2    # "l":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v3    # "l1":Landroid/widget/FrameLayout$LayoutParams;
    :cond_1e
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1084
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    const v5, 0x7f0200ac

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_b

    .line 1092
    :cond_1f
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f0a00ac

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    if-ge v0, v4, :cond_20

    .line 1093
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f0a00ac

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setX(F)V

    .line 1094
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f0a00ac

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, v3, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1095
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_c

    .line 1096
    :cond_20
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f0a00ad

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    sub-int/2addr v4, v5

    if-le v0, v4, :cond_21

    .line 1097
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0a00ad

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setX(F)V

    .line 1098
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f0a00ad

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    sub-int/2addr v4, v5

    iput v4, v3, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1099
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_c

    .line 1101
    :cond_21
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;

    int-to-float v5, v0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setX(F)V

    goto/16 :goto_c

    .line 895
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
