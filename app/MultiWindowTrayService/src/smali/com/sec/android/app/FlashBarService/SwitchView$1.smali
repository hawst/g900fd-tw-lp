.class Lcom/sec/android/app/FlashBarService/SwitchView$1;
.super Ljava/lang/Object;
.source "SwitchView.java"

# interfaces
.implements Landroid/animation/TimeAnimator$TimeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/SwitchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/SwitchView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SwitchView;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$1;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeUpdate(Landroid/animation/TimeAnimator;JJ)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/TimeAnimator;
    .param p2, "totalTime"    # J
    .param p4, "deltaTime"    # J

    .prologue
    const/4 v2, 0x0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$1;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mFrames:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$000(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$1;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # operator++ for: Lcom/sec/android/app/FlashBarService/SwitchView;->mFrames:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$008(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$1;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->invalidate()V

    .line 193
    :goto_0
    return-void

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$1;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mTimeAnimator:Landroid/animation/TimeAnimator;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$100(Lcom/sec/android/app/FlashBarService/SwitchView;)Landroid/animation/TimeAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->cancel()V

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$1;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # setter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mFrames:I
    invoke-static {v0, v2}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$002(Lcom/sec/android/app/FlashBarService/SwitchView;I)I

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$1;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mHideRequested:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$200(Lcom/sec/android/app/FlashBarService/SwitchView;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$1;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateZoomOut()V

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$1;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mSwitchWindowControllerCallbacks:Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$300(Lcom/sec/android/app/FlashBarService/SwitchView;)Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;->onFirstAnimationBegin()V

    goto :goto_0

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$1;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # setter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mHideRequested:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$202(Lcom/sec/android/app/FlashBarService/SwitchView;Z)Z

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$1;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # invokes: Lcom/sec/android/app/FlashBarService/SwitchView;->triggerEndCallback()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$400(Lcom/sec/android/app/FlashBarService/SwitchView;)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$1;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # invokes: Lcom/sec/android/app/FlashBarService/SwitchView;->triggerRealEndCallback()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$500(Lcom/sec/android/app/FlashBarService/SwitchView;)V

    goto :goto_0
.end method
