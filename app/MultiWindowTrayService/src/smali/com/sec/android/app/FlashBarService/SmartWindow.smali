.class public Lcom/sec/android/app/FlashBarService/SmartWindow;
.super Ljava/lang/Object;
.source "SmartWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;,
        Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;,
        Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowItemViewHolder;
    }
.end annotation


# static fields
.field private static SCROLL_MOVE_DELAY:I


# instance fields
.field private final APPLIST_LONG_PRESS:I

.field private final APPLIST_TIMER_LONG_PRESS:I

.field private final ESTIMATE_INVALID_VALUE:S

.field private final PEN_WINDOW_RATIO_MARGIN:F

.field private doOneTimeAfterDragStarts:Z

.field private mAddEmptyItemPosition:I

.field private mAddToListItem:I

.field private mAppIconIndex:I

.field mAppIconLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mAppListDragMode:Z

.field private mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

.field mCloseSmartWindowListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mCurrentDownView:Landroid/view/View;

.field private mDisplayHeight:I

.field private mDisplayRect:Landroid/graphics/Rect;

.field private mDisplayWidth:I

.field private mDragEnterChildCount:I

.field mDragHandler:Landroid/os/Handler;

.field private mDragWindow:Landroid/view/Window;

.field mEditSmartWindowListener:Landroid/view/View$OnClickListener;

.field private mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

.field private mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

.field private mGuideWindowLayout:Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;

.field private mHasEditWindow:Z

.field private mIvt:[B

.field private mLastPositionX:I

.field private mLastPositionY:I

.field private mMinHeight:I

.field private mMinSizeRatio:F

.field private mMinWidth:I

.field private mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

.field private mScrollAreaMargin:I

.field private mScrollDirection:I

.field private mShadowPaddingRect:Landroid/graphics/Rect;

.field protected mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

.field private mSmartWindow:Landroid/view/Window;

.field private mSmartWindowAdapter:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

.field private mSmartWindowCloseButton:Landroid/widget/ImageButton;

.field mSmartWindowDragListener:Landroid/view/View$OnDragListener;

.field private mSmartWindowEditButton:Landroid/widget/ImageButton;

.field private mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

.field mSmartWindowItemDragListener:Landroid/view/View$OnDragListener;

.field mSmartWindowItemTouchListener:Landroid/view/View$OnTouchListener;

.field private mSmartWindowKeyListener:Landroid/view/View$OnKeyListener;

.field private mSmartWindowLayout:Landroid/widget/LinearLayout;

.field private mSmartWindowScrollView:Landroid/widget/ScrollView;

.field private mSmartWindowTextView:Landroid/widget/TextView;

.field private mSmartWindowTitle:Landroid/widget/RelativeLayout;

.field private mStatusBarHeight:I

.field private mSupportMultiInstance:Z

.field mTimerHandler:Landroid/os/Handler;

.field mTitleHoverListener:Landroid/view/View$OnHoverListener;

.field private mTitleTouchListener:Landroid/view/View$OnTouchListener;

.field private mVibrator:Landroid/os/SystemVibrator;

.field private mWindowManager:Landroid/view/WindowManager;

.field private mcurDstIndex:I

.field private mcurSrcIndex:I

.field private orgIcon:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    const/16 v0, 0x32

    sput v0, Lcom/sec/android/app/FlashBarService/SmartWindow;->SCROLL_MOVE_DELAY:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p3, "tayinfo"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->APPLIST_TIMER_LONG_PRESS:I

    .line 69
    const/16 v0, 0xca

    iput v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->APPLIST_LONG_PRESS:I

    .line 71
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->PEN_WINDOW_RATIO_MARGIN:F

    .line 72
    iput-short v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->ESTIMATE_INVALID_VALUE:S

    .line 83
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    .line 102
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    .line 103
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mHasEditWindow:Z

    .line 104
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSupportMultiInstance:Z

    .line 111
    iput v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mScrollDirection:I

    .line 114
    iput v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I

    .line 115
    iput v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I

    .line 116
    iput v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddEmptyItemPosition:I

    .line 117
    iput v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddToListItem:I

    .line 118
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->doOneTimeAfterDragStarts:Z

    .line 119
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mAppListDragMode:Z

    .line 123
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    .line 124
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 127
    const/16 v0, 0x26

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mIvt:[B

    .line 154
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartWindow$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartWindow$1;-><init>(Lcom/sec/android/app/FlashBarService/SmartWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowKeyListener:Landroid/view/View$OnKeyListener;

    .line 322
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartWindow$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartWindow$3;-><init>(Lcom/sec/android/app/FlashBarService/SmartWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mEditSmartWindowListener:Landroid/view/View$OnClickListener;

    .line 335
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartWindow$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartWindow$4;-><init>(Lcom/sec/android/app/FlashBarService/SmartWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mCloseSmartWindowListener:Landroid/view/View$OnClickListener;

    .line 466
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartWindow$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartWindow$5;-><init>(Lcom/sec/android/app/FlashBarService/SmartWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mTimerHandler:Landroid/os/Handler;

    .line 613
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartWindow$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartWindow$6;-><init>(Lcom/sec/android/app/FlashBarService/SmartWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mTitleTouchListener:Landroid/view/View$OnTouchListener;

    .line 717
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartWindow$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartWindow$7;-><init>(Lcom/sec/android/app/FlashBarService/SmartWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowItemTouchListener:Landroid/view/View$OnTouchListener;

    .line 738
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartWindow$8;-><init>(Lcom/sec/android/app/FlashBarService/SmartWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowItemDragListener:Landroid/view/View$OnDragListener;

    .line 854
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartWindow$9;-><init>(Lcom/sec/android/app/FlashBarService/SmartWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowDragListener:Landroid/view/View$OnDragListener;

    .line 983
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartWindow$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartWindow$10;-><init>(Lcom/sec/android/app/FlashBarService/SmartWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragHandler:Landroid/os/Handler;

    .line 1056
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartWindow$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartWindow$11;-><init>(Lcom/sec/android/app/FlashBarService/SmartWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mAppIconLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 1095
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartWindow$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartWindow$12;-><init>(Lcom/sec/android/app/FlashBarService/SmartWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mTitleHoverListener:Landroid/view/View$OnHoverListener;

    .line 169
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    .line 170
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .line 171
    iput-object p3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mWindowManager:Landroid/view/WindowManager;

    .line 173
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-direct {v0, v1, p0, v2}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/SmartWindow;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/SystemVibrator;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mVibrator:Landroid/os/SystemVibrator;

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1050010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mStatusBarHeight:I

    .line 178
    invoke-static {}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->getInstance()Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    const-string v1, "multiwindow_facade"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/multiwindow/MultiWindowFacade;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportMultiInstance(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSupportMultiInstance:Z

    .line 181
    return-void

    .line 127
    :array_0
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0xat
        0x0t
        0x14t
        0x0t
        0x0t
        0x0t
        0x20t
        0x1t
        0x0t
        0x0t
        0x7ft
        0x0t
        0x0t
        0x42t
        0x0t
        0x0t
        0x4dt
        0x0t
        0x61t
        0x0t
        0x67t
        0x0t
        0x53t
        0x0t
        0x77t
        0x0t
        0x65t
        0x0t
        0x65t
        0x0t
        0x70t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowAdapter:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mGuideWindowLayout:Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/FlashBarService/SmartWindow;Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;)Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mGuideWindowLayout:Lcom/sec/android/app/FlashBarService/SmartWindow$GuideView;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/view/WindowManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mWindowManager:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/FlashBarService/SmartWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mStatusBarHeight:I

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/FlashBarService/SmartWindow;Landroid/view/View;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/SmartWindow;->findCurIndex(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->orgIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/FlashBarService/SmartWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->orgIcon:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/FlashBarService/SmartWindow;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mIvt:[B

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/os/SystemVibrator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mVibrator:Landroid/os/SystemVibrator;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mHasEditWindow:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/FlashBarService/SmartWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I

    return v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I

    return p1
.end method

.method static synthetic access$2102(Lcom/sec/android/app/FlashBarService/SmartWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mAppListDragMode:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/sec/android/app/FlashBarService/SmartWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddEmptyItemPosition:I

    return v0
.end method

.method static synthetic access$2202(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddEmptyItemPosition:I

    return p1
.end method

.method static synthetic access$2300(Lcom/sec/android/app/FlashBarService/SmartWindow;III)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/SmartWindow;->changlistItemFromEditList(III)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/FlashBarService/SmartWindow;Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/view/DragEvent;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/SmartWindow;->decideScrollMove(Landroid/view/View;Landroid/view/DragEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddToListItem:I

    return v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddToListItem:I

    return p1
.end method

.method static synthetic access$2600(Lcom/sec/android/app/FlashBarService/SmartWindow;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/SmartWindow;->changelistItem(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/FlashBarService/SmartWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->doOneTimeAfterDragStarts:Z

    return v0
.end method

.method static synthetic access$2702(Lcom/sec/android/app/FlashBarService/SmartWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->doOneTimeAfterDragStarts:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/sec/android/app/FlashBarService/SmartWindow;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/SmartWindow;->addAndChangelistItem(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/FlashBarService/SmartWindow;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/SmartWindow;->setDragWindow(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mCurrentDownView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/FlashBarService/SmartWindow;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mCurrentDownView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/sec/android/app/FlashBarService/SmartWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragEnterChildCount:I

    return v0
.end method

.method static synthetic access$3102(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragEnterChildCount:I

    return p1
.end method

.method static synthetic access$3200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/FlashBarService/SmartWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mScrollDirection:I

    return v0
.end method

.method static synthetic access$3400()I
    .locals 1

    .prologue
    .line 64
    sget v0, Lcom/sec/android/app/FlashBarService/SmartWindow;->SCROLL_MOVE_DELAY:I

    return v0
.end method

.method static synthetic access$3420(I)I
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 64
    sget v0, Lcom/sec/android/app/FlashBarService/SmartWindow;->SCROLL_MOVE_DELAY:I

    sub-int/2addr v0, p0

    sput v0, Lcom/sec/android/app/FlashBarService/SmartWindow;->SCROLL_MOVE_DELAY:I

    return v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mAppIconIndex:I

    return v0
.end method

.method static synthetic access$3502(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mAppIconIndex:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/SmartWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSupportMultiInstance:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/view/Window;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/FlashBarService/SmartWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayHeight:I

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayHeight:I

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/FlashBarService/SmartWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayWidth:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p1, "x1"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayWidth:I

    return p1
.end method

.method private addAndChangelistItem(II)Z
    .locals 2
    .param p1, "dstIndex"    # I
    .param p2, "editIndex"    # I

    .prologue
    .line 1005
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->moveToSmartWindowItem(II)V

    .line 1006
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SmartWindow;->updateSmartWindowRelayout()V

    .line 1007
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I

    .line 1008
    const/4 v0, 0x1

    return v0
.end method

.method private changelistItem(II)Z
    .locals 1
    .param p1, "srcIndex"    # I
    .param p2, "dstIndex"    # I

    .prologue
    .line 1012
    if-ne p1, p2, :cond_0

    .line 1013
    const/4 v0, 0x0

    .line 1018
    :goto_0
    return v0

    .line 1015
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0, p2, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->reorderSmartWindowList(II)V

    .line 1017
    iput p2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I

    .line 1018
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private changlistItemFromEditList(III)Z
    .locals 3
    .param p1, "appListIndex"    # I
    .param p2, "editListIndex"    # I
    .param p3, "action"    # I

    .prologue
    const/4 v2, 0x1

    .line 1022
    const/4 v0, 0x6

    if-ne p3, v0, :cond_1

    .line 1023
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->changeSmartWindowItem(IIZ)V

    .line 1025
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SmartWindow;->checkAppListLayout()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1026
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->invalidateViews()V

    .line 1033
    :cond_0
    :goto_0
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I

    .line 1034
    return v2

    .line 1029
    :cond_1
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mAppListDragMode:Z

    if-ne v0, v2, :cond_0

    .line 1030
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0, p1, p2, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->changeSmartWindowItem(IIZ)V

    .line 1031
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->invalidateViews()V

    goto :goto_0
.end method

.method private decideScrollMove(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 12
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v11, 0x1

    const/4 v7, 0x0

    const/4 v10, 0x3

    .line 942
    if-nez p1, :cond_1

    move v0, v7

    .line 980
    :cond_0
    :goto_0
    return v0

    .line 945
    :cond_1
    const/4 v4, -0x1

    .line 946
    .local v4, "rawEnd":I
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0051

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    div-int/lit8 v8, v8, 0x3

    iput v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mScrollAreaMargin:I

    .line 948
    const/4 v0, 0x0

    .line 949
    .local v0, "bRet":Z
    const/4 v8, 0x2

    new-array v2, v8, [I

    .line 950
    .local v2, "mOffset":[I
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 951
    aget v8, v2, v11

    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v9

    float-to-int v9, v9

    add-int v4, v8, v9

    .line 953
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    invoke-virtual {v8}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 955
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v8, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowTitle:Landroid/widget/RelativeLayout;

    invoke-virtual {v9}, Landroid/widget/RelativeLayout;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v8, v9

    iget v9, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mScrollAreaMargin:I

    add-int v6, v8, v9

    .line 956
    .local v6, "scrollTopBound":I
    iget v8, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v9}, Landroid/widget/ScrollView;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v8, v9

    iget v9, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mScrollAreaMargin:I

    sub-int v5, v8, v9

    .line 958
    .local v5, "scrollBottomBound":I
    iget v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I

    const/4 v9, -0x1

    if-ne v8, v9, :cond_2

    iget-boolean v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mHasEditWindow:Z

    if-eqz v8, :cond_3

    .line 959
    :cond_2
    if-le v4, v5, :cond_4

    .line 960
    const/4 v8, 0x4

    iput v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mScrollDirection:I

    .line 961
    const/4 v0, 0x1

    .line 972
    :cond_3
    :goto_1
    if-eqz v0, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v8, v10}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v8

    if-nez v8, :cond_0

    .line 973
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 974
    .local v3, "msg":Landroid/os/Message;
    const/16 v8, 0x32

    sput v8, Lcom/sec/android/app/FlashBarService/SmartWindow;->SCROLL_MOVE_DELAY:I

    .line 975
    iput v10, v3, Landroid/os/Message;->what:I

    .line 976
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v8, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 977
    iput-boolean v11, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->doOneTimeAfterDragStarts:Z

    move v0, v7

    .line 978
    goto :goto_0

    .line 962
    .end local v3    # "msg":Landroid/os/Message;
    :cond_4
    if-ge v4, v6, :cond_5

    .line 963
    iput v10, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mScrollDirection:I

    .line 964
    const/4 v0, 0x1

    goto :goto_1

    .line 966
    :cond_5
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v8, v10}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 967
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v8, v10}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_1
.end method

.method private findCurIndex(Landroid/view/View;)I
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, -0x1

    .line 1044
    instance-of v3, p1, Landroid/widget/LinearLayout;

    if-nez v3, :cond_1

    move v1, v4

    .line 1053
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v2, p1

    .line 1047
    check-cast v2, Landroid/widget/LinearLayout;

    .line 1048
    .local v2, "item":Landroid/widget/LinearLayout;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildCount()I

    move-result v0

    .line 1049
    .local v0, "appListSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 1050
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    check-cast v3, Landroid/widget/LinearLayout;

    if-eq v2, v3, :cond_0

    .line 1049
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v1, v4

    .line 1053
    goto :goto_0
.end method

.method private setDragWindow(Z)V
    .locals 3
    .param p1, "isDragMode"    # Z

    .prologue
    .line 344
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 345
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    if-eqz p1, :cond_1

    .line 346
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->getDimWindow()Landroid/view/Window;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragWindow:Landroid/view/Window;

    .line 347
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragWindow:Landroid/view/Window;

    if-eqz v1, :cond_0

    .line 348
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragWindow:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 349
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x20

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 355
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 356
    return-void

    .line 352
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragWindow:Landroid/view/Window;

    .line 353
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit8 v1, v1, -0x21

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_0
.end method


# virtual methods
.method public checkAppListLayout()Z
    .locals 2

    .prologue
    .line 1038
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getPopupAppCnt()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildCount()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 1039
    const/4 v0, 0x1

    .line 1040
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public closeSmartWindow()V
    .locals 2

    .prologue
    .line 359
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->setAppListSizeChangeListener(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;)V

    .line 360
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SmartWindow;->hideSmartEditWindow()V

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeSmartWindow()V

    .line 362
    return-void
.end method

.method public hideSmartEditWindow()V
    .locals 1

    .prologue
    .line 459
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mHasEditWindow:Z

    .line 460
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->savePopupList()V

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->closeSmartEditWindow()V

    .line 462
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeSmartEditWindow()V

    .line 463
    return-void
.end method

.method public isEnableMakePenWindow()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1187
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v3}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->isEnableMakePenWindow()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1194
    :goto_0
    return v1

    .line 1190
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->getMaxPenWindow(Landroid/content/Context;)I

    move-result v0

    .line 1191
    .local v0, "maxPenWindowCount":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080051

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v2

    invoke-virtual {v4, v5, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    move v1, v2

    .line 1194
    goto :goto_0
.end method

.method public keepLaunchedWindowRatio(Landroid/view/WindowManager$LayoutParams;)V
    .locals 6
    .param p1, "lp"    # Landroid/view/WindowManager$LayoutParams;

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 284
    iget v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayHeight:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayWidth:I

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 285
    .local v1, "disPlayRatio":F
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    int-to-float v2, v2

    iget v3, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 287
    .local v0, "currentRatio":F
    sub-float v2, v1, v5

    cmpg-float v2, v0, v2

    if-gez v2, :cond_2

    .line 288
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    int-to-float v2, v2

    iget v3, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    int-to-float v3, v3

    sub-float v4, v1, v5

    mul-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 289
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    int-to-float v2, v2

    sub-float v3, v1, v5

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 300
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    int-to-float v2, v2

    sub-float v3, v1, v5

    div-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    goto :goto_0

    .line 293
    :cond_2
    add-float v2, v1, v5

    cmpl-float v2, v0, v2

    if-lez v2, :cond_0

    .line 294
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    int-to-float v2, v2

    iget v3, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    int-to-float v3, v3

    add-float v4, v1, v5

    mul-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    .line 295
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    int-to-float v2, v2

    add-float v3, v1, v5

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    goto :goto_0

    .line 297
    :cond_3
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    int-to-float v2, v2

    add-float v3, v1, v5

    div-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 365
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 366
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/SmartWindow;->updateSmartWindowPosition(Z)V

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 371
    :cond_0
    return-void
.end method

.method public pkgManagerList(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->pkgManagerList(Landroid/content/Intent;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowAdapter:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 306
    :cond_0
    return-void
.end method

.method public setEditMode(Z)V
    .locals 4
    .param p1, "enableMode"    # Z

    .prologue
    const v3, 0x7f080001

    .line 309
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mHasEditWindow:Z

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->setEditWindowVisibility(Z)V

    .line 311
    if-eqz p1, :cond_0

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowEditButton:Landroid/widget/ImageButton;

    const v1, 0x7f02005c

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowEditButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080002

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 320
    :goto_0
    return-void

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080022

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowEditButton:Landroid/widget/ImageButton;

    const v1, 0x7f02005d

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowEditButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setSmartEditWindow(Landroid/view/Window;)V
    .locals 2
    .param p1, "window"    # Landroid/view/Window;

    .prologue
    .line 455
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->setWindow(Landroid/view/Window;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V

    .line 456
    return-void
.end method

.method public setSmartWindow(Landroid/view/Window;Landroid/graphics/Rect;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 9
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "displayRect"    # Landroid/graphics/Rect;
    .param p3, "service"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x3f000000    # 0.5f

    .line 185
    iput-object p3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .line 187
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    .line 188
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    const v4, 0x7f0f00e8

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowLayout:Landroid/widget/LinearLayout;

    .line 189
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    const v4, 0x7f0f00ce

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowTitle:Landroid/widget/RelativeLayout;

    .line 190
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    const v4, 0x7f0f00e9

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowTextView:Landroid/widget/TextView;

    .line 191
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    const v4, 0x7f0f00ee

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ScrollView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowScrollView:Landroid/widget/ScrollView;

    .line 192
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    const v4, 0x7f0f00ef

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    .line 193
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    const v4, 0x7f0f00eb

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowEditButton:Landroid/widget/ImageButton;

    .line 194
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    const v4, 0x7f0f00ed

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowCloseButton:Landroid/widget/ImageButton;

    .line 196
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mTitleTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 197
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mTitleHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 198
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 199
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowScrollView:Landroid/widget/ScrollView;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v3, v4}, Landroid/widget/ScrollView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 200
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowEditButton:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mEditSmartWindowListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowCloseButton:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mCloseSmartWindowListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x10e00b8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mMinSizeRatio:F

    .line 205
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 206
    .local v2, "point":Landroid/graphics/Point;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 207
    iget v3, v2, Landroid/graphics/Point;->x:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayWidth:I

    .line 208
    iget v3, v2, Landroid/graphics/Point;->y:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayHeight:I

    .line 209
    iget v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mMinSizeRatio:F

    mul-float/2addr v3, v4

    add-float/2addr v3, v5

    float-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mMinWidth:I

    .line 210
    iget v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayHeight:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mMinSizeRatio:F

    mul-float/2addr v3, v4

    add-float/2addr v3, v5

    float-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mMinHeight:I

    .line 212
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowLayout:Landroid/widget/LinearLayout;

    const v4, 0x7f020058

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 213
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 215
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 216
    .local v1, "l":Landroid/view/WindowManager$LayoutParams;
    if-eqz p2, :cond_6

    .line 217
    iget v3, p2, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mStatusBarHeight:I

    if-ge v3, v4, :cond_0

    .line 218
    iget v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mStatusBarHeight:I

    iput v3, p2, Landroid/graphics/Rect;->top:I

    .line 221
    :cond_0
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mMinWidth:I

    if-ge v3, v4, :cond_1

    .line 222
    iget v3, p2, Landroid/graphics/Rect;->left:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mMinWidth:I

    add-int/2addr v3, v4

    iput v3, p2, Landroid/graphics/Rect;->right:I

    .line 224
    :cond_1
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mMinHeight:I

    if-ge v3, v4, :cond_2

    .line 225
    iget v3, p2, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mMinHeight:I

    add-int/2addr v3, v4

    iput v3, p2, Landroid/graphics/Rect;->bottom:I

    .line 228
    :cond_2
    iget v3, p2, Landroid/graphics/Rect;->right:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayWidth:I

    if-le v3, v4, :cond_3

    .line 229
    iget v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayWidth:I

    iget v4, p2, Landroid/graphics/Rect;->right:I

    sub-int v0, v3, v4

    .line 230
    .local v0, "gap":I
    invoke-virtual {p2, v0, v6}, Landroid/graphics/Rect;->offset(II)V

    .line 232
    .end local v0    # "gap":I
    :cond_3
    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayHeight:I

    if-le v3, v4, :cond_4

    .line 233
    iget v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayHeight:I

    iget v4, p2, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v3, v4

    .line 234
    .restart local v0    # "gap":I
    invoke-virtual {p2, v6, v0}, Landroid/graphics/Rect;->offset(II)V

    .line 237
    .end local v0    # "gap":I
    :cond_4
    iget v3, p2, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 238
    iget v3, p2, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 239
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 240
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 248
    :goto_0
    if-eqz p2, :cond_7

    .line 249
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3, p2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayRect:Landroid/graphics/Rect;

    .line 250
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    neg-int v4, v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    neg-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    .line 254
    :goto_1
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mLastPositionX:I

    .line 255
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mLastPositionY:I

    .line 256
    const/16 v3, 0x33

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 258
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    invoke-virtual {v3, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 259
    const-string v3, "SmartWindow"

    invoke-virtual {v1, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 261
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-interface {v3, v4, v1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 262
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->requestFocus()Z

    .line 264
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getPopupAppCnt()I

    move-result v3

    if-nez v3, :cond_5

    .line 265
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeFlashBarList()V

    .line 268
    :cond_5
    new-instance v3, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    invoke-direct {v3, p0, v4}, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;-><init>(Lcom/sec/android/app/FlashBarService/SmartWindow;Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowAdapter:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    .line 269
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowAdapter:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 271
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->makeSmartEditWindow()V

    .line 272
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    new-instance v4, Lcom/sec/android/app/FlashBarService/SmartWindow$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/SmartWindow$2;-><init>(Lcom/sec/android/app/FlashBarService/SmartWindow;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->setAppListSizeChangeListener(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;)V

    .line 280
    return-void

    .line 242
    :cond_6
    iget v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayWidth:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mMinWidth:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 243
    iget v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayHeight:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mMinHeight:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 244
    iget v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mMinWidth:I

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 245
    iget v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mMinHeight:I

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v4

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    goto/16 :goto_0

    .line 252
    :cond_7
    new-instance v3, Landroid/graphics/Rect;

    iget v4, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v5, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v6, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v7, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/2addr v6, v7

    iget v7, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v8, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v7, v8

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayRect:Landroid/graphics/Rect;

    goto/16 :goto_1
.end method

.method public updateSmartWindowPosition(Z)V
    .locals 9
    .param p1, "configurationChanged"    # Z

    .prologue
    const/4 v5, 0x0

    .line 374
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 375
    .local v2, "fullscreen":Landroid/graphics/Point;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 376
    iget v6, v2, Landroid/graphics/Point;->x:I

    iput v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayWidth:I

    .line 377
    iget v6, v2, Landroid/graphics/Point;->y:I

    iput v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayHeight:I

    .line 378
    const/4 v0, 0x0

    .line 380
    .local v0, "bDataHasChanged":Z
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    invoke-virtual {v6}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 381
    .local v3, "l":Landroid/view/WindowManager$LayoutParams;
    iget-boolean v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mHasEditWindow:Z

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0162

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 383
    .local v1, "editWindowHeight":I
    :goto_0
    if-eqz p1, :cond_8

    .line 385
    iget v4, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 386
    .local v4, "temp":I
    iget v5, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 387
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 389
    iget v5, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    iget v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayHeight:I

    iget v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mStatusBarHeight:I

    sub-int/2addr v6, v7

    if-le v5, v6, :cond_0

    .line 390
    iget v5, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    iget v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mStatusBarHeight:I

    sub-int/2addr v5, v6

    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 393
    :cond_0
    iget v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayWidth:I

    iget v6, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v7, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/2addr v6, v7

    if-ge v5, v6, :cond_6

    .line 394
    iget v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayWidth:I

    iget v6, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v5, v6

    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 398
    :goto_1
    iget v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayHeight:I

    sub-int/2addr v5, v1

    iget v6, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v7, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v6, v7

    if-ge v5, v6, :cond_7

    .line 399
    iget v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayHeight:I

    iget v6, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v6, v1

    sub-int/2addr v5, v6

    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 420
    .end local v4    # "temp":I
    :cond_1
    :goto_2
    iget v5, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mStatusBarHeight:I

    if-ge v5, v6, :cond_2

    .line 421
    iget v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mStatusBarHeight:I

    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 423
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    invoke-virtual {v5, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 424
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindow:Landroid/view/Window;

    invoke-virtual {v6}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    invoke-interface {v5, v6, v3}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 425
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->requestFocus()Z

    .line 427
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getPopupAppCnt()I

    move-result v5

    if-nez v5, :cond_3

    .line 428
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeFlashBarList()V

    .line 429
    const/4 v0, 0x1

    .line 433
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayRect:Landroid/graphics/Rect;

    iget v6, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v6, v5, Landroid/graphics/Rect;->left:I

    .line 434
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayRect:Landroid/graphics/Rect;

    iget v6, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v6, v5, Landroid/graphics/Rect;->top:I

    .line 435
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayRect:Landroid/graphics/Rect;

    iget v6, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v7, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    add-int/2addr v7, v8

    sub-int/2addr v6, v7

    iput v6, v5, Landroid/graphics/Rect;->right:I

    .line 436
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayRect:Landroid/graphics/Rect;

    iget v6, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v7, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v7, v8

    sub-int/2addr v6, v7

    iput v6, v5, Landroid/graphics/Rect;->bottom:I

    .line 438
    if-eqz v0, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowAdapter:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    if-eqz v5, :cond_4

    .line 439
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowAdapter:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    .line 440
    new-instance v5, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;

    invoke-direct {v5, p0, v6}, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;-><init>(Lcom/sec/android/app/FlashBarService/SmartWindow;Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowAdapter:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    .line 442
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowAdapter:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 443
    return-void

    .end local v1    # "editWindowHeight":I
    :cond_5
    move v1, v5

    .line 381
    goto/16 :goto_0

    .line 396
    .restart local v1    # "editWindowHeight":I
    .restart local v4    # "temp":I
    :cond_6
    iget v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mLastPositionX:I

    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    goto/16 :goto_1

    .line 401
    :cond_7
    iget v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mLastPositionY:I

    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    goto/16 :goto_2

    .line 405
    .end local v4    # "temp":I
    :cond_8
    iget v6, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    iget v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayHeight:I

    iget v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mStatusBarHeight:I

    sub-int/2addr v7, v8

    if-le v6, v7, :cond_9

    .line 406
    iget v6, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    iget v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mStatusBarHeight:I

    sub-int/2addr v6, v7

    iput v6, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 408
    :cond_9
    iget v6, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    if-gez v6, :cond_a

    .line 409
    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 411
    :cond_a
    iget v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayWidth:I

    iget v6, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v7, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/2addr v6, v7

    if-ge v5, v6, :cond_b

    .line 412
    iget v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayWidth:I

    iget v6, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v5, v6

    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 414
    :cond_b
    iget v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayHeight:I

    sub-int/2addr v5, v1

    iget v6, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v7, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v6, v7

    if-ge v5, v6, :cond_1

    .line 415
    iget v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDisplayHeight:I

    iget v6, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v6, v1

    sub-int/2addr v5, v6

    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    goto/16 :goto_2
.end method

.method public updateSmartWindowRelayout()V
    .locals 2

    .prologue
    .line 446
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowAdapter:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;->notifyDataSetChanged()V

    .line 447
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowAdapter:Lcom/sec/android/app/FlashBarService/SmartWindow$SmartWindowAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 448
    return-void
.end method
