.class Lcom/sec/android/app/FlashBarService/PopupViewWindow$1;
.super Ljava/lang/Object;
.source "PopupViewWindow.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/PopupViewWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v7, 0x1

    .line 132
    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v7, :cond_1

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->closePopupViewWindow()V

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewEditWindow:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$000(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removePopupViewEditWindow()V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$000(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removePopupViewWindow()V

    :cond_0
    move v0, v7

    .line 150
    .end local p1    # "view":Landroid/view/View;
    :goto_0
    return v0

    .line 139
    .restart local p1    # "view":Landroid/view/View;
    :cond_1
    const/16 v0, 0x42

    if-ne p2, v0, :cond_3

    .line 140
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v7, :cond_2

    move-object v0, p1

    .line 141
    check-cast v0, Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getSelectedItemId()J

    move-result-wide v2

    long-to-int v1, v2

    .line 142
    .local v1, "id":I
    check-cast p1, Landroid/widget/GridView;

    .end local p1    # "view":Landroid/view/View;
    invoke-virtual {p1}, Landroid/widget/GridView;->getSelectedItemPosition()I

    move-result v0

    if-ltz v0, :cond_2

    if-ltz v1, :cond_2

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v6

    .line 144
    .local v6, "l":Landroid/view/WindowManager$LayoutParams;
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$300(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/AppListController;

    move-result-object v0

    iget v2, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v3, v6, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewTitleMargin:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$200(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v4

    add-int/2addr v3, v4

    iget v4, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v5, v6, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/2addr v4, v5

    iget v5, v6, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v8, v6, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v5, v8

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/FlashBarService/AppListController;->startFreeStyleActivity(IIIII)Z

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->closePopupViewWindow()V

    .end local v1    # "id":I
    .end local v6    # "l":Landroid/view/WindowManager$LayoutParams;
    :cond_2
    move v0, v7

    .line 148
    goto :goto_0

    .line 150
    .restart local p1    # "view":Landroid/view/View;
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
