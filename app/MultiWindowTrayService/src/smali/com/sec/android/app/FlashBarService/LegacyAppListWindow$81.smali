.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 4685
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v8, 0x7f080002

    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 4688
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-boolean v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbAnimating:Z

    if-nez v3, :cond_3

    .line 4689
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 4690
    const-string v3, "LegacyAppListWindow"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onClick : mExpandAppList = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4724
    :goto_0
    return-void

    .line 4694
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->cancelCollapseTimer()V
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    .line 4695
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v3, v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 4696
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200bc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 4697
    .local v0, "dim":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0029

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v2, v3

    .line 4698
    .local v2, "paddingTop":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a002a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v1, v3

    .line 4700
    .local v1, "paddingBottom":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v5, v0, v5, v5}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 4701
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v6, v2, v6, v1}, Landroid/widget/Button;->setPadding(IIII)V

    .line 4702
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 4703
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setSoundEffectsEnabled(Z)V

    .line 4704
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 4705
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/TextView;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 4706
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4707
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketEditBtn:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageButton;

    move-result-object v3

    const v4, 0x7f020031

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 4709
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->makeAppListEditWindow()V

    .line 4710
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    invoke-virtual {v3, v4, v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->displayAppListEditWindow(IZ)V

    .line 4711
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageButton;

    move-result-object v3

    const v4, 0x7f0200c4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 4712
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateBtn:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageButton;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setSoundEffectsEnabled(Z)V

    .line 4713
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPocketTemplateText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/TextView;

    move-result-object v3

    const v4, 0x3f19999a    # 0.6f

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 4714
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 4715
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4717
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->updateAppListForScrollView(Ljava/util/List;Z)V

    .line 4718
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->setDisabledActivatedItems(Z)V

    .line 4723
    .end local v0    # "dim":Landroid/graphics/drawable/Drawable;
    .end local v1    # "paddingBottom":I
    .end local v2    # "paddingTop":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissHistoryWindow(Z)V
    invoke-static {v3, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)V

    goto/16 :goto_0

    .line 4720
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->hideAppEditList(Z)V

    .line 4721
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$81;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startCollapseTimer()V
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    goto :goto_1
.end method
