.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$13;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->makeHistoryBarDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 1107
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1110
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v0

    .line 1111
    .local v0, "childCnt":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 1112
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 1113
    .local v2, "thumnail":Landroid/widget/RelativeLayout;
    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1114
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v4, v3, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mActivityManager:Landroid/app/ActivityManager;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbnailTaskInfos:Ljava/util/List;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget v3, v3, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v3, v5, v6}, Landroid/app/ActivityManager;->moveTaskToFront(IILandroid/os/Bundle;)V

    .line 1115
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->closeFlashBar()V
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    .line 1119
    .end local v2    # "thumnail":Landroid/widget/RelativeLayout;
    :cond_0
    return-void

    .line 1111
    .restart local v2    # "thumnail":Landroid/widget/RelativeLayout;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
