.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$12;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

.field final synthetic val$sendIntent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 1205
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$12;->val$sendIntent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/16 v1, 0x8

    .line 1218
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$2900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1219
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$2900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1220
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 1221
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1222
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationHistoryBarThumbNailNew()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    .line 1223
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1213
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1209
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMakeInstanceAniRunning:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$2702(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 1210
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$12;->val$sendIntent:Landroid/content/Intent;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mNewInstanceIntent:Landroid/content/Intent;
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$2802(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/content/Intent;)Landroid/content/Intent;

    .line 1211
    return-void
.end method
