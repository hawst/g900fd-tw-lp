.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 3839
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x1

    .line 3842
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 3875
    :cond_0
    :goto_0
    return-void

    .line 3846
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->cancelCollapseTimer()V
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    .line 3847
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/16 v8, 0x64

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v5

    .line 3848
    .local v5, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 3849
    .local v4, "resumedInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v1, 0x0

    .line 3850
    .local v1, "count":I
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationIcon:Ljava/util/List;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    .line 3851
    .local v0, "animationMaxSize":I
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 3852
    .local v6, "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    if-eqz v7, :cond_2

    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v7}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getType()I

    move-result v7

    if-ne v7, v9, :cond_2

    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v7}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v7

    if-eqz v7, :cond_2

    if-ge v1, v0, :cond_2

    .line 3856
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3857
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationIcon:Ljava/util/List;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v8, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getIconByRunningTaskInfo(Landroid/app/ActivityManager$RunningTaskInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3858
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3862
    .end local v6    # "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    if-le v7, v9, :cond_5

    .line 3863
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iput-object v4, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResumedInfos:Ljava/util/List;

    .line 3864
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->canAddTemplate()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 3865
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResumedInfos:Ljava/util/List;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getDefaultTemplateText(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 3866
    .local v2, "defaultTitle":Ljava/lang/CharSequence;
    if-eqz v2, :cond_4

    .line 3867
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v7, v2, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->createTemplateDialog(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 3869
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v9, 0x7f080020

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->createUnableDialog(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 3873
    .end local v2    # "defaultTitle":Ljava/lang/CharSequence;
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v9, 0x7f080021

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->createUnableDialog(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
