.class Lcom/sec/android/app/FlashBarService/PopupViewWindow$13;
.super Ljava/lang/Object;
.source "PopupViewWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/PopupViewWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V
    .locals 0

    .prologue
    .line 708
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 711
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1400(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->isEnableMakePenWindow()Z

    move-result v0

    if-nez v0, :cond_0

    .line 718
    :goto_0
    return-void

    .line 714
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 715
    .local v6, "count":I
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v7

    .line 716
    .local v7, "l":Landroid/view/WindowManager$LayoutParams;
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$300(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/AppListController;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerSelectedPage:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$400(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupWindowItemNum:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1500(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v2

    mul-int/2addr v1, v2

    add-int/2addr v1, v6

    iget v2, v7, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v3, v7, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewTitleMargin:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$200(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v4

    add-int/2addr v3, v4

    iget v4, v7, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v5, v7, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/2addr v4, v5

    iget v5, v7, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v8, v7, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v5, v8

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/FlashBarService/AppListController;->startFreeStyleActivity(IIIII)Z

    .line 717
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->closePopupViewWindow()V

    goto :goto_0
.end method
