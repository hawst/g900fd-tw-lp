.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$23;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->makeHistoryBarDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 1543
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$23;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 1546
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1547
    .local v0, "taskId":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$23;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mActivityManager:Landroid/app/ActivityManager;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2, v3}, Landroid/app/ActivityManager;->removeTask(II)Z

    .line 1548
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$23;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->setRemoveTask(I)V

    .line 1549
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$23;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissHistoryWindow(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)V

    .line 1550
    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-nez v1, :cond_0

    .line 1551
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$23;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListRelayout(Z)V

    .line 1555
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$23;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$23$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$23$1;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$23;)V

    const-wide/16 v4, 0x320

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1561
    return-void
.end method
