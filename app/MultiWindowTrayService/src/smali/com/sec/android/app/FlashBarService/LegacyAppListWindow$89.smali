.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$89;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateScrollPositionTemplate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 5522
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$89;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 5525
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$89;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateIconIndex:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    if-gez v1, :cond_0

    .line 5526
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$89;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateIconIndex:I
    invoke-static {v1, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9302(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 5528
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$89;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$89;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateIconIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getY()F

    move-result v1

    float-to-int v0, v1

    .line 5529
    .local v0, "y":I
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$89;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ScrollView;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    .line 5530
    return-void
.end method
