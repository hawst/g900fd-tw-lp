.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$2;
.super Landroid/content/BroadcastReceiver;
.source "AppListBezelWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 530
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 533
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastMultiWindowTypeArray:Landroid/util/SparseArray;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/util/SparseArray;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mUserId:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 534
    .local v0, "lastMultiWindowType":I
    if-eq v0, v6, :cond_0

    const-string v2, "com.sec.android.extra.MULTIWINDOW_SPLIT"

    invoke-virtual {p2, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 536
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getFrontActivityMultiWindowStyle(I)Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v1

    .line 539
    .local v1, "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getType()I

    move-result v2

    if-ne v2, v6, :cond_0

    const/high16 v2, 0x200000

    invoke-virtual {v1, v2}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 540
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$2$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$2$1;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow$2;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 546
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastMultiWindowTypeArray:Landroid/util/SparseArray;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/util/SparseArray;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mUserId:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 549
    .end local v1    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_0
    return-void
.end method
