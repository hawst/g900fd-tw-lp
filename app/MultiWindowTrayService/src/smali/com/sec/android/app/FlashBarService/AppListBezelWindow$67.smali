.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 4268
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 21
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 4271
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 4325
    :cond_0
    :goto_0
    return-void

    .line 4275
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-object/from16 v17, v0

    const/16 v18, 0x64

    const/16 v19, 0x1

    invoke-virtual/range {v17 .. v19}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v14

    .line 4276
    .local v14, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 4277
    .local v12, "resumedInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v7, 0x0

    .line 4278
    .local v7, "count":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationIcon:Ljava/util/List;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v6

    .line 4279
    .local v6, "animationMaxSize":I
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 4280
    .local v15, "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v0, v15, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2

    iget-object v0, v15, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getType()I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    iget-object v0, v15, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v17

    if-eqz v17, :cond_2

    if-ge v7, v6, :cond_2

    .line 4284
    invoke-interface {v12, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationIcon:Ljava/util/List;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getIconByRunningTaskInfo(Landroid/app/ActivityManager$RunningTaskInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 4286
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 4290
    .end local v15    # "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_3
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_6

    .line 4291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iput-object v12, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResumedInfos:Ljava/util/List;

    .line 4292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->canAddTemplate()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 4293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResumedInfos:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getDefaultTemplateText(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v8

    .line 4294
    .local v8, "defaultTitle":Ljava/lang/CharSequence;
    if-eqz v8, :cond_5

    .line 4295
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResumedInfos:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v17 .. v20}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addTemplate(Ljava/util/List;Ljava/lang/String;Z)I

    move-result v11

    .line 4296
    .local v11, "iconIndex":I
    if-ltz v11, :cond_0

    .line 4299
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateIconIndex:I
    invoke-static {v0, v11}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 4300
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->prepareAnimationIcon()V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    .line 4301
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/FlashBarService/AppListController;->updateWindowRects()V

    .line 4302
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResumedInfos:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v9, v0, :cond_4

    .line 4303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationIcon:Ljava/util/List;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 4305
    .local v4, "animationIcon":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResumedInfos:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/app/ActivityManager$RunningTaskInfo;

    move-object/from16 v0, v17

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v16

    .line 4306
    .local v16, "zoneInfo":I
    new-instance v5, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v5, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4308
    .local v5, "animationIconMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->getRectByZone(I)Landroid/graphics/Rect;

    move-result-object v13

    .line 4309
    .local v13, "splitRect":Landroid/graphics/Rect;
    iget v0, v13, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    iget v0, v13, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    iget v0, v13, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    div-int/lit8 v18, v18, 0x2

    add-int v17, v17, v18

    iget v0, v13, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    iget v0, v13, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    div-int/lit8 v19, v19, 0x2

    add-int v18, v18, v19

    const/16 v19, 0x0

    const/16 v20, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 4312
    new-instance v17, Landroid/widget/FrameLayout$LayoutParams;

    move-object/from16 v0, v17

    invoke-direct {v0, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4302
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_2

    .line 4315
    .end local v4    # "animationIcon":Landroid/widget/ImageView;
    .end local v5    # "animationIconMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v13    # "splitRect":Landroid/graphics/Rect;
    .end local v16    # "zoneInfo":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateIconIndex:I
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v18

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->templateAnimationIcon:Landroid/view/View;
    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8402(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/view/View;)Landroid/view/View;

    .line 4316
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationTemplateStart()V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    .line 4317
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->openFlashBar()V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    goto/16 :goto_0

    .line 4319
    .end local v9    # "i":I
    .end local v11    # "iconIndex":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const v19, 0x7f080020

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->createUnableDialog(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 4323
    .end local v8    # "defaultTitle":Ljava/lang/CharSequence;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const v19, 0x7f080021

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->createUnableDialog(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
