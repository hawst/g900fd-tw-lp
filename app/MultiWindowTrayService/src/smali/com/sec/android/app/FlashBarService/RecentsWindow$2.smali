.class Lcom/sec/android/app/FlashBarService/RecentsWindow$2;
.super Ljava/lang/Object;
.source "RecentsWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/RecentsWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/RecentsWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/RecentsWindow;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 230
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 231
    .local v0, "action":I
    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/RecentsWindow;->access$100(Lcom/sec/android/app/FlashBarService/RecentsWindow;)Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->isInContentArea(II)Z

    move-result v1

    if-nez v1, :cond_1

    .line 234
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/RecentsWindow;->dismiss()V

    .line 235
    const/4 v1, 0x1

    .line 237
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
