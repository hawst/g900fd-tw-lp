.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$7;
.super Ljava/lang/Object;
.source "CenterBarWindow.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0

    .prologue
    .line 1011
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1013
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 1047
    :cond_0
    :goto_0
    :pswitch_0
    return v6

    .line 1016
    :pswitch_1
    const/4 v2, 0x0

    .line 1018
    .local v2, "hoverImage":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getArrangeState()I

    move-result v3

    .line 1019
    .local v3, "mode":I
    if-le v3, v5, :cond_1

    .line 1020
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02006c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1030
    :goto_1
    if-eqz v2, :cond_0

    .line 1031
    const/4 v4, 0x0

    :try_start_0
    invoke-static {v4, v2}, Landroid/view/PointerIcon;->setHoveringSpenIcon(ILandroid/graphics/drawable/Drawable;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1033
    :catch_0
    move-exception v0

    .line 1034
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "CenterBarWindow"

    const-string v5, "Failed to change Pen Point to HOVERING_SPENICON_DEFAULT"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1022
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1100(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v4

    if-ne v4, v5, :cond_2

    .line 1023
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02006b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto :goto_1

    .line 1025
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02006d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto :goto_1

    .line 1035
    :catch_1
    move-exception v0

    .line 1036
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v4, "CenterBarWindow"

    const-string v5, "Failed to change Pen Point to HOVERING_SPENICON_DEFAULT"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1041
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    .end local v2    # "hoverImage":Landroid/graphics/drawable/Drawable;
    .end local v3    # "mode":I
    :pswitch_2
    const/4 v4, 0x1

    const/4 v5, -0x1

    :try_start_1
    invoke-static {v4, v5}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 1042
    :catch_2
    move-exception v1

    .line 1043
    .local v1, "e1":Landroid/os/RemoteException;
    const-string v4, "CenterBarWindow"

    const-string v5, "Failed to change Pen Point to HOVERING_SPENICON_DEFAULT"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1013
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
