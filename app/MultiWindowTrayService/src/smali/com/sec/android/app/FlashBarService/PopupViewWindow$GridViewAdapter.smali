.class public Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "PopupViewWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/PopupViewWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GridViewAdapter"
.end annotation


# instance fields
.field mInflater:Landroid/view/LayoutInflater;

.field mViewPagerNum:I

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;I)V
    .locals 2
    .param p2, "position"    # I

    .prologue
    .line 726
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 727
    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupWindowItemNum:I
    invoke-static {p1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1500(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v0

    mul-int/2addr v0, p2

    iput v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->mViewPagerNum:I

    .line 728
    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1600(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 729
    return-void
.end method


# virtual methods
.method public createView(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    const/4 v4, 0x0

    .line 759
    const/4 v0, 0x0

    .line 760
    .local v0, "convertView":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mLastConfig:Landroid/content/res/Configuration;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1800(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 761
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f03000b

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 766
    :goto_0
    new-instance v1, Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;

    invoke-direct {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;-><init>()V

    .line 767
    .local v1, "holder":Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;
    const v2, 0x7f0f0059

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;->iconView:Landroid/widget/ImageView;

    .line 768
    iget-object v2, v1, Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 769
    iget-object v2, v1, Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 770
    const v2, 0x7f0f005c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;->labelView:Landroid/widget/TextView;

    .line 771
    iget-object v2, v1, Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;->labelView:Landroid/widget/TextView;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 772
    iget-object v2, v1, Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;->labelView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 774
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerIconTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 775
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerIconClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 778
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 779
    return-object v0

    .line 763
    .end local v1    # "holder":Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f03000c

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 733
    const/4 v0, 0x0

    .line 734
    .local v0, "appcnt":I
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1700(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 735
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1700(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v1

    const/16 v2, 0x67

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getViewPagerAppListCnt(I)I

    move-result v0

    .line 736
    iget v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->mViewPagerNum:I

    sub-int v1, v0, v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupWindowItemNum:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1500(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v2

    if-le v1, v2, :cond_0

    .line 737
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupWindowItemNum:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1500(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v1

    .line 742
    :goto_0
    return v1

    .line 739
    :cond_0
    iget v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->mViewPagerNum:I

    sub-int v1, v0, v1

    goto :goto_0

    .line 742
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 748
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1700(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1700(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->mViewPagerNum:I

    add-int/2addr v1, p1

    const/16 v2, 0x67

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getViewPagerAppListItem(II)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 755
    iget v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->mViewPagerNum:I

    add-int/2addr v0, p1

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 784
    if-nez p2, :cond_0

    .line 785
    invoke-virtual {p0, p3, p1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->createView(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p2

    .line 786
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 787
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Recycled child has parent"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 789
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 790
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Recycled child has parent"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 793
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;

    .line 794
    .local v0, "holder":Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;
    const/4 v2, 0x0

    .line 796
    .local v2, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1700(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;->mViewPagerNum:I

    add-int/2addr v4, p1

    const/16 v5, 0x67

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getViewPagerAppListItem(II)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v2

    .line 798
    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    .line 799
    .local v1, "label":Ljava/lang/CharSequence;
    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;->labelView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 800
    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 801
    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 802
    return-object p2
.end method
